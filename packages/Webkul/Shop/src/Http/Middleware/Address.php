<?php

namespace Webkul\Shop\Http\Middleware;

use Closure;

class Address
{
    protected $customer;    
    protected $address;    

    public function __construct()
    {
        $this->customer = auth()->guard('customer')->user();
        $this->address = auth()->guard('customer')->user()->addresses;
    }
    
    public function handle($request, Closure $next)
    {
        switch ($request->route()->getName()) {
            case 'customer.address.edit':
            case 'customer.address.update':
            case 'make.default.address':
            case 'address.delete':
                $findAddress = $this->address->find(base64_decode($request->route('id')));

                if(empty($findAddress)){
                        abort(404, 'Restricted access to the ID');
                    return redirect()->back();
                }
                break;
        }
        return $next($request);
    }
}
<?php

namespace Webkul\Shop\Http\Middleware;

use Webkul\Sales\Repositories\OrderRepository;
use Webkul\Sales\Repositories\InvoiceRepository;
use Closure;

class Order
{
    protected $order;

    protected $invoice;

    public function __construct(OrderRepository $order, InvoiceRepository $invoice)
    {
        $this->order = $order;
        $this->invoice = $invoice;
    }
    
    public function handle($request, Closure $next)
    {
        $orders = $this->order->findWhere(['customer_id' => auth()->guard('customer')->user()->id]);

        switch ($request->route()->getName()) {
            case 'customer.orders.view':
                    $findOrder = $orders->find(base64_decode($request->route('id')));
                    if(empty($findOrder)){
                        abort(404, 'Restricted access');
                    }
                break;
            case 'customer.orders.print':
                    $findInvoice = $this->invoice->findWhereIn('order_id', $orders->pluck('id')->toArray())
                                                ->find(base64_decode($request->route('id')));
                    if(empty($findInvoice)){
                        abort(404, 'Restricted access');
                    }
                break;
        }
        return $next($request);
    }
}
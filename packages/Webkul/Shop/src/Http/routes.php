<?php

Route::get('cache-clear', function () {
    \Artisan::call('cache:clear');
    \Artisan::call('config:clear');
    \Artisan::call('route:clear');
    \Artisan::call('view:clear');
    \Artisan::call('clear-compiled');
    dd("cleared");
});


// Route::get('failure', function(){
//     $failure = DB::table('failed_implants')->get();
//     $data = [];

//     foreach($failure as $fail){

//         if($fail->replacement_product_id ==0){
//             DB::table('failed_implants')->where('id', $fail->id)->delete();
//             continue;
//         }else{
//             if($fail->order_type=='on'){    
//                 $order_item = DB::table('order_items')->where('id', $fail->order_item_id)->first();
//                 $data[] = [
//                     'failed_implants_id' => $fail->id,
//                     'order' => $order_item->order_id,
//                     'item' => $fail->order_item_id,
//                     'quantity' => '1',
//                     'type' => '1'
//                 ];
//                 $data[] = [
//                     'failed_implants_id' => $fail->id,
//                     'order' => '',
//                     'item' => $fail->replacement_product_id,
//                     'quantity' => '1',
//                     'type' => '2'
//                 ];
//             }else{
//                 $data[] = [
//                     'failed_implants_id' => $fail->id,
//                     'order' => $fail->order_number,
//                     'item' => $fail->order_item,
//                     'quantity' => '1',
//                     'type' => '1'
//                 ];
//                 $data[] = [
//                     'failed_implants_id' => $fail->id,
//                     'order' => '',
//                     'item' => $fail->replacement_product_id,
//                     'quantity' => '1',
//                     'type' => '2'
//                 ];            
//             } 
//         }               
//     }
//     DB::table('return_implants_items')->insert($data);
//     dd($data);
// });

/*
Route::POST('/response_dumper', function(){
	//$file = fopen('/var/www/html/storage/req_dump.txt','w');
	//fwrite($file, json_encode(request()->all()));
	//fclose($file);
	$data = request()->all();
	$tez = json_decode($data['details']['tezResponse']);
	$return_data = ['ok'=>false];
	
	if($tez['Status'] == "SUCCESS"){
		$return_data = ['ok'=>true];		
	}
	
	return $return_data;
}); */

Route::group(['middleware' => ['web', 'theme', 'locale', 'currency']], function () {
    
    //Store front home
    Route::get('/', 'Webkul\Shop\Http\Controllers\HomeController@index')->defaults('_config', [
        'view' => 'shop::home.index'
    ])->name('shop.home.index');

    Route::get('/contact-us', 'Webkul\Shop\Http\Controllers\ContactController@index')->defaults('_config', [
        'view' => 'shop::contact-us.index'
    ])->name('shop.contact.index');

    Route::post('/contact-us', 'Webkul\Shop\Http\Controllers\ContactController@store')->defaults('_config', [
        'redirect' => 'shop.contact.index'
    ])->name('shop.contact.store');

    Route::get('/product-list', function(){
        return view('shop::product-page.index');
    })->name('shop.home.products');

    Route::get('/events', 'Webkul\Shop\Http\Controllers\EventController@index')->defaults('_config', [
        'view' => 'shop::events.index'
    ])->name('shop.event.index');

    Route::get('/events/{slug}', 'Webkul\Shop\Http\Controllers\EventController@show')->defaults('_config', [
        'view' => 'shop::events.show'
    ])->name('shop.event.show');

    Route::post('/events/{slug}', 'Webkul\Shop\Http\Controllers\EventController@EventRegister')->defaults('_config', [
        'redirect' => 'shop.event.index'
    ])->name('shop.event.register');


    Route::get('/courses', 'Webkul\Shop\Http\Controllers\CourseController@index')->defaults('_config', [
        'view' => 'shop::courses.index'
    ])->name('shop.course.index');

    Route::get('/courses/{slug}', 'Webkul\Shop\Http\Controllers\CourseController@show')->defaults('_config', [
        'view' => 'shop::courses.show'
    ])->name('shop.course.show');

    Route::post('/courses/{slug}', 'Webkul\Shop\Http\Controllers\CourseController@CourseRegister')->defaults('_config', [
        'redirect' => 'shop.course.index'
    ])->name('shop.course.register');

    //pages
    Route::get('/{slug}', 'Webkul\Shop\Http\Controllers\PagesController@index')->defaults('_config', [
        'view' => 'shop::pages.index'
    ])->name('shop.pages.index');

    //subscription
    //subscribe
    Route::get('/subscribe', 'Webkul\Shop\Http\Controllers\SubscriptionController@subscribe')->name('shop.subscribe');

    //unsubscribe
    Route::get('/unsubscribe/{token}', 'Webkul\Shop\Http\Controllers\SubscriptionController@unsubscribe')->name('shop.unsubscribe');

    //Store front header nav-menu fetch
    Route::get('/categories/{slug}', 'Webkul\Shop\Http\Controllers\CategoryController@index')->defaults('_config', [
        'view' => 'shop::products.index'
    ])->name('shop.categories.index');

    Route::post('/get-product', 'Webkul\Shop\Http\Controllers\CategoryController@GetProductsByDiameter')->name('shop.ajax.getproducts');

    //Store front search
    Route::post('/search', 'Webkul\Shop\Http\Controllers\SearchController@index')->defaults('_config', [
        'view' => 'shop::search.search'
    ])->name('shop.search.index');

    //Country State Selector
    Route::get('get/countries', 'Webkul\Core\Http\Controllers\CountryStateController@getCountries')->defaults('_config', [
        'view' => 'shop::test'
    ])->name('get.countries');

    //Get States When Country is Passed
    Route::get('get/states/{country}', 'Webkul\Core\Http\Controllers\CountryStateController@getStates')->defaults('_config', [
        'view' => 'shop::test'
    ])->name('get.states');

    //checkout and cart
    //Cart Items(listing)
    Route::get('checkout/cart', 'Webkul\Shop\Http\Controllers\CartController@index')->defaults('_config', [
        'view' => 'shop::checkout.cart.index'
    ])->name('shop.checkout.cart.index');

    //Cart Items Add
    Route::post('checkout/cart/add/{id}', 'Webkul\Shop\Http\Controllers\CartController@add')->defaults('_config',[
        'redirect' => 'shop.checkout.cart.index'
    ])->name('cart.add');

    //Cart Items remove product quantity
    Route::post('checkout/cart/remove/{id}', 'Webkul\Shop\Http\Controllers\CartController@removeQty')->defaults('_config',[
        'redirect' => 'shop.checkout.cart.index'
    ])->name('cart.remove_qty');

    //Cart Items Add Configurable for more
    Route::get('checkout/cart/addconfigurable/{slug}', 'Webkul\Shop\Http\Controllers\CartController@addConfigurable')->name('cart.add.configurable');

    //Cart Items Remove
    Route::get('checkout/cart/remove/{id}', 'Webkul\Shop\Http\Controllers\CartController@remove')->name('cart.remove');

    //Cart Update Before Checkout
    Route::post('/checkout/cart', 'Webkul\Shop\Http\Controllers\CartController@updateBeforeCheckout')->defaults('_config',[
        'redirect' => 'shop.checkout.cart.index'
    ])->name('shop.checkout.cart.update');

    //Cart Items Remove
    Route::get('/checkout/cart/remove/{id}', 'Webkul\Shop\Http\Controllers\CartController@remove')->defaults('_config',[
        'redirect' => 'shop.checkout.cart.index'
    ])->name('shop.checkout.cart.remove');

    //Cart Items Remove
    Route::get('/minicart', 'Webkul\Shop\Http\Controllers\CartController@MiniCart')->defaults('_config',[
        'view' => 'shop::checkout.cart.mini-cart'
    ])->name('shop.checkout.minicart');

    //Cart Items Remove
    Route::post('/notify-me','Webkul\Shop\Http\Controllers\CartController@NotifyMe')->name('shop.checkout.notifyme');

    //Ref by dealer

    Route::get('/ref/{code}', 'Webkul\Customer\Http\Controllers\RegistrationController@dealerInvite')->name('dealer.invite.code');

    //Checkout Index page
    Route::get('/checkout/onepage', 'Webkul\Shop\Http\Controllers\OnepageController@index')->defaults('_config', [
        'view' => 'shop::checkout.onepage'
    ])->name('shop.checkout.onepage.index');

    Route::get('/checkout/promotion-products/{discount_id}', 'Webkul\Shop\Http\Controllers\OnepageController@ListPromotionProducts')->defaults('_config', [
        'view' => 'shop::checkout.promotion_products'
    ])->name('shop.checkout.promotion.products');
    //mail send in background
    /*Route::post('/checkout/trigger-mail', 'Webkul\Shop\Http\Controllers\OnepageController@TriggerMail')->name('trigger.mail');*/
    Route::post('/checkout/trigger-mail', 'Webkul\Shop\Http\Controllers\OnepageController@TriggerMail')->name('trigger.mail');

    Route::post('/checkout/razorpay', 'Webkul\Shop\Http\Controllers\OnepageController@RazorpayPayment')->name('razorpay.payment');
    Route::post('/checkout/notifystatus', 'Webkul\Shop\Http\Controllers\OnepageController@notifystatus')->name('onepage.notifystatus');

    Route::get('/checkout/after-razorpay', 'Webkul\Shop\Http\Controllers\OnepageController@RazorpayAfterPayment')->name('razorpay.after_payment');

    //Checkout Save Address Form Store
    Route::post('/checkout/save-address', 'Webkul\Shop\Http\Controllers\OnepageController@saveAddress')->name('shop.checkout.save-address');

    //Checkout Save Shipping Address Form Store
    Route::post('/checkout/save-shipping', 'Webkul\Shop\Http\Controllers\OnepageController@saveShipping')->name('shop.checkout.save-shipping');

    //Checkout Save Payment Method Form
    Route::post('/checkout/save-payment', 'Webkul\Shop\Http\Controllers\OnepageController@savePayment')->name('shop.checkout.save-payment');

    //Checkout Save Order
    Route::post('/checkout/save-order', 'Webkul\Shop\Http\Controllers\OnepageController@saveOrder')->name('shop.checkout.save-order');

    //Checkout Order Successfull
    Route::get('/checkout/success', 'Webkul\Shop\Http\Controllers\OnepageController@success')->defaults('_config', [
        'view' => 'shop::checkout.success'
    ])->name('shop.checkout.success');

    //Checkout coupon apply
    Route::post('/checkout/promocode', 'Webkul\Shop\Http\Controllers\OnepageController@ApplyPromoCodeDiscount')->name('shop.checkout.apply.promocode');

    //Checkout membership apply
    Route::post('/checkout/membership', 'Webkul\Shop\Http\Controllers\OnepageController@ApplyMemberShipDiscount')->name('shop.checkout.apply.membership');

    Route::post('/checkout/creditdiscount', 'Webkul\Shop\Http\Controllers\OnepageController@ApplyCreditDiscount')->name('shop.checkout.apply.credit-discount');


    //Checkout reward apply
    Route::post('/checkout/reward', 'Webkul\Shop\Http\Controllers\OnepageController@ApplyReward')->name('shop.checkout.apply.reward');
    //Checkout coupon remove
    Route::post('/checkout/remove', 'Webkul\Shop\Http\Controllers\OnepageController@RemoveCoupon')->name('shop.checkout.remove.discount');

    Route::post('/checkout/promotion', 'Webkul\Shop\Http\Controllers\OnepageController@ApplyPromotion')->name('shop.checkout.apply.promotion');

    Route::post('/checkout/get-promotion-products/{category_id}', 'Webkul\Shop\Http\Controllers\OnepageController@GetPromotionProducts')->name('shop.checkout.get.promotionProducts');

    //Shop buynow button action
    Route::get('buynow/{id}', 'Webkul\Shop\Http\Controllers\CartController@buyNow')->name('shop.product.buynow');

    //Shop buynow button action
    Route::get('move/wishlist/{id}', 'Webkul\Shop\Http\Controllers\CartController@moveToWishlist')->name('shop.movetowishlist');

    //Show Product Details Page(For individually Viewable Product)
    Route::get('/products/{slug}', 'Webkul\Shop\Http\Controllers\ProductController@index')->defaults('_config', [
        'view' => 'shop::products.view'
    ])->name('shop.products.index');

    // Show Product Review Form
    Route::get('/reviews/{slug}', 'Webkul\Shop\Http\Controllers\ReviewController@show')->defaults('_config', [
        'view' => 'shop::products.reviews.index'
    ])->name('shop.reviews.index');

    // Show Product Review(listing)
    Route::get('/product/{slug}/review', 'Webkul\Shop\Http\Controllers\ReviewController@create')->defaults('_config', [
        'view' => 'shop::products.reviews.create'
    ])->name('shop.reviews.create');

    // Show Product Review Form Store
    Route::post('/product/{slug}/review', 'Webkul\Shop\Http\Controllers\ReviewController@store')->defaults('_config', [
        'redirect' => 'customer.reviews.index'
    ])->name('shop.reviews.store');

    //customer routes starts here
    Route::prefix('customer')->group(function () {
        // forgot Password Routes
        // Forgot Password Form Show
        Route::get('/forgot-password', 'Webkul\Customer\Http\Controllers\ForgotPasswordController@create')->defaults('_config', [
            'view' => 'shop::customers.signup.forgot-password'
        ])->name('customer.forgot-password.create');

        // Forgot Password Form Store
        Route::post('/forgot-password', 'Webkul\Customer\Http\Controllers\ForgotPasswordController@store')->name('customer.forgot-password.store');

        // Reset Password Form Show
        Route::get('/reset-password/{token}', 'Webkul\Customer\Http\Controllers\ResetPasswordController@create')->defaults('_config', [
            'view' => 'shop::customers.signup.reset-password'
        ])->name('customer.reset-password.create');

        // Reset Password Form Store
        Route::post('/reset-password', 'Webkul\Customer\Http\Controllers\ResetPasswordController@store')->defaults('_config', [
            'redirect' => 'customer.profile.index'
        ])->name('customer.reset-password.store');

        // Login Routes
        // Login form show
        Route::get('login', 'Webkul\Customer\Http\Controllers\SessionController@show')->defaults('_config', [
            'view' => 'shop::customers.session.index',
        ])->name('customer.session.index');

        // Login form store
        Route::post('login', 'Webkul\Customer\Http\Controllers\SessionController@create')->defaults('_config', [
            'redirect' => 'shop.home.products'
        ])->name('customer.session.create');

        // Registration Routes
        //registration form show
        Route::get('register', 'Webkul\Customer\Http\Controllers\RegistrationController@show')->defaults('_config', [
            'view' => 'shop::customers.signup.index'
        ])->name('customer.register.index');

        Route::get('signup-success', 'Webkul\Customer\Http\Controllers\RegistrationController@signupSuccess')->defaults('_config', [
            'view' => 'shop::customers.signup.signup-success'
        ])->name('customer.register.success');


        Route::get('get-city-list', 'Webkul\Customer\Http\Controllers\RegistrationController@GetCityList');

        Route::get('set-user-password/{token}', 'Webkul\Customer\Http\Controllers\RegistrationController@SetUserPassword')->defaults('_config', [
            'view' => 'shop::customers.signup.setpassword'
        ])->name('customer.register.setpassword');

        Route::post('set-user-password/{token}','Webkul\Customer\Http\Controllers\RegistrationController@StoreUserPassword')->defaults('_config', [
            'redirect' => 'customer.session.index',
        ])->name('customer.register.storepassword');


        //registration form store
        Route::post('register', 'Webkul\Customer\Http\Controllers\RegistrationController@create')->defaults('_config', [
            'redirect' => 'customer.session.index',
        ])->name('customer.register.create');
		
		//verify otp
        Route::get('/check-otp/{customer_id}', 'Webkul\Customer\Http\Controllers\RegistrationController@checkOtp')->name('check.otp');

        Route::post('verify-otp', 'Webkul\Customer\Http\Controllers\RegistrationController@verifyOtp')->name('verify.otp');
        
        Route::get('change-otp/{id}', 'Webkul\Customer\Http\Controllers\RegistrationController@changeOtp')->name('change.otp');
        
        //verify account
        Route::get('/verify-account/{id}', 'Webkul\Customer\Http\Controllers\RegistrationController@verifyAccount')->name('customer.verify');

        //resend verification email
        Route::get('/resend/verification/{email}', 'Webkul\Customer\Http\Controllers\RegistrationController@resendVerificationEmail')->name('customer.resend.verification-email');

        // Auth Routes
        Route::group(['middleware' => ['customer']], function () {

            //Customer logout
            Route::get('logout', 'Webkul\Customer\Http\Controllers\SessionController@destroy')->defaults('_config', [
                'redirect' => 'customer.session.index'
            ])->name('customer.session.destroy');

            //Customer Wishlist add
            Route::get('wishlist/add/{id}', 'Webkul\Customer\Http\Controllers\WishlistController@add')->name('customer.wishlist.add');

            //Customer Wishlist remove
            Route::get('wishlist/remove/{id}', 'Webkul\Customer\Http\Controllers\WishlistController@remove')->name('customer.wishlist.remove');

            //Customer Wishlist remove
            Route::get('wishlist/removeall', 'Webkul\Customer\Http\Controllers\WishlistController@removeAll')->name('customer.wishlist.removeall');

            //Customer Wishlist move to cart
            Route::get('wishlist/move/{id}', 'Webkul\Customer\Http\Controllers\WishlistController@move')->name('customer.wishlist.move');

            //customer account
            Route::prefix('account')->group(function () {
                //Customer Dashboard Route
                Route::get('index', 'Webkul\Customer\Http\Controllers\AccountController@index')->defaults('_config', [
                    'view' => 'shop::customers.account.index'
                ])->name('customer.account.index');

                //Customer Profile Show
                Route::get('profile', 'Webkul\Customer\Http\Controllers\CustomerController@index')->defaults('_config', [
                'view' => 'shop::customers.account.profile.index'
                ])->name('customer.profile.index');

                //Customer Profile Edit Form Show
                Route::get('profile/edit', 'Webkul\Customer\Http\Controllers\CustomerController@editIndex')->defaults('_config', [
                    'view' => 'shop::customers.account.profile.edit'
                ])->name('customer.profile.edit');

                //Customer Profile Edit Form Store
                Route::post('profile/edit', 'Webkul\Customer\Http\Controllers\CustomerController@edit')->defaults('_config', [
                    'view' => 'shop::customers.account.profile.edit'
                ])->name('customer.profile.edit');
                /*  Profile Routes Ends Here  */

                /*    Routes for Addresses   */
                //Customer Address Show
                Route::get('addresses', 'Webkul\Customer\Http\Controllers\AddressController@index')->defaults('_config', [
                    'view' => 'shop::customers.account.address.index'
                ])->name('customer.address.index');

                //Customer Address Create Form Show
                Route::get('addresses/create', 'Webkul\Customer\Http\Controllers\AddressController@create')->defaults('_config', [
                    'view' => 'shop::customers.account.address.create'
                ])->name('customer.address.create');

                Route::get('addresses/change', 'Webkul\Customer\Http\Controllers\AddressController@change')->defaults('_config', [
                    'view' => 'shop::customers.account.address.change'
                ])->name('customer.address.change');

                Route::post('addresses/change', 'Webkul\Customer\Http\Controllers\AddressController@changedAddress')->defaults('_config', [
                    'redirect' => 'shop.checkout.onepage.index'
                ])->name('customer.address.changedAddress');

                //Customer Address Create Form Store
                Route::post('addresses/create', 'Webkul\Customer\Http\Controllers\AddressController@store')->defaults('_config', [
                    'view' => 'shop::customers.account.address.address',
                    'redirect' => 'customer.address.index'
                ])->name('customer.address.store');

                //Customer Address Edit Form Show
                Route::get('addresses/edit/{id}', 'Webkul\Customer\Http\Controllers\AddressController@edit')->defaults('_config', [
                    'view' => 'shop::customers.account.address.edit'
                ])->name('customer.address.edit');

                //Customer Address Edit Form Store
                Route::put('addresses/edit/{id}', 'Webkul\Customer\Http\Controllers\AddressController@update')->defaults('_config', [
                    'redirect' => 'customer.address.index'
                ])->name('customer.address.update');

                //Customer Address Make Default
                Route::get('addresses/default/{id}', 'Webkul\Customer\Http\Controllers\AddressController@makeDefault')->name('make.default.address');

                //Customer Address Delete
                Route::get('addresses/delete/{id}', 'Webkul\Customer\Http\Controllers\AddressController@destroy')->name('address.delete');


                //Customer credit frontend
                Route::get('make-payments','Webkul\Customer\Http\Controllers\CreditPaymentsController@pay')->defaults('_config', [
                    'view' => 'shop::customers.account.credit_payments.pay'
                ])->name('customer.credit_payment.pay');

                Route::post('make-payments', 'Webkul\Customer\Http\Controllers\CreditPaymentsController@PayViaCheque')->defaults('_config', [
                    'redirect' => 'customer.credit_payment.pay'
                ])->name('customer.credit_payment.payviacheque');

                Route::post('/make-payments/razorpay', 'Webkul\Customer\Http\Controllers\CreditPaymentsController@PayViaRazorpay')->name('customer.credit_payment.payviarazorpay');

                
                /* Wishlist route */
                //Customer wishlist(listing)
                Route::get('wishlist', 'Webkul\Customer\Http\Controllers\WishlistController@index')->defaults('_config', [
                    'view' => 'shop::customers.account.wishlist.wishlist'
                ])->name('customer.wishlist.index');

                Route::get('my-offers','Webkul\Customer\Http\Controllers\MyOffersController@index')->defaults('_config', [
                    'view' => 'shop::customers.account.my_offers.index'
                ])->name('customer.myoffers');

                //Failure Implants

                Route::get('return-implants','Webkul\Customer\Http\Controllers\FailureImplantsController@index')->defaults('_config', [
                    'view' => 'shop::customers.account.failure_implants.index'
                ])->name('failure.implants.index');

                Route::get('return-implants/create','Webkul\Customer\Http\Controllers\FailureImplantsController@create')->defaults('_config', [
                    'view' => 'shop::customers.account.failure_implants.create'
                ])->name('failure.implants.create');

                Route::post('return-implants/store', 'Webkul\Customer\Http\Controllers\FailureImplantsController@store')->name('failure.implants.store');
                
                Route::get('return-implants/edit/{id}','Webkul\Customer\Http\Controllers\FailureImplantsController@edit')->defaults('_config', [
                    'view' => 'shop::customers.account.failure_implants.edit'
                ])->name('failure.implants.edit');

                Route::post('return-implants/update/{id}', 'Webkul\Customer\Http\Controllers\FailureImplantsController@update')->name('failure.implants.update');

                Route::get('return-implants/view/{id}','Webkul\Customer\Http\Controllers\FailureImplantsController@show')->defaults('_config', [
                    'view' => 'shop::customers.account.failure_implants.view'
                ])->name('failure.implants.view');                

                // Route::get('failure-implants/usedform','Webkul\Customer\Http\Controllers\FailureImplantsController@usedform')->defaults('_config', [
                //     'view' => 'shop::customers.account.failure_implants.usedform'
                // ])->name('failure.implants.usedform');

                // Route::get('failure-implants/usededit/{id}','Webkul\Customer\Http\Controllers\FailureImplantsController@edit')->defaults('_config', [
                //     'view' => 'shop::customers.account.failure_implants.usedformedit'
                // ])->name('failure.implants.usededit');

                // Route::POST('failed-implants/usedupdate/{id}', 'Webkul\Customer\Http\Controllers\FailureImplantsController@usedupdate')->name('failed-implants.usedupdate');

                // Route::post('failure-implants-send/usedform', ['as'=>'failure.implants.usedsend','uses'=>'Webkul\Customer\Http\Controllers\FailureImplantsController@usedstore']);


                // Route::get('failure-implants/unusedform','Webkul\Customer\Http\Controllers\FailureImplantsController@unusedform')->defaults('_config', [
                //     'view' => 'shop::customers.account.failure_implants.unusedform'
                // ])->name('failure.implants.unusedform');

                // Route::get('failure-implants/unusededit/{id}','Webkul\Customer\Http\Controllers\FailureImplantsController@edit')->defaults('_config', [
                //     'view' => 'shop::customers.account.failure_implants.unusedformedit'
                // ])->name('failure.implants.unusededit');

                // Route::POST('failed-implants/unusedupdate/{id}', 'Webkul\Customer\Http\Controllers\FailureImplantsController@unusedupdate')->name('failed-implants.unusedupdate');
                
                Route::get('return-implants/delete/{id}','Webkul\Customer\Http\Controllers\FailureImplantsController@destroy')->name('failure.implants.delete');                
                
                Route::get('return-implants/print/{id}', 'Webkul\Customer\Http\Controllers\FailureImplantsController@print')->name('failed-implants.print');
                
                Route::POST('return-implants/download', 'Webkul\Customer\Http\Controllers\FailureImplantsController@download')->name('failed-implants.download');
                
                /*Webinar*/

                Route::get('webinar', 'Webkul\Customer\Http\Controllers\WebinarController@webinar')->defaults('_config', [
                    'view' => 'shop::customers.account.webinar.webinar'
                ])->name('webinar.index');


                Route::get('webinar/zoomview/{meet}', 'Webkul\Customer\Http\Controllers\WebinarController@liveview')->defaults('_config', [
                    'view' => 'shop::customers.account.webinar.view'
                ])->name('webinar.zoomview');

                Route::get('webinar/zoomlive', function(){
                    return view('shop::customers.account.webinar.live');
                })->name('webinar.zoomview.live');

                Route::get('ondemandwebinar', 'Webkul\Customer\Http\Controllers\WebinarController@ondemand')->defaults('_config', [
                    'view' => 'shop::customers.account.webinar.ondemandwebinar'
                ])->name('webinar.ondemand');

                Route::get('webinar/leavemeating', function(){
                    return view('shop::customers.account.webinar.leavezoom');
                })->name('webinar.zoomview.leavemeating');

                /* Orders route */
                //Customer orders(listing)
                Route::get('orders', 'Webkul\Shop\Http\Controllers\OrderController@index')->defaults('_config', [
                    'view' => 'shop::customers.account.orders.index'
                ])->name('customer.orders.index');

                //Customer orders view summary and status
                Route::get('orders/view/{id}', 'Webkul\Shop\Http\Controllers\OrderController@view')->defaults('_config', [
                    'view' => 'shop::customers.account.orders.view'
                ])->name('customer.orders.view');

                //Prints invoice
                Route::get('orders/print/{id}', 'Webkul\Shop\Http\Controllers\OrderController@print')->defaults('_config', [
                    'view' => 'shop::customers.account.orders.print'
                ])->name('customer.orders.print');

                /* Reviews route */
                //Customer reviews
                Route::get('reviews', 'Webkul\Customer\Http\Controllers\CustomerController@reviews')->defaults('_config', [
                    'view' => 'shop::customers.account.reviews.index'
                ])->name('customer.reviews.index');

                //Customer review delete
                Route::get('reviews/delete/{id}', 'Webkul\Shop\Http\Controllers\ReviewController@destroy')->defaults('_config', [
                    'redirect' => 'customer.reviews.index'
                ])->name('customer.review.delete');

                 //Customer all review delete
                Route::get('reviews/all-delete', 'Webkul\Shop\Http\Controllers\ReviewController@deleteAll')->defaults('_config', [
                    'redirect' => 'customer.reviews.index'
                ])->name('customer.review.deleteall');
            });
        });
    });
    //customer routes end here
});

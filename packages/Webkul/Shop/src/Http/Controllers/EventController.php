<?php

namespace Webkul\Shop\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Webkul\Customer\Models\Countries;
use Mail;
use Webkul\Shop\Mail\EventRegisterMail;


/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class EventController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductRepository object
     *
     * @var array
     */

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Product\Repositories\ProductRepository $product
     * @return void
  

    /**
     * Display a listing of the resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = today()->format('Y-m-d');
        $upcoming_events = DB::table('events')->where('end_date', '>=', $date)
                                                ->where('is_deleted',0)
                                                ->get();
        $past_events = DB::table('events')->where('end_date', '<', $date)
                                                ->where('is_deleted',0)
                                                ->get();
        
        return view('shop::events.index')->with(['upcoming_events' => $upcoming_events,'past_events' => $past_events]);
    }

    public function show($slug)
    {
        $event = DB::table('events')->where('slug',$slug)->first();
        return view('shop::events.show',compact('event',$event));
    }

    public function EventRegister(Request $request)
    {
        //dd($request->all());
        $this->validate(request(), [
            'my_title' => 'required',
            'first_name' => 'string|required',
            'email' => 'email|required|unique:users,email',
            'phone_number' => 'required|numeric',
            'country_id' => 'required',
            'event_msg' => 'required'
            ]);

        $data=$request->all();
        $data['full_name'] = $data['first_name'].' '.$data['last_name'];
        $data['country_name'] = Countries::GetCountryName($data['country_id']);
          $admins = DB::table('users')->where('role_id',1)->where('status',1)->get();
          //dd($admins);
                foreach($admins as $key =>$admin)
                {
                    Mail::to($admin->email)->send(new EventRegisterMail($data['event_name'],$data['my_title'],$data['full_name'], $data['email'],$data['phone_number'],$data['country_name'],$data['event_msg']));               
                }



        session()->flash('success', 'Register form has been sent! we will get in touch with you soon..');

        return redirect()->route('shop.event.index');
    }

}

<?php

namespace Webkul\Shop\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;

/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class PagesController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductRepository object
     *
     * @var array
     */

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Product\Repositories\ProductRepository $product
     * @return void
  

    /**
     * Display a listing of the resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $page = DB::table('pages')->where('page_slug', $slug)->first();
        if(isset($page) && $page->id == 4) {
           if(auth()->guard('customer')->check() != null || auth()->guard('admin')->check() != null)
               return view('shop::pages.index', compact('page','page'));
           else
                return redirect()->route('customer.session.index');
        }
        return view('shop::pages.index', compact('page','page'));
    }
}

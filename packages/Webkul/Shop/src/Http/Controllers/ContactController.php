<?php

namespace Webkul\Shop\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Webkul\Customer\Models\Countries;
use Mail;
use Webkul\Shop\Mail\ContactMail;


/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ContactController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductRepository object
     *
     * @var array
     */

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Product\Repositories\ProductRepository $product
     * @return void
  

    /**
     * Display a listing of the resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$page = DB::table('pages')->where('page_slug', $slug)->first();

        return view('shop::contact-us.index');
    }
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate(request(), [
            'interested_in' => 'required',
            'my_title' => 'required',
            'first_name' => 'string|required',
            'email' => 'email|required',
            'phone_number' => 'required|numeric',
            'country_id' => 'required',
            'contact_msg' => 'required'
            ]);


        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $data = [
                'secret' => env('RECAPTCHA_SECRET'),
                'response' => $request->get('recaptcha'),
                'remoteip' => $remoteip
              ];
        $options = [
                'http' => [
                  'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                  'method' => 'POST',
                  'content' => http_build_query($data)
                ]
            ];
        $context = stream_context_create($options);
                $result = file_get_contents($url, false, $context);
                $resultJson = json_decode($result);
        if ($resultJson->success != true) {
            session()->flash('error', 'ReCaptcha Error');
                return back()->withErrors(['captcha' => 'ReCaptcha Error']);
                }
        if ($resultJson->score >= 0.3) {
       
            $data=$request->all();
            $data['full_name'] = $data['first_name'].' '.$data['last_name'];
            $data['country_name'] = Countries::GetCountryName($data['country_id']);
              $admins = DB::table('users')->where('role_id',1)->where('status',1)->get();
                    foreach($admins as $key =>$admin)
                    {
                        Mail::to($admin->email)->send(new ContactMail($data['interested_in'],$data['my_title'],$data['full_name'], $data['email'],$data['phone_number'],$data['country_name'],$data['contact_msg']));               
                    }


            session()->flash('success', 'Your message has been successfully sent. We will contact you very soon!');

            return redirect()->route('shop.contact.index');
        }
        else {
                session()->flash('error', 'ReCaptcha Error');
            return back()->withErrors(['captcha' => 'ReCaptcha Error']);
        }
    }
}

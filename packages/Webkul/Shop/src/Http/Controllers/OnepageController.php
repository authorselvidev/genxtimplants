<?php

namespace Webkul\Shop\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use Webkul\Checkout\Facades\Cart;
use Webkul\Shipping\Facades\Shipping;
use Webkul\Payment\Facades\Payment;
use Webkul\Checkout\Http\Requests\CustomerAddressForm;
use Webkul\Sales\Repositories\OrderRepository;
use Session;
use Illuminate\Support\Facades\Mail;
use Webkul\Admin\Mail\NewOrderNotification;
use Webkul\Product\Repositories\ProductRepository as Product;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Product\Helpers\ProductImage as ProductImage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Event;
use Razorpay\Api\Api;
use Illuminate\Support\Collection;
use DB;


/**
 * Chekout controller for the customer and guest for placing order
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class OnepageController extends Controller
{
    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $orderRepository;

    protected $category;

    protected $product;
    protected $productImage;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Attribute\Repositories\OrderRepository  $orderRepository
     * @return void
     */
    public function __construct(OrderRepository $orderRepository, Category $category, Product $product, ProductImage $productImage)
    {
        $this->orderRepository = $orderRepository;

        $this->category = $category;

        $this->product = $product;
        $this->productImage = $productImage;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        if (Cart::hasError())
            return redirect()->route('shop.checkout.cart.index');

        $cart = Cart::getCart();

        //points will be experied after 12 months
        DB::table('reward_points')->where('user_id',$user->id)
                                   ->where('points_credited',1)
                                   ->where('updated_date','<=', Carbon::now()->subDays(365))
                                   ->update(['points_expired' => 1]);

        $allRewardPoints = DB::table('reward_points')
                            ->where('user_id',$user->id)
                            ->where('points_credited',1)
                            ->where('points_expired',0)
                            ->sum('reward_points');
        
        $usedRewardPoints =  DB::table('orders')
                            ->where('customer_id',$user->id)
                            ->sum('reward_points_used');

        $availableRewardPoints=0;
        if($allRewardPoints > 0)
            $availableRewardPoints = number_format(abs($allRewardPoints - $usedRewardPoints),0);

        $paylaterwithcredit = DB::table('core_config')->where('code', 'sales.paymentmethods.paylaterwithcredit.active')->first();

        session()->forget('membership_details');
        session()->forget('promocode_details');
        session()->forget('credit_discount_details');
        session()->forget('reward_details');
        session()->forget('promotion_details');

        $request = new \Illuminate\Http\Request();
        $request->setMethod('POST');

        if($cart && $cart->is_active && $cart->discount_type!=0){
            $discount_code = ($cart->discount_id!=0)? (DB::table('discounts as d')->where('id', $cart->discount_id)->addSelect('coupon_code')->value('coupon_code')):0;
            $request = new \Illuminate\Http\Request();
            $request->setMethod('POST');
            if($cart->discount_type == 2 && !session()->has('membership_details')){
                $request->request->add([
                    'discountType' => 2,
                    'discountId' => $cart->discount_id,
                    'discountPer'=> $cart->discount_percentage,

                    'couponCode' => $discount_code
                ]);
                $this->ApplyMemberShipDiscount($request);
            }else if($cart->discount_type == 3 && !session()->has('promocode_details')){
                
                $promo_codes = DB::table('promo_codes')->where('discount_id', $cart->discount_id)->addSelect('apply_membership', 'earn_points')->first();

                $request->request->add([
                    'discountType' => 3,
                    'discountId' => $cart->discount_id,
                    'discountPer'=> $cart->discount_percentage, 

                    'couponCode' => $discount_code,
                    'applyMembership' => $promo_codes->apply_membership,
                    'earnPoints' => $promo_codes->earn_points, 
                ]);

                $this->ApplyPromoCodeDiscount($request);
            }else if($cart->discount_type == 6 && !session()->has('reward_details')){
                //dd($cart->reward_discount);

                $request->request->add([
                    'rewardPoints' => $cart->reward_points_applied
                ]);
                $this->ApplyReward($request);
            }else if($cart->discount_type == 5 && !session()->has('credit_discount_details')){
                //dd($cart->reward_discount);

                $promo_codes = DB::table('promo_codes')->where('discount_id', $cart->discount_id)->addSelect('apply_membership', 'earn_points')->first();

                $request->request->add([
                    'discountType' => 5,
                    'discountId' => $cart->discount_id,
                    'discountPer'=> $cart->discount_percentage, 

                    'couponCode' => $discount_code, 
                    'applyMembership' => $promo_codes->apply_membership, 
                    'earnPoints' => $promo_codes->earn_points,
                ]);
                $this->ApplyCreditDiscount($request);
            }
        }

        return view($this->_config['view'], [
            'paylaterwithcredit' => $paylaterwithcredit,
            'cart' => Cart::getCart(),
            'availableRewardPoints' => $availableRewardPoints]);
    }

    /**
     * Saves customer address.
     *
     * @param  \Webkul\Checkout\Http\Requests\CustomerAddressForm $request
     * @return \Illuminate\Http\Response
    */
    public function saveAddress(CustomerAddressForm $request)
    {
        if (Cart::hasError() || !Cart::saveCustomerAddress(request()->all()) || !$rates = Shipping::collectRates())
            return response()->json(['redirect_url' => route('shop.checkout.cart.index')], 403);

        Cart::collectTotals();

        return response()->json($rates);
    }

    /**
     * Saves shipping method.
     *
     * @return \Illuminate\Http\Response
    */
    public function saveShipping()
    {
        $shippingMethod = request()->get('shipping_method');

        if (Cart::hasError() || !$shippingMethod || !Cart::saveShippingMethod($shippingMethod))
            return response()->json(['redirect_url' => route('shop.checkout.cart.index')], 403);

        Cart::collectTotals();

        return response()->json(Payment::getSupportedPaymentMethods());
    }

    /**
     * Saves payment method.
     *
     * @return \Illuminate\Http\Response
    */
    public function savePayment()
    {
        $payment = request()->get('payment');

        if (Cart::hasError() || !$payment || !Cart::savePaymentMethod($payment))
            return response()->json(['redirect_url' => route('shop.checkout.cart.index')], 403);

        $cart = Cart::getCart();

        return response()->json([
                'jump_to_section' => 'review',
                'html' => view('shop::checkout.onepage.review', compact('cart'))->render()
            ]);
    }


    public function TriggerMail()
    {
        $order = session('order');
        $is_mailed = DB::table('orders')->where('id', $order->id)->first()->is_mailed;
        if($is_mailed == 0){
            DB::table('orders')->where('id', $order->id)->update(['is_mailed'=>1]);
            Session::put('order_email','email_sent');
            Event::fire('checkout.order.save.after', $order);
        }
    }

    /**
     * Saves order.
     *
     * @return \Illuminate\Http\Response
    */
    public function saveOrder()
    {
        $this->validate(request(), [
            'payment' => 'required',
            'delivery_address' => 'required',
            'billing_address' => 'required'
        ]);
        if (Cart::hasError())
            return redirect()->route('shop.checkout.cart.index');

        Cart::collectTotals();

        $cart = Cart::getCart();

        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        $user_credit = $user->credit_limit - ($user->credit_used - $user->credit_paid) + $user->credit_balance;

        if(request('payment')=='paylaterwithcredit' && (request('grand_total') > $user_credit)){
            session()->flash('error', "You don't have enough credit balance");
            return redirect()->route('shop.checkout.cart.index');
        }

        //$this->validateOrder();
        //$this->saveShipping();
        $this->savePayment();
        //dd($redirectUrl);
        

        /*if ($redirectUrl = Payment::getRedirectUrl($cart)) {
            return $redirectUrl;
        }*/
        //dd(request()->all());
        Session::put('cart_details',request()->all());
        if(request()->get('payment') == 'paypal')
        {
            return redirect()->route('paypal.standard.redirect');
        }

        //dd(request()->all());
        $order = $this->orderRepository->create(Cart::prepareDataForOrder());
        
        //$mail_sent = Mail::send(new NewOrderNotification($order));
        if($order){
            
            $dealer_id = DB::table('users')->where('id',$order->customer_id)->value('dealer_id');

            
            if(isset($dealer_id) && $dealer_id !=0){
                $comm_categories = DB::table('dealer_commissions')
                                        ->where('dealer_id', $dealer_id)
                                        ->pluck('category_id')->toArray();

                    
                        foreach ($cart->items as $key => $item) {
                            $product_price=0;
                                $product_price = DB::table('cart_items')->where('cart_id', Cart::getCart()->id)
                                                    ->where('product_id', $item->product_id)
                                                    ->select(DB::raw('(sum(total)-sum(discount_applied_amount)) AS total_price'))
                                                    ->value('total_price');

                                $each_prod_categories = DB::table('product_categories')->where('product_id',$item->product_id)->pluck('category_id')->toArray();
                                $comm_percent=$comm_amount="0";
                                $discount_id = 0;

                                if($item->discount_applied == 1){
                                    if($order->promocode_discount_id != 0) {
                                        $discount_id = $order->promocode_discount_id;
                                    }
                                    if($order->membership_discount_id != 0) {
                                        $discount_id = $order->membership_discount_id;
                                    }
                                    if($order->credit_discount_id != 0) {
                                        $discount_id = $order->credit_discount_id;
                                    }

                                    
                                    $comm_percent=DB::table('discounts')->where('id',$discount_id)->value('dealer_commission');
                                }else{

                                    $containsAllValues = false;

                                    foreach($each_prod_categories as $cat){
                                        if(in_array($cat,  $comm_categories)){
                                            $containsAllValues = true;
                                            break;
                                        }
                                    }


                                    if($containsAllValues){
                                        $comm_percent = DB::table('dealer_commissions as dc')
                                                    ->where('dealer_id', $dealer_id)
                                                    ->whereIn('dc.category_id',$each_prod_categories)
                                                    ->groupBy('dc.category_id')
                                                    ->orderBy('dc.category_id','DESC')
                                                    ->value('dc.commission_percent');


                                    }
                                }
                                $comm_percent  = empty($comm_percent)? 0 :$comm_percent;

                                $comm_amount = round(($product_price * $comm_percent) / 100,2);

                                DB::table('order_items')->where('order_id', $order->id)
                                                    ->where('product_id', $item->product_id)
                                                    ->update([
                                                        'dealer_comm_percent'=>$comm_percent,
                                                        'dealer_comm_amount'=>$comm_amount,
                                                    ]);
                           }
                        
                        

                    $total_comm_amount = DB::table('order_items')->where('order_id', $order->id)->sum('dealer_comm_amount');
                        DB::table('orders')->where('id', $order->id)
                                            ->update(['total_comm_amount'=>$total_comm_amount]);
                
            }


            Cart::deActivateCart();
            Session::put('order',$order);
            $otp_data = array();
            $order_id = 'Genxt#' . $order->id;
            $message = 'Your order has been placed successfully, your orderID:' .$order_id;
            $get_user = DB::table('users')->where('id',$order->customer_id)->first();
            $otp_data['sms_numbers'] = '91'.$get_user->phone_number;
            $otp_data['sms_message'] = $message;
            $sent_sms=view('shop::send-sms',$otp_data)->render();
            //dd($sent_sms);
            //session()->flash('order', 'Your Order has been placed!');
            session()->forget('cart_details');
            session()->forget('membership_details');
            session()->forget('promocode_details');
            session()->forget('reward_details');
            session()->forget('promotion_details');
            session()->forget('credit_discount_details');
            Session::save();
            //dd($order);
            return redirect()->route('shop.checkout.success')->with('order_id',$order->id);
        }
        
        return redirect()->back();
    

    }

    /**
     * Order success page
     *
     * @return \Illuminate\Http\Response
    */
    public function success()
    {
        //dd(session('order'));
        if (! $order = session('order'))
            return redirect()->route('shop.checkout.success')->with('order_id',$order->id);

        return view($this->_config['view'], compact('order'));
    }

    /**
     * Validate order before creation
     *
     * @return mixed
     */
    public function validateOrder()
    {
        $cart = Cart::getCart();
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
        $shipping_address = DB::table('user_addresses')
                                ->where('customer_id',$user->id)
                                ->where('billing_selected',1)
                                ->orwhere('default_address',1)->latest()->first();
        $delivery_address = DB::table('user_addresses')
                                ->where('customer_id',$user->id)
                                ->where('delivery_selected',1)
                                ->orwhere('default_address',1)->latest()->first();
        //dd($shipping_address);

        if (! $shipping_address) {
            throw new \Exception(trans('Please check shipping address.'));
        }

        if (! $delivery_address) {
            throw new \Exception(trans('Please check billing address.'));
        }

        /*if (! $cart->selected_shipping_rate) {
            throw new \Exception(trans('Please specify shipping method.'));
        }

        if (! $cart->payment) {
            throw new \Exception(trans('Please specify payment method.'));
        }*/
    }


    public static function GetDiscountProductIds()
    {
        $cart = cart()->getCart();

        $prodCategories=[];
        foreach ($cart->items as $key => $item) {
            $prodCategories[$item->product_id] = DB::table('product_categories')->where('product_id',$item->product_id)->pluck('category_id')->toArray();
        }
       //dd($prodCategories);
       //dd(count($prodCategories));
      /*  $product_ids=$final_data=[];
        $data=array();
        foreach ($prodCategories as $key => $item) { 
             foreach ($item AS $key2 => $item_c) {
            $data[$item_c][]=$key;
        }
    }
    $final_data=array();
    foreach ($data as $key => $final_d) {
        if (count($prodCategories) == 1){
            array_push($final_data, $final_d);
        }
        else if (count($final_d)>1) {
            array_push($final_data, $final_d);
        //}
    }
    $same_product_ids = array_unique($final_data,SORT_REGULAR);*/
  //dd($data,$prodCategories,$final_data);


    $product_ids=$data=[];
    
        foreach ($prodCategories as $key => $item) { 
            $keys = array_keys($prodCategories, $item);
            if (count($keys) > 1) {
                $product_ids[] = $keys;
            }
            else{
                $product_ids[] = $keys;
                foreach ($item AS $key2 => $item_c) {
                    $data[$item_c][]=$key;
                }
            }
        }
        if(count($data) > 0){       
             $final_data=array();
                foreach ($data as $key => $final_d) {
                    if (count($final_d)>1) {
                        array_push($product_ids, $final_d);
                    }
                //$product_ids[] = $keys;
                }   
            }
        $same_product_ids = array_unique($product_ids,SORT_REGULAR);    
        
        return $same_product_ids;
    }
    
    public static function ListAllCoupons($discount_type, $customer_id=null)
    {
        $offers=$list_offers=array();
        $user = $cart = null;

        if($customer_id){
            $user = DB::table('users')->where('id', $customer_id)->first();
            $cart = cart()->getCart(['customer_id'=>$user->id]);

        }else{
            $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
            if(empty($user))
                $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
            
            $cart = cart()->getCart();
        }

        
        $customer_id = $user->id;        

        $get_customer_group_id = \Webkul\Customer\Models\Customer::customerGroupId($user->id);

        //dd($get_customer_group_id);
        
   
        $apply_btn = 'apply-promocode';
        $users_table = DB::table('users')->where('id',$user->id)->first();
        $role_id = $users_table->role_id;
        $list_offers_unique=array();

        

        $prodCategories=$cartProductIds=[];

        foreach ($cart->items as $key => $item){
            $prodCategories[] = DB::table('product_categories')->where('product_id',$item->product_id)->pluck('category_id')->toArray();
            $cartProductIds[] = $item->product_id;
        }

        $prod_categories = array_merge(...$prodCategories);

        $select_offers = DB::table('discounts as d')
                                ->where('d.active',1)
                                ->where('d.is_deleted',0)
                                ->where('d.discount_type', $discount_type)
                                ->leftjoin('users as u','u.customer_group_id','d.customer_group_id')
                                ->leftjoin('discount_users as du','du.discount_id','d.id')
                                ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                ->leftjoin('promo_codes as pc','pc.discount_id','d.id');


            //$available_discount = DB::table('discounts as d')->where('is_deleted',0)->where('active',1)->where('discount_type', 2)->where('customer_group_id', $get_customer_group_id)->first()->id;

            //dd($available_discount);
                                
            if($discount_type == 3)
            {
                $select_offers->whereIn('pc.for_whom',[0,$role_id]);
            }

            if($discount_type == 2)
            {
                //$total_orders_amount = DB::table('user_groups')->where('id', $get_customer_group_id)->value('order_amount_from');
                //$total_orders_amount = DB::table('orders')->where('customer_id',$user->id)->where('status','completed')->sum('grand_total');
                //dd($total_orders_amount);

                $select_offers->where('u.id',$user->id)
                                ->leftjoin('user_groups as ug','ug.id','d.customer_group_id')
                                /*->where('ug.order_amount_from', '<=',$total_orders_amount)
                                ->where('ug.order_amount_to','>=', $total_orders_amount)
                                ->where('d.minimum_order_amount','<=', $price_total) */
                                ->where('d.customer_group_id', $get_customer_group_id)
                                ->where('d.for_all_users',1)
                                ->orwhere(function($query) use($user) {
                                    $query->where('d.for_all_users',0);
                                    $query->where('du.user_id',$user->id);
                                    $query->where('d.discount_type',2);
                                });
                                
                                
               
                $apply_btn = 'apply-membership';
            }

            if($discount_type == 5){
                $apply_btn = 'apply-creditdiscount';
                $user_discount = DB::table('discount_users')->where('user_id', $user->id)->pluck('discount_id');
                $alluser_discount = DB::table('discounts')->where('discount_type', $discount_type)->where('for_all_users', 1)->pluck('id');

                $select_offers->whereIn('d.id', $user_discount->merge($alluser_discount))
                                            ->whereIn('pc.for_whom',[0,$role_id])
                                            ->where(function($query){
                                                $query->where(function($query2){
                                                    $query2->whereDate('date_start','<=',date('Y-m-d'))
                                                           ->whereDate('date_end','>=',date('Y-m-d'));
                                                })->orWhere(function($query2){
                                                    $query2->whereNull('date_start')->whereNull('date_end');
                                                })->orWhere(function($query2){
                                                    $query2->whereDate('date_start','<=',date('Y-m-d'))
                                                           ->whereNull('date_end');
                                                })->orWhere(function($query2){
                                                    $query2->whereNull('date_start')
                                                           ->whereDate('date_end','>=',date('Y-m-d'));
                                                });
                                            });
            }

            $discount_categories = $select_offers->pluck('category_id');
            
            //dd($discount_categories);

            $temp_offers = $select_offers->get();


            foreach ($discount_categories as $key => $discount_category) {
                if(!in_array($discount_category, $prod_categories)) {
                    unset($discount_categories[$key]);
                    $temp_offers->forget($key);
                }
            }


            $list_offers= new Collection();
            $collection = collect($temp_offers);
            $unique_coupons = $collection->sortByDesc('discount_id')->unique('coupon_code');


            foreach ($unique_coupons as $key2 => $unique_coupon) {
                $each_discount_categories = DB::table('discounts as d')
                                ->where('d.id', $unique_coupon->discount_id)
                                ->orwhere('d.coupon_code', $unique_coupon->coupon_code)
                                ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                ->pluck('dc.category_id')->toArray();



                $discount_products=[];$min_order_amount=0;
                
                foreach ($cart->items as $key => $item) {
                    $prodCategories = DB::table('product_categories')->where('product_id', $item->product_id)->pluck('category_id')->toArray();
                    foreach($prodCategories as $cat){
                        if(in_array($cat , $each_discount_categories)) {
                            $discount_products[] = $item->product_id;
                        }
                    }

                    // $each_prod_categories = array_diff($prodCategories, $each_discount_categories);
                    // array_push($ary, array_diff($prodCategories, $each_discount_categories));                    
                    // if(count($each_prod_categories) == 0){
                    //     $discount_products[] = $item->product_id;
                    // }
                }

                $discount_products = array_unique($discount_products);

                $cart_total_amount = DB::table('cart_items')->where('cart_id', $cart->id)
                                            ->whereIn('product_id', $discount_products)
                                            ->select(DB::raw('(sum(total) - sum(discount_amount) + sum(tax_amount)) AS total_price'))
                                            ->value('total_price');

                if($unique_coupon->minimum_order_amount > $cart_total_amount){
                    $unique_coupons->forget($key2);
                }
            }
                 
            $list_offers = $unique_coupons;

            if($discount_type == 4)
            {
                $apply_btn = 'apply-promotion';
                $prod_quantity = $cart->items_qty; 
                $list_offers = DB::table('discounts as d')
                                ->where('d.is_deleted',0)
                                ->leftjoin('promotions as pm','pm.discount_id','d.id')
                                ->leftjoin('promotion_ordered_category as pc','pc.discount_id','d.id')
                                ->where('pm.min_order_qty','<=',$prod_quantity)
                                ->whereIn('pm.for_whom',[0,$role_id])
                                ->whereIn('pc.category_id',$prod_categories)
                                ->where('d.discount_type',4)->get();
            }    
           
            $list_offers_unique = $list_offers->unique('coupon_code');

            $coupons=array();
            foreach($list_offers_unique as $key => $list_offer)
            {
                if(isset($list_offer->coupon_code)) {
                $get_discount = DB::table('discounts')->where('coupon_code',$list_offer->coupon_code)->first();
                $discount_limit='';
                $check_avail=$free_prod_count='';
                //if($discount_type == 3){
                   
                    //dd($discount_used,$discount_limit);
                    //print_r($discount_limit);
                    if($discount_type == 3){
                        $discount_used = DB::table('orders')->where('promocode_discount_id',$get_discount->id)->where('customer_id',$user->id)->count('id');
                        $discount_limit = DB::table('discounts')->where('discount_type',$discount_type)->where('id',$get_discount->id)->where('discount_usage_limit','>',$discount_used)->first(); 
                        $check_avail = '<input type="hidden" class="earn-point"  name="earn_points" value="'.$list_offer->earn_points.'"><input type="hidden" class="apply-membershp" name="apply_membership" value="'.$list_offer->apply_membership.'">';
                    }

                    if($discount_type == 5){
                        $discount_used = DB::table('orders')->where('credit_discount_id',$get_discount->id)->where('customer_id',$user->id)->count('id');
                        $discount_limit = DB::table('discounts')->where('discount_type',$discount_type)->where('id',$get_discount->id)->where('discount_usage_limit','>',$discount_used)->first(); 
                        $check_avail = '<input type="hidden" class="earn-point"  name="earn_points" value="'.$list_offer->earn_points.'"><input type="hidden" class="apply-membershp" name="apply_membership" value="'.$list_offer->apply_membership.'">';
                    }
                    if($discount_type == 4)
                    $free_prod_count = '<li>You have '.$list_offer->free_products_count.' FREE Products<li>';

                //}

                $coupons[] = $list_offer->coupon_code;
                $apply_offer = '<button type="button" class="btn apply-offer '.$apply_btn.'">APPLY OFFER</button>';
                if($discount_type == 4)
                    $apply_offer = '<a href="'.route('shop.checkout.promotion.products',$get_discount->id).'" class="btn apply-offer '.$apply_btn.'">Select Your FREE Products</a>';
                if($list_offer->coupon_code && $get_discount->id)
                {
                   if(($discount_limit != null && $discount_type == 3) || $discount_type == 2 || $discount_type == 4 || $discount_type == 1 || ($discount_limit != null && $discount_type == 5))
                    {
                        $offer_discount = (isset($list_offer->discount)) ? $list_offer->discount : "";
                        $offers[] = '<div class="offer-section">
                        <input type="hidden" class="discount-id" name="discount_id" value="'.$get_discount->id.'">
                        <input type="hidden" class="discount-type" name="discount_type" value="'.$discount_type.'">

                        <input type="hidden" class="discount-per" name="discount_per" value="'.$offer_discount.'">
                        <input type="hidden" class="coupon-code" name="coupon_code" value="'.$list_offer->coupon_code.'">'.$check_avail.'
                        <ul class="offer">
                                <li class="offer-code">'.$list_offer->coupon_code.'</li>
                                <li class=title>'.$list_offer->discount_title.'</li>
                                <li>'.$list_offer->discount_description.'</li>'.$free_prod_count.'
                            </ul>
                           '.$apply_offer.'
                        </div>';
                   }
               
                }
            }
            }
           $all_coupons[] = $coupons;

           $allOffers = implode('',$offers);
           $response = array('all_coupons' => $all_coupons, 'allOffers' =>$allOffers);
           return $response;
    }

    public function ListPromotionProducts($discount_id, Request $request)
    {
        $all_categories = DB::table('promotion_free_product_category')->where('discount_id',$discount_id)->pluck('category_id')->toArray();
        return view($this->_config['view'],compact('all_categories','discount_id'));
    }

    public function ApplyMemberShipDiscount(Request $request)
    {
        if(empty($request->user_id)){
            $cart = cart()->getCart();
        }else{
            $cart = cart()->getCart(['customer_id'=>$request->user_id]);
        }

        $mem_discount_price=0;
        $membership_discount=array();
        $get_cartitems = DB::table('cart_items')->where('cart_id', $cart->id)->get();
        $data=array();
        if(!empty($request->user_id)){
            foreach ($get_cartitems as $key => $cartitem) {
                //$data['discount'] = '0';
                $data['quantity'] = $cartitem->quantity;
                Cart::updateItem($cartitem->product_id, $data, $cartitem->id);  
            }
        }

        $discount_categories = DB::table('discounts as d')
                                    ->where('d.id', $request->get('discountId'))
                                    ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                    ->pluck('dc.category_id')->toArray();


        foreach ($cart->items as $key => $item) {
            $product_price=0;

            $product_price = DB::table('cart_items')->where('cart_id', $cart->id)
                                                    ->where('product_id', $item->product_id)
                                                    ->select(DB::raw('sum(total) AS total_price'))
                                                    ->value('total_price');

            $each_prod_categories = DB::table('product_categories')->where('product_id',$item->product_id)->pluck('category_id')->toArray();

            $containsDiscount = false;

            foreach($each_prod_categories as $cat){
                if(in_array($cat,  $discount_categories)){
                    $containsDiscount = true;
                    break;
                }
            }

            $discount_percent=$discount_apply="0";

            if($containsDiscount){
                $discount_percent = DB::table('discounts as d')
                                        ->where('d.id', $request->get('discountId'))
                                        ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                        ->whereIn('dc.category_id',$each_prod_categories)
                                        ->groupBy('dc.category_id')
                                        ->orderBy('dc.category_id','DESC')
                                        ->value('dc.discount');

                $discount_apply = round(($product_price * $discount_percent) / 100,2);
                $discounted_subtotal = $product_price - $discount_apply;

                DB::table('cart_items')->where('cart_id', $cart->id)
                                       ->where('product_id', $item->product_id)
                                       ->update(['discount_applied'=>1,
                                                 'discount_id'=>$request->get('discountId'),
                                                 'discount_percentage'=>$discount_percent,
                                                 'discount_applied_amount'=>$discount_apply,
                                                 'discounted_subtotal'=>$discounted_subtotal]);
            }
        }

        $mem_discount_price = DB::table('cart_items')->where('cart_id', $cart->id)->where('discount_applied',1)->sum('discount_applied_amount');
        
        $discount_used = DB::table('discounts')->where('id', $request->get('discountId'))->increment('discount_usage_count',1);

        $response = [   'success' =>true,
                        'mem_discount_applied' => true,
                        'mem_discount_price' => $mem_discount_price,
                        'discount_per' => $request->get('discountPer'),
                        'membership_discount_id' =>$request->get('discountId'),
                        'discount_id' => $request->get('discountId'),
                        'coupon_code' => $request->get('couponCode')
                    ];

        $cart->discount_id = $request->get('discountId');
        $cart->discount_type = 2;
        $cart->discount_percentage = $request->get('discountPer');
        $cart->membership_discount = $mem_discount_price;
        $cart->save();

        if(empty($request->user_id)){
            Session::forget('membership_details');
            Session::forget('promocode_details');
            Session::forget('reward_details');
            Session::forget('promotion_details');
            Session::forget('credit_discount_details');
            Session::put('membership_details',$response);
        }

        $get_cartitem = DB::table('cart_items')->where('cart_id', $cart->id)->where('price','0.0000')->delete();
        return response()->json($response);
    }

    public function ApplyPromoCodeDiscount(Request $request)
    {
       //dd($request->all());
        if(empty($request->user_id)){
            $cart = cart()->getCart();
        }else{
            $cart = cart()->getCart(['customer_id'=>$request->user_id]);
        }

        $promo_code_price=0;
        $promocode_discount=array();
        $get_cartitems = DB::table('cart_items')->where('cart_id', $cart->id)->get();
        $data=array();
        if(!empty($request->user_id)){
            foreach ($get_cartitems as $key => $cartitem) {
                //$data['discount'] = '0';
                $data['quantity'] = $cartitem->quantity;
                Cart::updateItem($cartitem->product_id, $data, $cartitem->id);  
            }
        }
                    $discount_categories = DB::table('discounts as d')
                                ->where('d.id', $request->get('discountId'))
                                ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                ->pluck('dc.category_id')->toArray();
                                                      
                    foreach ($cart->items as $key => $item) {
                        $product_price=0;
                        $product_price = DB::table('cart_items')->where('cart_id', $cart->id)
                                            ->where('product_id', $item->product_id)
                                            ->select(DB::raw('sum(total) AS total_price'))
                                            ->value('total_price');
                         $each_prod_categories = DB::table('product_categories')->where('product_id',$item->product_id)->pluck('category_id')->toArray();
                        
                        $containsDiscount = false;
                        foreach($each_prod_categories as $cat){
                            if(in_array($cat,  $discount_categories)){
                                $containsDiscount = true;
                                break;
                            }
                        }
                        $discount_percent=$discount_apply="";
                        if($containsDiscount){
                            $discount_percent = DB::table('discounts as d')
                                                ->where('d.id', $request->get('discountId'))
                                                ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                                ->whereIn('dc.category_id',$each_prod_categories)
                                                ->groupBy('dc.category_id')
                                                ->orderBy('dc.category_id','DESC')
                                                ->value('dc.discount');
                      //var_dump($discount_percent); echo '<br>';
                        $discount_apply = round(($product_price * $discount_percent) / 100,2);
                        $discounted_subtotal = $product_price - $discount_apply;
                          DB::table('cart_items')->where('cart_id', $cart->id)
                                            ->where('product_id', $item->product_id)
                                            ->update([
                                                'discount_applied'=>1,
                                                'discount_id'=>$request->get('discountId'),
                                                'discount_percentage'=>$discount_percent,
                                                'discount_applied_amount'=>$discount_apply,
                                                'discounted_subtotal'=>$discounted_subtotal
                                            ]);
                                        }
                       
                }
         
          
        $promo_code_price = DB::table('cart_items')->where('cart_id', $cart->id)->where('discount_applied',1)->sum('discount_applied_amount');

        $discount_used = DB::table('discounts')->where('id', $request->get('discountId'))
                                                ->increment('discount_usage_count',1);
        
                $response = [   'success' =>true,
                                'promo_code_applied' => true,
                                'promo_code_price' => $promo_code_price,
                                'promo_code_discount_id' => $request->get('discountId'),
                                'discount_per' => $request->get('discountPer'),
                                'coupon_code' => $request->get('couponCode'),
                                'apply_membership' => $request->get('applyMembership'),
                                'earn_points' => $request->get('earnPoints')];

            $get_cartitem = DB::table('cart_items')->where('cart_id', $cart->id)->where('price','0.0000')->delete();            
            $cart->discount_type = 3;
            $cart->discount_id = $request->get('discountId');
            $cart->promocode_discount = $promo_code_price;
            $cart->save();

            if(empty($request->user_id)){
                Session::forget('membership_details');
                Session::forget('promocode_details');
                Session::forget('reward_details');
                Session::forget('promotion_details');
                Session::forget('credit_discount_details');
                Session::put('promocode_details',$response);
            }
            return response()->json($response);       
    }


    public function ApplyCreditDiscount(Request $request)
    {
        if(empty($request->user_id)){
            $cart = cart()->getCart();
        }else{
            $cart = cart()->getCart(['customer_id'=>$request->user_id]);
        }

        $credit_discount_price=0;
        $get_cartitems = DB::table('cart_items')->where('cart_id', $cart->id)->get();
        $data=array();

        if(!empty($request->user_id)){
            foreach ($get_cartitems as $key => $cartitem) {
                //$data['discount'] = '0';
                $data['quantity'] = $cartitem->quantity;
                Cart::updateItem($cartitem->product_id, $data, $cartitem->id);  
            }
        }


        $discount_categories = DB::table('discounts as d')
                                    ->where('d.id', $request->get('discountId'))
                                    ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                    ->pluck('dc.category_id')->toArray();

        foreach ($cart->items as $key => $item) {
            $product_price=0;

            $product_price = DB::table('cart_items')->where('cart_id', $cart->id)
                                            ->where('product_id', $item->product_id)
                                            ->select(DB::raw('sum(total) AS total_price'))
                                            ->value('total_price');

            $each_prod_categories = DB::table('product_categories')->where('product_id',$item->product_id)->pluck('category_id')->toArray();

            $containsDiscount = false;
            foreach($each_prod_categories as $cat){
                if(in_array($cat,  $discount_categories)){
                    $containsDiscount = true;
                    break;
                }
            }

            $discount_percent=$discount_apply="";

            if($containsDiscount){
                $discount_percent = DB::table('discounts as d')
                            ->where('d.id', $request->get('discountId'))
                            ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                            ->whereIn('dc.category_id',$each_prod_categories)
                            ->groupBy('dc.category_id')
                            ->orderBy('dc.category_id','DESC')
                            ->value('dc.discount');

                $discount_apply = round(($product_price * $discount_percent) / 100,2);
                $discounted_subtotal = $product_price - $discount_apply;

                DB::table('cart_items')->where('cart_id', $cart->id)
                                    ->where('product_id', $item->product_id)
                                    ->update([
                                        'discount_applied'=>1,
                                        'discount_id'=>$request->get('discountId'),
                                        'discount_percentage'=>$discount_percent,
                                        'discount_applied_amount'=>$discount_apply,
                                        'discounted_subtotal'=>$discounted_subtotal
                                    ]);
            }
        }
        
        $credit_discount_price = DB::table('cart_items')->where('cart_id', $cart->id)->where('discount_applied',1)->sum('discount_applied_amount');

        // $discount_used = DB::table('discounts')->where('id', $request->get('discountId'))
        //                                         ->increment('discount_usage_count',1);
        
        $response = [
            'success' =>true,
            'credit_discount_applied' => true,
            'credit_discount_price' => $credit_discount_price,
            'credit_discount_id' => $request->get('discountId'),
            'discount_per' => $request->get('discountPer'),
            'coupon_code' => $request->get('couponCode'),
            'apply_membership' => $request->get('applyMembership'),
            'earn_points' => $request->get('earnPoints')
        ];

        if(empty($request->user_id)){
            Session::forget('membership_details');
            Session::forget('promocode_details');
            Session::forget('reward_details');
            Session::forget('promotion_details');
            Session::forget('credit_discount_details');
            Session::put('credit_discount_details',$response);
        }
        
        $get_cartitem = DB::table('cart_items')->where('cart_id', $cart->id)->where('price','0.0000')->delete();
        $cart->discount_type = 5;
        $cart->discount_id = $request->get('discountId');
        $cart->credit_discount = $credit_discount_price;
        $cart->save();
        
        return response()->json($response);       
    }

    public function ApplyReward(Request $request)
    {
        if(empty($request->user_id)){
            $cart = cart()->getCart();
            $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
            if(empty($user))
                    $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
        }else{
            $cart = cart()->getCart(['customer_id'=>$request->user_id]);
            $user = DB::table('users')->where('id', $request->user_id)->first();
        }

        $reward_points = $request->get('rewardPoints');

        DB::table('cart')->where('id', $cart->id)->update(['reward_points_applied'=> $reward_points]);
        
        $allRewardPoints = DB::table('reward_points')
                            ->where('user_id',$user->id)
                            ->where('points_credited',1)
                            ->where('points_expired',0)
                            ->sum('reward_points');
       
        $usedRewardPoints =  DB::table('orders')
                            ->where('customer_id',$user->id)
                            ->sum('reward_points_used');

        $availableRewardPoints=0;
        if($allRewardPoints > 0)
            $availableRewardPoints = abs($allRewardPoints - $usedRewardPoints);

        $get_point = DB::table('points_and_commission_history')
                                ->where('type',1)
                                ->where('status',1)
                                ->first();

        $coin_value=$coin_val='';
        $message = false;
        
        if($reward_points <= $availableRewardPoints)
        {
           // dd($reward_points,$get_point->value);
            $coin_value = $reward_points / $get_point->value;
            $message = true;
            $get_cartitems = DB::table('cart_items')->where('cart_id', $cart->id)->get();
            $data=array();
            
            if(!empty($request->user_id)){
                foreach ($get_cartitems as $key => $cartitem) {
                    //$data['discount'] = '0';
                    $data['quantity'] = $cartitem->quantity;
                    Cart::updateItem($cartitem->product_id, $data, $cartitem->id);  
                }
            }
                
        }
        
        $remainingAvailPoints=0;
        if($allRewardPoints > 0)
            $remainingAvailPoints = abs($availableRewardPoints - $reward_points);
        
        $response = [   'success' => $message,
                        'reward_point_applied' => true,
                        'reward_points' => $reward_points,
                        'remaining_avail_points' => $remainingAvailPoints,
                        'coin_value' => $coin_value
                        ];
        if($message == true){

            if(empty($request->user_id)){
                Session::forget('membership_details');
                Session::forget('promocode_details');
                Session::forget('reward_details');
                Session::forget('promotion_details');
                Session::forget('credit_discount_details');
                Session::put('reward_details',$response);
            }

            $get_cartitem = DB::table('cart_items')->where('cart_id', $cart->id)->where('price','0.0000')->delete();
            $cart->discount_type = 6;
            $cart->reward_discount = $coin_value;
            $cart->save();
        }
        
        return response()->json($response);
    }

    public function ApplyPromotion(Request $request){
        //dd($request->all());
        $discount_id = $request->discountId;
        $total_qty_selected = $request->free_product_qty;
        $discount_detail = DB::table('discounts')->where('id',$discount_id)->first();
        $promotion_detail = DB::table('promotions')->where('discount_id',$discount_id)->first();
        $get_cartitems = DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->get();

        $data=array();
        foreach ($get_cartitems as $key => $cartitem) {
            $data['discount'] = '0';
            $data['quantity'] = $cartitem->quantity;
            Cart::updateItem($cartitem->product_id, $data, $cartitem->id);  
        }

        $response = [   'success' =>true,
                        'promotion_applied' => true,
                        'promootion_discount_id' => $request->get('discountId'),
                        'coupon_code' => $discount_detail->coupon_code,
                        'earn_points' => $promotion_detail->earn_points,
                        'total_qty_selected' => $total_qty_selected];
            
            $cart->discount_id = $request->get('discountId');
            
            
            $cart->discount_type = 4;
            $cart->promotion_applied = 1;
            $cart->save();
            
            Session::forget('membership_details');
            Session::forget('promocode_details');
            Session::forget('reward_details');
            Session::forget('promotion_details');
            Session::forget('credit_discount_details');        

            Session::put('promotion_details',$response);

        return response()->json(['success' => true, 'redirect' => url('/checkout/onepage') ]);

    }

    public function GetPromotionProducts($category_id,Request $request)
    {
         $sub_category =  DB::table('categories')->where('id',$category_id)->where('parent_id', '!=', null)->first();
         $response="";

         if(isset($sub_category)){
                     $all_products = $this->product->findAllByCategory($category_id);
                  $response = '<div class="rows">';
                  $response.='<div class="product-grid-3 col-md-12">';

                foreach ($all_products as $productFlat) {

                $product = $productFlat->product;
                $product_info = $productFlat->length_label;
        
        $productBaseImage = $this->productImage->getProductBaseImage($product); 
        if($product->haveSufficientQuantity(1)) {
            $response.='<div class="product-card col-md-3 col-sm-4 col-xs-6">
                <div class="product-image text-center col-xs-12 col-md-12 col-sm-12">
                    <img src="'.$productBaseImage['medium_image_url'].'" />
                </div>
                <div class="product-information col-xs-12 col-md-12 col-sm-12">
                    <div class="product-name">
                        <span>
                            Item #: '.$product->name.'
                        </span>
                    </div>
                    <div class="product-size">
                        <span>
                           <label>Length: </label>'. $product_info .'
                        </span>
                    </div>
                    <input type="hidden" name="discount_id" value="'.$request->discount_id.'">
                    <div class="add-qty col-md-12">
                        <button type="button" class="col-md-2 qty-minus choose-qty btn pull-left remove_cart btn-sm btn-primary"><i class="fa fa-minus"></i></button>
                        <input type="text" placeholder="Quantity..." name="free_product_qty['.$product->id.']" product_id ="'.$product->id.'" class="col-md-8 product-quantity">
                        <button type="button" class="col-md-2 qty-plus choose-qty btn pull-right btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>';
        }
       }
        $response.='</div></div>';
    }

        return $response;

    }

    public function RemoveCoupon(Request $request)
    {
        if($request->get('data') == 'membership_details'){
            $request->session()->forget('membership_details');
            $update_discount = DB::table('cart')->where('id', Cart::getCart()->id)->update(['membership_discount'=>0, 'discount_id'=>0, 'discount_percentage'=>0, 'discount_type'=>0 ]);
            DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->update(['discount_applied'=>0,'discount_percentage'=>null,'discount_id'=>null,'discounted_subtotal'=>null,'discount_applied_amount'=>null]);
        }

        if($request->get('data') == 'promocode_details'){
            $request->session()->forget('promocode_details');
            $update_discount = DB::table('cart')->where('id', Cart::getCart()->id)->update(['promocode_discount'=>0, 'discount_id'=>0, 'discount_percentage'=>0, 'discount_type'=>0]);
            DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->update(['discount_applied'=>0,'discount_percentage'=>null,'discount_id'=>null,'discounted_subtotal'=>null,'discount_applied_amount'=>null]);
        }

        if($request->get('data') == 'credit_discount_details'){
            $update_discount = DB::table('cart')->where('id', Cart::getCart()->id)->update(['credit_discount'=>0, 'discount_id'=>0, 'discount_percentage'=>0, 'discount_type'=>0]);
            DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->update(['discount_applied'=>0,'discount_percentage'=>null,'discount_id'=>null,'discounted_subtotal'=>null,'discount_applied_amount'=>null]);
            $request->session()->forget('credit_discount_details');
        }

        if($request->get('data') == 'reward_details'){
            $request->session()->forget('reward_details');
            $update_discount = DB::table('cart')->where('id', Cart::getCart()->id)->update(['reward_discount'=>0, 'discount_id'=>0, 'reward_points_applied'=> 0, 'discount_type'=>0]); 
            DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->update(['discount_applied'=>0,'discount_percentage'=>null,'discount_id'=>null,'discounted_subtotal'=>null,'discount_applied_amount'=>null]);
        }

        if($request->get('data') == 'promotion_details'){
            $promotion_details = session('promotion_details');
            $get_cartitem = DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->where('price','0.0000')->delete();
            $update_discount = DB::table('cart')->where('id', Cart::getCart()->id)->update(['discount_id'=>0, 'discount_type'=>0]); 
            $request->session()->forget('promotion_details');
        }

        return response()->json(['success' =>true]);
    }
            
    public function RazorpayPayment(Request $request){
        
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
        $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
        
        $input = $request->all();

        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $payment = $api->payment->fetch($input['payment_id']);

        try{
            if(count($input)  && !empty($input['payment_id'])) {
                try {
                    $response= $api->payment
                                   ->fetch($input['payment_id'])
                                   ->capture(['amount' => $payment['amount']]);
                }catch(\Exception $e) {
                        // Do nothing
                        // Failed Capture              
                }
            }else{
                throw new Exception("Payment not Successful");
            }
        }catch(\Exception $e) {
            return  $e->getMessage();
            return redirect()->back();
        }

        $data = [   'user_id' => $user->id,
                    'payment_id' => $request->payment_id,
                    'amount' => $request->amount];
                    
        $getId = DB::table('payments')->insertGetId($data);
        $arr = array('payment_id'   => $request->payment_id,
                     'msg'          => 'Payment successfully credited',
                     'status'       => true);

        return Response()->json($arr);       
    }

    public function RazorpayAfterPayment(Request $request)
    {
       return view('shop::checkout.after_payment');
    }
    
    public function notifystatus(Request $request){
        $data = $request->all();
        $subject = '';        
        $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
        if(empty($user))
            $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";

        $cart = cart()->getCart();
        $cart = DB::table('cart')->where('id', $cart->id)->first();

        
        $cart_items = DB::table('cart_items')->where('cart_id', $cart->id)->get();
        $cart = collect($cart)->toArray();        
        $cart_data = json_encode($cart);
        $cart_items_data = json_encode($cart_items->toArray());

        $data['cart'] = [
            'cart_data' => $cart_data,
            'cart_item_data' => $cart_items_data,
        ];

        $data['user'] = [
            'name' =>  $user->first_name .' '. $user->last_name,
            'email' =>  $user->email,
        ];

        if($request->success =="false"){
            $subject = 'Genxt Payment: Error';
        }else{
            $subject = 'Genxt Payment: Success';
        }

        Mail::send(array(), array(), function ($message) use ($data, $subject) {
            $user_data = json_encode($data['user']) .'<br><hr>'.$data['datanofify'];    

            $user_data .= '<br><hr>'.$data['cart']['cart_data'];
            $user_data .= '<br><hr>'.$data['cart']['cart_item_data'];

            $message->to(['deepak@authorselvi.com', 'rajesh.settu@gmail.com'])
              ->subject($subject)
              ->from('info@genxtimplants.com', 'GenXT')
              ->setBody($user_data, 'text/html');
          });        
    }
}

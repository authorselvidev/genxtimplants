<?php

namespace Webkul\Shop\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Webkul\Checkout\Repositories\CartRepository;
use Webkul\Checkout\Repositories\CartItemRepository;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Product\Helpers\ProductImage;
use Webkul\Product\Helpers\View as ProductView;
use Illuminate\Support\Facades\Event;
use Mail;
use Webkul\Shop\Mail\NotifyMeAdminEmail;
use Cart;
use Session;
use DB;

/**
 * Cart controller for the customer and guest users for adding and
 * removing the products in the cart.
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CartController extends Controller
{

    /**
     * Protected Variables that holds instances of the repository classes.
     *
     * @param Array $_config
     * @param $cart
     * @param $cartItem
     * @param $customer
     * @param $product
     * @param $productImage
     * @param $productView
     */
    protected $_config;

    protected $cart;

    protected $cartItem;

    protected $customer;

    protected $product;

    protected $productView;

    public function __construct(
        CartRepository $cart,
        CartItemRepository $cartItem,
        CustomerRepository $customer,
        ProductRepository $product,
        ProductImage $productImage,
        ProductView $productView
    ) {

        $this->middleware('customer')->only(['moveToWishlist']);

        $this->customer = $customer;

        $this->cart = $cart;

        $this->cartItem = $cartItem;

        $this->product = $product;

        $this->productImage = $productImage;

        $this->productView = $productView;

        $this->_config = request('_config');
    }

    /**
     * Method to populate the cart page which will be populated before the checkout process.
     *
     * @return Mixed
     */
    public function index()
    {
        return view($this->_config['view'])->with('cart', Cart::getCart());
    }

    public function MiniCart()
    {
        return view($this->_config['view']);
    }

    /**
     * Function for guests user to add the product in the cart.
     *
     * @return Mixed
     */
    public function add(Request $request, $id)
    {   
        try {
            Event::fire('checkout.cart.add.before', $id);
             if(request()->get('freeprice') != null) {
            $request->session()->push('promotion_details', request()->except('_token'));
        }
            $result = Cart::add($id, request()->except('_token'));

            session()->forget('cart_details');
            session()->forget('membership_details');
            session()->forget('promocode_details');
            session()->forget('reward_details');
            session()->forget('credit_discount_details');

           if(request()->get('freeprice') == null) {
            $promotion_details = session('promotion_details');
            if(isset($promotion_details)) {
              $get_cartitem = \DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->where('price','0.0000')->delete();
            }
             session()->forget('promotion_details');
           }


            Event::fire('checkout.cart.add.after', $result);

            if ($result) {
                session()->flash('success', trans('shop::app.checkout.cart.item.success'));
            } else {
                session()->flash('warning', trans('shop::app.checkout.cart.item.error-add'));
            }

       if(!isset($request->freeprice)) Cart::collectTotals();
           
       
        
        return Response::json($result);

        } 
        catch(\Exception $e) {
            session()->flash('error', trans($e->getMessage()));

            return redirect()->back();
        }
    }


    public function removeQty(Request $request, $id)
    {
        $request = request()->except('_token');
    
        try {
            if ($request['quantity']==0) {
                $itemId=Cart::checkIfItemExists($id,request()->except('_token'));
               $result=Cart::removeItem($itemId);
            }
            else{
                 $result = Cart::removeQty($id, request()->except('_token'));
            }
            session()->forget('cart_details');
            session()->forget('membership_details');
            session()->forget('promocode_details');
            session()->forget('reward_details');
            session()->forget('credit_discount_details');           

          
            $promotion_details = session('promotion_details');

            //dd($promotion_details);
            if(isset($promotion_details)) {
              $get_cartitem = \DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->where('price','0.0000')->delete();
            }
             session()->forget('promotion_details');

            if ($result) {
                session()->flash('success', trans('shop::app.checkout.cart.item.success'));
            } else {
                session()->flash('warning', trans('shop::app.checkout.cart.item.error-add'));
            }

            Cart::collectTotals();

            return Response::json($result);

        } catch(\Exception $e) {
            session()->flash('error', trans($e->getMessage()));

            return redirect()->back();
        }
    }

    /**
     * Removes the item from the cart if it exists
     *
     * @param integer $itemId
     */
    public function remove(Request $request, $itemId)
    {

        Event::fire('checkout.cart.delete.before', $itemId);

        Cart::removeItem($itemId);

        Event::fire('checkout.cart.delete.after', $itemId);

         session()->forget('cart_details');
            session()->forget('membership_details');
            session()->forget('promocode_details');
            session()->forget('reward_details');
            session()->forget('credit_discount_details');

          
            $promotion_details = session('promotion_details');

            //dd($promotion_details);
            if(isset($promotion_details)) {
              $get_cartitem = \DB::table('cart_items')->where('cart_id', Cart::getCart()->id)->where('price','0.0000')->delete();
            }
             session()->forget('promotion_details');


        Cart::collectTotals();

        return redirect()->back();
    }

    /**
     * Updates the quantity of the items present in the cart.
     *
     * @return response
     */
    public function updateBeforeCheckout()
    {
        $request = request()->except('_token');
        //dd($request['qty']);

        foreach ($request['qty'] as $id => $quantity) {
            if ($quantity <= 0) {
                session()->flash('warning', trans('shop::app.checkout.cart.quantity.illegal'));

                return redirect()->back();
            }
        }

        $cart = cart()->getCart();
        // $cart->discount_type =0;
        // $cart->discount_id =0;
        // $cart->discount_percentage =0;
        // $cart->reward_points_applied =0;
        // $cart->save();
        $update_discount = DB::table('cart')->where('id', $cart->id)->update([
            'membership_discount'=>0,
            'promocode_discount'=>0,
            'credit_discount'=>0,
            'discount_id'=>0,
            'reward_discount'=>0,
            'discount_percentage'=>0,
            'discount_type'=>0 ]);
    
        DB::table('cart_items')->where('cart_id', $cart->id)->update(
            ['discount_applied'=>0,
            'discount_percentage'=>null,
            'discount_id'=>null,
            'discounted_subtotal'=>null,
            'discount_applied_amount'=>null]);

        foreach ($request['qty'] as $key => $value) {
            $item = $this->cartItem->findOneByField('id', $key);
            $data['quantity'] = $value;

            Event::fire('checkout.cart.update.before', $key);

            Cart::updateItem($item->product_id, $data, $key);

            Event::fire('checkout.cart.update.after', $item);

            unset($item);
            unset($data);
        }
        
        $promotion_details = session('promotion_details');

        //dd($promotion_details);
        if(isset($promotion_details)) {
            $get_cartitem = \DB::table('cart_items')->where('cart_id', $cart->id)->where('price','0.0000')->delete();
        }

        session()->forget('cart_details');
        session()->forget('membership_details');
        session()->forget('promocode_details');
        session()->forget('reward_details');
        session()->forget('credit_discount_details');
        session()->forget('promotion_details');         
            
        Cart::collectTotals();

        return redirect()->back();

    }



    /**
     * Add the configurable product
     * to the cart.
     *
     * @return response
     */
    public function addConfigurable($slug)
    {
        session()->flash('warning', trans('shop::app.checkout.cart.add-config-warning'));
        return redirect()->route('shop.products.index', $slug);
    }

    public function buyNow($id)
    {
        Event::fire('checkout.cart.add.before', $id);

        $result = Cart::proceedToBuyNow($id);

        Event::fire('checkout.cart.add.after', $result);

        Cart::collectTotals();

        if (! $result) {
            return redirect()->back();
        } else {
            return redirect()->route('shop.checkout.onepage.index');
        }
    }

    /**
     * Function to move a already added product to wishlist
     * will run only on customer authentication.
     *
     * @param instance cartItem $id
     */
    public function moveToWishlist($id)
    {
        $result = Cart::moveToWishlist($id);

        if (! $result) {
            Cart::collectTotals();

            session()->flash('success', trans('shop::app.wishlist.moved'));

            return redirect()->back();
        } else {
            session()->flash('warning', trans('shop::app.wishlist.move-error'));

            return redirect()->back();
        }
    }

    public function NotifyMe(Request $request)
    {
        //dd($request->all());
        $notify_me = \DB::table('notify_me')->insert([
                        'customer_id' => $request->get('user_id'),
                        'product_id' => $request->get('product_id'),
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);


        $customer = DB::table('users')->where('id',$request->get('user_id'))->first();
        $product = DB::table('products_grid')->where('product_id',$request->get('product_id'))->first();
        $admins = DB::table('users')->whereIn('role_id',[1,4])->where('status',1)->get();
        foreach($admins as $key =>$admin)
        {
            $admin_name = $admin->first_name;
            Mail::to($admin->email)->send(new NotifyMeAdminEmail($admin_name,$customer->first_name,$customer->email,$product->name));               
        }
       
        /*session()->flash('success', 'Thank you! The product is currently out-of-stock, you will be notified via email when stock is back.');*/



        return response()->json(['success' => true]);
    }
}

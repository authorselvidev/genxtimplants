<?php

namespace Webkul\Shop\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Product\Repositories\ProductRepository as Product;
use DB;

/**
 * Category controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CategoryController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CategoryRepository object
     *
     * @var array
     */
    protected $category;

    /**
     * ProductRepository object
     *
     * @var array
     */
    protected $product;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Category\Repositories\CategoryRepository $category
     * @param  Webkul\Product\Repositories\ProductRepository $product
     * @return void
     */
    public function __construct(Category $category, Product $product)
    {
        $this->product = $product;

        $this->category = $category;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $category = $this->category->findBySlugOrFail($slug);
        
        return view($this->_config['view'], compact('category'));
    }

    /*public function GetProductsByDiameter(Request $request)
    {
       // print_r($request->all());
       $size_id = $request->get('size_id');
       $category_id = $request->get('category_id')[0];
       $active_cls = 'active';
       $get_products = $this->product->findAllByCategory($category_id);
       //dd($get_products);
        $data=$response=array();
        $data['size_id'] =$size_id;
        $data['category_id'] =$category_id;
        $data['products'] =$get_products;
        //dd($get_products);

        if($category_id == 17)
        {
            $get_child_categories  = \DB::table('categories')->where('parent_id',$category_id)->get();

            foreach ($get_child_categories as $ch_key => $get_child_category)
            {
                $get_child_sub_categories  = \DB::table('categories')->where('parent_id',$get_child_category->id)->get();
                $child_category_name = $this->category->FindCategoryNameById($get_child_category->id);
                $child_categ_name = $child_category_name->name;
                foreach ($get_child_sub_categories as $ch_key => $child_sub_category)
                {
                $child_sub_cat_name = $this->category->FindCategoryNameById($child_sub_category->id);
                $data['child_sub_categ_name'] = $child_sub_cat_name->name;
                $chd_category_products=$this->product->findAllByCategory($child_sub_category->id);
                //dd($chd_category_products);
                $data['parnt_category_id'] =$category_id;
                $data['category_id'] =$get_child_category->id;
                $data['products'] =$chd_category_products;  
                $view_render[] = view('shop::products.list.ajax_products',$data)->render();
                }
               $all_categ_prod[] =  implode('',$view_render);
            }
            //dd($all_categ_prod);
            $response['view'] = '<h2 class="child-cat text-center">'.$child_categ_name.'</h2>'.implode('',$all_categ_prod);

        }
        else{
            $data['parnt_category_id'] = $category_id;
            $response['view'] = view('shop::products.list.ajax_products',$data)->render();
        }
        
        //dd($data['products']);
        //dd($get_products);
        //$response['view'] = 'success';
        return \Response::json($response);
    }*/
    
    
    
    public function GetProductsByDiameter(Request $request)
    {
       // print_r($request->all());
       $size_id = $request->get('size_id');
       $category_id = $request->get('category_id')[0];
       $active_cls = 'active';
       $get_products = $this->product->findAllByCategory($category_id);
		//dd($size_id);
        $data=$response=$chd_category_products=$get_child_categories=array();
        $data['size_id'] =$size_id;
        $data['category_id'] =$category_id;
        $data['products'] =$get_products->sortBy('length');
        //dd($get_products->sortBy('length'));
        $get_child_categories = $this->GetSubCategories($category_id);
        if(count($get_child_categories) > 0)
        {
            //$get_child_categories  = \DB::table('categories')->where('parent_id',$category_id)->get();

			$chld_category_products=$all_sub_categ_prod=$all_categ_prod=array();
            foreach ($get_child_categories as $ch_key => $get_child_category)
            {
				$get_child_sub_categories=array();
                $get_child_sub_categories  = $this->GetSubCategories($get_child_category->id);
                $child_category_name = $this->category->FindCategoryNameById($get_child_category->id);
                $child_categ_name = $child_category_name->name;
                $data['parnt_category_id'] =$category_id;
                $data['category_id'] =$get_child_category->id;
                $view_rend=array();
                if(count($get_child_sub_categories) >0)
                {
					$all_sub_categ_prod=$view_rend=$chld_category_products=array();
                    foreach ($get_child_sub_categories as $ch_key => $child_sub_category)
                    {
                        $child_sub_cat_name = $this->category->FindCategoryNameById($child_sub_category->id);
                        $child_sub_categ_name = $child_sub_cat_name->name;
                        $get_second_level_categories  = $this->GetSubCategories($child_sub_category->id);
                        //dd($get_second_level_categories);
                        if(count($get_second_level_categories) > 0)
                        {
							$sec_view_rend=array();
                            foreach ($get_second_level_categories as $ch_key => $get_second_level_category)
                            {

                                $child_category_name = $this->category->FindCategoryNameById($get_second_level_category->id);
                                $sec_level_cat_name = $child_category_name->name;
                                $chd_category_products=$this->product->findAllByCategory($get_second_level_category->id);
                                //dd($chd_category_products);
                                $data['products'] =$chd_category_products;  
                                $sec_view_render=view('shop::products.list.ajax_products',$data)->render();
                                $sec_view_rend[] ='<h4 class="col-md-12 sub-child-cat text-center">'.$sec_level_cat_name.'</h4>'.$sec_view_render;
                            }
                            $view_rend=implode('',$sec_view_rend);
                        }
                        else
                        {
                            $chld_category_products=$this->product->findAllByCategory($child_sub_category->id);
                            $data['products'] =$chld_category_products;  
                            $view_rend=view('shop::products.list.ajax_products',$data)->render();
                            
                        }
                        
                        $all_sub_categ_prod[] ='<h3 class="col-md-12 sub-cat text-center">'.$child_sub_categ_name.'</h3>'.$view_rend;  

                    }
                    $all_categ_prod[] ='<div class="sub-cat-section col-md-12"><h2 class="col-md-12 child-cat text-center">'.$child_categ_name.'</h2><div class="sub-cat-child-section">'.implode('',$all_sub_categ_prod).'</div></div>';
  
                }
                
                else
                {
					//$data['child_categ_name'] = $child_categ_name;
                    $chd_category_products=$this->product->findAllByCategory($get_child_category->id);
                    $data['products'] =$chd_category_products;
                    $view_render=view('shop::products.list.ajax_products',$data)->render();
                    //dd($get_child_category->id);
                    $product_arr = json_decode(json_encode($chd_category_products), true); 
					$get_size = array_column($product_arr, 'size');
					if((isset($child_categ_name) && $size_id == null) || ($size_id != null && in_array($size_id,$get_size)))
						$all_categ_prod[] = '<div class="sub-cat-section col-md-12"><h2 class="col-md-12 child-cat text-center">'.$child_categ_name.'</h2>'.$view_render.'</div>';
                }
            }

            //dd($all_categ_prod);
            $response['view'] = implode('',$all_categ_prod);

        }
        else{
            $data['parnt_category_id'] = $category_id;
            $response['view'] = view('shop::products.list.ajax_products',$data)->render();
        }
        
        //dd($data['products']);
        //dd($get_products);
        //$response['view'] = 'success';
        return \Response::json($response);
    }

    public function GetSubCategories($cat_id) {

        $get_sub_categories  = \DB::table('categories')->where('parent_id',$cat_id)->orderBy('position', 'ASC')->get();
        return $get_sub_categories;
    }
    
    
}

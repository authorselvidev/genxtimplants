<?php

namespace Webkul\Shop\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Webkul\Customer\Models\Countries;
use Mail;
use Webkul\Shop\Mail\CourseRegisterMail;


/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CourseController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductRepository object
     *
     * @var array
     */

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Product\Repositories\ProductRepository $product
     * @return void
  

    /**
     * Display a listing of the resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = today()->format('Y-m-d');
        $upcoming_courses = DB::table('courses')->where('end_date', '>=', $date)
                                                ->where('is_deleted',0)
                                                ->orderBy('id', 'DESC')
                                                ->get();
                                                
        $past_courses = DB::table('courses')->where('end_date', '<', $date)
                                                ->where('is_deleted',0)
                                                ->orderBy('id', 'DESC')
                                                ->get();
        
        return view('shop::courses.index')->with(['upcoming_courses' => $upcoming_courses,'past_courses' => $past_courses]);
    }

    public function show($slug)
    {
        $course = DB::table('courses')->where('slug',$slug)->first();
        $slider1_images = DB::table('course_images')->where('course_id',$course->id)->where('slider_set',1)->get();
        $slider2_images = DB::table('course_images')->where('course_id',$course->id)->where('slider_set',2)->get();
        return view('shop::courses.show',compact('course','slider1_images','slider2_images'));
    }

    public function CourseRegister(Request $request)
    {
        //dd($request->all());
        $this->validate(request(), [
            'my_title' => 'required',
            'first_name' => 'string|required',
            'email' => 'email|required|unique:users,email',
            'phone_number' => 'required|numeric',
            'country_id' => 'required',
            'course_msg' => 'required'
            ]);

        $data=$request->all();
        $data['full_name'] = $data['first_name'].' '.$data['last_name'];
        $data['country_name'] = Countries::GetCountryName($data['country_id']);
          $admins = DB::table('users')->where('role_id',1)->where('status',1)->get();
          //dd($admins);
                foreach($admins as $key =>$admin)
                {
                    Mail::to($admin->email)->send(new CourseRegisterMail($data['course_name'],$data['my_title'],$data['full_name'], $data['email'],$data['phone_number'],$data['country_name'],$data['course_msg']));               
                }



        session()->flash('success', 'Register form has been sent! we will get in touch with you soon..');

        return redirect()->route('shop.course.index');
    }

}

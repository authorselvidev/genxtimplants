<?php

return [
    [
        'key' => 'account.profile',
        'name' => 'shop::app.layouts.profile',
        'route' =>'customer.profile.index',
        'sort' => 1
    ],
    [
        'key' => 'account.orders',
        'name' => 'shop::app.layouts.orders',
        'route' =>'customer.orders.index',
        'sort' => 2
    ],
    [
        'key' => 'account.wishlist',
        'name' => 'shop::app.layouts.wishlist',
        'route' =>'customer.wishlist.index',
        'sort' => 3
    ], 
    [
        'key' => 'account.reviews',
        'name' => 'shop::app.layouts.reviews',
        'route' =>'customer.reviews.index',
        'sort' => 4
    ],
    [
        'key' => 'account.failed-implants',
        'name' => 'Return Implants',
        'route' => 'failure.implants.index',
        'sort' => 5
    ], 
    [
        'key' => 'account.myoffers',
        'name' => 'My Offers',
        'route' =>'customer.myoffers',
        'sort' => 6
    ],
    [
        'key' => 'account',
        'name' => 'shop::app.layouts.my-account',
        'route' =>'customer.profile.index',
        'sort' => 7
    ],
    [
        'key' => 'account.webinar',
        'name' => 'My Webinars',
        'route' =>'webinar.index',
        'sort' => 8
    ],
      [
        'key' => 'account.ondemandwebinar',
        'name' => 'On-Demand Webinars',
        'route' =>'webinar.ondemand',
        'sort' => 9
    ],
    [
        'key' => 'account.credit-payments',
        'name' => 'Make Payments',
        'route' =>'customer.credit_payment.pay',
        'sort' => 10
    ],  [
        'key' => 'account.address',
        'name' => 'shop::app.layouts.address',
        'route' =>'customer.address.index',
        'sort' => 11
    ], 
    

];

?>

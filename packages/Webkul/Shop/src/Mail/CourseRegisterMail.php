<?php

namespace Webkul\Shop\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourseRegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($course_name,$my_title,$full_name,$email,$phone_number,$country_name,$course_msg)
    {
        $this->course_name = $course_name;
        $this->my_title = $my_title;
        $this->full_name = $full_name;
        $this->email = $email;
        $this->phone_number = $phone_number;
        $this->country_name = $country_name;
        $this->course_msg = $course_msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@genxtimplants.com', 'GenXT')->view('shop::emails.course-mail',[
                                            'course_name' => $this->course_name,
                                            'my_title'=>$this->my_title,
                                            'full_name'=>$this->full_name,
                                            'email'=>$this->email,
                                            'phone_number' => $this->phone_number,
                                            'country_name' => $this->country_name,
                                            'course_msg' => $this->course_msg,
                                         ])->subject('GenXT - Course Form!');
    }
}

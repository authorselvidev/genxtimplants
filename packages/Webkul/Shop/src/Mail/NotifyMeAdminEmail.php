<?php

namespace Webkul\Shop\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyMeAdminEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($admin_name,$first_name,$email,$product_name)
    {
        $this->admin_name = $admin_name;
        $this->first_name = $first_name;
        $this->email = $email;
        $this->product_name = $product_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@genxtimplants.com', 'GenXT')->view('shop::emails.requested-product-toadmin',[
                                            'admin_name' => $this->admin_name,
                                            'first_name' => $this->first_name,
                                            'email' => $this->email,
                                            'product_name'=>$this->product_name,
                                         ])->subject('GenXT - Customer Requested Product!');
    }
}

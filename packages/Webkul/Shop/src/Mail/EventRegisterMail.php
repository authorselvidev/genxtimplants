<?php

namespace Webkul\Shop\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventRegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($event_name,$my_title,$full_name,$email,$phone_number,$country_name,$event_msg)
    {
        $this->event_name = $event_name;
        $this->my_title = $my_title;
        $this->full_name = $full_name;
        $this->email = $email;
        $this->phone_number = $phone_number;
        $this->country_name = $country_name;
        $this->event_msg = $event_msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@genxtimplants.com', 'GenXT')->view('shop::emails.event-mail',[
                                            'event_name' => $this->event_name,
                                            'my_title'=>$this->my_title,
                                            'full_name'=>$this->full_name,
                                            'email'=>$this->email,
                                            'phone_number' => $this->phone_number,
                                            'country_name' => $this->country_name,
                                            'event_msg' => $this->event_msg,
                                         ])->subject('GenXT - Event Form!');
    }
}

@extends('shop::layouts.master')
@section('page_title')
    {{ __('shop::app.customer.signup-form.page-title') }}
@endsection
@section('content-wrapper')

<div class="auth-content">

    {!! view_render_event('bagisto.shop.customers.signup.before') !!}
    <?php  
       // $customer = (session('customer') != null) ? session('customer') : "";
    $customer="";
    if(isset($_GET['id'])){
      $customer = DB::table('temp_users')->where('id',$_GET['id'])->first();
    if(!isset($customer))
      $customer = DB::table('users')->where('id',$_GET['id'])->first();
    }   ?>
    <form method="post" action="{{ route('customer.register.create') }}" >

        @csrf

        <div class="login-form">
            <div class="login-text text-center">{{ __('shop::app.customer.signup-form.title') }}</div>

            {!! view_render_event('bagisto.shop.customers.signup_form_controls.before') !!}
            <div class="form-fields col-md-12">
                <div class="form-field col-md-12">
                    <div class="control-group col-md-3 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                        <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                        <select name="my_title" class="control" v-validate="'required'">
                            <option @if(isset($customer->my_title) && $customer->my_title == "Mr.") {{ 'selected'}} @endif value="Mr.">Mr.</option>
                            <option @if(isset($customer->my_title) && $customer->my_title == "Mrs.") {{ 'selected'}} @endif  value="Mrs.">Mrs.</option>
                            <option @if(isset($customer->my_title) && $customer->my_title == "Mr. Dr.") {{ 'selected'}} @endif value="Mr. Dr.">Mr. Dr.</option>
                            <option @if(isset($customer->my_title) && $customer->my_title == "Mrs. Dr.") {{ 'selected'}} @endif value="Mrs. Dr.">Mrs. Dr.</option>
                        </select>
                        <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                    </div>
                   
                    <div class="control-group col-md-5" :class="[errors.has('first_name') ? 'has-error' : '']" data-vv-as="&quot;{{ __('First Name') }}&quot;">
                        <label for="first_name" class="required">First Name</label>
                        <input type="text" class="control" name="first_name" v-validate="'required'" value="{{ (empty(old('first_name'))) ? ((isset($customer->first_name)) ? $customer->first_name : '') : old('first_name') }}" data-vv-as="&quot;{{ __('First Name') }}&quot;">
                        <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                    </div>
                    <div class="control-group col-md-4 cpass">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="control" name="last_name" value="{{ (empty(old('last_name'))) ? ((isset($customer->last_name)) ? $customer->last_name : '') : old('last_name') }}">
                        
                    </div>
                </div>
                <div class="form-field col-md-12">
                    <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                        <label for="phone_number" class="required">Mobile Number</label>
                        <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ (empty(old('phone_number'))) ? ((isset($customer->phone_number)) ? $customer->phone_number : '') : old('phone_number') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.phonenumber') }}&quot;">
                        <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                    </div>

                    <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                        <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                        <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ (empty(old('email'))) ? ((isset($customer->email)) ? $customer->email : '') : old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                        <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                    </div>
                </div>

                
                <div class="doctor-part col-md-12">
                    <div class="form-field col-md-12">
                        <div class="control-group pass col-md-6" :class="[errors.has('clinic_name') ? 'has-error' : '']">
                            <label for="clinic_name" class="required">{{ __('shop::app.customer.signup-form.clinicname') }}</label>
                            <input type="text" v-validate="'required'"  class="control" name="clinic_name" value="{{ (empty(old('clinic_name'))) ? ((isset($customer->clinic_name)) ? $customer->clinic_name : '') : old('clinic_name') }}" data-vv-as="&quot;{{ __('Clinic Name') }}&quot;">
                            <span class="control-error" v-if="errors.has('clinic_name')">@{{ errors.first('clinic_name') }}</span>
                            
                        </div>
                        <div class="control-group col-md-6 cpass">
                            <label for="clinic_number">Clinic Number</label>
                            <input type="text" class="control" name="clinic_number" value="{{ (empty(old('clinic_number'))) ? ((isset($customer->clinic_number)) ? $customer->clinic_number : '') : old('clinic_number') }}">                            
                        </div>
                    </div>
                    <div class="form-field col-md-12">
                        <div class="control-group" :class="[errors.has('clinic_address') ? 'has-error' : '']">
                            <label for="clinic_address" class="required">{{ __('shop::app.customer.signup-form.clinicaddr') }}</label>
                            <textarea class="control" name="clinic_address" value="{{ old('clinic_address') }}" v-validate="'required'" data-vv-as="&quot;{{ __('Clinic Address') }}&quot;">{{ (empty(old('clinic_address'))) ? ((isset($customer->clinic_address)) ? $customer->clinic_address : '') : old('clinic_address') }}</textarea>
                            <span class="control-error" v-if="errors.has('clinic_address')">@{{ errors.first('clinic_address') }}</span>
                        </div>
                    </div>
                    <div class="form-field col-md-12">
                        <div class="control-group col-md-6 pass " :class="[errors.has('dental_license_no') ? 'has-error' : '']">
                            <label for="dental_license_no" class="required">Dental License Number</label>
                            <input type="text" class="control" name="dental_license_no" value="{{ (empty(old('dental_license_no'))) ? ((isset($customer->dental_license_no)) ? $customer->dental_license_no : '') : old('dental_license_no') }}" v-validate="'required'" data-vv-as="&quot;{{ __('Dental License Number') }}&quot;">
                            <span class="control-error" v-if="errors.has('dental_license_no')">@{{ errors.first('dental_license_no') }}</span>
                        </div>
                        <div class="control-group col-md-6 cpass">                            
                            <label for="invite_code">Invite Code</label>
                            @if(empty($dealer_invite_code))

                                <input type="text" class="control" name="invite_code" id="invite_code" value="{{old('invite_code')}}">
                            @else
                                
                                <input type="text" class="control" name="invite_code" readonly id="invite_code" value="{{$dealer_invite_code}}" style="cursor: not-allowed;">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-field col-md-12">
                    <div class="control-group pass col-md-6" :class="[errors.has('country_id') ? 'has-error' : '']">
                        <label for="country_id" class="required">{{ __('shop::app.customer.signup-form.country') }}</label>
                        <input type="text" class="control" name="country_id" v-validate="'required'" disabled="disabled" value="India">
                        <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                    </div>
                     <div class="control-group cpass col-md-6" :class="[errors.has('state_id') ? 'has-error' : '']">
                        <label for="state_id" class="required">State</label>
                        <?php $states = array();
                        $states = DB::table('country_states')->where('country_id',101)->get();?>
                        <select name="state_id" id="state" class="control" v-validate="'required'" data-vv-as="&quot;{{ __('State') }}&quot;">
                            <option value="">Choose your state...</option>
                                <?php foreach($states as $key => $state) { 
                                    $selected="";
                                if(isset($customer->state_id)){
                                    if($customer->state_id == $state->id) $selected = 'selected';
                                }?>
                            <option {{$selected}} value="{{ $state->id }}">{{ $state->name }}</option>
                            <?php } ?>
                        </select>
                        <span class="control-error" v-if="errors.has('state_id')">@{{ errors.first('state_id') }}</span>
                    </div>
                </div>
                <div class="form-field col-md-12">
                    <div class="control-group pass col-md-6" :class="[errors.has('city_id') ? 'has-error' : '']">
                        <label for="city" class="required">{{ __('shop::app.customer.signup-form.city') }}</label>
                            <?php $cities = array();
                            if(isset($customer->state_id)){
                                $cities = DB::table('country_state_cities')->where('state_id',$customer->state_id)->get();
                                ?>
                                <select name="city_id" id="city" class="control" v-validate="'required'">
                                    <option value="">Choose your city...</option>
                                    <?php foreach($cities as $key => $city) { ?>
                                        <option <?php if($city->id == $customer->city_id) echo 'selected'; ?> value="{{ $city->id }}">{{ $city->name }}</option>
                                    <?php } ?>
                                </select>
                            <?php } else { ?>
                                <select name="city_id" id="city" class="control" v-validate="'required'"  data-vv-as="&quot;{{ __('City') }}&quot;"></select>
                        <?php } ?>
                        <span class="control-error" v-if="errors.has('city_id')">@{{ errors.first('city_id') }}</span>
                    </div>



                <div class="control-group col-md-6 cpass" :class="[errors.has('pin_code') ? 'has-error' : '']">
                    <label for="pin_code" class="required">Pincode</label>
                    <input type="text" class="control" name="pin_code" value="{{ (empty(old('pin_code'))) ? ((isset($customer->pin_code)) ? $customer->pin_code : "") : old('pin_code') }}" v-validate="'required'"  data-vv-as="&quot;{{ __('Pincode') }}&quot;">
                    <span class="control-error" v-if="errors.has('pin_code')">@{{ errors.first('pin_code') }}</span>
                </div>
            </div>
            <input type="hidden" name="user_id" value="{{(isset($customer->id)) ? $customer->id : ""}}">
           


                {{-- <div class="signup-confirm" :class="[errors.has('agreement') ? 'has-error' : '']">
                    <span class="checkbox">
                        <input type="checkbox" id="checkbox2" name="agreement" v-validate="'required'">
                        <label class="checkbox-view" for="checkbox2"></label>
                        <span>{{ __('shop::app.customer.signup-form.agree') }}
                            <a href="">{{ __('shop::app.customer.signup-form.terms') }}</a> & <a href="">{{ __('shop::app.customer.signup-form.conditions') }}</a> {{ __('shop::app.customer.signup-form.using') }}.
                        </span>
                    </span>
                    <span class="control-error" v-if="errors.has('agreement')">@{{ errors.first('agreement') }}</span>
                </div> --}}

                {!! view_render_event('bagisto.shop.customers.signup_form_controls.after') !!}

                {{-- <div class="control-group" :class="[errors.has('agreement') ? 'has-error' : '']">

                    <input type="checkbox" id="checkbox2" name="agreement" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.agreement') }}&quot;">
                    <span>{{ __('shop::app.customer.signup-form.agree') }}
                        <a href="">{{ __('shop::app.customer.signup-form.terms') }}</a> & <a href="">{{ __('shop::app.customer.signup-form.conditions') }}</a> {{ __('shop::app.customer.signup-form.using') }}.
                    </span>
                    <span class="control-error" v-if="errors.has('agreement')">@{{ errors.first('agreement') }}</span>
                </div> --}}

                <button class="btn btn-primary btn-lg" type="submit">
                    {{ __('shop::app.customer.signup-form.button_title') }}
                </button>
            </div>
        </div>
    </form>

    {!! view_render_event('bagisto.shop.customers.signup.after') !!}
	
    <div class="sign-up-text">
        {{ __('shop::app.customer.signup-text.account_exists') }} - <a href="{{ route('customer.session.index') }}">{{ __('shop::app.customer.signup-text.title') }}</a>
    </div>
	
</div>
@endsection

@push('scripts')
<script>

    $(document).ready(function(){
        var city_id = "{{ (empty(old('city_id'))) ? ((isset($customer->city_id)) ? $customer->city_id : '') : old('city_id') }}";
        var state_id = "{{ (empty(old('state_id'))) ? ((isset($customer->state_id)) ? $customer->state_id : '') : old('state_id') }}"; 
        getCity(state_id);
        function getCity(stateID){
            if(stateID){
                $.ajax({
                type:"GET",
                url:"{{url('customer/get-city-list')}}?state_id="+stateID,
                success:function(res){               
                    if(res){
                        $("#city").empty();
                        $.each(res,function(key,value){
                            var selected_city='';
                            if(city_id == key) { selected_city = "{{ 'selected' }}" }
                            $("#city").append('<option  value="'+key+'" '+selected_city+'>'+value+'</option>');
                        });
                
                    }else{
                    $("#city").empty();
                    }
                }
                });
           
            }else{
                $("#city").empty();
            }
        }

        $('body').on('change','#state', function(){
                getCity($(this).val());
        });
    });
</script>
@endpush

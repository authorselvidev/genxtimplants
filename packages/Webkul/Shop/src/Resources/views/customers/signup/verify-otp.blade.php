@extends('shop::layouts.master')
@section('page_title')
    {{ __('shop::app.customer.signup-form.page-title') }}
@endsection
@section('content-wrapper')

<div class="auth-content">

    <form method="post" action="{{route('verify.otp')}}" >

        @csrf

        @if(session()->get('already_exists_user')==true)
            <div style="color:red; padding: 10px; text-align: center; width: 100%">
                @if(session()->get('is_phone_exists')==true)
                    <?php $msg = "Mobile number already exists";
                          session()->forget('is_phone_exists'); ?>
                @elseif(session()->get('is_email_exists')==true)
                    <?php $msg = "Email already exists";
                          session()->forget('is_email_exists'); ?>
                @elseif(session()->get('is_phone_email_exists')==true)
                    <?php $msg = "Mobile number and email already exists"; 
                          session()->forget('is_phone_email_exists'); ?>
                @endif
                {{$msg}}, you can continue with registration
            </div>
        @endif

        <div class="login-form">
            <div class="login-text">Mobile Number Verification</div>
            
            <div class="control-group">
                <label for="mobile">Mobile Number</label>
                <input type="text" class="control" name="phone_number" value="{{$customer_details->phone_number}}" readonly />
            </div>

            <div class="control-group">
                <label for="otp">Enter OTP</label>
                <input type="text" class="control" id="otp" name="verify_otp">
                <div style="float: right; margin-top:10px" id="countdown-container">
                    @if(session()->get('already_exists_user')==true || !session()->get('otp_resend'))
                        <a href="{{route('change.otp',$customer_details->id)}}">Resend OTP</a>
                    @else
                        Resend OTP in : <span id="countdown"></span> minutes
                    @endif                 
                </div>
            </div>
            
            <input class="btn btn-primary btn-lg verifyOtp" type="submit" id="submit" value="Submit">
        </div>
    </form>
</div>
<script type="text/javascript">
    function getTime(value){
        value = parseInt(value);
        let min = Math.floor(value/60).toString().padStart(2,'0');
        let sec = (value - min*60).toString().padStart(2,'0');
        return min+':'+sec;
    }
    
    var timeleft = 900; //900
    var container = $('#countdown');
    container.text(getTime(timeleft));
    if(container.length){
        var optTimer = setInterval(function(){
            $('#countdown').text(getTime(--timeleft));
            if(timeleft <= 0){
                clearInterval(optTimer);
                $("#countdown-container").html('<a href="{{route("change.otp",$customer_details->id)}}">Resend OTP</a>');
            }
        }, 1000);
    }
</script>
<?php 
    session()->forget('already_exists_user');
    session()->forget('otp_resend');
?>
@endsection


 @push('scripts')
 <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', '.verifyOtp', function(event) {
            $(this).css('opacity', '0.6'); 
            $(this).css('pointer-events', 'none'); 
        });

        $('.content-container').css({
            'background': '#F1F3F6',
        });
    });
</script>
@endpush

@extends('shop::layouts.master')
@section('page_title')
    {{ 'Signup Success - GenXT' }}
@endsection
@section('content-wrapper')
<div class="signup-success section text-center" style="margin:12% 0">

<h1>Thank you</h1>
<h3>Your details have been successfully submitted.</h3>
<h3>You will receive a link to create your password within 24 hours, as soon your details are verified and approved.</h3>


</div>
@endsection

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <title>@yield('page_title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/shop.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/webkul/ui/assets/css/ui.css') }}">
    <link href="{{ bagisto_asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{!! bagisto_asset('js/jquery.min.js') !!}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    @if ($favicon = core()->getCurrentChannel()->favicon_url)
        <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
    @else
        <link rel="icon" sizes="16x16" href="{{ bagisto_asset('images/favicon.ico') }}" />
    @endif

    @yield('head')

    @section('seo')
        <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
    @show

    @stack('css')

    {!! view_render_event('bagisto.shop.layout.head') !!}

</head>
<style>
.header .header-top{width:100%;text-align:center;display:inline-block;padding:12px 0;}
.footer-bottom{position:fixed;bottom:0;}
.set-pwd-form .login-form{margin:auto;float:none;}
.control-group .control{width:100%;}
</style>
<body class="set-password-page">
	{!! view_render_event('bagisto.shop.layout.body.before') !!}

    <div id="app">
        <flash-wrapper ref='flashes'></flash-wrapper>
        <div class="header" id="header">
		    <div class="header-top-section">
		        <div class="main-container-wrapper">
		            <div class="header-top">
		                <a href="{{ route('shop.home.index') }}">
		                    @if ($logo = core()->getCurrentChannel()->logo_url)
		                        <img class="logo" src="{{ $logo }}" />
		                    @else
		                        <img class="logo" src="{{ bagisto_asset('images/genxt-logo.png') }}" />
		                    @endif
		                </a>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="set-pwd-form login-form col-md-12">
			<form method="POST" action="{{ route('customer.register.storepassword', $get_user->id) }}" @submit.prevent="onSubmit">
            @csrf
            <input type="hidden" name="user_id" value="{{ $get_user->id}}">
            <input type="hidden" name="email" value="{{ $get_user->email}}">
            <div class="login-form col-md-5">
                <h3 class="text-center login-text">Set your Password</h3>

                {!! view_render_event('bagisto.shop.customers.login_form_controls.before') !!}

                <div class="control-group" :class="[errors.has('email') ? 'has-error' : '']">
                    <label for="email" class="required">{{ __('shop::app.customer.login-form.email') }}</label>
                    <input type="text" class="control" name="email" disabled="disabled" value="{{ $get_user->email }}" >
                    <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                </div>

                <div class="control-group" :class="[errors.has('password') ? 'has-error' : '']">
                    <label for="password" class="required">{{ __('shop::app.customer.login-form.password') }}</label>
                    <input type="password" class="control" name="password" v-validate="'required|min:6'" value="{{ old('password') }}" data-vv-as="&quot;{{ __('shop::app.customer.login-form.password') }}&quot;">
                    <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span>
                </div>

                {!! view_render_event('bagisto.shop.customers.login_form_controls.after') !!}

                 <div class="control-group" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                    <label for="password" class="required">Confirm Password</label>
                    <input type="password" class="control" name="password_confirmation" v-validate="'required|min:6'" value="" >
                    <span class="control-error" v-if="errors.has('password_confirmation')">@{{ errors.first('password') }}</span>
                </div>
                

                <input class="btn btn-primary btn-lg" type="submit" value="Set Password">
            </div>
        </form>
		</div>







    <div class="footer-bottom">
            <div class="main-container-wrapper">
                <div class="footer-inner col-md-12">
                    <div class="col-md-3 col-sm-12">
                        <a href="{{ url('/') }}"><img src="{!! bagisto_asset('images/footer-logo.png') !!}"></a>
                    </div>
                    <div class="copyright col-md-6 col-sm-12">
                        {{ __('shop::app.webkul.copy-right') }}
                    </div>
                    <div class="social-icons text-right col-md-3 col-sm-12">
                        <ul>
                            <li><a href="https://www.facebook.com/genxt.implants" target="_blank"><img src="{!! bagisto_asset('images/fb-icon.png') !!}"></a></li>
                            <li><a href="https://www.youtube.com/c/genxtimplantology" target="_blank"><img src="{!! bagisto_asset('images/yt-icon.png') !!}"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>





    <script type="text/javascript">
        window.flashMessages = [];

        @if ($success = session('success'))
            window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
        @elseif ($warning = session('warning'))
            window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
        @elseif ($error = session('error'))
            window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }
            ];
        @elseif ($info = session('info'))
            window.flashMessages = [{'type': 'alert-info', 'message': "{{ $info }}" }
            ];
        @endif

        window.serverErrors = [];
        @if(isset($errors))
            @if (count($errors))
                window.serverErrors = @json($errors->getMessages());
            @endif
        @endif
    </script>

    <script type="text/javascript" src="{{ bagisto_asset('js/shop.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>
    <script src="{{ bagisto_asset('js/bootstrap.min.js') }}"></script>

    @stack('scripts')

    {!! view_render_event('bagisto.shop.layout.body.after') !!}

    <div class="modal-overlay"></div>
<script>
            $(function(){
    var current = location.href;
    $('.category-nav li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('href') === current){
            $this.parent().addClass('active');
        }
    })
})
        </script>
</body>

</html>

@extends('shop::layouts.master')

@section('page_title')
    Return Implants
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">
    <div class="account-content">
        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">

            <div class="account-head mb-10">
                <span class="back-icon">
                    <a href="{{ route('customer.account.index') }}">
                        <i class="icon angle-left-icon back-link"></i>
                    </a>
                </span>
                <span class="account-heading">
                    Return Implants
                </span>

                <div class="horizontal-rule"></div>
            </div>

            <div class="account-items-list">

                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">

                                <li class="active" style="cursor: pointer;">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" id="usedImplants" role="tab" title="Step 1">
                                        <span class="round-tab">
                                            Used Implants
                                        </span>
                                    </a>
                                </li>

                                <li class="" style="cursor: pointer;">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" id="notUsedImplants" role="tab" title="Step 2">
                                        <span class="round-tab">
                                            Unopened Implants Exchange
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active" role="tabpanel" id="step1">
                                <div class="table">
                                    <a class="btn btn-primary btn-sm create_btn" href="{{route('failure.implants.usedform')}}">Create</a>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th style="width: 120px;">Order ID</th>
                                                <th> Status </th>
                                                <th> Order Mode </th>
                                                <th> Product Name </th>
                                                <th> Created Date </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach ($failure_implants_used as $item)
                                                <?php
                                                    $order = DB::table('order_items')->where('id', $item->order_item_id)->addSelect('order_id', 'name')->first();
                                                ?>
                                                <tr>
                                                    <td>
                                                        @if($item->order_type == 'on')
                                                            <a href="{{ route('customer.orders.view', base64_encode($order->order_id) ) }}">
                                                                #{{ $order->order_id }}
                                                            </a> 
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($item->status == 'Processing')
                                                            <span class="badge badge-md badge-success" style="background-color: #212427;">Processing</span>
                                                        @elseif ($item->status == 'Completed')
                                                            <span class="badge badge-md badge-success" style="background-color: #007704;">Completed</span>
                                                        @elseif ($item->status == 'Approved')
                                                            <span class="badge badge-md badge-success" style="background-color: #2D7172;">Approved</span>
                                                        @elseif ($item->status == 'Rejected')
                                                            <span class="badge badge-md badge-danger" style="background-color: #e40000;">Rejected</span>
                                                        @elseif ($item->status == 'Pending')
                                                            <span class="badge badge-md badge-warning">Pending</span>
                                                        @endif
                                                    </td>
                                                    <td>{{($item->order_type =='on')?'Online':'Offline'}}</td>
                                                    <td>{{($item->order_type=='on')?$order->name:$item->order_item}}</td>
                                                    <td>{{date('d M Y', strtotime($item->created_at))}}</td>
                                                    <td style="font-size: 16px">
                                                        <a href="{{ route('failure.implants.view', $item->id) }}">
                                                            <i class="icon eye-icon"></i>
                                                        </a>
                                                        @if($item->status == 'Approved' || $item->status == 'Processing' )
                                                            <a href="{{ route('failed-implants.print', $item->id) }}" style="color: #999">
                                                                <i class="fa fa-print"></i>
                                                            </a>
                                                        @endif

                                                        @if($item->status == 'Pending')
                                                        <a href="{{ route('failure.implants.usededit', $item->id) }}">
                                                            <i class="icon pencil-lg-icon"></i>
                                                        </a>
                                                        @endif
                                                        @if($item->status == 'Pending')
                                                            <a href="{{ route('failure.implants.delete', $item->id) }}">
                                                                <i class="icon trash-icon"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach

                                            @if (!$failure_implants_used->count())
                                                <tr>
                                                    <td class="empty" colspan="6">{{ __('admin::app.common.no-result-found') }}</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    <p class="print_notice"><em>Note: Please print this form and attach on the Return implants.</em></p>
                                </div>

                                @if (!$failure_implants_used->count())
                                    <div class="responsive-empty">{{ __('admin::app.common.no-result-found') }}</div>
                                @endif

                            </div>

                            <div class="tab-pane" role="tabpanel" id="step2">
                                <div class="table">
                                    <a class="btn btn-primary btn-sm create_btn" href="{{route('failure.implants.unusedform')}}">Create</a>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th style="width: 120px;">Order ID</th>
                                                <th> Status </th>
                                                <th> Order Mode </th>
                                                <th> Product Name </th>
                                                <th> Created Date </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach ($failure_implants_notused as $item)
                                                <?php
                                                    $order = DB::table('order_items')->where('id', $item->order_item_id)->addSelect('order_id', 'name')->first();
                                                ?>
                                                <tr>
                                                    <td>
                                                        @if($item->order_type == 'on')
                                                            <a href="{{ route('customer.orders.view', base64_encode($order->order_id) ) }}">
                                                                #{{ $order->order_id }}
                                                            </a> 
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($item->status == 'Processing')
                                                            <span class="badge badge-md badge-success" style="background-color: #212427;">Processing</span>
                                                        @elseif ($item->status == 'Completed')
                                                            <span class="badge badge-md badge-success" style="background-color: #007704;">Completed</span>
                                                        @elseif ($item->status == 'Approved')
                                                            <span class="badge badge-md badge-success" style="background-color: #2D7172;">Approved</span>
                                                        @elseif ($item->status == 'Rejected')
                                                            <span class="badge badge-md badge-danger" style="background-color: #e40000;">Rejected</span>
                                                        @elseif ($item->status == 'Pending')
                                                            <span class="badge badge-md badge-warning">Pending</span>
                                                        @endif
                                                    </td>
                                                    <td>{{($item->order_type =='on')?'Online':'Offline'}}</td>
                                                    <td>{{($item->order_type=='on')?$order->name:$item->order_item}}</td>
                                                    <td>{{date('d M Y', strtotime($item->created_at))}}</td>
                                                    <td style="font-size: 16px">
                                                        <a href="{{ route('failure.implants.view', $item->id) }}">
                                                            <i class="icon eye-icon"></i>
                                                        </a>
                                                        @if($item->status == 'Approved' || $item->status == 'Processing' )
                                                            <a href="{{ route('failed-implants.print', $item->id) }}" style="color: #999">
                                                                <i class="fa fa-print"></i>
                                                            </a>
                                                        @endif
                                                        
                                                        @if($item->status == 'Pending')
                                                        <a href="{{ route('failure.implants.unusededit', $item->id) }}">
                                                            <i class="icon pencil-lg-icon"></i>
                                                        </a>
                                                        @endif
                                                        @if($item->status == 'Pending')
                                                            <a href="{{ route('failure.implants.delete', $item->id) }}">
                                                                <i class="icon trash-icon"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach

                                            @if (!$failure_implants_notused->count())
                                                <tr>
                                                    <td class="empty" colspan="6">{{ __('admin::app.common.no-result-found') }}</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    <p class="print_notice"><em>Note: Please print this form and attach on the return implants.</em></p>
                                </div>

                                @if (!$failure_implants_notused->count())
                                    <div class="responsive-empty">{{ __('admin::app.common.no-result-found') }}</div>
                                @endif

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('scripts')
    <style type="text/css">
        .create_btn{
            display: inline-block;
            float:right;
            font-size: 1.5rem;
            margin-bottom: 5px;
            margin: 8px;
        }

        .print_notice{
            display: block;
            padding: 5px 10px;
            margin-top:10px;
            color: #444;
            font-size: 1.5rem;
            text-align: center;

        }

        .icon{
            width: 18px !important;
            height: 18px !important;
        }

        #order_item, .selectize-input{
            max-width: 320px !important;
            width: 100% !important;
        }

        .active .round-tab{
            color: #0995CD;
        }

		button.first_page{
            display: flex !important;
            align-items: center !important;
            overflow: hidden;
        }

        .spinning{
            margin-left: 10px;
            display:inline-block;
            border: 2px solid #f3f3f3;
            border-radius: 50%;
            border-top: 2px solid #3498db;
            padding: 6px;
            -webkit-animation: loadingSpin 1.2s linear infinite;
            animation: loadingSpin 1.2s linear infinite;
        }

        .imgThumb{
            width: 100px;
            margin: 10px 10px 0;
            cursor: pointer;
        }

        @-webkit-keyframes loadingSpin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }


        @keyframes loadingSpin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        @media(max-width: 768px){
            .nav-tabs a{
                text-align: center;
                border:1px solid #fff !important;
            }

            .active .round-tab{
                border-bottom: 1px solid #0995CD;
            }
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        });
    </script>
@endpush

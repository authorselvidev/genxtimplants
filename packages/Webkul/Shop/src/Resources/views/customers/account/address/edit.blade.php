@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.address.edit.page-title') }}
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">

    <div class="account-content">
        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">

            <div class="account-head mb-15">
                <span class="back-icon"><a href="{{ route('customer.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
                <span class="account-heading">{{ __('shop::app.customer.account.address.edit.title') }}</span>
                <span></span>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.address.edit.before', ['address' => $address]) !!}
            
            <form method="post" action="{{ route('customer.address.update', base64_encode($address->id)) }}" @submit.prevent="onSubmit">
        <?php $get_name = explode('-',$address->name); ?>
                <div class="account-table-content addr-sec">
                    @method('PUT')
                    @csrf
                    @if(isset($_GET['returnto']))
                        <input type="hidden" name="checkout" value="{{$_GET['returnto']}}">
                    @endif
                    
                    {!! view_render_event('bagisto.shop.customers.account.address.edit_form_controls.before', ['address' => $address]) !!}
                    <div class="form-field">
                        <div class="control-group col-md-3 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                        <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                        <select name="my_title" class="control" v-validate="'required'">
                            <option @if($get_name[0]=="Mr.") {{'selected'}} @endif value="Mr.">Mr.</option>
                            <option @if($get_name[0]=="Mrs.") {{'selected'}} @endif value="Mrs.">Mrs.</option>
                            <option @if($get_name[0]=="Mr. Dr.") {{ 'selected'}} @endif value="Mr. Dr.">Mr. Dr.</option>
                            <option @if($get_name[0]=="Mrs. Dr.") {{ 'selected'}} @endif value="Mrs. Dr.">Mrs. Dr.</option>
                        </select>
                        <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                    </div>

                        <div class="control-group col-md-5" :class="[errors.has('name') ? 'has-error' : '']">
                            <label for="name" class="required">First Name</label>
                            <input type="text" class="control" name="name" v-validate="'required'" value="{{ $get_name[1] }}" data-vv-as="&quot;First Name&quot;">
                            <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                        </div>

                        <div class="control-group col-md-4 cpass">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="control" name="last_name" value="{{ $get_name[2] or "" }}">
                        
                    </div>
                        
                    </div>
                    
                    <div class="form-field">
                        <div class="control-group pass col-md-6" :class="[errors.has('phone') ? 'has-error' : '']">
                            <label for="phone" class="required">{{ __('shop::app.customer.account.address.create.phone') }}</label>
                            <input type="text" class="control" name="phone" v-validate="'required'" value="{{ $address->phone }}" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.phone') }}&quot;">
                            <span class="control-error" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                        </div>
                        <div class="control-group cpass col-md-6" :class="[errors.has('address') ? 'has-error' : '']">
                            <label for="first_name" class="required">Address</label>
                            <textarea class="control" name="address" v-validate="'required'">{{ $address->address }}</textarea>
                            <span class="control-error" v-if="errors.has('address')">@{{ errors.first('address1') }}</span>
                        </div>

                    </div>

                    <div class="form-field">

                        <div class="control-group pass col-md-3" :class="[errors.has('postcode') ? 'has-error' : '']">
                            <label for="postcode" class="required">{{ __('shop::app.customer.account.address.create.postcode') }}</label>
                            <input type="text" class="control" name="postcode" v-validate="'required'" value="{{ $address->postcode }}" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.postcode') }}&quot;">
                            <span class="control-error" v-if="errors.has('postcode')">@{{ errors.first('postcode') }}</span>
                        </div>
                    <div class="control-group pass col-md-3" :class="[errors.has('country') ? 'has-error' : '']">
                        <label for="country_id">{{ __('shop::app.customer.signup-form.country') }}</label>
                        <input type="text" class="control" name="country" v-validate="'required'" disabled="disabled" value="India">
                        <span class="control-error" v-if="errors.has('country')">@{{ errors.first('country') }}</span>
                    </div>
                    <div class="control-group pass col-md-3" :class="[errors.has('state') ? 'has-error' : '']">
                        <label for="state" class="required">State</label>
                        <?php $states = array();
                        $states = DB::table('country_states')->where('country_id',101)->get();?>
                        <select name="state" id="state" class="control" v-validate="'required'">
                            <option value="">Choose your state...</option>
                            <?php foreach($states as $key => $state) { ?>
                                <option <?php if($state->id == $address->state) echo 'selected'; ?> value="{{ $state->id }}">{{ $state->name }}</option>
                            <?php } ?>
                        </select>
                        <span class="control-error" v-if="errors.has('state')">@{{ errors.first('state_id') }}</span>
                    </div>
                    <div class="control-group pass cpass col-md-3" :class="[errors.has('city') ? 'has-error' : '']">
                        <label for="city" class="required">{{ __('shop::app.customer.signup-form.city') }}</label>
                        <?php $cities = array();
                                $cities = DB::table('country_state_cities')->where('state_id',$address->state)->get();?>
                                <select name="city" id="city" class="control" v-validate="'required'">
                                    <option value="">Choose your city...</option>
                                    <?php foreach($cities as $key => $city) { ?>
                                        <option <?php if($city->id == $address->city) echo 'selected'; ?> value="{{ $city->id }}">{{ $city->name }}</option>
                                    <?php } ?>
                                </select>
                        <span class="control-error" v-if="errors.has('city')">@{{ errors.first('city') }}</span>
                    </div>
                </div>


                    <!-- <div class="control-group" :class="[errors.has('address2') ? 'has-error' : '']">
                        <label for="address2">{{ __('shop::app.customer.account.address.create.address2') }}</label>
                        <input type="text" class="control" name="address2" value ="{{ $address->address2 }}">
                        <span class="control-error" v-if="errors.has('address2')">@{{ errors.first('address2') }}</span>
                    </div>

                    @include ('shop::customers.account.address.country-state', ['countryCode' => old('country') ?? $address->country, 'stateCode' => old('state') ?? $address->state]) -->

                    

                    {!! view_render_event('bagisto.shop.customers.account.address.edit_form_controls.after', ['address' => $address]) !!}

                    <div class="button-group">
                        <button class="btn btn-primary btn-lg" type="submit">
                            Update Address
                        </button>
                    </div>
                </div>

            </form>

            {!! view_render_event('bagisto.shop.customers.account.address.edit.after', ['address' => $address]) !!}

        </div>
    </div>
</div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function(){
$('body').on('change','#state', function(){
    
    var stateID = $(this).val(); 
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('customer/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
  });

    $('.content-container').css({
            'background': '#F1F3F6',
        });
});
</script>
@endpush

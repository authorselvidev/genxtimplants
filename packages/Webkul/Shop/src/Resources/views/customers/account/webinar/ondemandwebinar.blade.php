<?php
    function convertYoutube($string) {
        return preg_replace(
            "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
            "<iframe class=\"webinar_frame\" src=\"https://www.youtube-nocookie.com/embed/$2?rel=0\" title=\"GenXT Implants\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
            $string
        );
    }
?>




@extends('shop::layouts.master')

@section('page_title')
    On-Demand Webinars
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">
    <div class="account-content">
        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">

            <div class="account-head mb-10">
                <span class="back-icon">
                    <a href="{{ route('customer.account.index') }}">
                        <i class="icon angle-left-icon back-link"></i>
                    </a>
                </span>
                <span class="account-heading">
                    On-Demand Webinars
                </span>
                <div class="horizontal-rule"></div>
            </div>

            <div class="account-items-list">

                @foreach($category as $cat)
                    <accordian :title="'{{$cat->category}}'">
                        <div slot="body">
                            <div class="form-field col-md-12 video-items">
                                <?php
                                    $videos = DB::table("webinars")->where('is_live', 0)->where('is_active', 1)->where('category', $cat->category)->get();
                                ?>
                                @foreach($videos as $video)
                                    {!! convertYoutube($video->video_url) !!}
                                    <h3>{{$video->name}}</h3>
                                    <span>{{$video->description}}</span>
                                    <hr>
                                @endforeach
                                <div>For questions or any queries please Whatsapp on <a href="tel:+918484088331">8484088331</a></div>
                            </div>
                        </div>
                    </accordian>
                @endforeach
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('scripts')
    <style type="text/css">
        .webinar_frame{
            height:60vh;
            padding: 10px 0;
        }

        .video-items{
            padding: 10px 40px;
            display: flex;
            flex-direction: column;
        }

        .account-items-list{
            height: 700px;
            overflow-y: scroll;
        }

        @media(max-width:600px){
            .video-items{
                padding: 0 0 !important;
            }
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        });
    </script>
@endpush

@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.profile.index.title') }}
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">
<div class="account-content">

    @include('shop::customers.account.partials.sidemenu')

    <div class="account-layout">

        <div class="account-head">

            <span class="back-icon"><a href="{{ route('customer.account.index') }}"><i class="icon icon-menu-back"></i></a></span>

            <span class="account-heading">{{ __('shop::app.customer.account.profile.index.title') }}</span>

            <?php 
            $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
            if(empty($user))
                $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
                
                $total_loyalty_points = DB::table('reward_points as rp')
                                        ->where('rp.points_credited',1)
                                        ->where('rp.points_expired',0)
                                        ->where('rp.user_id',auth()->guard('customer')->user()->id)
                                        ->sum('reward_points');

                $used_loyalty_points = DB::table('orders')
                                        ->where('customer_id',auth()->guard('customer')->user()->id)
                                        ->sum('reward_points_used');
                $available_points=0;
                if($total_loyalty_points > 0)                       
                    $available_points = abs($total_loyalty_points - $used_loyalty_points);

                $total_sales = DB::table('orders')->where('customer_id', $user->id)->where('status','completed')->sum('grand_total');
            ?>
            <span class="loyalty-points">
                <span class="account">Your Reward Points:</span> <span>{{$available_points}}</span>
                
            </span>

            <div class="horizontal-rule"></div>
        </div>

         {!! view_render_event('bagisto.shop.customers.account.profile.view.before', ['customer' => $customer]) !!}

        <div class="account-table-content">
            <span class="account-action"><a href="{{ route('customer.profile.edit') }}">{{ __('shop::app.customer.account.profile.index.edit') }}</a></span>
            <div class="profile_sec">
				<p><label>Full Name</label> {{ $customer->first_name }} {{ $customer->last_name }}</p>
				<p><label>{{ __('shop::app.customer.account.profile.email') }}</label> {{ $customer->email }}</p>
				<p><label>Phone Number</label> {{ $customer->phone_number }}</p>
			@if($customer->role_id == 3)
				<p><label>{{ __('shop::app.customer.signup-form.clinicname') }}</label> {{ $customer->clinic_name }}</p>
				<p><label>Clinic Registered Number</label> {{ $customer->clinic_number }}</p>
				<p><label>Clinic Address</label> {{ $customer->clinic_address }}</p>
			@endif
				<p><label>Country</label> India</p>
				<p><label>State</label> {{ Webkul\Customer\Models\States::GetStateName($customer->state_id) }}</p>
				<p><label>City</label> {{ Webkul\Customer\Models\Cities::GetCityName($customer->city_id) }}</p>
				<p><label>Pincode</label> {{ $customer->pin_code }}</p>
            @if($customer->role_id == 3 && !empty($customer->dealer_id))
                <?php $dealer = DB::table('users')->where('id', $customer->dealer_id)->first(); ?>
                <p><label>Dealer Name</label>{{ $dealer->my_title }} {{ $dealer->first_name }} {{ $dealer->last_name }}</p>
            @endif
				<p><label>Group Name</label> {{ Webkul\Customer\Models\CustomerGroup::CustomerGroupName($customer->customer_group_id) }}</p>
				<p><label>Group Points (Order Total)</label> {{ Webkul\Customer\Models\CustomerGroup::CustomerGroupAmountRange($customer->customer_group_id) }}</p>
                <p><label>Total Sales</label> <i class="fa fa-inr"></i> {{ number_format($total_sales,2) }}</p>
			{{-- @if ($customer->subscribed_to_news_letter == 1)
				<p><label> {{ __('shop::app.footer.subscribe-newsletter') }}</label> <a class="btn btn-sm btn-primary" href="{{ route('shop.unsubscribe', $customer->email) }}">{{ __('shop::app.subscription.unsubscribe') }} </a></p>
			@endif --}}
			</div>
        </div>

         {!! view_render_event('bagisto.shop.customers.account.profile.view.after', ['customer' => $customer]) !!}

    </div>
</div>
</div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        });
    </script>
@endpush

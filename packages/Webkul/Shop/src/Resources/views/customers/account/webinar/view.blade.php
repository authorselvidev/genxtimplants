@extends('shop::layouts.master')

@section('page_title')
    My Webinars
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">
    <div class="account-content">
        <div class="account-layout" style="width: 100%;">
            <div class="account-items-list">
                <?php
                    $params = '';
                    foreach($config as $key=>$value){
                        $params .= $key.'='.$value.'&';
                    }
                    $params = trim($params, '&');                    
                ?>
                <iframe src="{{route('webinar.zoomview.live').'?'.$params}}" width="1024" height="600" style="border:none"></iframe>
                <div style="text-align:center;">
                    For questions or any queries please Whatsapp on <a href="tel:+918484088331">8484088331</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        });
    </script>
@endpush

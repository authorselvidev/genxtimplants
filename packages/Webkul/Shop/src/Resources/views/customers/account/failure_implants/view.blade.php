@extends('shop::layouts.master')

@section('page_title')
    Return Implants
@stop

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">

        <div class="account-content">

            @include('shop::customers.account.partials.sidemenu')

            <div class="account-layout">
                <div class="account-head mb-10">
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                    <span class="account-heading">Return Implant</span>
                    @if($failed_implant->status == 'Pending' || $failed_implant->status == 'Approved' || $failed_implant->status == 'Processing')
                        <a href="{{route('failed-implants.print', $failed_implant->id)}}" class="btn btn-primary" style="float: right;">Print</a>
                        <br>
                    @endif
                </div>

                <?php
                    $status = array('0'=>'False', '1'=>'True');
                ?>

                <tabs>
                    <tab name="Order Details" :selected="true">
                        <div class="table_content">
                            <table>
                                <tr>
                                    <td class="title">Return Implant status</td>
                                    <td class="text-center">:</td>
                                    <td class="value">
                                        {{$failed_implant->status}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="title">GenXT Comments</td>
                                    <td class="text-center">:</td>
                                    <td class="value">
                                        <span>{{$failed_implant->comments}}</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Order Mode:</td>
                                    <td class="text-center">:</td>
                                    <td class="value"> {{($failed_implant->order_type =='on')?'Online':'Offline'}} </td>
                                </tr>

                                <tr>
                                    <td class="title">Item Status</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{($failed_implant->item_status==0)?'Used':'Unused'}}</td>
                                </tr>                                
 
                                <tr>
                                    <td class="title">Replacement Reason</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->replacement_reason ?? ''}}</td>
                                </tr>                                                                

                                <tr>
                                    <td class="title">Product Code(or label)</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->implant_code}}</td>
                                </tr>

                                <tr>
                                    <td class="title">LOT #</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->lot}}</td>
                                </tr>

                            </table>
                            <hr>
                            <h4 style="font-weight: bold">Return Items</h4>
                            @if($failed_implant->order_type =='on')
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Order ID</th>
                                            <th scope="col">Return Product</th>
                                            <th scope="col">Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($return_implants_return as $impnt)
                                            <tr>
                                                <td scope="row">#{{$impnt->order}}</td>
                                                
                                                <td>{{ DB::table('order_items')->where('id', $impnt->item)->value('name') }}</td>
                                                <td>{{$impnt->quantity}}</td>
                                            </tr>   
                                        @endforeach                                                                     
                                    </tbody>                                    
                                </table>
                            @else
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Order number</th>
                                            <th scope="col">Return Product</th>
                                            <th scope="col">Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($return_implants_return as $impnt)
                                            <tr>
                                                <td scope="row">#{{$impnt->order}}</td>                                                
                                                <td>{{ $impnt->item }}</td>
                                                <td>{{$impnt->quantity}}</td>
                                            </tr>   
                                        @endforeach                                                                     
                                    </tbody>                                    
                                </table>
                            @endif
                            <hr>
                            <h4 style="font-weight: bold">Replacement Items</h4>
                            
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Replacement Item</th>
                                        <th scope="col">Replace Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    @foreach ($return_implants_replace as $impnt)
                                        <tr>
                                            <td scope="row">{{  \DB::table('product_flat')->where('product_id',$impnt->item)->value('name')}}</td>
                                            <td>{{$impnt->quantity}}</td>
                                        </tr>   
                                    @endforeach                                                                     
                                </tbody>                                    
                            </table>
                            
                        </div>
                    </tab>
                </tabs>
            </div>
        </div>

    </div>
</div>
@endsection

@push('scripts')
  <style type="text/css">
    .angle-left-icon{
        width: 20px !important;
        height: 20px !important;
        margin-right: 10px;
    }

    .table_content{
        margin-left: 20px;
    }

    .down_btn{
        font-size: 1.3rem;
    }

    .back-link{
        cursor: pointer;
    }
    h4{
        color: #0995CD;
    }
  </style>
  <script type="text/javascript">
        $('.content-container').css({
            'background': '#F1F3F6',
        });
  </script>
@endpush
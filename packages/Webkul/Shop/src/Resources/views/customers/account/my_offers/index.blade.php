@extends('shop::layouts.master')

@section('page_title')
    My Offers
@stop

<?php
    function formatNumber($num){
        $num = number_format(abs($num), 2, '.', ""); 
        $num = explode('.', $num);
        $new = array();
        for($i=strlen($num[0])-1,$pos=3; $i>=0; $i--){
            array_unshift($new, $num[0][$i]);
            if($i==(strlen($num[0]) - $pos) && $i!=0){
                array_unshift($new, ',');
                $pos+=2;
            }else if($i==0){
                array_unshift($new, '₹');
            }
        }
        $num[0] = join('', $new);
        return join('.', $num);
    };
?>

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">

        <div class="account-content">

            @include('shop::customers.account.partials.sidemenu')

            <div class="account-layout">
                <div class="account-head mb-10">
                    <span class="back-icon"><a href="{{ route('customer.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
                    <span class="account-heading">My Offers</span>
                    <span></span>
                    <br>
                </div>

                <tabs>
                    @if($promo_codes->count()) 
                        <tab name="{{ __('Promo Code') }}" :selected="true">
                            <div class="myoffers-container">
                                @foreach($promo_codes as $promo)
                                    <div class="promo-code-card">

                                        <div class="card-group">
                                            <span class="card-sub-title">Discount Name</span>
                                            <span class="card-name"><strong>{{$promo->discount_title}}</strong></span>
                                        </div>


                                        <div class="card-group">
                                            <span class="card-sub-title">Min Order Amount</span>
                                            <span class="card-amount"><strong>INR {{formatNumber($promo->minimum_order_amount)}}</strong></span>
                                        </div>

                                        <div class="card-group">
                                            <?php
                                                $discount_percent  = array_unique(\DB::table('discount_category')->where('discount_id','=', $promo->id)->get()->pluck('discount')->toArray());
                                                $discount_percent = array_map(function($val){ return $val.'%'; },$discount_percent);
                                            ?>
                                            <span class="card-sub-title">Discount: <strong>{{ join(', ', $discount_percent) }}</strong></span>
                                        </div>

                                        <div class="card-group">
                                            <span class="card-sub-title">Usage Limit (Per User): <strong>{{ $used_promo[$promo->id] ?? '0' }} out of  {{ $promo->discount_usage_limit }} times</strong></span>
                                        </div>

                                        <?php
                                            $arr = DB::table('discount_category')->select('category_id')->where('discount_id','=', $promo->id)->get()->all();
                                            $newArr = array();
                                            foreach($arr as $item => $val){
                                                array_push($newArr, $val->category_id);
                                            }

                                            $cats = DB::table('category_translations')->whereIn('id', $newArr)->get()->all();
                                        ?>

                                        <div class="card-group">
                                            <a href="#" class="view_cat">+ More</a>
                                            <div class="wrapper_class" style="display: none">
                                                    <div class="container">
                                                    <span class="close">&times;</span>
                                                        <div class="sub_container">
                                                            <h4><strong>{{$promo->discount_title}}</strong></h4>
                                                            @if(strlen($promo->discount_description))
                                                                <div class="card-group">
                                                                    <span class="card-sub-title"><strong>Discount Description</strong></span>
                                                                    <span class="card-desc">{{$promo->discount_description}}</span>
                                                                </div>
                                                            @endif
                                                            <span class="card-sub-title"><strong>Applicable Categories</strong></span><br>
                                                            <span class="coupon-cat">
                                                                <?php
                                                                    $lastElement = end($cats);
                                                                    foreach($cats as $cat){
                                                                        if($cat == $lastElement){
                                                                            echo($cat->name.'.');
                                                                        }else{
                                                                            echo($cat->name.', ');
                                                                        }
                                                                    }
                                                                ?>
                                                            </span>
                                                        </div>
                                                    </div>                                                    
                                            </div>
                                        </div>


                                        <div class="card-group">
                                            <span class="card-sub-title">Discount Code</span>
                                            <div class="coupon-code text-center">{{$promo->coupon_code}}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="promo_paginate">
                                {{$promo_codes->links()}}
                            </div>
                        </tab>
                    @endif

                    @if (count($member_ship))
                        <tab name="{{ __('Membership Discount') }}">
                            <div class="myoffers-container">
                                @foreach($member_ship as $key => $discount)
                                    <div class="promo-code-card">
                                        @if($discount["customer_group_id"]== $customer->customer_group_id)
                                            <div class="card_active">
                                                <img src="{{asset('themes/default/assets/images/crown.svg')}}">
                                            </div>
                                        @endif
                                        <div class="card-group">
                                            <span class="card-sub-title">Discount Name</span>
                                            <span class="card-name"><strong>{{$discount["discount_title"]}}</strong></span>
                                        </div>

                                        <div class="card-group">
                                            <span class="card-sub-title">Min Order Amount</span>
                                            <span class="card-amount"><strong>INR {{formatNumber($discount["minimum_order_amount"])}}</strong></span>
                                        </div>

                                        <div class="card-group">
                                            <?php
                                                $discount_percent  = array_unique(\DB::table('discount_category')->where('discount_id','=', $discount["id"])->get()->pluck('discount')->toArray());
                                                $discount_percent = array_map(function($val){ return $val.'%'; },$discount_percent);
                                            ?>
                                            <span class="card-sub-title">Discount: <strong>{{ join(', ', $discount_percent) }}</strong></span>
                                        </div>

                                        <div class="card-group">
                                            <span class="card-sub-title">Group: <strong>{{ Webkul\Customer\Models\CustomerGroup::CustomerGroupName($discount["customer_group_id"]) }}</strong></span>
                                        </div>

                                        <div class="card-group">
                                            <span class="card-sub-title">Group Range: <strong>{{ Webkul\Customer\Models\CustomerGroup::CustomerGroupAmountRange($discount["customer_group_id"]) }}</strong></span>
                                        </div>

                                        {{-- <div class="card-group">
                                            <span class="card-sub-title">Discount: <strong>{{ \DB::table('discount_category')->where('discount_id','=', $discount->id)->first()->discount }}%</strong></span>
                                        </div> --}}

                                        <?php
                                            $arr = DB::table('discount_category')->select('category_id')->where('discount_id','=', $discount["id"])->get()->all();
                                            $newArr = array();
                                            foreach($arr as $item => $val){
                                                array_push($newArr, $val->category_id);
                                            }

                                            $cats = DB::table('category_translations')->whereIn('id', $newArr)->get()->all();
                                        ?>

                                        <div class="card-group">
                                            <a href="#" class="view_cat">+ More</a>
                                            <div class="wrapper_class" style="display: none">
                                                    <div class="container">
                                                    <span class="close">&times;</span>
                                                        <div class="sub_container">
                                                            <h4><strong>{{$discount["discount_title"]}}</strong></h4>
                                                            @if(strlen($discount["discount_description"]))
                                                                <div class="card-group">
                                                                    <span class="card-sub-title"><strong>Discount Description</strong></span>
                                                                    <span class="card-desc">{{$discount["discount_description"]}}</span>
                                                                </div>
                                                            @endif
                                                            <span class="card-sub-title"><strong>Applicable Categories</strong></span><br>
                                                            <span class="coupon-cat">
                                                                <?php
                                                                    $lastElement = end($cats);
                                                                    foreach($cats as $cat){
                                                                        if($cat == $lastElement){
                                                                            echo($cat->name.'.');
                                                                        }else{
                                                                            echo($cat->name.', ');
                                                                        }
                                                                    }
                                                                ?>
                                                            </span>
                                                        </div>
                                                    </div>                                                    
                                            </div>
                                        </div>

                                        <div class="card-group">
                                            <span class="card-sub-title">Discount Code</span>
                                            <div class="coupon-code text-center">{{$discount["coupon_code"]}}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </tab>
                    @endif
                </tabs>
            </div>
        </div>

    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.view_cat').click(function(e){
                $(this).siblings('.wrapper_class').fadeIn(200);
                e.preventDefault();
            });

            $('.wrapper_class').click(function(e){
                if($(e.target).hasClass('wrapper_class') || $(e.target).hasClass('close')){
                    $('.wrapper_class').fadeOut(100, function(){
                        $(this).hide();
                    });
                }
            });

            $('.content-container').css({
                'background': '#F1F3F6',
            });
        });
        
    </script>
@endpush

@push('scripts')
  <style type="text/css">
        .promo-code-card{
            position: relative;
        }
        .card_active{
            position: absolute;
            right: 15px;
            top:15px;
        }

        .card_active img{
            width: 30px;
        }
  </style>
@endpush

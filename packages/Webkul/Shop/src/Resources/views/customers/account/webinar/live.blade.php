<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.9.9/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.9.9/css/react-select.css" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="origin-trial" content="">
</head>

<body>
    <script src="https://source.zoom.us/1.9.9/lib/vendor/react.min.js"></script>
    <script src="https://source.zoom.us/1.9.9/lib/vendor/react-dom.min.js"></script>
    <script src="https://source.zoom.us/1.9.9/lib/vendor/redux.min.js"></script>
    <script src="https://source.zoom.us/1.9.9/lib/vendor/redux-thunk.min.js"></script>
    <script src="https://source.zoom.us/1.9.9/lib/vendor/lodash.min.js"></script>
    <script src="https://source.zoom.us/zoom-meeting-1.9.9.min.js"></script>
    <script type="text/javascript" src="{{asset('themes/default/assets/js/tool.js')}}"></script>
    <script type="text/javascript" src="{{asset('themes/default/assets/js/vconsole.min.js')}}"></script>
    <script type="text/javascript">
		window.addEventListener('DOMContentLoaded', function(event) {
		    console.log('DOM fully loaded and parsed');
		    websdkready();
		});

		function websdkready() {
		    var testTool = window.testTool;
		    // get meeting args from url
		    var tmpArgs = testTool.parseQuery();
		    var meetingConfig = {
		        apiKey: tmpArgs.apiKey,
		        meetingNumber: tmpArgs.mn,
		        userName: (function() {
		            if (tmpArgs.name) {
		                try {
		                    return testTool.b64DecodeUnicode(tmpArgs.name);
		                } catch (e) {
		                    return tmpArgs.name;
		                }
		            }
		            return (
		                "CDN#" +
		                tmpArgs.version +
		                "#" +
		                testTool.detectOS() +
		                "#" +
		                testTool.getBrowserInfo()
		            );
		        })(),
		        passWord: tmpArgs.pwd,
		        leaveUrl: "{{route('webinar.zoomview.leavemeating')}}",
		        role: parseInt(tmpArgs.role, 10),
		        userEmail: (function() {
		            try {
		                return testTool.b64DecodeUnicode(tmpArgs.email);
		            } catch (e) {
		                return tmpArgs.email;
		            }
		        })(),
		        lang: tmpArgs.lang,
		        signature: tmpArgs.signature || "",
		        china: tmpArgs.china === "1",
		    };

		    // a tool use debug mobile device
		    if (testTool.isMobileDevice()) {
		        vConsole = new VConsole();
		    }
		    console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

		    if (meetingConfig.china)
		        ZoomMtg.setZoomJSLib("https://jssdk.zoomus.cn/1.9.9/lib", "/av"); // china cdn option
		    ZoomMtg.preLoadWasm();
		    ZoomMtg.prepareJssdk();

		    function beginJoin(signature) {
		        ZoomMtg.init({
		            leaveUrl: meetingConfig.leaveUrl,
		            webEndpoint: meetingConfig.webEndpoint,
		            success: function() {
		                console.log(meetingConfig);
		                console.log("signature", signature);
		                ZoomMtg.i18n.load(meetingConfig.lang);
		                ZoomMtg.i18n.reload(meetingConfig.lang);
		                ZoomMtg.join({
		                    meetingNumber: meetingConfig.meetingNumber,
		                    userName: meetingConfig.userName,
		                    signature: signature,
		                    apiKey: meetingConfig.apiKey,
		                    userEmail: meetingConfig.userEmail,
		                    passWord: meetingConfig.passWord,
		                    success: function(res) {
		                        console.log("join meeting success");
		                        console.log("get attendeelist");
		                        ZoomMtg.getAttendeeslist({});
		                        ZoomMtg.getCurrentUser({
		                            success: function(res) {
		                                console.log("success getCurrentUser", res.result.currentUser);
		                            },
		                        });
		                    },
		                    error: function(res) {
		                        console.log(res);
		                    },
		                });
		            },
		            error: function(res) {
		                console.log(res);
		            },
		        });

		        ZoomMtg.inMeetingServiceListener('onUserJoin', function(data) {
		            console.log('inMeetingServiceListener onUserJoin', data);
		        });

		        ZoomMtg.inMeetingServiceListener('onUserLeave', function(data) {
		            console.log('inMeetingServiceListener onUserLeave', data);
		        });

		        ZoomMtg.inMeetingServiceListener('onUserIsInWaitingRoom', function(data) {
		            console.log('inMeetingServiceListener onUserIsInWaitingRoom', data);
		        });

		        ZoomMtg.inMeetingServiceListener('onMeetingStatus', function(data) {
		            console.log('inMeetingServiceListener onMeetingStatus', data);
		        });
		    }

		    beginJoin(meetingConfig.signature);
		};
    </script>

    <script>
        const simd = async () => WebAssembly.validate(new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0, 1, 4, 1, 96, 0, 0, 3, 2, 1, 0, 10, 9, 1, 7, 0, 65, 0, 253, 15, 26, 11]))
        simd().then((res) => {
          console.log("simd check", res);
        });
    </script>
</body>

</html>
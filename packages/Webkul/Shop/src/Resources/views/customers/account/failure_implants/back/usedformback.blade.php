@extends('shop::layouts.master')

@section('page_title')
    Return Implants
@stop

@section('content-wrapper')
<div class="profile-page">
        <div class="main-container-wrapper">

    <div class="account-content">

        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">
            <div class="account-head mb-10">
                <span class="back-icon"><a href="{{ route('customer.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
                <span class="account-heading">Used Return Implants</span>
                <span></span>
            </div>
            <form method="post" id="form_failure" action="{{route('failure.implants.usedsend')}}" files="true" enctype="multipart/form-data" >
                @csrf
                <input type="hidden" value="1" name="formtype" id="formtype">
                <div class="form-fieldss failure-form col-md-12">
                    <section>
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="disabled active" style="pointer-events: none;">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                        <span class="round-tab">
                                            Personal Details
                                        </span>
                                    </a>
                                </li>

                                <li class="disabled perhide" style="pointer-events: none;">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                        <span class="round-tab">
                                           Failure Details
                                        </span>
                                    </a>
                                </li>

                                <li class="disabled perhide" style="pointer-events: none;">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                                        <span class="round-tab">
                                            History Details
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <form role="form">
                            <input type="hidden" name="item_status"value="0">
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <h3>Step 1</h3>
                                    <hr>
                                            <div class="form-field col-md-12">
                                                <div class="control-group col-md-126 pass cpass">
                                                    <label for="email" class="required">Email</label>
                                                    <input type="email" name="email" class="form-control form-required" value="{{ $user_details->email }}" placeholder="Your answer">
                                                    <span class="error-msg text-danger"></span>
                                                </div>
                                            </div>
                                            <div class="form-field col-md-12">
                                                    <div class="control-group">
                                                        <label for="first_name" class="required">Doctor's Name</label>
                                                        <input type="text" name="doctor_name" class="form-control form-required" value="{{ $user_details->first_name.' '.$user_details->last_name }}" placeholder="Your answer">
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>
                                            <div class="form-field col-md-12">
                                                    <div class="control-group">
                                                        <label for="last_name" class="required">Mobile Number</label>
                                                        <input type="text" name="mobile_number" class="form-control form-required" value="{{ $user_details->phone_number }}">
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>
                                            <div class="form-field col-md-12">
                                                    <div class="control-group">
                                                        <label for="last_name" class="required">Detailed Clinic Address</label>
                                                        <textarea class="form-control form-required" name="clinic_address" rows="4" placeholder="Your answer">{{ $user_details->clinic_address }}</textarea>
                                                        <span class="error-msg text-danger"></span>
                                                    </div>

                                            </div>
                                            <div class="order_type">
                                                <label style="margin-left: 15px;">Order Mode:</label>
                                                <div class="form-field col-md-12" style="margin-bottom: 10px;">
                                                    <input type="radio" name="order_type" value="on" id="online" checked> <label for="online">Online</label>
                                                    <input type="radio" name="order_type" value="off" id="offline"> <label for="offline">Offline</label>
                                                </div>
                                                <div class="form-field col-md-12 online_order activeOrder">
                                                        <div class="col-md-6 pright" style="padding: 0px;">
                                                            <div class="control-group order_control">
                                                                <label for="order_item" class="required">Order Item</label>
                                                                <select class='form-control' name="order_item" id="order_item" placeholder="Select a Order...">
                                                                    <option value="">Select a Order...</option>
                                                                    @foreach($orders as $order)
                                                                        @foreach($order->items as $item)
                                                                            <?php
                                                                                $invoice_items_id = DB::table('invoice_items')->where('order_item_id', DB::table('order_items')->where('id', $item->id)->first()->id)->first();
                                                                                $lot_items = ($invoice_items_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_items_id->id)->addSelect('lot_number')->get()):[];
                                                                                $lot_string = [];

                                                                                if(count($lot_items)>0){
                                                                                    foreach($lot_items as $lot)
                                                                                        array_push($lot_string, $lot->lot_number);
                                                                                }
                                                                                $lot_string = join($lot_string,', ');
                                                                            ?>
                                                                            <option data-data='{"count":<?php echo($item->qty_ordered) ?>, "lot_number": "<?php echo($lot_string) ?>" }'  value="{{$item->id}}">Order #{{$order->id}} - {{$item->name}}</option>
                                                                        @endforeach
                                                                    @endforeach
                                                                </select>
                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" style="padding: 0px;">
                                                            <div class="control-group">
                                                                <label for="order_item" class="required">Replacement Item</label>
                                                                <select class='form-control' name="replacement_item_online" id="replacement_item_online" placeholder="Select a Item...">
                                                                    <option value="">Select a Item...</option>
                                                                    @foreach($products as $key => $product)
                                                                        <?php
                                                                            $product_ordered_inventory = DB::table('product_ordered_inventories')->where('product_id',$product->product_id)->first();
                                                                            $product_ordered_qty = (isset($product_ordered_inventory)) ? $product_ordered_inventory->qty : 0;
                                                                            $product_available_qty = $product->qty - $product_ordered_qty;
                                                                        ?>
                                                                        @if($product_available_qty > 0)
                                                                            <option data-data='{"product_id":<?php echo($product->product_id) ?>, "qty_available": <?php echo($product_available_qty) ?>}' value="{{$product->product_id}}">{{$product->name}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>

                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 qty_count pright" style="padding: 0px;">
                                                            <div class="control-group">
                                                                <label for="online_qty" class="required">Order Quantity</label>
                                                                <input type="number" min="1" name="order_qty" id="online_qty" class="form-control form-active" value="" placeholder="Qty" readonly>
                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 qty_count" style="padding: 0px;">
                                                            <div class="control-group">
                                                                <label for="replace_qty" class="required">Replace Quantity</label>
                                                                <input type="number" min="1" name="replace_qty" id="replace_qty" class="form-control form-active" value="" placeholder="Qty">
                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="form-field col-md-12 offline_order" style="display: none">
                                                    <div class="col-md-6 pright" style="padding: 0px; ">
                                                        <div class="control-group">
                                                            <label for="order_number" class="required">Order number:</label>
                                                            <input type="text" name="order_number" class="form-control form-active" value="" placeholder="Order number">
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 qty_count" style="padding: 0px;">
                                                        <div class="control-group">
                                                            <label for="product_name" class="required">Product Name:</label>
                                                            <input type="text" name="product_name" id="product_name" class="form-control form-active" placeholder="Product name">
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 pright" style="padding: 0px;">
                                                        <div class="control-group">
                                                            <label for="order_item" class="required">Replacement Item</label>
                                                            <select class='form-control' name="replacement_item_offline" id="replacement_item_offline" placeholder="Select a Item...">
                                                                <option value="">Select a Item...</option>
                                                                @foreach($products as $key => $product)
                                                                    <?php
                                                                        $product_ordered_inventory = DB::table('product_ordered_inventories')->where('product_id',$product->product_id)->first();
                                                                        $product_ordered_qty = (isset($product_ordered_inventory)) ? $product_ordered_inventory->qty : 0;
                                                                        $product_available_qty = $product->qty - $product_ordered_qty;
                                                                    ?>
                                                                    @if($product_available_qty > 0)
                                                                        <option data-data='{"product_id":<?php echo($product->product_id) ?>, "qty_available": <?php echo($product_available_qty) ?>}' value="{{$product->product_id}}">{{$product->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 qty_count" style="padding: 0px;">
                                                        <div class="control-group">
                                                            <label for="offline_qty" class="required">Order Quantity</label>
                                                            <input type="number" min="1" name="order_qty_offline" id="offline_qty" class="form-control form-active" value="" placeholder="Qty">
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>   

                                                    
                                                    
                                                    <div class="col-md-12" style="padding: 0px;clear:both">
                                                        <div class="control-group">
                                                            <label for="order_item" class="required">Replace Quantity</label>
                                                            <input type="number" min="1" name="replace_offline_qty" id="replace_offline_qty" class="form-control form-active" value="" placeholder="Qty">
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-field col-md-12">
                                                <div class="control-group cpass">
                                                    <label for="last_name" class="required">Reason for Replacement</label>
                                                    <input type="text" name="replacement_reason" class="form-control form-required" value="" placeholder="Your answer">
                                                    <span class="error-msg text-danger"></span>
                                                </div>
                                            </div>

                                            <div class="form-field col-md-12">
                                                    <div class="control-group cpass">
                                                        <label for="last_name" class="required">GenXT Implant Product Code(or label)</label>
                                                        <input type="text" name="implant_code" class="form-control form-required" value="" placeholder="Your answer">
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>
                                            <div class="form-field col-md-12">
                                                    <div class="control-group cpass">
                                                        <label for="last_name" class="required">LOT #</label>
                                                        <input type="text" name="lot" id="lot" class="form-control form-required" value="" placeholder="Your answer">
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>
                                            <input type="hidden" class="form1-validation-count" value="0">
                                    <ul class="list-inline pull-right">
                                        <li><button type="button" class="btn btn-primary next-step perhide first_page">Next</button></li>
                                    </ul>
                                    <ul class="list-inline pull-right pershow" style="display: none">
                                        <li><button type="button" class="btn btn-primary btn-info-full">
                                            <span>Submit</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="step2">
                                    <h3>Step 2</h3>
                                    <hr>
                                    <div class="form-field col-md-12">
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="email" class="required">Bone Type</label>
                                            <select name="bone_type" class="form-control form-required">
                                                <option value="">Please select bone type</option>
                                                <option value="I">I</option>
                                                <option value="II">II</option>
                                                <option value="III">III</option>
                                                <option value="IV">IV</option>
                                            </select>
                                             <span class="error-msg text-danger"></span>
                                        </div>
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="email" class="required">Flapless</label>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="flapless" class="form-required" value="Yes"> Yes</label>
                                            </div>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="flapless" class="form-required" value="No"> No</label>
                                            </div>
                                             <span class="error-msg-flapless text-danger"></span>
                                        </div>
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required">Immediate Implant </label>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="immediate_implant" value="Yes"> Yes</label>
                                            </div>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="immediate_implant" value="No"> No</label>
                                            </div>
                                             <span class="error-msg-immediate-implant text-danger"></span>
                                        </div>
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required">Immediate Load </label>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="immediate_load" value="Yes"> Yes</label>
                                            </div>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="immediate_load" value="No"> No</label>
                                            </div>
                                             <span class="error-msg-immediate-load text-danger"></span>
                                        </div>
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required">Implantation Date </label>
                                            <date>
                                                <input type="text" name="implant_date" class="form-control form-required date-start">
                                            </date>
                                             <span class="error-msg text-danger"></span>
                                        </div>
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required">Implant Removal Date</label>
                                                <date>
                                                    <input type="text" name="implant_removal_date" class="form-control form-required">
                                                </date>
                                             <span class="error-msg text-danger"></span>
                                        </div>
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required">Implant Removal Location</label>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="remove_location" value="Anterior Mandible"> Anterior Mandible</label>
                                            </div>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="remove_location" value="Posterior Mandible"> Posterior Mandible</label>
                                            </div>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="remove_location" value="Anterior Maxilla"> Anterior Maxilla</label>
                                            </div>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="remove_location" value="Posterior Maxilla"> Posterior Maxilla</label>
                                            </div>
                                             <span class="error-msg-remove-location text-danger"></span>
                                        </div>
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required">Reasons for Failure</label>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Mobility"> Mobility</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Infection"> Infection</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Prosthesis broke/decemented"> Prosthesis broke/decemented</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Pain"> Pain</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Bone Loss"> Bone Loss</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Occlusion"> Occlusion</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Overload"> Overload</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Low insertion Torque"> Low insertion Torque</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Implant Fracture"> Implant Fracture</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Granuloma"> Granuloma</label>
                                            </div>
                                            <div class="checkbox-sec">
                                              <label><input type="checkbox" name="reason[]" value="Nerve Impaction/Penetration"> Nerve Impaction/Penetration</label>
                                            </div>
                                             <span class="error-msg-reason text-danger"></span>
                                        </div>
                                        <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="">Other reason for Failure / Case Outcome</label>
                                            <textarea class="form-control" rows="5" name="other_reason_failure" placeholder="Your answer"></textarea>
                                             <span class="error-msg text-danger"></span>
                                        </div>
                                    </div>
                                    <ul class="list-inline pull-right">
                                        <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                        <li><button type="button" class="btn btn-primary next-step second_page">Next</button></li>
                                    </ul>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="step3">
                                    <h3>Step 3</h3>
                                    <hr>
                                         <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required"> Age</label>
                                            <input type="text" name="age" class="form-control form-required" value="">
                                            <span class="error-msg text-danger"></span>
                                        </div>
                                         <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required"> Gender</label>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="gender" value="Male"> Male</label>
                                            </div>
                                            <div class="radio-sec">
                                              <label><input type="radio" name="gender" value="Female"> Female</label>
                                            </div>
                                            <span class="error-msg-gender text-danger"></span>
                                        </div>
                                         <div class="control-group col-md-12 pass cpass">
                                            <label for="" class="required"> History</label>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Yes</th>
                                                        <th>No</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Normal</td>
                                                        <td><label><input type="radio" name="normal_history" value="Yes"></label></td>
                                                        <td><label><input type="radio" name="normal_history" value="No"></label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Smoker</td>
                                                        <td><label><input type="radio" name="smoker_history" value="Yes"></label></td>
                                                        <td><label><input type="radio" name="smoker_history" value="No"></label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hypertension</td>
                                                        <td><label><input type="radio" name="hypertension_history" value="Yes"></label></td>
                                                        <td><label><input type="radio" name="hypertension_history" value="No"></label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cardiac Problems</td>
                                                        <td><label><input type="radio" name="cardiac_problems_history" value="Yes"></label></td>
                                                        <td><label><input type="radio" name="cardiac_problems_history" value="No"></label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Diabetes</td>
                                                        <td><label><input type="radio" name="diabetes_history" value="Yes"></label></td>
                                                        <td><label><input type="radio" name="diabetes_history" value="No"></label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alcoholism</td>
                                                        <td><label><input type="radio" name="alcoholism_history" value="Yes"></label></td>
                                                        <td><label><input type="radio" name="alcoholism_history" value="No"></label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Trauma</td>
                                                        <td><label><input type="radio" name="trauma_history" value="Yes"></label></td>
                                                        <td><label><input type="radio" name="trauma_history" value="No"></label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cancer</td>
                                                        <td><label><input type="radio" name="cancer_history" value="Yes"></label></td>
                                                        <td><label><input type="radio" name="cancer_history" value="No"></label></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <span class="error-msg-history text-danger"></span>
                                         </div>
                                         <div class="control-group col-md-12 pass cpass">
                                             <label for="" class="required"> Others (Specify) </label>
                                             <textarea rows="5" class="form-control form-required" name="others" placeholder="Your answer"></textarea>
                                             <span class="error-msg text-danger"></span>
                                         </div>
                                         <div class="control-group col-md-12 pass cpass">
                                             <label for="" class="required"> Attach Image (Xray of the implant when placed) </label>
                                             <input type="file" name="xray_implant" class="form-control form-required img-thumb">
                                             <span class="error-msg text-danger"></span>
                                         </div>
                                         <div class="control-group col-md-12 pass cpass">
                                             <label for="" class="required"> Attach Image (Xray showing the failed implant, just before the implant is removed). </label>
                                             <input type="file" name="before_implant_removed" class="form-control form-required img-thumb">
                                             <span class="error-msg text-danger"></span>
                                         </div>

                                    <ul class="list-inline pull-right">
                                        <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                        <li><button type="button" class="btn btn-primary btn-info-full next-step third_page">
                                            <span>Submit</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </section>
                </div>
            </form>
         </div>

        </div>

    </div>
</div>
<script type="text/javascript">

        function validateForm1(check=null){
            var check_form_valid=true;
            $('#step1 .form-required, .activeOrder #order_item, .activeOrder .form-active, .activeOrder #replacement_item_online, .activeOrder #replacement_item_offline ').each(function(response,key) {
                if ($(this).val() === ''){
                    $(this).parent().find('.error-msg').text('This field is required');
                }else{
                    $(this).parent().find('.error-msg').text('');
                    check_form_valid = ($('.activeOrder input[max]').filter(function () {
                        if(stat=parseInt($(this).val())>parseInt($(this).attr('max'))){
                            $(this).parent().find('.error-msg').text(`Maximum allowed quantity ${$(this).attr('max')}`);
                        }
                        return stat;
                    }).length!=0)?false:check_form_valid;

                    check_form_valid = ($('.activeOrder input[min]').filter(function () {
                        if(stat=parseInt($(this).val())=='0'){
                            $(this).parent().find('.error-msg').text(`Zero not allowed`);
                        }
                        return stat;
                    }).length!=0)?false:check_form_valid;
                }
            });

            check_form_valid = ($('#step1 .form-required, .activeOrder #order_item, .activeOrder .form-active, .activeOrder #replacement_item_online, .activeOrder #replacement_item_offline').filter(function (){return $.trim($(this).val()).length == 0}).length!=0)?false:check_form_valid;
            if (check_form_valid==true&& check==null) {                
                activeTab();
                $(window).scrollTop(0);
            }
            return check_form_valid;
        }

        function validateForm2(argument) {

          $('#step2 .form-required').each(function(response,key) {
            if ( $(this).val() === '' ){
                $(this).parent().find('.error-msg').text('This field is required');
            }
            else{
                $(this).parent().find('.error-msg').text('');
            }
          });

          var check_form_valid= $('#step2 .form-required').filter(function () {
                return $.trim($(this).val()).length == 0
            }).length == 0;
             var flapless_checked=$('[name="flapless"]').is(':checked');
              var immediate_implant_checked=$('[name="immediate_implant"]').is(':checked');
              var immediate_load_checked=$('[name="immediate_load"]').is(':checked');
              var remove_location_checked=$('[name="remove_location"]').is(':checked');
              var reason_checked=$('[name="reason[]"]').is(':checked');

            if (!flapless_checked) {
                $('.error-msg-flapless').text('This field is required');
            }
            else{
                $('.error-msg-flapless').text('');
            }

            if (!immediate_implant_checked) {
                $('.error-msg-immediate-implant').text('This field is required');
            }
            else{
                $('.error-msg-immediate-implant').text('');
            }

            if (!immediate_load_checked) {
                $('.error-msg-immediate-load').text('This field is required');
            }
            else{
                $('.error-msg-immediate-load').text('');
            }

            if(!remove_location_checked){
                $('.error-msg-remove-location').text('This field is required');
            }
            else{
                $('.error-msg-remove-location').text('');
            }
            if(!reason_checked){
                $('.error-msg-reason').text('This field is required');
            }
            else{
                $('.error-msg-reason').text('');
            }

            if (check_form_valid && flapless_checked && immediate_implant_checked && immediate_load_checked && remove_location_checked && reason_checked) {
                    activeTab();
                    $(window).scrollTop(0);
            }
        }
        function validateForm3() {
          $('#step3 .form-required').each(function(response,key) {
            if ($(this).val() === ''){
                $(this).parent().find('.error-msg').text('This field is required');
            }else{
                $(this).parent().find('.error-msg').text('');
            }
          });

            var normal_history=$('[name="normal_history"]').is(':checked');
            var smoker_history=$('[name="smoker_history"]').is(':checked');
            var hypertension_history=$('[name="hypertension_history"]').is(':checked');
            var cardiac_problems_history=$('[name="cardiac_problems_history"]').is(':checked');
            var diabetes_history=$('[name="diabetes_history"]').is(':checked');
            var alcoholism_history=$('[name="alcoholism_history"]').is(':checked');
            var trauma_history=$('[name="trauma_history"]').is(':checked');
            var cancer_history=$('[name="cancer_history"]').is(':checked');

            if (normal_history && smoker_history && hypertension_history && cardiac_problems_history && diabetes_history && alcoholism_history && trauma_history && cancer_history) {
                var history_status=true;
                 $('.error-msg-history').text('');
            }
            else{
                $('.error-msg-history').text('This field is required');
                var history_status=false;
            }

            if (!$('[name="gender"]').is(':checked')) {
                var gender_status=false
                $('.error-msg-gender').text('This field is required');
            }
            else{
                $('.error-msg-gender').text('');
                var gender_status=true;
            }
            var check_form_valid= $('#step3 .form-required').filter(function () {
                return $.trim($(this).val()).length == 0
            }).length == 0;

            if (check_form_valid && history_status && gender_status) {
                $('button[type="button"]').css('pointer-events','none');
                $('#formtype').val(1);
                $('button[type="button"].third_page').animate({
                    width:'+=20px'
                },100, function(){
                    $(this).append('<i class="spinning" style="display:none"></i>');
                    $('.spinning').fadeIn(100);
                });
                $("#form_failure").submit();
            }
        }

        function activeTab() {

            var $active = $('.failure-form .wizard .nav-tabs li.active');
            nextTab($active);

        }

        $(document).ready(function () {
            $(document).on('click', '.next-step', function(event) {
                var current_tab=$(this).attr('class').split(' ').pop();
                if (current_tab=="first_page") {
                    validateForm1();
                }
                else if(current_tab=="second_page"){
                    validateForm2();
                }
                else if(current_tab=="second_page"){
                    validateForm2();
                }
                else if(current_tab=="third_page"){
                    validateForm3();
                }

            });

            $(document).on('click', '.pershow button', function(event){
                event.preventDefault();
                if(validateForm1(1)){
                    $(this).append('<i class="spinning" style="display:none"></i>');
                    $('.spinning').fadeIn(100);                
                    $('#formtype').val(2);
                    $("#form_failure").submit();                    
                }                
            })

             $(document).on('click', '.prev-step', function(event) {
                var $active = $('.failure-form .wizard .nav-tabs li.active');
                prevTab($active);

            });

            $('.nav-tabs > li a[title]').tooltip();

            //Wizard
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

                var $target = $(e.target);

                if ($target.parent().hasClass('disabled')) {
                    return false;
                }
            });

        });

        function nextTab(elem) {
            $(elem).next().find('a[data-toggle="tab"]').click();
        }
        function prevTab(elem) {
            $(elem).prev().find('a[data-toggle="tab"]').click();
        }

    </script>
@endsection




@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.content-container').css({
                'background': '#F1F3F6',
            });
            
            $('#order_item').selectize({
                sortField: 'text',

                render: {
                    option: function (data, escape) {
                        return `<div class="option" data-count="${escape(data.count)}">${escape(data.text)}</div>`;
                    }
                },

                onChange: function(val) {
                    var data = this.options[val];
                    $('input#lot').val(data.lot_number);                    
                    let qty = $('.qty_count');
                    qty.find('input#online_qty').attr('max',data.count).val(data.count);
                    $('#replace_qty').val(data.count);
                    qty.show();
                }
            });

            $('#replacement_item_online').selectize({
                sortField: 'text',

                render: {
                    option: function (data, escape) {
                        console.log(data);
                        return `<div class="option" data-product_id="${escape(data.product_id)}" data-qty_available="${data.qty_available}">${escape(data.text)}</div>`;
                    }
                },
            });

            $('#replacement_item_offline').selectize({
                sortField: 'text',

                render: {
                    option: function (data, escape) {
                        console.log(data);
                        return `<div class="option" data-product_id="${escape(data.product_id)}" data-qty_available="${data.qty_available}">${escape(data.text)}</div>`;
                    }
                },
            });

            $('form').find('input[name=order_type]').change(function(){
                const stat= $(this).val();
                const order_on = $('.order_type .online_order');
                const order_off = $('.order_type .offline_order');
                $('.perhide').show();
                $('.pershow').hide();
                $('#replace_qty, #replace_offline_qty').val('');
                if(stat=='on'){
                    order_on.addClass('activeOrder').show();
                    order_off.removeClass('activeOrder').hide();
                }else if(stat=='off'){
                    order_on.removeClass('activeOrder').hide();
                    order_off.addClass('activeOrder').show();
                }
            });

            $('.img-thumb').on('change',function(event) {
                let input = event.target;
                let reader = new FileReader();
                reader.onload = function(){
                    let dataURL = reader.result;
                    let output = $(`<img class='imgThumb'>`).hide();
                    output.attr('src', dataURL);
                    $(input).next('.imgThumb').remove();
                    $(input).after(output);
                    output.fadeIn();
                    output.addClass('img-enlargeable').click(function(){
                        var src = $(this).attr('src');
                        var modal;
                        function removeModal(){ modal.remove(); $('body').off('keyup.modal-close'); }
                        modal = $('<div>').css({
                            background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
                            backgroundSize: 'contain',
                            width:'100%', height:'100%',
                            position:'fixed',
                            zIndex:'10000',
                            top:'0', left:'0',
                            cursor: 'pointer'
                        }).click(function(){
                            removeModal();
                        }).appendTo('body');
                        $('body').on('keyup.modal-close', function(e){
                          if(e.key==='Escape'){
                            removeModal();
                        }
                        });
                    });
                };
                reader.readAsDataURL(input.files[0]);
            });

            $('body').on('change', '#replace_qty', function(e){
                let order_qty = parseInt($('#online_qty').val());
                let replace_qty = parseInt($(this).val());
                if(order_qty < replace_qty) {
                    alert('Replacement Quantity not exceed order quantity');
                    $(this).val(order_qty);
                    return false;                    
                }
                let percentage = (replace_qty/order_qty)*100;
                if(percentage<=10){
                    $('.perhide').hide();
                    $('.pershow').show();
                }else{
                    $('.perhide').show();
                    $('.pershow').hide();
                }
            });

            $('body').on('change', '#replace_offline_qty', function(e){
                let order_qty = parseInt($('#offline_qty').val());
                let replace_qty = parseInt($(this).val());
                if(order_qty < replace_qty) {
                    alert('Replacement Quantity not exceed order quantity');
                    $(this).val(order_qty);
                    return false;                    
                }
                let percentage = (replace_qty/order_qty)*100;
                if(percentage<=10){
                    $('.perhide').hide();
                    $('.pershow').show();
                }else{
                    $('.perhide').show();
                    $('.pershow').hide();
                }
            });            
        });
    </script>
    <style type="text/css">
        #order_item, .selectize-input{
            width: 100% !important;
        }

        .order_control{
            margin-bottom: 0px;
        }

        .active .round-tab{
            color: #0995CD;
        }

		button.third_page{
            display: flex !important;
            align-items: center !important;
            overflow: hidden;
        }

        .spinning{
            margin-left: 10px;
            display:inline-block;
            border: 2px solid #f3f3f3;
            border-radius: 50%;
            border-top: 2px solid #3498db;
            padding: 6px;
            -webkit-animation: loadingSpin 1.2s linear infinite;
            animation: loadingSpin 1.2s linear infinite;
        }

        .pright{
            padding-right:20px !important;
        }

        .imgThumb{
            width: 100px;
            margin: 10px 10px 0;
            cursor: pointer;
        }

        @-webkit-keyframes loadingSpin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }


        @keyframes loadingSpin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        @media(max-width: 768px){
            .nav-tabs a{
                text-align: center;
                border:1px solid #fff !important;
            }

            .active .round-tab{
                border-bottom: 1px solid #0995CD;
            }
        }

        @media(max-width: 990px){
            .pright{
                padding-right:0px !important;
            }
        }
    </style>
@endpush

<div class="sidebar">
    {{--  {{dd($menu->items)}} --}}
    @foreach ($menu->items as $menuItem)
        <div class="menu-block">
            <div class="menu-block-title">
                {{ trans($menuItem['name']) }}

                <i class="icon icon-arrow-down right" id="down-icon"></i>
            </div>

            <div class="menu-block-content">
                <ul class="menubar">
                    @foreach ($menuItem['children'] as $subMenuItem)
                  <?php  $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
if(empty($user))
$user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

                            $route = $subMenuItem['url'];
                            if(strpos($route, 'customer') && $user->role_id == 2)
                                $route =  str_replace('customer', 'admin', $route);
                            //$due_amount = $user->credit_used - $user->credit_paid;
                        ?>
                        
                        {{-- 
                            @if(!strpos($route, 'credit-payment') || ($user->role_id == 2 && $due_amount > 0))
                                <li class="menu-item {{ $menu->getActive($subMenuItem) }}">
                                    <a href="{{ url($route) }}"> {{ trans($subMenuItem['name']) }} </a>
                                </li>
                            @endif

                        --}}
     
                        @if(!strpos($route, 'make-payment') || $user->credit_available==1)
                            <li class="menu-item {{ $menu->getActive($subMenuItem) }}">
                                <a href="{{ url($route) }}">
                                    {{ trans($subMenuItem['name']) }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                    {{-- <li class="menu-item {{ Request::is('youtube-videos')?'active':'' }}">
                        <a href="{{ url('youtube-videos') }}">
                            My Webinar
                        </a>
                    </li>
                     <li class="menu-item {{ Request::is('customer/account/failure-implants')?'active':'' }}">
                        <a href="{{ route('failure.implants') }}">
                            Failed Implants
                        </a>
                    </li> --}}
                </ul>
            </div>
        </div>
    @endforeach
</div>

@push('scripts')
<script>
    $(document).ready(function() {
        $(".icon.icon-arrow-down.right").on('click', function(e){
            var currentElement = $(e.currentTarget);
            if (currentElement.hasClass('icon-arrow-down')) {
                $(this).parents('.menu-block').find('.menubar').show();
                currentElement.removeClass('icon-arrow-down');
                currentElement.addClass('icon-arrow-up');
            } else {
                $(this).parents('.menu-block').find('.menubar').hide();
                currentElement.removeClass('icon-arrow-up');
                currentElement.addClass('icon-arrow-down');
            }
        });
    });
</script>
@endpush

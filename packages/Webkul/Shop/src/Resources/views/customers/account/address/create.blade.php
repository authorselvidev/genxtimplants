@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.address.create.page-title') }}
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">

    <div class="account-content">

        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">
            <div class="account-head mb-15">
                <span class="back-icon"><a href="{{ route('customer.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
                <span class="account-heading">{{ __('shop::app.customer.account.address.create.title') }}</span>
                <span></span>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.address.create.before') !!}

            <form method="post" action="{{ route('customer.address.store') }}" @submit.prevent="onSubmit">

                <div class="account-table-content addr-sec">
                    @csrf

                    @if(isset($_GET['returnto']))
                        <input type="hidden" name="checkout" value="{{$_GET['returnto']}}">
                    @endif
                    @if(isset($_GET['type']))
                        <input type="hidden" name="type" value="{{$_GET['type']}}">
                    @endif

                    {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.before') !!}
                    <div class="form-field">
                        <div class="control-group col-md-3 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                            <label for="my_title" class="required">My Title</label>
                            <select name="my_title" class="control" v-validate="'required'">
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                                <option value="Mr. Dr.">Mr. Dr.</option>
                                <option value="Mrs. Dr.">Mrs. Dr.</option>
                            </select>
                            <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                        </div>
                        <div class="control-group col-md-5" :class="[errors.has('name') ? 'has-error' : '']">
                        <label for="name" class="required">First Name</label>
                        <input type="text" class="control" name="name" v-validate="'required'" data-vv-as="&quot;{{ __('Name') }}&quot;">
                        <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                        </div>
                        <div class="control-group col-md-4 cpass">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="control" name="last_name" value="{{ old('last_name') }}">
                        </div>
					</div>
                    <div class="form-field">
                    <div class="control-group pass col-sm-6" :class="[errors.has('phone') ? 'has-error' : '']">
                            <label for="phone" class="required">{{ __('shop::app.customer.account.address.create.phone') }}</label>
                            <input type="text" class="control" name="phone" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.phone') }}&quot;">
                            <span class="control-error" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                        </div>
					                        <div class="control-group cpass col-md-6" :class="[errors.has('address') ? 'has-error' : '']">
                            <label for="address" class="required">Address</label>
                            <textarea type="text" class="control" name="address" v-validate="'required'"></textarea>
                            <span class="control-error" v-if="errors.has('address')">@{{ errors.first('address') }}</span>
                        </div>
                    </div>

                    <!--  <div class="control-group" :class="[errors.has('address2') ? 'has-error' : '']">
                        <label for="address2">{{ __('shop::app.customer.account.address.create.address2') }}</label>
                        <input type="text" class="control" name="address2">
                        <span class="control-error" v-if="errors.has('address2')">@{{ errors.first('address2') }}</span>
                    </div> 

                    @include ('shop::customers.account.address.country-state', ['countryCode' => old('country'), 'stateCode' => old('state')])

                    <div class="control-group" :class="[errors.has('city') ? 'has-error' : '']">
                        <label for="city" class="required">{{ __('shop::app.customer.account.address.create.city') }}</label>
                        <input type="text" class="control" name="city" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.city') }}&quot;">
                        <span class="control-error" v-if="errors.has('city')">@{{ errors.first('city') }}</span>
                    </div>-->


                <div class="form-field">
                    <div class="control-group pass col-md-3 col-sm-6" :class="[errors.has('postcode') ? 'has-error' : '']">
                        <label for="postcode" class="required">{{ __('shop::app.customer.account.address.create.postcode') }}</label>
                        <input type="text" class="control" name="postcode" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.postcode') }}&quot;">
                        <span class="control-error" v-if="errors.has('postcode')">@{{ errors.first('postcode') }}</span>
                    </div>
                    <div class="control-group pass col-md-3 col-sm-6" :class="[errors.has('country') ? 'has-error' : '']">
                        <label for="country_id" class="required">{{ __('shop::app.customer.signup-form.country') }}</label>
                        <input type="text" class="control" name="country" v-validate="'required'" disabled="disabled" value="India">
                        <span class="control-error" v-if="errors.has('country')">@{{ errors.first('country') }}</span>
                    </div>
                    <div class="control-group pass col-md-3 col-sm-6" :class="[errors.has('state') ? 'has-error' : '']">
                        <label for="state" class="required">State</label>
                        <?php $states = array();
                        $states = DB::table('country_states')->where('country_id',101)->get();?>
                        <select name="state" id="state" class="control" v-validate="'required'">
                            <option value="">Choose your state...</option>
                            <?php foreach($states as $key => $state) { ?>
                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                            <?php } ?>
                        </select>
                        <span class="control-error" v-if="errors.has('state')">@{{ errors.first('state_id') }}</span>
                    </div>
                    <div class="control-group pass cpass col-md-3 col-sm-6" :class="[errors.has('city') ? 'has-error' : '']">
                        <label for="city" class="required">{{ __('shop::app.customer.signup-form.city') }}</label>
                        <select name="city" id="city" class="control" v-validate="'required'">
                        </select>
                        <span class="control-error" v-if="errors.has('city')">@{{ errors.first('city') }}</span>
                    </div>
                </div>


                    {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.after') !!}

                    <div class="button-group">
                        <input class="btn btn-primary btn-lg" type="submit" value="{{ __('shop::app.customer.account.address.create.submit') }}">
                        {{-- <button class="btn btn-primary btn-lg" type="submit">
                            {{ __('shop::app.customer.account.address.edit.submit') }}
                        </button> --}}
                    </div>

                </div>

            </form>

            {!! view_render_event('bagisto.shop.customers.account.address.create.after') !!}

        </div>
    </div>
</div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function(){
$('body').on('change','#state', function(){
    
    var stateID = $(this).val(); 
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('customer/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
  });
});
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        });
</script>
@endpush



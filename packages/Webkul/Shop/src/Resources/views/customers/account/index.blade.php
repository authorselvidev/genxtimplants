@extends('shop::layouts.master')

@section('content-wrapper')
    <div class="account-content">
	
	
	@if(Request::is('customer/account/index'))
		<div class="show-menu">@include('shop::customers.account.partials.sidemenu')</div>
	@else
        <div class="show-menu">@include('shop::customers.account.partials.sidemenu')</div>
	@endif
    </div>
    
@endsection

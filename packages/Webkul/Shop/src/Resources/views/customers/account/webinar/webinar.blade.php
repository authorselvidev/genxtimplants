@extends('shop::layouts.master')

@section('page_title')
    My Webinars
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">
    <div class="account-content">
        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">

            <div class="account-head mb-10">
                <span class="back-icon">
                    <a href="{{ route('customer.account.index') }}">
                        <i class="icon angle-left-icon back-link"></i>
                    </a>
                </span>
                <span class="account-heading">
                    My Webinars
                </span>
                <div class="horizontal-rule"></div>
            </div>

            <div class="account-items-list">
                @if(isset($webinars) && !empty($webinars))
                    <div class="webinar_container">
                        @foreach($webinars as $web_id => $webinar)
                            
                            <div class="webinar">
                                <div class="webinar_details">
                                    <h3>{{$webinar['topic']}}</h3>
                                    <p class="web_info" style="max-width: 700px;" >{{$webinar['description']}}</p>

                                    @if(isset($webinar['start_time']))
                                        <span class="web_info">Start at: {{date('d/m/Y h:ia', $webinar['start_time'])}}</span>
                                    @endif
                                    
                                    @if(isset($webinar['duration']))
                                        <span class="web_info">Duration: {{ date('h:i', mktime(0,$webinar['duration'])) }} hours</span>
                                    @endif

                                    <span class="web_info">Type: {{ $webinar['type_full'] }}</span>
                                    @if($webinar['type']==9)
                                        <?php
                                            $type = [
                                                1=> 'Daily',
                                                2=> 'Weekly',
                                                3=> 'Monthly'
                                            ];
                                        ?>
                                        <span class="web_info">Recurrence: {{ $type[$webinar['recurrence']['type']] }}</span>
                                        @if($webinar['recurrence']['type'] == 2)
                                            <span class="web_info">Weekly on: {{ $webinar['recurrence']['weekly_days'] }}</span>
                                        @endif
                                        
                                        @if(isset($webinar['recurrence']['end_date_time']))
                                            <span class="web_info">Recurrence end: {{ date('d/m/Y h:ia', $webinar['recurrence']['end_date_time']) }}</span>
                                        @endif

                                        @if(isset($webinar['recurrence']['end_times']))
                                            <span class="web_info">Nos of Recurrence: {{ $webinar['recurrence']['end_times'] }}</span>
                                        @endif

                                    @endif
                                </div>
                                <div class="webinar_action" style="align-content: flex-end;">
                                    @if(isset($webinar['password']))
                                        <a href="#verifyPasscode" data-key="{{ base64_encode($webinar['password']) }}" data-href="{{ base64_encode(route('webinar.zoomview', $web_id)) }}" data-toggle="modal" class="webinar_view btn btn-small btn-primary">Join</a>
                                    @else
                                        <a href="{{route('webinar.zoomview', $web_id)}}" class="btn btn-small btn-primary" target="_blank">Join</a>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <span class="no_webinar">There is no webinar scheduled.</span>
                @endif
                <div style="text-align:center;">
                    For questions or any queries please Whatsapp on <a href="tel:+918484088331">8484088331</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="verifyPasscode" tabindex="-1" role="dialog" aria-labelledby="verifyPasscode" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="verifyPasscode">Enter the Meeting Passcode</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="key" id="pass_key">
        <input type="hidden" name="pass_lc" id="pass_lc">
        <div class="form-group px-5 pass_contain" >
            <label for="passKey">Enter the passcode:</label>
            <input type="text" class="form-control" style="max-width: 400px;" id="passKey" name="passKey" placeholder="Password">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary webpass_view">View</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
    <style type="text/css">
        .account-items-list, .webinar_container, .webinar, .webinar_details{
            display: flex;
        }

        .webinar_container{
            flex-direction: column;
            margin-bottom: 10px;
        }

        .webinar_container h3{
            margin-top: 5px;
            margin-bottom: 5px;
            font-weight: 500;
        }

        .webinar{
            flex-direction: row;
            justify-content: space-between;
            padding: 10px;
            margin-bottom: 5px;
            border: 1px solid #CCC;
        }

        .webinar_details, .webinar_action{
            flex-direction: column;
        }

        .webinar_action{
            display: flex;
            justify-content: center;
            align-content: center;
            justify-content: flex-end;
        }

        .account-items-list{
            padding: 0px 10px;
            flex-direction: column;
        }

        .no_webinar{
            display: block;
            text-align: center;
            padding:20px 0;
        }

        .web_info{
            margin-top: 5px;    
        }
        .err{
            color: #9f0000;
            margin-top: 5px;
            display: inline-block;
        }

        @media(max-width:750px){
            .webinar{
                flex-direction: column;
            }
            .webinar_action{
                margin-top: 10px;
                align-self: center;
            }
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('#verifyPasscode').on('shown.bs.modal', function (e) {
                $('#verifyPasscode .pass_contain').find('.err').remove();
                $(this).find('#pass_key').val($(e.relatedTarget).data('key'));
                $(this).find('#pass_lc').val($(e.relatedTarget).data('href'));
            });

            $('.webpass_view').click(function(e){
                $('#verifyPasscode .pass_contain').find('.err').remove();
                if(!$('#verifyPasscode #passKey').val()){
                    $('#verifyPasscode .pass_contain').append('<span class="err">This field is required</span>');
                    return;
                }

                if($('#verifyPasscode #passKey').val() != atob($('#verifyPasscode #pass_key').val())){
                    $('#verifyPasscode .pass_contain').append('<span class="err">Invalid Passcode</span>');
                }else{
                    location.href = atob($('#verifyPasscode #pass_lc').val());
                }
                e.preventDefault();
            });

            
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        });
    </script>
@endpush
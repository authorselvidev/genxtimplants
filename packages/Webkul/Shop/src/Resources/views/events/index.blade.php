@extends('shop::layouts.master')

@section('page_title')
    Events - GenXT
@endsection

@section('content-wrapper')


<div class="section events-page col-md-12 col-xs-12">
	<ul class="nav nav-tabs text-center col-md-12 col-xs-12" style="padding-right: 0px !important;">
		<li class="active"><a data-toggle="tab" href="#upcoming-events" style="margin-right: 0px">Upcoming Events</a></li>
		<li><a data-toggle="tab" href="#past-events" style="margin-right: 0px">Past Events</a></li>
	</ul>
	<div class="tab-content" style="padding-top: 25px;">
		<div id="upcoming-events" class="tab-pane fade in active">
			<ul class="all-events section col-md-12">
				@foreach($upcoming_events as $key => $event)
					<li class="each-event col-md-6">
						<a href="{{url('/events/'.$event->slug)}}" style="margin-right: 0px !important;">{{$event->name}}</a>
					</li>
		  		@endforeach
		  	</ul>
		</div>
		<div id="past-events" class="tab-pane">
			<ul class="all-events section col-md-12 ">
				@foreach($past_events as $key => $event)
				    <li class="each-event col-md-6">
						<a href="{{url('/events/'.$event->slug)}}" style="margin-right: 0px !important;">{{$event->name}}</a>
					</li>
			  	@endforeach
			</ul>
		</div>
	</div>
</div>
@endsection

@push('scripts')
	<script>
		$(function(){
			$('.content-container').attr('style', 'min-height: calc(100vh - 375px);');
		});
	</script>
@endpush
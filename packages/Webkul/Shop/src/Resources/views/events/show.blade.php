@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.home.page-title') }}
@endsection

@section('content-wrapper')


<div class="auth-content single-events-page section events-page col-md-12 col-xs-12">
	<div class="single-event section col-md-12 col-xs-12">
		<h2 class="event-title text-center">{{$event->name}}</h2>

		<ul class="nav nav-tabs text-center col-md-12 col-xs-12">
		 	<li class="active"><a data-toggle="tab" href="#overview">Overview</a></li>
		  	<li><a data-toggle="tab" href="#special-offer">Special Offer</a></li>
		</ul>
		<div class="tab-content">
		    <div id="overview" class="tab-pane fade in active">
				<div class="section event-content col-md-12">{!! $event->description !!}</div>
		    </div>
		    <div id="special-offer" class="tab-pane fade in">
		    	<div class="section col-md-12 col-xs-12">
            <form method="post" action="{{ route('shop.event.register',$event->slug) }}" @submit.prevent="onSubmit">

                @csrf
                <input type="hidden" name="event_name" value="{{$event->name}}">
                <div class="col-md-12 event-form">
                    <h2 class="contact-head">Register your seat for FREE</h2>
                    <div class="col-md-12 form-fields">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-3 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option value="Mr.">Mr.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Mr. Dr.">Mr. Dr.</option>
                                    <option value="Mrs. Dr.">Mrs. Dr.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-md-5" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'" value="{{ old('first_name') }}" data-vv-as="&quot;{{ __('First Name') }}&quot;">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group col-md-4 cpass">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name" value="{{ old('last_name') }}">
                                
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">Mobile Number</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ old('phone_number') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.phonenumber') }}&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>

                            <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group pass cpass col-md-12" :class="[errors.has('country_id') ? 'has-error' : '']">
                                <label for="country_id" class="required">Country</label>
                                <?php $states = array();
                                $countries = DB::table('countries')->get();?>
                                <select name="country_id" id="state" class="control" v-validate="'required'" data-vv-as="&quot;{{ __('Country') }}&quot;">
                                    <option value="">Choose your country...</option>
                                    <option value="101">India</option>
                                    <!--<?php foreach($countries as $key => $country) { ?>
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    <?php } ?>-->
                                </select>
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                        </div>
                    <div class="form-field col-md-12">
                        <div class="control-group col-md-12 pass cpass" :class="[errors.has('event_msg') ? 'has-error' : '']">
                            <label for="event_msg" class="required">Message</label>
                            <textarea class="control" name="event_msg" value="{{ old('event_msg') }}" v-validate="'required'" data-vv-as="&quot;{{ __('Message') }}&quot;"></textarea>
                            <span class="control-error" v-if="errors.has('event_msg')">@{{ errors.first('event_msg') }}</span>
                        </div>
                    </div>
                        <button class="btn btn-primary btn-lg" type="submit">
                           Submit
                        </button>
                    </div>
                </div>
            </form>
    </div>
</div>
		</div>
	</div>

</div>
@endsection

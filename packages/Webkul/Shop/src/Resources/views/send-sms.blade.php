<?php

 $apiKey = env('SMS_API_KEY');
    $apikey = urlencode($apiKey);
    // Message details
    //$numbers = array($sms_numbers);
   //dd($numbers);
    $sender = urlencode('GENXTI');
    //dd($sms_message);
    $sms_message = $sms_message."\n- GenXT Dental Implants";
    $message = rawurlencode($sms_message);
    //dd($message);
    //$numbers = implode(',', $numbers);
     
    // Prepare data for POST request
    $data = array('apikey' => $apikey, 'numbers' => $sms_numbers, 'sender' => $sender, 'message' => $message);
    // Send the POST request with cURL
    $ch = curl_init('https://api.textlocal.in/send/');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    // Process your response here
    
    $json_data = json_decode($response, true);
    $json_data['details'] = $data;
    \Log::channel('sms')->info(json_encode($json_data));
    echo $response;    
?>

@extends('shop::layouts.master')
@section('page_title')
    Contact Us - GenXT
@endsection
@section('content-wrapper')

<div class="auth-content">

    <div class="section contact-page col-md-12 col-xs-12">
        <div class="contact-left col-md-8">
            <form method="post" action="{{ route('shop.contact.store') }}" @submit.prevent="onSubmit">

                @csrf

                <div class="col-md-12 contact-form">
                    <h2 class="contact-head">Contact Form</h2>
                    <div class="col-md-12 form-fields">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-12 pass cpass" :class="[errors.has('interested_in') ? 'has-error' : '']">
                                <label for="interested_in" class="required">I am interested in:</label>
                                <select  data-vv-as="&quot;{{ __('Interested In') }}&quot;" name="interested_in" class="control" v-validate="'required'">
                                    <option value="">Select your interest...</option>
                                    <option value="products">Products</option>
                                    <option value="documentation">Documentation</option>
                                    <option value="technical-skills">Technical Details</option>
                                    <option value="sales">Sales</option>
                                    <option value="courses">Courses</option>
                                    <option value="new-ideas-suggestions">New Ideas & Suggestions</option>
                                </select>
                                <span class="control-error" v-if="errors.has('interested_in')">@{{ errors.first('interested_in') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-3 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option value="Mr.">Mr.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Mr. Dr.">Mr. Dr.</option>
                                    <option value="Mrs. Dr.">Mrs. Dr.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-md-5" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'"  data-vv-as="&quot;{{ __('First Name') }}&quot;" value="{{ old('first_name') }}">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group col-md-4 cpass">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name" value="{{ old('last_name') }}">
                                
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">Mobile Number</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ old('phone_number') }}" data-vv-as="&quot;{{ __('Mobile Number') }}&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>

                            <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group pass cpass col-md-12" :class="[errors.has('country_id') ? 'has-error' : '']">
                                <label for="country_id" class="required">Country</label>
                                <?php $states = array();
                                $countries = DB::table('countries')->get();?>
                                <input type="text" value="India" class="form-control" disabled>
                                <input type="hidden" name="country_id" value="101">
                               <!-- <select  data-vv-as="&quot;{{ __('Country') }}&quot;" name="country_id" id="state" class="control" v-validate="'required'">
                                    <option value="">Choose your country...</option>
                                    <option value="101">India</option>
                                    <?php foreach($countries as $key => $country) { ?>
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    <?php } ?>
                                </select>-->
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                        </div>
                    <div class="form-field col-md-12">
                        <div class="control-group" :class="[errors.has('contact_msg') ? 'has-error' : '']">
                            <label for="contact_msg" class="required">Message</label>
                            <textarea class="control" name="contact_msg" value="{{ old('contact_msg') }}"  data-vv-as="&quot;{{ __('Message') }}&quot;" v-validate="'required'"></textarea>
                            <span class="control-error" v-if="errors.has('contact_msg')">@{{ errors.first('contact_msg') }}</span>
                        </div>
                    </div>
                    <input type="hidden" name="recaptcha" id="recaptcha">
                        <button class="btn btn-primary btn-lg" type="submit">
                           Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    
    <div class="contact-right col-md-4 col-xs-12">
        <h2 class="contact-head">Contact Us</h2>
        <div class="contact-mail">
			<h4>Address</h4>
			<h5>Regenics</h5>
			Plot no.11 , lane M, Mundhwa Road,<br />
			Near Westin hotel,<br />
			Pune 411001.

			<h4>Contact Number</h4>
			+918484088331{{--  / +919823179349 --}}

			<h4>Email</h4>
			<a href="mailto:info@genxtimplants.com">info@genxtimplants.com</a>
        </div>
    </div>
    </div>
</div>
@endsection


<div class="order-summary">
    <h3>{{ __('shop::app.checkout.total.order-summary') }}</h3>

    <div class="item-detail">
        <label>
           {{--  {{ intval($cart->items_qty) }}
            {{ __('shop::app.checkout.total.sub-total') }}
            {{ __('shop::app.checkout.total.price') }} --}}
            Sub Total
        </label>
        <label class="right">{{ core()->currency($cart->base_sub_total) }}</label>
    </div>
    <?php $discounted_subtotal=0; ?>
                                    @if($cart->membership_discount == 0 && $cart->promocode_discount == 0 && $cart->reward_discount == 0 && $cart->base_discount != 0)
                                    <div class="item-detail">
                                        <label>Special Discount:</label>
                                        <label class="right">- {{ core()->currency($cart->discount) }}</label>
                                    </div>
                                    <?php $discounted_subtotal = $cart->base_sub_total - $cart->discount; ?>

                                    @endif

                                    @if($cart->membership_discount != 0)
                                    <div class="item-detail">
                                        <label>Membership Discount:</label>
                                        <label class="right">- {{ core()->currency($cart->membership_discount) }}</label>
                                    </div>
                                    <?php $discounted_subtotal = $cart->base_sub_total - $cart->membership_discount; ?>
                                    @endif

                                    @if($cart->promocode_discount != 0)
                                    <div class="item-detail">
                                        <label>Promo Code Discount:</label>
                                        <label class="right">- {{ core()->currency($cart->promocode_discount) }}</label>
                                    </div>
                                    <?php $discounted_subtotal = $cart->base_sub_total - $cart->promocode_discount; ?>
                                    @endif

                                    @if($cart->reward_discount != 0)
                                   <div class="item-detail">
                                        <label>Reward Points:</label>
                                        <label class="right">- {{ core()->currency($cart->reward_discount) }}</label>
                                    </div>
                                    <?php $discounted_subtotal = $cart->base_sub_total - $cart->reward_discount; ?>
                                    @endif
                                    @if($discounted_subtotal != 0)
                                    <div class="item-detail">
                                        <label>Discounted Subtotal</label>
                                        <label class="right">{{ core()->formatBasePrice($discounted_subtotal) }}</label>
                                    </div>
                                    @endif
    @if ($cart->shipping_amount)
        <div class="item-detail">
            <label>{{ __('shop::app.checkout.total.delivery-charges') }}</label>
            <label class="right">{{ core()->currency($cart->shipping_amount) }}</label>
        </div>
    @endif

    @if ($cart->tax_total)
        <div class="item-detail">
            <label>{{ __('shop::app.checkout.total.tax') }}</label>
            <label class="right">{{ core()->currency($cart->tax_total) }}</label>
        </div>
    @endif

    <div class="payble-amount">
        <label>{{ __('shop::app.checkout.total.grand-total') }}</label>
        <label class="right">{{ core()->currency($cart->grand_total) }}</label>
    </div>
</div>
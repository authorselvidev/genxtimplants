@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

<?php $cart = cart()->getCart();
$user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
if(empty($user))
$user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

 ?>

@if ($cart)
    @php
        Cart::collectTotals();
    @endphp

    <?php $items = $cart->items;
    $get_tax = DB::table('tax_rates')->where('state',$user->state_id)->first(); ?>
    <div class="hidden-lg hidden-sm hidden-md">
    <a class="cart-link" href="{{ route('shop.checkout.cart.index') }}">
            <span class="icon fa fa-shopping-cart"></span>
    </a>
</div>

    <div class="dropdown-toggle hidden-xs" id="cart-info" data-toggle="dropdown">
        <a class="cart-link" href="{{ route('shop.checkout.cart.index') }}">
            <span class="icon fa fa-shopping-cart"></span>
        </a>
        <span class="name ">
            <!--{{ __('shop::app.header.cart') }}-->
            <span class="count"> {{ $cart->items->count() }} Cart</span>
        </span>

        <!--<i class="icon arrow-down-icon"></i>-->
    </div>

    <div class="dropdown-list hidden-xs" aria-labelledby="cart-info" style="display: none; top: 50px; right: 0px">
        <div class="dropdown-container">
            <div class="dropdown-cart">

                <div class="dropdown-content">
                    @foreach ($items as $item)
                        <div class="item" id="item-{{$item->id}}">
                            <div class="item-image" >
                                <?php
                                    if ($item->type == "configurable")
                                        $images = $productImageHelper->getProductBaseImage($item->child->product);
                                    else
                                        $images = $productImageHelper->getProductBaseImage($item->product);
                                ?>
                                <img src="{{ $images['small_image_url'] }}" />
                            </div>

                            <div class="item-details">
                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]) !!}

                                <div class="item-name">{{ $item->name }}</div>

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]) !!}


                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.options.before', ['item' => $item]) !!}

                                @if ($item->type == "configurable")
                                    <div class="item-options">
                                        {{ trim(Cart::getProductAttributeOptionDetails($item->child->product)['html']) }}
                                    </div>
                                @endif

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.options.after', ['item' => $item]) !!}

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.before', ['item' => $item]) !!}

                                <div class="item-qty">Quantity - <span>{{ $item->quantity }}</span></div>

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.after', ['item' => $item]) !!}
                            </div>
                        
							<div class="item-price">

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]) !!}
                                <?php 
                                    $price_with_tax = $item->total + $item->tax_amount - $item->discount_amount; ?>
                                <div class="item-price">{{ core()->currency($price_with_tax) }}</div>
                                <div style="display:none" class="each-price">{{ $price_with_tax }}</div>
                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]) !!}

							</div>
						</div>

                    @endforeach
                </div>

                <div class="dropdown-header">
                    <p class="heading">
                        {{ __('shop::app.checkout.cart.cart-subtotal') }}

                        {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.before', ['cart' => $cart]) !!}

                        <span>{{ core()->currency($cart->base_grand_total) }}</span>

                        {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.after', ['cart' => $cart]) !!}
                    </p>
                </div>
				
                <div class="dropdown-footer">
                    <a href="{{ route('shop.checkout.cart.index') }}">{{ __('shop::app.minicart.view-cart') }}</a>

                    <a class="btn btn-primary btn-lg" style="color: white;" href="{{ route('shop.checkout.onepage.index') }}">{{ __('shop::app.minicart.checkout') }}</a>
                </div>
            </div>
        </div>
    </div>

@else

    <div class="dropdown-toggle">
        <div class="cart-section">
            <span class="fa fa-shopping-cart"></span>
            <!--<span class="name">{{ __('shop::app.minicart.cart') }}-->
            <span class="count"> {{ __('shop::app.minicart.zero') }}</span><span> Items</span>
        </div>
    </div>
@endif
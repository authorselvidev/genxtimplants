@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.checkout.cart.title') }}
@stop

@section('content-wrapper')
    @inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
    <section class="cart">
        @if ($cart)
            <div class="title">
                {{ __('shop::app.checkout.cart.title') }}
            </div>

            <div class="cart-content">
                <div class="left-side">
                    <form action="{{ route('shop.checkout.cart.update') }}" method="POST" @submit.prevent="onSubmit">

                        <div class="cart-item-list account-items-list" style="margin-top: 0">
                            <div class="table">
									<table>
										<thead>
											<tr><th class="hidden-xs">Image</th><th style="width: 125px;">Title</th><th>Price</th><th>Quantity</th><th>Total</th><th>Action</th></tr>
										</thead>
                            @csrf
                            @foreach ($cart->items as $item)

                                <?php
                                    if ($item->type == "configurable")
                                        $productBaseImage = $productImageHelper->getProductBaseImage($item->child->product);
                                    else
                                        $productBaseImage = $productImageHelper->getProductBaseImage($item->product);
                                ?>

										<tbody>
											<tr>

												<td>
                                                    <img class="item-image" width="100" height="100" src="{{ $productBaseImage['medium_image_url'] }}" />
                                                </td>
												<td data-value="Name">
													{!! view_render_event('bagisto.shop.checkout.cart.item.name.before', ['item' => $item]) !!}
															{{ $item->product->name }}
													{!! view_render_event('bagisto.shop.checkout.cart.item.name.after', ['item' => $item]) !!}
												</td>
												<td data-value="Price">
													{!! view_render_event('bagisto.shop.checkout.cart.item.price.before', ['item' => $item]) !!}
													<?php $price_with_tax = $item->base_price + $item->base_tax_amount - $item->base_discount_amount; ?>
														{{ core()->currency($price_with_tax) }}
													{!! view_render_event('bagisto.shop.checkout.cart.item.price.after', ['item' => $item]) !!}
												</td>
												<td data-value="Quantity">
													{!! view_render_event('bagisto.shop.checkout.cart.item.quantity.before', ['item' => $item]) !!}

                                        <!-- <div class="misc"> -->
                                            <div class="each-col" :class="[errors.has('qty[{{$item->id}}]') ? 'has-error' : '']">
                                                <input type="number" @if($item->price == '0.00') {{ 'disabled' }}  @endif class="control" v-validate="'required|numeric|min_value:1'" name="qty[{{$item->id}}]" value="{{ $item->quantity }}" data-vv-as="&quot;{{ __('shop::app.checkout.cart.quantity.quantity') }}&quot;" min="0">
                                                <br/>
                                            </div>

                                            <?php
                                                $qty = \DB::table('product_inventories')->where('product_id',$item->product_id)->pluck('qty')[0];
                                            ?>

                                            @if($qty<=0)
                                                <span class="error_qty">Out of Stock.</span>
                                            @elseif(!\Cart::isItemHaveQuantity($item))
                                                <span class="error_qty">Quantity exceeds, Only {{$qty}} left.</span>
                                            @endif
                                        </td>
                                        <td data-value="Total">
                                            <div class="price each-col col-md-2">
                                                <?php $total = $price_with_tax * $item->quantity; ?>
                                                {{ core()->currency($total) }}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="each-col">
                                            <span class="remove">
                                                <a href="{{ route('shop.checkout.cart.remove', $item->id) }}" onclick="removeLink('Do you really want to do this?')"><i class="fa fa-trash"></i></a></span>

                                            @auth('customer')
                                                <span class="towishlist">
                                                    @if ($item->parent_id != 'null' ||$item->parent_id != null)
                                                        <a href="{{ route('shop.movetowishlist', $item->id) }}" class="add-to-wishlist" onclick="removeLink('Do you really want to do this?')" title="Move to Wishlist"><span class="icon wishlist-icon"></span></a>
                                                    @else
                                                        <a href="{{ route('shop.movetowishlist', $item->child->id) }}" class="add-to-wishlist" onclick="removeLink('{{ __('shop::app.checkout.cart.cart-remove-action') }}')" title="Move to Wishlist"><span class="icon wishlist-icon"></span></a>
                                                    @endif
                                                </span>
                                            @endauth
                                        </div>

                            @endforeach
                            </tbody>
							</table>
								</div>
                        </div>

                        {!! view_render_event('bagisto.shop.checkout.cart.controls.after', ['cart' => $cart]) !!}

                        <div class="misc-controls">
                            <a href="{{ route('shop.home.index') }}" class="link con-shop"><i class="fa fa-caret-left"></i> {{ __('shop::app.checkout.cart.continue-shopping') }}</a>

                            <div>
                                <button type="submit" class="btn btn-lg btn-primary">
                                    {{ __('shop::app.checkout.cart.update-cart') }}
                                </button>
                            </div>
                        </div>

                        {!! view_render_event('bagisto.shop.checkout.cart.controls.after', ['cart' => $cart]) !!}
                    </form>
                </div>

                <div class="right-side">
                    {!! view_render_event('bagisto.shop.checkout.cart.summary.after', ['cart' => $cart]) !!}

                    @include('shop::checkout.total.summary', ['cart' => $cart])

                    {!! view_render_event('bagisto.shop.checkout.cart.summary.after', ['cart' => $cart]) !!}
                </div>
                @if (!cart()->hasError())
                    <a href="{{ route('shop.checkout.onepage.index') }}" class="btn btn-lg btn-primary">
                        {{ __('shop::app.checkout.cart.proceed-to-checkout') }}
                    </a>
                @else
                    <h3 class="error_qty error_qty_all">Please check the quantity !</h3>
                @endif
            </div>

        @else

            <div class="title">
                {{ __('shop::app.checkout.cart.title') }}
            </div>

            <div class="cart-content">
                
                {{-- <p>{{ __('shop::app.checkout.success.info') }}</p> --}}
               

                <p>
                    {{ __('shop::app.checkout.cart.empty') }}!
                </p>
                <p>Click on continue Shopping below to buy further!</p>

                <p style="display: inline-block;">
                    <a style="display: inline-block;" href="{{ route('shop.home.index') }}" class="btn btn-lg btn-primary">{{ __('shop::app.checkout.cart.continue-shopping') }}</a>
                </p>
            </div>

        @endif
    </section>

@endsection

@push('scripts')
    <script>
        function removeLink(message) {
            if (!confirm(message))
            event.preventDefault();
        }
    </script>
    <style type="text/css">
        .error_qty{
            color:#C00;
            margin:5px 0;
            display:inline-block;
        }

        .error_qty_all{
            display:block;
            text-align:right;
            padding-top:5px;
        }

    </style>
@endpush

@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.checkout.onepage.title') }}
@stop

@section('content-wrapper')
<style type="text/css">
   .checkout-products th,.checkout-products td{
        padding: 15px 0px;
        text-align: center;
    }
</style>
<?php
    $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
    if(empty($user))
        $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
    $user_credit = $user->credit_limit - ($user->credit_used - $user->credit_paid) + $user->credit_balance;

?>
  <div class="checkout-section col-xs-12 col-md-12">
    <form method="post" id="order-form" action="{{route('shop.checkout.save-order')}}">
        @csrf
        <div class="checkout-left col-xs-12 col-md-5">
            <div class="sec-inner col-xs-12 col-md-12">
                <div class="ordered-products" style="overflow-x:auto" >
                    <h3 class="text-center">Your Order</h3>
                    @if($cart->discount != 0.0000)
                        <p class="text-center" style="color:#0A96CE;">If you used any of the discount below, your special price will not be available!</p>
                    @endif
                    <div class="checkout-products">
                    <table style="width:100%;" >
                        <thead>
                            <tr class="products head">
                            <th>Products</th>
                            <th>Price</th>
                            <th>Qty</th>
                            @if(($cart->promocode_discount != 0 || $cart->membership_discount !=0 || $cart->reward_discount != 0 || $cart->credit_discount != 0) || (($cart->promocode_discount == 0 && $cart->membership_discount ==0 && $cart->reward_discount == 0 && $cart->credit_discount == 0) && $cart->discount != 0.0000))
                                <th>Discount</th>
                            @endif
                            <th>Amount <br/> (incl.taxes)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  $product_weights=$product_categories=$get_categories=array(); ?>
                            @foreach ($cart->items as $item)
                                <tr class="products">
                                    <td>{{ $item->name }}</td>
                                    <td>{{ core()->currency( $item->base_price ) }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <?php //$same_product_ids = Webkul\Shop\Http\Controllers\OnepageController::GetDiscountProductIds(); 
                                $discount_price=$product_price=0; ?>
                                

                                @if($cart->promocode_discount != 0 || $cart->membership_discount !=0 || $cart->reward_discount != 0 ||  $cart->credit_discount != 0)
                                    @if($item->discount_applied == 1)
                                        <?php 
                                            $product_price = ($item->total);
                                            $discount_price = $item->discount_applied_amount;
                                        ?>
                                        @if($discount_price != 0)
                                            <td>{{ core()->currency($discount_price) }}</td>
                                        @endif
                                    @else
                                        <td>NA</td>
                                    @endif
                                @elseif(($cart->promocode_discount == 0 && $cart->membership_discount == 0 && $cart->reward_discount == 0 && $cart->credit_discount == 0) && $cart->discount != 0.0000)
                                    @if ($item->discount_applied == 0 && $cart->discount != 0.0000)
                                        <?php 
                                            $product_price = ($item->total - $item->discount_amount);
                                        ?>
                                        @if($item->discount_amount != 0)
                                            <td>{{ core()->currency($item->discount_amount) }}</td>
                                        @else
                                            <td>NA</td>
                                        @endif
                                    @else
                                        <td>NA</td>
                                    @endif
                                @endif
                                
                                    <?php
                                        $total_price_item = 0;
                                        
                                        if($cart->promocode_discount != 0 || $cart->membership_discount !=0 || $cart->reward_discount != 0 || $cart->credit_discount != 0){
                                            $total_price_item = $item->total - $discount_price; 
                                        }else if(($cart->promocode_discount == 0 && $cart->membership_discount == 0 && $cart->reward_discount == 0 && $cart->credit_discount) && $cart->discount != 0.0000){
                                            $total_price_item = $item->total - $item->discount_amount; 
                                        }else{
                                            $total_price_item = $item->total - $item->discount_amount - $discount_price;
                                        }
                                
                                        $get_tax = DB::table('tax_rates')->where('state',$user->state_id)->first(); 
                                        $item_tax = $total_price_item * ($get_tax->tax_rate / 100); 
                                        $price_with_tax = $total_price_item + $item_tax; 
                                     ?>
                                    <td>{{ core()->currency($price_with_tax) }}</td>
                                </tr>
                                <?php    
                                    $product_weights[] = $item->total_weight; 
                                    $get_categories = DB::table('product_categories')->where('product_id', $item->product_id)->pluck('category_id')->toArray();
                                    $product_categories = array_merge($product_categories, $get_categories);
                                ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>


    <?php  
        $prod_quantity = $cart->items_qty; 
        $users_table = \DB::table('users')->where('id',$user->id)->first();
        $role_id = $users_table->role_id;
        $list_offers = \DB::table('discounts as d')
                                ->where('d.is_deleted',0)
                                ->leftjoin('promotions as pm','pm.discount_id','d.id')
                                ->leftjoin('promotion_ordered_category as pc','pc.discount_id','d.id')
                                ->where('pm.min_order_qty','<=',$prod_quantity)
                                ->whereIn('pm.for_whom',[0,$role_id])
                                ->whereIn('pc.category_id',$product_categories)
                                ->where('d.discount_type',4)->get();

        $use_promotion = false;
        if(count($list_offers) > 0)
            $use_promotion = true;

        $shipping_amount = 0;
        $total_product_weights = array_sum($product_weights);
        $get_weight_ranges = DB::table('shipping')->where('status',1)->get();

        foreach($get_weight_ranges as $label => $get_weight_range)
        {
            if($total_product_weights >= $get_weight_range->min_weight && $total_product_weights <= $get_weight_range->max_weight)
            {
                $shipping_amount = $get_weight_range->shipping_amount;
                break;
            }
        }
        //dealer or admin clear cart -> need to forgot discounts        
        
        $membership_details = session('membership_details');
        $promocode_details = session('promocode_details');
        $reward_details = session('reward_details');
        $promotion_details = session('promotion_details');
        $credit_discount = session('credit_discount_details');

        //dd($promotion_details);
        $promocode_percent=$membership_percent=$promocode_price=$reward_coins=$membership_price=$promotion_price=$credit_discount_price=$credit_percent=0;
        $earn_points=$apply_membership=$promocode_discount_id=$membership_discount_id=$disable_discount=$credit_discount_id=$couponcode="";
        $earn_points=1;
        
        if(isset($promocode_details))
        {
            $promocode_price = $promocode_details['promo_code_price'];
            $promocode_discount_id = $promocode_details['promo_code_discount_id'];
            $promocode_percent = $promocode_details['discount_per'];
            $earn_points = $promocode_details['earn_points'];
            $apply_membership = $promocode_details['apply_membership'];
            $couponcode = $promocode_details['coupon_code'];
        }

        if(isset($credit_discount))
        {
            $credit_discount_price = $credit_discount['credit_discount_price'];
            $credit_discount_id = $credit_discount['credit_discount_id'];
            $credit_percent = $credit_discount['discount_per'];
            $earn_points = $credit_discount['earn_points'];
            $apply_membership = $credit_discount['apply_membership'];
            $couponcode = $credit_discount['coupon_code'];
        }

        
        if(isset($membership_details))
        {
            $membership_price = $membership_details['mem_discount_price'];
            $membership_discount_id = $membership_details['membership_discount_id'];
            $membership_percent = $membership_details['discount_per'];
            $earn_points=0;
            $couponcode = $membership_details['coupon_code'];
        } 

        if(isset($promotion_details))
        {
            $promotion_price = 1;
            $earn_points = $promotion_details['earn_points'];
        }
       
        if(isset($reward_details)){
            $reward_coins = $reward_details['coin_value'];
            $earn_points=0;
        }

        if($promocode_details != null && $apply_membership == 0 || $credit_discount != null && $apply_membership == 0){
            $disable_discount = 'disabled';
        }

        if($promocode_details != null || $membership_details != null || $reward_details != null || $promotion_details != null || $credit_discount != null){
             $disable_discount = 'disabled';
        }
        

        if($promocode_price ==0 && $reward_coins==0 && $membership_price==0 && $promotion_price==0 && $credit_discount_price==0)
            $get_price = abs($cart->sub_total - $cart->discount);
        else
            $get_price = abs($cart->sub_total - $promocode_price - $reward_coins - $membership_price - $credit_discount_price);

        $sub_total_shipping = $get_price + $shipping_amount;
        //dd($promocode_price, $reward_coins, $membership_price, $promotion_price, $credit_discount_price);
        
        $get_tax = DB::table('tax_rates')->where('state',$user->state_id)->first();

        $tax_amount = $sub_total_shipping * ($get_tax->tax_rate / 100);
        $total_amount = $sub_total_shipping + $tax_amount;
        $total_amount = round($total_amount, 2);

        $discount_applied['applied'] = true;
        if(isset($membership_details)){
            $discount_applied['coupon_code'] = $membership_details['coupon_code'];
            $discount_applied['type'] = 'mem';

        }elseif(isset($credit_discount)){
            $discount_applied['coupon_code'] = $credit_discount['coupon_code'];
            $discount_applied['type'] = 'credit';

        }elseif(isset($promocode_details)){
            $discount_applied['coupon_code'] = $promocode_details['coupon_code'];
            $discount_applied['type'] = 'promo';

        }elseif(isset($promotion_details)){
            $discount_applied['quandity_selected'] = $promotion_details['total_qty_selected'];
            $discount_applied['type'] = 'promotion';

        }else{
            $discount_applied['applied'] = false;
        }
        
    ?>
    </div>
                <!--start discount-->
                    <div class="col-md-12 col-xs-12 discount-sec">
                        <h3>Apply Discount</h3>

                        @if($discount_applied['applied'])
                            <div class="cart-discount-applied">
                                @if($discount_applied['type']=='promotion')
                                    <span class="pull-left text-left"><span class="text-promotion">You have selected {{ $discount_applied['total_qty_selected'] }} products</span></span>
                                @else
                                    <span class="pull-left text-left"><div class="coupon-code">{{$discount_applied['coupon_code']}}</div>Offer applied on the bill</span>
                                @endif
                            
                                <span class="pull-right remove-discount remove-{{$discount_applied['type']}} text-right"><button class="remove" type="button">REMOVE</button></span>
                            </div>
                        @else
                            <div class="discount">
                                <button type="button" {{ $disable_discount }} class="btn text-center apply-coupon" data-toggle="modal" data-target="#availableDiscounts">Apply Discount</button>
                            </div>
                        @endif
                    </div>
                <!--end discount-->

                <!--start reward points-->
                    <div class="col-md-12 col-xs-12 discount-sec">
                        <h3 class="pull-left">Your Reward Points</h3>
                        
                        @if(isset($reward_details))
                            <span class="pull-right">{{ $reward_details['remaining_avail_points'] }} Points</span>
                            <div class="reward-applied">
                                <span class="pull-left text-left"><div class="reward-point">{{$reward_details['reward_points']}} points ({{$reward_details['coin_value'] }})</div>Reward applied on the bill</span>
                                <span class="pull-right remove-reward text-right"><button class="remove" type="button">REMOVE</button></span>
                                <input type="hidden" name="reward_points_used" value="{{$reward_details['reward_points']}}">
                            </div>
                        @else
                            <span class="pull-right">{{ $availableRewardPoints }} Points</span>
                            <div class="reward">
                                <div class="control-group">
                                    <input type="text" {{ $disable_discount }} class="control reward-points" name="reward_points_used" value="" placeholder="Enter reward points">
                                    <button type="button" {{ $disable_discount }} class="btn text-center apply-reward">Apply</button>
                                </div>
                            </div>
                        @endif
                    </div>
                <!--end reward points-->
                
                
                <input type="hidden" name="base_discount_amount" class="base-discount-amount" value="{{ $cart->discount }}">

                <input type="hidden" name="mem_discount_percent" class="mem-discount-percent" value="{{ $membership_percent }}">
                <input type="hidden" name="mem_discount_price" class="mem-discunt-price" value="{{ $membership_price }}">
                <input type="hidden" name="membership_discount_id" class="promocde-discount-id" value="{{ $membership_discount_id }}">

                <input type="hidden" name="promo_code_percent" class="promo-code-percent" value="{{ $promocode_percent }}">
                <input type="hidden" name="promo_code_price" class="promo-cde-price" value="{{ $promocode_price }}">
                <input type="hidden" name="promocode_discount_id" class="promocde-discount-id" value="{{ $promocode_discount_id }}">

                <input type="hidden" name="credit_percent" class="promo-code-percent" value="{{ $credit_percent }}">
                <input type="hidden" name="credit_discount_price" class="promo-cde-price" value="{{ $credit_discount_price }}">
                <input type="hidden" name="credit_discount_id" class="promocde-discount-id" value="{{ $credit_discount_id }}">
                
                <input type="hidden" name="reward_coin" class="rewrd-coin" value="{{$reward_coins}}">

                <input type="hidden" name="couponcode" class="couponcode" value="{{$couponcode}}">
                <input type="hidden" name="earn_points" class="earn-pnt" value="{{$earn_points}}">
                <input type="hidden" name="special_price" class="spl-price" value="{{$cart->discount != 0.0000 ? $cart->sub_total - $cart->discount : 0}}">


                

                <div class="order-amount col-md-12 col-xs-12">
                    <div class="amount-sec">
                        <div class="sub-amount col-md-12 col-xs-12">
                            <span class="pull-left text-left">Sub Total:</span>
                            <span class="pull-right text-right">{{ core()->currency($cart->sub_total) }}</span>
                        </div>
                        <?php $discounted_subtotal=0; ?>
                        @if($cart->discount != 0.0000 && ($promocode_price ==0 && $reward_coins==0 && $membership_price==0 && $promotion_price==0 && $credit_discount_price==0))
                            <div class="special-price col-md-12 col-xs-12" style="color:#12DDFB;">
                                <span class="pull-left text-left">Special Price:</span>
                                <span class="pull-right text-right mem-discount-price">{{ core()->currency($cart->sub_total - $cart->discount) }}</span>
                                <?php $discounted_subtotal = round($cart->base_sub_total - $cart->base_discount,2); ?>
                            </div>
                        @endif

                        @if(isset($membership_details))
                        <div class="discount-apply col-md-12 col-xs-12">
                            <span class="pull-left text-left">Membership Discount:</span>
                            <span class="pull-right text-right mem-discount-price">- {{core()->currency($membership_details['mem_discount_price']) }}</span>
                        </div>
                        <?php $discounted_subtotal = round($cart->base_sub_total - $cart->membership_discount,2); ?>
                        @endif

                        @if(isset($promocode_details))
                            <div class="discount-apply col-md-12 col-xs-12">
                              <span class="pull-left text-left">Promo Code Discount:</span>
                              <span class="pull-right text-right promo-code-price">- {{ core()->currency($promocode_details['promo_code_price']) }}</span>
                            </div>
                            <?php $discounted_subtotal = round($cart->base_sub_total - $cart->promocode_discount,2); ?>
                        @endif

                        @if(isset($credit_discount))
                            <div class="discount-apply col-md-12 col-xs-12">
                              <span class="pull-left text-left">Advance Payment Discount:</span>
                              <span class="pull-right text-right credit-discount-price">- {{ core()->currency($credit_discount['credit_discount_price']) }}</span>
                            </div>
                            <?php $discounted_subtotal = round($cart->base_sub_total - $cart->credit_discount,2); ?>
                        @endif

                        @if(isset($reward_details))
                        <div class="reward-points col-md-12 col-xs-12">
                            <span class="pull-left text-left">Reward Points:</span>
                            <span class="pull-right text-right reward-coin">- {{ core()->currency($reward_coins) }}</span>
                        </div>
                        <?php $discounted_subtotal = round($cart->base_sub_total - $cart->reward_discount,2); ?>
                        @endif
                        @if($discounted_subtotal != 0)
                            <div class="tax-amount col-md-12 col-xs-12">
                                <span class="pull-left text-left">Discounted Subtotal</span>
                                <span class="pull-right text-right">{{ core()->formatBasePrice($discounted_subtotal) }}</span>
                            </div>
                         @endif
                        <div class="shipping-charge col-md-12 col-xs-12">
                            <span class="pull-left text-left">Shipping Cost:</span>
                            <span class="pull-right text-right">+ {{ $shipping_amount }}</span>
                        </div>

                        <?php if($user->state_id == 22) {
                                $cart_tax = $tax_amount/2; ?>
                            <div class="tax-amount col-md-12 col-xs-12">
                                <span class="pull-left text-left">CGST:</span>
                                <span class="pull-right text-right">+ {{ number_format($cart_tax,2) }}</span>
                            </div>
                            <div class="tax-amount col-md-12 col-xs-12">
                                <span class="pull-left text-left">SGST:</span>
                                <span class="pull-right text-right">+ {{ number_format($cart_tax,2) }}</span>
                            </div>
                          <?php  } else { ?>
                            <div class="shipping-charge col-md-12 col-xs-12">
                                <span class="pull-left text-left">IGST:</span>
                                <span class="pull-right text-right">+ {{ number_format($tax_amount,2) }}</span>
                            </div>
                        <?php } ?>
                                                   
                        <div class="total-amounts col-md-12 col-xs-12">
                            <span class="pull-left text-left">Total Amount:</span>
                            <span class="pull-right total-amount text-right">{{ core()->currency($total_amount) }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="checkout-right col-md-7 col-xs-12">
            <div class="sec-inner col-md-12 col-xs-12">
                <div class="delivery-addr border-bottom col-md-12 col-xs-12">
                    <h3>Delivery Address</h3>
                    <?php $delivery_address = DB::table('user_addresses')
                                    ->where('customer_id',$user->id)
                                    ->where('delivery_selected',1)
                                    ->latest()
                                    ->first();
                               if(!$delivery_address)
                               {
                                    $delivery_address = DB::table('user_addresses')
                                                ->where('default_address',1)
                                                ->where('delivery_selected',0)
                                                ->where('customer_id',$user->id)
                                                ->latest()
                                                ->first();
                                }
                                $route_name='admin';
                                if($user->role_id == 3){
                                    $route_name = 'customer';
                                }
                                    
                    ?>
                    <div class="addr-sec control-group" :class="[errors.has('delivery_address') ? 'has-error' : '']">
                        <div class="addr-left text-center col-md-1 col-xs-2">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="addr-right col-md-7 col-xs-10">
                            <?php if(isset($delivery_address)) { ?> 
                            <p class="bold">{{ str_replace('-',' ',$delivery_address->name) }}</p>
                            <p>{{ $delivery_address->address }}</p>
                            <p>{{ Webkul\Customer\Models\Cities::GetCityName($delivery_address->city)}}, {{ Webkul\Customer\Models\States::GetStateName($delivery_address->state)}}, India -
                            {{ $delivery_address->postcode }}</p>

                            <input type="hidden" name="delivery_address" class="delivery-address" v-validate="'required'" value="{{$delivery_address->id}}"><?php } ?>
                            <div class="add-new">
                                <a href="{{route($route_name.'.address.create')}}?type=delivery&returnto=checkout"><i class="fa fa-plus"></i>Add A New Address</a>
                            </div>
                        </div>
                        <?php if(isset($delivery_address)) { ?>
                            <div class="top-right col-xs-12 text-right col-md-4">
                                <a href="{{route($route_name.'.address.edit', base64_encode($delivery_address->id))}}?returnto=checkout">Edit</a>
                                <a href="{{route($route_name.'.address.change')}}?type=delivery">Change</a>
                            </div>
                        <?php } else { ?>
                        <input type="hidden" name="delivery_address" class="delivery-address" v-validate="'required'" value="">
                        <?php } ?>
                        <span class="control-error" style="float:left;margin:10px 0;" v-if="errors.has('delivery_address')">@{{ errors.first('delivery_address') }}</span>
                    </div>
                </div>

                <div class="section border-bottom billing-addr col-md-12 col-xs-12">
                    <h3>Billing Address</h3>
                    <?php $billing_address = DB::table('user_addresses')
                                        ->where('customer_id',$user->id)
                                        ->where('billing_selected',1)
                                        ->latest()
                                        ->first();
                        if(!$billing_address)
                        {
                            $billing_address = DB::table('user_addresses')
                                                ->where('default_address',1)
                                                ->where('billing_selected',0)
                                                ->where('customer_id',$user->id)
                                                ->latest()
                                                ->first();
                        } 
                                       
                    ?>
                    <div class="addr-sec control-group" :class="[errors.has('billing_address') ? 'has-error' : '']">
                        <div class="addr-left text-center col-md-1 col-xs-2">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="addr-right col-md-7 col-xs-10">
                            <?php if(isset($billing_address)) { ?>
                                <p class="bold">{{ str_replace('-',' ',$billing_address->name) }}</p>
                                <p>{{ $billing_address->address }}</p>
                            <p>{{ Webkul\Customer\Models\Cities::GetCityName($billing_address->city)}}, {{ Webkul\Customer\Models\States::GetStateName($billing_address->state)}}, India -
                            {{ $billing_address->postcode }}</p>
                            <input type="hidden" name="billing_address" class="billing-address" v-validate="'required'" value="{{$billing_address->id}}"><?php } ?>
                            <div class="add-new">
                                <a href="{{route($route_name.'.address.create')}}?type=billing&returnto=checkout"><i class="fa fa-plus"></i> Add A New Address</a>
                            </div>
                        </div>
                        <?php if(isset($billing_address)) { ?>
                            <div class="top-right col-xs-12 text-right col-md-4">
                            <a href="{{route($route_name.'.address.edit', base64_encode($billing_address->id))}}?returnto=checkout">Edit</a>
                            <a href="{{route($route_name.'.address.change')}}?type=billing">Change</a>
                        </div>
                        <?php } else { ?>
                        <input type="hidden" name="billing_address" class="billing-address" v-validate="'required'" value="">
                        <?php } ?>
                        <span class="control-error" style="float:left;margin:10px 0;" v-if="errors.has('billing_address')">@{{ errors.first('billing_address') }}</span>
                    </div>
                </div>

                <div class="payment-section col-md-12 col-xs-12">
                    <h3 class="required">Choose Payment Method</h3>
                    <div class="list-payments control-group" :class="[errors.has('payment') ? 'has-error' : '']">
                        <?php $cashondelivery = DB::table('core_config')->where('code','sales.paymentmethods.cashondelivery.active')->first();
                      
                        $razorpay = DB::table('core_config')->where('code','sales.paymentmethods.razorpay.active')->first();
                        $cashondelivery = DB::table('core_config')->where('code','sales.paymentmethods.cashondelivery.active')->first();
                        $paypal = DB::table('core_config')->where('code','sales.paymentmethods.paypal_standard.active')->first();

                        

                        $prepayment = DB::table('core_config')->where('code','sales.paymentmethods.prepayment.active')->first(); ?>
                        @if($cashondelivery->value == 1)
                            <div class="each-payment">
                                <span class="border-padding col-md-12 col-xs-12"><input type="radio" id="cash_on_delivery" name="payment" class="select-payment cashondelivery-payment" value="cashondelivery"  v-validate="'required'" @if(isset($credit_discount)) disabled @endif><label for="cash_on_delivery">Cash On Delivery</label></span>
                            </div>
                        @endif
                        @if($prepayment->value == 1 && $user->prepayment_available)
                            <div class="each-payment">
                                <span class="border-padding col-md-12 col-xs-12"><input type="radio" id="prepayment" name="payment" class="select-payment prepayment-payment" value="prepayment"  v-validate="'required'" @if(isset($credit_discount)) disabled @endif><label for="prepayment">PrePayment</label></span>
                            </div>
                        @endif
                        @if($paypal->value == 1)
                            <div class="each-payment">
                                <span class="border-padding col-md-12 col-xs-12"><input type="radio" id="paypal" name="payment" class="select-payment" value="paypal"  v-validate="'required'" @if(isset($credit_discount)) disabled @endif><label for="paypal">Paypal</label></span>
                            </div>
                        @endif
                        @if($razorpay->value == 1)
                            <div class="each-payment">
                                <span class="border-padding col-md-12 col-xs-12"><input type="radio" id="razorpay" name="payment" class="select-payment razorpay-payment" value="razorpay" @if(isset($credit_discount)) disabled @else checked @endif><label for="razorpay"  v-validate="'required'">Razorpay</label></span>
                            </div>
                        @endif

                        @if($user->credit_available == 1)
                                <?php
                                    $payment_name =  "Pay with ".(($user->role_id=='2')?'credit':'advance');
                                ?>
                                <div class="each-payment">
                                    <span class="border-padding col-md-12 col-xs-12">
                                        @if($paylaterwithcredit->value == 1 && ($total_amount <= $user_credit))
                                            <input type="radio" id="paylaterwithcredit" name="payment" class="select-payment paylaterwithcredit-payment" value="paylaterwithcredit" @if(isset($credit_discount)) checked @endif>
                                            <label for="paylaterwithcredit"  v-validate="'required'" style="width: 80%;">
                                                {{$payment_name}} {{ '(Available: '.core()->currency($user_credit).')' }} @if(isset($credit_discount)) {!! '<br><br>Discount only available for Advance Payment' !!} @endif
                                            </label>
                                        @else
                                            <input type="radio" id="paylaterwithcredit" name="payment" class="select-payment paylaterwithcredit-payment" value="paylaterwithcredit" disabled>
                                            <label for="paylaterwithcredit"  v-validate="'required'" style="width: 80%;">
                                                {{$payment_name}}  {{ '(Available: '.core()->currency($user_credit).')' }}  <br><br> <span style="color:#ca0000;"> {!! "You don't have enough balance</span>" !!} </span>
                                            </label>
                                        @endif
                                    </span>
                                </div>
                        @endif
                        
                        <span class="control-error" v-if="errors.has('payment')">@{{ errors.first('payment') }}</span>
                    </div>
                </div>

                <div class="order-amount col-md-12 col-xs-12">
                    <div class="amount-sec">
                        <div class="sub-amount col-md-12 col-xs-12">
                            <span class="pull-left text-left">Sub Total:</span>
                            <span class="pull-right text-right">{{ core()->currency($cart->sub_total) }}</span>
                        </div>
                        <?php $discounted_subtotal=0; ?>
                        @if($cart->discount != 0.0000 && ($promocode_price ==0 && $reward_coins==0 && $membership_price==0 && $promotion_price==0 && $credit_discount_price==0))
                        <div class="special-price col-md-12 col-xs-12" style="color:#12DDFB;">
                            <span class="pull-left text-left">Special Price:</span>
                            <span class="pull-right text-right mem-discount-price">{{ core()->currency($cart->sub_total - $cart->discount) }}</span>
                        </div>
                        <?php $discounted_subtotal = round($cart->base_sub_total - $cart->base_discount,2); ?>
                        @endif

                        @if(isset($membership_details))
                        <div class="discount-apply col-md-12 col-xs-12">
                            <span class="pull-left text-left">Membership Discount:</span>
                            <span class="pull-right text-right mem-discount-price">- {{ core()->currency($membership_details['mem_discount_price']) }}</span>
                        </div>
                        <?php $discounted_subtotal = round($cart->base_sub_total - $cart->membership_discount,2); ?>
                        @endif

                        @if(isset($promocode_details))
                        <div class="discount-apply col-md-12 col-xs-12">
                            <span class="pull-left text-left">Promo Code Discount:</span>
                            <span class="pull-right text-right promo-code-price">- {{ core()->currency ($promocode_details['promo_code_price']) }}</span>
                        </div>
                        <?php $discounted_subtotal = round($cart->base_sub_total - $cart->promocode_discount,2); ?>
                        @endif

                        @if(isset($credit_discount))
                            <div class="discount-apply col-md-12 col-xs-12">
                              <span class="pull-left text-left">Advance Payment Discount:</span>
                              <span class="pull-right text-right credit-discount-price">- {{ core()->currency($credit_discount['credit_discount_price']) }}</span>
                            </div>
                            <?php $discounted_subtotal = round($cart->base_sub_total - $cart->credit_discount,2); ?>
                        @endif

                        @if(isset($reward_details))
                        <div class="reward-points col-md-12 col-xs-12">
                            <span class="pull-left text-left">Reward Points:</span>
                            <span class="pull-right text-right reward-coin">- {{ core()->currency($reward_coins) }}</span>
                        </div>
                        <?php $discounted_subtotal = round($cart->base_sub_total - $cart->reward_discount,2); ?>
                        @endif
                        @if($discounted_subtotal != 0)
                            <div class="tax-amount col-md-12 col-xs-12">
                                <span class="pull-left text-left">Discounted Subtotal</span>
                                <span class="pull-right text-right">{{ core()->formatBasePrice($discounted_subtotal) }}</span>
                            </div>
                        @endif
                        <div class="shipping-charge col-md-12 col-xs-12">
                            <span class="pull-left text-left">Shipping Cost:</span>
                            <span class="pull-right text-right">+ {{ $shipping_amount }}</span>
                        </div>
                        <?php if($user->state_id == 22) {
                                $cart_tax = $tax_amount/2; ?>
                            <div class="tax-amount col-md-12 col-xs-12">
                                <span class="pull-left text-left">CGST:</span>
                                <span class="pull-right text-right">+ {{ number_format($cart_tax,2) }}</span>
                            </div>
                            <div class="tax-amount col-md-12 col-xs-12">
                                <span class="pull-left text-left">SGST:</span>
                                <span class="pull-right text-right">+ {{ number_format($cart_tax,2) }}</span>
                            </div>
                          <?php  } else { ?>
                            <div class="shipping-charge col-md-12 col-xs-12">
                                <span class="pull-left text-left">IGST:</span>
                                <span class="pull-right text-right">+ {{ number_format($tax_amount,2) }}</span>
                            </div>
                        <?php } ?>
                        
                        <div class="total-amounts col-md-12 col-xs-12">
                            <span class="pull-left text-left">Grand Total:</span>
                            <span class="pull-right total-amount text-right">{{ core()->currency($total_amount) }}</span>
                        </div>
                    </div>
                    
                    <input type="hidden" class="grand-total" name="grand_total" value="{{$cart->grand_total}}">
                    <input type="hidden" class="total-amunt" name="total_amount" value="{{ $total_amount }}">


                    <input type="hidden" name="shipping_method" value="free_free">
                    <input type="hidden" class="shipping-amount" name="shipping_amount" value="{{ $shipping_amount }}">
                    <input type="hidden" class="tax-total" name="tax_total" value="{{ $tax_amount }}">
                    <input type="hidden" class="payment_id" name="payment_id" value="">
                    <div class="col-md-12 col-xs-12 section place-order">
                        {{-- <button type="submit" class="btn btn-lg btn-primary proceed-pay">
                                Proceed To Pay
                            </button> --}}
                        <a href="javascript:void(0)" class="btn btn-lg btn-primary order_by_razorpay" data-amount="{{ $total_amount }}" data-id="3" @if(isset($credit_discount)) style="display:none" @endif>Pay with RazorPay</a>
                        <button type="submit" class="btn btn-lg btn-primary order_by_without_payment" data-amount="{{ $total_amount }}" data-id="4">Confirm Order</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
    </div>


    <div class="modal fade" id="availableDiscounts" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Available Offers</h4>
                </div>
                <div class="modal-body">
                    <?php
                        $types = ['mem' => 2, 'promo'=>3, 'credit'=>5, 'promotion'=>4];
                        $discount_present_flag = 0;
                        foreach($types as $dis => $type){
                            if($type==2 && $user->role_id == 2 ||
                              ($type==5 && !($user->role_id != 1 && $user->role_id != 4 && $paylaterwithcredit->value == 1 && $user->credit_available && ($total_amount <= $user->credit_balance)))) continue;


                            $coupons = Webkul\Shop\Http\Controllers\OnepageController::ListAllCoupons($type);
                            if(!empty($coupons['allOffers'])){
                                $discount_present_flag =1;
                                echo '<h3>';
                                    switch($dis){
                                        case 'mem':
                                            echo 'Membership Discount';        
                                            break; 
                                        case 'promo':
                                            echo 'Promo Code Discount';        
                                            break; 
                                        case 'credit':
                                            echo 'Advance Payment Discount';        
                                            break; 
                                        case 'promotion':
                                            echo 'Promotions Discount';        
                                            break;
                                        default:
                                            echo 'Discount';        
                                            break;
                                    }
                                echo '</h3>';
                                
                                echo '<div style="padding-left:10px; border-bottom: 1px solid #ddd;">'.$coupons['allOffers'].'</div>';
                            }
                        }
                        if($discount_present_flag==0) echo '<h4 style="text-align:center">No discounts available!</h4>';
                    ?>
                </div>
                <div class="modal-footer" style="border-top:unset">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

 @push('scripts')
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            const it_credit_payment = <?= isset($credit_discount)?'true':'false' ?> 
            if(it_credit_payment){
                $('a.order_by_razorpay').hide();
                $('button.order_by_without_payment').show();
                $('button.order_by_without_payment').addClass('order_by_paylaterwithcredit');
                $('.place-order .proceed-pay').hide();
            }



            $(document).on('click', '.order_by_cashondelivery', function(event) {
                $(this).css('opacity', '0.6'); 
                $(this).css('pointer-events', 'none'); 
            });

            $(document).on('click', '.order_by_prepayment', function(event) {
                $(this).css('opacity', '0.6'); 
                $(this).css('pointer-events', 'none'); 
            });

            $(document).on('click', '.order_by_paylaterwithcredit', function(event) {
                var credit_balance = "{{ $user_credit }}";
                var total_amount = $(this).data('amount');

                if(total_amount <= credit_balance){
                    $(this).css('opacity', '0.6'); 
                    $(this).css('pointer-events', 'none'); 
                }else{
                    alert("You cannot make this payment. You have don\'t have enough Advance balance! Use other payment method.");
                    event.preventDefault();
                    return false;
                }

            });

                    $('.place-order .proceed-pay').show();

                    $('.select-payment').change(function(){
                        $('a.order_by_razorpay').hide();
                        $('.place-order .proceed-pay').show();
                        $('button.order_by_without_payment').hide();
                        $('button.order_by_without_payment').removeClass('order_by_cashondelivery');
                        $('button.order_by_without_payment').removeClass('order_by_paylaterwithcredit');
                         $('button.order_by_without_payment').removeClass('order_by_prepayment');
                        if($(this).hasClass('razorpay-payment')){
                            $('a.order_by_razorpay').show();
                            $('.place-order .proceed-pay').hide();
                            $('.order_by_cashondelivery').hide();
                        }
                        if($(this).hasClass('cashondelivery-payment')) {
                            $('a.order_by_razorpay').hide();
                            $('button.order_by_without_payment').show();
                            $('button.order_by_without_payment').addClass('order_by_cashondelivery');
                            $('.place-order .proceed-pay').hide();
                        }
                         if($(this).hasClass('prepayment-payment')) {
                            $('a.order_by_razorpay').hide();
                            $('button.order_by_without_payment').show();
                            $('button.order_by_without_payment').addClass('order_by_prepayment');
                            $('.place-order .proceed-pay').hide();
                        }
                        if($(this).hasClass('paylaterwithcredit-payment')) {
                            $('a.order_by_razorpay').hide();
                            $('button.order_by_without_payment').show();
                            $('button.order_by_without_payment').addClass('order_by_paylaterwithcredit');
                            $('.place-order .proceed-pay').hide();
                        }
                        
                    });
            
            
                     $.ajaxSetup({
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       }
                     }); 
                    $('body').on('click', 'a.order_by_razorpay', function(e){
                       var bill_addr = $('.billing-address').val();
                       var deliv_addr = $('.delivery-address').val();
                       if(deliv_addr != "" && bill_addr != "") {
                            var totalAmount = $(this).attr("data-amount");
                            console.log(parseInt(totalAmount*100));
                            console.log(totalAmount);
                            var amount_in_paise = parseInt(totalAmount*100);
                            var options = {
                               "key": "{{ env('RAZORPAY_KEY') }}",
                               "amount": amount_in_paise, // 2000 paise = INR 20
                               "currency": "INR",
                               "name": "Genxt Payment",
                               "description": "Pay with RazorPay",
                               "image": "{!! bagisto_asset('images/login-logo.png') !!}",
                               "handler": function (response){
                               	    $('.overlay-checkout').css('display','inline-flex');
                                      $.ajax({
                                          url: '{{route("razorpay.payment")}}',
                                          type: 'post',
                                          dataType: 'json',
                                          data: {
                                              '_token': '{{ csrf_token() }}',
                                              payment_id: response.razorpay_payment_id,
                                              amount : totalAmount,
                                              response_data : response
                                          }, 
                                          success: function (msg) {                                              
                                              $('.overlay-checkout').css('display','inline-flex');
                                              $('#order-form').find('.payment_id').val(msg.payment_id);
                                              $('#order-form').submit();      
                                          },
                                          error: function(error, exception) {   
                                              let error_array = {
                                                  statusCode: error.status,
                                                  exception: exception, 
                                                  razorpaydetails: response,
                                                  textStatus: error.responseText,
                                                };
                                              let notify_msg = JSON.stringify(error_array);
                                              $.ajax({
                                                  url: '{{route("onepage.notifystatus")}}',
                                                  type: 'post',
                                                  dataType: 'json',
                                                  data: {
                                                      '_token': '{{ csrf_token() }}',
                                                      success: false,
                                                      datanofify:notify_msg
                                                  },
                                                  success: function(res){}
                                              });
                                          }
                                    });                                   
                               },
                              "prefill": {
                                   "contact": '{{ $user->phone_number }}',
                                   "email":   '{{ $user->email }}',
                               },
                              "theme": {
                                   "color": "#528FF0"
                              }
                            };

                            
                            
                            var rzp1 = new Razorpay(options);
                            rzp1.open();
                            e.preventDefault();
                        }else{
                            if(bill_addr == ""){
                                $('.billing-address').parent().addClass('has-error');
                                $('<span class="control-error" style="float: left; margin: 10px 0px;">The billing address field is required.</span>').insertAfter(".billing-address"); 
                            }
                            if(deliv_addr == ""){
                                $('.delivery-address').parent().addClass('has-error');
                                $('<span class="control-error" style="float: left; margin: 10px 0px;">The delivery address field is required.</span>').insertAfter(".delivery-address"); 
                            }
                            
                               
                        }
                    });

        var reward_price = $('body').find('.rewrd-coin').val();
        var shipping_amount = $('body').find('.shipping-amount').val();
        var _token = $("input[name='_token']").val();


        $('.apply-membership').click(function(){
           $('#availableDiscounts').modal('hide'); 
            var discount_id = $(this).parent().find('.discount-id').val();
            var discount_type = $(this).parent().find('.discount-type').val();
            var discount_per = $(this).parent().find('.discount-per').val();
            var promo_code_price = $('body').find('.promo-cde-price').val();
            var coupon_code = $(this).parent().find('.coupon-code').val();
            $.ajax
            ({ 
                url: "{{route('shop.checkout.apply.membership')}}",
                data: { _token:_token,discountId:discount_id,discountType:discount_type,promoCodePrice:promo_code_price,discountPer:discount_per,couponCode:coupon_code,rewardPrice:reward_price,shippingAmount:shipping_amount},
                type: 'POST',
                dataType: "json",
                success: function(result)
                {
                    location.reload(true);
                },
                error: function(xhr, textStatus, errorThrown) {
                    console.log(xhr.responseText);
                }
            });
        });



        $('.apply-promocode').click(function(){
            $('#availableDiscounts').modal('hide'); 
            var discount_id = $(this).parent().find('.discount-id').val();
            var discount_type = $(this).parent().find('.discount-type').val();
            var discount_per = $(this).parent().find('.discount-per').val();
            var mem_discount_price = $('body').find('.mem-discunt-price').val();
            var coupon_code = $(this).parent().find('.coupon-code').val();
            var apply_membership = $(this).parent().find('.apply-membershp').val();
            var earn_points = $(this).parent().find('.earn-point').val();
            
            $.ajax
            ({ 
                url: "{{route('shop.checkout.apply.promocode')}}",
                data: { _token:_token,discountId:discount_id,discountType:discount_type,memDiscountPrice:mem_discount_price,discountPer:discount_per,couponCode:coupon_code,applyMembership:apply_membership,earnPoints:earn_points,rewardPrice:reward_price,shippingAmount:shipping_amount},
                type: 'POST',
                dataType: "json",
                success: function(result)
                {
                    location.reload(true);
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                }
            });
        });


        $('.apply-creditdiscount').click(function(){
            $('#availableDiscounts').modal('hide'); 
            var discount_id = $(this).parent().find('.discount-id').val();
            var discount_type = $(this).parent().find('.discount-type').val();
            var discount_per = $(this).parent().find('.discount-per').val();
            var mem_discount_price = $('body').find('.mem-discunt-price').val();
            var coupon_code = $(this).parent().find('.coupon-code').val();
            var apply_membership = $(this).parent().find('.apply-membershp').val();
            var earn_points = $(this).parent().find('.earn-point').val();
            
            $.ajax
            ({ 
                url: "{{route('shop.checkout.apply.credit-discount')}}",
                data: { _token:_token,discountId:discount_id,discountType:discount_type,memDiscountPrice:mem_discount_price,discountPer:discount_per,couponCode:coupon_code,applyMembership:apply_membership,earnPoints:earn_points,rewardPrice:reward_price,shippingAmount:shipping_amount},
                type: 'POST',
                dataType: "json",
                success: function(result)
                {
                    location.reload(true);
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                }
            });
        });


        $('.apply-reward').click(function(){

        var reward_points = $(this).parent().find('.reward-points').val();
        var promo_code_price = $('body').find('.promo-cde-price').val();
        var mem_discount_price = $('body').find('.mem-discunt-price').val();
        var _token = $("input[name='_token']").val();
        var shipping_amount = $('body').find('.shipping-amount').val();
            $.ajax
            ({ 
                url: "{{route('shop.checkout.apply.reward')}}",
                data: { _token:_token,rewardPoints:reward_points,promoCodePrice:promo_code_price,memDiscountPrice:mem_discount_price,shippingAmount:shipping_amount},
                type: 'POST',
                dataType: "json",
                success: function(result)
                {
                    if(result.success)
                    {
                        location.reload(true);
                    }
                   else
                        alert('Please enter valid point.');
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('You exceeds your point limit!');
                }
            });
        });

        $('body').on('click','.remove', function(){

            var _token = $("input[name='_token']").val();

            if($(this).parent().hasClass('remove-mem'))
                var data = 'membership_details';

            if($(this).parent().hasClass('remove-promo'))
                var data = 'promocode_details';

            if($(this).parent().hasClass('remove-reward'))
                var data = 'reward_details';

            if($(this).parent().hasClass('remove-promotion'))
                var data = 'promotion_details';

            if($(this).parent().hasClass('remove-credit'))
                var data = 'credit_discount_details';


            $.ajax
            ({ 
                url: "{{route('shop.checkout.remove.discount')}}",
                data: { _token:_token,data:data},
                type: 'POST',
                dataType: "json",
                success: function(result)
                {
                    if(result.success)
                       location.reload(true);
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('You exceeds your point limit!');
                    alert(xhr.responseText);
                }
            });
        });
    });
 
 </script>

@endpush 

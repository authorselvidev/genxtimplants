@extends('shop::layouts.master')

@section('page_title')
  Promotion Products
@stop

@section('content-wrapper')

@inject ('productRepository', 'Webkul\Product\Repositories\ProductRepository')
@inject ('categoryRepository', 'Webkul\Category\Repositories\CategoryRepository')
<div class="promotion section">
    <form method="post" class="free-product-form" name="free-product-form">
        @csrf
        <button type="button" class="submit-free-products btn btn-primary pull-right" name="submit_free_product">Save</button>
        <span class="loading-gif pull-right" style="display:none;"><img class="" src="{{bagisto_asset('images/loading.gif')}}"></span>
            <?php
            $all_products=array();

            //echo '<div class="category-title col-md-12">';

            foreach($all_categories as $key => $free_category) {
                 $sub_category =  DB::table('categories')->where('id',$free_category)->where('parent_id', '!=', null)->first();
                  $category_name = $categoryRepository->FindCategoryNameById($free_category);
                  ?>
                  <div class="panel-group col-md-12">
  <div class="panel panel-default col-md-12 prom-pro">
    <div class="panel-heading">
        <h4 class="panel-title">
                   <a href="#category{{$free_category}}" id="{{$free_category}}" class="categ-prod" data-toggle="collapse">{{ $category_name->name }}</a>
               </h4>
           </div>
                    <div id="category{{$free_category}}" class="panel-collapse collapse">
                 
                                    <div class="secton-title col-md-12">
                                        <h3 class="category-title">{{ $category_name->name }}</h3>
                                    </div>
                                    <span class="loading-notify" style="display:none;"><img src="{{ bagisto_asset("/images/loading.gif") }}"></span>
                                  
                 <?php

$user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
if(empty($user))
$user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";      
         //dd($pagination);      ?>
           <div class="section-content">
  {{--  --}}
</div></div></div></div>
<?php
                }
               
?>
                
            <?php   //echo '</div>';
            //dd($all_products);

            //$free_products = collect($all_products);
           // $get_products = $free_products->unique();
           // dd($free_products,$get_products);
            ?>
       

        <button type="button" class="submit-free-products btn btn-primary pull-right" name="submit_free_product">Save</button>
        <span class="pull-right loading-gif" style="display:none;"><img class="" src="{{bagisto_asset('images/loading.gif')}}"></span>
</form>
</div>
<?php $get_promotion = DB::table('promotions')->where('discount_id',$discount_id)->first();
    $free_products_count = $get_promotion->free_products_count; ?>
<style type="text/css"></style>
@endsection



 @push('scripts')

 <script type="text/javascript">
$(document).ready(function(){

var qty_count =1;
 var free_prod_qty = '{{ json_decode($free_products_count) }}';

// Colocar a 0 ao iníci
// Aumenta ou diminui o valor sendo 0 o mais baixo possível
$("body").on("click",".choose-qty",function(){
   
    var $input = $(this).parent().find('.product-quantity');
    var prodquantity = $('.product-quantity').filter(function() {
                if(this.value != '' && this.value != 0)
                return this.value != ''
            });
    var qty_added=0;
    prodquantity.each(function() {
    //var $this = $(this);
    //if ($this.is(':checked')) {
     if(!isNaN(this.value) && this.value.length!=0) 
   {
    qty_added += parseFloat(this.value);            
  }  

    });
   //alert(qty_added);
    if($input.val() == "") $input.val(0);
    if ($(this).hasClass('qty-minus') && $input.val()>=1){
        $input.val(parseInt($input.val())-1);
        qty_added=qty_added-1;
    }
    //alert(qty_count);
    if(qty_added < free_prod_qty){
        if ($(this).hasClass('qty-plus')){
          $input.val(parseInt($input.val())+1);
          qty_added=qty_added+1;
        }
    }
    else{
        alert('You exceeded the free products limit!')
        return false;
    }
      
});
var qty_added = 0;
$("body").on('focusout','.product-quantity',function(){
    qty_added=0;
    var prodquantity = $('.product-quantity').filter(function() {
                if(this.value != '' && this.value != 0)
                return this.value != ''
            });
    prodquantity.each(function() {
    //var $this = $(this);
    //if ($this.is(':checked')) {
     if(!isNaN(this.value) && this.value.length!=0) 
  {
    qty_added += parseFloat(this.value);            
  }  

    });
    if(qty_added > free_prod_qty){
        alert('You exceeded the free products limit!')
        $(this).val("");
        qty_added=0;
        return false;
    }
});

$(".categ-prod").click(function(){
    var category_id = $(this).attr('id');
    var discount_id = "{{$discount_id}}";
    var route_name = '{{ route("shop.checkout.get.promotionProducts", ":category_id") }}';
    route_name=route_name.replace(':category_id',category_id);
    var this_cls = $(this);
    $(this).find('.loading-notify').css('display','block');
    $.ajax({
        url: route_name,
        method: 'POST',
        data: {
            '_token' : '{{ csrf_token() }}',
            'discount_id':discount_id
        },
        success: function(response) {
             this_cls.find('.loading-notify').css('display','none');
            $('#category'+category_id).find('.section-content').html(response);
        }
    });


});
  /*  $('body').on('click','.btn-choose-qty',function(){
        chooseQuantity($(this))
    });
    $('body').on('click','.cart-increment > button',function(){
        chooseQuantity($(this))
    });

    $("body").on('focusout','.cart_count',function(){
        chooseQuantity($(this))
    });
    var qty_count =0;

    function chooseQuantity(this_var){
            var free_prod_qty = '{{ json_decode($free_products_count) }}';
            var quantity=this_var.parent().find('.product-quantity').val();
            if(quantity == 0 || quantity == "") quantity = 1;
            else if (this_cls.hasClass('addtocart'))    quantity++; 
            if(this_cls.hasClass('remove_cart'))    quantity--;
            qty_count = qty_count + quantity;
            alert(free_prod_qty);
            alert(qty_count);
            alert(quantity);
            if(qty_count <= free_prod_qty ) {
            //route_name = route_name.replace(':id', product_id);
            $.ajax({
                  url: route_name,
                  method: 'POST',
                  data: {
                    product: product_id,
                    quantity: quantity,
                    freeprice: price,
                    _token: "{{ csrf_token() }}",
                },
                  success: function(data){
                    console.log(data);
                    this_cls.closest('.product-information').find('.qty_exceeded').html('');
                    if(data == false) this_cls.closest('.product-information').find('.qty_exceeded').html('Out of stock');

                    this_cls.parent().find('.cart_count').val(data.quantity);
                    if(data.quantity == 1) 
                        this_cls.closest('form').find('.remove_cart').attr("disabled",true);
                    else
                       this_cls.closest('form').find('.remove_cart').removeAttr("disabled"); 

                    this_cls.parent().find('.cart-increment').show();
                    $.ajax({
                        url: '{{ route("shop.checkout.minicart") }}',
                        method: 'GET',
                        success: function(response) {
                            var result = $(response).find('.cart-dropdown-container').html();
                               $('.cart-dropdown-container').html(result);
                        }
                    });
                    this_cls.parent().find('.btn-choose-qty').hide();
                  },
                error: function(data){
                    var errors = data.responseJSON;
                    console.log(errors);
                }
            });
        }
         else{
             qty_count = qty_count - quantity;
                alert('You exceeded the free products limit!')
                return false;
            }


}

*/





   $('body').on('click','.submit-free-products',function(e){
            e.preventDefault();
           
            var prodquantity = $('.product-quantity').filter(function() {
                if(this.value != '' && this.value != 0)
                return this.value != ''
            });


            var loop_length = prodquantity.length;
           
            var free_prod_qty = '{{ json_decode($free_products_count) }}';
            var discount_id = '{{ json_decode($discount_id) }}';
            var loop_count = 1;

            var qty_count = 0;
            prodquantity.each(function() {
                if(!isNaN(this.value) && this.value.length!=0) 
                {
                    qty_count += parseFloat(this.value);            
                }  

            });
           
            //alert(qty_count);
            if(qty_count == 0){
                alert('Please enter a quantity of the product!')
                return false;
            }
            else if(qty_count <= free_prod_qty)
            {
                prodquantity.each(function(index, el) {
                    $('body').find('.loading-gif').show();
                    var product_id = $(this).attr('product_id');
                    var product_quantity = $(this).val();
                    var price = '0.0000';
                    var route_name = "{{ route('cart.add', ':id') }}";
                    route_name = route_name.replace(':id', product_id);
                    $.ajax({
                        url: route_name,
                        type: 'POST',
                        data: {
                            product: product_id,
                            quantity: product_quantity,
                            freeprice: price,
                            _token: "{{ csrf_token() }}",
                            qty_selected: qty_count,
                        },
                        cache: false,
                        async: false,
                        success: function(data){
                            console.log(data);
                           if(loop_count === loop_length) {
                                $.ajax({
                                    url: "{{route('shop.checkout.apply.promotion')}}",
                                    type: 'POST',
                                    data: {
                                        discountId: discount_id,
                                        free_product_qty: qty_count,
                                        _token: "{{ csrf_token() }}",
                                    },
                                    success: function(data){
                                        console.log(data);
                                        $('body').find('.loading-gif').hide();
                                       window.location.href = data.redirect;
                                    }
                                });
                            }
                        },
                        error: function(data){
                            var errors = data.responseJSON;
                            console.log(errors);
                        },
                    });
                    loop_count = loop_count+1; 
        
                });
            }
            else{
                alert('You exceeded the free products limit!')
                return false;
            }

             
        });
});
</script>
    
@endpush
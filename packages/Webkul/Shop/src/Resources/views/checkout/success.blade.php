@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.checkout.success.title') }}
@stop

@section('content-wrapper')

<?php 
 $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
if(empty($user))
$user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

?>
<style>
.order-details{
        margin: 10px auto 2px 0px;
    }
    .order-details-cs{
        margin: 5px auto 2px 0px;
    }
    .account-details{
        margin: 5px auto 2px 0px;
    }
    .billing-info{
        border: 1px solid #e6e2e2;
        margin-left: 5px;
        width: 48.8%;
    }
    .shipping-info{
        border: 1px solid #e6e2e2;
        margin-left: 10px;
        width: 48.6%;
    }
    .payment-info{
        border: 1px solid #e6e2e2;
        margin-left: 5px;
        width: 48.8%;
    }
    .shipping-details{
        margin: 2px auto 5px 0px;
    }
    .payment-details{
        margin: 1px auto 5px 0px;
    }
    .payment-shipping-info{
        margin: 2px auto 5px 0px;
        display: flex;
    }
    .shipping-info{
        border: 1px solid #e6e2e2;
        margin-left: 10px;
        width: 48.8%;
    }
    .payment-details-cs{
        margin: 2px auto 1px 0px;
    }
    .sale-summary{
        float: right;
        margin: 2% 10px 10px auto;
    }
    .sale-container .sale-section .secton-title 
    {
        font-size: 20px;
        color: #8e8e8e;
        padding: 15px 10px;
        border-bottom: 1px solid #e6e2e2;
    }

    .sale-container .sale-section .section-content 
    {
        display: block;
        padding: 20px 10px;
        border-bottom: 3px solid #e6e2e;
        border-bottom: 3px solid #e6e2e;
    }
    .billing-addr{
        display: contents;
    }
    .payment-addr{
        display : contents;
    }
    @media (max-width: 991px){
        .order_account-responsive{
            width :100%;
            display: block;
            margin : 10px;
        }
        .order-info{
            width: 100%;
            margin: 10px;
        }
        .account-info{
            width: 100%;
            margin-left: 10px;
        }
        .billing-addr{
            width :100%;
            display: block;
            margin : 10px;
        }
        .payment-addr{
            width :100%;
            display: block;
            margin : 10px;
        }
        .shipping-info{
            width: 100%;
            margin: 10px;
        }
        .billing-info{
            width: 100%;
            margin: 10px;
        }
        .payment-info{
            width: 100%;
            margin: 10px;
        }
        .payment-details-cs{
            margin : 10px 10px 10px 10px;
        }
        .payment-details{
            margin : 10px 10px 10px 10px;
        }

    }
</style>
    <div class="order-success-content col-xs-12" style="min-height: 300px;">

        <h1>{{ __('shop::app.checkout.success.title') }}</h1>

        <div class="misc-controls" style="float:right">
            <a style="display: inline-block" href="{{ route('shop.home.index') }}" class="btn btn-lg btn-primary">
                {{ __('shop::app.checkout.cart.continue-shopping') }}
            </a>
        </div>

        <h3>{{ __('shop::app.checkout.success.thanks') }}</h3>

        <p>{{ __('shop::app.checkout.success.order-id-info', ['order_id' => $order->id]) }}</p>
        <?php 
        $points_earned = DB::table('reward_points')->where('order_id',$order->id)->value('reward_points'); ?>

        <p>{{ __('shop::app.checkout.success.info') }}</p>

        @if($order->payment[0]->method == 'prepayment')
            <p>Your order is placed, we are waiting for payment approval before dispatch, once payment is credited order will be dispatched.</p>

            <p>Here are the bank details of us.</p>
            <div class="sale-container">
            <accordian :title="'Bank Details'" :active="true">
                <div slot="body">
                    <div class="sale-section col-md-12 order-info">
                        <div class="inner-sale">
                            <div class="secton-title">
                                <span>Bank Details</span>
                            </div>
                            <div class="section-content"> 
                                <div class="rows">
                                    <span class="title col-md-5">
                                        Account Name
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        M/S Regenics
                                    </span>
                                </div>
                                <div class="rows">
                                    <span class="title col-md-5">
                                        Bank
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        HDFC
                                    </span>
                                </div>
                                <div class="rows">
                                    <span class="title col-md-5">
                                        Branch
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        East Street, Pune
                                    </span>
                                </div>
                                <div class="rows">
                                    <span class="title col-md-5">
                                        Account Number
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        50200001213129
                                    </span>
                                </div>
                                <div class="rows">
                                    <span class="title col-md-5">
                                        IFSC Code
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        HDFC0000148
                                    </span>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </accordian>
            </div>

        @endif


                    <div class="sale-container">

                        <accordian :title="'{{ __('admin::app.sales.orders.order-and-account') }}'" :active="true">
                           
                            <div slot="body">
                                <div class="sale-section col-md-6 order-info">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.order-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.order-date') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->created_at }}
                                            </span>
                                        </div>

                                        <div class="rows order-details">
                                            <span class="title col-md-5 order-details-cs">
                                                {{ __('admin::app.sales.orders.order-status') }}
                                            </span>
                                            <span class="text-center col-md-1 order-details-cs">:</span>
                                            <span class="value col-md-6 order-details-cs">
                                                {{ $order->status_label }}
                                                
                                                @if($order->status != 'completed' && $user->role_id == 1)
                                                    <a class="pull-right"  data-toggle="modal" data-target="#changeOrderStatus" href="">Change</a>
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div class="sale-section col-md-6 account-info">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.account-info') }}</span>
                                    </div>
                                    <?php $customer_name = DB::table('users as u')->leftjoin('orders as o', 'o.customer_id', 'u.id')
                                ->where('o.customer_id',$order->customer_id)->first(); 
                                 ?>
                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.customer-name') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $customer_name->my_title }} {{ $customer_name->first_name }} {{ $customer_name->last_name }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5 account-details">
                                                {{ __('admin::app.sales.orders.email') }}
                                            </span>
                                            <span class="text-center col-md-1 account-details">:</span>
                                            <span class="value col-md-6 account-details">
                                                {{ $order->customer_email }}
                                            </span>
                                        </div>

                                        @if (! is_null($order->customer))
                                            <div class="rows">
                                                <span class="title col-md-5 account-details">
                                                    {{ __('admin::app.customers.customers.customer_group') }}
                                                </span>
                                                <span class="text-center col-md-1 account-details">:</span>
                                                <span class="value col-md-6 account-details">
                                                    {{ $order->customer->customerGroup['name'] }}
                                                </span>
                                            </div>
                                        @endif
                                        <div class="rows">
                                            <span class="title col-md-5 account-details">
                                                Reward Points Earned
                                            </span>
                                            <span class="text-center col-md-1 account-details">:</span>
                                            <span class="value col-md-6 account-details">
                                                {{ $points_earned or 0 }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                </div>

                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.address') }}'" :active="false">
                            <div class="billing-addr" slot="body">
                                <div class="sale-section col-md-6 billing-info">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.billing-address') }}</span>
                                    </div>
                                    <div class="section-content">
                            <?php $get_order = DB::table('orders')->where('id',$order->id)->first();
                            //dd($get_order);
                            $billing_address = DB::table('user_addresses')->where('id',$get_order->billing_address)->latest()->first();
                            ?>
                                        @include ('admin::sales.address', ['address' => $billing_address])
                                    </div>
                                </div>
                            </div>

                                @if ($get_order->shipping_address)
                                <?php $shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->latest()->first(); ?>
                                    <div class="sale-section col-md-6 shipping-info">
                                        <div class="inner-sale">
                                        <div class="secton-title">
                                            <span>{{ __('admin::app.sales.orders.shipping-address') }}</span>
                                        </div>

                                        <div class="section-content">

                                            @include ('admin::sales.address', ['address' => $shipping_address])

                                        </div>
                                    </div>
                                </div>
                                @endif

                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.payment-and-shipping') }}'" :active="false">
                            <div slot="body">

                                <div class="sale-section col-md-6 payment-info">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.payment-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows payment_shipping-info">
                                            <span class="title col-md-5 payment-details-cs">
                                                {{ __('admin::app.sales.orders.payment-method') }}
                                            </span>
                                            <span class="text-center col-md-1 payment-details-cs">:</span>

                                            <span class="value col-md-6 payment-details-cs">
                                                @if($order->payment[0]->method == 'paylaterwithcredit')
                                                    {{ "Pay with ".(($order->customer->role_id=='2')?'credit':'advance') }}
                                                @else
                                                    {{ core()->getConfigData('sales.paymentmethods.' . $order->payment[0]->method . '.title') }}
                                                @endif
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5 payment-details">
                                                {{ __('admin::app.sales.orders.currency') }}
                                            </span>
                                            <span class="text-center col-md-1 payment-details">:</span>
                                            <span class="value col-md-6 payment-details">
                                                {{ $order->order_currency_code }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <div class="sale-section col-md-6 shipping-info acc-in">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.shipping-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        @if($order->shipping_title)
                                            <div class="col-md-12 payment_shipping-info">
                                                <span class="title col-md-5 shipping-details">
                                                    {{ __('admin::app.sales.orders.shipping-method') }}
                                                </span>
                                                <span class="text-center col-md-1 shipping-details">:</span>
                                                <span class="value col-md-6 shipping-details">
                                                    {{ $order->shipping_title }}
                                                </span>
                                            </div>
                                        @endif

                                        <div class="rows col-md-12">
                                            <span class="title col-md-5 shipping-details">
                                                {{ __('admin::app.sales.orders.shipping-price') }}
                                            </span>
                                            <span class="text-center col-md-1 shipping-details">:</span>
                                            <span class="value col-md-6 shipping-details">
                                                {{ core()->formatBasePrice($order->base_shipping_amount) }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.products-ordered') }}'" :active="false">
                            <div slot="body">
                                <?php $discounted_subtotals = array_column($order->items, 'discounted_subtotal');
                                                $discounted_subtotals = array_filter($discounted_subtotals); ?>
                                <div class="table-orded">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                                <th>{{ __('admin::app.sales.orders.price') }}</th>
                                                {{-- <th>{{ __('admin::app.sales.orders.tax-amount') }}</th> --}}
                                                <th>Quantity</th>
                                                {{-- <th>{{ __('admin::app.sales.orders.subtotal') }}</th> --}}
                                                {{-- <th>{{ __('admin::app.sales.orders.tax-percent') }}</th> --}}
                                                <th>Sub Total</th>
                                                @if(count($discounted_subtotals) > 0)
                                                    <th>Discounted Sub Total</th>
                                                @endif
                                                <th>Grand Total <br> (incl.taxes)</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            @foreach ($order->items as $item)
                                                <tr>
                                                    <td>
                                                        {{ $item['name'] }}
                                                    </td>
                                                    <td>
                                                        <?php $price_with_tax = $item['base_price'] + $item['base_tax_amount']; ?>
                                                        {{ core()->formatBasePrice($price_with_tax) }}
                                                    </td>
                                                    {{-- <td>{{ core()->formatBasePrice($item['base_tax_amount']) }}</td> --}}
                                                    <td>{{ $item['additional']['quantity'] }}</td>
                                                    {{-- <td>{{ core()->formatBasePrice($item['base_total']) }}</td> --}}
                                                    {{-- <td>{{ $item['tax_percent'] }}%</td> --}}
                                                    <td>{{ core()->formatBasePrice($item['base_total']) }} </td>
                                                    <?php $discounted_subtotal=0; ?>
                                                    @if(count($discounted_subtotals) > 0)
                                                        @if(isset($item['discounted_subtotal']))
                                                        <?php $discounted_subtotal = $item['discounted_subtotal']; ?>
                                                            <td>{{ core()->formatBasePrice($item['discounted_subtotal']) }}</td>
                                                        @else
                                                            <td>NA</td>
                                                        @endif
                                                    @endif
                                                    <?php $total_price_item = $item['total'];
                                                    if($discounted_subtotal != 0)
                                                        $total_price_item = $discounted_subtotal; 
                                                    
                                        $get_tax = DB::table('tax_rates')->where('state',$user->state_id)->first(); 
                                        $item_tax = $total_price_item * ($get_tax->tax_rate / 100); 
                                        $price_with_tax = $total_price_item + $item_tax; ?> 
                                                    <td>{{ core()->formatBasePrice($price_with_tax) }}</td>
                                                </tr>
                                            @endforeach
                                    </table>
                                </div>

                                <table class="sale-summary" style="float:right;margin-top:2%;margin: 1% 10px 10px auto;">
                                    <tr>
                                        <td>{{ __('admin::app.sales.orders.subtotal') }}</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($order->base_sub_total) }}</td>
                                    </tr>

                                    <?php $discounted_subtotal=0; ?>
                                    @if($order->mem_discount_amount == 0.0000 && $order->promo_code_amount == 0.0000 && $order->reward_coin == 0.0000 && $order->credit_discount_amount == 0.0000 && $order->base_discount_amount != 0.0000)
                                    <tr>
                                        <td class="pull-left text-left">Special Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->base_discount_amount) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->base_discount_amount; ?>

                                    @endif

                                    @if($order->mem_discount_amount != 0.0000)
                                    <tr>
                                        <td class="pull-left text-left">Membership Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->mem_discount_amount) }}</td>
                                    </tr>
                                    @if (! is_null($order->customer))
                                        <tr>
                                            <td>Group:</td>
                                            <td>:</td>
                                            <td> {{ $order->customer->customerGroup['name'] }}</td>
                                        </tr>
                                    @endif
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->mem_discount_amount; ?>

                                    @endif

                                    @if($order->promo_code_amount != 0.0000)
                                    <tr>
                                        <td>Promo Code Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->promo_code_amount) }}</td>
                                    </tr>
                                    @if(isset($order->coupon_code) || $order->coupon_code != "")
                                    <tr>
                                        <td>Discount Code:</td>
                                        <td>:</td>
                                        <td> {{$order->coupon_code }}</td>
                                    </tr> 
                                    @endif
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->promo_code_amount; ?>
                                    @endif

                                    @if($order->credit_discount_amount != 0.0000)
                                        <tr>
                                            <td>Advance Payment Discount</td>
                                            <td>:</td>
                                            <td>- {{ core()->currency($order->credit_discount_amount) }}</td>
                                        </tr>
                                        @if(isset($order->coupon_code) || $order->coupon_code != "")
                                            <tr>
                                                <td>Discount Code:</td>
                                                <td>:</td>
                                                <td> {{$order->coupon_code }}</td>
                                            </tr> 
                                        @endif
                                        <?php $discounted_subtotal = $order->base_sub_total - $order->credit_discount_amount; ?>
                                    @endif

                                    @if($order->reward_coin != 0.0000)
                                    <tr>
                                        <td>Reward Points:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->reward_coin) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->reward_coin; ?>
                                    @endif
                                    @if($discounted_subtotal != 0)
                                    <tr>
                                        <td>Discounted Subtotal</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($discounted_subtotal) }}</td>
                                    </tr>
                                    @endif


                                    
                                    <tr>
                                        <td>Delivery Charges</td>
                                        <td>:</td>
                                        <td>+ {{ core()->formatBasePrice($order->base_shipping_amount) }}</td>
                                    </tr>

                                    <tr class="border">
                                        <td>{{ __('admin::app.sales.orders.tax') }}</td>
                                        <td>:</td>
                                        <td>+ {{ core()->formatBasePrice($order->tax_amount) }}</td>
                                    </tr>

                                    <tr class="bold">
                                        <td>{{ __('admin::app.sales.orders.grand-total') }}</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                    </tr>
                                </table>

                            </div>
                        </accordian>

                    </div>
         

<?php $order_array = session('order'); //dd(json_encode($order_array));?>


    </div>

@endsection
@push('scripts')
<script type="text/javascript">
    function loadajax(){
        var order = "{{ $order_array }}";
  $.ajax({ 
    url:"{{route('trigger.mail')}}",
    type:"post",
    data:{"_token":"{{csrf_token()}}", "order":order},
    success:function(res){
        console.log(res);
    },
    error:function(res){
        console.log(res);
    }
  });
}
$(function(){
    setTimeout(loadajax,2000);
});
</script>
@endpush

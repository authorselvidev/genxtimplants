@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.checkout.success.title') }}
@stop

@section('content-wrapper')

 <div class="order-success-content col-xs-12">
 	<h2>Please wait... We are processing your payment and this may take few seconds.</h2>
 </div>
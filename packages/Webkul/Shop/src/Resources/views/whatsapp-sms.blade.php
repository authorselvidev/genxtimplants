<?php
$messages = array(
    // Put parameters here such as force or test
    'send_channel' => 'whatsapp',
    'messages' => array(
        array(
            'number' => $sms_numbers,
            'template' => array(
                'id' => '577005',
                'merge_fields' => array(
                    'FirstName' => $first_name
                )
            )
        )
    )
);
 
// Prepare data for POST request
$data = array(
    'apikey' => 'Your API key',
    'data' => json_encode($messages)
);
 
// Send the POST request with cURL
$ch = curl_init('https://api.textlocal.in/bulk_json/');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
 
echo $response;
?>
@extends('shop::layouts.master')
@section('page_title')
    Products - GenXT
@endsection
@section('content-wrapper')

<div class="auth-content">
	<div class="products-page section col-md-12 col-xs-12">
<h1 class="text-center">Shop</h1>    
<section class="category-sec col-md-12">
   	<div class="main-container-wrapper">
   		
    <?php $get_categories = DB::table('categories as c')->where('c.status' , 1)->join('category_translations as ct', 'c.id', 'ct.category_id')->get(); ?>
	<div class="all-categories">
		@foreach($get_categories as $key => $category)
		 

		 <div class="each-category col-md-3 col-sm-4 col-xs-6 text-left">
			<div class="cat-left col-md-3 col-sm-3 col-xs-3">
				<a href="{{ 'categories/'.$category->slug }}#category-7">
					<img src="{{ url('storage/app/public/'.$category->image) }}" alt="">
				</a>
			</div>
			<div class=" cat-right  test col-md-9 col-sm-9 col-xs-9">
				<h3><a href="{{ 'categories/'.$category->slug }}#category-7">{{ $category->name }}</a></h3>
				<p>{{ $category->short_description }}</p>
				<a class="read-more" href="{{ 'categories/'.$category->slug }}#category-7">Read More</a>
			</div>
        </div>

		@endforeach
	</div>
	</div>

		
</section>


	</div>
</div>
@endsection

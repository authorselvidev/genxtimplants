@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.home.page-title') }}
@endsection

@section('content-wrapper')
</div>
    @include('shop::home.slider')

    {!! view_render_event('bagisto.shop.home.content.before') !!}

   <?php 
   $get_home = DB::table('pages')->where('is_deleted',0)->where('is_home',1)->first();
   ?>
   <section class="content-part col-md-12">
   	<div class="main-container-wrapper">
   {!! $get_home->page_content !!}
   </div>
</section>


   <section class="category-sec col-md-12">
   	<div class="main-container-wrapper">

    	<?php 
$get_categories = DB::table('categories as c')->where('c.status' , 1)->join('category_translations as ct', 'c.id', 'ct.category_id')->get();
 ?>
	<div class="all-categories">
		@foreach($get_categories as $key => $category)
		 

		 <div class="each-category col-md-3 col-sm-4 col-xs-6 text-left">
			<div class="cat-left col-md-3 col-sm-3 col-xs-3">
				<a href="{{ 'categories/'.$category->slug }}#category-7">
                    <img src="{{ url('storage/app/public/'.$category->image) }}" alt="">
                </a>
			</div>
			<div class=" cat-right  test col-md-9 col-sm-9 col-xs-9">
				<h3><a href="{{ 'categories/'.$category->slug }}#category-7">{{ $category->name }}</a></h3>
				<p>{{ $category->short_description }}</p>
				<a class="read-more" href="{{ 'categories/'.$category->slug }}#category-7">Read More</a>
			</div>
        </div>

		@endforeach
	</div>
	</div>

</section>

    {{ view_render_event('bagisto.shop.home.content.after') }}

@endsection

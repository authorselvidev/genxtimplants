<section class="slider-block">
    <image-slider :slides='@json($sliderData)' public_path="{{ url()->to('/') }}"></image-slider>
</section>
@push('scripts')
    <script>
        $(document).ready(function () {
   $('.slider-content .slider-item').each(function() {
       
        var old_img = $(this).attr('src');
         console.log(old_img);
        var new_img = old_img.replace('storage', 'storage/app/public');
        console.log(new_img);
        $(this).attr('src',new_img);
    });
});
</script>
@endpush
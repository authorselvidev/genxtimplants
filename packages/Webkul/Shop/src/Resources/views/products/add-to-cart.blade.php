{!! view_render_event('bagisto.shop.products.add_to_cart.before', ['product' => $product]) !!}

<!-- <button type="submit" class="btn btn-lg btn-primary addtocart" {{ $product->type != 'configurable' && !$product->haveSufficientQuantity(1) ? 'disabled' : '' }}>
    {{ __('shop::app.products.add-to-cart') }}
</button> -->
<?php 
	$cart = cart()->getCart();
    $cart_product_id=$quantity=[];
    if ($cart)
    {
        foreach($cart->items as $key => $value)
        {
            $quantity[$value->additional['product']] = $value->quantity;
            $cart_product_id[] = $value->additional['product'];
        }
    } 
$show=$hide=$product_qty=''; 

if(in_array($product->id,$cart_product_id)) { 
    $show = 'show'; $hide = 'hide'; $product_qty=$quantity[$product->id];
} ?>               
           
    <div class="cart-increment {{ $show }}">
       <button type="button" class="btn pull-left remove_cart btn-sm btn-primary addtocart_{{$product->id}}" {{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}><i class="fa fa-minus"></i></button>
        <span class="cart_count">{{$product_qty}}</span>
        <button type="button" class="btn pull-right btn-sm btn-primary addtocart_{{$product->id}}" {{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}><i class="fa fa-plus"></i></button>
    </div>
    <button type="button" class="btn btn-lg btn-addtocart btn-primary addtocart_{{$product->id}} {{ $hide }}" {{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}>{{ __('shop::app.products.add-to-cart') }}</button>

{!! view_render_event('bagisto.shop.products.add_to_cart.after', ['product' => $product]) !!}
@extends('shop::layouts.master')

@section('page_title')
    {{ $category->name }}
@stop

@section('seo')
    <meta name="description" content="{{ $category->meta_description }}"/>
    <meta name="keywords" content="{{ $category->meta_keywords }}"/>
@stop

@section('content-wrapper')

    @inject ('productRepository', 'Webkul\Product\Repositories\ProductRepository')
    @inject ('categoryRepository', 'Webkul\Category\Repositories\CategoryRepository')

    <div class="main">
        {!! view_render_event('bagisto.shop.products.index.before', ['category' => $category]) !!}

        <div  class="category-container">
            <?php $cart = cart()->getCart(); ?>  
      
     
                {{-- @include ('shop::products.list.layered-navigation') --}}
                <div id="category{{$category->id}}" class="category-section col-md-12">
                    <div class="category-desc col-md-12">{!! $category->description !!}</div>
                     <a href="#" class="cat-btn btn btn-lg btn-primary show-more">Learn More...</a>
                </div>
               
                

            <div  class="category-block col-md-12">
                {{-- 
                <div class="hero-image mb-35">
                        @if (!is_null($category->image))
                            <img class="logo" src="{{ url('storage/app/public/'.$category->image) }}" />
                        @endif
                </div>
                --}}

                <?php
                    $active_cls="";
                    $get_categories  = DB::table('categories')->where('parent_id', $category->id)->pluck('id')->toArray();
                    $get_products=$categories=array();
                    //$th_count=1;

                    //Genweld           - 13
                    //Basal MU          - 11
                    //Root form         - 1 
                    //Compression MU    - 9
                    //Instant provisinals - 15
                    
                    //Compression       - 3
                    //Basal             - 5
                    //Basal SS          - 7
                    if($category->id == 3 || $category->id == 5 || $category->id == 7){
                        //Accessories id = 17, Instruments id = 24
                        array_push($get_categories, 17, 24);
                    }

                    //dd($category->id);
                ?>
                <ul class="sub-categry nav nav-pills">
                    <?php
                        foreach ($get_categories as $key => $categry_id)
                        {
                            if($key == 0) $active_cls = 'active';
                            $category_name = $categoryRepository->FindCategoryNameById($categry_id);
                            //dd($category_name); ?>

                            <li class="{{ $active_cls }} ">
                                <a id="cat{{ $categry_id }}" href="#category{{ $categry_id }}" data-toggle="tab" class="sub-category">{{$category_name->name}}</a>
                            </li>

                        <?php  
                            $get_products[$categry_id] = $productRepository->findAllByCategory($categry_id);
                            
                            $active_cls='';
                        }?>
                </ul>

                <div class="sub-category-content tab-content">
                 <?php  
                //dd($get_products);
                 $cat_count =0;  $category_ids=$size_ids=array();
                 foreach($get_products as $cat_id => $products)
                {
                ?>  
                    <?php
                    $sizeids=array();
                    $category_ids[] = $cat_id;
                    if ($products->count())
                    {
					$active_cls='';
                    if($cat_count == 0) $active_cls = 'active';
                     
                     $cat_count=$cat_count+1;
                     echo '<div class="each-prod tab-pane '.$active_cls.'" id="category'.$cat_id.'">';
                     $cat_slug='';
                     $cat_slug = DB::table('category_translations')->where('category_id',$cat_id)->first();
                    //dd(strpos('implants',$cat_slug->slug));
                    if(strpos($cat_slug->slug,'implants') == false) {
                    //if($cat_id == 17 || $cat_id == 24){
                        echo '<div class="size-content clearfix"></div></div>';
                    }
                    else{
                    $size_row=$sort_order=$size=$product_arr=array();

                    $product_arr = $products->toArray(); 
                    //dd($product_arr);         
                                     $get_size = array_column($product_arr, 'size');
                                    //dd($get_size);
                                    foreach($get_size as $key => $size_id){
                                       $sort_order[] = DB::table('attribute_options')->where('id',$size_id)->first();
                                    }
                                    //dd($sort_order);
                                    $sort_order = collect($sort_order); 
                                        $size_row = $sort_order->sortBy('sort_order')->unique('sort_order');
                                       //dd($size_row);
                                    $size_count=0; ?>
                                    <ul class="size-tab nav nav-pills">
                                        @foreach($size_row as $key => $size)
                                        
                                        <?php 
                                        if(isset($size->id)){
											$sizeids[] = $size->id; 
                                            $active_cls='';
                                        if($size_count == 0) $active_cls = 'active';
                                            $size_id = 'others';
                                            $admin_name = 'Others';
                                            if(isset($size->id)) $size_id = $size->id;
                                            if(isset($size->admin_name)) $admin_name = $size->admin_name;
                                             ?>
                                            <li class="{{ $active_cls }}"><a id="{{ $size_id }}" href="#diameter{{ $size_id }}" class="sizeid" data-toggle="tab">{{ $admin_name }}</a></li>
                                            <?php $active_cls=$size_id=$admin_name=""; $size_count++; }
                                            else $sizeids[] = '';?>
                                        @endforeach
                                    </ul>
                                    <div class="size-content clearfix">

                                    </div>
                        
                <?php echo '<div class="first-sizeid">'.$sizeids[0].'</div></div>';
                }
                

             }
               
       } ?>
          <div class="loader"></div>

      </div>
         
            </div>
      
      
        </div>

        {!! view_render_event('bagisto.shop.products.index.after', ['category' => $category]) !!}
    </div>
@stop

@push('scripts')

<script>
    $(document).ready(function () {
        $('.loader').hide();
        $('.first-sizeid').hide();
        var cat_id = $('.category-block .each-prod.active').attr('id').match(/\d+/);
        // var sizeid = $('.category-block .each-prod.active').find('.first-sizeid').html();
        //alert($('.each-prod.active').attr('id'));
        var sizeid=$('.sizeid').first().attr('id');
        getProductsBySize(cat_id,sizeid);

        $('body').on('click','.sub-category',function(){
            $('body').find('.size-content').html('');
            var cat_id = $(this).attr('id').match(/\d+/);
            var sizeid = $('#category'+cat_id).find('.first-sizeid').html();
            if(sizeid != undefined)
                var sizeid=$('.sizeid').first().attr('id');
            getProductsBySize(cat_id,sizeid);
        });
        
        $('body').on('click','.sizeid',function(){
            var size_id = $(this).attr('id');
            var categry_id = $(this).closest('.each-prod').attr('id').match(/\d+/);
            getProductsBySize(categry_id,size_id);
        });
        function getProductsBySize(categry_id,size_id)
        {
            $('.loader').show();
             $('body').find('.size-content').html('');
                $.ajax({
                  url: "{{ route('shop.ajax.getproducts') }}",
                  type: "POST",
                  data: {
                    size_id: size_id,
                    category_id: categry_id,
                    _token: "{{ csrf_token() }}"
                },
                success: function(data){
                    setTimeout(function() {
                        $('body').find('.size-content').html(data.view);
                        $('.loader').hide();
                    }, 1000);
                    
                },
                error: function(data, xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
                console.log(data);
            }
            });

        }


         /*$('body').on('click','.addtocart',function(){
            var product_id =$(this).closest('form').find('input[name="product"]').val();
            var this_cls = $(this);
            this_cls.removeAttr("disabled");
            var route_name = "{{ route('cart.add', ':id') }}";
            if(this_cls.hasClass('remove_cart')){ 
                route_name = "{{ route('cart.remove_qty', ':id') }}";
            }
            route_name = route_name.replace(':id', product_id);
            $.ajax({
                  url: route_name,
                  method: 'POST',
                  data: {
                    product: $(this).parent().parent().find('input[name="product"]').val(),
                    quantity: $(this).parent().parent().find('input[name="quantity"]').val(),
                    discount: $(this).parent().parent().find('input[name="discount"]').val(),
                    is_configurable: $(this).parent().parent().find('input[name="is_configurable"]').val(),
                    _token: $(this).parent().parent().find('input[name="_token"]').val()
                },
                  success: function(data){
                    console.log(data);
                    this_cls.closest('.product-information').find('.qty_exceeded').html('');
                    if(data == false) this_cls.closest('.product-information').find('.qty_exceeded').html('Out of stock');

                    this_cls.parent().find('.cart_count').html(data.quantity);
                    if(data.quantity == 1) 
                        this_cls.closest('form').find('.remove_cart').attr("disabled",true);
                    else
                       this_cls.closest('form').find('.remove_cart').removeAttr("disabled"); 

                    this_cls.parent().find('.cart-increment').show();
                    $.ajax({
                        url: '{{ route("shop.checkout.minicart") }}',
                        method: 'GET',
                        success: function(response) {
                            var result = $(response).find('.cart-dropdown-container').html();
                               $('.cart-dropdown-container').html(result);
                        }
                    });
                    this_cls.parent().find('.btn-addtocart').hide();
                  },
                error: function(data){
                    var errors = data.responseJSON;
                    console.log(errors);
                }
            });
        
        });*/

        

        $('body').on('click','.cart-increment > button',function(){
            addtocartsection($(this))
        });
        $('body').on('click','.btn-addtocart',function(){
            addtocartsection($(this))
        });
        $("body").on('focusout','.cart_count',function(){
            addtocartsection($(this))
        });
        $("body").on('keypress','.cart_count',function(e){
            if(e.which == 13) {
                addtocartsection($(this));
                $(this).blur();
                e.preventDefault();
            }
        });



         $('body').on('click','.notify-me',function(){
            var this_var = $(this);
            var product_id =$(this).data('product_id');
            var user_id =$(this).data('user_id');
            $(this).html('Notified');
            $(this).prop('disabled', true);
            //$(this).next().css('display','block');
            /*var wishlist_route = "{{ route('customer.wishlist.add', ':product_id') }}";
            wishlist_route = wishlist_route.replace(':product_id', product_id);*/
            /*$.ajax({
                url: wishlist_route,
                method: 'GET',
                success: function(response) {*/
                     $.ajax({
                        url: "{{ route('shop.checkout.notifyme') }}",
                        method: 'POST',
                        data: {
                            product_id: product_id,
                            user_id: user_id,
                            _token: "{{ csrf_token() }}"
                            },
                        success: function(data){
                            //$(this).next().css('display','none');
                            //location.reload(); 
                        }
                    });
                    
               /* }
            });*/
        });


     /*      $('body').on('click','.notify-me',function(){
            var this_var = $(this);
            var product_id =$(this).data('product_id');
            var user_id =$(this).data('user_id');
            $(this).next().css('display','block');
            $.ajax({
                url: "{{ route('shop.checkout.notifyme') }}",
                method: 'POST',
                data: {
                    product_id: product_id,
                    user_id: user_id,
                    _token: "{{ csrf_token() }}"
                    },
                      success: function(data){
                        var wishlist_route = "{{ route('customer.wishlist.add', ':product_id') }}";
                        wishlist_route = wishlist_route.replace(':product_id', product_id);
                        $.ajax({
                        url: wishlist_route,
                        method: 'GET',
                        success: function(response) {
                            location.reload();
                        }
                    });
                        $(this).next().css('display','none');
                        

                    }
            });
    });*/
          
    function addtocartsection(this_var){
        var product_id =this_var.closest('form').find('input[name="product"]').val();
            var this_cls = this_var;
            this_cls.removeAttr("disabled");
            var route_name = "{{ route('cart.add', ':id') }}";

            var quantity =this_var.parent().parent().find('input[name="quantity"]').val();

            
            if(quantity == "") quantity = 1;
            else if (this_cls.hasClass('addtocart')){
                  quantity++; 
            }
            
            if(this_cls.hasClass('remove_cart')){ 
                route_name = "{{ route('cart.remove_qty', ':id') }}";
                quantity--;
                
            }

            // alert((quantity >= 0));
  /*          this_cls.parent().find('.cart_count').val(quantity);
            alert(quantity);
            return false;*/

            if(quantity >= 0){
                this_cls.parent().find('.cart_count').val(quantity);

                route_name = route_name.replace(':id', product_id);
                $.ajax({
                      url: route_name,
                      method: 'POST',
                      data: {
                        product: this_var.parent().parent().find('input[name="product"]').val(),
                        quantity: quantity,
                        discount: this_var.parent().parent().find('input[name="discount"]').val(),
                        is_configurable: $(this).parent().parent().find('input[name="is_configurable"]').val(),
                        _token: this_var.parent().parent().find('input[name="_token"]').val()
                    },
                      success: function(data){
                        console.log(data);
                        if(data == false){
                            alert('Out of stock');
                            // this_cls.closest('.product-information').find('.qty_exceeded').html('Out of stock');
                            var prev_quantity=parseInt(quantity)-1
                            this_cls.parent().find('.cart_count').val(prev_quantity);

                            return false;
                        }
                        else{
                            this_cls.closest('.product-information').find('.qty_exceeded').html('');
                            // this_cls.parent().find('.cart_count').val(data.quantity);
                        }

                        
                        if(data.quantity > 0) 
                            this_cls.closest('form').find('.remove_cart').addClass("remove-item");
                        else
                           this_cls.closest('form').find('.remove_cart').removeClass("remove-item"); 

                        this_cls.parent().find('.cart-increment').show();
                        $.ajax({
                            url: '{{ route("shop.checkout.minicart") }}',
                            method: 'GET',
                            success: function(response) {
                                var result = $(response).find('.cart-dropdown-container').html();
                                   $('.cart-dropdown-container').html(result);
                            }
                        });
                        this_cls.parent().find('.btn-addtocart').hide();
                      },
                    error: function(data){
                        var errors = data.responseJSON;
                        console.log(errors);
                    }
                });
                if (quantity==0) {
                    this_cls.closest('form').find('.cart-increment').removeClass('show').addClass('hide');
                    this_cls.closest('form').find('.btn-addtocart').removeClass('hide').addClass('show');
                }
                else{
                    this_cls.closest('form').find('.cart-increment').removeClass('hide').addClass('show');
                    this_cls.closest('form').find('.btn-addtocart').removeClass('show').addClass('hide');  
                }
            }
        }
});

       
</script>

    <script>

        $(document).ready(function() {

            $('.show-more').click(function() {
            if($('.category-desc').css('height') != '370px'){
                $('.category-desc').stop().animate({height: '370px'}, 200);
                $(this).text('Learn More...');
            }else{
                $('.category-desc').css({height:'100%'});
                var xx = $('.category-desc').height();
                $('.category-desc').css({height:'370px'});
                $('.category-desc').stop().animate({height: xx}, 400);
                // ^^ The above is beacuse you can't animate css to 100% (or any percentage).  So I change it to 100%, get the value, change it back, then animate it to the value. If you don't want animation, you can ditch all of it and just leave: $('.show-more-snippet').css({height:'100%'});^^ //
                $(this).text('Learn Less...');
            }
        });



            var sort = document.getElementById("sort");
            var filter = document.getElementById("filter");
            var sortLimit = document.getElementsByClassName('pager')[0];
            //var   = document.getElementsByClassName('responsive-layred-filter')[0];

            //layerFilter.style.display ="none";

            if (sort && filter) {
                sort.addEventListener("click", sortFilter);
                filter.addEventListener("click", sortFilter);
            }

            function sortFilter() {
                var className = document.getElementById(this.id).className;

                if (className === 'icon sort-icon') {
                    sort.classList.remove("sort-icon");
                    sort.classList.add("icon-menu-close-adj");

                    filter.classList.remove("icon-menu-close-adj");
                    filter.classList.add("filter-icon");

                    sortLimit.style.display = "flex";
                    sortLimit.style.justifyContent = "space-between";
                    //layerFilter.style.display ="none";
                } else if (className === 'icon filter-icon') {
                    filter.classList.remove("filter-icon");
                    filter.classList.add("icon-menu-close-adj");

                    sort.classList.remove("icon-menu-close-adj");
                    sort.classList.add("sort-icon");

                    //layerFilter.style.display = "block";
                    //layerFilter.style.marginTop = "10px";

                    sortLimit.style.display = "none";
                } else {
                    sort.classList.remove("icon-menu-close-adj");
                    sort.classList.add("sort-icon");

                    filter.classList.remove("icon-menu-close-adj");
                    filter.classList.add("filter-icon");

                    sortLimit.style.display = "none";
                    //layerFilter.style.display = "none";
                }
            }
        });
    </script>


@endpush

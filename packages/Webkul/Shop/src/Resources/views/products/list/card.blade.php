<?php 
$product_info = "";
$product=[];
if(isset($sdata)) {
$product=$sdata['product'];
$product_info=$sdata['product_info'];
}
$discount_price=0;

 ?>

 @if(Auth::guard('admin')->check() || Auth::guard('customer')->check())
<?php
$user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
if(empty($user))
$user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

if(isset($product->id)) {
$product_category = DB::table('product_categories')
                                    ->where('product_id',$product->id)
                                    ->pluck('category_id');
            $get_price = $product->price;
            $get_discount = DB::table('discount_users as du')
                            ->where('du.user_id',$user->id)
                            ->leftjoin('discount_category as dc','dc.discount_id','du.discount_id')
                            ->leftjoin('discounts as d','d.id','du.discount_id')
                            ->where('d.discount_type',1)
                            ->whereIn('dc.category_id',$product_category)
                            ->get();
                            
    if(count($get_discount) > 0){
        $discount_per = $get_discount[0]->discount;
        $discount_price = $get_price *($discount_per / 100);
    }
} ?>
@endif


            
@if($product)
{!! view_render_event('bagisto.shop.products.list.card.before', ['product' => $product]) !!}

<div class="product-card col-md-3 col-sm-4 col-xs-6 col-sm-12">

    @inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

    <?php $productBaseImage = $productImageHelper->getProductBaseImage($product); ?>
<?php $days_ago = \Carbon\Carbon::today()->subDays(7); ?>
    {{-- @if (isset($product->new)) --}}
    @if($product->created_at > $days_ago)
        <div class="sticker new">
            {{ __('shop::app.products.new') }}
        </div>
    @endif
    @if(!$product->haveSufficientQuantity(1))
     <div class="sticker sold">
            Sold Out
        </div>
    @endif
    <?php //var_dump($product_info); ?>

    <div class="product-image text-center col-xs-12 col-md-12 col-sm-12">
        <img src="{{ $productBaseImage['medium_image_url'] }}" />
    </div>

    <div class="product-information col-xs-12 col-md-12 col-sm-12">

        <div class="product-name">
            <span>
                Item #: {{ $product->name }}
            </span>
        </div>
        <div class="product-size">
            <span>
               <label>Length: </label> {{ $product_info }}
            </span>
        </div>


        @if(Auth::guard('admin')->check() || Auth::guard('customer')->check())
             @include ('shop::products.price', ['product' => $product])
        @endif
        

        @if (Route::currentRouteName() == "shop.products.index")
            @include ('shop::products.add-to', ['product' => $product])
        @else
           
            @if ($product->type == "configurable")
                <div class="cart-wish-wrap">
                    <a href="{{ route('cart.add.configurable', $product->url_key) }}" class="btn btn-lg btn-primary addtocart">
                        {{ __('shop::app.products.add-to-cart') }}
                    </a>

                    @include('shop::products.wishlist')
                </div>
            @else
                @if(Auth::guard('admin')->check() || Auth::guard('customer')->check())
                <?php $cart = cart()->getCart();
                $cart_product_id=$quantity=[];
                if ($cart)
                {
                foreach($cart->items as $key => $value){
                    $quantity[$value->additional['product']] = $value->quantity;
                    $cart_product_id[] = $value->additional['product'];
                    
                }
                } //dd($quantity); exit; ?>
                <div class="cart-wish-wrap">
                    <form action="" method="POST">
                        @csrf
                        <input type="hidden" name="product" value="{{ $product->id }}">
                        <input type="hidden" name="discount" value="{{ $discount_price }}">
                        <!--<input type="hidden" name="quantity" value="1">-->
                        <input type="hidden" value="false" name="is_configurable">
                        <?php $show=$hide=$product_qty=''; 
                            //dd($product->haveSufficientQuantity(1));
                            if(in_array($product->id,$cart_product_id)) { 
                                $show = 'show'; $hide = 'hide'; $product_qty=$quantity[$product->id];} ?>               
                            <div class="cart-increment {{ $show }}">
                                <button type="button" class="btn pull-left remove_cart btn-sm btn-primary" {{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}><i class="fa fa-minus"></i></button>
                                <input type="text" class="cart_count" name="quantity" value="<?php echo $product_qty; ?>">
                                <button type="button" class="btn pull-right btn-sm btn-primary addtocart" {{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}><i class="fa fa-plus"></i></button>
                               
                            </div>
                        @if($product->haveSufficientQuantity(1))
                        <button type="button" class="btn btn-lg btn-addtocart btn-primary addtocart {{ $hide }}" {{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}>{{ __('shop::app.products.add-to-cart') }}</button>
                        @else
                        <?php $notified = DB::table('notify_me')->where('product_id',$product->id)->where('customer_id',$user->id)->where('notify_sent',0)->first(); ?>
                         <button type="button" data-user_id="{{$user->id}}" data-product_id="{{$product->id}}" class="btn btn-lg btn-primary notify-me" @if(isset($notified)) {{'disabled'}} @endif>Notify Me</button>
                         <span class="loading-notify" style="display:none;"><img src="{{ bagisto_asset("/images/loading.gif") }}"></span>
                        @endif
                    </form>

                    @include('shop::products.wishlist')
                </div>
                <span class="qty_exceeded"></span>
                 @else
                    <a class="btn btn-primary btn-sm" href="{{route('customer.session.index')}}">Buy Now</a>
                @endif
            @endif
        @endif
    </div>

</div>


{!! view_render_event('bagisto.shop.products.list.card.after', ['product' => $product]) !!}

@endif

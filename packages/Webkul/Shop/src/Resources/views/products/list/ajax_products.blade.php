<div class="tab-pane product-lists col-md-12" id="{{ $size_id }}">
    <div class="product-grid-3 col-md-12 cat{{$category_id}}">
        @foreach ($products as $productFlat)
            <?php
                $sdata=['product'=> $productFlat->product,'product_info'=>$productFlat->length_label];
            ?>
             @if(($size_id == $productFlat->size) || ($size_id == null))
                @include ('shop::products.list.card', $sdata)
            @endif
        @endforeach
    </div>
</div>

{!! view_render_event('bagisto.shop.products.price.before', ['product' => $product]) !!}
<div class="product-price">
   
    @inject ('priceHelper', 'Webkul\Product\Helpers\Price')

    @if ($product->type == 'configurable')

        <span class="price-label">{{ __('shop::app.products.price-label') }}</span>

        <span class="final-price">{{ core()->currency($priceHelper->getMinimalPrice($product)) }}</span>

    @else

        @if ($priceHelper->haveSpecialPrice($product))

            <div class="sticker sale">
                {{ __('shop::app.products.sale') }}
            </div>

            <span class="regular-price">{{ core()->currency($product->price) }}</span>

            <span class="special-price">{{ core()->currency($priceHelper->getSpecialPrice($product)) }}</span>

        @else
            <?php
          $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
            if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
                           //dd($get_discount); 
            $discounted_price_with_tax="";      
            $get_tax = DB::table('tax_rates')->where('state',$user->state_id)->first();      
            if(count($get_discount) > 0)
            {
                $discount_per = $get_discount[0]->discount;
                $discount_price = $get_price *($discount_per / 100);
                $discounted_price = abs($get_price - $discount_price);
                $special_price = '<label style="color:green;">Special Price: </label>';
                $discounted_tax_amount = $discounted_price * ($get_tax->tax_rate / 100); 
                $discounted_price_with_tax = $discounted_price + $discounted_tax_amount;
            }

            $tax_amount = $get_price * ($get_tax->tax_rate / 100); 
            $price_with_tax = $get_price + $tax_amount; ?>
            @if($discounted_price_with_tax != "")
                <strike>{{ core()->currency($price_with_tax) }}</strike> <span style="color:green;font-size:18px;">{{ core()->currency($discounted_price_with_tax) }}</span>
            @else
            <span style="font-size:18px;">{{ core()->currency($price_with_tax) }}</span>
            @endif

            <br><small style="color:#777">(Incl. of all taxes)</small>
            
        @endif

    @endif

</div>


{!! view_render_event('bagisto.shop.products.price.after', ['product' => $product]) !!}
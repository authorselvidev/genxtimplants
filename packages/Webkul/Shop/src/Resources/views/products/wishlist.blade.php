    {!! view_render_event('bagisto.shop.products.wishlist.before') !!}
    <?php 
    $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
if(empty($user))
$user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

    $added='';
    $added_wishlist = DB::table('wishlist')->select('id')->where('product_id',$product->id)->where('customer_id',$user->id)->first();
    if($user->role_id == 2 || $user->role_id == 1 || $user->role_id == 4) $route_role = 'admin';
    if($user->role_id == 3) $route_role = 'customer';
    $route_name = $route_role.'.wishlist.add';
    if($added_wishlist) {
    	$added = 'added';
    	$route_name = $route_role.'.wishlist.remove';
    	} ?>
    <a class="add-to-wishlist <?php echo $added; ?>" href="{{ route($route_name, $product->id) }}" id="wishlist-changer">
        <span class="icon wishlist-icon"></span>
    </a>

    {!! view_render_event('bagisto.shop.products.wishlist.after') !!}

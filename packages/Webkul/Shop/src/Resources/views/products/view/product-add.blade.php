{!! view_render_event('bagisto.shop.products.view.product-add.after', ['product' => $product]) !!}

<div class="add-to-buttons">
    @include ('shop::products.add-to-cart', ['product' => $product])

    @include ('shop::products.buy-now')
</div>
<span class="qty_exceeded"></span>

{!! view_render_event('bagisto.shop.products.view.product-add.after', ['product' => $product]) !!}
@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.home.page-title') }}
@endsection

@section('content-wrapper')

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/owl.carousel.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/assets/owl.carousel.min.css">
<style type="text/css">
.container {
  width: 1170px;
  margin: 0 auto;
}

.gallery-item {
  position: relative;
}
.gallery-item img {
  opacity: 1;
  transition: opacity 0.35s;
}
.gallery-item::before {
  position: absolute;
  content: "";
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #14222d;
  background-image: url(http://randomsite.zzz.com.ua/cabel/img/s8_zoom.png);
  background-position: center;
  background-repeat: no-repeat;
}
.gallery-item:hover img {
  opacity: 0.2;
  transition: opacity 0.35s;
}

.owl-prev {
  position: absolute;
  left: 0;
  top: 50%;
  transform: translateY(-50%);
}

.owl-next {
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
}

</style>
<div class="auth-content single-events-page section events-page">
	<div class="single-event section">
		<div class="sec1 row">
			<div class="col-md-7">
				<h1 class="event-title">{{$course->name}}</h1>
				<p><label>Date: </label> {{$course->course_date}}</p>
				<p><label>Venue: </label> {{$course->venue}}</p>
				<p><label>Contact: </label> {{$course->contact}}</p>
			</div>
			<div class="col-md-5 text-center">
				<img src="{{bagisto_asset('themes/default/assets/images/courses/'.$course->image)}}" alt="" />
			</div>
			<div class="hig-sec col-md-12 text-center">
				<h3 class="cor-tit">
            @if(strpos($course->slug, "webinar") != false)
              Webinar
            @else
              Highlights
            @endif
        </h3>
				{!! $course->course_highlights !!}
			</div>
		</div>				

		<div class="sec4">
			{!! $course->faculty !!}
		</div>

		<div class="sec2">
			{!! $course->description !!}
		</div>
		
		<div class="sec5">
			<h3 class="cor-tit">Review</h3>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4">
					<iframe style="width: 100%; height: 200px;" src="https://www.youtube.com/embed/NsyK_QlBZlE" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<iframe style="width: 100%; height: 200px;" src="https://www.youtube.com/embed/nr6rRXwrOcU" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<iframe style="width: 100%; height: 200px;" src="https://www.youtube.com/embed/Uq769xhT0-I" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
				</div>
			</div>
		</div>

</div></div></div>

<div  class="sec6">
@if($course->register_form==1)
<div class="main-container-wrapper">
<div class="single-event section">
		
			<form method="post" action="{{ route('shop.course.register',$course->slug) }}" @submit.prevent="onSubmit">
                @csrf
                <input type="hidden" name="course_name" value="{{$course->name}}">
                <div class="col-md-12 event-form">
                    <h2 class="contact-head">Register your seat now!</h2>
                    <div class="col-md-12 form-fields">
                        <div class="form-field col-xs-12 col-sm-12 col-md-12">
                            <div class="control-group col-xs-12 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option value="Mr.">Mr.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Mr. Dr.">Mr. Dr.</option>
                                    <option value="Mrs. Dr.">Mrs. Dr.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-xs-12 col-sm-5" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'" data-vv-as="&quot;{{ __('First Name') }}&quot;" value="{{ old('first_name') }}">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group col-xs-12 col-sm-5 cpass">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name" value="{{ old('last_name') }}">
                                
                            </div>
                        </div>
                        <div class="form-field col-xs-12 col-sm-12 col-md-12">
                            <div class="control-group col-md-4 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">Mobile Number</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ old('phone_number') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.phonenumber') }}&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>
                            <div class="control-group col-md-4 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
							<div class="control-group pass cpass col-md-4" :class="[errors.has('country_id') ? 'has-error' : '']">
                                <label for="country_id" class="required">Country</label>
                                <?php $states = array();
                                $countries = DB::table('countries')->get();?>
                                <select name="country_id" id="state" class="control" v-validate="'required'" data-vv-as="&quot;{{ __('Country') }}&quot;" >
                                    <option value="">Choose your country...</option>
                                    <option value="101">India</option>
<!--                                     <option value="101">India</option>
                                    <?php foreach($countries as $key => $country) { ?>
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    <?php } ?> -->
                                </select>
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
						</div>
						<div class="form-field col-xs-12 col-sm-12 col-md-12">
							<div class="control-group col-md-12 pass cpass" :class="[errors.has('course_msg') ? 'has-error' : '']">
								<label for="course_msg" class="required">Message</label>
								<textarea class="control" name="course_msg" value="{{ old('course_msg') }}" v-validate="'required'" data-vv-as="&quot;{{ __('Message') }}&quot;"></textarea>
								<span class="control-error" v-if="errors.has('course_msg')">@{{ errors.first('course_msg') }}</span>
							</div>
						</div>
                        <div class="form-field col-xs-12 col-sm-12 col-md-12">
							<button class="btn btn-primary btn-lg" type="submit">Submit</button>
						</div>
                    </div>
                </div>
            </form>
		

<!--	
		<h2 class="event-title text-center">{{$course->name}}</h2>

		<ul class="nav nav-tabs text-center col-md-12 col-xs-12">
		 	<li class="active"><a data-toggle="tab" href="#overview">Overview</a></li>
		  	<li><a data-toggle="tab" href="#special-offer">Special Offer</a></li>
		</ul>
		<div class="tab-content">
		    <div id="overview" class="tab-pane fade in active">
				<div class="section event-content col-md-12"></div>
		    </div>
		    <div id="special-offer" class="tab-pane fade in">
		    	<div class="section col-md-12 col-xs-12">
            <form method="post" action="{{ route('shop.course.register',$course->slug) }}" @submit.prevent="onSubmit">

                @csrf
                <input type="hidden" name="course_name" value="{{$course->name}}">
                <div class="col-md-12 event-form">
                    <h2 class="contact-head">Register your seat for FREE</h2>
                    <div class="col-md-12 form-fields">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-3 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option value="Mr.">Mr.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Mr. Dr.">Mr. Dr.</option>
                                    <option value="Mrs. Dr.">Mrs. Dr.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-md-5" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'" value="{{ old('first_name') }}">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group col-md-4 cpass">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name" value="{{ old('last_name') }}">
                                
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">Mobile Number</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ old('phone_number') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.phonenumber') }}&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>

                            <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group pass cpass col-md-12" :class="[errors.has('country_id') ? 'has-error' : '']">
                                <label for="country_id" class="required">Country</label>
                                <?php $states = array();
                                $countries = DB::table('countries')->get();?>
                                <select name="country_id" id="state" class="control" v-validate="'required'">
                                    <option value="">Choose your country...</option>
                                    <?php foreach($countries as $key => $country) { ?>
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                        </div>
                    <div class="form-field col-md-12">
                        <div class="control-group col-md-12 pass cpass" :class="[errors.has('course_msg') ? 'has-error' : '']">
                            <label for="course_msg" class="required">Message</label>
                            <textarea class="control" name="course_msg" value="{{ old('course_msg') }}" v-validate="'required'"></textarea>
                            <span class="control-error" v-if="errors.has('course_msg')">@{{ errors.first('course_msg') }}</span>
                        </div>
                    </div>
                        <button class="btn btn-primary btn-lg" type="submit">
                           Submit
                        </button>
                    </div>
                </div>
            </form>
    </div>
</div>
		</div>
-->
	
</div>
</div>
@endif

@endsection

@push('scripts')

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>


<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

<script>
    $(document).ready(function() {
        $('#myCarousel').carousel({
          interval: 10000
        });
        $('#myCarousel2').carousel({
          interval: 10000
        });
    });

$('.popup-gallery').magnificPopup({
  delegate: '.col-md-3 > a',
  type: 'image',
  removalDelay: 500, //delay removal by X to allow out-animation
  callbacks: {
    beforeOpen: function () {
      // just a hack that adds mfp-anim class to markup 
      this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
      this.st.mainClass = this.st.el.attr('data-effect');
    } },

  tLoading: 'Loading image #%curr%...',
  mainClass: 'mfp-img-mobile',
  gallery: {
    enabled: true,
    navigateByImgClick: true,
    preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
  },
  image: {
    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
    titleSrc: function (item) {
      return item.el.attr('title') + '<small></small>';
    } }
});
</script>
@endpush

 @extends('shop::layouts.master')

@section('page_title')
    Courses - GenXT
@endsection

@section('content-wrapper')
	<div class="section events-page col-md-12 col-xs-12">
		<ul class="nav nav-tabs text-center col-md-12 col-xs-12">
			<li class="active"><a data-toggle="tab" href="#upcoming-courses" style="margin-right: 0px">Upcoming Courses</a></li>
			<li><a data-toggle="tab" href="#past-courses" style="margin-right: 0px">Past Courses</a></li>
		</ul>
		<div class="tab-content">
			<div id="upcoming-courses" class="tab-pane fade in active">
				<ul class="all-events section">
					@foreach($upcoming_courses as $key => $course)
						<li class="each-event row">
							<div class="col-md-12"><a href="{{url('/courses/'.$course->slug)}}"><h3>{{$course->name}}</h3></a></div>
							<div class="col-md-9">
								<p><label>Date: </label> {{$course->course_date}} </p>
								<p><label>Venue: </label> {{$course->venue}} </p>
								<p><label>Contact: </label> {{$course->contact}} </p>
							</div>
							<div class="col-md-3">
								<a class="btn" href="{{url('/courses/'.$course->slug)}}">Read More</a>
							</div>
						</li>
					@endforeach
		  		</ul>
		    </div>
			<div id="past-courses" class="tab-pane">
			    <ul class="all-events section">
			    	@foreach($past_courses as $key => $course)
						<li class="each-event row">
							<div class="col-md-12"><a href="{{url('/courses/'.$course->slug)}}"><h3>{{$course->name}}</h3></a></div>
							<div class="col-md-9">
								<p><label>Date: </label> {{$course->course_date}} </p>
								<p><label>Venue: </label> {{$course->venue}} </p>
								<p><label>Contact: </label> {{$course->contact}} </p>
							</div>
							<div class="col-md-3">
								<a class="btn" href="{{url('/courses/'.$course->slug)}}">Read More</a>
							</div>
						</li>
			  		@endforeach
			  	</ul>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
	<script>
		$(function(){
			$('.content-container').attr('style', 'min-height: calc(100vh - 375px);');
		});
	</script>
@endpush
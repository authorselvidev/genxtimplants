@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.search.page-title') }}
@endsection

@section('content-wrapper')
    @if (! $results)
        <div class="search-result-status">                        
            <h2>{{ __('shop::app.products.whoops') }}</h2>
            <span>No products found</span>
        </div>
    @endif

    @if ($results)
        <div class="main mb-30" style="min-height: 27vh;">            
            <div class="search-result-status mb-20">
                <span><b>{{ $results->count() }} </b>@if($results->count() == 1){{ __('shop::app.search.found-result') }}@else{{ __('shop::app.search.found-results') }}@endif : <strong>{{ $term }}</strong></span>
            </div>
            
            <div class="product-grid-3">
                @foreach ($results as $productFlat)
                    <?php $sdata=['product'=> $productFlat->product,'product_info'=>$productFlat->length_label]; ?>
                    @include ('shop::products.list.card', $sdata)
                @endforeach
            </div>
        </div>
    @endif
@endsection

@push('scripts')
<style>
    .cart-increment {
        width: 136px !important;
    }
    .product-card{
        margin-bottom: 50px !important;
    }
</style>
<script>
    $(document).ready(function () {
 $('body').on('click','.cart-increment > button',function(){
            addtocartsection($(this))
        });
        $('body').on('click','.btn-addtocart',function(){
            addtocartsection($(this))
        });
        $("body").on('focusout','.cart_count',function(){
            addtocartsection($(this))
        });
    });
  function addtocartsection(this_var){
        var product_id =this_var.closest('form').find('input[name="product"]').val();
            var this_cls = this_var;
            this_cls.removeAttr("disabled");
            var route_name = "{{ route('cart.add', ':id') }}";

            var quantity =this_var.parent().parent().find('input[name="quantity"]').val();

            
            if(quantity == "") quantity = 1;
            else if (this_cls.hasClass('addtocart')){
                  quantity++; 
            }
            
            if(this_cls.hasClass('remove_cart')){ 
                route_name = "{{ route('cart.remove_qty', ':id') }}";
                quantity--;
                
            }

            // alert((quantity >= 0));
  /*          this_cls.parent().find('.cart_count').val(quantity);
            alert(quantity);
            return false;*/

            if(quantity >= 0){
                this_cls.parent().find('.cart_count').val(quantity);

                route_name = route_name.replace(':id', product_id);
                $.ajax({
                      url: route_name,
                      method: 'POST',
                      data: {
                        product: this_var.parent().parent().find('input[name="product"]').val(),
                        quantity: quantity,
                        discount: this_var.parent().parent().find('input[name="discount"]').val(),
                        is_configurable: $(this).parent().parent().find('input[name="is_configurable"]').val(),
                        _token: this_var.parent().parent().find('input[name="_token"]').val()
                    },
                      success: function(data){
                        console.log(data);
                        if(data == false){
                            alert('Out of stock');
                            // this_cls.closest('.product-information').find('.qty_exceeded').html('Out of stock');
                            var prev_quantity=parseInt(quantity)-1
                            this_cls.parent().find('.cart_count').val(prev_quantity);

                            return false;
                        }
                        else{
                            this_cls.closest('.product-information').find('.qty_exceeded').html('');
                            // this_cls.parent().find('.cart_count').val(data.quantity);
                        }

                        
                        if(data.quantity > 0) 
                            this_cls.closest('form').find('.remove_cart').addClass("remove-item");
                        else
                           this_cls.closest('form').find('.remove_cart').removeClass("remove-item"); 

                        this_cls.parent().find('.cart-increment').show();
                        $.ajax({
                            url: '{{ route("shop.checkout.minicart") }}',
                            method: 'GET',
                            success: function(response) {
                                var result = $(response).find('.cart-dropdown-container').html();
                                   $('.cart-dropdown-container').html(result);
                            }
                        });
                        this_cls.parent().find('.btn-addtocart').hide();
                      },
                    error: function(data){
                        var errors = data.responseJSON;
                        console.log(errors);
                    }
                });
                if (quantity==0) {
                    this_cls.closest('form').find('.cart-increment').removeClass('show').addClass('hide');
                    this_cls.closest('form').find('.btn-addtocart').removeClass('hide').addClass('show');
                }
                else{
                    this_cls.closest('form').find('.cart-increment').removeClass('hide').addClass('show');
                    this_cls.closest('form').find('.btn-addtocart').removeClass('show').addClass('hide');  
                }
            }
        }

       
</script>
@endpush

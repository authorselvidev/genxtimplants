<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    @if(!strcmp(request()->getHost(),'genxtimplants.com')) 
    <!-- Facebook Verification -->
    <meta name="facebook-domain-verification" content="sfuoewc55f57uiku370ik3h71k7v3r" />
    <!-- Facebook Verification -->

        <!-- Logrocket code -->
        <script src="https://cdn.lr-ingest.io/LogRocket.min.js" crossorigin="anonymous"></script>
        <script>window.LogRocket && window.LogRocket.init('efycfu/genxt-implants');</script>
        <!-- Logrocket code -->
    @endif

    <title>@yield('page_title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">


        @if(auth()->guard('admin')->user() !=null &&  auth()->guard('admin')->user()->role_id == 1)
            <link rel="manifest" href="https://genxtimplants.com/manifest-admin.json">
            <link rel="prefetch" href="https://genxtimplants.com/manifest-admin.json">
        @elseif( auth()->guard('admin')->user() !=null && auth()->guard('admin')->user()->role_id == 2)
            <link rel="manifest" href="https://genxtimplants.com/manifest-dealer.json">
            <link rel="prefetch" href="https://genxtimplants.com/manifest-dealer.json">
        @elseif( auth()->guard('admin')->user() !=null && auth()->guard('admin')->user()->role_id == 4)
            <link rel="manifest" href="https://genxtimplants.com/manifest-store.json">
            <link rel="prefetch" href="https://genxtimplants.com/manifest-store.json">
        @else
            <link rel="manifest" href="https://genxtimplants.com/manifest.json">
            <link rel="prefetch" href="https://genxtimplants.com/manifest.json">
        @endif
        <meta name="theme-color" content="#0995CD">    
        <script>
            if('serviceWorker' in navigator){
                window.addEventListener('load', function() {
                    @if( auth()->guard('admin')->user() !=null)
                        navigator.serviceWorker.register('https://genxtimplants.com/sw-admin.js')
                    @else
                        navigator.serviceWorker.register('https://genxtimplants.com/sw.js')
                    @endif
                        .then(function(registration) { console.log('Service worker ready'); registration.update(); })
                        .catch(function(error) { console.log('Registration failed with ' + error); });
                });
            }
            
            window.mobileCheck = function() {
                let check = false;
                (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
                    return check;
            };
        </script>
        

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/shop.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/header.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/webkul/ui/assets/css/ui.css') }}">
    <link href="{{ bagisto_asset('css/bootstrap.min.css') }}" rel="stylesheet">

    
    <script src="{!! bagisto_asset('js/jquery.min.js') !!}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{!! bagisto_asset('js/ajax_jquery.min.js') !!}"></script>
<script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_SITEKEY') }}"></script>
<script>
         grecaptcha.ready(function() {
             grecaptcha.execute('{{ env('RECAPTCHA_SITEKEY') }}', {action: 'contact'}).then(function(token) {
                if (token) {
                  document.getElementById('recaptcha').value = token;
                }
             });
         });
</script>

    @if ($favicon = core()->getCurrentChannel()->favicon_url)
        <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
    @else
        <link rel="icon" sizes="16x16" href="{{ bagisto_asset('images/favicon.ico') }}" />
    @endif

    @yield('head')

    @section('seo')
        <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
    @show

    @stack('css')
        <style>
            .footer-inner{
                /*flex-direction: column;*/
                align-items: center;
            }

            @media(max-width:940px){
                .footer-inner{
                    flex-direction: column;
                }        
                
                .logo_main{
                    margin-bottom: 20px;
                }
            }
        </style>

    {!! view_render_event('bagisto.shop.layout.head') !!}



</head>

<body @if (app()->getLocale() == 'ar')class="rtl"@endif>

    {!! view_render_event('bagisto.shop.layout.body.before') !!}

    <div id="app">
        <div class="overlay-checkout" style="display:none"><h3 style="float: left;width: 100%;"><img src="/themes/default/assets/images/loading.gif" style="padding: 10px 20px;">Please wait... We are processing your payment and this may take few seconds.</h3></div>
        <flash-wrapper ref='flashes'></flash-wrapper>

        

            {!! view_render_event('bagisto.shop.layout.header.before') !!}

            @include('shop::layouts.header.index')

            {!! view_render_event('bagisto.shop.layout.header.after') !!}

            @yield('slider')

            <div class="content-container" style="min-height: calc(100vh - 119px);backgroung:#F1F3F6">
                <div class="main-container-wrapper">
                    {!! view_render_event('bagisto.shop.layout.content.before') !!}

                    @yield('content-wrapper')

                    {!! view_render_event('bagisto.shop.layout.content.after') !!}
                    
                </div>            
            </div>

        {!! view_render_event('bagisto.shop.layout.footer.before') !!}

        @include('shop::layouts.footer.footer')

        {!! view_render_event('bagisto.shop.layout.footer.after') !!}

        <div class="footer-bottom">
            <div class="main-container-wrapper">
                <div class="footer-inner " style="display: flex;justify-content:space-between">
                    <div class="logo_main">
                        <a href="{{ url('/') }}"><img src="{!! bagisto_asset('images/footer-logo.png') !!}"></a>
                    </div>
                    <div class="copyright">
                        <p  class="developed">Copyright &copy; <?php echo date('Y'); ?> GenXT. All Rights Reserved</p>                        
                    </div>
                    <div class="social-icons" style="margin-left:8px">
                        <a target="_blank" class="terms-footer"  href="https://genxtimplants.com/terms-conditions">Terms & Conditions</a>
                        <a target="_blank" class="terms-footer" style="margin-right: 10px;margin-left: 10px" href="https://genxtimplants.com/privacy-policy">Privacy Policy</a>
                        <a target="_blank" class="terms-footer" style="margin-right: 10px" href="https://genxtimplants.com/return-refund-policy">Refund & Return Policy</a>
                        <a target="_blank" class="terms-footer" style="margin-right: 10px" href="https://genxtimplants.com/shipping-policy">Shipping Policy</a>
                        
                    </div>                    
                    <ul style="display: flex;justify-content:center;align-items:center;margin-top:4px;">
                        <li style="margin-right: 15px"><a href="https://www.facebook.com/genxt.implants" style="font-size: 20px" target="_blank"><i class="fa fa-facebook" style="color:#FFF" aria-hidden="true"></i></li>
                        <li><a href="https://www.youtube.com/c/genxtimplantology" style="font-size: 20px" target="_blank"><i class="fa fa-youtube-play" style="color:#FFF" aria-hidden="true"></i></a></li>
                    </ul>
				</div>
            </div>
            <div id="WAButton"></div>  
        </div>

    </div>


    <script type="text/javascript">
        window.flashMessages = [];

        @if ($success = session('success'))
            window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
        @elseif ($warning = session('warning'))
            window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
        @elseif ($error = session('error'))
            window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }
            ];
        @elseif ($info = session('info'))
            window.flashMessages = [{'type': 'alert-info', 'message': "{{ $info }}" }
            ];
        @endif

        window.serverErrors = [];
        @if(isset($errors))
            @if (count($errors))
                window.serverErrors = @json($errors->getMessages());
            @endif
        @endif
    </script>

    <script type="text/javascript" src="{{ bagisto_asset('js/shop.js') }}"></script>
    <script src="{{ bagisto_asset('js/jquery-3.3.1.min.js') }}"></script>
    <link rel="stylesheet" href="{{ bagisto_asset('css/floating-wpp.min.css') }}">
    <script src="{{ bagisto_asset('js/floating-wpp.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>
    <script src="{{ bagisto_asset('js/bootstrap.min.js') }}"></script>
    

    @stack('scripts')

    {!! view_render_event('bagisto.shop.layout.body.after') !!}

    <div class="modal-overlay"></div>
    <?php $poopup_message = 'Hi,'."\n\r".' Welcome to GenXT Dental Implants, we provide the latest range of dental implants that can help you give a happy smile to all your patients. How can we help you? You can msg your query here , or call 8484088331 for a customer support agent to help you.'; ?>
<script>
            $(function(){
    var current = location.href;
    $('.category-nav li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('href') === current){
            $this.parent().addClass('active');
        }
    })
})
        </script>
            <script type="text/javascript">  
   $(function () {
           $('#WAButton').floatingWhatsApp({
               phone: '918484088331', //WhatsApp Business phone number
               headerTitle: 'Chat with us on WhatsApp!', //Popup Title
               popupMessage: 'Hi, Welcome to GenXT Dental Implants, we provide the latest range of dental implants that can help you give a happy smile to all your patients. How can we help you? You can msg your query here , or call 8484088331 for a customer support agent to help you.', //Popup Message
               showPopup: true, //Enables popup display
               buttonImage: '<img src="{!! bagisto_asset('images/whatsapp.svg') !!}" />', //Button Image
               headerColor: '#0995CD', //Custom header color
               backgroundColor: '#0995CD', //Custom background button color
               position: "right" //Position: left | right,
              

           });
       });
   $(window).scroll(function(){
      if ($(this).scrollTop() > 425) {
          $('.header-top-section').addClass('header-fixed');
      } else {
          $('.header-top-section').removeClass('header-fixed');
      }
  });
</script>
@if(!strcmp(request()->getHost(), 'genxtimplants.com'))  
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '1189489958117764'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=1189489958117764&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178144758-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-178144758-1');
    </script>
    <!--  - Google Analytics -->
@endif

</body>

</html>

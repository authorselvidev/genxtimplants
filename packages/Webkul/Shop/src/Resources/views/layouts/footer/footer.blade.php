@if(request()->url() != route('customer.session.index'))
    @if(request()->url() != route('customer.register.index'))
        <div class="footer">
            <div class="main-container-wrapper">
                <div class="footer-content">  
                    <div class="support-sales">
                        <h3 class="text-center">Support and sales</h3>
                        <ul class="col-md-12 col-sm-12">
                            <?php $get_support = DB::table('support_and_sales')->where('is_deleted',0)->get();
                                foreach ($get_support as $key => $support) { ?>
                                    <li class="text-center">
                                        @if($key<=2)
                                            <a href="<?php if($support->id==1){
                                                        echo(Request::root().'/product-list');
                                                    }else if($support->id==2){
                                                        echo(Request::root().'/contact-us');             
                                                    }else if($support->id==3){
                                                        echo(Request::root().'/courses');
                                                    }?>">
                                                <img src="{!! bagisto_asset('images/'.$support->support_image) !!}">
                                                <p><?php echo $support->support_text; ?></p>
                                            </a>
                                        @else
                                            <img src="{!! bagisto_asset('images/'.$support->support_image) !!}">
                                            <p><?php echo $support->support_text; ?></p>
                                        @endif
                                    </li>
                            <?php  }  ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif


    <?php /*
        <!--<div class="footer-list-container">

            @if (count($categories))
                <div class="list-container">
                    <span class="list-heading">Categories</span>

                    <ul class="list-group">
                        @foreach ($categories as $key => $category)
                            <li>
                                <a href="{{ route('shop.categories.index', $category->slug) }}">{{ $category->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! DbView::make(core()->getCurrentChannel())->field('footer_content')->render() !!}

            <div class="list-container">
                <span class="list-heading">{{ __('shop::app.footer.subscribe-newsletter') }}</span>
                <div class="form-container">
                    <form action="{{ route('shop.subscribe') }}">
                        <div class="control-group" :class="[errors.has('email') ? 'has-error' : '']">
                            <input type="email" class="control subscribe-field" name="email" placeholder="Email Address" required><br/>

                            <button class="btn btn-md btn-primary">{{ __('shop::app.subscription.subscribe') }}</button>
                        </div>
                    </form>
                </div>

                <span class="list-heading">{{ __('shop::app.footer.locale') }}</span>
                <div class="form-container">
                    <div class="control-group">
                        <select class="control locale-switcher" onchange="window.location.href = this.value">

                            @foreach (core()->getCurrentChannel()->locales as $locale)
                                <option value="?locale={{ $locale->code }}" {{ $locale->code == app()->getLocale() ? 'selected' : '' }}>{{ $locale->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="currency">
                    <span class="list-heading">{{ __('shop::app.footer.currency') }}</span>
                    <div class="form-container">
                        <div class="control-group">
                            <select class="control locale-switcher" onchange="window.location.href = this.value">

                                @foreach (core()->getCurrentChannel()->currencies as $currency)
                                    <option value="?currency={{ $currency->code }}" {{ $currency->code == core()->getCurrentCurrencyCode() ? 'selected' : '' }}>{{ $currency->code }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>--> */ ?>
 
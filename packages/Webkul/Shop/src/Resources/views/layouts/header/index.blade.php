<div class="header_new">
    <div class="main_container">  
        <span class="menu_toggle">
            <i class="fa fa-bars"></i>
            <span style="font-size:13px">Menu</span>            
        </span>

        <nav class="nav_container">
            <a href="{{ route('shop.home.index') }}">
                @if ($logo = core()->getCurrentChannel()->logo_url)
                    <img  class="logo_genxt" src="{{ $logo }}" />
                @else
                    <img  class="logo_genxt" src="{{ bagisto_asset('images/genxt-logo.png') }}" />
                @endif
            </a>
            
            <?php $menus = DB::table('menus')->where('is_active',1)->get(); ?>
            
            <ul class="menu_items" id="menu_toggle_target">
                @foreach($menus as $key => $menu)
                    <li>
                        <a href="{{ $menu->menu_link }}"> {{ $menu->menu_title }} </a>
                    </li>
                @endforeach            
            </ul>
        </nav>

        <div class="search_container">
            <form role="search" action="{{ route('shop.search.index') }}" method="POST" id="head_search">
                @csrf
                <div class="searchBar">                    
                    <input type="text" id="searchInput" name="term" class="search_box" placeholder="Search product..." autocomplete="off">
                    <button id="search_submit" type="submit">
                        <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="#666666" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                        </svg>
                    </button>
                </div>
            </form>
        </div>

        <div class="profile_container">
            @if(Auth::guard('admin')->check() || Auth::guard('customer')->check())
                <?php
                    $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
                    
                    if(empty($user))
                        $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
                    
                    if($user->role_id == 3)
                        $user_route = 'customer';
                    else
                        $user_route = 'admin';
                ?>
                
                <div class="profile_sec">
                    <div class="dropdown-toggle" id="cust_profile" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-user hidden-lg hidden-md hidden-sm"></i> <span class="name">{{ $user->first_name }}</span>
                        <i class="fa fa-angle-down"></i>
                    </div>
                
                    <ul class="dropdown-menu account customer" aria-labelledby="cust_profile">
                        <li class="hidden-md hidden-sm hidden-lg">
                            <a href="{{route($user_route.'.account.index') }}"><i class="fa fa-user"></i>My Account</a>
                        </li>
                        <li>
                            <a href="{{ route($user_route.'.orders.index') }}"><i class="fa fa-shopping-cart"></i>{{ __('shop::app.header.orders') }}</a>
                        </li>
                        <li>
                            <a href="{{ route($user_route.'.wishlist.index') }}"><i class="fa fa-heart"></i>{{ __('shop::app.header.wishlist') }}</a>
                        </li>
                        <li>
                            <a href="{{ route($user_route.'.profile.index') }}"><i class="fa fa-user"></i>{{ __('shop::app.header.profile') }}</a>
                        </li>
                        
                        @if($user->role_id == 2)
                            <li>
                                <a href="{{ route($user_route.'.dashboard.index') }}"><i class="fa fa-user"></i>Dashboard</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.failure.implants.index') }}"><i class="fa fa-user"></i>Return Implants</a>
                            </li>
                        @elseif($user->role_id == 3)
                            <li>
                                <a href="{{ route('failure.implants.index') }}"><i class="fa fa-user"></i>Return Implants</a>
                            </li>
                        @endif           

                        <li>
                            <a href="{{ route($user_route.'.session.destroy') }}"><i class="fa fa-sign-out"></i>{{ __('shop::app.header.logout') }}</a>
                        </li>
                    </ul>
                </div>

                <div class="right-content-menu">
                    {!! view_render_event('bagisto.shop.layout.header.cart-item.before') !!}
                        <div class="cart-dropdown-container">
                            @include('shop::checkout.cart.mini-cart')
                        </div>
                    {!! view_render_event('bagisto.shop.layout.header.cart-item.after') !!}
                </div>               
                
            @else
                <a href="{{ route('customer.session.index') }}">{{ __('shop::app.header.sign-in') }}</a> 
                <span style="color: #FFF" class="hide_mobile"> | </span>
                <a href="{{ route('customer.register.index') }}" class="hide_mobile">{{ __('shop::app.header.sign-up') }}</a>
            @endif
        </div>
        
        {!! view_render_event('bagisto.shop.layout.header.account-item.after') !!}
    </div>
</div>

@push('scripts')
    <script>
        $(function(){
            $('#head_search').on('submit', function(e){
                if(!($(this).find('#searchInput').val())){
                    e.preventDefault();
                };
            });

            $('.menu_toggle').click(function(){
                $('#menu_toggle_target').slideToggle();
            });

            $(window).resize(function(size){
                if($(window).width()>=850){
                    $('#menu_toggle_target').removeAttr("style");
                }
            });            
        });              
    </script>    
@endpush
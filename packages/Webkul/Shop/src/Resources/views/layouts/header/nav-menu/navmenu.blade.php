{!! view_render_event('bagisto.shop.layout.header.category.before') !!}
<!--<category-nav categories='@json($categories)' url="{{url()->to('/')}}"></category-nav>-->

<?php 
$get_categories = DB::table('categories as c')->where('c.status' , 1)->join('category_translations as ct', 'c.id', 'ct.category_id')->get();
 ?>
	<ul class="category-nav">
		@foreach($get_categories as $key => $category)
		 <li id="category-{{$key}}" class="col-xs-6">
		 	<a href="{{ url('categories/'.$category->slug) }}#category-7">
		 		<img src="{{ url('storage/app/public/'.$category->image) }}">
		 		<p>{{ $category->name }} </p>
		 	</a>
		 </li>

		@endforeach
	</ul>

{!! view_render_event('bagisto.shop.layout.header.category.after') !!}

@extends('shop::layouts.master')

@section('page_title')
    {{ (isset($page->meta_title)) ? $page->meta_title : ((isset($page->page_name)) ? $page->page_name : "") }}
@stop

@section('seo')
   
@stop

@section('content-wrapper')

<?php if(isset($page->page_content)) { ?>
{!! $page->page_content !!}
<?php } ?>
@stop
 @extends('shop::emails.layouts.default')

@section('email-wrapper')

<div>
	Dear {{ $user->first_name }},<br/><br/>
	<div class="sub-content">
		<p>Your registration process is not completed, Please click on the link below to continue the registration process</p><br/><br/>
		<a href="{{$verify_url}}">Continue Registration</a>
		<br/><br/>
		<div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
            For any queries or support please WhatsApp +918484088331<br/>
        </div>

	</div>
</div>

@endsection

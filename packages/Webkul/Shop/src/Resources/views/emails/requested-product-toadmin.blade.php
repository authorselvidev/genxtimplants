 @extends('shop::emails.layouts.default')

@section('email-wrapper')

<div>
	Dear {{$admin_name}}, <br/>
	<div class="sub-content">
		The product <strong>{{$product_name}}</strong> which is in out of stock now. The follwing doctor has requested  this product. Please contact them via the details given below.<br/><br/>
		Name: {{$first_name}}<br/>
		Email: {{$email}}<br/>
	</div>
</div>

@endsection

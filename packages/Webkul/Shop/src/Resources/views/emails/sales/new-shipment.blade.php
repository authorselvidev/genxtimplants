 @extends('shop::emails.layouts.default')

@section('email-wrapper')
  
    <?php $order = $shipment->order;
    $customer_full_name = $order->customer_first_name.' '.$order->customer_last_name;
    //dd($order);?>

    <div style="padding: 30px;">
        <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
            
            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                Hello Dr {{ $customer_full_name }},
            </p><br/><br/>

            <div class="sub-content">Thank you for you order<br/>
            Your order has been shipped. The following items have been sent to you.</div><br/><br/>

            <p class="sub-content" style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {!! __('shop::app.mail.order.greeting', [
                    'order_id' => '<a href="' . route('customer.orders.view', base64_encode($order->id)) . '" style="color: #0041FF; font-weight: bold;">#' . $order->id . '</a>',
                    'created_at' => $order->created_at
                    ]) 
                !!}
            </p>
        </div>
        <div class="sub-content">
        <div style="font-weight: bold;font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 20px !important;">
            {{ __('shop::app.mail.shipment.summary') }}
        </div>

        <div style="display: flex;flex-direction: row;margin-top: 20px;justify-content: space-between;margin-bottom: 40px;">
            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    {{ __('shop::app.mail.order.shipping-address') }}
                </div>

                <?php $shipping_address = DB::table('user_addresses')->where('id',$shipment->shippingAdress)->first(); // ?>
                <div>
                    @include ('admin::sales.address', ['address' => $shipping_address])
                </div>

                <div>---</div>


                <div style="font-size: 16px;color: #242424;">
                    <div style="font-weight: bold;">
                        {{ $order->shipping_title }}
                    </div>

                    <div style="margin-top: 5px;">
                        <span style="font-weight: bold;">{{ __('shop::app.mail.shipment.carrier') }} : </span>{{ $shipment->carrier_title }}
                    </div>

                    <div style="margin-top: 5px;">
                        <span style="font-weight: bold;">{{ __('shop::app.mail.shipment.tracking-number') }} : </span>{{ $shipment->track_number }}
                    </div>
                </div>
            </div>

            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    {{ __('shop::app.mail.order.billing-address') }}
                </div>

                <?php $billing_address = DB::table('user_addresses')->where('id',$shipment->billingAdress)->first();  ?>
                <div>
                    @include ('admin::sales.address', ['address' => $billing_address])
                </div>

                <div>---</div>

            </div>
        </div>

        @foreach ($shipment->items as $item)
            <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px;margin-bottom: 10px">
                <p style="font-size: 18px;color: #242424;line-height: 24px;margin-top: 0;margin-bottom: 10px;font-weight: bold;">
                    {{ $item->name }}
                </p>

                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        {{ __('shop::app.mail.order.price') }}
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ core()->formatPrice($item->price, $order->order_currency_code) }}
                    </span>
                </div>

                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        {{ __('shop::app.mail.order.quantity') }}
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ $item->qty }}
                    </span>
                </div>                
            </div>
        @endforeach

        <div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
           <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
            For any queries or support please WhatsApp +918484088331<br/><br/><br/><br/>
        </div>
    </div>
        
    </div>
@endsection

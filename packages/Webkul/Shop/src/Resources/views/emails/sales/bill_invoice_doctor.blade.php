@extends('shop::emails.layouts.default')

@section('email-wrapper')
<div>
    <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
        <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
            Hi, {{$customer->my_title}} {{$customer->first_name}} {{$customer->last_name}},
        </p><br/>
        <div class="sub-content" style="margin:5px 0;font-family:Arial, sans-sarif;font-size: 16px">Thank you for the purchase. @if($order_completed==true) Your order status for an order #{{$order->id}} has been completed. @else Your invoice for order #{{$order->id}} is attached to this email for your reference. @endif</div>
        
    </div>

    <div class="sub-content" style="font-family:Arial, sans-sarif;font-size: 16px">
        In this year {{date('Y')}}, you have purchased products worth of Rs {{$total_orders_amount_year}}.<br><br>

        @if($current_group_offers)
        <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 5px;padding: 15px;margin-bottom: 10px;font-family: Arial, sans-sarif;">
            <h3>Current Membership Group : {{\DB::table('user_groups')->where('id', $current_group_offers['customer_group_id'])->first()->name}}</h3>
            <strong>Discount Name :</strong><br>
            {{$current_group_offers["discount_title"]}}<br><br>
            <strong>Min Order Amount :</strong><br>
            INR {{number_format($current_group_offers["minimum_order_amount"],2)}}<br><br>
            <strong>Group Range :</strong><br>
            {{ \Webkul\Customer\Models\CustomerGroup::CustomerGroupAmountRange($current_group_offers["customer_group_id"]) }}<br><br>
            @if(strlen($current_group_offers["discount_description"]))
                <strong>Discount Description :</strong><br>
                {{$current_group_offers["discount_description"]}}
            @endif
        </div>
        @endif

        @if($next_group_offers)
            <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 5px;padding: 15px;margin-bottom: 10px;font-family: Arial, sans-sarif;">
                <h3>Next Membership Group : {{\DB::table('user_groups')->where('id', $next_group_offers['customer_group_id'])->first()->name}}</h3>
                <strong>Discount Name :</strong><br>
                {{$next_group_offers["discount_title"]}}<br><br>
                <strong>Min Order Amount :</strong><br>
                INR {{number_format($next_group_offers["minimum_order_amount"],2)}}<br><br>
                <strong>Group Range :</strong><br>
                {{ \Webkul\Customer\Models\CustomerGroup::CustomerGroupAmountRange($next_group_offers["customer_group_id"]) }}<br><br>        
                @if(strlen($next_group_offers["discount_description"]))
                    <strong>Discount Description :</strong><br>
                    {{$next_group_offers["discount_description"]}}
                @endif
            </div>
        @endif

        <h3>Reward Points in account : {{$reward_points}} Points</h3>

        Click on My Offers in the dashboard to see all other available offers.         
            
        <div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
            For any queries or support please WhatsApp +918484088331<br/>
        </div>
    </div>
</div>
@endsection

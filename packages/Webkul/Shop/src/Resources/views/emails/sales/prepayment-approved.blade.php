@extends('shop::emails.layouts.default')

@section('email-wrapper')
    <div style="padding: 30px;">
        <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                Hello Dr {{ $user->first_name }} {{$user->last_name}},
            </p>

            <br/>
            <div class="sub-content">
                Your prepayment for the order #{{$order->id}} has been Approved.

                <div style="font-weight: bold;font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 20px !important;">
           The items listed below are part of the order.
        </div>

        <div style="display: inline-block;margin-top: 20px;justify-content: space-between;margin-bottom: 40px;">
            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    {{ __('shop::app.mail.order.shipping-address') }}
                </div>
                

                <?php $get_order = DB::table('orders')->where('id',$order->id)->first();
                    $shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->first();
                //dd($order->shipping_address);
                  ?>
              <div>
                @include ('admin::sales.address', ['address' => $shipping_address])
            </div>

                <div style="padding:20px 0;"></div>
                
            </div>

            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    {{ __('shop::app.mail.order.billing-address') }}
                </div>

                <?php $billing_address=DB::table('user_addresses')->where('id',$get_order->billing_address)->first(); ?>

                <div>
                @include ('admin::sales.address', ['address' => $billing_address])
            </div>

            <div style="padding:20px 0;"></div>

                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    Payment Method
                </div>
                <div>
                    {{ $order->payment_method }}
                </div>
            </div>
        </div>

        <div style="font-weight: bold;font-size: 16px;color: #242424;padding:0 0 10px">Ordered Products</div>
        @foreach ($order->items as $item)
        
            <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px;margin-bottom: 10px">
                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        Title
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ $item['name'] }}
                    </span>
                </div>

                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        {{ __('shop::app.mail.order.price') }}
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ core()->formatPrice($item['price'], $order->order_currency_code) }}
                    </span>
                </div>

                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        {{ __('shop::app.mail.order.quantity') }}
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ $item['qty_ordered'] }}
                    </span>
                </div>
                
                
            </div>
        @endforeach

        <div style="font-size: 16px;color: #242424;line-height: 30px;float: right;margin-top: 20px;">
            <div>
                <span>{{ __('shop::app.mail.order.subtotal') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->sub_total, $order->order_currency_code) }}
                </span>
            </div>
            

            @if($order->base_discount_amount != 0.0000)
            <?php $special_price = abs($order->sub_total - $order->base_discount_amount);?>
            <div>
                <span>Special Price</span>
                <span style="float: right;">
                    {{ core()->formatPrice($special_price, $order->order_currency_code) }}
                </span>
            </div>
            @endif

            @if($order->promo_code_amount != 0)
            <div>
                <span>PromoCode Discount</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->promo_code_amount, $order->order_currency_code) }}
                </span>
            </div>
            @endif


            @if($order->mem_discount_amount != 0)
            <div>
                <span>Membership Discount</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->mem_discount_amount, $order->order_currency_code) }}
                </span>
            </div>
            @endif

            @if($order->reward_coin != 0)
            <div>
                <span>Reward Coins</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->reward_coin, $order->order_currency_code) }}
                </span>
            </div>
            @endif
            
            <div>
                <span>{{ __('shop::app.mail.order.shipping-handling') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->shipping_amount, $order->order_currency_code) }}
                </span>
            </div>
            <div>
                <span>{{ __('shop::app.mail.order.tax') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->tax_amount, $order->order_currency_code) }}
                </span>
            </div>

            <div style="font-weight: bold">
                <span>{{ __('shop::app.mail.order.grand-total') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}
                </span>
            </div>
            
        </div>
            </div>
            <br/>
            <br/>
        </div>
        <div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p>
                You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.
            </p>
            
            <br/>

                For any queries or support please WhatsApp +918484088331.
            <br/><br/>
        </div>
    </div>
@endsection

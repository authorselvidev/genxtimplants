@component('shop::emails.layouts.master')
    <div style="text-align: center;">
        <a href="{{ config('app.url') }}">
            @if ($logo = core()->getCurrentChannel()->logo_url)
                <img class="logo" src="{{ $logo }}" />
            @else
                <img class="logo" src="{{ bagisto_asset('images/genxt-logo.png') }}" />
            @endif
        </a>
    </div>

    <?php $order = $invoice->order; 
    $customer_full_name = $order->customer_first_name.' '.$order->customer_last_name; ?>

    <div style="padding: 30px;">
        <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
            <span style="font-weight: bold;">
                {{ __('shop::app.mail.invoice.heading', ['order_id' => $order->id, 'invoice_id' => $invoice->id]) }}
            </span> <br>

            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {{ __('shop::app.mail.order.dear', ['customer_name' => $customer_full_name]) }},
            </p>

            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {!! __('shop::app.mail.order.greeting', [
                    'order_id' => '<a href="' . route('customer.orders.view', base64_encode($order->id)) . '" style="color: #0041FF; font-weight: bold;">#' . $order->id . '</a>',
                    'created_at' => $order->created_at
                    ]) 
                !!}
            </p>
        </div>

        <div style="font-weight: bold;font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 20px !important;">
            {{ __('shop::app.mail.invoice.summary') }}
        </div>

        <div style="display: flex;flex-direction: row;margin-top: 20px;justify-content: space-between;margin-bottom: 40px;">
            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    {{ __('shop::app.mail.order.shipping-address') }}
                </div>

                <?php $shipping_address = DB::table('user_addresses')->where('id',$invoice->shippingAdress)->first(); // ?>
                <div>
                    @include ('admin::sales.address', ['address' => $shipping_address])
                </div>

                <div>---</div>
               
            </div>

            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    {{ __('shop::app.mail.order.billing-address') }}
                </div>

                <?php $billing_address = DB::table('user_addresses')->where('id',$invoice->billingAdress)->first();  ?>
                <div>
                    @include ('admin::sales.address', ['address' => $billing_address])
                </div>

                <div>---</div>
            </div>
        </div>

        @foreach ($invoice->items as $item)
            <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px;margin-bottom: 10px">
                <p style="font-size: 18px;color: #242424;line-height: 24px;margin-top: 0;margin-bottom: 10px;font-weight: bold;">
                    {{ $item->name }}
                </p>
                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        {{ __('shop::app.mail.order.price') }}
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ core()->formatPrice($item->price, $invoice->order_currency_code) }}
                    </span>
                </div>

                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        {{ __('shop::app.mail.order.quantity') }}
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ $item->qty }}
                    </span>
                </div>
            </div>
        @endforeach

        <div style="font-size: 16px;color: #242424;line-height: 30px;float: right;width: 40%;margin-top: 20px;">
            <div>
                <span>{{ __('shop::app.mail.order.subtotal') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->sub_total, $order->order_currency_code) }}
                </span>
            </div>

            @if($order->base_discount_amount != 0.0000)
            <?php $special_price = abs($order->sub_total - $order->base_discount_amount);?>
            <div>
                <span>Special Price</span>
                <span style="float: right;">
                    {{ core()->formatPrice($special_price, $order->order_currency_code) }}
                </span>
            </div>
            @endif


            @if($order->promo_code_amount != 0)
            <div>
                <span>Promo Code Discount</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->promo_code_amount, $order->order_currency_code) }}
                </span>
            </div>
            @endif

            @if($order->mem_discount_amount != 0)
            <div>
                <span>Membership Discount Discount</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->mem_discount_amount, $order->order_currency_code) }}
                </span>
            </div>
            @endif

            @if($order->reward_coin != 0)
            <div>
                <span>Reward Coins</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->reward_coin, $order->order_currency_code) }}
                </span>
            </div>
            @endif


            <div>
                <span>{{ __('shop::app.mail.order.shipping-handling') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($invoice->shipping_amount, $invoice->order_currency_code) }}
                </span>
            </div>

            <div>
                <span>{{ __('shop::app.mail.order.tax') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->tax_amount, $order->order_currency_code) }}
                </span>
            </div>

            <div style="font-weight: bold">
                <span>{{ __('shop::app.mail.order.grand-total') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}
                </span>
            </div>
        </div>

        <div style="margin-top: 65px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {!! 
                    __('shop::app.mail.order.help', [
                        'support_email' => '<a style="color:#0041FF" href="mailto:' . config('mail.from.address') . '">' . config('mail.from.address'). '</a>'
                        ]) 
                !!}
            </p>

            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {{ __('shop::app.mail.order.thanks') }}
            </p>
        </div>
    </div>
@endcomponent

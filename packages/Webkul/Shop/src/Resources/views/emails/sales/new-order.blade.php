 @extends('shop::emails.layouts.default')

@section('email-wrapper')

    <div style="padding: 30px;">
        <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">

        <?php
            $role_id = \Webkul\Customer\Models\Customer::customerRoleId($user_id);
            $order_customer_role_id = \Webkul\Customer\Models\Customer::customerRoleId($order->customer_id);
            $is_doctor_prepayment = false;
            if($order->payment_method == "prepayment" && $order_customer_role_id == 3 && $role_id == 2){
                $is_doctor_prepayment = true;
            }
        ?>
        <?php $customer_name = \Webkul\Customer\Models\Customer::customerName($user_id);  ?>
        
        @if($role_id != 1 && $role_id != 4 && !$is_doctor_prepayment)
            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                Hello Dr {{$customer_name}},
            </p>
            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {!! __('shop::app.mail.order.greeting', [
                    'order_id' => '<a href="' . route('customer.orders.view', base64_encode($order->id)) . '" style="color: #0041FF; font$-weight: bold;">#' . $order->id . '</a>',
                    'created_at' => $order->created_at
                    ]) 
                !!}
            </p>

            @if($order->payment_method == 'prepayment')
                <p>Your order is placed, we are waiting for payment approval before dispatch, once payment is credited order will be dispatched.</p>
                <p>Here are the bank details of us.</p>
                <div style="font-weight: bold;font-size: 16px;color: #242424;padding:0 0 10px">Bank Details for payment</div>
                <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px;margin-bottom: 10px">
                    <div style="margin-bottom: 10px;">
                        <label style="font-size: 16px;color: #5E5E5E;">
                            Account Name
                        </label>
                        <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                            M/S Regenics
                        </span>
                    </div>

                    <div style="margin-bottom: 10px;">
                        <label style="font-size: 16px;color: #5E5E5E;">
                            Bank
                        </label>
                        <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                            HDFC
                        </span>
                    </div>

                    <div style="margin-bottom: 10px;">
                        <label style="font-size: 16px;color: #5E5E5E;">
                            Branch
                        </label>
                        <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                            East Street, Pune
                        </span>
                    </div>
                    <div style="margin-bottom: 10px;">
                        <label style="font-size: 16px;color: #5E5E5E;">
                            Account Number
                        </label>
                        <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                            50200001213129
                        </span>
                    </div>
                    <div style="margin-bottom: 10px;">
                        <label style="font-size: 16px;color: #5E5E5E;">
                            IFSC Code
                        </label>
                        <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                            HDFC0000148
                        </span>
                    </div>
                </div>
            @endif
            
            @else
           <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
               Hello {{$customer_name}},
            </p>
            <div>New order has been received by {{ \Webkul\Customer\Models\Customer::customerName($order->customer_id) }}</div>
            @endif
        </div>

        <div style="font-weight: bold;font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 20px !important;">
            {{ __('shop::app.mail.order.summary') }}
        </div>

        <div style="display: inline-block;margin-top: 20px;justify-content: space-between;margin-bottom: 40px;">
            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    {{ __('shop::app.mail.order.shipping-address') }}
                </div>
                

                <?php $shipping_address = DB::table('user_addresses')->where('id',$order->shippingAdress)->first();
                //dd($order);
                  ?>
              <div>
                @include ('admin::sales.address', ['address' => $shipping_address])
            </div>

                <div style="padding:20px 0;"></div>
                
            </div>

            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    {{ __('shop::app.mail.order.billing-address') }}
                </div>

                <?php $billing_address=DB::table('user_addresses')->where('id',$order->billingAdress)->first(); ?>

                <div>
                @include ('admin::sales.address', ['address' => $billing_address])
            </div>

            <div style="padding:20px 0;"></div>

                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    Payment Method
                </div>
                <div>
                    {{ $order->payment_method }}
                </div>
            </div>
        </div>

        <div style="font-weight: bold;font-size: 16px;color: #242424;padding:0 0 10px">Ordered Products</div>
        @foreach ($order->items as $item)
        
            <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px;margin-bottom: 10px">
                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        Title
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ $item['name'] }}
                    </span>
                </div>

                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        {{ __('shop::app.mail.order.price') }}
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ core()->formatPrice($item['price'], $order->order_currency_code) }}
                    </span>
                </div>

                <div style="margin-bottom: 10px;">
                    <label style="font-size: 16px;color: #5E5E5E;">
                        {{ __('shop::app.mail.order.quantity') }}
                    </label>
                    <span style="font-size: 18px;color: #242424;margin-left: 40px;font-weight: bold;">
                        {{ $item['qty_ordered'] }}
                    </span>
                </div>
                
                
            </div>
        @endforeach

        <div style="font-size: 16px;color: #242424;line-height: 30px;float: right;margin-top: 20px;">
            <div>
                <span>{{ __('shop::app.mail.order.subtotal') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->sub_total, $order->order_currency_code) }}
                </span>
            </div>
            

            @if($order->base_discount_amount != 0.0000)
            <?php $special_price = abs($order->sub_total - $order->base_discount_amount);?>
            <div>
                <span>Special Price</span>
                <span style="float: right;">
                    {{ core()->formatPrice($special_price, $order->order_currency_code) }}
                </span>
            </div>
            @endif

            @if($order->promo_code_amount != 0)
            <div>
                <span>PromoCode Discount</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->promo_code_amount, $order->order_currency_code) }}
                </span>
            </div>
            @endif


            @if($order->mem_discount_amount != 0)
            <div>
                <span>Membership Discount</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->mem_discount_amount, $order->order_currency_code) }}
                </span>
            </div>
            @endif

            @if($order->reward_coin != 0)
            <div>
                <span>Reward Coins</span>
                <span style="float: right;">
                   - {{ core()->formatPrice($order->reward_coin, $order->order_currency_code) }}
                </span>
            </div>
            @endif
            
            <div>
                <span>{{ __('shop::app.mail.order.shipping-handling') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->shipping_amount, $order->order_currency_code) }}
                </span>
            </div>
            <div>
                <span>{{ __('shop::app.mail.order.tax') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->tax_amount, $order->order_currency_code) }}
                </span>
            </div>

            <div style="font-weight: bold">
                <span>{{ __('shop::app.mail.order.grand-total') }}</span>
                <span style="float: right;">
                    {{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}
                </span>
            </div>
            
        </div>


        <div style="margin-top: 65px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block">
            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
               You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.
            </p><br/>
            For any queries or support please WhatsApp +918484088331<br/><br/><br/><br/>


        </div>


    </div>
@endsection

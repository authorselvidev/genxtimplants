@extends('shop::emails.layouts.default')

@section('email-wrapper')
<div>
    <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
        <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
            Hi, {{$customer->my_title}} {{$customer->first_name}} {{$customer->last_name}},
        </p><br/>
        <div class="sub-content" style="margin:5px 0;font-family:Arial, sans-sarif;font-size: 16px">Thank you for the purchase. @if($order_completed==true) Your order status for an order #{{$order->id}} has been completed. @else Your invoice for order #{{$order->id}} is attached to this email for your reference. @endif</div>
        
    </div>

    <div class="sub-content" style="font-family:Arial, sans-sarif;font-size: 16px">
        In this year {{date('Y')}}, you have purchased products worth of Rs {{$total_orders_amount_year}}.<br><br>

        @if($customer->credit_available==1)
            <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 5px;padding: 15px;margin-bottom: 10px;font-family: Arial, sans-sarif;">
                <strong style="display:inline-block; margin-bottom: 5px;">Credit Limit :</strong><br>
                Rs {{ number_format($customer->credit_limit, 2) }}<br><br>

                <strong style="display:inline-block; margin-bottom: 5px;">Due Amount :</strong><br>
                Rs {{number_format($credit_pending_amount, 2)}}<br><br>

                <strong style="display:inline-block; margin-bottom: 5px;">Credit Limit Available :</strong><br>
                Rs {{ number_format($available_credit_limit, 2)}}<br><br>

                <strong style="display:inline-block; margin-bottom: 5px;">Advance Payment :</strong><br>
                Rs {{ number_format($customer->credit_balance, 2) }}<br><br>

            </div>
        @endif

        <h3>Reward Points in account : {{$reward_points}} Points</h3>

        Click on My Offers in the dashboard to see all other available offers.         
            
        <div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
            For any queries or support please WhatsApp +918484088331<br/>
        </div>
    </div>
</div>
@endsection

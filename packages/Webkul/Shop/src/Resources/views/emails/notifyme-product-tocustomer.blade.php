 @extends('shop::emails.layouts.default')

@section('email-wrapper')

<div>
	Dear {{$notify_info['first_name']}},<br/><br/>
	<div class="sub-content">
		The product <strong>{{$notify_info['product_name']}}</strong> which you have requested has stock available now.<br/>
		Enjoy your shopping!<br/><br/>
	</div>
</div>

@endsection

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
			body
	{
		background: #f5f2f2;
		padding: 0;
	}
	p{
		font-size: 16px;
		font-family: sans-serif;
	}
	li{
		font-size: 16px;
	    font-family: sans-serif;
	    line-height: 1.5;
	}
	#heading{
		width: 100%;
		height: 100px;
		background: #0995CD;
	}
	.heading_content{
		width: 50%;
		padding: 20px;
	}
	#footer{
		width: 100%;
		height: 70px;
		background: #0995CD;
	}
	.body_content{
		width: 90%;
	    /* height: 600px; */
	    background: white;
	    margin: 20px auto 20px auto;
	}
	#main-content{
		width: 95%;
		margin: 20px auto 20px auto;
	}
	.reg_content{
		width: 95%;
		margin: 20px auto 20px auto;
		line-height: 0.9;
	}
	.sub-content{
		width: 96%;
		margin: 20px auto 20px auto;
	}
	.link_vref{
		background: #10ff00;
		text-decoration: none;
		margin: 0px 0px 0px 5px;
	}
	.icon{
		float: right;
		padding: 24px;
		font-size: larger;
		color: #fff;
	}
	</style>
</head>
<body>
	<div class="body_content">
		<header id="heading">
			<div class="heading_content">
				<img src="{{ bagisto_asset('images/genxt-logo.png') }}">
			</div>
		</header>
		<section id="main-content">
		@yield('email-wrapper')
	</section>
		<div class="reg_content" style="width: 95%;">
			<div style="padding: 0 30px">				
				<p>Best Wishes</p>
				<p>GenXT WebX Support Team</p>
				<p><a href="#">sales@genxtimplants.com</a></p>
				<p>+918484088331</p>
			</div>
		</div>
		<footer id="footer">
			<div class="footer-content">
				<img src="{{ bagisto_asset('images/footer-logo.png') }}" style="padding: 15px;">
				<div class="icon">
					<a href="#" style="color: white;text-decoration: none;"><i class="fa fa-facebook"></i>&nbsp;</a>&nbsp;&nbsp;
					<a href="#" style="color: white;text-decoration: none;"><i class="fa fa-twitter"></i>&nbsp;</a>&nbsp;&nbsp;
					<a href="#" style="color: white;text-decoration: none;"><i class="fa fa-envelope"></i>&nbsp;</a>
				</div>
			</div>
		</footer>
	</div>
</body>
</html>
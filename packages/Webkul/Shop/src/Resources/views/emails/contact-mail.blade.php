 @extends('shop::emails.layouts.default')

@section('email-wrapper')

<div>
	Dear Admin,<br/><br/>
	<div class="sub-content">
		You have received a new enquiry!.<br/><br/>


		<strong>Interested In:</strong> {{$interested_in }}<br/>
		<strong>Full Name:</strong> {{$my_title }} {{ $full_name}}<br/>
		<strong>Email:</strong> {{ $email }}<br/>
		<strong>Phone Number:</strong> {{ $phone_number}}<br/>
		<strong>Country:</strong> {{ $country_name }}<br/>
		<strong>Message:</strong> {{ $contact_msg }}<br/><br/><br/>
	</div>

</div>
@endsection

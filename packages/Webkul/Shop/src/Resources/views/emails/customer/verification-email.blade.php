 @extends('shop::emails.layouts.default')

@section('email-wrapper')
<div>
   
        <div>
            Hello Dr {{$verificationData['first_name']}}, <br/>
        </div>

        <div class="sub-content">
        <div  style="font-size:16px; color:#242424; font-weight:600; margin-top: 60px; margin-bottom: 15px">
            Greetings from GenXT WebX.
        </div><br/>

        <div>
            You are 1 step away from the Next Generation Dental Implant experience.<br/>
            Your dealer {{$verificationData['created_by']}} has Pre-filled your details. Please click on the link below, and verify/edit the details. Once you click on Register, you will receive an OTP on the mobile number mentioned.<br/>
            Please enter the OTP to validate your registration.<br/>
        </div><br/>

        <div  style="margin-top: 40px; text-align: center">
            <a href="{{ route('customer.register.index') }}?id={{$verificationData['id']}}" style="font-size: 16px;color: #FFFFFF; text-align: center; background: #0031F0; padding: 10px 20px;text-decoration: none;width:100%;">Verify Your Account</a>
        </div><br/><br/>
        <div>
            GenXT WebX will keep you updated on the latest and give you a place to<br/>
            <ul>
                <li>Easily browse&amp; buy implants, accessories, instruments</li>
                <li>Earn reward points on regular purchases</li>
                <li>Request for implant support</li>
                <li>Avail of exclusive WebX special discounts</li>
                <li>You can place orders 24x7 at the convenience of your home/office</li>
                <li>Shopping online from www.genxtimplants.com gives you prompt and accurate online
support, to track your orders until they are delivered.</li>
            </ul>
        </div><br/>

        <div>
            It will take up to 24hrs for WebX to verify and approve your account.<br/>
            You will receive an email to let you know your account is active.<br/>
            For any queries or support please WhatsApp +918484088331<br/>
        </div><br/><br/><br/>

        
</div>
        
    </div>

@endsection
 @extends('shop::emails.layouts.default')

@section('email-wrapper')
<div>

    <div class="sub-content">
        <table class="table" width="100%">
                <tbody>
                        <tr>
                            <td>Email</td>
                            <td>{{ $input["email"] }}</td>
                        </tr>
                        <tr>
                            <td>Doctor's Name</td>
                            <td>{{ $input["doctor_name"] }}</td>
                        </tr>
                        <tr>
                            <td>Mobile Number</td>   
                             <td>{{ $input["mobile_number"] }}</td>   
                        </tr>
                        <tr>
                            <td>Detailed Clinic Address</td>
                            <td>{{ $input["clinic_address"] }}</td>   
                        </tr>
                        <tr>
                            <td>GenXT Implant Product Code(or label)</td>
                            <td>{{ $input["implant_code"] }}</td>
                        </tr>
                        @if($input['order_type'] == 'on')
                            <?php $item = DB::table('order_items')->where('id', intval($input['order_item']))->addSelect('name', 'order_id')->first(); ?>
                            <tr>
                                <td>Order ID</td>
                                <td>#{{  $item->order_id }}</td>
                            </tr>
                            <tr>
                                <td>Product Name</td>                                
                                <td>{{  $item->name }}</td>                                
                            </tr>
                            <tr>
                                <td>Product Quantity</td>
                                <td>{{ $input['order_qty'] }}</td>
                            </tr>

                            <tr>
                                <td>Quantity Replace</td>
                                <td>{{ $input['replace_qty'] }}</td>
                            </tr>

                            <?php
                                $item = DB::table('order_items')->where('id', intval($input['order_item']))->addSelect('name', 'order_id')->first();
                            ?>
                            <tr>
                                <?php
                                    $invoice_items_id = DB::table('invoice_items')->where('order_item_id', DB::table('order_items')->where('id', intval($input['order_item']))->first()->id)->first();
                                    $lot_items = ($invoice_items_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_items_id->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get()):[];
                                ?>
                                <td>Lot Number</td>
                                <td>
                                    @if(count($lot_items)>0)
                                        @foreach($lot_items as $key => $lot_item)
                                            {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}},
                                            {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}},
                                            {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </td>                            
                            </tr>
                        @else
                            <tr>
                                <td>Order_number</td>
                                <td>#{{ $input['order_number'] }}</td>                            
                            </tr>
                            <tr>
                                <td>Product Name</td>                                
                                <td>{{ $input['product_name'] }}</td>                                
                            </tr>
                            <tr>
                                <td>Quantity Ordered</td>
                                <td>{{ $input['order_qty_offline'] }}</td>
                            </tr>
                            <tr>
                                <td>Replace Ordered</td>
                                <td>{{ $input['replace_offline_qty'] }}</td>
                            </tr>
                        @endif

                        <?php
                            $replace_id = ($input['order_type']=='on')?$input['replacement_item_online']:$input['replacement_item_offline'];
                            $product = \DB::table('product_flat')->where('product_id', $replace_id)->first();
                        ?>

                        <tr>
                            <td>Replacement Product</td>   
                            <td>{{ $product->name ?? ''}}</td>   
                        </tr>

                        <tr>
                            <td>Replacement Reason</td>   
                            <td>{{ $input["replacement_reason"] }}</td>   
                        </tr>

                        <tr>
                             <td>LOT #</td>   
                             <td>{{ $input["lot"] }}</td>   
                        </tr>

                        @if($input['item_status']==0 && $input['formtype']==1)
                            <tr>
                                <td>Bone Type</td>   
                                <td>{{ $input["bone_type"] }}</td>   
                            </tr>
                            
                            <tr>
                                <td>Flapless</td>   
                                <td>{{ $input["flapless"] }}</td>   
                            </tr>
                            <tr>
                                <td>Immediate Implant</td>   
                                <td>{{ $input["immediate_implant"] }}</td>   
                            </tr>
                            <tr>
                                <td>Immediate Load</td>   
                                <td>{{ $input["immediate_load"] }}</td>   
                            </tr>
                            <tr>
                                <td>Implantation Date</td>   
                                <td>{{ $input["implant_date"] }}</td>   
                            </tr>
                            <tr>
                                <td>Implant Removal Date</td>   
                                <td>{{ $input["implant_removal_date"] }}</td>   
                            </tr>
                            <tr>
                                <td>Implant Removal Location</td>   
                                <td>{{ $input["remove_location"] }}</td>   
                            </tr>
                            <tr>
                                <td>Reasons for Failure</td>   
                                <td>{{ $input["reason"] }}</td>   
                            </tr>
                            <tr>
                                <td>Other reason for Failure / Case Outcome</td>   
                                <td>{{ $input["other_reason_failure"] }}</td>   
                            </tr>
                            <tr>
                                <td>Age</td>   
                                <td>{{ $input["age"] }}</td>   
                            </tr>
                            <tr>
                                <td>Gender</td>   
                                <td>{{ $input["gender"] }}</td>   
                            </tr>
                            <tr>
                                <td>Others (Specify)</td>
                                <td>{{ $input["others"] }}</td>
                            </tr>
                        @endif
                        <tr>
                            <td>Return type:</td>
                            <td>{{ ($input['item_status']==0)?'Used Implant':"Unopened Implant" }}</td>
                        </tr>
                </tbody>
        </table>
        @if($input['item_status']==0  && $input['formtype']==1)
            <hr>
            <table class="table history" width="100%">
                    <thead>
                            <tr>
                                <th colspan="2">History</th>
                            </tr>
                    </thead>
                    <tbody>
                            <tr>
                                <td>Normal</td>
                                <td>{{ $input["normal_history"] }}</td>
                            </tr>
                            <tr>
                                    <td>Smoker</td>
                                    <td>{{ $input["smoker_history"] }}</td>
                            </tr>
                            <tr>
                                    <td>Hypertension</td>
                                    <td>{{ $input["hypertension_history"] }}</td>
                            </tr>
                            <tr>
                                    <td>Cardiac Problems</td>
                                    <td>{{ $input["cardiac_problems_history"] }}</td>
                            </tr>
                            <tr>
                                    <td>Diabetes</td>
                                    <td>{{ $input["diabetes_history"] }}</td>
                            </tr>
                            <tr>
                                    <td>Alcoholism</td>
                                    <td>{{ $input["alcoholism_history"] }}</td>
                            </tr>
                            <tr>
                                    <td>Trauma</td>
                                    <td>{{ $input["trauma_history"] }}</td>
                            </tr>
                            <tr>
                                    <td>Cancer</td>
                                    <td>{{ $input["cancer_history"] }}</td>
                            </tr>
                    </tbody>
            </table>
        @endif
        
        <div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
            For any queries or support please WhatsApp +918484088331<br/><br/>
        </div>
    </div>
</div>
@endsection
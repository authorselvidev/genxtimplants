@extends('shop::emails.layouts.default')

@section('email-wrapper')
<div style="padding: 30px;">
    <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
        <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
            Hi, {{$user->my_title}} {{$user->first_name}} {{$user->last_name}},
        </p><br/>
        <div class="sub-content" style="margin:5px 0;">Your return implant status has been {{$failed_implant->status}}.</div>
    </div>

    <div class="sub-content">
        <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px;margin-bottom: 10px">
                <table  style="margin-bottom: 10px;">
                    <tr>
                        <td colspan="2">
                            <span style="font-size: 18px;color: #242424;line-height: 24px;margin-top: 0;margin-bottom: 10px;font-weight: bold;font-family: sans-serif;">
                                @if($failed_implant->order_type == 'on')
                                    <?php
                                        $item = DB::table('order_items')->where('id', intval($failed_implant->order_item_id))->addSelect('name', 'order_id')->first();
                                    ?>
                                    {{ $item->name }}
                                @else
                                    {{ $failed_implant->order_item }}
                                @endif      
                            </span>
                        </td>
                    </tr>
                    @if($failed_implant->order_type == 'on')
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Order ID:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">#{{ $order->id }}</span></td>
                        </tr>
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Product Quantity:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">{{ $failed_implant->qty }}</span></td>
                        </tr>                        
                        <?php $product = \DB::table('product_flat')->where('product_id', $failed_implant->replacement_product_id )->first()->name; ?>
                        
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Replacement Product:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">{{ $product }}</span></td>
                        </tr>
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Replacement Quantity:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">{{ $failed_implant->replace_qty ?? 0 }}</span></td>                            
                        </tr>
                    @else
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Order Number:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">#{{ $failed_implant->order_number }}</span></td>
                        </tr>
                        
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Product Name:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">{{ $failed_implant->order_item }}</span></td>
                        </tr>
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Product Quantity:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">{{ $failed_implant->qty  }}</span></td>
                        </tr> 
                        <?php $product = \DB::table('product_flat')->where('product_id', $failed_implant->replacement_product_id )->first()->name; ?>
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Replacement Product:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">{{ $product }}</span></td>
                        </tr>                       
                        <tr>
                            <td><label style="font-size: 14px;color: #5E5E5E;">Replacement Quantity:</label></td>
                            <td><span style="font-size: 14px;color: #242424;">{{ $failed_implant->replace_offline_qty ?? 0 }}</span></td>                            
                        </tr>
                    @endif
                    <tr>
                        <td><label style="font-size: 14px;color: #5E5E5E;">Comments:</label></td>
                        <td><span style="font-size: 14px;color: #242424;">{{$failed_implant->comments }}</span></td>
                    </tr>
                </table>
           
        </div>
        <div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
            For any queries or support please WhatsApp +918484088331<br/>
        </div>
    </div>
</div>
@endsection
 @extends('shop::emails.layouts.default')

@section('email-wrapper')
<div style="padding: 15px 3px;">
    <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
        <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
            Hi, {{$user->my_title}} {{$user->first_name}} {{$user->last_name}},
        </p><br/>
        <div class="sub-content" style="margin:5px 0;">Your return implant status has been {{$failed_implant->status}}.</div>
    </div>

    <div class="sub-content">
        <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px 3px;margin-bottom: 10px">
            <table style="font-family:arial" width="100%">
                <tbody>
                        <tr>
                            <td>Doctor's Name</td>
                            <td>{{$user->my_title.' '.$user->first_name.' '.$user->last_name}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <td>Mobile Number</td>   
                             <td>{{$user->phone_number}}</td>   
                        </tr>
                        <tr>
                            <td>Membership group</td>
                            <td>{{ Webkul\Customer\Models\CustomerGroup::CustomerGroupName($user->customer_group_id) }}</td>
                        </tr>    
                        
                        @if(!empty($user->dealer_id))
                            <tr>
                                <?php $dealer = DB::table('users')->where('id',$user->dealer_id)->first(); ?>
                                <td>Dealer Name</td>   
                                <td>{{ $dealer->my_title.' '.$dealer->first_name.' '.$dealer->last_name }}</td>
                            </tr>
                        @endif

                        <tr>
                            <td>Detailed Clinic Address</td>
                            <td>{{ $user->clinic_address }}</td>   
                        </tr>

                        <tr>
                            <td>GenXT Implant Product Code(or label)</td>
                            <td>{{ $failed_implant->implant_code }}</td>
                        </tr>
                        <tr>
                            <td>Order Mode</td>                            
                            <td>{{($failed_implant->order_type =='on')?'Online':'Offline'}}</td>
                        </tr>
                        <tr>
                            <td>Item Status</td>
                            <td>{{($failed_implant->item_status==0)?'Used':'Unused'}}</td>
                        </tr>                                
                        <tr>
                            <td>Replacement Reason</td>   
                            <td>{{ $failed_implant->replacement_reason }}</td>   
                        </tr>
                        <tr>
                             <td>LOT #</td>   
                             <td>{{ $failed_implant->lot }}</td>   
                        </tr>                                        
                    </tbody>
            </table>
            <hr>
            <h4 style="font-weight: bold;font-family:arial">Return Items</h4>
            @if($failed_implant->order_type =='on')
                <table style="font-family:arial" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" style="text-align: center">Order ID</th>
                            <th scope="col" style="text-align: center">Return Product</th>
                            <th scope="col" style="text-align: center">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($return_implants_return as $impnt)
                            <tr>
                                <td scope="row" style="text-align: center">#{{$impnt->order}}</td>
                                
                                <td style="text-align: center">{{ DB::table('order_items')->where('id', $impnt->item)->value('name') }}</td>
                                <td style="text-align: center">{{$impnt->quantity}}</td>
                            </tr>   
                        @endforeach                                                                     
                    </tbody>                                    
                </table>
            @else
                <table  style="font-family:arial" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" style="text-align: center">Order number</th>
                            <th scope="col" style="text-align: center">Return Product</th>
                            <th scope="col" style="text-align: center">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($return_implants_return as $impnt)
                            <tr>
                                <td scope="row" style="text-align: center">#{{$impnt->order}}</td>                                                
                                <td style="text-align: center">{{ $impnt->item }}</td>
                                <td style="text-align: center">{{$impnt->quantity}}</td>
                            </tr>   
                        @endforeach                                                                     
                    </tbody>                                    
                </table>
            @endif
            <hr>
            <h4 style="font-weight: bold;font-family:arial">Replacement Items</h4>
            
            <table style="font-family:arial" width="100%">
                <thead>
                    <tr>
                        <th scope="col" style="text-align: center">Replacement Item</th>
                        <th scope="col" style="text-align: center">Replace Quantity</th>
                    </tr>
                </thead>
                <tbody>                                    
                    @foreach ($return_implants_replace as $impnt)
                        <tr>
                            <td scope="row" style="text-align: center">{{  \DB::table('product_flat')->where('product_id',$impnt->item)->value('name')}}</td>
                            <td style="text-align: center">{{$impnt->quantity}}</td>
                        </tr>   
                    @endforeach                                                                     
                </tbody>                                    
            </table>    
           
        </div>
        <div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
            For any queries or support please WhatsApp +918484088331<br/>
        </div>
    </div>
</div>
@endsection
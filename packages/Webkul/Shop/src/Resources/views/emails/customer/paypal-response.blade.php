 @extends('shop::emails.layouts.default')

@section('email-wrapper')
    <div class="sub-content">
       Payment Status: {{ $payment_status }}
    </div>
@endsection
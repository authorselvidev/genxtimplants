 @extends('shop::emails.layouts.default')

@section('email-wrapper')
<div style="padding: 15px 3px;">
    <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
        <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
            Hi, {{$user->my_title}} {{$user->first_name}} {{$user->last_name}},
        </p><br/>
        <div class="sub-content" style="margin:5px 0;">Your return implant status has been submited.</div>
    </div>

    <div class="sub-content">
        <div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px 3px;margin-bottom: 10px">
            <table class="table" width="100%">
                <tbody>
                        <tr>
                            <td>Doctor's Name</td>
                            <td>{{ $input["doctor_name"] }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $input["email"] }}</td>
                        </tr>
                       
                        <tr>
                            <td>Mobile Number</td>   
                             <td>{{ $input["mobile_number"] }}</td>   
                        </tr>
                        <tr>
                            <td>Membership group</td>
                            <td>{{ Webkul\Customer\Models\CustomerGroup::CustomerGroupName($user->customer_group_id) }}</td>
                        </tr>

                        @if(!empty($user->dealer_id))
                            <tr>
                                <?php $dealer = DB::table('users')->where('id',$user->dealer_id)->first(); ?>
                                <td>Dealer Name</td>   
                                <td>{{ $dealer->my_title.' '.$dealer->first_name.' '.$dealer->last_name }}</td>
                            </tr>
                        @endif

                        <tr>
                            <td>Detailed Clinic Address</td>
                            <td>{{ $input["clinic_address"] }}</td>   
                        </tr>
                        <tr>
                            <td>GenXT Implant Product Code(or label)</td>
                            <td>{{ $input["implant_code"] }}</td>
                        </tr>
                        <tr>
                            <td>Order Mode</td>                            
                            <td>{{($input["order_type"] =='on')?'Online':'Offline'}}</td>
                        </tr>
                        <tr>
                            <td>Item Status</td>
                            <td>{{($input["item_status"]==0)?'Used':'Unused'}}</td>
                        </tr>                                
                        

                        <tr>
                            <td>Replacement Reason</td>   
                            <td>{{ $input["replacement_reason"] }}</td>   
                        </tr>
                        <tr>
                             <td>LOT #</td>   
                             <td>{{ $input["lot"] }}</td>   
                        </tr>                                        
                    </tbody>
            </table>

            <hr>
            <h4 style="font-weight: bold">Return Items</h4>
            @if($input['order_type'] == 'on')
                <table style="width:100%; margin-top:10px;">
                    <thead>
                        <tr>
                            <th scope="col">Order ID</th>
                            <th scope="col">Return Product</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Lot Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($input["return_item"] as $impnt)
                            <tr>
                                <td style="text-align: center">#{{$impnt["order"]}}</td>                                            
                                <?php
                                    $order_item = DB::table('order_items')->where('id', $impnt["item"])->first();
                                    $invoice_items_id = DB::table('invoice_items')->where('order_item_id', $order_item->id)->first();
                                    $lot_items = ($invoice_items_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_items_id->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get()):[];
                                ?>
                                <td style="text-align: center">{{ $order_item->name }}</td>
                                <td style="text-align: center">{{ $impnt["qty"] }}</td>
                                <td style="text-align: center">
                                    @if(count($lot_items)>0)
                                        @foreach($lot_items as $key => $lot_item)
                                            {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}},
                                            {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}},
                                            {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>   
                        @endforeach                                                                     
                    </tbody>                                    
                </table>
                <hr>
                <h4 style="font-weight: bold">Replacement Items</h4>                
                <table style="width:100%; margin-top:10px;">
                    <thead>
                        <tr>
                            <th scope="col">Replacement Item</th>
                            <th scope="col">Replace Quantity</th>
                        </tr>
                    </thead>
                    <tbody>                                    
                        @foreach ($input['replace_item'] as $impnt)
                            <tr>
                                <td style="text-align: center">{{  \DB::table('product_flat')->where('product_id',$impnt["item"])->value('name') }}</td>
                                <td style="text-align: center">{{$impnt["qty"]}}</td>
                            </tr>   
                        @endforeach                                                                     
                    </tbody>                                    
                </table>
            @else
                <table style="width:100%; margin-top:10px;">
                    <thead>
                        <tr>
                            <th scope="col">Order number</th>
                            <th scope="col">Return Product</th>
                            <th scope="col">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($input['replace_item_offline'] as $impnt)
                            <tr>
                                <td style="text-align: center">#{{$impnt["order"]}}</td>                                                
                                <td style="text-align: center">{{ $impnt["item"] }}</td>
                                <td style="text-align: center">{{$impnt["qty"]}}</td>
                            </tr>   
                        @endforeach                                                                     
                    </tbody>                                    
                </table>        
                <hr>
                <h4 style="font-weight: bold">Replacement Items</h4>                
                <table style="width:100%; margin-top:10px;">
                    <thead>
                        <tr>
                            <th scope="col">Replacement Item</th>
                            <th scope="col">Replace Quantity</th>
                        </tr>
                    </thead>
                    <tbody>                                    
                        @foreach ($input['return_item_offline'] as $impnt)
                            <tr>
                                <td style="text-align: center">{{  \DB::table('product_flat')->where('product_id',$impnt["item"])->value('name') }}</td>
                                <td style="text-align: center">{{$impnt["qty"]}}</td>
                            </tr>   
                        @endforeach                                                                     
                    </tbody>                                    
                </table>      
            @endif   
            </div>
        <div style="margin-top: 20px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block;width: 100%">
            <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
            For any queries or support please WhatsApp +918484088331<br/>
        </div>
    </div>
</div>
@endsection
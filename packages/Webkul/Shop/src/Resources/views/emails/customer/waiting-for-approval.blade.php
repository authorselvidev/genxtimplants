 @extends('shop::emails.layouts.default')

@section('email-wrapper')
<div>
   
    <div>
        Hello Dr {{$first_name}}, <br/>
    </div>

    <div class="sub-content">
    <div>
        Congratulations and Welcome to GenXT WebX.
    </div><br/>
    <div>
        You have successfully submitted your details to GenXT WebX.
    </div><br/>
    <div>
        Thank you for choosing GenXT Implants - Next Generation Implantology. You have made the right choice to partner with GenXT
    </div><br/>

    <div>
        It will take up to 24hrs for WebX to verify and approve your account. You will receive an email to let you know your account is active.
    </div><br/>

    <div>
        GenXT provides a truly wide variety of dental Implants and GenXT WebX helps you make the most of this:-<br/>
        <ul>
            <li>Easily browse &amp; buy implants, accessories, instruments</li>
            <li>Earn reward points on regular purchases</li>
            <li>Request for implant support</li>
            <li>Avail of exclusive WebX special discounts</li>
            <li>You can place orders 24x7 at the convenience of your home/office</li>
            <li>Shopping online from www.genxtimplants.com gives you prompt and accurate online support, to track your orders until they are delivered.</li>
        </ul>
    </div><br/>
    <div>For any queries or support please WhatsApp +918484088331</div><br/>
    <br/><br/><br/>

  </div>
</div>
@endsection
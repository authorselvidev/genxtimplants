 @extends('shop::emails.layouts.default')

@section('email-wrapper')
<div>
	Dear Admin,<br/><br/>
	    <div class="sub-content">
			<strong>Fullname:</strong> <?php echo $data['my_title'].' '.$data['first_name'].' '.$data['last_name']; ?> <br/>
			<strong>Email:</strong> <?php echo $data['email']; ?> <br/>
			<strong>Phone Number:</strong> <?php echo $data['phone_number']; ?> <br/>
			<strong>Country:</strong> <?php echo $data['country_name']; ?> <br/>
			<strong>State:</strong> <?php echo $data['state_name']; ?> <br/>
			<strong>City:</strong> <?php echo $data['city_name']; ?> <br/><br/>
			<strong>Pincode:</strong> <?php echo $data['pin_code']; ?> <br/><br/>
			<strong>Clinic Name:</strong> <?php echo $data['clinic_name']; ?> <br/>
			<strong>Clinic Address:</strong> <?php echo $data['clinic_address']; ?> <br/>
			<strong>Clinic Number:</strong> <?php echo $data['clinic_number']; ?> <br/><br/>
			<strong>Dental License No:</strong> <?php echo $data['dental_license_no']; ?> <br/><br/>


			Please go to Admin Dashboard to see further details and approve them.<br/><br/>
		</div>
	
</div>
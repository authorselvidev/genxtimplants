 @extends('shop::emails.layouts.default')

@section('email-wrapper')  
<div>
	Hello Dr {{ $full_name }},<br/><br/>
	<div class="sub-content">
	<div>Congratulations and Welcome to GenXT WebX.</div><br/>
	<div>You GenXT WebX account is active, Please Click <a href="{{ $set_password_url }}">Set a password</a> to create a password. Once your password is created you can log in and enjoy the benefits of GenXT</div><br/>
	<strong>Your Login Id is your email:</strong> <?php echo $email; ?> <br/>
		
	<div>
		You can go to Genxtimplant.com or click <a href="https://genxtimplants.com">here</a> to login now and start enjoying the following benefits:-<br/>
		<ul>
			<li>Easily browse &amp; buy implants, accessories, instruments</li>
			<li>Earn reward points on regular purchases</li>
			<li>Request for implant support</li>
			<li>Avail of exclusive WebX special discounts</li>
			<li>You can place orders 24x7 at the convenience of your home/office</li>
			<li>Shopping online from www.genxtimplants.com gives you prompt and accurate online support, to track your orders until they are delivered.</li>
		</ul>
	</div><br/><br/>
	<div>
		Quick Usage instructions:-<br/>
		<ul>
			<li>Browse products on the products page <a href="https://genxtimplants.com/product-list">Products</a> by their categories</li>
			<li>Click on the shopping cart icon on the top right corner and click check out</li>
			<li>Select the discount if any applicable.</li>
			<li>Verify shipping details, Select Razor pay, and click proceed to pay.</li>
		</ul>
	</div><br/><br/>
	<div>
		Detailed instructions:-<br/>
		<ul>
			<li>Browse products on the products page <a href="https://genxtimplants.com/product-list">Products</a> by their categories</li>
			<li>Add them to your cart by clicking on the plus sign or entering the desired quantity</li>
			<li>Any product that you may be interested in but is not in stock at the moment will be shown as notify me Please press this button it will let us know that you are interested in the product and allow us to increase our stock of the product, and inform you when it&#39;s vailable.</li>
			<li>Once done adding all products click on the shopping cart icon on the top right corner of thescreen. View/ edit the shopping cart, and then click on check out.</li>
			<li>Select the form of discount if any applicable. Note only 1 type of discount will be applicable at a time. You can click on the promo code or promotion buttons to see any active discounts, please choose any 1 which discount is best for you and then select apply.</li>
			<li>Verify the Billing and Shipping address and contact details that will go on the shipment</li>
			<li>Select Razor pay as the payment method. It supports all types of online payments.</li>
			<li>Check the final billed amount</li>
			<li>Press Proceed to pay and follow the instructions.</li>
			<li>You will receive regular updates in your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</li>
		</ul>
	</div><br/><br/>

	<div>
		For any queries or support please WhatsApp +918484088331
	</div><br/><br/><br/>

	<div>
        Best Wishes<br/>
        GenXT WebX Support Team<br/>
        sales@genxtimplants.com<br/>
        +918484088331<br/>
    </div>

</div>
</div>
@endsection
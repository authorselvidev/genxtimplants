@extends('shop::emails.layouts.default')

@section('email-wrapper')
<div style="padding: 30px;">
    <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
        <p style="font-size: 16px;line-height: 24px;">
            Hi, {{$user->my_title}} {{$user->first_name}} {{$user->last_name}},
        </p><br/>
        <div class="sub-content" style="margin:5px 0; font-family:Arial, Helvetica, sans-serif;">Dental implants items added to your cart - GenXT</div>
    </div>

    <div style="background: #FFFFFF;padding: 0px;margin-bottom: 10px; line-height: 24px; font-family:Arial, Helvetica, sans-serif;font-size: 16px; color:#000;">
        The site owner/dealer added one or more items to your cart. Please login to Genxt portal with your access details and complete your order.
        <a href="{{ route('shop.home.index') }}/{{ $user->role_id==3?'customer':'admin' }}/login?redirect=cart">View Cart</a>
    </div>

    <div style="margin-top: 20px;font-size: 16px;line-height: 24px;display: inline-block;width: 100%; font-family:Arial, Helvetica, sans-serif;">
        <p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p>
        <br/>
            For any queries or support please WhatsApp +918484088331<br/>
    </div>    
</div>
@endsection

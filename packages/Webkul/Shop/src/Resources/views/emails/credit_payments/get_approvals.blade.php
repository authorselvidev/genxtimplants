 @extends('shop::emails.layouts.default')

@section('email-wrapper')

<div>
	Dear {{$admin->my_title}} {{$admin->first_name}} {{$admin->last_name}},<br/><br/>
	<div class="sub-content">
		The  customer {{$user->my_title}} {{$user->first_name}} {{$user->last_name}} has been requested for {{ (($credit_payment->payment_type==2)?'Cheque':(($credit_payment->payment_type==3)?'Bank Transfer':'')) }} payment<br/>
		Please login as admin and approve their payment.<br/><br/>
	</div>
</div>

@endsection

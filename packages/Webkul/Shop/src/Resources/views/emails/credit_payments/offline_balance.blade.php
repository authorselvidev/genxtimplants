@extends('shop::emails.layouts.default')

@section('email-wrapper')

<div>
	Dear {{$user->my_title}} {{$user->first_name}} {{$user->last_name}},<br/><br/>
	<div class="sub-content">
		<p>Due for the Offline order #{{$offline_balance["order_number"]}} has been debited from your credit/advance payment</p>			
		
		<div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px;margin-bottom: 10px">
			<table style="font-family:arial;">
				<tr stl>
					<td style="font-size: 16px;color: #5E5E5E; padding:10px">Order ID</td>
					<td>:</td>
					<td style="font-size: 16px;color: #242424;margin-left:8px">{{ $offline_balance["order_number"]}}</td>
				</tr>
				<tr>
					<td style="font-size: 16px;color: #5E5E5E; padding:10px">Date</td>
					<td>:</td>
					<td style="font-size: 16px;color: #242424;margin-left:8px">{{$offline_balance["due_date"]}}</td>
				</tr>
				<tr>
					<td style="font-size: 16px;color: #5E5E5E; padding:10px">Due Amount</td>
					<td>:</td>
					<td style="font-size: 16px;color: #242424;margin-left:8px">- {{ core()->formatBasePrice($offline_balance["credit_due_price"]) }} </td>
				</tr>
				<tr>
					<td style="font-size: 16px;color: #5E5E5E; padding:10px">Comments/Products</td>
					<td>:</td>
					<td style="font-size: 16px;color: #242424;margin-left:8px">{{ $offline_balance["credit_due_comment"]}}</td>
				</tr>
			</table>	
        </div>

		<div style="background: #FFFFFF;border: 1px solid #E8E8E8;border-radius: 3px;padding: 20px;margin-bottom: 10px">
			<h3 style="font-family:arial;">Current Credit/Advance Balance in account</h3>

			<?php 
				$credit_pending_amount=$available_credit_limit=0;
				$credit_pending_amount = $user->credit_used - $user->credit_paid;
				$available_credit_limit = $user->credit_limit - $credit_pending_amount;
			?>

			<table style="font-family:arial;">
				<tr>
					<td style="font-size: 16px;color: #5E5E5E; padding:10px">Balance Due</td>
					<td>:</td>
					<td style="font-size: 16px;color: #242424;margin-left:8px">- {{  core()->formatBasePrice($credit_pending_amount) }}</td>
				</tr>
				<tr>
					<td style="font-size: 16px;color: #5E5E5E; padding:10px">Advance Balance</td>
					<td>:</td>
					<td style="font-size: 16px;color: #242424;margin-left:8px">{{ core()->formatBasePrice($user->credit_balance) }}</td>
				</tr>
				<tr>
					<td style="font-size: 16px;color: #5E5E5E; padding:10px">Credit Limit Available</td>
					<td>:</td>
					<td style="font-size: 16px;color: #242424;margin-left:8px">{{ core()->formatBasePrice($available_credit_limit) }}</td>
				</tr>
			</table>
        </div>	
		<div style="margin-top: 20px;font-size: 16px;color: #4E4E4E;line-height: 24px;display: inline-block;width: 100%">
			<p>You will receive regular updates about your order via email or SMS, or you can check your profile page by clicking on the small icon next to your name in the top right corner and selecting Profile.</p><br/>
			For any queries or support please WhatsApp +918484088331<br/>
		</div>
	</div>
</div>

@endsection

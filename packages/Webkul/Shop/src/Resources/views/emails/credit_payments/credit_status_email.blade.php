 @extends('shop::emails.layouts.default')

@section('email-wrapper')

<div>
	Dear {{$credit_user->my_title}} {{$credit_user->first_name}} {{$credit_user->last_name}},<br/><br/>
	<div class="sub-content">
		@if($credit_detail->status == 1)
			<p>Your payment {{ (($credit_detail->payment_type==2)?'with Cheque':(($credit_detail->payment_type==3)?'with Bank Transfer':'')) }} status has been approved by the GenXT.</p>
		@endif
		
		@if($credit_detail->status == 2)
			<p>Sorry, your payment with {{ ($credit_detail->payment_type==2)?'with Cheque':(($credit_detail->payment_type==3)?'with Bank Transfer':'') }} status has been rejected by the GenXT.</p>
			@if($credit_detail->rejected_comment)
				<p>Reason for reject: {{ $credit_detail->rejected_comment }}</p>
			@endif
			<p>Can you please try again or contact GenXT.</p>
		@endif

	</div>
</div>

@endsection

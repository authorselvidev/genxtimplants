 @extends('shop::emails.layouts.default')

@section('email-wrapper')
<div>
	Dear {{ $full_name }},<br/><br/>
	<div class="sub-content">
		<?php if($type == 'create'){ ?>
			Your account has been successfully created on GenXT.<br/>
		<?php } elseif($type == 'update'){?>
			Genxt admin has changed your account password.<br/>
	<?php } ?>
		You can access your account by the following credentials<br/>
		<strong>Access URL: </strong><a href="{{url('/admin/login')}}" target="_blank">{{url('/admin/login')}}</a><br/>
		<strong>Email:</strong> <?php echo $email; ?> <br/>
		<strong>Password:</strong> <?php echo $password; ?> <br/><br/>
</div>
</div>
@endsection

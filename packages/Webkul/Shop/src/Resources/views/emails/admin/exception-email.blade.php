<table>
	<tr>
		<td>URL:</td>
		<td>:</td>
		<td>{{$request_url}}</td>
	</tr>

	<tr>
		<td>Request</td>
		<td>:</td>
		<td>{{$request_data}}</td>
	</tr>

	<tr>
		<td>Method</td>
		<td>:</td>
		<td>{{$request_method}}</td>
	</tr>

	@if(!empty($request_user))
		<tr>
			<td>User</td>
			<td>:</td>
			<td>{{$request_user->id}} - {{$request_user->email}} ({{$role}})</td>
		</tr>
	@endif

	<tr>
		<td>IP</td>
		<td>:</td>
		<td>{{$request_ip}}</td>
	</tr>

	<tr>
		<td>Session</td>
		<td>:</td>
		<td>{{$request_session}}</td>
	</tr>

	<tr>
		<td>User-Agent</td>
		<td>:</td>
		<td>{{$request_user_agent}}</td>
	</tr>

	

</table>

{!! html_entity_decode($exception) !!}
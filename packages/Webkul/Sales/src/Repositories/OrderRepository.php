<?php

namespace Webkul\Sales\Repositories;

use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Webkul\Core\Eloquent\Repository;
use Webkul\Sales\Repositories\OrderItemRepository;

/**
 * Order Reposotory
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */

class OrderRepository extends Repository
{
    /**
     * OrderItemRepository object
     *
     * @var Object
     */
    protected $orderItem;

    /**
     * Create a new repository instance.
     *
     * @param  Webkul\Sales\Repositories\OrderItemRepository $orderItem
     * @return void
     */
    public function __construct(
        OrderItemRepository $orderItem,
        App $app
    )
    {
        $this->orderItem = $orderItem;

        parent::__construct($app);
    }

    /**
     * Specify Model class name
     *
     * @return Mixed
     */

    function model()
    {
        return 'Webkul\Sales\Contracts\Order';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        DB::beginTransaction();
        //dd($data['payment']);
        //dd($data['items']);
        try {
            Event::fire('checkout.order.save.before', $data);
            $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
            if(empty($user))
                $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

            if (isset($data['customer']) && $data['customer']) {
                $data['customer_id'] = $data['customer']->id;
                $data['customer_type'] = get_class($data['customer']);
            } else {
                unset($data['customer']);
            }

            if (isset($data['channel']) && $data['channel']) {
                $data['channel_id'] = $data['channel']->id;
                $data['channel_type'] = get_class($data['channel']);
                $data['channel_name'] = $data['channel']->name;
            } else {
                unset($data['channel']);
            }

            $data['status'] = 'confirmed';

            if($data['payment'] == 'prepayment')
                $data['status'] = 'prepayment_order';

            $earn_points = $data['earn_points'];
            unset($data['earn_points']);
            //save order
            $order = $this->model->create(array_merge($data, ['increment_id' => $this->generateIncrementId()]));
            
            //$order->payment()->create(array('method' => $data['payment']));
            DB::table('order_payment')->insert([
                                    'order_id' => $order->id,
                                    'method' => $data['payment'],
                                    'created_at' => date('Y-m-d H:i:s')
                                ]);
            $user_address = DB::table('orders')->where('id',$order->id)->first();
            //save order items
            foreach ($data['items'] as $item) {
                $orderItem = $this->orderItem->create(array_merge($item, ['order_id' => $order->id]));

                if (isset($item['child']) && $item['child']) {
                    $orderItem->child = $this->orderItem->create(array_merge($item['child'], ['order_id' => $order->id, 'parent_id' => $orderItem->id]));
                }

                $this->orderItem->manageInventory($orderItem);
            }

           

            //save shipping and billing address
            DB::table('orders')->where('id',$order->id)->update(['billing_address' =>$data['billing_address'], 'shipping_address' =>$data['shipping_address']]);
            
            //paylaterwithcredit update credit_used amount
            if($data['payment'] == 'paylaterwithcredit') {
                
                /*$already_used = $user->credit_used;
                $credit_used_total = $already_used + $order->grand_total;
                DB::table('users')->where('id',$user->id)->update(['credit_used' => $credit_used_total]);*/

                $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
                if(empty($user))
                    $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

                $user_db = array();
                $order_amount = $order->grand_total;
                $user_balance = $user->credit_balance;
                $user_credit  = $user->credit_limit - ($user->credit_used - $user->credit_paid);

                if($user_balance > $order_amount){
                    $user_db = ['credit_balance' => ($user_balance-$order_amount)];
                }else if($user_balance <= 0){
                    $user_db = ['credit_used' => $user->credit_used+$order_amount];
                }else{
                    $user_db = ['credit_used' => $user->credit_used+($order_amount-$user_balance), 'credit_balance' => 0];
                }

                DB::table('users')->where('id', $user->id)->update($user_db);
            }

            DB::table('payments')->where('payment_id',$order->payment_id)->update(['order_id' => $order->id]);
            $user_address = DB::table('orders')->where('id',$order->id)->first();
            //dd($billingAddress->billing_address);
            $order['billingAdress'] = $user_address->billing_address;
            
            $order['shippingAdress'] = $user_address->shipping_address;
            $order['customer_full_name'] =\Webkul\Customer\Models\Customer::CustomerName($data["customer_id"]);
            $order['payment_method'] = $data['payment'];
            $order['items'] =  $data['items'];
            //dd($order['billingAdress']);
            //save reward points

            if($earn_points == 1 && $data['customer']->role_id != 2)
            {
                $discounted_grand_total = $order->sub_total - $order->total_discount_applied;
                $rewardPoints =  $discounted_grand_total / 10;
                DB::table('reward_points')->insert(['user_id' =>$user->id,
                                                 'reward_points' =>$rewardPoints,
                                                 'order_id' => $order->id
                                             ]);
            }

            $message = 'Hello Dr '.$data['customer']->first_name.', your order is confirmed and will be shipped shortly. Check the status here: https://genxtimplants.com/customer/account/orders';
            $sms_info=$whatsapp_info=array();
            $sms_info['sms_numbers'] = $data['customer']->phone_number;
            $sms_info['sms_message'] = $message;     
            $sent_sms=view('shop::send-sms',$sms_info)->render();

            /*$whatsapp_info['sms_numbers'] = '91'.$sms_info['sms_numbers'];
            $whatsapp_info['first_name'] = $data['customer']->first_name;
            $sent_whatsapp=view('shop::whatsapp-sms',$whatsapp_info)->render();
            dd($sent_whatsapp);*/
            //Event::fire('checkout.order.save.after', $order);
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        return $order;
    }

    /**
     * @param int $orderId
     * @return mixed
     */
    public function cancel($orderId)
    {
        $order = $this->findOrFail($orderId);

        if (! $order->canCancel())
            return false;

        Event::fire('sales.order.cancel.before', $order);

        foreach ($order->items as $item) {
            if ($item->qty_to_cancel) {
                $this->orderItem->returnQtyToProductInventory($item);

                $item->qty_canceled += $item->qty_to_cancel;

                $item->save();
            }
        }

        $this->updateOrderStatus($order);

        Event::fire('sales.order.cancel.after', $order);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function generateIncrementId()
    {
        $lastOrder = $this->model->orderBy('id', 'desc')->limit(1)->first(); 
        $lastId = $lastOrder ? $lastOrder->id : 0;
        return $lastId + 1;
    }

    /**
     * @param mixed $order
     * @return void
     */

    public function isInShippedState($order)
    {
        $totalQtyOrdered = 0;
        $totalQtyInvoiced = 0;
        $totalQtyShipped = 0;
        $totalQtyDelivered = 0;


        foreach ($order->items  as $item) {
            $totalQtyOrdered += $item->qty_ordered;
            $totalQtyInvoiced += $item->qty_invoiced;
            $totalQtyShipped += $item->qty_shipped;
            $totalQtyDelivered += $item->qty_delivered;
        }

        if ($totalQtyOrdered == $totalQtyInvoiced && $totalQtyOrdered == $totalQtyShipped && $totalQtyOrdered != $totalQtyDelivered)
            return true;

        return false;
    }


    public function isInCompletedState($order)
    {
        $totalQtyOrdered = 0;
        $totalQtyInvoiced = 0;
        $totalQtyShipped = 0;
        $totalQtyDelivered = 0;
        $totalQtyRefunded = 0;
        $totalQtyCanceled = 0;

        foreach ($order->items  as $item) {
            $totalQtyOrdered += $item->qty_ordered;
            $totalQtyInvoiced += $item->qty_invoiced;
            $totalQtyShipped += $item->qty_shipped;
            $totalQtyDelivered += $item->qty_delivered;
            $totalQtyRefunded += $item->qty_refunded;
            $totalQtyCanceled += $item->qty_canceled;
        }

        if ($totalQtyOrdered != ($totalQtyRefunded + $totalQtyCanceled) && 
            $totalQtyOrdered == $totalQtyInvoiced + $totalQtyRefunded + $totalQtyCanceled &&
            $totalQtyOrdered == $totalQtyShipped + $totalQtyRefunded + $totalQtyCanceled &&
            $totalQtyOrdered == $totalQtyDelivered + $totalQtyRefunded + $totalQtyCanceled)
            return true;

        return false;
    }

    /**
     * @param mixed $order
     * @return void
     */
    public function isInCanceledState($order)
    {
        $totalQtyOrdered = 0;
        $totalQtyCanceled = 0;

        foreach ($order->items as $item) {
            $totalQtyOrdered += $item->qty_ordered;
            $totalQtyCanceled += $item->qty_canceled;
        }

        if ($totalQtyOrdered == $totalQtyCanceled)
            return true;

        return false;
    }

    /**
     * @param mixed $order
     * @return void
     */
    public function isInClosedState($order)
    {
        $totalQtyOrdered = 0;
        $totalQtyRefunded = 0;
        $totalQtyCanceled = 0;

        foreach ($order->items  as $item) {
            $totalQtyOrdered += $item->qty_ordered;
            $totalQtyRefunded += $item->qty_refunded;
            $totalQtyCanceled += $item->qty_canceled;
        }

        if ($totalQtyOrdered == $totalQtyRefunded + $totalQtyCanceled)
            return true;

        return false;
    }

    /**
     * @param mixed $order
     * @return void
     */
    public function updateOrderStatus($order)
    {
        $status = 'processing';

        if ($this->isInShippedState($order))
            $status = 'shipped';
        if ($this->isInCompletedState($order))
            $status = 'completed';

        if ($this->isInCanceledState($order))
            $status = 'canceled';
        else if ($this->isInClosedState($order))
            $status = 'closed';

        $order->status = $status;
        $order->save();
    }

    /**
     * @param mixed $order
     * @return mixed
     */
    public function collectTotals($order)
    {
        $order->sub_total_invoiced = $order->base_sub_total_invoiced = 0;
        $order->shipping_invoiced = $order->base_shipping_invoiced = 0;
        $order->tax_amount_invoiced = $order->base_tax_amount_invoiced = 0;

        foreach ($order->invoices as $invoice) {
            $order->sub_total_invoiced += $invoice->sub_total;
            $order->base_sub_total_invoiced += $invoice->base_sub_total;

            $order->shipping_invoiced += $invoice->shipping_amount;
            $order->base_shipping_invoiced += $invoice->base_shipping_amount;

            $order->tax_amount_invoiced += $invoice->tax_amount;
            $order->base_tax_amount_invoiced += $invoice->base_tax_amount;
        }

        $order->grand_total_invoiced = $order->sub_total_invoiced + $order->shipping_invoiced + $order->tax_amount_invoiced;
        $order->base_grand_total_invoiced = $order->base_sub_total_invoiced + $order->base_shipping_invoiced + $order->base_tax_amount_invoiced;

        $order->save();

        return $order;
    }
}

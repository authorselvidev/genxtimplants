<?php

namespace Webkul\Product\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductFlat extends Model
{
    protected $table = 'product_flat';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public $timestamps = false;

    /**
     * Get the product that owns the attribute value.
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    static function ProductName($id='')
    {
        $product_name=DB::table('product_flat')->where('product_id',$id)->value('name');
        return $product_name;
    }
}
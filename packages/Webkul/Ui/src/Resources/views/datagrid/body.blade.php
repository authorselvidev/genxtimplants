<tbody>
    @if (count($records))

        @foreach ($records as $key => $record)
            <tr>
                @if ($enableMassActions)
                    <td>
                        <span class="checkbox">
                            <input type="checkbox" v-model="dataIds" @change="select" value="{{ $record->{$index} }}">

                            <label class="checkbox-view" for="checkbox"></label>
                        </span>
                    </td>
                @endif
                
                @foreach ($columns as $column)
                    @php
                        $columnIndex = explode('.', $column['index']);

                        $columnIndex = end($columnIndex);
                        $columnLabel = str_replace('_', ' ', $columnIndex);
                        $columnLabel = ucfirst($columnLabel);
                        //dd($columnIndex);

                    @endphp
        
                    @if(!isset($column['route_data']))

                    @if (isset($column['wrapper']) )
                        @if (isset($column['closure']) && $column['closure'] == true)
                            <td data-value="{{$columnLabel}}">{!! $column['wrapper']($record) !!}</td>
                        @else
                            <td data-value="{{$columnLabel}}">
                                 {{ $column['wrapper']($record) }} 
                            </td>
                        @endif
                    @else
                        @if($column['type'] == 'price')
                            @if(isset($column['currencyCode']))
                                <td data-value="{{$columnLabel}}">{{ core()->formatPrice($record->{$columnIndex}, $column['currencyCode']) }}</td>
                            @else
                                <td data-value="{{$columnLabel}}">{{ core()->formatBasePrice($record->{$columnIndex}) }}</td>
                            @endif
                        @else
                            <td data-value="{{$columnLabel}}">{{ $record->{$columnIndex} }}</td>
                        @endif
                    @endif

                    @else 
                        <td data-value="{{$columnLabel}}">
                        <a href="{{route('admin.'.$column['route_data'].'.show',[$record->{$index}])}}">
                            {!! $record->{$columnIndex} !!}
                        </a>
                        </td>
                    @endif

                @endforeach

                @if ($enableActions)
                    <td class="actions" style="width: 100px;">
                        <div>
                            <?php $class=""; ?>
                            @foreach ($actions as $action)
                                @if(isset($action['shipment_data']))
                                <?php $shipment=DB::table('shipments')->where('id',$record->shipment_id)->first(); 
                                //dd($shipment); ?>
                                <a data-order_id="{{ $record->shipment_order_id }}" data-carr-id="{{ $shipment->shipping_carrier_id }}" data-track-no="{{ $shipment->track_number }}" class="tracking-info" data-toggle="modal" data-target="#UpdateTrackingInfo" href="#"><i class="icon pencil-lg-icon"></i></a>
                                @elseif(isset($action['smart_edit_qty']))
                                    <?php $product=DB::table('products')->where('id',$record->product_id)->first(); 
                                    //dd($product); ?>
                                    <a data-product_id="{{ $record->product_id }}" data-quantity="{{ $record->quantity }}" class="update-qty" data-toggle="modal" data-target="#UpdateQuantityInfo" href="#"><i class="{{ $action['icon'] }}"></i></a>
                                
                                @elseif(isset($action['reject_comment']))
                                    <a data-credit_id="{{ $record->credit_id }}" class="reject-comment" data-toggle="modal" data-target="#RejectCommentModal" href="#"><i class="{{ $action['icon'] }}"></i></a>
                                        
                                @elseif(isset($action['smart_edit_status']))
                                    <?php $order=DB::table('orders')->where('id',$record->id)->first(); 
                                    //dd($product); ?>
                                    @if(($order->status != 'completed') && (auth()->guard('admin')->user()->role_id == 1 || auth()->guard('admin')->user()->role_id==4))
                                        <a class="pull-right" class="update-status" data-toggle="modal" data-order_id = "{{ $record->id }}" data-target="#changeOrderStatus" href=""><i class="{{ $action['icon'] }}"></i></a>
                                    @endif
                                @elseif(isset($action['smart_edit_failure']))
                                    <?php 
                                        $return_implant = \DB::table('failed_implants')->where('id', $record->id)->first();
                                    ?>
                                    @if($return_implant->status != 'Completed' && $return_implant->status != 'Rejected')
                                        <a class="update-status" data-toggle="modal" data-status="{{$record->f_status}}" data-update_id = "{{ $record->id }}" data-target="#changeFailedImplantStatus" href="">
                                            <i class="{{ $action['icon'] }}"></i>
                                        </a>
                                    @endif
                                @elseif(isset($action['smart_edit_discount']))
                                    <?php 
                                        $discount = \DB::table('discounts')->where('id', $record->id)->first();
                                    ?>
                                    @if($discount->active==1)
                                        <a class="update-status" aria-hidden="true" href="{{route($action['route'])}}?id={{$record->id}}&state=1">
                                            <i class="fa fa-toggle-on" style="font-size:1.8rem"></i>
                                        </a>
                                    @else
                                        <a class="update-status" aria-hidden="true" href="{{route($action['route'])}}?id={{$record->id}}&state=0">
                                            <i class="fa fa-toggle-off" style="font-size:1.8rem"></i>
                                        </a>
                                    @endif
                                @else
                                    <a title="@if(isset($action['class'])){{$action['class']}}@endif" class="@if(isset($action['class'])){{$action['class']}}@endif @if($action["type"]=='Delete'){{'delete-conform'}}@endif" href="{{ route($action['route'], $record->{$index}) }}">
                                        <span class="{{ $action['icon'] }}"></span>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </td>
                @endif
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="10" style="text-align: center;">{{ $norecords }}</td>
        </tr>
    @endif
</tbody>
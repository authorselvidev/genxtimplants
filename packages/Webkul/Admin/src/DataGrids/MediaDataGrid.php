<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * LocalesDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class MediaDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('media')->where('is_deleted',0)->addSelect('id','media_name','is_deleted');
        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('admin::app.datagrid.id'),
            'type' => 'number',
            'searchable' => false,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'media_name',
            'label' => trans('admin::app.datagrid.media_name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'default' => ''
        ]);

        $this->addColumn([
            'index' => 'is_deleted',
            'label' => trans('admin::app.datagrid.is_deleted'),
            'type' => '',
            'searchable' => true,
            'sortable' => true,
            'default' => '1'
        ]);

        /*$this->addColumn([
            'index' => 'created_at',
            'label' => trans('admin::app.datagrid.created_at'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);*/
    }

    public function prepareActions() {
        /*$this->addAction([
            'type' => 'Edit',
            'route' => 'admin.media.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);*/

        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.media.destroy',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Menu']),
            'icon' => 'icon trash-icon'
        ]);
    }
}
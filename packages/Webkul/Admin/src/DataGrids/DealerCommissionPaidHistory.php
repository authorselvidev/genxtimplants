<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * OrderDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DealerCommissionPaidHistory extends DataGrid
{
    protected $index = 'dealer_id';

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
       $queryBuilder = DB::table('users as u')
                ->leftJoin('dealer_commission as dc','dc.dealer_id','u.id')
                ->leftJoin('orders as o','o.id','dc.order_id')
                ->leftJoin('order_items as oi','oi.order_id','o.id')
                ->where('u.is_deleted',0)
                ->addSelect( DB::raw('CONCAT(u.first_name, " ", u.last_name) as full_name'))
               // ->addSelect( DB::raw('sum(dc.commission_paid_amount) as total_comm_paid'))
                 ->addSelect( DB::raw('count(oi.product_id) as no_of_products'))
                ->addSelect('u.id as dealer_id','o.id as order_id','dc.commission_paid_amount');

       //$this->addFilter('dealer_commissions', 'u.dealer_commission');
             // $this->addFilter('dealer_id', 'u.id');
              $this->addFilter('order_id', 'o.id');
              $this->addFilter('no_of_products', DB::raw('count(oi.product_id'));
              $this->addFilter('commission_paid_amount', 'dc.commission_paid_amount');

             // $this->addFilter('total_comm_paid', DB::raw('sum(dc.commission_paid_amount)'));

        $this->addFilter('full_name',  DB::raw('CONCAT(u.first_name, " ", u.last_name)'));

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
         
        $this->addColumn([
            'index' => 'full_name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'searchable' => false,
            'sortable' => false,
        ]);

        $this->addColumn([
            'index' => 'order_id',
            'label' =>'Order ID',
            'type' => 'number',
            'searchable' => false,
            'sortable' => false,
        ]);
        $this->addColumn([
            'index' => 'no_of_products',
            'label' =>'No. of Products',
            'type' => 'number',
            'searchable' => false,
            'sortable' => false,
        ]);

        $this->addColumn([
            'index' => 'commission_paid_amount',
            'label' =>'Commission Paid',
            'type' => 'string',
            'searchable' => false,
            'sortable' => false,
        ]);
        
        
    }

    public function prepareActions() {
     
    }
}

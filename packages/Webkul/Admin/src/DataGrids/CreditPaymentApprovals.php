<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;
use Webkul\Customer\Models\Customer;

/**
 * CustomerDataGrid class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CreditPaymentApprovals extends DataGrid
{
    protected $index = 'credit_id'; //the column that needs to be treated as index column

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('credit_payments as cp')
                ->where('cp.status',0)
                ->whereIn('cp.payment_type',[2,3])
                ->leftJoin('users as u', 'u.id', 'cp.user_id')
                ->addSelect('cp.id as credit_id', 'cp.user_id','cp.paid_by','cp.payment_type' ,'cp.status', 'cp.cheque_number', 'cp.notes','cp.cheque_image','cp.amount','cp.created_at')
                ->addSelect('u.id as user_id', 'u.role_id as user_role')
                ->addSelect(DB::raw('CONCAT(u.first_name, " ", u.last_name) as full_name'));

        $this->addFilter('credit_id', 'cp.id');
        $this->addFilter('cheque_number', 'cp.cheque_number');
        $this->addFilter('user_id', 'u.id');
        $this->addFilter('user_role', 'u.role_id');
        $this->addFilter('cheque_image', 'cp.cheque_image');
        $this->addFilter('payment_type', 'cp.payment_type');
        $this->addFilter('notes', 'cp.notes');
        $this->addFilter('amount', 'cp.amount');
        $this->addFilter('created_at', 'cp.created_at');
        $this->addFilter('full_name', DB::raw('CONCAT(u.first_name, " ", u.last_name)'));

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        
        $this->addColumn([
            'index' => 'user_id',
            'label' => 'ID',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'closure' => true,
            'wrapper' => function($value) {
                $profile_route = '';

                if($value->user_role=='2'){
                    $profile_route = route('admin.dealer.show', $value->user_id);
                }else{
                    $profile_route = route('admin.doctor.show', $value->user_id);
                }

                return '<a href="'.$profile_route.'" target="_blank">'.$value->user_id.'</a>';
            }
        ]);

        $this->addColumn([
            'index' => 'full_name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'user_role',
            'label' => 'User Type',
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value) {
                $roles = array(
                    '1' => 'Admin',
                    '2' => 'Dealer',
                    '3' => 'Doctor',
                    '4' => 'Store Manager');
                return $roles[$value->user_role];
            }
        ]);
        $this->addColumn([
            'index' => 'amount',
            'label' => 'Amount',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'payment_type',
            'label' => 'Payment Type',
            'type'  => 'string',
            'searchable' => true,
            'sortable' =>true,
            'wrapper' => function($value){
                if($value->payment_type == '1'){
                    return 'Razorpay';
                }else if($value->payment_type == '2'){
                    return 'Cheque';
                }else if($value->payment_type == '3'){
                    return 'Bank/NEFT/UPI';
                }
            }
        ]);

        $this->addColumn([
            'index' => 'cheque_number',
            'label' => 'Cheque Number',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'cheque_image',
            'label' => 'Cheque Image',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
             'closure' => true,
            'wrapper' => function($value) {
                if($value->cheque_image != "")
                    return '<a target="_blank" class="img-enlargeable" href="'.url(bagisto_asset('images/credit_images/'.$value->cheque_image)).'">Click here</a>';
                else
                    return "";
            }
        ]);

        $this->addColumn([
            'index' => 'notes',
            'label' => 'Notes',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true
        ]);


        $this->addColumn([
            'index' => 'status',
            'label' => 'Status',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                if ($value->status == 0)
                    return '<span class="badge badge-md badge-warning">Pending</span>';
                else if ($value->status == 1)
                    return '<span class="badge badge-md badge-success" style="background-color: #007704;">Approved</span>';
                else if ($value->status == 2)
                    return '<span class="badge badge-md badge-danger"  style="background-color: #e40000;">Rejected</span>';
            }
        ]);
        $this->addColumn([
            'index' => 'created_at',
            'label' => 'Date',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value) {
                   return date('d/m/Y',strtotime($value->created_at));
            }
        ]);
    }

    public function prepareActions() {
       
        //$get_user = DB::table('users')->where('is_approved')->get();
        $this->addAction([
            'type' => 'Edit',
            'route' => 'admin.credit_payment.approve',
            'icon' => 'fa fa-check',
            'class' => 'approve'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'route' => '',
            'icon' => 'fa fa-times',
            'class' => 'reject',
            'reject_comment' => 'reject_comment'
        ]);
    }

}


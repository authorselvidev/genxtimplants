<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * CustomerDataGrid class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CustomerGroupDataGrid extends DataGrid
{
    protected $index = 'id'; //the column that needs to be treated as index column

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('user_groups')->addSelect('id', 'name','order_amount_from','order_amount_to')->orderBy('order_amount_from');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => 'ID',
            'type' => 'number',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value){
                static $val = 0;
                $val++;
                return $val;
            }
        ]);

        $this->addColumn([
            'index' => 'name',
            'label' => 'Name',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'order_amount_from',
            'label' => 'Order Amount From',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value){
                return number_format($value->order_amount_from,2);
            }
        ]);
        $this->addColumn([
            'index' => 'order_amount_to',
            'label' => 'Order Amount To',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value){
                return number_format($value->order_amount_to,2);
            }
        ]);
    }

    public function prepareActions() {
        $this->addAction([
            'type' => 'Edit',
            'route' => 'admin.groups.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.groups.delete',
            'icon' => 'icon trash-icon'
        ]);
    }
}
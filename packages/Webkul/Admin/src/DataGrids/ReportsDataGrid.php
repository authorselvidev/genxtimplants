<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * LocalesDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ReportsDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder(){

        $queryBuilder = DB::table('users')->join('orders', 'orders.customer_id', 'users.id')->addSelect('orders.id', 'orders.status',  DB::raw('CONCAT(users.my_title, " ", users.first_name, " ",users.last_name) AS full_name'), 'users.role_id as role', 'orders.total_qty_ordered', DB::raw('DATE(orders.created_at) as order_date'), 'orders.base_grand_total');

        $this->addFilter('role', 'users.role_id');
        $this->addFilter('order_date', DB::raw('DATE(orders.created_at)'));
        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'orders.id',
            'label' => 'Order ID',
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
        ]);


        $this->addColumn([
            'index' => 'full_name',
            'label' => 'Customer name',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'role',
            'label' => 'User Role',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value) {
                $roles = array(
                    '1' => 'Admin',
                    '2' => 'Dealer',
                    '3' => 'Doctor',
                    '4' => 'Store Manager');
                return $roles[$value->role];
            }
        ]);

        /*$this->addColumn([
            'index' => 'users.email',
            'label' => 'Email',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);*/

        $this->addColumn([
            'index' => 'orders.status',
            'label' => 'Status',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value){
                return ucwords($value->status);
            }
        ]);

        $this->addColumn([
            'index' => 'order_date',
            'label' => 'Order Date',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'orders.total_qty_ordered',
            'label' => 'Total Quantity',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
        ]);

        /*$this->addColumn([
            'index' => 'orders.base_sub_total',
            'label' => 'Sub Total',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if($value->base_sub_total>0){
                    return core()->formatBasePrice($value->base_sub_total);
                }else{
                    return '-';
                }
            }
        ]);

        $this->addColumn([
            'index' => 'orders.coupon_code',
            'label' => 'Coupon',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if(strlen($value->coupon_code)>0){
                    return $value->coupon_code;
                }else{
                    return '-';
                }
            }
        ]);

        $this->addColumn([
            'index' => 'orders.mem_discount_amount',
            'label' => 'MemberShip Discount (-)',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if(intval($value->mem_discount_amount)>0){
                    return core()->formatBasePrice($value->mem_discount_amount);
                }else{
                    return '-';
                }
            }
        ]);

        $this->addColumn([
            'index' => 'orders.promo_code_amount',
            'label' => 'Promo Code Discount (-)',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if(intval($value->promo_code_amount)>0){
                    return core()->formatBasePrice($value->promo_code_amount);
                }else{
                    return '-';
                }
            }
        ]);

        $this->addColumn([
            'index' => 'orders.reward_coin',
            'label' => 'Reward Points (-)',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if(intval($value->reward_coin)>0){
                    return core()->formatBasePrice($value->reward_coin);
                }else{
                    return '-';
                }
            }
        ]);

        $this->addColumn([
            'index' => 'orders.base_discount_amount',
            'label' => 'Special Discount (-)',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if(intval($value->base_discount_amount)>0){
                    return core()->formatBasePrice($value->base_discount_amount);
                }else{
                    return '-';
                }
            }
        ]);

        $this->addColumn([
            'index' => 'orders.base_shipping_amount',
            'label' => 'Shipping & Handling',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if(intval($value->base_shipping_amount)>0){
                    return core()->formatBasePrice($value->base_shipping_amount);
                }else{
                    return '-';
                }
            }
        ]);
        
        $this->addColumn([
            'index' => 'orders.tax_amount',
            'label' => 'Tax (12%)',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if(intval($value->tax_amount)>0){
                    return core()->formatBasePrice($value->tax_amount);
                }else{
                    return '-';
                }
            }
        ]);*/

        $this->addColumn([
            'index' => 'orders.base_grand_total',
            'label' => 'Grand Total',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                if(intval($value->base_grand_total)>0){
                    return core()->formatBasePrice($value->base_grand_total);
                }else{
                    return '-';
                }
            }
        ]);

    }
}
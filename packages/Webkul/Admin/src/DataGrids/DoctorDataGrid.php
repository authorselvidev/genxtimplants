<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;
use Webkul\Customer\Models\Customer;

/**
 * CustomerDataGrid class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DoctorDataGrid extends DataGrid
{
    protected $index = 'customer_id'; //the column that needs to be treated as index column

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        //dd(auth()->guard('admin')->user()->role_id);


        $queryBuilder = DB::table('users as u')
                ->where('role_id',3)
                ->where('is_approved',1)
                ->where('is_deleted',0)
                ->addSelect('u.id as customer_id', 'u.email','u.phone_number','u.dealer_id','ug.name as group_name','u.created_at')
                ->addSelect(DB::raw('CONCAT(u.my_title, " ", u.first_name, " ", u.last_name) as full_name'))
                ->leftJoin('user_groups as ug', 'u.customer_group_id', '=', 'ug.id');


        $dealer_id='';
        if(auth()->guard('admin')->user()->role_id == 2){
            $dealer_id = auth()->guard('admin')->user()->id;
            $queryBuilder->where('dealer_id',$dealer_id);
        }
        $this->addFilter('customer_id', 'u.id');
        $this->addFilter('full_name', DB::raw('CONCAT(u.my_title, " ", u.first_name, " ", u.last_name)'));
        $this->addFilter('group_name', 'ug.name');
        $this->addFilter('created_at', 'u.created_at');


        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {

        $this->addColumn([
            'index' => 'full_name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
             'route_data' => 'doctor',
        ]);

        $this->addColumn([
            'index' => 'email',
            'label' => trans('admin::app.datagrid.email'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'phone_number',
            'label' => 'Mobile Number',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'group_name',
            'label' => 'Group',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        if(auth()->guard('admin')->user()->role_id != 2) {
            $this->addColumn([
                'index' => 'dealer_id',
                'label' => 'Dealer',
                'type' => 'string',
                'searchable' => false,
                'sortable' => true,
                'wrapper' => function($value) {
                       return Customer::CustomerName($value->dealer_id);
                }
            ]);
        }
        $this->addColumn([
            'index' => 'created_at',
            'label' => 'Date',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value) {
                   return date('d/m/Y',strtotime($value->created_at));
            }
        ]);
        
    }

    public function prepareActions() {
        if(auth()->guard('admin')->user()->role_id == 1){
            $this->addAction([
                'type' => 'View',
                'route' => 'admin.doctor.show',
                'icon' => 'icon eye-icon',
                'class' => 'view'
            ]);
            $this->addAction([
                'type' => 'Edit',
                'route' => 'admin.doctor.edit',
                'icon' => 'icon pencil-lg-icon',
                'class' => 'edit'
            ]);

            $this->addAction([
                'type' => 'Delete',
                'route' => 'admin.doctor.delete',
                'icon' => 'icon trash-icon',
                'class' => 'delete'
            ]);
        }
    }

    public function prepareMassActions() {
        if(auth()->guard('admin')->user()->role_id == 1){
            $this->addMassAction([
                'type' => 'delete',
                'label' => 'Delete',
                'action' => route('admin.doctor.massdelete'),
                'method' => 'DELETE'
            ]);

            $this->enableMassAction = true;
        }
    }
}

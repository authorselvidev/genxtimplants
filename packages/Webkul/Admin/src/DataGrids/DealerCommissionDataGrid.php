<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * OrderDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DealerCommissionDataGrid extends DataGrid
{
    protected $index = 'dealer_id';

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
       $queryBuilder = DB::table('users as u')
                ->leftJoin('dealer_commission as dc','dc.dealer_id','u.id')
                ->where('u.role_id',2)
                ->where('u.role_id',2)
                ->where('u.is_deleted',0)
                ->where('dc.commission_paid_amount','!=',null)
                ->addSelect( DB::raw('CONCAT(u.first_name, " ", u.last_name) as full_name'))
                ->addSelect( DB::raw('sum(dc.commission_paid_amount) as total_comm_paid'))
                ->addSelect('u.id as dealer_id','u.email','u.dealer_commission as dealer_commissions')
                ->groupBy('dc.dealer_id');
       $this->addFilter('dealer_commissions', 'u.dealer_commission');
              $this->addFilter('dealer_id', 'u.id');

              $this->addFilter('total_comm_paid', DB::raw('sum(dc.commission_paid_amount)'));

        $this->addFilter('full_name',  DB::raw('CONCAT(u.first_name, " ", u.last_name)'));
        //dd($queryBuilder->tosql());
        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
         $this->addColumn([
            'index' => 'dealer_id',
            'label' => 'ID',
            'type' => 'number',
            'searchable' => false,
            'sortable' => true,
        ]);
        $this->addColumn([
            'index' => 'full_name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'route_data' =>'dealer_commission',
        ]);

        $this->addColumn([
            'index' => 'email',
            'label' =>'Email',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'dealer_commissions',
            'label' =>'Dealer Commission(%)',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
        ]);


        $this->addColumn([
            'index' => 'total_comm_paid',
            'label' =>'Total Commission Paid',
            'type' => 'integer',
            'searchable' => false,
            'sortable' => true,
        ]);
        
        
    }

    public function prepareActions() {
        $this->addAction([
            'type' => 'Show',
            'route' => 'admin.dealer_commission.show',
            'icon' => 'icon eye-icon'
        ]);
    }
}

<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * CustomerDataGrid class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DealerDataGrid extends DataGrid
{
    protected $index = 'customer_id'; //the column that needs to be treated as index column

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('users as u')
                ->where('role_id',2)
                ->where('is_approved',1)
                ->where('is_deleted',0)
                ->addSelect('u.id as customer_id', 'u.email','u.phone_number','u.custom_code', 'ug.name','u.created_at')
                ->addSelect( DB::raw('CONCAT(u.my_title, " ", u.first_name, " ", u.last_name) as full_name'))->leftJoin('user_groups as ug', 'u.customer_group_id', '=', 'ug.id');

        $this->addFilter('customer_id', 'u.id');
        $this->addFilter('email', 'u.email');
        $this->addFilter('phone_number', 'u.phone_number');
        $this->addFilter('custom_code', 'u.custom_code');
        $this->addFilter('created_at', 'u.created_at');
        $this->addFilter('dealer_commission', 'u.dealer_commission');
        $this->addFilter('full_name',  DB::raw('CONCAT(u.my_title, " ", u.first_name, " ", u.last_name)'));

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'full_name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'route_data' => 'dealer',
        ]);

        $this->addColumn([
            'index' => 'email',
            'label' => trans('admin::app.datagrid.email'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'phone_number',
            'label' => 'Mobile Number',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        /*$this->addColumn([
            'index' => 'custom_code',
            'label' => 'Dealer ID',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            
        ]);*/
        $this->addColumn([
            'index' => 'created_at',
            'label' => 'Date',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value) {
                   return date('d/m/Y',strtotime($value->created_at));
            }
        ]);
    }

    public function prepareActions() {
        $this->addAction([
                'type' => 'View',
                'route' => 'admin.dealer.show',
                'icon' => 'icon eye-icon',
                'class' => 'view'
        ]);
        $this->addAction([
            'type' => 'Edit',
            'route' => 'admin.dealer.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.dealer.delete',
            'icon' => 'icon trash-icon'
        ]);
    }

    public function prepareMassActions() {
        $this->addMassAction([
            'type' => 'delete',
            'label' => 'Delete',
            'action' => route('admin.dealer.massdelete'),
            'method' => 'DELETE'
        ]);

        $this->enableMassAction = true;
    }
}
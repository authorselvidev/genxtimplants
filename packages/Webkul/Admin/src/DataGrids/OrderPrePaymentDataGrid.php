<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * OrderDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class OrderPrePaymentDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        
                /*->leftJoin('user_addresses as order_address_shipping', function($leftJoin) {
                    $leftJoin->on('order_address_shipping.id', '=', 'orders.shipping_address')
                        ->where('order_address_shipping.delivery_selected', 1)
                        ->orwhere('order_address_shipping.default_address', 1);
                })
                ->leftJoin('user_addresses as order_address_billing', function($leftJoin) {
                    $leftJoin->on('order_address_billing.id', '=', 'orders.billing_address')
                        ->where('order_address_billing.billing_selected', 1)
                        ->orwhere('order_address_shipping.default_address', 1);
                })*/
		$queryBuilder = DB::table('orders')
                ->where('status','prepayment_order')
                ->leftJoin('user_addresses as shipping_addr', 'shipping_addr.id', 'orders.shipping_address')
                ->leftJoin('user_addresses as billing_addr', 'billing_addr.id', 'orders.billing_address')
                ->addSelect('orders.id', 'orders.base_sub_total', 'orders.base_grand_total', 'orders.created_at', 'orders.channel_name', 'orders.status')
                ->addSelect(DB::raw('replace(billing_addr.name,"-", " ") as billed_to'))
                ->addSelect(DB::raw('replace(shipping_addr.name,"-", " ") as shipped_to'));

                /*->addSelect(DB::raw('CONCAT(order_address_shipping.first_name, " ", order_address_shipping.last_name) as shipped_to'));*/

        $dealer_id='';
        if(auth()->guard('admin')->user()->role_id == 2){
            $dealer_id = auth()->guard('admin')->user()->id;
            $customer_id = DB::table('users')->where('dealer_id',$dealer_id)->pluck('id')->toArray();
            //print_r($customer_id);
            $queryBuilder->whereIn('orders.customer_id',$customer_id);
        }
        $this->addFilter('id', 'orders.id');
        $this->addFilter('base_sub_total', 'orders.base_sub_total');
        $this->addFilter('base_grand_total', 'orders.base_grand_total');
        $this->addFilter('created_at', 'orders.created_at');
        $this->addFilter('channel_name', 'orders.channel_name');
        $this->addFilter('status', 'orders.status');
        $this->addFilter('billed_to', DB::raw('replace(billing_addr.name,"-", " ")'));
        $this->addFilter('shipped_to', DB::raw('replace(shipping_addr.name,"-", " ")'));

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('admin::app.datagrid.id'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
        ]);

/*        $this->addColumn([
            'index' => 'base_sub_total',
            'label' => trans('admin::app.datagrid.sub-total'),
            'type' => 'price',
            'searchable' => false,
            'sortable' => true,
        ]);*/

        $this->addColumn([
            'index' => 'base_grand_total',
            'label' => 'Total',
            'type' => 'price',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'created_at',
            'label' => trans('admin::app.datagrid.order-date'),
            'type' => 'datetime',
            'sortable' => true,
            'searchable' => true,
        ]);

/*        $this->addColumn([
            'index' => 'channel_name',
            'label' => trans('admin::app.datagrid.channel-name'),
            'type' => 'string',
            'sortable' => true,
            'searchable' => true,
        ]);*/

        $this->addColumn([
            'index' => 'status',
            'label' => trans('admin::app.datagrid.status'),
            'type' => 'string',
            'sortable' => true,
            'searchable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                if ($value->status == "prepayment_order")
                    return '<span class="badge badge-md badge-warning">PrePayment Order</span>';
            }
        ]);

        $this->addColumn([
            'index' => 'billed_to',
            'label' => trans('admin::app.datagrid.billed-to'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'shipped_to',
            'label' => trans('admin::app.datagrid.shipped-to'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);
    }

    public function prepareActions() {
        $this->addAction([
            'type' => 'View',
            'route' => 'admin.sales.prepayment_order.view',
            'icon' => 'icon eye-icon'
        ]);
    }
}

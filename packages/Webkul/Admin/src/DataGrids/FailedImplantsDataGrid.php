<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

class FailedImplantsDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc'; //asc or desc

    private $myFilter = [];


    public function addMyFilter($arr){
        $this->myFilter = $arr;
    }

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('failed_implants as fi')
            ->leftJoin('order_items as oi', 'oi.id', '=', 'fi.order_item_id')
            ->leftJoin('orders as o', 'o.id', '=', 'oi.order_id')
            ->leftJoin('users as u', 'u.id', '=', 'fi.user_id')
            ->where('fi.is_deleted',0)
            ->whereIn('fi.status', $this->myFilter)
            ->addSelect('fi.id as id')
            ->addSelect(DB::raw('CONCAT(u.my_title, u.first_name, " ", u.last_name) as user_name'))
            ->addSelect('u.role_id as user_role')
            ->addSelect('fi.status as f_status')
            ->addSelect('fi.order_item as fo_item')
            ->addSelect('fi.order_type as fo_type')
            ->addSelect('fi.item_status as item_status')
            ->addSelect('o.id as f_order_id')
            ->addSelect('oi.name as item_name')
            ->addSelect('fi.created_at as f_created');
        
        $this->addFilter('id', 'fi.id');
        $this->addFilter('user_name', DB::raw('CONCAT(u.my_title, u.first_name, " ", u.last_name)'));
        $this->addFilter('user_role', 'u.role_id');
        $this->addFilter('f_status', 'fi.status');
        $this->addFilter('fo_type', 'fi.order_type');
        $this->addFilter('f_order_id', 'o.id');
        $this->addFilter('item_name', 'oi.name');
        $this->addFilter('f_created', 'fi.created_at');
        
        $this->setQueryBuilder($queryBuilder);
    }    

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('admin::app.datagrid.id'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'user_name',
            'label' => 'User Name',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'user_role',
            'label' => 'User Type',
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value) {
                $roles = array(
                    '1' => 'Admin',
                    '2' => 'Dealer',
                    '3' => 'Doctor',
                    '4' => 'Store Manager');
                return $roles[$value->user_role];
            }
        ]);

        $this->addColumn([
            'index' => 'fo_type',
            'label' => 'Order Type',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value){
                $stat = array('on' => 'Online', 'off' => 'Offline');
                return $stat[$value->fo_type];
            }
        ]);

        $this->addColumn([
            'index' => 'f_status',
            'label' => 'Status',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'item_status',
            'label' => 'Product Status',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value){
                if($value->item_status == 0) return 'Used';
                return 'Unopened';
            }
        ]);

        $this->addColumn([
            'index' => 'f_created',
            'label' => trans('admin::app.datagrid.created_at'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value){
                return date('Y-m-d', strtotime($value->f_created));
            }
        ]);
    }

    public function prepareActions() {
        $this->addAction([
            'type' => 'Edit',
            'route' => '',
            'icon' => 'fa fa-pencil-square-o',
            'smart_edit_failure' => 'smart_edit_failure'
        ]);
        
        $this->addAction([
            'type' => 'View',
            'route' => 'admin.catalog.failed-implants.view',
            'icon' => 'icon eye-icon'
        ]);

/*        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.catalog.failed-implants.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Menu']),
            'icon' => 'icon trash-icon'
        ]);*/
    }
}
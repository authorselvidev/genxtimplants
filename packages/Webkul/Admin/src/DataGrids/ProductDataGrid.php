<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * ProductDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductDataGrid extends DataGrid
{
    protected $sortOrder = 'desc'; //asc or desc

    protected $index = 'product_id';

    public function prepareQueryBuilder()
    {

      
        $queryBuilder = DB::table('products_grid as pg')
                ->leftJoin('products as pr', 'pg.product_id', '=', 'pr.id')
                ->leftJoin('product_inventories as pi', 'pi.product_id', '=', 'pr.id')
                ->where('pr.is_deleted',0)
                ->leftJoin('product_flat as pf','pf.product_id','=','pg.product_id')
                ->addSelect('pg.product_id as product_id', 'pg.sku as product_sku', 'pg.name', 'pr.type as product_type', 'pg.status', 'pg.price', 'pi.qty as quantity','pr.slot_number','pf.weight');

        $this->addFilter('product_id', 'pg.product_id');
        $this->addFilter('product_sku', 'pg.sku');
        $this->addFilter('product_type', 'pr.type');
        $this->addFilter('name', 'pg.name');
        $this->addFilter('slot_number', 'pr.slot_number');
        $this->addFilter('price', 'pg.price');
        $this->addFilter('weight', 'pf.weight');
        $this->addFilter('quantity', 'pi.qty');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {

        $this->addColumn([
            'index' => 'product_sku',
            'label' => trans('admin::app.datagrid.sku'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            //'width' => '100px'
        ]);

        $this->addColumn([
            'index' => 'name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'product_type',
            'label' => trans('admin::app.datagrid.type'),
            'type' => 'string',
            'sortable' => true,
            'searchable' => true
        ]);
        $this->addColumn([
            'index' => 'slot_number',
            'label' => 'Slot Number',
            'type' => 'string',
            'sortable' => true,
            'searchable' => true
        ]);

        $this->addColumn([
            'index' => 'weight',
            'label' => 'Weight',
            'type' => 'string',
            'sortable' => true,
            'searchable' => true
        ]);

        /*$this->addColumn([
            'index' => 'status',
            'label' => trans('admin::app.datagrid.status'),
            'type' => 'boolean',
            'sortable' => true,
            'searchable' => false,
            'wrapper' => function($value) {
                if ($value->status == 1)
                    return 'Active';
                else
                    return 'Inactive';
            }
        ]);*/

        $this->addColumn([
            'index' => 'price',
            'label' => trans('admin::app.datagrid.price'),
            'type' => 'price',
            'sortable' => true,
            'searchable' => false
        ]);

        $this->addColumn([
            'index' => 'quantity',
            'label' => trans('admin::app.datagrid.qty'),
            'type' => 'number',
            'sortable' => true,
            'searchable' => false
        ]);
    }

    public function prepareActions() {
        $this->addAction([
            'type' => 'Edit',
            'route' => '',
            'icon' => 'fa fa-pencil-square-o',
            'smart_edit_qty' => 'smart_edit_qty'
        ]);
        if(auth()->guard('admin')->user()->role_id != 4){

        $this->addAction([
            'type' => 'Show',
            'route' => 'admin.catalog.products.show',
            'icon' => 'fa fa-eye'
        ]);
        }

        $this->addAction([
            'type' => 'Edit',
            'route' => 'admin.catalog.products.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);
        
        if(auth()->guard('admin')->user()->role_id != 4){

        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.catalog.products.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'product']),
            'icon' => 'icon trash-icon'
        ]);
        }

        $this->enableAction = true;
    }

    public function prepareMassActions() {
         if(auth()->guard('admin')->user()->role_id != 4){
            $this->addMassAction([
                'type' => 'delete',
                'label' => 'Delete',
                'action' => route('admin.catalog.products.massdelete'),
                'method' => 'DELETE'
            ]);

            $this->addMassAction([
                'type' => 'update',
                'label' => 'Update Status',
                'action' => route('admin.catalog.products.massupdate'),
                'method' => 'PUT',
                'options' => [
                    'Active' => 1,
                    'Inactive' => 0
                ]
            ]);
        }

        $this->enableMassAction = true;
    }

    public function disablePaginate() {
        $this->paginate = false;
    }
}
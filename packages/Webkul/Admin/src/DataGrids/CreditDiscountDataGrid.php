<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use Webkul\Customer\Models\CustomerGroup;
use DB;

/**
 * LocalesDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CreditDiscountDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('discounts')->where('discount_type',5)
                                              ->where('product_id',null)
                                              ->where('is_deleted',0)
                                              ->addSelect('id','discount_title','minimum_order_amount','coupon_code');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('admin::app.datagrid.id'),
            'type' => 'integer',
            'searchable' => false,
            'sortable' => true,
            'autoIncrement' => true,
            'unsigned' => true
        ]);

        $this->addColumn([
            'index' => 'discount_title',
            'label' => 'Discount Title',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'minimum_order_amount',
            'label' => 'Min Order Amount',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'coupon_code',
            'label' => 'Discount Code',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);
        
    }

    public function prepareActions() {
        $this->addAction([
            'route' => 'admin.credit-discount.status',
            'smart_edit_discount' => 'smart_edit_discount'
        ]);

        $this->addAction([
            'type' => 'Edit',
            'route' => 'admin.credit-discount.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.credit-discount.destroy',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Menu']),
            'icon' => 'icon trash-icon'
        ]);
    }
}
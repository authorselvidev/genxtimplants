<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Customer\Models\Customer;
use Webkul\Ui\DataGrid\DataGrid;
use DB;

/**
 * CustomerDataGrid class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class UnverifiedDoctorDataGrid extends DataGrid
{
    protected $index = 'customer_id'; //the column that needs to be treated as index column

    protected $sortOrder = 'desc'; //asc or desc

    

    public function prepareQueryBuilder()
    {   
        $queryBuilder = DB::table('users as u')
                ->where('u.is_verified',0)
                ->where('u.is_approved',0)
                ->where('u.is_rejected',0)
                ->addSelect('u.id as customer_id', 'u.email','u.phone_number','u.dealer_id','ug.name','u.created_at', 'u.is_verified')
                ->addSelect(DB::raw('CONCAT(u.first_name, " ", u.last_name) as full_name'))
                ->leftJoin('user_groups as ug', 'u.customer_group_id', '=', 'ug.id');

        $this->addFilter('customer_id', 'u.id');
        $this->addFilter('full_name', DB::raw('CONCAT(u.first_name, " ", u.last_name)'));
        $this->addFilter('email', 'u.email');
        $this->addFilter('is_verified', 'u.is_verified');
        $this->addFilter('phone_number', 'u.phone_number');
        $this->addFilter('created_at', 'u.created_at');     

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'full_name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
             'route_data' => 'doctor',
        ]);

        $this->addColumn([
            'index' => 'email',
            'label' => trans('admin::app.datagrid.email'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'phone_number',
            'label' => 'Phone Number',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'dealer_id',
            'label' => 'Dealer',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value){
                   return Customer::CustomerName($value->dealer_id);
            }
        ]);

        $this->addColumn([
            'index' => 'created_at',
            'label' => 'Date',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value) {
                   return date('d/m/Y',strtotime($value->created_at));
            }
        ]);
    }

    public function prepareActions() {
       
        $this->addAction([
            'type' => 'Edit',
            'route' => 'admin.doctors.unverified.notify',
            'icon' => 'fa fa-envelope-o',            
            'class' => 'notify'
        ]);


        $this->addAction([
            'type' => 'Edit',
            'route' => 'admin.doctors.unverified.verify',
            'icon' => 'fa fa-check',
            'class' => 'verify'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.doctor.unverified.delete',
            'icon' => 'fa fa-times',
            'class' => 'delete-user'
        ]);
    }

    public function prepareMassActions() {
        $this->addMassAction([
            'type' => 'delete',
            'label' => 'Delete',
            'action' => route('admin.doctor.unverified.massDelete'),
            'method' => 'DELETE'
        ]);

        $this->enableMassAction = true;
    }
}
<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;
use Webkul\Customer\Models\Customer;

/**
 * CustomerDataGrid class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DoctorApprovals extends DataGrid
{
    protected $index = 'customer_id'; //the column that needs to be treated as index column

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('users as u')
                ->where('u.role_id',3)
                ->where('u.is_deleted',0)
                ->where('u.is_approved',0)
                ->where('u.is_verified',1)
                ->where('u.is_rejected',0)
                ->addSelect('u.id as customer_id', 'u.email','u.phone_number','u.dealer_id','ug.name','u.created_at')
                ->addSelect(DB::raw('CONCAT(u.first_name, " ", u.last_name) as full_name'))->leftJoin('user_groups as ug', 'u.customer_group_id', '=', 'ug.id');

        $this->addFilter('customer_id', 'u.id');
        $this->addFilter('full_name', DB::raw('CONCAT(u.first_name, " ", u.last_name)'));

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {

        $this->addColumn([
            'index' => 'full_name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
             'route_data' => 'doctor',
        ]);

        $this->addColumn([
            'index' => 'email',
            'label' => trans('admin::app.datagrid.email'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'phone_number',
            'label' => 'Phone Number',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
        ]);

        $this->addColumn([
            'index' => 'dealer_id',
            'label' => 'Dealer',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function($value) {
                   return Customer::CustomerName($value->dealer_id);
            }
        ]);
        $this->addColumn([
            'index' => 'created_at',
            'label' => 'Date',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($value) {
                   return date('d/m/Y',strtotime($value->created_at));
            }
        ]);
    }

    public function prepareActions() {
       
        //$get_user = DB::table('users')->where('is_approved')->get();
        $this->addAction([
            'type' => 'Edit',
            'route' => 'admin.doctor.approve',
            'icon' => 'fa fa-check',
            'class' => 'approve'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.doctor.reject',
            'icon' => 'fa fa-times',
            'class' => 'reject'
        ]);
    }

    public function prepareMassActions() {
        $this->addMassAction([
            'type' => 'delete',
            'label' => 'Delete',
            'action' => route('admin.doctor.massdelete'),
            'method' => 'DELETE'
        ]);

        $this->enableMassAction = true;
    }
}
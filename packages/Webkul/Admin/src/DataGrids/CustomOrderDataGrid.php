<?php

namespace Webkul\Admin\DataGrids;

use Webkul\Ui\DataGrid\DataGrid;
use DB;
use Webkul\Customer\Models\Customer;


/**
 * OrderDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CustomOrderDataGrid extends DataGrid
{
    protected $index = 'customer_id';

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('custom_orders as co')
                /*->leftJoin('user_addresses as order_address_shipping', function($leftJoin) {
                    $leftJoin->on('order_address_shipping.id', '=', 'orders.shipping_address')
                        ->where('order_address_shipping.delivery_selected', 1)
                        ->orwhere('order_address_shipping.default_address', 1);
                })
                ->leftJoin('user_addresses as order_address_billing', function($leftJoin) {
                    $leftJoin->on('order_address_billing.id', '=', 'orders.billing_address')
                        ->where('order_address_billing.billing_selected', 1)
                        ->orwhere('order_address_shipping.default_address', 1);
                })*/
                
                ->addSelect('co.id', 'co.created_by', 'co.created_at', 'co.status','co.customer_id');
                /*->addSelect(DB::raw('CONCAT(order_address_shipping.first_name, " ", order_address_shipping.last_name) as shipped_to'));*/

        $dealer_id='';
        if(auth()->guard('admin')->user()->role_id == 2){
            $dealer_id = auth()->guard('admin')->user()->id;
            $customer_id = DB::table('users')->where('dealer_id',$dealer_id)->pluck('id')->toArray();
            //print_r($customer_id);
            $queryBuilder->whereIn('co.customer_id',$customer_id);
        }
        

        $this->addFilter('id', 'co.id');
        $this->addFilter('customer_id', 'co.customer_id');
        $this->addFilter('created_by', 'co.created_by');
        $this->addFilter('created_at', 'co.created_at');
        $this->addFilter('status', 'co.status');
        $this->setQueryBuilder($queryBuilder);

       
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('admin::app.datagrid.id'),
            'type' => 'number',
            'searchable' => false,
            'sortable' => true,
        ]);
      

        $this->addColumn([
            'index' => 'created_by',
            'label' => 'Created By',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'wrapper' => function ($value) {
                
                    return Customer::CustomerName($value->created_by);
            }
        ]);
  
        $this->addColumn([
            'index' => 'created_at',
            'label' => trans('admin::app.datagrid.order-date'),
            'type' => 'datetime',
            'sortable' => true,
            'searchable' => false,
            'wrapper' => function($value) {
                return date('d-m-Y h:ia', strtotime($value->created_at));
            }
        ]);

/*        $this->addColumn([
            'index' => 'channel_name',
            'label' => trans('admin::app.datagrid.channel-name'),
            'type' => 'string',
            'sortable' => true,
            'searchable' => true,
        ]);*/

        $this->addColumn([
            'index' => 'status',
            'label' => trans('admin::app.datagrid.status'),
            'type' => 'string',
            'sortable' => true,
            'searchable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                if ($value->status == 0)
                    return 'Inactive';
                else if ($value->status == 1)
                    return 'Active';
            }
        ]);

    }

    public function prepareActions() {
      /*  $this->addAction([
            'type' => 'Edit',
            'route' => '',
            'icon' => 'fa fa-pencil-square-o',
            'smart_edit_status' => 'smart_edit_status'
        ]);

        $this->addAction([
            'type' => 'View',
            'route' => 'admin.sales.orders.view',
            'icon' => 'icon eye-icon'
        ]);*/
    }
}

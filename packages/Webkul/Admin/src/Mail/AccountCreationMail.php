<?php

namespace Webkul\Admin\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountCreationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($full_name,$email,$set_password_url)
    {
        $this->full_name = $full_name;
        $this->email = $email;
        $this->set_password_url = $set_password_url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@genxtimplants.com', 'GenXT')->view('shop::emails.customer.account-creation',['full_name'=>$this->full_name,'email'=>$this->email,'set_password_url' => $this->set_password_url])->subject('GenXT - New Account Generated!');
    }
}

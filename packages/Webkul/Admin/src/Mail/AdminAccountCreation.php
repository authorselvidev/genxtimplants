<?php

namespace Webkul\Admin\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminAccountCreation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($full_name,$email,$password,$type)
    {
        $this->full_name = $full_name;
        $this->email = $email;
        $this->password = $password;
        $this->type = $type;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('preethi@authorselvi.com', 'GenXT')->view('shop::emails.admin.admin-account-creation',['full_name'=>$this->full_name,'email'=>$this->email,'password' => $this->password,'type' => $this->type])->subject('GenXT - New Account Generated!');
    }
}

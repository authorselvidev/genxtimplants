<?php

namespace Webkul\Admin\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountVerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($otp_verification,$set_verification_url)
    {
        $this->otp_verification = $otp_verification;
        $this->set_verification_url = $set_verification_url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@genxtimplants.com', 'GenXT')->view('shop::emails.customer.account-verification',['otp_verification'=>$this->otp_verification,'set_verification_url' => $this->set_verification_url])->subject('GenXT - Account Verification!');
    }
}

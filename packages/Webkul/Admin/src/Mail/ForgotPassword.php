<?php

namespace Webkul\Admin\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($interested_in,$my_title,$full_name,$email,$phone_number,$country_name,$contact_msg)
    {
        $this->interested_in = $interested_in;
        $this->my_title = $my_title;
        $this->full_name = $full_name;
        $this->email = $email;
        $this->phone_number = $phone_number;
        $this->country_name = $country_name;
        $this->contact_msg = $contact_msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@genxtimplants.com', 'GenXT')->view('shop::emails.contact-mail',[
                                            'interested_in' => $this->interested_in,
                                            'my_title'=>$this->my_title,
                                            'full_name'=>$this->full_name,
                                            'email'=>$this->email,
                                            'phone_number' => $this->phone_number,
                                            'country_name' => $this->country_name,
                                            'contact_msg' => $this->contact_msg,
                                         ])->subject('GenXT - Contact Form!');
    }
}

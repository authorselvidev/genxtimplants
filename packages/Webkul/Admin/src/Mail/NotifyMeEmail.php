<?php

namespace Webkul\Admin\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * New Order Mail class
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class NotifyMeEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * The order instance.
     *
     * @var Order
     */
    public $notify_info;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($notify_info)
    {
        $this->notify_info = $notify_info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@genxtimplants.com', 'GenXT')
                ->to($this->notify_info['email'], $this->notify_info['first_name'])
                ->subject('GenXT - Stock Available now!')
                ->view('shop::emails.notifyme-product-tocustomer');
    }
}

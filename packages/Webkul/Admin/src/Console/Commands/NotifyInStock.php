<?php

namespace Webkul\Admin\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use Webkul\Admin\Mail\NotifyMeEmail;
use Webkul\Product\Repositories\ProductRepository as Product;


class NotifyInStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:instock';

    protected $product;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The command used to execute everyday to notify customers for products InStock.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notify_customers = DB::table('notify_me')->where('notify_sent',0)->get();
        $notify_info = array();
        foreach($notify_customers as $key => $notify)
        {
            $customer_info = DB::table('users')->where('id',$notify->customer_id)->first();
            $product_info = DB::table('products_grid')->where('product_id',$notify->product_id)->first();
            $inventory = DB::table('product_inventories')->where('product_id',$notify->product_id)->where('inventory_source_id',1)->first();
            $ordered_inventory = DB::table('product_ordered_inventories')->where('product_id',$notify->product_id)->first();
            $qty_available = $inventory->qty;
            if(isset($ordered_inventory)) {
                $qty_available = $inventory->qty - $ordered_inventory->qty;
            }
            if($qty_available > 0 && isset($customer_info)) {
                $notify_info['email'] = $customer_info->email;
                $notify_info['first_name'] = $customer_info->first_name;
                $notify_info['product_name'] = $product_info->name;
                $mail_sent = Mail::send(new NotifyMeEmail($notify_info));
                DB::table('notify_me')->where('id',$notify->id)->update(['notify_sent' => 1]);
            }


        }

    }
}

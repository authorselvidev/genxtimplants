<div class="navbar-top">
    <div class="navbar-top-left">
        <div class="brand-logo">
            <a href="{{ (auth()->guard('admin')->user()->role_id==4)?route('admin.sales.orders.index'):route('admin.dashboard.index') }}">
                <img src="{{ bagisto_asset('images/footer-logo.png') }}" alt="GenXT"/>
            </a>
        </div>
    </div>

    <div class="navbar-top-right">
		
		@if(auth()->guard('admin')->user()->role['name'] == 'Dealer' )
		<div class="navbar-menu">
			<div class="dropdown-toggle"><i class="fa fa-bars"></i> Menu</div>
			<div class="dropdown-list bottom-right" style="bottom: unset">
			<div class="dropdown-container">
			@include('admin::layouts.nav-left')
			</div></div>
		</div>	
		@endif
		
        <div class="profile">
            <span class="avatar">
            </span>

            <div class="profile-info">
                <div class="dropdown-toggle">
                    <div style="display: inline-block; vertical-align: middle;">
                        <!--<img src="{{ bagisto_asset('images/profile-pic.png') }}" width="30" height="30">-->
                        <span class="name">
                            {{ auth()->guard('admin')->user()->name }}
                        </span>

                        <span class="role">
                            {{ auth()->guard('admin')->user()->role['name'] }}
                        </span>
                    </div>
                    <!--<i class="icon arrow-down-icon active"></i>-->
                </div>

                <div class="dropdown-list bottom-right">
                    <div class="dropdown-container">
                        <label>Account</label>
                        <ul>
                            @if(auth()->guard('admin')->user()->role['id'] != 2)
                                <li>
                                    <a href="{{ route('shop.home.index') }}" target="_blank">{{ trans('admin::app.layouts.visit-shop') }}</a>
                                </li>
                            @endif 
                            <li>
                                <a href="{{ route('admin.profile.index') }}">{{ trans('admin::app.layouts.my-account') }}</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.session.destroy') }}">{{ trans('admin::app.layouts.logout') }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

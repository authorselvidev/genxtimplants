<div @if(auth()->guard('admin')->user()->role['id'] != 2) class="navbar-left" @endif  id="sidebar-wrapper">
    <ul class="menubar">
        <?php //dd($menu->items); ?>
        @foreach ($menu->items as $menuItem)
         @if(strpos($menuItem['route'],'credit_payment.pay') !== false)
                <?php $user = auth()->guard('admin')->user();
                $due_amount = $user->credit_used - $user->credit_paid; 
                //dd($due_amount); ?>
                @if($user->role_id == 2 && $due_amount > 0)
                    <li class="menu-item {{ $menu->getActive($menuItem) }}">
                        <a href="{{ count($menuItem['children']) ? current($menuItem['children'])['url'] : $menuItem['url'] }}">
                           <span class="icon {{ $menuItem['icon-class'] }}"></span>
                            {{ trans($menuItem['name']) }}
                        </a>
                        @if($menu->getActive($menuItem) == 'active') @include ('admin::layouts.nav-aside') @endif
                    </li>
                @endif
            @else
                <li class="menu-item {{ $menu->getActive($menuItem) }}">
                    <a href="{{ count($menuItem['children']) ? current($menuItem['children'])['url'] : $menuItem['url'] }}">
                       <span class="icon {{ $menuItem['icon-class'] }}"></span>
                        {{ trans($menuItem['name']) }}
                    </a>
                    @if($menu->getActive($menuItem) == 'active') @include ('admin::layouts.nav-aside') @endif
                </li>
            @endif
        @endforeach

        @if(auth()->guard('admin')->user()->role['id'] == 2)
            <li class="menu-item active">
                <a href="{{route('admin.dealer.commission_show', auth()->guard('admin')->user()->id)}}">
                    <span class="icon fa fa-user"></span>
                    Commission
                </a>
            </li>
            <li class="menu-item active">
                <a href="{{ route('shop.home.index') }}">
                    <span class="icon fa fa-user"></span>
                    {{ trans('admin::app.layouts.visit-shop') }}
                </a>
            </li>
        @endif

        
    </ul>
</div>

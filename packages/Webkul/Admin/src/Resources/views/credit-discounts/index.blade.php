@extends('admin::layouts.content')

@section('page_title')
    Advance Payment Discount
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Advance Payment Discount</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.credit-discount.create') }}" class="btn btn-lg btn-primary">
                    Create Discount
                </a>
            </div>
        </div>

        <div class="page-content">

           @inject('credit_discount','Webkul\Admin\DataGrids\CreditDiscountDataGrid')
            {!! $credit_discount->render() !!}
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(document).ready(function(){
        $('body').on('click', '.fa-toggle-on', function(e){
            if(!confirm('Are you sure, you want to disable')){
                e.preventDefault();
            }
        });
    });
</script>
@endpush
@extends('admin::layouts.content')

@section('page_title')
    Edit Advance Payment Discount
@stop

@section('content')
    <div class="content">

        <form method="post" id="creditForm" action="{{ route('admin.credit-discount.update', $discount->id) }}">
            @csrf()
            <input name="_method" type="hidden" value="PATCH">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/credit-discount') }}';"></i>
                        Edit Advance Payment Discount
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary btn-submit-credit">
                        Update Discount
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    
                <?php
                    $product_discount = DB::table('discounts')->get();
                ?>
                <accordian :title="'General'" :active="true">
                    <div slot="body">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-6" :class="[errors.has('discount_title') ? 'has-error' : '']">
                                <label class="required" for="discount_title">Discount Title</label>
                                <input type="text" v-validate="'required'" id="discount_title" class="control" name="discount_title" value="{{ $discount->discount_title }}" data-vv-as="Discount Title">
                                <span class="control-error" v-if="errors.has('discount_title')">@{{ errors.first('discount_title') }}</span>
                            </div>
                            <div class="control-group col-md-6" :class="[errors.has('discount_description') ? 'has-error' : '']">
                                <label class="required" for="discount_description">Discount Description</label>
                                <textarea id="discount_description" v-validate="'required'" class="control" name="discount_description" data-vv-as="Discount Description">{{ $discount->discount_description }}</textarea>
                                <span class="control-error" v-if="errors.has('discount_description')">@{{ errors.first('discount_description') }}</span>
                            </div>
                        </div>
                    </div>
                </accordian>

                <accordian :title="'Discount Details'" :active="true">
                    <div slot="body">
                        <div class="check-discount">
                                <div class="control-group col-md-12" :class="[errors.has('dealer_commission') ? 'has-error' : '']">
                                    <span class="col-md-5"><label class="required" for="dealer_commission">Dealer Commission (%)</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <input type="text" v-validate="'required'" id="dealer_commission" class="control" name="dealer_commission" value="{{ $discount->dealer_commission }}" data-vv-as="Dealer Commission">
                                        <span class="control-error" v-validate="'required'" v-if="errors.has('dealer_commission')">@{{ errors.first('dealer_commission') }}</span>
                                    </div>
                                </div>

                                <div class="control-group col-md-12" :class="[errors.has('earn_points') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="earn_points" class="required">Earn Loyalty Points</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <span class="doctor_ck"><input v-validate="'required'" type="radio" class="control" @if($promo_codes->earn_points == 1) {{ 'checked' }} @endif id="earn_points_yes" name="earn_points" value="1"><label for="earn_points_yes">Yes</label></span>
                                        <span class="dealer_ck"><input v-validate="'required'" type="radio" class="control" @if($promo_codes->earn_points == 0) {{ 'checked' }} @endif id="earn_points_no" name="earn_points" value="0"><label for="earn_points_no">No</label></span>
                                        <span class="control-error" v-if="errors.has('earn_points')">@{{ errors.first('earn_points') }}</span>
                                    </div>
                                </div>
                                
                                <div class="control-group col-md-12" :class="[errors.has('discount_usage_limit') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="discount_usage_limit" class="required">How many times a doctor/dealer can use?</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <input type="text" v-validate="'required'" id="discount_usage_limit" data-vv-as="Discount Usage Limit" class="control" name="discount_usage_limit" value="{{ $discount->discount_usage_limit }}">
                                        <span class="control-error" v-if="errors.has('discount_usage_limit')">@{{ errors.first('discount_usage_limit') }}</span>
                                    </div>
                                </div>

                                <div class="control-group col-md-12" :class="[errors.has('discount_per') ? 'has-error' : '']">
                                    <span class="col-md-5">
                                        <label for="select_users" class="required">Select Users</label>
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <select v-validate="'required'" class="control select_users" id="select_users" name="for_all_users" data-vv-as="Select Users">
                                            <option value="1" @if($discount->for_all_users == 1 && $promo_codes->for_whom==0) selected @endif>All</option>
                                            <option value="3" @if($discount->for_all_users == 1 && $promo_codes->for_whom==3) selected @endif>Doctors</option>
                                            <option value="2" @if($discount->for_all_users == 1 && $promo_codes->for_whom==2) selected @endif>Dealers</option>
                                            <option value="0" @if($discount->for_all_users == 0  && $promo_codes->for_whom==0) selected @endif>Custom</option>
                                        </select>
                                    </div>
                                    <span class="control-error" v-if="errors.has('select_users')">@{{ errors.first('select_users') }}</span>
                                </div>
                            </div>


                                <div style="padding:0 35px;">
                                    <div class="table user_table_list" @if($discount->for_all_users == 1) style="display:none;" @endif>
                                        <table class="table">
                                            <thead>
                                                <tr style="height: 65px;">
                                                    <th class="grid_head">Enable</th>
                                                    <th class="grid_head">ID</th>
                                                    <th class="grid_head">Name</th>
                                                    <th class="grid_head">Email</th>
                                                    <th class="grid_head">User type</th>
                                                    <th class="grid_head">Advance Available <br> in account</th>
                                                    <th class="grid_head">Due Amount <br> in account<br></th>
                                                    <th class="grid_head">Total Amount <br>Deposited</th>
                                                    <th class="grid_head">Last deposited date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($credit_users as $user)
                                                    <tr>
                                                        <td data-value="Enable"><input type="checkbox" class="users_list" name="users_list[]" value="{{$user->user_id}}" @if(in_array($user->user_id, $user_discount)) checked @endif></td>
                                                        <td data-value="ID">{{$user->user_id}}</td>
                                                        <td data-value="Name">{{$user->user_name}}</td>
                                                        <td data-value="Email">{{$user->user_email}}</td>
                                                        <td data-value="User type">{{[1=>'Admin', 2=>'Dealer', 3=>'Doctor', 4=>'Store'][$user->user_role]}}</td>
                                                        <td data-value=">Advance Available in account">{{$user->user_credit}}</td>
                                                        <td data-value=">Due Amount in account">{{$user->credit_due}}</td>
                                                        <td data-value="Total amount deposited">{{$user->total_credit}}</td>
                                                        <td data-value="Last deposited date">{{date('d-m-Y',strtotime($user->last_credit))}}</td>
                                                    </tr> 
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <span class="control-error"></span>
                                    </div>
                                </div>



                        <div class="discount-part form-field col-md-12">
                            <div class="discount-section col-md-12">
                                <div class="discounts" id="disount_container"></div>
                                <input type="hidden" name="discount_id" value="{{$discount->id}}">
                                <button type="button" class="add-discount pull-right btn btn-primary" name="add-more"><i class="fa fa-plus"></i></button>
                            </div>
                            
                            <div class="discount-inner col-md-12">
                                <div class="control-group col-md-3" :class="[errors.has('minimum_order_amount') ? 'has-error' : '']">
                                    <label for="minimum_order_amount" class="required">Min Order Total</label>
                                    <input v-validate="'required'" type="text" class="control" id="minimum_order_amount" name="minimum_order_amount" value="{{ $discount->minimum_order_amount}}" data-vv-as="Min Order Total">
                                    <span class="control-error" v-if="errors.has('minimum_order_amount')">@{{ errors.first('minimum_order_amount') }}</span>
                                </div>
                                <div class="control-group col-md-3">
                                    <label for="coupon_code">Discount Code</label>
                                    <input type="text" id="coupon_code" class="control" name="coupon_code" value="{{ $discount->coupon_code }}" data-vv-as="Coupon Code">
                                </div>

                                <div class="control-group col-md-3">
                                    <label for="date_start">Date Start</label>
                                    <date>
                                        <input type="text" class="control date-start" name="date_start" value="{{ $discount->date_start }}">
                                    </date> 
                                </div>
                                <div class="control-group col-md-3">
                                    <label for="date_end">Date End</label>
                                    <date>
                                        <input type="text" class="control date-end" name="date_end" value="{{ $discount->date_end }}">
                                    </date>
                                </div>
                           </div>                            
                        </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/simTree.css')}}">
<script type="text/javascript" src="{{asset('themes/default/assets/js/simTree.js')}}"></script>

<script>
$(document).ready(function () {

    $('body').on('change', '.select_users', function(){
        if($(this).val()==0){
            $('.user_table_list').show();
        }else{
            $('.user_table_list').hide();
        }
    });

    $('body').on('click', '.remove-me', function(){
        if(confirm('Do you like to remove this category?')){
            $(this).closest('.discount-part').remove();

            if(!$('.discount-part').length) addTree();
        }
    });

    const category_tree = '{!! $items !!}';
        const discounts_categories = {!! $all_discount !!};
        let treeCount =0;

        function addTree(dis=null){
            let cat_tree = JSON.parse(category_tree);

            let html_temp = $(`<div class="discount-part form-field col-md-12">
                                        <div class="discount-inner col-md-12">
                                            <div class="accordian active">
                                                <div class="accordian-header">Category Based Discount
                                                    <button type="button" id="remove${treeCount}" class="pull-right remove-me" ><i class="fa fa-times"></i></button>
                                                </div>
                                                <div class="accordian-content">
                                                    <div class="category-part section">
                                                        <div class="categorieTrees" data-id=${treeCount}>
                                                            <input type="hidden" name="discount[${treeCount}][categories]" id="categories${treeCount}" class="categorys_values">
                                                            <div id="categorieTree${treeCount}"></div>
                                                        </div>
                                                    </div>

                                                    <div class="control-group discount-percnt col-md-12">
                                                        <label for="discount_per">Discount (%)</label>
                                                        <input type="text" id="discount_per" class="control" name="discount[${treeCount}][discount_per]" value="${dis?dis.percentage:''}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> `);

            $('#disount_container').append(html_temp);

            cat_tree = cat_tree.map((val)=>{
                if(dis && dis.categorys.includes(val.id)) val.checked = true; 

                if(val.pid) val.pid = treeCount+'-'+val.pid;
                    val.id = treeCount+'-'+val.id;
                    return val;
                });

            if(dis) $(`#categories${treeCount}`).val(dis.categorys.map((val)=> treeCount+'-'+val));
            
            $(`#categorieTree${treeCount}`).simTree({
                data: cat_tree,
                check: true,
                linkParent: true,
                onChange: function(item){
                    const id = $(this['$el']).closest('.categorieTrees').data('id');
                    $(`#categories${id}`).val(item.filter((ele)=>ele.end==true).map((ele)=>ele.id));
                }
            });
            treeCount++;
        }

        function checkValidFields(){
            let $check = true;
            let discounts_container = $('#disount_container');
            discounts_container.find('.error-msg').remove();


            discounts_container.children().each(function(ele){
                if(!$(this).find('.categorys_values').val()){
                    $(this).find('.categorieTrees').append('<span class="error-msg">Please choose any categories in this tree!</span>');
                    $check = false;
                };

                if(! +$(this).find('#discount_per').val()) {
                    $(this).find('.discount-percnt').append('<span class="error-msg">This discount percentage is required!</span>');
                    $check = false;  
                }
            });
            return $check;
        }

        $(".add-discount").click(function(e){
            if(checkValidFields()) addTree();
            e.preventDefault();
        });
        discounts_categories.forEach((dis)=> addTree(dis));

    $('#creditForm').on('submit', function(e){
        if(!checkValidFields()) e.preventDefault();

        let err_msg = $('.user_table_list>.control-error');
        err_msg.text('');
        if($('#select_users').val() == '0'){
            if(!$('form').find('input.users_list:checked').length){
                err_msg.text('Users checkbox is required!');
                e.preventDefault();
            }
        }
    });    
});
</script>
    <style type="text/css">
        .remove-me, .add-discount{
            border: none;
            outline: none !important;
        }

        .remove-me{
            border-radius: 50%;
            background-color: #ff4a4a;
            color: #FFF;
        }

        .add-discount{
            margin-right: 15px;
        }
        .error-msg{
            display: inline-block;
            margin:10px 3px;
            color: #8F0000;
            font-size: 1.5rem;
        }
    </style>
@endpush

@extends('admin::layouts.content')

@section('page_title')
    Payment Approvals - GenXT
@stop

@section('content')
    <div class="content">
        <div class="page-header page-section">
            <div class="page-title">
                <h1>Payment Approvals</h1>
            </div>
        </div>

        <div class="page-content" style="overflow:scroll; cleaer:both">
           @inject('credit_payment_approvals','Webkul\Admin\DataGrids\CreditPaymentApprovals')
            {!! $credit_payment_approvals->render() !!}
        </div>
    </div>

    <div class="modal fade" id="RejectCommentModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reject Comments</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ route('admin.credit_payment.reject')}}">
                        @csrf
                        <input type="hidden" name="credit_id" class="credit_id" value="">
                        <label for="comments" class="label_comments">Enter Comments</label><br>
                        <textarea id="reject-comment" name="reject_comment"></textarea>
                        <div class="text-left mt-20">
                            <input type="submit" class="btn btn-primary" value="Submit" >
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')

    <script type="text/javascript">
        $('document').ready(function(){

            $('body').find('.reject-comment').on('click', function(event){
                var credit_id = $(this).data('credit_id');
                $('#RejectCommentModal').find(".credit_id").val(credit_id);
            }); 


            $('body').find('.approve').on('click', function(event){
                var x = confirm("Are you sure you want to approve this payment?");
                if(x)
                    return true;
                else
                    return false;
            });
            function imgClickFunction(e){
                e.preventDefault();
                var src = $(this).attr('src') ?? $(this).attr('href');
                var modal;
                function removeModal(){ modal.remove(); $('body').off('keyup.modal-close'); }
                modal = $('<div>').css({
                    background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
                    backgroundSize: 'contain',
                    width:'100%', height:'100%',
                    position:'fixed',
                    zIndex:'10000',
                    top:'0', left:'0',
                    cursor: 'pointer'
                }).click(function(){
                    removeModal();
                }).appendTo('body');


                $('body').on('keyup.modal-close', function(e){
                    if(e.key==='Escape'){
                        removeModal();
                    }
                });
            }
            $('.img-enlargeable').click(imgClickFunction);        
        });
    </script>
@endpush

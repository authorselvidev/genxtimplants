@extends('admin::layouts.content')

@section('page_title')
    Show Dealer Commission
@stop

@section('content')
<div class="content">
    <div class="page-header">
        <div class="page-title">
            <h1>
                <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dealers') }}';"></i>
                        Dealer Commission
            </h1>
        </div>            
    </div>

    <div class="page-content">
        <div class="dashboard">
      <div class="dashboard-stats">

                <div class="dashboard-card">
                    <div class="title">
                        Total Commisson Paid
                    </div>

                    <div class="data">
                        <i class="fa fa-inr"></i> {{ $total_comm_paid }}

                        
                    </div>
                </div>
                <div class="dashboard-card">
                    <div class="title">
                        Dealer Commission(%)
                    </div>

                    <div class="data">
                        {{ $dealer->dealer_commission }} %

                        
                    </div>
                </div>
                 <div class="dashboard-card">
                    <div class="title">
                        Total doctors under this dealer
                    </div>

                    <div class="data">
                        {{ count($doctors_under_dealer) }}

                        
                    </div>
                </div>
                <div class="dashboard-card">
                    <div class="title">
                        Total Orders by the doctors under this dealer
                    </div>

                    <div class="data">
                        {{ count($doctor_orders) }}

                        
                    </div>
                </div>
                <div class="dashboard-card">
                    <div class="title">
                        Total Order values by the doctors under this dealer
                    </div>

                    <div class="data">
                        <i class="fa fa-inr"></i> {{ $total_orders_value }}

                        
                    </div>
                </div>
                <div class="dashboard-card">
                    <div class="title">
                        Balance Commission to pay
                    </div>

                    <div class="data">
                        <i class="fa fa-inr"></i> {{ $balance_commission_to_pay }}

                        
                    </div>
                </div>
                
                
            </div>
</div>

<div class="section table">
    <h3>Doctor Orders</h3>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>No. of products</th>
                                            <th>Doctor Name</th>
                                            <th>Order Total (<i class="fa fa-inr"></i>)</th>
                                            <th>Commission Paid (<i class="fa fa-inr"></i>)</th>
                                            <th>Commission Paid Date</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($doctor_orders as $doctor_order)
                                        <?php $no_of_products = DB::table('order_items')->where('order_id',$doctor_order->id)->count();
                                        $dealer_comm = DB::table('dealer_commission')->where('order_id',$doctor_order->id)->first(); ?>
                                        @if(isset($dealer_comm))
                                            <tr>
                                                <td>
                                                    #{{ $doctor_order->id }}
                                                </td>
                                                <td>
                                                    {{ $no_of_products }}
                                                </td>
                                                <td>
                                                    {{ Webkul\Customer\Models\Customer::CustomerName($doctor_order->customer_id) }}
                                                </td>
                                                <td>
                                                    {{ $doctor_order->grand_total}}
                                                </td>
                                                <td>
                                                    {{ $dealer_comm->commission_paid_amount }}
                                                </td>
                                                <td>
                                                    {{ date('F j, Y', strtotime($dealer_comm->created_at)) }}
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>

                                </table>
		</div>
<!-- /.panel -->
        

@stop
@push('scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(e){
$(".profile-pic").click(function() {
    $("#upload-profile").click();
});

$("input[type='file']").change(function () {
var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'upload-profile')
                reader.onload = profileUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });


function profileUploaded(e) {
    $('.profile-pic').attr('src', e.target.result);
};
});
</script>


@endpush
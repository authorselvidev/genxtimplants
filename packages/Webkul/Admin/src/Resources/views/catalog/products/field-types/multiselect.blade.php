<?php $get_options_id = DB::table('product_attribute_values')
									->where('product_id', $product->id)
									->where('attribute_id',$attribute->id)
									->pluck('text_value')->toArray();
?>
<select v-validate="'{{$validations}}'" class="control" id="{{ $attribute->code }}" name="{{ $attribute->code }}[]" data-vv-as="&quot;{{ $attribute->admin_name }}&quot;" multiple {{ $disabled ? 'disabled' : '' }}>

    @foreach ($attribute->options as $option)
        <option value="{{ $option->id }}" {{ in_array($option->id, $get_options_id) ? 'selected' : ''}}>
            {{ $option->admin_name }}
        </option>
    @endforeach

</select>

@extends('admin::layouts.content')

@section('page_title')
    Show Product
@stop

@section('content')
    <div class="content">
        <?php $locale = request()->get('locale') ?: app()->getLocale(); ?>
        <?php $channel = request()->get('channel') ?: core()->getDefaultChannelCode(); ?>


            <div class="page-header">

                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                        Show Product
                    </h1>
                </div>

                
            </div>

            <div class="page-content">
                <?php $doctor_groups = DB::table('user_groups')->get(); 
                $product_discount = DB::table('discounts')->where('product_id', $product->id)->get();
               ?>
                @foreach ($product->attribute_family->attribute_groups as $attributeGroup)
                   @if($attributeGroup->id == 6)
                    <accordian :title="'{{ __($attributeGroup->name) }}'" :active="true">

                        <div slot="body">
                            <div class="discount-section">
                            <?php if(count($product_discount) > 0) {
                                foreach($product_discount as $pr_key => $discount){ ?>
                                
                            <div id="discount{{$pr_key}}" class="discount-part form-field col-md-12">
                                <div class="discount-inner col-md-12">
                                <div class="control-group col-md-2">
                                    <label for="doctor_group"> Doctor Groups</label>
                                    <select v-validate="'{{$validations}}'" class="control doctor_group" id="doctor_group" name="doctor_group[]">
                                        @foreach($doctor_groups as $key => $doctor_group)
                                            <option <?php if($discount->customer_group_id == $doctor_group->id) echo 'selected'; ?> value="{{ $doctor_group->id }}">{{ $doctor_group->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="control-group col-md-2">
                                    <label for="select_doctors">Select Doctors</label>
                                    <select class="control select_doctors" id="select_doctors" name="for_all_users[]">
                                        <option <?php if($discount->for_all_users == 1) echo 'selected'; ?> value="1">All</option>
                                        <option <?php if($discount->for_all_users == 0) echo 'selected'; ?> value="0">Custom</option>
                                    </select>
                                </div>
                                <div class="control-group col-md-1">
                                    <label for="discount_qty">Quantity</label>
                                    <input type="text" class="control" id="discount_qty" name="discount_qty[]" value="{{ $discount->quantity }}">
                                </div>
                                <div class="control-group col-md-2">
                                    <label for="discount_per">Discount (%)</label>
                                    <input type="text" id="discount_per" class="control" name="discount_per[]" value="">
                                </div>
                                <div class="control-group col-md-2">
                                    <label for="date_start">Date Start</label>
                                    <date>
                                        <input type="text" class="control date-start" name="date_start[]" value="{{ $discount->date_start }}">
                                    </date> 
                                </div>
                                <div class="control-group col-md-2">
                                    <label for="date_end">Date End</label>
                                    <date>
                                        <input type="text" class="control date-end" name="date_end[]" value="{{ $discount->date_end }}">
                                    </date>
                                </div>
                                @if($pr_key !=0)
                                <div class="control-group col-md-1">
                                    <button id="remove'{{$pr_key}}'" class="btn pull-right btn-danger remove-me" ><i class="fa fa-minus"></i></button>
                                </div>
                                @endif
                            </div>
                            <textarea class="doctors_by_groups"></textarea>
                            

                            </div>
                        <?php } 
                        }
                        else { ?>

                            <div id="discount0" class="discount-part form-field col-md-12">
                                <div class="discount-inner col-md-12">
                                <div class="control-group col-md-2">
                                    <label for="doctor_group"> Doctor Groups</label>
                                    <select v-validate="'{{$validations}}'" class="control doctor_group" id="doctor_group" name="doctor_group[]">
                                        @foreach($doctor_groups as $key => $doctor_group)
                                            <option value="{{ $doctor_group->id }}">{{ $doctor_group->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="control-group col-md-2">
                                    <label for="select_doctors">Select Doctors</label>
                                    <select class="control select_doctors" id="select_doctors" name="select_doctors[]">
                                        <option value="0">All</option>
                                        <option value="1">Custom</option>
                                    </select>
                                </div>
                                <div class="control-group col-md-1">
                                    <label for="discount_qty">Quantity</label>
                                    <input type="text" class="control" id="discount_qty" name="discount_qty[]" value="">
                                </div>
                                <div class="control-group col-md-2">
                                    <label for="discount_per">Discount (%)</label>
                                    <input type="text" id="discount_per" class="control" name="discount_per[]" value="">
                                </div>
                                <div class="control-group col-md-2">
                                    <label for="date_start">Date Start</label>
                                    <date>
                                        <input type="text" class="control date-start" name="date_start[]" value="">
                                    </date> 
                                </div>
                                <div class="control-group col-md-2">
                                    <label for="date_end">Date End</label>
                                    <date>
                                        <input type="text" class="control date-end" name="date_end[]" value="">
                                    </date>
                                </div>
                            </div>
                            <textarea class="doctors_by_groups"></textarea>
                            

                            </div>
                        <?php }?>
                            <div class="doctor-sec">
                                <?php
                                foreach($doctor_groups as $key => $each_group)
                                {
                                    if($each_group->id == 1)
                                        $list_doctors = DB::table('users')->where('role_id',3)->get();
                                    else
                                        $list_doctors = DB::table('users')->where('role_id',3)->where('customer_group_id',$each_group->id)->get();
                                    $group_mem = array();
                                    $full_name='';
                                    foreach($list_doctors as $key => $list_doctor){
                                        $full_name = $list_doctor->first_name.' '.$list_doctor->last_name;
                                        $group_mem[] = array('id' => $list_doctor->id, 'name' => $full_name);
                                    }
                                    $all_group[] = $group_mem;
                                    echo '<ul class="doctor-grp" id="doctor-group-'.$each_group->id.'"></ul>';
                                } 
                                //dd($all_group);
                                ?>
                                </div>
                        </div>
                            <div class="control-group">
                                <button class="add-discount pull-right btn btn-primary" name="add-more"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </accordian>
                   @else
                    @if (count($attributeGroup->custom_attributes))

                        <accordian :title="'{{ __($attributeGroup->name) }}'" :active="true">
                            <div slot="body">
                                <?php $custom_group = $attributeGroup->custom_attributes->sortBy('position'); ?>
                                <div class="form-fields col-md-12">
                                    <div class="form-field col-md-12">
                                @foreach ($custom_group as $attribute)

                                    @if (! $product->super_attributes->contains($attribute))
                                        <?php
                                            $validations = [];
                                            $disabled = false;
                                            if ($product->type == 'configurable' && in_array($attribute->code, ['price', 'cost', 'special_price', 'special_price_from', 'special_price_to', 'width', 'height', 'depth', 'weight'])) {
                                                if (! $attribute->is_required)
                                                    continue;

                                                $disabled = true;
                                            } else {
                                                if ($attribute->is_required) {
                                                    array_push($validations, 'required');
                                                }

                                                if ($attribute->type == 'price') {
                                                    array_push($validations, 'decimal');
                                                }

                                                array_push($validations, $attribute->validation);
                                            }

                                            $validations = implode('|', array_filter($validations));
                                        ?>
                                        <?php //print_r($attribute->code); //dd($attribute); echo '<br><br>'; ?>
                                        @if (view()->exists($typeView = 'admin::catalog.products.field-types.' . $attribute->type))

                                            <div class="control-group col-md-6 {{ $attribute->type }}" :class="[errors.has('{{ $attribute->code }}') ? 'has-error' : '']">
                                                <label for="{{ $attribute->code }}">
                                                    {{ $attribute->admin_name }}

                                                    @if ($attribute->type == 'price')
                                                        <span class="currency-code">({{ core()->currencySymbol(core()->getBaseCurrencyCode()) }})</span>
                                                    @endif

                                                    <?php
                                                        $channel_locale = [];
                                                        if ($attribute->value_per_channel) {
                                                            array_push($channel_locale, $channel);
                                                        }

                                                        if ($attribute->value_per_locale) {
                                                            array_push($channel_locale, $locale);
                                                        }
                                                    ?>

                                                    @if (count($channel_locale))
                                                        <span class="locale">[{{ implode(' - ', $channel_locale) }}]</span>
                                                    @endif
                                                </label>
                                                
                                                {{ $product[$attribute->code] }}
                                                <!-- @include ($typeView) -->

                                                <span class="control-error" v-if="errors.has('{{ $attribute->code }}')">@{{ errors.first('{!! $attribute->code !!}') }}</span>
                                            </div>

                                        @endif

                                    @endif

                                @endforeach
                            </div>
                            </div>
                            </div>
                        </accordian>
                    @endif
                    @endif
                @endforeach
               <!--  -->

            </div>

    </div>
@stop

@push('scripts')
    <script src="{{ asset('vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#channel-switcher, #locale-switcher').on('change', function (e) {
                $('#channel-switcher').val()
                var query = '?channel=' + $('#channel-switcher').val() + '&locale=' + $('#locale-switcher').val();

                window.location.href = "{{ route('admin.catalog.products.edit', $product->id)  }}" + query;
            })

            tinymce.init({
                selector: 'textarea#description, textarea#short_description',
                height: 200,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true
            });


            /*****discount section****/
            var count = '{{ count($product_discount) }}';
            if(count == 0) count = 1;
            $(".add-discount").click(function(e){
                e.preventDefault();
                var clone=$('#discount0').clone();
                clone.attr('id', 'discount' + count).appendTo(".discount-section");
                clone.find('.discount-inner').append('<div class="control-group col-md-1"><button id="remove'+count+'" class="btn pull-right btn-danger remove-me" ><i class="fa fa-minus"></i></button></div>')
                clone.find('.date-start').val('').attr('id','date-start-'+count);
                clone.find('.date-end').val('').attr('id','date-end-'+count);
                flatpickr("#date-start-"+count);
                flatpickr("#date-end-"+count);
                count = count + 1;
            });

            $('body').on('click', '.remove-me', function(){
                $(this).parent().parent().parent().remove();
            });

            /*$('body').on('focus','.date-start', function(){
                flatpickr(".date-start", {
                "dateFormat":"n/j/Y"
                });
            });
            $('body').on('focus','.date-end', function(){
                flatpickr(".date-end", {
                "dateFormat":"n/j/Y"
                });
            });*/


$('body').find('.select_doctors').each(function(){
    var parent_row = $(this).parent().parent();
    var doctor_group = parent_row.find('.doctor_group :selected').val();
    parent_row.parent().find('.doctor-grp').removeClass('active')
    if($(this).val() == 1){
        parent_row.parent().find('#doctor-group-'+doctor_group).addClass('active')
    }

})
    

$('body').on('change','.select_doctors', function(e){
    var parent_row = $(this).parent().parent();
    var doctor_group = parent_row.find('.doctor_group :selected').val();
    parent_row.parent().find('.doctor-grp').removeClass('active')
    if($(this).val() == 1){
        parent_row.parent().find('#doctor-group-'+doctor_group).addClass('active')
    }
});


$('body').on('change','.doctor_group', function(e){
    var parent_row = $(this).parent().parent();
    var select_doctors = parent_row.find('.select_doctors :selected').val();
    parent_row.parent().find('.doctor-grp').removeClass('active');
    if(select_doctors == 1)
         parent_row.parent().find('#doctor-group-'+$(this).val()).addClass('active');
        });
});
    </script>
@endpush
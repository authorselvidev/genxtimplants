@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.catalog.products.title') }}
@stop

@section('content')
    <div class="content" style="height: 100%;">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.catalog.products.title') }}</h1>
            </div>
            
            <div class="page-action">
                @if(auth()->guard('admin')->user()->role_id != 4)
                <div class="export" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>
                @endif
                <a href="{{ route('admin.catalog.products.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.catalog.products.add-product-btn-title') }}
                </a>
            </div>
            
        </div>

        <div class="page-content">
            {{-- @inject('product','Webkul\Admin\DataGrids\ProductDataGrid')
            {!! $product->render() !!} --}}

            @inject('products', 'Webkul\Admin\DataGrids\ProductDataGrid')
            {!! $products->render() !!}

            @inject('products_export', 'Webkul\Admin\DataGrids\ProductDataGrid')
        </div>
    </div>
    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>

       <div class="modal fade" id="UpdateQuantityInfo" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Product Quantity</h4>
        </div>
        <div class="modal-body col-md-12">
            <div class="change-order-status col-md-12">
                {{-- {{ route('admin.update.product.quantity', $product->id)}} --}}

                 <form method="post" action="" onsubmit="return confirm('Are you sure you want to update product quantity?');">
                    @csrf
                    <h3>Product Information</h3>
                    <div class="update-qty-info col-md-12">
                            <div class="">
                                <div class="form-field section-content col-md-12">
                                    <div class="show-qty col-md-6"><img src="{{ bagisto_asset('/images/loading.gif') }}"></div>
                                    <div class="control-group col-md-6">
                                         <input type="submit" name="change_quantity" class="btn btn-primary change-qty" value="Update" >
                                    </div>
                            </div>
                        </div>
                        </div>
                   
                </form> 
            </div>
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>

@stop
@push('scripts')

<script type="text/x-template" id="export-form-template">
    <form method="POST" action="{{ route('admin.datagrid.export') }}">

        <div class="page-content">
            <div class="form-container">
                @csrf()

                <?php
                    $products_export->disablePaginate();
                    $products_export->render();
                    $data = json_encode((array) $products_export);
                ?>

                <input type="hidden" name="gridData" value="{{ $data }}">
                <input type="hidden" name="file_name" value="Product">

                <div class="control-group">
                    <label for="format" class="required">
                        {{ __('admin::app.export.format') }}
                    </label>
                    <select name="format" class="control" v-validate="'required'">
                        <option value="xls">{{ __('admin::app.export.xls') }}</option>
                        <option value="csv">{{ __('admin::app.export.csv') }}</option>
                    </select>
                </div>

            </div>
        </div>

        <button type="submit" class="btn btn-lg btn-primary" @click="closeModal">
            {{ __('admin::app.export.export') }}
        </button>

    </form>
</script>

<script>
    Vue.component('export-form', {
        template: '#export-form-template',
        methods: {
            closeModal () {
                this.$parent.closeModal();
            }
        }
    });

    $(document).ready(function(){
        $('body').find('.update-qty').on('click', function(event){
              var product_id = $(this).data('product_id');
              $('body').find('.show-qty').html("<img src='{{ bagisto_asset('/images/loading.gif') }}'>");
             $.ajax({
                type:'get',
                url: '{{url("/admin/catalog/products/")}}'+'?product_id='+product_id,
                success: function(response){
                    $('body').find('.show-qty').html(response);
                }
            });
          
            var qty_route = "{{ route('admin.update.product.quantity', ':product_id')}}"
            qty_route = qty_route.replace(':product_id', product_id);
            $('#UpdateQuantityInfo').find('form').attr('action',qty_route);
            var quantity = $(this).data('quantity');
            $('#UpdateQuantityInfo').find(".quantity").val(quantity);
        }); 
    });
</script>


@endpush
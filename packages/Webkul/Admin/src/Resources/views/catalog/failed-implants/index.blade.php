@extends('admin::layouts.content')

@section('page_title')
    Return Implants
@stop

@section('content')
    <div class="content" style="height: 100%;">
        <div class="page-header">
            <div class="page-title">
                <a href="{{route('admin.catalog.failed-implants.index')}}"><i class="icon angle-left-icon back-link"></i></a>
                <h1>Return Implants
                <?php
                    $currentRoute = \Route::current()->getName();
                    if($currentRoute == 'admin.catalog.failed-implants.approval'){
                        echo "Approval";
                    }else if($currentRoute == 'admin.catalog.failed-implants.rejected'){
                        echo "Rejected";                        
                    }
                ?>
                </h1>
            </div>
        </div>

        <div class="tab">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation">
                    <a href="{{route('admin.catalog.failed-implants.index')}}" class="btn btn-lg btn-primary <?php
                        echo ($currentRoute == 'admin.catalog.failed-implants.index')?'btn-active':'';
                    ?>">All</a>
                </li>
                <li role="presentation">
                    <a href="{{route('admin.catalog.failed-implants.approval')}}" class="btn btn-lg btn-primary <?php
                        echo ($currentRoute == 'admin.catalog.failed-implants.approval')?'btn-active':'';
                    ?>">Waiting for approval</a>
                </li>
                <li role="presentation">
                    <a href="{{route('admin.catalog.failed-implants.rejected')}}" class="btn btn-lg btn-primary <?php
                        echo ($currentRoute == 'admin.catalog.failed-implants.rejected')?'btn-active':'';
                    ?>">Rejected</a>
                </li>
            </ul>
        </div><!--tabls-->

        <div class="page-content">

            @inject('failedImplants', 'Webkul\Admin\DataGrids\FailedImplantsDataGrid')
            {!! $failedImplants->addMyFilter($myFilter) !!}

            {!! $failedImplants->render() !!}
        </div>

            <div class="modal fade" id="changeFailedImplantStatus" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content col-md-12">
                    <div class="modal-header col-md-12">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Change Return Implant Status</h4>
                    </div>
                    <div class="modal-body col-md-12">
                        <div class="col-md-12">
                            <form method="post" action="{{ route('admin.catalog.failed-implants.update')}}" id="failute_implants_status">
                                @csrf
                                <input type="hidden" name="failed_id" class="failed_id" value="">
                                <table class="dialog_table">
                                    <tr>
                                        <td>
                                            <span class="input-area">
                                                <input type="radio" id="Approved" name="failed_status" class="failedstatus" value="Approved">
                                                <label for="Approved">Approved</label>
                                            </span>
                                        </td>
                                        <td>
                                            <span class="input-area">
                                                <input type="radio" id="Rejected" name="failed_status" class="failedstatus" value="Rejected">
                                                <label for="Rejected">Rejected</label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="input-area">
                                                <input type="radio" id="Processing" name="failed_status" class="failedstatus" value="Processing">
                                                <label for="Processing">Processing</label>
                                            </span>
                                        </td>
                                        <td>
                                            <span class="input-area">
                                                <input type="radio" id="Completed" name="failed_status" class="failedstatus" value="Completed">
                                                <label for="Completed">Completed</label>
                                            </span>
                                        </td>
                                    </tr>
                                </table>

                                <label for="comments" class="label_comments">GenXT Comments:</label><br>
                                <textarea id="comments" name="comments" maxlength="191"></textarea>
                                
                                <label for="comments" class="label_comments refund_class">Refund Amount:</label><br>

                                <input type="number" name="refund_amount" id="refund_amount" value="0">

                                <div class="text-center mt-20">
                                    <input type="submit" class="btn btn-primary change-status" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer col-md-12">
                      <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
                    </div>
                  </div>
                </div>
              </div>
    </div>
@stop

@push('scripts')
    <style type="text/css">
        .mt-20{
            margin-top:20px;
        }
        .modal-dialog{
            max-width: 400px;
            width: 80%;
            margin: 20px auto;
        }

        .dialog_table{
            margin: 0 auto;
        }

        .dialog_table td{
            padding: 2px 5px;
            text-align: left;
        }

        .modal-body form{
            text-align: left;
        }

        .input-area {
            display: inline-block;
        }

        .btn-active{
            background-color: #1ED1BE !important;
            color: #FFF !important;
        }
        #comments{
            width: 100%;
            padding: 5px;
        }

        .label_comments{
            margin-top:10px;
            margin-bottom: 0px !important;            
        }

        .class-error{
            color:rgb(141, 0, 0);
        }
    </style>


    <script>
            $(document).on('submit','#failute_implants_status', function(e){
            let status = true;            
            if(!($(this).find('input[name="failed_status"]:checked').length)){
                $(this).find('.dialog_table').append(`<span class="class-error">This field is required</span>`);
                status = false;
            }

            if(status && confirm('Are you sure you want to change Return implant status?')){
                $('.change-status').prop('disabled', true);
            }else{
                status = false;
            }

            if(!status) e.preventDefault();
        });
        
        
        $(document).ready(function(){
            $('form').on('change', 'input[type="radio"]', function(e){
                let target = $(e.target);
                let root_form = target.closest('form');
                let ele_ref = root_form.find('#refund_amount');
                if(target.is('#Completed')){
                    ele_ref.show();
                    root_form.find('.refund_class').show();
                }else{
                    ele_ref.hide();
                    root_form.find('.refund_class').hide();
                    ele_ref.val(0);
                }
                //
            });

            $('body').find('.update-status').on('click', function(event){
                let update_id = $(this).data('update_id');
                $('#changeFailedImplantStatus').find(".failed_id").val(update_id);
                let status = $(this).data('status');
                $('input#'+status).prop("checked", true);
                if(status =='Completed'){
                    $('#changeFailedImplantStatus .refund_class, #changeFailedImplantStatus #refund_amount ').show();
                }else{
                    $('#changeFailedImplantStatus .refund_class, #changeFailedImplantStatus #refund_amount ').hide();
                }
            });
        });
</script>
@endpush


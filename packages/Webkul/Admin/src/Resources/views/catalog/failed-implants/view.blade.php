
@extends('admin::layouts.master')

@section('page_title')
    {{'Return Implant #'.$failed_implant->id}}
@stop

@section('content-wrapper')
    <div class="content full-page">

        <div class="page-header">

            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                    {{ 'Return Implant #'.$failed_implant->id }}
                </h1>
            </div>

        </div>

        <div class="page-content ">
            <?php
                $status = array('0' => 'False', '1' => 'True');
            ?>
            <div class="sale-container">
                <tabs class="nav-tabs">
                    <tab name="Order Details" :selected="true">
                        <div class="col-md-8">
                            <table>
                                <h4 style="font-weight: bold">Order Details</h4>
                                <tr>
                                    <td class="title">User's Name</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$user->my_title.' '.$user->first_name.' '.$user->last_name}}
                                </td>
                                </tr>
                                <tr>
                                    <td class="title">Email</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$user->email}}</td>
                                </tr>
                                <tr>
                                    <td class="title">Mobile Number</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$user->phone_number}}</td>
                                </tr>                                
                                <tr>
                                    <td class="title">User Type</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{array(
                                        '1' => 'Admin',
                                        '2' => 'Dealer',
                                        '3' => 'Doctor',
                                        '4' => 'Store Manager')[$user->role_id]}}</td>
                                </tr>
                                <tr>
                                    <td class="title">Membership group</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{Webkul\Customer\Models\CustomerGroup::CustomerGroupName($user->customer_group_id)}}</td>
                                </tr>
                                @if(!empty($user->dealer_id))
                                    <tr>
                                        <td class="title">Dealer Name</td>
                                        <td class="text-center">:</td>
                                        <?php $dealer = DB::table('users')->where('id',$user->dealer_id)->first(); ?>
                                        <td class="value">{{ $dealer->my_title.' '.$dealer->first_name.' '.$dealer->last_name }}</td>
                                    </tr>
                                @endif

                                <tr>
                                    <td class="title">Detailed Clinic Address</td>
                                    <td class="text-center">:</td>
                                    <td class="value"> <span>{{ $user->clinic_address }}</span> </td>
                                </tr>
                                <tr>
                                    <td class="title">GenXT Implant Product Code(or label)</td>
                                    <td class="text-center">:</td>
                                    <td class="value"> <span>{{ $failed_implant->implant_code }}</span> </td>
                                </tr>
                                <tr>
                                    <td class="title">Return Implant status</td>
                                    <td class="text-center">:</td>
                                    <td class="value">
                                        {{$failed_implant->status}}
                                        @if($failed_implant->status!="Completed" && $failed_implant->status!="Rejected")
                                            <a class="pull-right update-status" data-toggle="modal" data-status="{{$failed_implant->status}}" data-update_id = "{{$failed_implant->id }}" data-target="#changeFailedImplantStatus" href="">Change</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="title">Item Status</td>
                                    <td class="text-center">:</td>
                                    <td class="value"> <span>{{($failed_implant->item_status==0)?'Used':'Unused'}}</span> </td>
                                </tr>
                                <tr>
                                    <td class="title">GenXT Comments</td>
                                    <td class="text-center">:</td>
                                    <td class="value"> <span>{{$failed_implant->comments}}</span> </td>
                                </tr>
                                <tr>
                                    <td class="title">Replacement Reason</td>
                                    <td class="text-center">:</td>
                                    <td class="value"> <span>{{$failed_implant->replacement_reason}}</span> </td>
                                </tr>
                                <tr>
                                    <td class="title">Order Mode</td>
                                    <td class="text-center">:</td>
                                    <td class="value">
                                         {{($failed_implant->order_type =='on')?'Online':'Offline'}}
                                    </td>
                                </tr>

                                <?php /*
                                @if($failed_implant->order_type=="on")
                                    <tr>
                                        <td class="title">Order ID</td>
                                        <td class="text-center">:</td>
                                        <td class="value">
                                            <a href="{{route('admin.sales.orders.view', ['id'=>$order->id])}}" target="_blank">
                                                #{{ $order->id }}
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title">Item Name</td>
                                        <td class="text-center">:</td>
                                        <td class="value">
                                            {{ $order_items->name }}
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td class="title">Order Number:</td>
                                        <td class="text-center">:</td>
                                        <td class="value">
                                            {{ $failed_implant->order_number }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title">Item Name</td>
                                        <td class="text-center">:</td>
                                        <td class="value">
                                            {{ $failed_implant->order_item }}
                                        </td>
                                    </tr>
                                @endif

                                <?php 
                                    $product = \DB::table('product_flat')
                                        ->where('product_id', $failed_implant->replacement_product_id)
                                        ->first();                                
                                ?>
                                <tr>
                                    <td class="title">Replacement Item</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$product->name ?? ''}}</td>
                                </tr>
                                <tr>
                                    <td class="title">Replacement Reason</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->replacement_reason}}</td>
                                </tr>
                                <tr>
                                    <td class="title">Order Quantity</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->qty}}</td>
                                </tr>
                                <tr>
                                    <td class="title">Replace Quantity</td>
                                    <td class="text-center">:</td>
                                    @if($failed_implant->order_type=='on')
                                        <td class="value">{{$failed_implant->replace_qty}}</td>
                                    @else
                                        <td class="value">{{$failed_implant->replace_offline_qty}}</td>
                                    @endif
                                </tr>
                                @if($failed_implant->order_type=="on")
                                    <tr>
                                        <td class="title">Lot Number</td>
                                        <td class="text-center">:</td>
                                        <td class="value">
                                            <?php
                                                $lot = ($invoice_item_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_item_id->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get()):[];
                                            ?>

                                            @if(count($lot)>0)
                                                @foreach($lot as $key => $lot_item)
                                                    {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}} ,
                                                    {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}} ,
                                                    {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
                                                @endforeach
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td class="title">Product Code(or label)</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->implant_code}}</td>
                                </tr>


                                */ ?>

                                <tr>
                                    <td class="title">LOT #</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->lot}}</td>
                                </tr>
                                
                            </table>

                            <hr>
                            <h4 style="font-weight: bold">Return Items</h4>
                            @if($failed_implant->order_type =='on')
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Order ID</th>
                                            <th scope="col">Return Product</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Lot Number</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($return_implants_return as $impnt)
                                            <tr>
                                                <td scope="row">#{{$impnt->order}}</td>
                                                <?php
                                                    $order_item = DB::table('order_items')->where('id', $impnt->item)->first();
                                                    $invoice_items_id = DB::table('invoice_items')->where('order_item_id', $order_item->id)->first();
                                                    $lot_items = ($invoice_items_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_items_id->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get()):[];
                                                ?>
                                                
                                                <td>{{ $order_item->name }}</td>
                                                <td>{{$impnt->quantity}}</td>
                                                <td>
                                                    @if(count($lot_items)>0)
                                                        @foreach($lot_items as $key => $lot_item)
                                                            {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}},
                                                            {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}},
                                                            {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
                                                        @endforeach
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>   
                                        @endforeach                                                                     
                                    </tbody>                                    
                                </table>
                            @else
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Order number</th>
                                            <th scope="col">Return Product</th>
                                            <th scope="col">Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($return_implants_return as $impnt)
                                            <tr>
                                                <td scope="row">#{{$impnt->order}}</td>                                                
                                                <td>{{ $impnt->item }}</td>
                                                <td>{{$impnt->quantity}}</td>
                                            </tr>   
                                        @endforeach                                                                     
                                    </tbody>                                    
                                </table>
                            @endif
                            <hr>
                            <h4 style="font-weight: bold">Replacement Items</h4>
                            
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Replacement Item</th>
                                        <th scope="col">Replace Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    @foreach ($return_implants_replace as $impnt)
                                        <tr>
                                            <td scope="row">{{  \DB::table('product_flat')->where('product_id',$impnt->item)->value('name')}}</td>
                                            <td>{{$impnt->quantity}}</td>
                                        </tr>   
                                    @endforeach                                                                     
                                </tbody>                                    
                            </table>



                        </div>
                    </tab>
                    <?php /*@if($failed_implant->item_status==0 && $failed_implant->is_lessper ==1)
                        <tab name="Return Details">
                            <div class="col-md-8">
                                <table>
                                    <tr>
                                        <td class="title">Bone Type</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->bone_type}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Flapless</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->flapless]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Immediate Implant</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->immediate_implant]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Immediate Load</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->immediate_load]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Implantation Date</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{date('Y-m-d', strtotime($failed_implant->implant_date))}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Implant Removal Date</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{date('Y-m-d', strtotime($failed_implant->implant_removal_date))}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Implant Removal Location</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->remove_location}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Reasons for Return</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->reason}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Other reason for Failure<br>/ Case Outcome</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->other_reason_failure}}</td>
                                    </tr>
                                </table>
                            </div>
                        </tab>

                        <tab name="History Details">
                            <div class="col-md-8">
                                <table>
                                    <tr>
                                        <td class="title">Age</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->patient_age}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Gender</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->gender}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Normal</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->normal_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Smoker</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->smoker_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Hypertension</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->hypertension_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Cardiac Problems</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->cardiac_problems_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Diabetes</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->diabetes_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Alcoholism</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->alcoholism_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Trauma</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->trauma_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Cancer</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->cancer_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Others (Specify)</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->others}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Xray of the implant when placed</td>
                                        <td class="text-center">:</td>
                                        <td class="value"><form method="POST" action="{{route('admin.catalog.failed-implants.download')}}">
                                            @method("POST")
                                            @csrf
                                            <input type="hidden" name="type" value="before">
                                            <input type="hidden" name="file" value="{{$failed_implant->xray_implant}}">
                                            <button type="submit" class="btn btn-sm btn-primary">
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                                <span>&nbsp;Download</span>
                                            </button>
                                        </form></td>
                                    </tr>
                                    <tr>
                                        <td class="title">Xray showing the failed implant,<br>just before the implant is removed</td>
                                        <td class="text-center">:</td>
                                        <td class="value"><form method="POST" action="{{route('admin.catalog.failed-implants.download')}}">
                                            @method("POST")
                                            @csrf
                                            <input type="hidden" name="type" value="after">
                                            <input type="hidden" name="file" value="{{$failed_implant->before_implant_removed}}">
                                            <button type="submit" class="btn btn-sm btn-primary">
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                                <span>&nbsp;Download</span>
                                            </button>
                                        </form></td>
                                    </tr>
                                </table>
                            </div>
                        </tab>
                        @endif
                    */ ?>
                </tabs>
            </div>
        </div>
    </div>


     <div class="modal fade" id="changeFailedImplantStatus" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content col-md-12">
                    <div class="modal-header col-md-12">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Change Return Implant Status</h4>
                    </div>
                    <div class="modal-body col-md-12">
                        <div class="col-md-12">
                            <form method="post" action="{{ route('admin.catalog.failed-implants.update')}}" id="failute_implants_status">
                                @csrf
                                <input type="hidden" name="failed_id" class="failed_id" value="">
                                <table class="dialog_table">
                                    <tr>
                                        <td>
                                            <span class="input-area">
                                                <input type="radio" id="Approved" name="failed_status" class="failedstatus" value="Approved">
                                                <label for="Approved">Approved</label>
                                            </span>
                                        </td>
                                        <td>
                                            <span class="input-area">
                                                <input type="radio" id="Rejected" name="failed_status" class="failedstatus" value="Rejected">
                                                <label for="Rejected">Rejected</label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="input-area">
                                                <input type="radio" id="Processing" name="failed_status" class="failedstatus" value="Processing">
                                                <label for="Processing">Processing</label>
                                            </span>
                                        </td>
                                        <td>
                                            <span class="input-area">
                                                <input type="radio" id="Completed" name="failed_status" class="failedstatus" value="Completed">
                                                <label for="Completed">Completed</label>
                                            </span>
                                        </td>
                                    </tr>
                                </table>

                                <label for="comments" class="label_comments">GenXT Comments:</label><br>
                                <textarea id="comments" name="comments" maxlength="191"></textarea>
                                
                                <label for="comments" class="label_comments refund_class">Refund Amount:</label><br>

                                <input type="number" name="refund_amount" id="refund_amount" value="0">

                                <div class="text-center mt-20">
                                    <input type="submit" class="btn btn-primary change-status" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer col-md-12">
                      <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
                    </div>
                  </div>
                </div>
              </div>
@stop

@push('scripts')
<style type="text/css">
    table{
        margin-left: 50px;
        margin-top: 20px;
    }

    td{
        padding:10px 15px;
    }

    .mt-20{
        margin-top:20px;
    }

    .dialog_table{
        margin: 0 auto;
    }

    .dialog_table td{
        padding: 2px 5px;
        text-align: left;
    }
    .value span{
        display: inline-block;
        word-wrap: break-word;
        max-width: 200px;
    }
    
    .modal-dialog{
        max-width: 400px;
        width: 80%;
        margin: 20px auto;
    }

    .modal-body form{
        text-align: left;
    }

    .input-area {
        display: inline-block;
    }

    #comments{
        width: 100%;
        padding: 5px;
    }

    .label_comments{
        margin-top:10px;
        margin-bottom: 0px !important;
    }

    .class-error{
        color:rgb(141, 0, 0);
    }

    .tabs ul li a{
        font-weight: bold !important;
        font-size: 1.4rem;
    }

    @media(max-width:600px){
        table{
            margin: 10px auto;
        }

        td{
            padding:5px;
        }
        .tabs ul li{
            display: block;
            width: 100%;
            text-align: center;
        }

        .tabs ul li a{
            display: inline-block;
        }

    }
</style>
@endpush


@push('scripts')
    <script type="text/javascript">       

        $(document).on('submit','#failute_implants_status', function(e){
            let status = true;            
            if(!($(this).find('input[name="failed_status"]:checked').length)){
                $(this).find('.dialog_table').append(`<span class="class-error">This field is required</span>`);
                status = false;
            }

            if(status && confirm('Are you sure you want to change Return implant status?')){
                $('.change-status').prop('disabled', true);
            }else{
                status = false;
            }

            if(!status) e.preventDefault();
        });

        $(document).ready(function(){
            $('form').on('change', 'input[type="radio"]', function(e){
                let target = $(e.target);
                let root_form = target.closest('form');
                let ele_ref = root_form.find('#refund_amount');
                if(target.is('#Completed')){
                    ele_ref.show();
                    root_form.find('.refund_class').show();
                }else{
                    ele_ref.hide();
                    root_form.find('.refund_class').hide();
                    ele_ref.val(0);
                }
                //
            });

            $('body').find('.update-status').on('click', function(event){
                let update_id = $(this).data('update_id');
                $('#changeFailedImplantStatus').find(".failed_id").val(update_id);
                let status = $(this).data('status');
                $('input#'+status).prop("checked", true);
                if(status =='Completed'){
                    $('#changeFailedImplantStatus .refund_class, #changeFailedImplantStatus #refund_amount ').show();
                }else{
                    $('#changeFailedImplantStatus .refund_class, #changeFailedImplantStatus #refund_amount ').hide();
                }
            });
        });
    </script>
@endpush

@extends('admin::layouts.content')

@section('page_title')
    Courses - GenXT
@stop

@section('content')
    <div class="content">

        <div class="page-header page-section">
            <div class="page-title">
                <h1>Courses</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.courses.create') }}" class="btn btn-lg btn-primary">
                    Create
                </a>
            </div>

        </div>

        <div class="page-content">

           @inject('courses','Webkul\Admin\DataGrids\CoursesDataGrid')
            {!! $courses->render() !!}
        </div>
    </div>
@stop
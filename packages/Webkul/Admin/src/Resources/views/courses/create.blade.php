@extends('admin::layouts.content')

@section('page_title')
    Create Courses - GenXT
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.courses.store') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.settings.pages.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save Course
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.pages.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-fields col-md-12">
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('course_name') ? 'has-error' : '']">
                                        <label for="menu-title" class="required">Course Name</label>
                                        <input v-validate="'required'" class="control" id="course-name" name="course_name" data-vv-as="Course Name"/>
                                        <span class="control-error" v-if="errors.has('course_name')">@{{ errors.first('course_name') }}</span>
                                    </div>

                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('course_slug') ? 'has-error' : '']">
                                        <label for="course-slug" class="required">Course Slug</label>
                                        <input v-validate="'required'" class="control" id="course-slug" name="course_slug" v-slugify data-vv-as="Course Slug"/>
                                        <span class="control-error" v-if="errors.has('course_slug')">@{{ errors.first('course_slug') }}</span>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('start_date') ? 'has-error' : '']">
                                        <label for="date_start" class="required">Course Start Date</label>
                                        <date>
                                            <input v-validate="'required'" type="text" class="control date-start" name="start_date" data-vv-as="Course Start Date">
                                        </date> 
                                         <span class="control-error" v-if="errors.has('start_date')">@{{ errors.first('start_date') }}</span>
                                    </div>
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('end_date') ? 'has-error' : '']">
                                        <label for="date_end" class="required">Course End Date</label>
                                        <date>
                                            <input v-validate="'required'" type="text" class="control date-end" name="end_date" data-vv-as="Course End Date">
                                        </date>
                                        <span class="control-error" v-if="errors.has('end_date')">@{{ errors.first('end_date') }}</span>
                                    </div>
                                </div>

                                <div class="form-field col-md-12">

                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="menu-title" style="display:inline;">Registation form:</label>
                                        <input type="checkbox" id="course-reg" name="course_reg" checked style="margin-left:5px" data-vv-as="Registation form"/>
                                    </div>

                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="menu-title">Course Date</label>
                                        <input class="control" id="course-date" name="course_date" data-vv-as="Course Date"/>
                                    </div>
                                </div>

                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="menu-title">Course Image</label>
                                        <input type="file" id="course-image" name="course_image" data-vv-as="Course Image"/>
                                        <img id="show-image" style="display:none;width: 100%;margin: 15px 0 0;" src="#" alt="your image" />
                                    </div>
                                </div>

                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="course-contact">Course Contact</label>
                                        <input class="control" id="course-contact" name="course_contact" data-vv-as="Course Contact"/>
                                    </div>
                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="course-venue">Course Venue</label>
                                        <textarea class="control" id="course-venue" name="course_venue" data-vv-as="Course Venue"></textarea>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12 col-xs-12">
                                        <label for="course-highlights">Course Highlights</label>
                                        <textarea class="control course-textarea" id="course-highlights" name="course_highlights" data-vv-as="Course Highlights"></textarea>
                                    </div>   
                                </div>
                                
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12" :class="[errors.has('course_content') ? 'has-error' : '']">
                                        <label for="page-content" class="required">Course Content</label>
                                        <textarea v-validate="'required'" class="control course-textarea" id="course-content" name="course_content" data-vv-as="Course Content"></textarea>
                                         <input name="image" type="file" id="upload" class="hidden" style="display:none" onchange="">
                                        <span class="control-error" v-if="errors.has('course_content')">@{{ errors.first('course_content') }}</span>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12">
                                        <label for="page-content">Faculty</label>
                                        <textarea class="control course-textarea" id="course-faculty" name="course_faculty" data-vv-as="Faculty"></textarea>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12">
                                        <label for="page-content">Review</label>
                                        <textarea class="control course-textarea" id="course-review" name="course_review" data-vv-as="Review"></textarea>
                                    </div>
                                </div>

                                <div class="form-field images-clone col-md-12">
                                    <h3 for="page-content">Slider Images - Set 1</h3>
                                    <div id="clonedInput0" class="clonedInput col-md-3">
                                        <div class="courseInput-inner">
                                            <div class="input-group control-group increment">
                                              <input type="file" name="course_images_set_1[]" class="" style="display:none">
                                              <img src="{{bagisto_asset('themes/default/assets/images/placeholder-image.png')}}" id="display-image0" width="100" height="100">
                                            </div>
                                            <div class="actions">
                                                <button type="button" class="clone">Add</button> 
                                                <button type="button" class="slider1 remove">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-field images2-clone col-md-12">
                                    <h3 for="page-content">Slider Images - Set 2</h3>
                                    <?php $loop_count=0;
                                        if(isset($course_images_set1))
                                            $loop_count = count($course_images_set1);
                                    ?>
                                    <div id="clonedInput{{$loop_count}}" class="clonedInput col-md-3">
                                        <div class="courseInput-inner">
                                            <div class="input-group control-group increment">
                                              <input type="file" name="course_images_set_2[]" class="" style="display:none">
                                              <img src="{{bagisto_asset('themes/default/assets/images/placeholder-image.png')}}" id="display-image{{$loop_count}}" width="100" height="100">
                                            </div>
                                            <div class="actions">
                                                <button type="button" class="clone">Add</button> 
                                                <button type="button" class="slider2 remove">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <button type="submit" class="btn btn-primary" style="margin-top:10px">Submit</button>


                            
                            </div>
                        </div>
                    </accordian>

                    {{-- <accordian :title="'{{ __('admin::app.settings.pages.meta_desc') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('meta_title') ? 'has-error' : '']">
                                <label for="meta-title" class="required">{{ __('admin::app.settings.pages.meta_title') }}</label>
                               <textarea v-validate="'required'" class="control" id="meta-title" name="meta_title" data-vv-as="&quot;meta-title&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_title')">@{{ errors.first('meta_title') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('meta_keyword') ? 'has-error' : '']">
                                <label for="meta-keyword">{{ __('admin::app.settings.pages.meta_keyword') }}</label>
                                <textarea class="control" id="meta-keyword" name="meta_keyword" data-vv-as="&quot;meta-keyword&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_keyword')">@{{ errors.first('meta_keyword') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('meta_description') ? 'has-error' : '']">
                                <label for="meta-description">{{ __('admin::app.settings.pages.meta_description') }}</label>
                                <textarea  class="control" id="meta-description" name="meta_description" data-vv-as="&quot;meta-description&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_description')">@{{ errors.first('meta_description') }}</span>
                            </div>
                        </div>
                    </accordian> --}}
                   
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
    <script src="{{ asset('vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js') }}"></script>

    <script>
        $(document).ready(function () {
   function readIMG(input,image_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+image_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $('body').on('change','input[type=file]', function(){
      var image_id = $(this).parent().find('img').attr('id');
     // alert(image_id);
        readIMG(this,image_id);
    });



$('body').on('click', '.courseInput-inner > div > img', function(){
 $(this).parent().find('input[type=file]').trigger('click'); 
});


var regex = /^(.+?)(\d+)$/i;
var cloneIndex = $(".clonedInput").length;
//alert(cloneIndex);
function clone(){
//alert($(this).parents(".clonedInput").find('button.remove').html());
        $clone = $(this).parents(".clonedInput").clone();
        $clone.insertAfter($(this).parents(".clonedInput"));
        $clone.attr("id", "clonedInput" +  cloneIndex);
        $clone.find('img').attr('id', "display-image" +  cloneIndex)
             .attr('src', "{{bagisto_asset('themes/default/assets/images/placeholder-image.png')}}");
        $clone.find('.actions > button.remove').removeAttr("data-id");
        //$clone.on('click', 'button.clone', clone);
       // $clone.on('click', 'button.remove', remove);
       //alert($clone);
        //$(this).parents(".clonedInput").find('.clone').css('display','none');
    cloneIndex++;
}

function remove(){
    var image_id = $(this).data('id');
     var this_var = $(this);
    if(image_id != undefined){
   
    var route_name="{{route('admin.courses.delete', ':image_id')}}";
    route_name=route_name.replace(':image_id',image_id);
    $.ajax({
        type: "GET",
        url: route_name,
        success: function(response){
            console.log(response);
            if(response.count_images == 0){
                this_var.parents(".clonedInput").find('button.clone').trigger('click');
            }

            this_var.parents(".clonedInput").remove();
        }
    });   
    }
    else{
        //alert(this_var.parent().parent().html());
        var total_sliders = $('body').find('.images-clone .clonedInput').length;
        if(this_var.hasClass('slider2'))
            total_sliders = $('body').find('.images2-clone .clonedInput').length;

        if(total_sliders <= 1){
            this_var.parents(".clonedInput").find('button.clone').trigger('click');
        }
        this_var.parents(".clonedInput").remove();
    }
}

$("body").on("click", "button.clone", clone);
$("body").on("click","button.remove", remove);


            $('#channel-switcher, #locale-switcher').on('change', function (e) {
                $('#channel-switcher').val()
                var query = '?channel=' + $('#channel-switcher').val() + '&locale=' + $('#locale-switcher').val();

                window.location.href = "{{ route('admin.pages.create')  }}" + query;
            })

            tinymce.init({
                selector: 'textarea.course-textarea',
                height: 200,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#show-image').css('display', 'block');
                        $('#show-image').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }


            
            $("#course-image").change(function(){
                readURL(this);
            });

 });
       </script>
@endpush

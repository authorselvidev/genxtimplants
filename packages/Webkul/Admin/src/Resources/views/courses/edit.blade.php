@extends('admin::layouts.content')

@section('page_title')
    Edit Course - GenXT
@stop

@section('content')
    <div class="content">

        <form method="post" action="{{ route('admin.courses.update', $course->id) }}" enctype="multipart/form-data">
            @csrf()
            <input name="_method" type="hidden" value="PATCH">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" ></i>

                        Edit Course
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Update Course
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    
<accordian :title="'{{ __('admin::app.settings.pages.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-fields col-md-12">
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('course_name') ? 'has-error' : '']">
                                        <label for="menu-title" class="required">Course Name</label>
                                        <input v-validate="'required'" class="control" id="course-name" name="course_name" value="{{$course->name}}" data-vv-as="Course Name"/>
                                        <span class="control-error" v-if="errors.has('course_name')">@{{ errors.first('course_name') }}</span>
                                    </div>

                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('course_slug') ? 'has-error' : '']">
                                        <label for="course-slug" class="required">Course Slug</label>
                                        <input v-validate="'required'" class="control" id="course-slug" name="course_slug" value="{{$course->slug}}" v-slugify data-vv-as="Course Slug"/>
                                        <span class="control-error" v-if="errors.has('course_slug')">@{{ errors.first('course_slug') }}</span>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('start_date') ? 'has-error' : '']">
                                        <label for="date_start" class="required">Course Start Date</label>
                                        <date>
                                            <input v-validate="'required'" type="text" class="control date-start" name="start_date" value="{{$course->start_date}}" data-vv-as="Course Start Date">
                                        </date> 
                                         <span class="control-error" v-if="errors.has('start_date')">@{{ errors.first('start_date') }}</span>
                                    </div>
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('end_date') ? 'has-error' : '']">
                                        <label for="date_end" class="required">Course End Date</label>
                                        <date>
                                            <input v-validate="'required'" type="text" class="control date-end" name="end_date" value="{{$course->end_date}}" data-vv-as="Course End Date">
                                        </date>
                                        <span class="control-error" v-if="errors.has('end_date')">@{{ errors.first('end_date') }}</span>
                                    </div>
                                </div>

                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="menu-title" style="display:inline;">Registation form:</label>
                                        <input type="checkbox" id="course-reg" name="course_reg" data-vv-as="Registation form" @if($course->register_form==1) checked @endif style="margin-left:5px"/>
                                    </div>
                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="menu-title">Course Date</label>
                                        <input class="control" id="course-date" name="course_date" value="{{$course->course_date}}" data-vv-as="Course Date"/>
                                    </div>
                                </div>

                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="menu-title">Course Image</label>
                                        <input type="file" id="course-image" name="course_image" data-vv-as="Course Image"/>
                                        @if(isset($course->image))
                                            <img id="show-image" style="width: 100%;margin: 15px 0 0;" src="{{bagisto_asset('themes/default/assets/images/courses/'.$course->image)}}" alt="your image" />
                                        @else
                                            <img id="show-image" style="display:none;width: 100%;margin: 15px 0 0;" src="#" alt="your image" />
                                        @endif
                                    </div>
                                </div>

                                <div class="form-field col-md-12">
                                    
                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="course-contact">Course Contact</label>
                                        <input class="control" id="course-contact" name="course_contact" value="{{$course->contact}}" data-vv-as="Course Contact"/>
                                    </div>
                                    <div class="control-group col-md-6 col-xs-12">
                                        <label for="course-venue">Course Venue</label>
                                        <textarea class="control" id="course-venue" name="course_venue" data-vv-as="Course Venue">{{$course->venue or ""}}</textarea>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12 col-xs-12">
                                        <label for="course-highlights">Course Highlights</label>
                                        <textarea class="control course-textarea" id="course-highlights" name="course_highlights" data-vv-as="Course Highlights">{{$course->course_highlights or ""}}</textarea>
                                    </div>
                                </div>

                                
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12" :class="[errors.has('course_content') ? 'has-error' : '']">
                                        <label for="page-content" class="required">Course Content</label>
                                        <textarea v-validate="'required'" class="control course-textarea" id="course-content" name="course_content" data-vv-as="Course Content"> {{$course->description}}</textarea>
                                         <input name="image" type="file" id="upload" class="hidden" style="display:none" onchange="">
                                        <span class="control-error" v-if="errors.has('course_content')">@{{ errors.first('course_content') }}</span>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12">
                                        <label for="page-content">Faculty</label>
                                        <textarea class="control course-textarea" id="course-faculty" name="course_faculty" data-vv-as="Faculty"> {{$course->faculty or ""}}</textarea>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12">
                                        <label for="page-content">Review</label>
                                        <textarea class="control course-textarea" id="course-review" name="course_review" data-vv-as="Review"> {{$course->review or ""}}</textarea>
                                    </div>
                                </div>

                                <?php $course_images_set1 = DB::table('course_images')->where('slider_set',1)->where('course_id',$course->id)->get();
                                ?>
                                <div class="form-field images-clone col-md-12">
                                    <h3 for="page-content">Slider Images - Set 1</h3>
                                    @if(count($course_images_set1) > 0)
                                        @foreach($course_images_set1 as $key => $course_image)
                                           <div id="clonedInput{{$key}}" class="clonedInput col-md-3">
                                            <div class="courseInput-inner">
                                            <div class="input-group control-group increment">
                                              <input type="file" name="course_images_set_1[]" class="" value = '{{$course_image->name}}' style="display:none">
                                              <img src="{{bagisto_asset('themes/default/assets/images/courses/'.$course_image->name)}}" id="display-image{{$key}}" width="100" height="100">
                                            </div>
                                            <div class="actions">
                                                <button type="button" class="clone">Add</button> 
                                                <button type="button" data-id="{{$course_image->id}}" class="slider1 remove">Remove</button>
                                            </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    @else
                                        <div id="clonedInput0" class="clonedInput col-md-3">
                                            <div class="courseInput-inner">
                                            <div class="input-group control-group increment">
                                              <input type="file" name="course_images_set_1[]" class="" style="display:none">
                                              <img src="{{bagisto_asset('themes/default/assets/images/placeholder-image.png')}}" id="display-image0" width="100" height="100">
                                            </div>
                                            <div class="actions">
                                                <button type="button" class="clone">Add</button> 
                                                <button type="button" class="slider1 remove">Remove</button>
                                            </div>
                                        </div>
                                        </div>
                                    @endif
                                </div><!-- slider set 1-->

                                 <?php $course_images_set2 = DB::table('course_images')->where('slider_set',2)->where('course_id',$course->id)->get();
                                ?>
                                <div class="form-field images2-clone col-md-12">
                                    <h3 for="page-content">Slider Images - Set 2</h3>
                                    <?php $loop_count=0;
                                        if(isset($course_images_set1))
                                            $loop_count = count($course_images_set1);
                                    ?>
                                    @if(count($course_images_set2) > 0)
                                        @foreach($course_images_set2 as $key => $course_image)
                                           <div id="clonedInput{{$loop_count}}" class="clonedInput col-md-3">
                                            <div class="courseInput-inner">
                                            <div class="input-group control-group increment">
                                              <input type="file" name="course_images_set_2[]" class="" value = '{{$course_image->name}}' style="display:none" data-vv-as="">
                                              <img src="{{bagisto_asset('themes/default/assets/images/courses/'.$course_image->name)}}" id="display-image{{$loop_count}}" width="100" height="100">
                                            </div>
                                            <div class="actions">
                                                <button type="button" class="clone">Add</button> 
                                                <button type="button" data-id="{{$course_image->id}}" class="slider2 remove">Remove</button>
                                            </div>
                                            </div>
                                        </div>
                                        <?php $loop_count++; ?>
                                        @endforeach
                                    @else
                                        <div id="clonedInput{{$loop_count}}" class="clonedInput col-md-3">
                                            <div class="courseInput-inner">
                                            <div class="input-group control-group increment">
                                              <input type="file" name="course_images_set_2[]" class="" style="display:none">
                                              <img src="{{bagisto_asset('themes/default/assets/images/placeholder-image.png')}}" id="display-image{{$loop_count}}" width="100" height="100">
                                            </div>
                                            <div class="actions">
                                                <button type="button" class="clone">Add</button> 
                                                <button type="button" class="slider2 remove">Remove</button>
                                            </div>
                                        </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </accordian>

                  
                </div>
            </div>
        </form>
    </div>
@stop
@push('scripts')
    <script src="{{ asset('vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js') }}"></script>

    <script>
        $(document).ready(function () {

        function readIMG(input,image_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+image_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $('body').on('change','input[type=file]', function(){
      var image_id = $(this).parent().find('img').attr('id');
     // alert(image_id);
        readIMG(this,image_id);
    });



$('body').on('click', '.courseInput-inner > div > img', function(){
 $(this).parent().find('input[type=file]').trigger('click'); 
});


var regex = /^(.+?)(\d+)$/i;
var cloneIndex = $(".clonedInput").length;
//alert(cloneIndex);
function clone(){
//alert($(this).parents(".clonedInput").find('button.remove').html());
        $clone = $(this).parents(".clonedInput").clone();
        $clone.insertAfter($(this).parents(".clonedInput"));
        $clone.attr("id", "clonedInput" +  cloneIndex);
        $clone.find('img').attr('id', "display-image" +  cloneIndex)
             .attr('src', "{{bagisto_asset('themes/default/assets/images/placeholder-image.png')}}");
        $clone.find('.actions > button.remove').removeAttr("data-id");
        //$clone.on('click', 'button.clone', clone);
       // $clone.on('click', 'button.remove', remove);
       //alert($clone);
        //$(this).parents(".clonedInput").find('.clone').css('display','none');
    cloneIndex++;
}

function remove(){
    var image_id = $(this).data('id');
     var this_var = $(this);
    if(image_id != undefined){
   
    var route_name="{{route('admin.courses.delete', ':image_id')}}";
    route_name=route_name.replace(':image_id',image_id);
    $.ajax({
        type: "GET",
        url: route_name,
        success: function(response){
            console.log(response);
            if(response.count_images == 0){
                this_var.parents(".clonedInput").find('button.clone').trigger('click');
            }

            this_var.parents(".clonedInput").remove();
        }
    });   
    }
    else{
        //alert(this_var.parent().parent().html());
        var total_sliders = $('body').find('.images-clone .clonedInput').length;
        if(this_var.hasClass('slider2'))
        	total_sliders = $('body').find('.images2-clone .clonedInput').length;

        if(total_sliders <= 1){
        	this_var.parents(".clonedInput").find('button.clone').trigger('click');
        }
        this_var.parents(".clonedInput").remove();
    }
}

$("body").on("click", "button.clone", clone);
$("body").on("click","button.remove", remove);



    $('#channel-switcher, #locale-switcher').on('change', function (e) {
                $('#channel-switcher').val()
                var query = '?channel=' + $('#channel-switcher').val() + '&locale=' + $('#locale-switcher').val();

                window.location.href = "{{ route('admin.pages.create')  }}" + query;
            })

            tinymce.init({
                selector: 'textarea.course-textarea',
                height: 200,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#show-image').css('display', 'block');
                        $('#show-image').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }


            
            $("#course-image").change(function(){
                readURL(this);
            });

        });
    </script>
@endpush



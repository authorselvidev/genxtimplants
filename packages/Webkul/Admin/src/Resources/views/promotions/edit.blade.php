@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.promotions.edit-title') }}
@stop

@section('content')
    <div class="content">

        <form method="post" action="{{ route('admin.promotion.update', $discount->id) }}">
            @csrf()
            <input name="_method" type="hidden" value="PATCH">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.promotions.edit-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.promotions.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                 
                <accordian :title="'{{ __('admin::app.promotions.general') }}'" :active="true">
                    <div slot="body">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-4" :class="[errors.has('discount_title') ? 'has-error' : '']">
                                <label class="required" for="discount_title">Promotion Title</label>
                                <input type="text" v-validate="'required'" id="discount_title" class="control" name="discount_title" value="{{ $discount->discount_title }}" data-vv-as="Promotion Title">
                                <span class="control-error" v-if="errors.has('discount_title')">@{{ errors.first('discount_title') }}</span>
                            </div>                            
                            <div class="control-group col-md-4" :class="[errors.has('min_order_qty') ? 'has-error' : '']">
                                <label class="required" for="min_order_qty">Minimum Order Quantity</label>
                                <input type="text" v-validate="'required'" id="min_order_qty" class="control" name="min_order_qty" value="{{$promotion->min_order_qty}}" data-vv-as="Minimum Order Quantity">
                                <span class="control-error" v-if="errors.has('min_order_qty')">@{{ errors.first('min_order_qty') }}</span>
                            </div>
                            <div class="control-group col-md-4" :class="[errors.has('free_products_count') ? 'has-error' : '']">
                                <label class="required" for="free_products_count">No. of free Products</label>
                                <input type="text" v-validate="'required'" id="free_products_count" class="control" name="free_products_count" value="{{$promotion->free_products_count}}" data-vv-as="No. of free Products">
                                <span class="control-error" v-if="errors.has('free_products_count')">@{{ errors.first('free_products_count') }}</span>
                            </div>
                            
                                
                        </div> 
                        <div class="check-discount col-md-12">   
                            <div class="control-group col-md-12 @if ($errors->has('for_whom')) {{'has-error'}} @endif">
                                <span class="col-md-5"><label for="for_whom" class="required">For Whom</label></span>
                                <span class="text-center col-md-1">:</span>
                                <div class="col-md-6">
                                <span class="doctor_ck"><input type="checkbox" class="control" @if($promotion->for_whom == 3 || $promotion->for_whom == 0) {{ 'checked' }} @endif id="for_whom_doctor" name="for_whom[]" value="3"><label for="for_whom_doctor">Doctor</label></span>
                                <span class="dealer_ck"><input type="checkbox" class="control" @if($promotion->for_whom == 2 || $promotion->for_whom == 0) {{ 'checked' }} @endif id="for_whom_dealer" name="for_whom[]" value="2"><label for="for_whom_dealer">Dealer</label></span>
                                @if ($errors->has('for_whom'))
                                    <span class="control-error">
                                        {{ $errors->first('for_whom') }}
                                    </span>
                                @endif
                                </div>
                            </div>   
                               <div class="control-group col-md-12" :class="[errors.has('earn_points') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="earn_points" class="required">Earn Loyalty Points</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <span class="doctor_ck"><input v-validate="'required'" type="radio" class="control" @if($promotion->earn_points == 1) {{ 'checked' }} @endif id="earn_points_yes" name="earn_points" value="1"><label for="earn_points_yes">Yes</label></span>
                                        <span class="dealer_ck"><input v-validate="'required'" type="radio" class="control" @if($promotion->earn_points == 0) {{ 'checked' }} @endif id="earn_points_no" name="earn_points" value="0"><label for="earn_points_no">No</label></span>
                                        <span class="control-error" v-if="errors.has('earn_points')">@{{ errors.first('earn_points') }}</span>
                                    </div>
                                </div>
                                
                                <div class="control-group col-md-12" :class="[errors.has('discount_usage_limit') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="discount_usage_limit" class="required">How many times a doctor/dealer can use?</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <input type="text" v-validate="'required'" id="discount_usage_limit" class="control" name="discount_usage_limit" value="{{ $discount->discount_usage_limit }}" data-vv-as="How many times">
                                        <span class="control-error" v-if="errors.has('discount_usage_limit')">@{{ errors.first('discount_usage_limit') }}</span>
                                    </div>
                                </div>  
                        </div>  
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-4">
                                <label for="coupon_code">Discount Code</label>
                                <input type="text" id="coupon_code" class="control" name="coupon_code" value="{{ $discount->coupon_code }}" data-vv-as="Discount Code">
                            </div>
                            <div class="control-group col-md-4">
                                <label for="date_start">Date Start</label>
                                <date>
                                    <input type="text" class="control date-start" name="date_start" value="{{ $discount->date_start }}" data-vv-as="Date Start">
                                </date> 
                            </div>
                            <div class="control-group col-md-4">
                                <label for="date_end">Date End</label>
                                <date>
                                    <input type="text" class="control date-end" name="date_end" value="{{ $discount->date_end }}" data-vv-as="Date End">
                                </date>
                            </div>  
                        </div>          
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-12" :class="[errors.has('discount_description') ? 'has-error' : '']">
                                <label class="required" for="discount_description">Promotion Description</label>
                                <textarea id="discount_description" v-validate="'required'" class="control" name="discount_description" data-vv-as="Promotion Description">{{ $discount->discount_description }}</textarea>
                                <span class="control-error" v-if="errors.has('discount_description')">@{{ errors.first('discount_description') }}</span>
                            </div>
                        </div>
                    </div>
                </accordian>

                <accordian :title="'Promotions Details'" :active="true">
                    <div slot="body">
                        
                        <div class="discount-part form-field col-md-12">
                            <div class="discount-section col-md-12">

                            <?php $promotion_ordered_categories=$promotion_free_categories=array();
                                $promotion_ordered_categories = DB::table('promotion_ordered_category')
                                                ->where('promotion_id',$promotion->id)
                                                ->pluck('category_id');
                                $promotion_ordered_categorys=array();                
                                foreach($promotion_ordered_categories as $promotion_ordered_category)
                                {
                                    $promotion_ordered_categorys[] = (int)$promotion_ordered_category;
                                }
                                $promotion_free_categories=DB::table('promotion_free_product_category')
                                                ->where('promotion_id',$promotion->id)
                                                ->pluck('category_id');
                                 $promotion_free_categorys=array();                
                                foreach($promotion_free_categories as $promotion_free_category)
                                {
                                    $promotion_free_categorys[] = (int)$promotion_free_category;
                                }                
                                if(count($promotion_ordered_categorys) > 0 && count($promotion_free_categorys) > 0) {    ?>
                                    
                                <div id="discount0" class="form-field col-md-12">
                                    <div class="discount-inner col-md-12">
                                        @if ($categories->count())
                                            <div class="category-part col-md-6">
                                                <label class="required">Ordered Categories</label>
                                                <tree-view behavior="normal" value-field="id" name-field="promotion_ordered_categories" input-type="checkbox" items='@json($categories)' value='@json($promotion_ordered_categorys)'></tree-view>
                                            </div>   
                                            <div class="category-part col-md-6">
                                                <label class="required">Ordered Categories</label>
                                                <tree-view behavior="normal" value-field="id" name-field="promotion_free_categories" input-type="checkbox" items='@json($categories)' value='@json($promotion_free_categorys)'></tree-view>
                                            </div>                                           
                                        @endif
                                    </div>
                                </div>

                                <input type="hidden" name="promotion_id" value="{{$promotion->id}}">
                                <?php //$cd_count++; 
                            } ?>

                                                                
                            </div>                            
                        </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

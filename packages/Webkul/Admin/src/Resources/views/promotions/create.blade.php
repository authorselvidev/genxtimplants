@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.promotions.add-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.promotion.store') }}" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/promotions') }}';"></i>

                        {{ __('admin::app.promotions.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.promotions.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                
                    <accordian :title="'{{ __('admin::app.promotions.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-field col-md-12">
                                <div class="control-group col-md-4" :class="[errors.has('discount_title') ? 'has-error' : '']">
                                    <label class="required" for="discount_title">Promotion Title</label>
                                    <input type="text" v-validate="'required'" id="discount_title" class="control" name="discount_title" value="{{old('discount_title')}}" data-vv-as="Promotion Title">
                                    <span class="control-error" v-if="errors.has('discount_title')">@{{ errors.first('discount_title') }}</span>
                                </div>
                                <div class="control-group col-md-4" :class="[errors.has('min_order_qty') ? 'has-error' : '']">
                                    <label class="required" for="min_order_qty">Minimum order quantity</label>
                                    <input type="text" v-validate="'required'" id="min_order_qty" class="control" name="min_order_qty" value="{{old('min_order_qty')}}" data-vv-as="Minimum order quantity">
                                    <span class="control-error" v-if="errors.has('min_order_qty')">@{{ errors.first('min_order_qty') }}</span>
                                </div>
                                <div class="control-group col-md-4" :class="[errors.has('free_products_count') ? 'has-error' : '']">
                                    <label class="required" for="free_products_count">No. of free Products</label>
                                    <input type="text" v-validate="'required'" id="free_products_count" class="control" name="free_products_count" value="{{old('free_products_count')}}" data-vv-as="No. of free Products">
                                    <span class="control-error" v-if="errors.has('free_products_count')">@{{ errors.first('free_products_count') }}</span>
                                </div>
                                
                            </div>
                            <div class="check-discount col-md-12">
                            <div class="control-group col-md-12 @if ($errors->has('for_whom')) {{'has-error'}} @endif">
                                    <span class="col-md-5"><label for="for_whom" class="required">For Whom</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                    <span class="doctor_ck"><input type="checkbox" class="control" id="for_whom_doctor" name="for_whom[]" value="3" data-vv-as="For Whom"><label for="for_whom_doctor">Doctor</label></span>
                                    <span class="dealer_ck"><input type="checkbox" class="control" id="for_whom_dealer" name="for_whom[]" value="2" data-vv-as="For Whom"><label for="for_whom_dealer">Dealer</label></span>
                                    @if ($errors->has('for_whom'))
                                        <span class="control-error">
                                            {{ $errors->first('for_whom') }}
                                        </span>
                                    @endif
                                </div>
                                </div>

                                <div class="control-group col-md-12" :class="[errors.has('earn_points') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="earn_points" class="required">Earn Loyalty Points</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <span class="doctor_ck"><input v-validate="'required'" type="radio" class="control" id="earn_points_yes" name="earn_points" value="1"><label for="earn_points_yes">Yes</label></span>
                                        <span class="dealer_ck"><input v-validate="'required'" type="radio" class="control" id="earn_points_no" name="earn_points" value="0"><label for="earn_points_no">No</label></span>
                                        <span class="control-error" v-if="errors.has('earn_points')">@{{ errors.first('earn_points') }}</span>
                                    </div>
                                </div>
                                
                                <div class="control-group col-md-12" :class="[errors.has('discount_usage_limit') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="discount_usage_limit" class="required">How many times a doctor/dealer can use?</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <input type="text" v-validate="'required'" id="discount_usage_limit" class="control" name="discount_usage_limit" value="{{old('discount_usage_limit')}}" data-vv-as="How many times">
                                        <span class="control-error" v-if="errors.has('discount_usage_limit')">@{{ errors.first('discount_usage_limit') }}</span>
                                    </div>
                                </div>
                            </div>
                             <div class="form-field col-md-12">
                                <?php $custom_code = mt_rand(10000, 99999);
                                    $gxt_custom_code = "GXT".$custom_code; ?>
                                <div class="control-group col-md-4" :class="[errors.has('coupon_code') ? 'has-error' : '']">
                                    <label class="required" for="coupon_code">Discount Code</label>
                                    <input v-validate="'required'" type="text" id="coupon_code" class="control" name="coupon_code" value="{{ $gxt_custom_code }}" data-vv-as="Discount Code">
                                    <span class="control-error" v-if="errors.has('coupon_code')">@{{ errors.first('coupon_code') }}</span>
                                </div>
                            <div class="control-group col-md-4">
                                    <label for="date_start">Date Start</label>
                                    <date>
                                        <input type="text" class="control date-start" name="date_start" value="" data-vv-as="Date Start">
                                    </date> 
                                </div>
                                <div class="control-group col-md-4">
                                    <label for="date_end">Date End</label>
                                    <date>
                                        <input type="text" class="control date-end" name="date_end" value="" data-vv-as="Date End">
                                    </date>
                                </div>
                            </div>
                            <div class="form-field col-md-12">
                                <div class="control-group col-md-12" :class="[errors.has('discount_description') ? 'has-error' : '']">
                                    <label class="required" for="discount_description">Promotion Description</label>
                                    <textarea id="discount_description" v-validate="'required'" class="control" name="discount_description" data-vv-as="Promotion Description">{{ old('discount_description')}}</textarea>
                                    <span class="control-error" v-if="errors.has('discount_description')">@{{ errors.first('discount_description') }}</span>
                                </div>
                            </div>
                        </div>
                    </accordian>
                    
                    <accordian :title="'Promotion Details'" :active="true">
                        <div slot="body">
                            
                           <div class="discount-part form-field col-md-12">
                            <div class="discount-section">
                                <div id="discount0" class="category-based col-md-12">
                                    <div class="discount-inner category col-md-12">
                                        <div class="category-part col-md-6 section @if ($errors->has('promotion_ordered_categories')) {{'has-error'}} @endif">
                                            <label class="required">Ordered Categories</label>
                                            <tree-view behavior="normal" value-field="id" name-field="promotion_ordered_categories" input-type="checkbox" items='@json($categories)' value='@json($categories->pluck("category_id"))'  v-validate="'required'"></tree-view>
                                            @if ($errors->has('promotion_ordered_categories'))
                                                <span class="control-error">{{ $errors->first('promotion_ordered_categories') }}
                                                </span>
                                            @endif

                                        </div>
                                        <div class="category-part col-md-6 section @if ($errors->has('promotion_free_categories')) {{'has-error'}} @endif">
                                            <label class="required">Free Product Categories</label>
                                            <tree-view behavior="normal" value-field="id" name-field="promotion_free_categories" input-type="checkbox" items='@json($categories)' value='@json($categories->pluck("category_id"))'  v-validate="'required'"></tree-view>
                                            @if ($errors->has('promotion_free_categories'))
                                                <span class="control-error">{{ $errors->first('promotion_free_categories') }}
                                                </span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        
                           
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop


@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
$(document).ready(function () {
    var count = 1;
    $(".add-discount").click(function(e){
        e.preventDefault();
        var clone=$('#discount0').clone();
        clone.attr('id', 'discount' + count).appendTo(".discount-section");
        clone.find('.checkbox > input').attr('name', 'discount['+count+'][categories][]');
        clone.find('.discount-percnt > input').attr('name', 'discount['+count+'][discount_per]');
        clone.find('.discount-inner').append('<div class="control-group col-md-1"><button id="remove'+count+'" class="btn pull-right btn-danger remove-me" ><i class="fa fa-minus"></i></button></div>');
        count = count + 1;
    });

    $('body').on('click', '.remove-me', function(){
        $(this).parent().parent().parent().remove();
    });
});
</script>
@endpush

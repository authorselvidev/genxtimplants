@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.customers.customers.edit-title') }}
@stop

@section('content')
    <div class="content">
        <form method="POST" id="doctorCatDiscount" action="{{ route('admin.doctor.update', $customer->id) }}">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/doctors') }}';"></i>

                        {{ __('admin::app.customers.customers.title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.customers.customers.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">

                <div class="user-sec form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="form-fields col-md-12">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-3 col-sm-3 col-xs-12 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option <?php if($customer->my_title == "Mr.") echo 'selected'; ?> value="Mr.">Mr.</option>
                                    <option <?php if($customer->my_title == "Mrs.") echo 'selected'; ?> value="Mrs.">Mrs.</option>
                                    <option <?php if($customer->my_title == "Mr. Dr..") echo 'selected'; ?> value="Mr. Dr.">Mr. Dr.</option>
                                    <option <?php if($customer->my_title == "Mrs. Dr.") echo 'selected'; ?> value="Mrs. Dr.">Mrs. Dr.</option>
                                    <option <?php if($customer->my_title == "Mr. Dr. Prof.") echo 'selected'; ?> value="Mr. Dr. Prof.">Mr. Dr. Prof.</option>
                                    <option <?php if($customer->my_title == "Mrs. Dr. Prof.") echo 'selected'; ?> value="Mrs. Dr. Prof.">Mrs. Dr. Prof.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-md-5 col-sm-5 col-xs-12" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'" value="{{ $customer->first_name }}" data-vv-as="&quot;First Name&quot;">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group col-md-4 col-sm-4 col-xs-12 cpass">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name"value="{{ $customer->last_name }}" data-vv-as="Last Name">
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">Mobile Number</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ $customer->phone_number }}" data-vv-as="&quot;Mobile Number&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>

                            <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ $customer->email }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
                        </div>

                        
                        {{-- <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('password') ? 'has-error' : '']">
                                <label for="password" class="required">{{ __('shop::app.customer.signup-form.password') }}</label>
                                <input type="password" class="control" name="password" v-validate="'required|min:6'" ref="password" value="{{ old('password') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.password') }}&quot;">
                                <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span>
                            </div>

                            <div class="control-group cpass col-md-6" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                                <label for="password_confirmation" class="required">{{ __('shop::app.customer.signup-form.confirm_pass') }}</label>
                                <input type="password" class="control" name="password_confirmation"  v-validate="'required|min:6|confirmed:password'" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.confirm_pass') }}&quot;">
                                <span class="control-error" v-if="errors.has('password_confirmation')">@{{ errors.first('password_confirmation') }}</span>
                            </div>
                        </div> --}}
                        <div class="doctor-part col-md-12">
                            <div class="form-field col-md-12">
                                <div class="control-group pass col-md-6" :class="[errors.has('clinic_name') ? 'has-error' : '']">
                                    <label for="clinic_name" class="required">{{ __('shop::app.customer.signup-form.clinicname') }}</label>
                                    <input type="text" class="control" name="clinic_name" v-validate="'required'" value="{{ $customer->clinic_name }}" data-vv-as="Clinic Name">
                                    <span class="control-error" v-if="errors.has('clinic_name')">@{{ errors.first('clinic_name') }}</span>
                                </div>
                                <div class="control-group col-md-6 cpass" :class="[errors.has('clinic_number') ? 'has-error' : '']">
                                    <label for="clinic_number" class="required">Clinic Phone Number</label>
                                    <input type="text" class="control" name="clinic_number" v-validate="'required'" value="{{ $customer->clinic_number }}" data-vv-as="Clinic Phone Number">
                                    <span class="control-error" v-if="errors.has('clinic_number')">@{{ errors.first('clinic_number') }}</span>
                                </div>
                            </div>
                            <div class="form-field col-md-12">
                                <div class="control-group" :class="[errors.has('clinic_address') ? 'has-error' : '']">
                                    <label for="clinic_address" class="required">{{ __('shop::app.customer.signup-form.clinicaddr') }}</label>
                                    <textarea class="control" name="clinic_address" v-validate="'required'" data-vv-as="Clinic Address">{{ $customer->clinic_address }}</textarea>
                                    <span class="control-error" v-if="errors.has('clinic_address')">@{{ errors.first('clinic_address') }}</span>
                                </div>
                            </div>
                            <div class="form-field col-md-12">
                                <?php
                                    $user_role = auth()->guard('admin')->user()->role_id;
                                ?>
                                <div class="control-group pass col-md-6 @if($user_role==1) col-md-6 @else col-md-12 @endif" :class="[errors.has('dental_license_no') ? 'has-error' : '']">
                                    <label for="dental_license_no" class="required">Dental License No.</label>
                                    <input type="text" class="control" name="dental_license_no" v-validate="'required'" value="{{ $customer->dental_license_no }}" data-vv-as="Dental License No.">
                                    <span class="control-error" v-if="errors.has('dental_license_no')">@{{ errors.first('dental_license_no') }}</span>
                                </div>
                                
                                
                                @if($user_role == 1)
                                    <div class="control-group cpass col-md-6" :class="[errors.has('customer_group_id') ? 'has-error' : '']">
                                        <label for="group">Customer Group</label>
                                        <select name="customer_group_id" id="group" class="control" v-validate="'required'" data-vv-as="&quot;{{ "Group" }}&quot;">
                                            @foreach($customerGroup as $group)
                                                <option value="{{$group->id}}" @if($group->id==$customer->customer_group_id) selected @endif>{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="control-error" v-if="errors.has('customer_group_id')">@{{ errors.first('customer_group_id') }}</span>
                                    </div>
                                @else
                                    <input type="hidden" name="customer_group_id" value="{{$customer->customer_group_id}}">
                                @endif
                            </div>
                        </div>

                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('country_id') ? 'has-error' : '']">
                                <label for="country_id" class="required">{{ __('shop::app.customer.signup-form.country') }}</label>
                                <input type="text" class="control" name="country_id" v-validate="'required'" disabled="disabled" value="India">
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                            <div class="control-group cpass col-md-6" :class="[errors.has('state_id') ? 'has-error' : '']">
                                <label for="state_id" class="required">State</label>
                                <?php $states = array();
                                $states = DB::table('country_states')->where('country_id',101)->get();?>
                                <select name="state_id" id="state" class="control" v-validate="'required'" data-vv-as="State">
                                    <option value="">Choose your state...</option>
                                    <?php foreach($states as $key => $state) { ?>
                                        <option <?php if($state->id == $customer->state_id) echo 'selected'; ?> value="{{ $state->id }}">{{ $state->name }}</option>
                                    <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('state_id')">@{{ errors.first('state_id') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                        <div class="control-group pass col-md-6" :class="[errors.has('city_id') ? 'has-error' : '']">
                            <label for="city" class="required">{{ __('shop::app.customer.signup-form.city') }}</label>
                            <?php $cities = array();
                                $cities = DB::table('country_state_cities')->where('state_id',$customer->state_id)->get();?>
                                <select name="city_id" id="city" class="control" v-validate="'required'" data-vv-as="City">
                                    <option value="">Choose your city...</option>
                                    <?php foreach($cities as $key => $city) { ?>
                                        <option <?php if($city->id == $customer->city_id) echo 'selected'; ?> value="{{ $city->id }}">{{ $city->name }}</option>
                                    <?php } ?>
                                </select>
                            <span class="control-error" v-if="errors.has('city_id')">@{{ errors.first('city_id') }}</span>
                        </div>

                        <div class="control-group cpass col-md-6" :class="[errors.has('pin_code') ? 'has-error' : '']">
                            <label for="pin_code" class="required">Pincode</label>
                            <input type="text" class="control" name="pin_code" v-validate="'required'" value="{{ $customer->pin_code }}">
                            <span class="control-error" v-if="errors.has('pin_code')">@{{ errors.first('pin_code') }}</span>
                        </div>
                    </div>
                    <div class="form-field col-md-12">
                        @if(auth()->guard('admin')->user()->role_id != 2)
                        <div class="control-group cpass col-md-6" :class="[errors.has('dealer_id') ? 'has-error' : '']">
                                <?php $dealers = array();
                                $dealers = DB::table('users')->where('role_id',2)->get();
                                ?>
                                <label for="city" class="required">Under Dealer</label>
                                <select name="dealer_id" id="dealer_id" class="control" v-validate="'required'" data-vv-as="Under Dealer">
                                    <option value="">Choose Dealer...</option>
                                    <?php foreach($dealers as $key => $dealer) { ?>
                                        <option <?php if($dealer->id == $customer->dealer_id) echo 'selected'; ?> value="{{ $dealer->id }}">{{ $dealer->first_name }}, {{ $dealer->last_name }} {{ Webkul\Customer\Models\Cities::GetCityName($dealer->city_id)}}, {{ Webkul\Customer\Models\States::GetStateName($dealer->state_id)}}</option>
                                    <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                        @endif
                        @if(auth()->guard('admin')->user()->role_id == 1)
                            <div class="control-group cpass col-md-6">
                                <label for="prepayment_available">Prepayment Available</label>
                                <select name="prepayment_available" id="prepayment_available" class="control">
                                <option value="0" <?php if($customer->prepayment_available == 0) echo 'selected'; ?>>No</option>
                                <option value="1" <?php if($customer->prepayment_available == 1) echo 'selected'; ?>>Yes</option>
                                </select>
                            </div>
                        @endif
                    </div>

                    @if(auth()->guard('admin')->user()->role_id != 2)
                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6">
                                <label for="credit_available">Advance Payment Available</label>
                                <select name="credit_available" id="credit_available" class="control">
                                    <option value="0" <?php if($customer->credit_available == 0) echo 'selected'; ?>>No</option>
                                    <option value="1" <?php if($customer->credit_available == 1) echo 'selected'; ?>>Yes</option>
                                </select>
                            </div>
                            <div class="control-group cpass col-md-6">
                                <label for="credit_balance">Advance</label>
                                <input type="text" class="control" name="credit_balance" value="{{ $customer->credit_balance }}">
                            </div>
                        </div>
                    @endif
                   
                    <div class="discount-section">
                            <accordian :title="'Category Based Discount'" :active="true">
                                    <div slot="body">
                                        <div class="discounts" id="disount_container"></div>
                                        <input type="hidden" name="discount_id" value="{{$categry_discount{0}->id ?? ''}}">
                                        <button type="button" class="add-discount pull-right btn btn-primary" name="add-more">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                            </accordian>                            
                        </div>
                    

                        {{-- <div class="signup-confirm" :class="[errors.has('agreement') ? 'has-error' : '']">
                            <span class="checkbox">
                                <input type="checkbox" id="checkbox2" name="agreement" v-validate="'required'">
                                <label class="checkbox-view" for="checkbox2"></label>
                                <span>{{ __('shop::app.customer.signup-form.agree') }}
                                    <a href="">{{ __('shop::app.customer.signup-form.terms') }}</a> & <a href="">{{ __('shop::app.customer.signup-form.conditions') }}</a> {{ __('shop::app.customer.signup-form.using') }}.
                                </span>
                            </span>
                            <span class="control-error" v-if="errors.has('agreement')">@{{ errors.first('agreement') }}</span>
                        </div> --}}

                        {!! view_render_event('bagisto.shop.customers.signup_form_controls.after') !!}

                        {{-- <div class="control-group" :class="[errors.has('agreement') ? 'has-error' : '']">

                            <input type="checkbox" id="checkbox2" name="agreement" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.agreement') }}&quot;">
                            <span>{{ __('shop::app.customer.signup-form.agree') }}
                                <a href="">{{ __('shop::app.customer.signup-form.terms') }}</a> & <a href="">{{ __('shop::app.customer.signup-form.conditions') }}</a> {{ __('shop::app.customer.signup-form.using') }}.
                            </span>
                            <span class="control-error" v-if="errors.has('agreement')">@{{ errors.first('agreement') }}</span>
                        </div> --}}

                    </div>
                </div>
            </div><!--page-content-->
            <div class="page-footer send-link">
                <button type="submit" class="btn btn-lg pull-right btn-primary">
                    {{ __('admin::app.customers.customers.save-btn-title') }}
                </button>
            </div><!--page footer-->
        </form>
    </div>
@stop
@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/simTree.css')}}">
<script type="text/javascript" src="{{asset('themes/default/assets/js/simTree.js')}}"></script>
<script>
$(document).ready(function(){
$('body').on('change','#state', function(){
    
    var stateID = $(this).val(); 
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('customer/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
  });

        $('body').on('click', '.remove-me', function(){
            if(confirm('Do you like to remove this category?')){
                $(this).closest('.discount-part').remove();
            }
        });

        const category_tree = '{!! $categories !!}';
        const discounts_categories = {!! $all_discount !!};
        let treeCount =0;

        function addTree(dis=null){
            let cat_tree = JSON.parse(category_tree);

            let html_temp = $(`<div class="discount-part form-field col-md-12">
                                            <div class="discount-inner col-md-12">
                                                <div class="accordian active accordian_header">
                                                    <button type="button" id="remove${treeCount}" class="pull-right remove-me" ><i class="fa fa-times"></i></button>
                                                    
                                                    <div class="accordian-content">
                                                        <div class="category-part section">
                                                            <div class="categorieTrees" data-id=${treeCount}>
                                                                <input type="hidden" name="discount[${treeCount}][categories]" id="categories${treeCount}" class="categorys_values">
                                                                <div id="categorieTree${treeCount}"></div>
                                                            </div>
                                                        </div>

                                                        <div class="control-group discount-percnt col-md-12">
                                                            <label for="discount_per">Discount (%)</label>
                                                            <input type="text" id="discount_per" class="control" name="discount[${treeCount}][discount_per]" value="${dis?dis.percentage:''}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> `);

            $('#disount_container').append(html_temp);

            cat_tree = cat_tree.map((val)=>{
                if(dis && dis.categorys.includes(val.id)) val.checked = true; 

                if(val.pid) val.pid = treeCount+'-'+val.pid;
                    val.id = treeCount+'-'+val.id;
                    return val;
                });

            if(dis) $(`#categories${treeCount}`).val(dis.categorys.map((val)=> treeCount+'-'+val));
            
            $(`#categorieTree${treeCount}`).simTree({
                data: cat_tree,
                check: true,
                linkParent: true,
                onChange: function(item){
                    const id = $(this['$el']).closest('.categorieTrees').data('id');
                    $(`#categories${id}`).val(item.filter((ele)=>ele.end==true).map((ele)=>ele.id));
                }
            });
            treeCount++;
        }

        function checkValidFields(){
            let $check = true;
            let discounts_container = $('#disount_container');
            discounts_container.find('.error-msg').remove();


            discounts_container.children().each(function(ele){
                if(!$(this).find('.categorys_values').val()){
                    $(this).find('.categorieTrees').append('<span class="error-msg">Please choose any categories in this tree!</span>');
                    $check = false;
                };

                if(! +$(this).find('#discount_per').val()) {
                    $(this).find('.discount-percnt').append('<span class="error-msg">This discount percentage is required!</span>');
                    $check = false;  
                }
            });
            return $check;
        }

        $(".add-discount").click(function(e){
            if(checkValidFields()) addTree();
            e.preventDefault();
        });
        discounts_categories.forEach((dis)=> addTree(dis));
        $('#doctorCatDiscount').on('submit', function(e){
            if(!checkValidFields()) e.preventDefault();
        });
  });
</script>

    <style type="text/css">
       .accordian_container{
            clear: both;
            display: flex;
            align-items: center;
            align-content: center;
        }

        .accordian_header{
            border-top:1px solid #DEDEDE;
            position: relative;
        }

        .accordian_container>button{
            height: 30px;
            margin-left: 10px;
        }
        .remove-me, .add-discount{
            border: none;
            outline: none !important;
        }

        .remove-me{
            border-radius: 50%;
            background-color: #ff4a4a;
            color: #FFF;
            position: absolute;
            right: 10px;
            top: 10px;
        }

        .add-discount{
            margin-right: 15px;
        }
        .error-msg{
            display: inline-block;
            margin:10px 3px;
            color: #8F0000;
            font-size: 1.5rem;
        }
    </style>
@endpush

@extends('admin::layouts.content')

@section('page_title')
    Show Doctor
@stop

@section('content')
<div class="content">
    <div class="page-header">
        <div class="page-title">
            <h1>
                <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/doctors') }}';"></i>
                        Doctors
            </h1>
        </div>    
        <h3 class="text-center col-md-12">{{ Webkul\Customer\Models\Customer::CustomerName($customer->id) }}</h3>        
    </div>

    <div class="page-contents1">
    <div class="all-details col-md-12">
            <div class="tab" role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#dashboard" role="tab" data-toggle="tab">Dashboard</a></li>
                    @if(auth()->guard('admin')->user()->role_id != 2)
                    <li role="presentation"><a href="#doctor-details" class="notHavMentor" role="tab" data-toggle="tab">Doctor Details</a></li>
                    @endif
                    <li role="presentation"><a href="#orders" role="tab" data-toggle="tab">Orders</a></li>
                    <li role="presentation"><a href="#points-coins" role="tab" data-toggle="tab">Genxt Points/Coins</a></li>
                    {{-- <li role="presentation"><a href="#credit_orders" role="tab" data-toggle="tab">Advance Payment Orders</a></li> --}}
                    <li role="presentation"><a href="#credit_payments" role="tab" data-toggle="tab">Payments</a></li> 

                    @if(auth()->guard('admin')->user()->permission_to_create_order == 1 || auth()->guard('admin')->user()->role_id == 1)
                        {{-- <li role="presentation"><a href="{{route('admin.sales.custom_orders.create',$customer->id)}}">Create Order</a></li> --}}
                        <li role="presentation"><a href="{{route('admin.sales.custom_orders.cart',$customer->id)}}">Manage Cart</a></li>
                    @endif
                </ul>
                <div class="tab-content tabs">
                  <div role="tabpanel" class="tab-pane fade in active" id="dashboard">
                    <div class="dashboard-section col-lg-12">
                       <div class="count-box section]">
                          <div class="box-inner col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                                <div class="inner">
                                 <p>Total Orders</p>
                                 <h3>{{ $totalDoctorOrders }}</h3>
                                </div>
                            </div>
                          </div>
                           <div class="box-inner col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box col-md-12">
                                <div class="inner">
                                <p>Total Sales</p>
                                <h3><i class="fa fa-inr"></i> <span>{{ (int)$doctorSales}}</span> </h3>
                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="page-content section table-list show-doc">
                                  <div class="panels panel-default">
                                      <div class="panel-title">
                                         <h3>Latest Orders</h3>
                                      </div>
                                      <!-- /.panel-heading -->
                                      <div class="panel-body lat-ord">
									  <div class="table">
                        <table class="table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Date Added</th>
                                                  <th>User Type</th>
                                                  <th>Name</th>
                                                  <th>Products</th>
                                                  <th>Quantity</th>
                                                  <th>Total</th>
                                                  <th>Status</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                   if(count($doctorOrders) > 0)
                                                   {
                                                      foreach($doctorOrders as $o_key => $doctorOrder)
                                                      {
                                                        if(isset($doctorOrder->id)) {
                                                        $created_at = date("d-m-Y", strtotime($doctorOrder->created_at)); 
                                                        $type='Doctor';
                                                          ?>
                                                        <tr class="odd gradeX">
                                                          <td data-value="Order id">{{ $doctorOrder->id }}</td>
                                                          <td data-value="Placed date">{{ $created_at }}</td>
                                                          <td data-value="User type">{{ $type }}</td>
                                                          <td data-value="Doctor name">{{ Webkul\Customer\Models\Customer::CustomerName($doctorOrder->customer_id) }}</td>
                                                          <?php $orderProducts = DB::table('order_items')->where('order_id',$doctorOrder->id)->get(); 
                                                          $product_name=$product_qty=[];
                                                          foreach($orderProducts as $key => $orderProduct) {
                                                              $product_name[] = $orderProduct->name;
                                                              $product_qty[] = $orderProduct->qty_ordered;
                                                          } ?>
                                                          <td data-value="Product name">{{ implode(",",$product_name)}}</td>
                                                          <td data-value="Product quantity">{{ implode(",",$product_qty)}}</td>
                                                          <td data-value="Total amount">{{ (int)$doctorOrder->grand_total }}</td>
                                                          <td data-value="Order status">{{ ucwords(join(' ', explode("_", $doctorOrder->status))) }}</td>


                                                        </tr>
                                                <?php 
                                                       }
                                                      //if($o_key == 4) exit; 
                                                     }
                                                     }
                                                     else{ ?>
                                                        <tr><td class="text-center" colspan=9>No results Found</td></tr>
                                                     <?php   }  ?>  
                                                  </tbody>
                                              </table>
                                        </div></div>

                                        <!-- /.panel-body -->
                                    </div>
                                      <!-- /.panel -->
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                    </div>
                    @if(auth()->guard('admin')->user()->role_id != 2)
                    <div role="tabpanel" class="tab-pane fade in" id="doctor-details">
                      <div class="col-lg-12 table-list">
                          <div class="panel panel-default">
                              <!-- /.panel-heading -->
                              <div class="panel-body" style="clear: both; overflow: scroll;">
                                <form method="POST" action="{{ route('admin.doctor.update', $customer->id) }}">

            <div class="page-content">

                <div class="form-container">
                    @csrf()

                    <input name="_method" type="hidden" value="PUT">
                    <div class="form-fields col-md-12">
                        <div class="form-field col-md-12">
                           <div class="control-group col-md-3" :class="[errors.has('my_title') ? 'has-error' : '']">
                               <input type="file" name="upload_profile" id="upload-profile" class="upload-file" value="{{ old('upload_profile') }}">
                                <img class="upload-pic profile-pic" src="{!! bagisto_asset('images/dealer-profile.png') !!}">
                              </div>
                            <div class="col-md-9">
                            <div class="control-group pass cpass col-md-3" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option <?php if($customer->my_title == "Mr.") echo 'selected'; ?> value="Mr.">Mr.</option>
                                    <option <?php if($customer->my_title == "Mrs.") echo 'selected'; ?> value="Mrs.">Mrs.</option>
                                    <option <?php if($customer->my_title == "Mr. Dr..") echo 'selected'; ?> value="Mr. Dr.">Mr. Dr.</option>
                                    <option <?php if($customer->my_title == "Mrs. Dr.") echo 'selected'; ?> value="Mrs. Dr.">Mrs. Dr.</option>
                                    <option <?php if($customer->my_title == "Mr. Dr. Prof.") echo 'selected'; ?> value="Mr. Dr. Prof.">Mr. Dr. Prof.</option>
                                    <option <?php if($customer->my_title == "Mrs. Dr. Prof.") echo 'selected'; ?> value="Mrs. Dr. Prof.">Mrs. Dr. Prof.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-md-5" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First_name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'" value="{{ $customer->first_name }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.firstname') }}&quot;">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group cpass col-md-4">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name"value="{{ $customer->last_name }}">
                            </div>
                             <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">{{ __('shop::app.customer.signup-form.phonenumber') }}</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ $customer->phone_number }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.phonenumber') }}&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>

                            <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ $customer->email }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
                        </div>
                      </div>
                      <div class="form-field col-md-12">
                          <div class="control-group pass col-md-6" :class="[errors.has('clinic_name') ? 'has-error' : '']">
                            <label for="clinic_name" class="required">{{ __('shop::app.customer.signup-form.clinicname') }}</label>
                            <input type="text" class="control" name="clinic_name" v-validate="'required'" value="{{ $customer->clinic_name }}">
                            <span class="control-error" v-if="errors.has('clinic_name')">@{{ errors.first('clinic_name') }}</span>
                          </div>
                          <div class="control-group col-md-6 cpass">
                            <label for="clinic_number">Clinic Phone Number</label>
                            <input type="text" class="control" name="clinic_number"value="{{ $customer->clinic_number }}">
                          </div>
                      </div>
                      <div class="form-field col-md-12">
                        <div class="control-group pass cpass col-md-12" :class="[errors.has('clinic_address') ? 'has-error' : '']">
                          <label for="clinic_address" class="required">{{ __('shop::app.customer.signup-form.clinicaddr') }}</label>
                          <textarea class="control" name="clinic_address" v-validate="'required'">{{ $customer->clinic_address }}</textarea>
                          <span class="control-error" v-if="errors.has('clinic_address')">@{{ errors.first('clinic_address') }}</span>
                        </div>
                      </div>
                      <div class="form-field col-md-12">
                        <div class="control-group pass cpass col-md-12" :class="[errors.has('dental_license_no') ? 'has-error' : '']">
                          <label for="dental_license_no" class="required">Dental License No.</label>
                          <input type="text" class="control" name="dental_license_no" v-validate="'required'" value="{{ $customer->dental_license_no }}">
                          <span class="control-error" v-if="errors.has('dental_license_no')">@{{ errors.first('dental_license_no') }}</span>
                        </div>
                      </div>
                      <div class="form-field col-md-12">
                        <div class="control-group pass col-md-6" :class="[errors.has('country_id') ? 'has-error' : '']">
                          <label for="country_id" class="required">{{ __('shop::app.customer.signup-form.country') }}</label>
                          <input type="text" class="control" name="country_id" v-validate="'required'" disabled="disabled" value="India">
                          <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                        </div>
                        <div class="control-group cpass col-md-6" :class="[errors.has('state_id') ? 'has-error' : '']">
                          <label for="state_id" class="required">State</label>
                          <?php $states = array();
                                $states = DB::table('country_states')->where('country_id',101)->get();
                          ?>
                          <select name="state_id" id="state" class="control" v-validate="'required'">
                            <option value="">Choose your state...</option>
                            <?php foreach($states as $key => $state) { ?>
                                <option <?php if($state->id == $customer->state_id) echo 'selected'; ?> value="{{ $state->id }}">{{ $state->name }}</option>
                            <?php } ?>
                          </select>
                          <span class="control-error" v-if="errors.has('state_id')">@{{ errors.first('state_id') }}</span>
                        </div>
                      </div>
                      <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('city_id') ? 'has-error' : '']">
                                <label for="city" class="required">{{ __('shop::app.customer.signup-form.city') }}</label>
                                <?php $cities = array();
                                    $cities = DB::table('country_state_cities')->where('state_id',$customer->state_id)->get();?>
                                    <select name="city_id" id="city" class="control" v-validate="'required'">
                                        <option value="">Choose your city...</option>
                                        <?php foreach($cities as $key => $city) { ?>
                                            <option <?php if($city->id == $customer->city_id) echo 'selected'; ?> value="{{ $city->id }}">{{ $city->name }}</option>
                                        <?php } ?>
                                    </select>
                                <span class="control-error" v-if="errors.has('city_id')">@{{ errors.first('city_id') }}</span>
                            </div>
                              <div class="control-group cpass col-md-6" :class="[errors.has('pin_code') ? 'has-error' : '']">
                                    <label for="pin_code" class="required">Pin Code</label>
                                    <input type="text" class="control" name="pin_code" value="{{ $customer->pin_code }}" v-validate="'required'">
                                    <span class="control-error" v-if="errors.has('pin_code')">@{{ errors.first('pin_code') }}</span>
                            </div>

                            <!-- <div class="control-group cpass col-md-6" :class="[errors.has('dealer_commission') ? 'has-error' : '']">
                                    <label for="state_id">Dealer Commission</label>
                                    <input type="text" class="control" name="dealer_commission" value="{{ $customer->dealer_commission }}">
                                    <span class="percent">%</span>
                                    <span class="control-error" v-if="errors.has('dealer_commission')">@{{ errors.first('dealer_commission') }}</span>
                            </div> -->


                        </div>


                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('dealer_id') ? 'has-error' : '']">
                                <?php $dealers = array();
                                $dealers = DB::table('users')->where('role_id',2)->get();
                                ?>
                                <label for="city" class="required">Under Dealer</label>
                                <select name="dealer_id" id="state" class="control" v-validate="'required'">
                                    <option value="">Choose Dealer...</option>
                                    <?php foreach($dealers as $key => $dealer) { ?>
                                        <option <?php if($dealer->id == $customer->dealer_id) echo 'selected'; ?> value="{{ $dealer->id }}">{{ $dealer->first_name }} {{ $dealer->last_name }},{{ Webkul\Customer\Models\Cities::GetCityName($dealer->city_id)}}, {{ Webkul\Customer\Models\States::GetStateName($dealer->state_id)}}</option>
                                    <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                        </div>

                        
                   
                        <button class="btn btn-primary btn-lg" type="submit">
                           Update
                        </button>
                    </div>
            </div>
        </div>
        </form>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>
                  </div>

                  @endif
                    <div role="tabpanel" class="tab-pane fade in" id="orders">
					  <div class="page-content section table-list show-doc">
                          <div>
                              <div class="panel-title">
                                  <h3>Orders</h3>
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body lat-ord">
									  <div class="table">
                                   <table  class="table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Date Added</th>
                                                  <th>User Type</th>
                                                  <th>Name</th>
                                                  <th>Products</th>
                                                  <th>Quantity</th>
                                                  <th>Total</th>
                                                  <th>Status</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                   if(count($doctorOrders) >0)
                                                   {
                                                      foreach($doctorOrders as $o_key => $doctorOrder)
                                                      {
                                                        if(isset($doctorOrder->id)) {
                                                        $created_at = date("d-m-Y", strtotime($doctorOrder->created_at));
                                                        $type='Doctor'; ?>
                                                        <tr class="odd gradeX">
                                                          <td data-value="Order id">{{ $doctorOrder->id }}</td>
                                                          <td data-value="Placed date">{{ $created_at }}</td>
                                                          <td data-value="User type">{{ $type }}</td>
                                                          <td data-value="Doctor name">{{ Webkul\Customer\Models\Customer::CustomerName($doctorOrder->customer_id) }}</td>
                                                          <?php $orderProducts = DB::table('order_items')->where('order_id',$doctorOrder->id)->get(); 
                                                          $product_name=$product_qty=[];
                                                          foreach($orderProducts as $key => $orderProduct) {
                                                              $product_name[] = $orderProduct->name;
                                                              $product_qty[] = $orderProduct->qty_ordered;
                                                          } ?>
                                                          <td data-value="Product name">{{ implode(",",$product_name)}}</td>
                                                          <td data-value="Product quantity">{{ implode(",",$product_qty)}}</td>
                                                          <td data-value="Total amount">{{ (int)$doctorOrder->grand_total }}</td>
                                                          <td data-value="Order status">{{ ucwords(join(' ', explode("_", $doctorOrder->status))) }}</td>
                                                          
                                                        </tr>
                                                <?php 
                                                       }
                                                     }
                                                     }
                                                     else{ ?>
                                                        <tr><td class="text-center" colspan=9>No results found</td></tr>
                                                    <?php }  ?>  
                                                  </tbody>
                                              </table>
                                </div></div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>

                    <?php 
                    $points_added=DB::table('reward_points')->where('user_id',$customer->id)->sum('reward_points');
                    $points_credited=DB::table('reward_points')->where('user_id',$customer->id)->where('points_credited',1)->sum('reward_points'); 
                    $points_used=DB::table('orders')->where('customer_id',$customer->id)->sum('reward_points_used');

                    $coins_added=$points_added / 10;
                    $coins_credited=$points_credited / 10; 
                    $coins_used = DB::table('orders')->where('customer_id',$customer->id)->sum('reward_coin'); ?>

                    <div role="tabpanel" class="tab-pane fade in" id="points-coins">
                    <div class="dashboard-section col-lg-12">
                       <div class="count-box section col-md-12 col-lg-12">
                          <div class="box-inner marbt col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                                <div class="inner">
                                 <p>Total Points added</p>
                                 <h3>{{ $points_added }}</h3>
                                </div>
                            </div>
                          </div>
                            <div class="box-inner marbt col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                                <div class="inner">
                                 <p>Total Points Credited</p>
                                 <h3>{{ $points_credited }}</h3>
                                </div>
                            </div>
                          </div>
                          <div class="box-inner marbt col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                                <div class="inner">
                                 <p>Total Points Used</p>
                                 <h3>{{ $points_used }}</h3>
                                </div>
                            </div>
                          </div>
                      </div>
                      <div class="count-box section col-md-12 col-lg-12">
                          <div class="box-inner marbt col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                                <div class="inner">
                                 <p>Total Coins added</p>
                                 <h3>{{ $coins_added }}</h3>
                                </div>
                            </div>
                          </div>
                            <div class="box-inner marbt col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                                <div class="inner">
                                 <p>Total coins Credited</p>
                                 <h3>{{ $coins_credited }}</h3>
                                </div>
                            </div>
                          </div>
                          <div class="box-inner marbt col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                                <div class="inner">
                                 <p>Total coins Used</p>
                                 <h3>{{ $coins_used }}</h3>
                                </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>


                 <div role="tabpanel" class="tab-pane fade in" id="credit_orders">
                      <div class="col-lg-12 table-list">
                          <div class="panel panel-default">
                              <div class="panel-title">
                                  <div class="panel-heading">
                                    <i class="fa fa-list"></i> Advance Payment Orders
                                  </div>
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body" style="clear: both; overflow: scroll;">
                                   <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Date Added</th>
                                                  <th>Name</th>
                                                  <th>Products</th>
                                                  <th>Quantity</th>
                                                  <th>Total</th>
                                                  <th>Status</th>
                                                  <th>Payment Method</th>
                                                  <th>Action</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                      foreach($creditOrders as $o_key => $creditOrder)
                                                      {
                                                        if(isset($creditOrder->id)) {
                                                        $created_at = date("d-m-Y", strtotime($creditOrder->created_at));
                                                         ?>
                                                        <tr class="odd gradeX">
                                                          <td>{{ $creditOrder->id }}</td>
                                                          <td>{{ $created_at }}</td>
                                                          <td>{{ Webkul\Customer\Models\Customer::CustomerName($creditOrder->customer_id) }}</td>
                                                          <?php $orderProducts = DB::table('order_items')->where('order_id',$creditOrder->id)->get(); 
                                                          $product_name=$product_qty=[];
                                                          foreach($orderProducts as $key => $orderProduct) {
                                                              $product_name[] = $orderProduct->name;
                                                              $product_qty[] = $orderProduct->qty_ordered;
                                                          } ?>
                                                          <td>{{ implode(",",$product_name)}}</td>
                                                          <td>{{ implode(",",$product_qty)}}</td>
                                                          <td>{{ (int)$creditOrder->grand_total }}</td>
                                                          <td>{{ $creditOrder->status }}</td>
                                                          <td>{{ $creditOrder->method }}</td>
                                                          <td class="action-menu">
                                                             <a href="{{ route('admin.sales.orders.view',$creditOrder->id) }}" onclick="removeLink('Do you really want to do this?')"><i class="fa fa-eye"></i></a>
                                                          </td>
                                                        </tr>
                                                <?php 
                                                       }
                                                     }  ?>  
                                                  </tbody>
                                              </table>
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="credit_payments">
                      <div class="count-box section col-md-12 col-lg-12">
                          {{-- <div class="box-inner col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                 <p>Credit Limit</p>
                                 <h3><i class="fa fa-inr"></i> <span>{{ $credit_limit }}</span></h3>
                                </div>
                              </a>
                            </div>
                          </div>
                          <div class="box-inner col-lg-3 col-xs-6">
                          <!-- small box -->
                            <div class="small-box col-md-12">
                                <a href="#">
                                  <div class="inner">
                                  <p>Available Credit Limit</p>
                                  <h3><i class="fa fa-inr"></i> <span>{{ $available_credit_limit}}</span> </h3>
                                </div>
                                
                              </a>
                              </div>
                          </div>
                          <div class="box-inner col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                <p>Due Amount</p>
                                <h3><i class="fa fa-inr"></i> <span>{{ $credit_pending_amount}}</span> </h3>
                              </div>
                              
                            </a>
                            </div>
                          </div> --}}

                          <div class="box-inner col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                  <p>Advance</p>
                                  <h3><i class="fa fa-inr"></i> <span>{{ $credit_balance}}</span> </h3>
                                </div>
                              </a>
                            </div>
                          </div>

                          <div class="box-inner col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                  <p>Pending Approval</p>
                                  <h3><i class="fa fa-inr"></i> <span>{{ $credit_pending}}</span> </h3>
                                </div>
                              </a>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-12 col-sm-12">
                          <form id="credit-payment-form" method="POST" action="{{ route('credit_payment.payviachequeandother') }}" enctype="multipart/form-data">
                                  <div class="page-content">
                                    <div class="form-container">
                                      @csrf()
                                       <div class="form-field inner-form col-md-12">
                                        <div class="control-group col-md-6 pass">
                                            <label class="required">Amount</label>
                                            <input type="number"  class="control pay_credit_amount" name="pay_credit_amount"  value="">
                                            <span class="control-error"></span>
                                        </div>
                                        <div class="control-group col-md-6 cpass">
                                            <label for="payment_type" class="required">Paying By</label>
                                            <select class="control payment_type" name="payment_type" class="control">
                                                <option value="2">Cheque Payment</option>
                                                <option value="3">Bank Transfer/NEFT/UPI</option>
                                                <option value="0">Other Payment</option>
                                            </select>
                                            <span class="control-error"></span>
                                        </div>
                                      </div>
                       
                                        <div class="form-field cheque-sec col-md-12">
                                          <div class="control-group col-md-6 pass">
                                            <label class="required">Cheque Number</label>
                                            <input type="text" class="control cheque_number" name="cheque_number"  value="">
                                            <span class="control-error"></span>
                                          </div>
                                          <div class="control-group col-md-6 cpass">
                                            <label>Cheque Image</label>
                                            <input type="file" name="cheque_image" class="img-thumb">                            
                                          </div>
                                        </div>

                                    <div class="form-field bank-neft-upi col-md-12"  style="display:none">
                                        <div class="control-group col-md-6 pass">
                                          <label class="required">Transation ID</label>
                                          <input type="text" class="control transation_number" name="transation_number"  value="">
                                          <span class="control-error"></span>
                                        </div>
                                        <div class="control-group col-md-6 cpass">
                                          <label>Transfer Receipt</label>
                                          <input type="file" name="receipt_image" class="img-thumb">
                                        </div>
                                    </div>

                                       <div class="form-field other-payment-sec col-md-12">
                                        <div class="control-group col-md-6 pass">
                                          <label class="required">Transaction ID</label>
                                          <input type="text"  v-validate="'required'" class="control transaction_id" name="transaction_id"  value="">
                                          <span class="control-error"></span>
                                        </div>
                                        <div class="control-group col-md-6 cpass">
                                          <label>Comment</label>
                                          <textarea class="form-control" cols="50" rows="10" id="transaction_comment" name="transaction_comment"></textarea>
                                        </div>
                                       </div>
                                       
                                      <input type="hidden" name="user_id" value="{{$customer->id}}">
                                      <input type="hidden" name="payment_id" class="payment_id" value="">
                                      <div class="control-group col-md-3 cpass">
                                          <button type="submit" class="btn amount-btn btn-lg btn-primary pay-via-cheque pay-credit-amount"> Pay </button>
                                      </div>
                                  </div>
                                </div>
                              </form>
                          </div>
                      <div class="col-lg-12 table-list">
                          <div class="panel panel-default">
                              <div class="panel-title">
                                  <div class="panel-heading">
                                    <i class="fa fa-list"></i> Payments
                                  </div>
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body" style="clear: both; overflow: scroll;">
                               

                                   <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>ID</th>
                                                  <th>Payment Type</th>
                                                  <th>Payment ID</th>
                                                  <th>Cheque Number</th>
                                                  <th>Cheque Image</th>
                                                  <th>Comments</th>
                                                  <th>Amount</th>
                                                  <th>Status</th>
                                                  <th>Paid By</th>
                                                  <th>Created Date</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                      foreach($creditPayments as $o_key => $creditPayment)
                                                      {
                                                        if(isset($creditOrder->id)) {
                                                        $created_at = date("d-m-Y", strtotime($creditPayment->created_at));
                                                         ?>
                                                        <tr class="odd gradeX">
                                                          <td>{{ $creditPayment->id }}</td>
                                                          <td>{{[0=>'Other',1=>'Razorpay', 2=>'Cheque', 3=>'Bank Transfer/NEFT/UPI'][$creditPayment->payment_type]}}</td>
                                                          <td>{{ $creditPayment->payment_id }}</td>
                                                          <td>{{ $creditPayment->cheque_number}}</td>
                                                          <td>@if($creditPayment->cheque_image)
                                                            <a target="_blank" href="{{url(bagisto_asset('images/credit_images/'.$creditPayment->cheque_image))}}" class="img-enlargeable">Click here</a>
                                                            @endif</td>
                                                          <td>{{$creditPayment->comments}}</td>
                                                          <td>{{ $creditPayment->amount }}</td>
                                                          <td>
                                                            @if ($creditPayment->status == 0)
                                                              <span class="badge badge-md badge-warning">Pending</span>
                                                            @elseif ($creditPayment->status == 1)
                                                              <span class="badge badge-md badge-success" style="background-color: #007704;">Approved</span>
                                                            @elseif ($creditPayment->status == 2)
                                                              <span class="badge badge-md badge-danger" style="background-color: #e40000;">Rejected</span>
                                                            @endif
                                                          </td>
                                                          <td>{{ Webkul\Customer\Models\Customer::customerRoleName($creditPayment->paid_by) }}</td>
                                                          <td>{{ $created_at }}</td>
                                                          
                                                        </tr>
                                                <?php 
                                                       }
                                                     }  ?>  
                                                  </tbody>
                                              </table>
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>

                    



              </div>
        </div>
    </div>
  </div>
</div>

@stop
@push('scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(e){
$(".profile-pic").click(function() {
    $("#upload-profile").click();
});

$("input[type='file']").change(function () {
var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'upload-profile')
                reader.onload = profileUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });


function profileUploaded(e) {
    $('.profile-pic').attr('src', e.target.result);
};
});
</script>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
 <script type="text/javascript">
    $(document).ready(function(){



$('body').on('change','.payment_type', function(){
  /*$('.other-payment-sec').css('display', 'none');
  $('.pay-credit-amount').removeClass('pay-via-other');
  $('.pay-credit-amount').addClass('pay-via-cheque');*/
  if($(this).val() == 2){
    $('.cheque-sec').css('display', 'block');
    $('.other-payment-sec').css('display', 'none');
    $('.bank-neft-upi').css('display', 'none');
    $('.pay-credit-amount').addClass('pay-via-cheque');
    $('.pay-credit-amount').removeClass('pay-via-other');
    $('.pay-credit-amount').removeClass('pay-via-net');
  }
  if($(this).val() == 3){
    $('.bank-neft-upi').css('display', 'block');
    $('.cheque-sec').css('display', 'none');
    $('.other-payment-sec').css('display', 'none');
    $('.pay-credit-amount').addClass('pay-via-net');
    $('.pay-credit-amount').removeClass('pay-via-other');
    $('.pay-credit-amount').removeClass('pay-via-cheque');
  }
  if($(this).val() == 0){
    $('.other-payment-sec').css('display', 'block');
    $('.bank-neft-upi').css('display', 'none');
    $('.cheque-sec').css('display', 'none');
    $('.pay-credit-amount').addClass('pay-via-other');
    $('.pay-credit-amount').removeClass('pay-via-cheque');
    $('.pay-credit-amount').removeClass('pay-via-net');
  }
});

    <?php /*

 function readIMG(input,image_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+image_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $('body').on('change','input[type=file]', function(){
      var image_id = $(this).parent().find('img').attr('id');
        readIMG(this,image_id);
    });



  $('body').on('click', '.cheque-sec > div > img', function(){
    $(this).parent().find('input[type=file]').trigger('click'); 
  });


  $('body').on('click', '.bank-neft-upi > div > img', function(){
    $(this).parent().find('input[type=file]').trigger('click'); 
  });


    function readIMG(input,image_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+image_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#cheque-image").change(function(){
      readURL(this);
    }); */ ?>

    $('#credit-payment-form').on('submit',function(e){
        $(this).find('.control-error').hide();
        var payment_type = $(this).find('.payment_type').val(); // 2 - cheque, 3 - bank, 0 - others
        var transaction_id = $(this).find('.transaction_id'); //others
        var transaction_num = $(this).find('.transation_number'); //bank
        var check_number = $(this).find('.cheque_number'); //check_number
        var pay_amount = $(this).find('.pay_credit_amount');

        function sayError(ele){
            ele.parent().find('.control-error').text('This field is required').show();
        }

        var validate = true;

        if(!pay_amount.val()){
            validate = false;
            sayError(pay_amount);
        }

        if(payment_type == 2){
            if(!check_number.val()){
                validate = false;
                sayError(check_number);
            }
        }

        if(payment_type == 3){
            if(!transaction_num.val()){
                validate = false;
                sayError(transaction_num);
            }
        }

        if(payment_type == 0){
            if(!transaction_id.val()){
                validate = false;
                sayError(transaction_id);
            }
        }

        if(!validate) e.preventDefault();
    });


            function imgClickFunction(e){
                e.preventDefault();
                var src = $(this).attr('src') ?? $(this).attr('href');
                var modal;
                function removeModal(){ modal.remove(); $('body').off('keyup.modal-close'); }
                modal = $('<div>').css({
                    background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
                    backgroundSize: 'contain',
                    width:'100%', height:'100%',
                    position:'fixed',
                    zIndex:'10000',
                    top:'0', left:'0',
                    cursor: 'pointer'
                }).click(function(){
                    removeModal();
                }).appendTo('body');

                $('body').on('keyup.modal-close', function(e){
                    if(e.key==='Escape'){
                        removeModal();
                    }
                });
            }

            $('.img-thumb').on('change',function(event) {
                let input = event.target;
                let reader = new FileReader();
                reader.onload = function(){
                    let dataURL = reader.result;
                    let output = $(`<img class='imgThumb'>`).hide();
                    output.attr('src', dataURL);
                    $(input).next('.imgThumb').remove();
                    $(input).after(output);
                    output.fadeIn();
                    output.addClass('img-enlargeable').click(imgClickFunction);
                };
                reader.readAsDataURL(input.files[0]);
            });

            $('.img-enlargeable').click(imgClickFunction);
});
</script>


<style type="text/css">
    .imgThumb{
        width: 200px;
        margin: 20px 10px 0;
        cursor: pointer;
    }
</style>




@endpush

@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.customers.groups.title') }}
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/doctors') }}';"></i>Unverified Doctors</h1>
            </div>
            <div class="page-action">
            </div>
        </div>

        <div class="page-content">
            @inject('unverifiedDoctor','Webkul\Admin\DataGrids\UnverifiedDoctorDataGrid')
            {!! $unverifiedDoctor->render() !!}
        </div>
    </div>

@stop


@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.page-content').on('click','a', function(){
                if($(this).hasClass('verify')){
                    return confirm('Are you sure you want to verify this user?');
                }
                
                if($(this).hasClass('delete-user')){
                    return confirm("This action can't be undone. \nAre you sure you want to delete this user?");
                }

                if($(this).hasClass('notify')){
                    return confirm("Do you like to send email notification to this user?");
                }
            });
        })
    </script>
@endpush

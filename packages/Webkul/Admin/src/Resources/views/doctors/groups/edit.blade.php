@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.customers.groups.edit-title') }}
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.groups.update', $group->id) }}">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                        
                        {{ __('admin::app.customers.groups.edit-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.customers.groups.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <input name="_method" type="hidden" value="PUT">

                    <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                        <label for="name" class="required">
                            {{ __('admin::app.customers.groups.name') }}
                        </label>
                        <input type="text" class="control" name="name" v-validate="'required'" value="{{ $group->name }}" data-vv-as="&quot;{{ __('admin::app.customers.groups.name') }}&quot;">
                        <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('order_amount_from') ? 'has-error' : '']">
                        <label for="order_amount_from" class="required">
                            Order Amount From
                        </label>
                        <input type="text" class="control" name="order_amount_from" v-validate="'required'" value="{{ $group->order_amount_from }}" data-vv-as="Order Amount From">
                        <span class="control-error" v-if="errors.has('order_amount_from')">@{{ errors.first('order_amount_from') }}</span>
                    </div>
                    <div class="control-group" :class="[errors.has('order_amount_to') ? 'has-error' : '']">
                        <label for="order_amount_to" class="required">
                            Order Amount To
                        </label>
                        <input type="text" class="control" name="order_amount_to" v-validate="'required'" value="{{ $group->order_amount_to }}" data-vv-as="Order Amount To">
                        <span class="control-error" v-if="errors.has('order_amount_to')">@{{ errors.first('order_amount_to') }}</span>
                    </div>

                    {{-- <div class="control-group" :class="[errors.has('points_needed') ? 'has-error' : '']">
                        <label for="points_needed" class="required">
                            Points Needed
                        </label>
                        <input type="text" class="control" name="points_needed" v-validate="'required'" value="{{ $group->points_needed }}" data-vv-as="Points Needed">
                        <span class="control-error" v-if="errors.has('points_needed')">@{{ errors.first('points_needed') }}</span>
                    </div> --}}
                </div>
            </div>
        </form>
    </div>
@stop


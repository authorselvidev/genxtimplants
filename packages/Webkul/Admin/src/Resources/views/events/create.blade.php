@extends('admin::layouts.content')

@section('page_title')
    Create Events - GenXT
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.events.store') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.settings.pages.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save Event
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.pages.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-fields col-md-12">
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('event_name') ? 'has-error' : '']">
                                        <label for="menu-title" class="required">Event Name</label>
                                        <input v-validate="'required'" class="control" id="event-name" name="event_name" data-vv-as="Event Name"/>
                                        <span class="control-error" v-if="errors.has('event_name')">@{{ errors.first('event_name') }}</span>
                                    </div>

                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('event_slug') ? 'has-error' : '']">
                                        <label for="event-slug" class="required">Event Slug</label>
                                        <input v-validate="'required'" class="control" id="event-slug" name="event_slug" v-slugify data-vv-as="Event Slug"/>
                                        <span class="control-error" v-if="errors.has('event_slug')">@{{ errors.first('event_slug') }}</span>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('start_date') ? 'has-error' : '']">
                                        <label for="date_start" class="required">Event Start Date</label>
                                        <date>
                                            <input v-validate="'required'" type="text" class="control date-start" name="start_date" data-vv-as="Event Start Date">
                                        </date> 
                                         <span class="control-error" v-if="errors.has('start_date')">@{{ errors.first('start_date') }}</span>
                                    </div>
                                    <div class="control-group col-md-6 col-xs-12" :class="[errors.has('end_date') ? 'has-error' : '']">
                                        <label for="date_end" class="required">Event End Date</label>
                                        <date>
                                            <input v-validate="'required'" type="text" class="control date-end" name="end_date" data-vv-as="Event End Date">
                                        </date>
                                        <span class="control-error" v-if="errors.has('end_date')">@{{ errors.first('end_date') }}</span>
                                    </div>
                                </div>
                                <div class="form-field col-md-12">
                                    <div class="control-group col-md-12" :class="[errors.has('event_content') ? 'has-error' : '']">
                                        <label for="page-content" class="required">Event Content</label>
                                        <textarea v-validate="'required'" class="control" id="event-content" name="event_content" data-vv-as="Event Content"></textarea>
                                         <input name="image" type="file" id="upload" class="hidden" style="display:none" onchange="">
                                        <span class="control-error" v-if="errors.has('event_content')">@{{ errors.first('event_content') }}</span>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </accordian>

                    {{-- <accordian :title="'{{ __('admin::app.settings.pages.meta_desc') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('meta_title') ? 'has-error' : '']">
                                <label for="meta-title" class="required">{{ __('admin::app.settings.pages.meta_title') }}</label>
                               <textarea v-validate="'required'" class="control" id="meta-title" name="meta_title" data-vv-as="&quot;meta-title&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_title')">@{{ errors.first('meta_title') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('meta_keyword') ? 'has-error' : '']">
                                <label for="meta-keyword">{{ __('admin::app.settings.pages.meta_keyword') }}</label>
                                <textarea class="control" id="meta-keyword" name="meta_keyword" data-vv-as="&quot;meta-keyword&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_keyword')">@{{ errors.first('meta_keyword') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('meta_description') ? 'has-error' : '']">
                                <label for="meta-description">{{ __('admin::app.settings.pages.meta_description') }}</label>
                                <textarea  class="control" id="meta-description" name="meta_description" data-vv-as="&quot;meta-description&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_description')">@{{ errors.first('meta_description') }}</span>
                            </div>
                        </div>
                    </accordian> --}}
                   
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
    <script src="{{ asset('vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#channel-switcher, #locale-switcher').on('change', function (e) {
                $('#channel-switcher').val()
                var query = '?channel=' + $('#channel-switcher').val() + '&locale=' + $('#locale-switcher').val();

                window.location.href = "{{ route('admin.pages.create')  }}" + query;
            })

            tinymce.init({
                selector: 'textarea#event-content',
                height: 200,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
            });
 });
       </script>
@endpush

@extends('admin::layouts.content')

@section('page_title')
    Events - GenXT
@stop

@section('content')
    <div class="content">

        <div class="page-header page-section">
            <div class="page-title">
                <h1>Events</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.events.create') }}" class="btn btn-lg btn-primary">
                    Create
                </a>
            </div>

        </div>

        <div class="page-content">

           @inject('events','Webkul\Admin\DataGrids\EventsDataGrid')
            {!! $events->render() !!}
        </div>
    </div>
@stop
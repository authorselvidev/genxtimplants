 <div class="list-comm-details col-md-12">
                <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                    <thead>
                        <tr>
                            <th>Paid At</th>
                            <th>Commission Paid Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($commission_paid_history as $commission_paid)
                            <tr>
                            <th>{{date('d-m-Y',strtotime($commission_paid->created_at))}}</th>
                            <th><i class="fa fa-inr"></i> {{$commission_paid->commission_paid_amount}}</th>
                           
                        </tr>
                        @endforeach 
                        <tr>
                            <td class="text-right">Total</td>
                            <td><i class="fa fa-inr"></i> {{$total_paid}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
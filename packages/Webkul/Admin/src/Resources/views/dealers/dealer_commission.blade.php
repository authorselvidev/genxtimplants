@extends('admin::layouts.content')

@section('page_title')
    Show Dealer
@stop

@section('content')
<div class="content">
    <div class="page-header">
        <div class="page-title">
            <h1>
                <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dealers') }}';"></i>
                        {{ __('admin::app.dealers.dealers.title') }}
            </h1>
        </div>       
        <h3 class="text-center col-md-12">{{ Webkul\Customer\Models\Customer::CustomerName($customer->id) }}</h3>
    </div>

    <div class="page-contents">
		<div class="all-details col-md-12">
            <div class="count-box section col-md-12 col-lg-12">

                            
                            <?php
                                $total_commission = 0;
                                $total_commison_paid=0;
                                $total_commison_pending=0;

                                foreach($doctorsUnderDealer as $doctor){
                                    
                                    $doctor_orders = DB::table('orders')
                                        ->where('customer_id', $doctor->id)
                                        ->whereIn('status', ["delivered", "completed"])
                                        ->where('total_comm_amount','!=', '0')
                                        ->orderBy('created_at', 'desc')
                                        ->get();
                                        $totalCommPaid=0;

                                        
                                    if(count($doctor_orders) > 0){
                                        $dealer_comm_amount=array();
                                        foreach ($doctor_orders as $key => $doctor_order) {
                                            $totalCommPaid = DB::table('dealer_commission')->where('order_id', $doctor_order->id)->where('dealer_id', $doctor->dealer_id)->where('doctor_id',$doctor->id)->sum('commission_paid_amount');
                                            $total_commison_paid += $totalCommPaid;

                                            $dealer_comm_amt = $doctor_order->total_comm_amount;
                                            $paid_commision = DB::table('dealer_commission')->where('order_id', $doctor_order->id)->where('dealer_id',$doctor->dealer_id)->first();
                                            $comm_paid_amount=0;
                                            if(isset($paid_commision)){
                                                $comm_paid_amount = $paid_commision->commission_paid_amount;
                                            }
                                            $dealer_comm_amount[] = $dealer_comm_amt;
                                        }
                                        $total_commission += array_sum($dealer_comm_amount);
                                    }
                                }

                                $total_commison_pending = $total_commission-$total_commison_paid;

                            ?>
                                <div class="count-box section col-md-12 col-lg-12">
                                            <div class="box-inner col-lg-4 col-xs-6">
                                              <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                   <p>Total Commision</p>

                                                   <h3><i class="fa fa-inr"></i> <span>{{number_format($total_commission,2)  }}</span></h3>
                                                  </div>
                                                </a>
                                              </div>
                                            </div>
                                             <div class="box-inner col-lg-4 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                  <p>Total Commision Paid</p>
                                                  <h3><i class="fa fa-inr"></i> <span>{{number_format($total_commison_paid,2)}}</span></h3>
                                                </div>
                                                
                                              </a>
                                              </div>
                                            </div>
                                            <div class="box-inner col-lg-4 col-xs-6">
                                            
                                            <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                  <p>Total Commision Pending</p>
                                                  <h3><i class="fa fa-inr"></i> <span>{{number_format($total_commison_pending,2)}}</span></h3>
                                                </div>
                                                
                                              </a>
                                              </div>
                                            </div>
                                    </div>          
                      </div>


                      @foreach($doctorsUnderDealer as $dr_key => $doctor)
                        <?php 
                            $doctor_orders = DB::table('orders')
                                        ->where('customer_id', $doctor->id)
                                        ->whereIn('status', ["delivered", "completed"])
                                        ->where('total_comm_amount','!=', '0')
                                        ->orderBy('created_at', 'desc')
                                        ->get();
                                        $totalCommPaid=0;
                        ?>

                            
                          @if(count($doctor_orders) > 0)

                             <accordian :title="'{{ Webkul\Customer\Models\Customer::CustomerName($doctor->id) }}'" :active="false">
                                    <div slot="body">
                                        <div class="panels panel-default">
                                      <div class="panel-body">
                                        {{-- <div style="display: flex; justify-content:flex-end; padding:10px">
                                          <button class="btn btn-small btn-primary pay-all-orders" data-doctorid="{{$doctor->id}}">Pay all Orders</button>
                                        </div>    --}}
                              <?php 
                                    
                                    $dealer_comm_amount=array();
                                    $totalCommPaid = 0;
                                    $totalCommAmount = 0;
                                    foreach ($doctor_orders as $key => $doctor_order) {
                                        $totalCommPaid += DB::table('dealer_commission')->where('order_id', $doctor_order->id)->where('dealer_id', $doctor->dealer_id)->where('doctor_id',$doctor->id)->sum('commission_paid_amount');
                                        $totalCommAmount += $doctor_order->total_comm_amount;
                                        //$paid_commision = DB::table('dealer_commission')->where('order_id', $doctor_order->id)->where('dealer_id',$doctor->dealer_id)->first();
                                        //$comm_paid_amount=0;
                                        //if(isset($paid_commision)){
                                          //$comm_paid_amount = $paid_commision->commission_paid_amount;
                                        //}
                                        //$dealer_comm_amount[] = $dealer_comm_amt - $comm_paid_amount;
                                    }
                            //dd($dealer_comm_amount);

                            //$total_comm_amount = array_sum($dealer_comm_amount);
                            $total_comm_amount = $totalCommAmount - $totalCommPaid;
                    
                    
                               ?>
                                         <div class="count-box section col-md-12 col-lg-12">
                                             <div class="box-inner col-lg-4 col-xs-6">
                                              <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                   <p>Total Commission</p>

                                                   <h3><i class="fa fa-inr"></i> <span>{{ number_format($totalCommAmount,2) }}</span></h3>
                                                  </div>
                                                </a>
                                              </div>
                                            </div>
                                            <div class="box-inner col-lg-4 col-xs-6">
                                              <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                   <p>Total Commission Paid</p>

                                                   <h3><i class="fa fa-inr"></i> <span>{{ number_format($totalCommPaid,2) }}</span></h3>
                                                  </div>
                                                </a>
                                              </div>
                                            </div>
                                             <div class="box-inner col-lg-4 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                  <p>Total Commission to pay</p>
                                                  <h3><i class="fa fa-inr"></i> <span>{{ number_format($total_comm_amount,2) }} </span></h3>
                                                </div>
                                                
                                              </a>
                                              </div>
                                            </div>

                      </div>
                        <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Order Date</th>
                                                
                                                  <th>Order Total</th>
                                                  <th>Commission Amount</th>
                                                  <th>Commission paid</th>
                                                  <th>Order Status</th>
                                                  
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                @foreach($doctor_orders as $doctor_order)
                                  <?php $created_at = date("d-m-Y", strtotime($doctor_order->created_at)); ?>
                                                        <tr class="odd gradeX">
                                                          <td><a href="{{ route('admin.sales.orders.view',$doctor_order->id) }}">{{ $doctor_order->id }}</a></td>
                                                          <td>{{ $created_at }}</td>
                                                         
                                                        
                                                          <td><i class="fa fa-inr"></i> {{ number_format($doctor_order->grand_total,2) }}</td>

                                                           <?php
                        $dealer_comm_amt = $doctor_order->total_comm_amount;
                        $comm_paid_amount=0;
                        $commission_paid_amount = DB::table('dealer_commission')->where('order_id',$doctor_order->id)->where('dealer_id',$doctor->dealer_id)->sum('commission_paid_amount');
                        
                        if($commission_paid_amount > 0){
                            $comm_paid_amount = $commission_paid_amount;
                        }
                        //$dealer_comm_amt = $dealer_comm_amt - $comm_paid_amount;
                        ?>
                                                          <td><i class="fa fa-inr"></i><a class="dealer-comm-amt" data-order_id="{{$doctor_order->id}}" data-toggle="modal" data-target="#ShowCommissionDetails" href="">{{ number_format($dealer_comm_amt,2) }}</a></td>
                                                          <td><i class="fa fa-inr"></i><a class="dealer-comm-paid-history" data-order_id="{{$doctor_order->id}}" data-toggle="modal" data-target="#CommissionPaidHistory" href="">{{ number_format($comm_paid_amount,2) }}</a></td>
                                                          <td>{{ $doctor_order->status }}</td>
                                                          
                                                        </tr>
                                                        @endforeach

                                                  </tbody>
                                              </table>
                                        </div>

                                    </div>

                                      </div>
                                  </accordian>
                                @endif
                              
                      @endforeach
                  
                </div>
                              <!-- /.panel -->
                        </div>
                    </div>

                    
            	</div>
    		</div>
		</div>
	</div>
</div>
 <div class="modal fade" id="ShowCommissionDetails" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Commission Details</h4>
        </div>
        <div class="modal-body col-md-12">
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>
   <div class="modal fade" id="CommissionPaidHistory" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Commission Paid History</h4>
        </div>
        <div class="modal-body col-md-12">
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>
@stop
@push('scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(e){
$(".profile-pic").click(function() {
    $("#upload-profile").click();
});

$("input[type='file']").change(function () {
var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'upload-profile')
                reader.onload = profileUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });


function profileUploaded(e) {
    $('.profile-pic').attr('src', e.target.result);
};

    $('body').find('.dealer-comm-amt').on('click', function(event){
            var order_id = $(this).data('order_id');
            $.ajax({
                type: "POST",
                url: "{{route('admin.dealer.comm_details')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    order_id: order_id
                },
                success: function(data) {
                    $('#ShowCommissionDetails').find('.modal-body').html(data.view);
                }
            });
    });

    $('body').find('.dealer-comm-paid-history').on('click', function(event){
            var order_id = $(this).data('order_id');
            $.ajax({
                type: "POST",
                url: "{{route('admin.dealer.comm_paid_history')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    order_id: order_id,
                    dealer_id: "{{ $customer->id }}"
                },
                success: function(data) {
                    $('#CommissionPaidHistory').find('.modal-body').html(data.view);
                }
            });
    }); 


});
</script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
 <script type="text/javascript">
    $(document).ready(function(){



$('body').on('change','.payment_type', function(){
  /*$('.other-payment-sec').css('display', 'none');
  $('.pay-credit-amount').removeClass('pay-via-other');
  $('.pay-credit-amount').addClass('pay-via-cheque');*/
  if($(this).val() == 2){
    $('.other-payment-sec').css('display', 'block');
    $('.cheque-sec').css('display', 'none');
    $('.pay-credit-amount').addClass('pay-via-other');
    $('.pay-credit-amount').removeClass('pay-via-cheque');
  }
  if($(this).val() == 1){
    $('.other-payment-sec').css('display', 'none');
    $('.cheque-sec').css('display', 'block');
    $('.pay-credit-amount').addClass('pay-via-cheque');
    $('.pay-credit-amount').removeClass('pay-via-other');
  }
});
/*$('body').on('click','.pay-via-cheque',function() {
  $('#credit-payment-form').submit();
});*/

   function readIMG(input,image_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+image_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $('body').on('change','input[type=file]', function(){
      var image_id = $(this).parent().find('img').attr('id');
        readIMG(this,image_id);
    });



$('body').on('click', '.cheque-sec > div > img', function(){
 $(this).parent().find('input[type=file]').trigger('click'); 
});


                    function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#display-image').css('display', 'block');
                        $('#display-image').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }


            
            $("#cheque-image").change(function(){
                readURL(this);
            });


                });
</script>


@endpush
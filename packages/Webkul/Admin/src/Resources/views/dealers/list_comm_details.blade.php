 <div class="list-comm-details col-md-12">
                <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Sale Price</th>
                            <th>Commission (%)</th>
                            <th>Commission Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($order_items as $item)
                            <tr>
                            <th>{{$item->name}}</th>
                            @if(isset($item->discounted_subtotal))
                                <th><i class="fa fa-inr"></i> {{ number_format($item->discounted_subtotal,2) }}</th>
                            @else
                                <th><i class="fa fa-inr"></i> {{ number_format($item->total,2) }}</th>
                            @endif
                            <th> {{$item->dealer_comm_percent}}</th>
                            <th><i class="fa fa-inr"></i> {{ number_format($item->dealer_comm_amount,2) }}</th>
                        </tr>
                        @endforeach 
                        <tr>
                            <td colspan="3" class="text-right">Total</td>
                            <td><i class="fa fa-inr"></i> {{ number_format($order->total_comm_amount,2) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

@extends('admin::layouts.content')

@section('page_title')
    Show Dealer
@stop

@section('content')
<div class="content">
    <div class="page-header">
        <div class="page-title">
            <h1>
                <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dealers') }}';"></i>
                {{ __('admin::app.dealers.dealers.title') }}
            </h1>
        </div>       
        <h3 class="text-center col-md-12">{{ Webkul\Customer\Models\Customer::CustomerName($customer->id) }}</h3>
    </div>

    <div class="page-contents">
        <div class="all-details col-md-12">
            <div class="tab" role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#dashboard" role="tab" data-toggle="tab">Dashboard</a></li>
                    <li role="presentation"><a href="#dealer-details" class="notHavMentor" role="tab" data-toggle="tab">Dealer Details</a></li>
                    <li role="presentation"><a href="#manage-doctors" role="tab" data-toggle="tab">Manage Doctors</a></li>
                    <li role="presentation"><a href="#payment-comm" role="tab" data-toggle="tab">Commission</a></li>
                    <li role="presentation"><a href="#orders" role="tab" data-toggle="tab">Doctor Orders</a></li>
                    <li role="presentation"><a href="#purchase" role="tab" data-toggle="tab">Purchases</a></li>
                    {{-- <li role="presentation"><a href="#credit_orders" role="tab" data-toggle="tab">Payment Orders</a></li> --}}
                    <li role="presentation"><a href="#credit_payments" role="tab" data-toggle="tab">Payments</a></li>

                    @if(auth()->guard('admin')->user()->permission_to_create_order == 1 || auth()->guard('admin')->user()->role_id == 1)
                        {{-- <li role="presentation"><a href="{{route('admin.sales.custom_orders.create',$customer->id)}}">Create Order</a></li> --}}
                        <li role="presentation"><a href="{{route('admin.sales.custom_orders.cart',$customer->id)}}">Manage Cart</a></li>
                    @endif
                </ul>
                <div class="tab-content tabs">
                    <div role="tabpanel" class="tab-pane fade in active" id="dashboard">
                    <div class="dashboard-section col-lg-12">
                        <div class="count-box section col-md-12 col-lg-12">
                                        <div class="box-inner col-lg-3 col-xs-6">
                                              <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                   <p>Total Doctor Orders</p>
                                                   <h3>{{ $totalDoctorOrders }}</h3>
                                                  </div>
                                                </a>
                                              </div>
                                            </div>
                                             <div class="box-inner col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                  <p>Total Sales of Doctor</p>
                                                  <h3><i class="fa fa-inr"></i> <span>{{ (int)$doctorSales}}</span> </h3>
                                                </div>
                                                
                                              </a>
                                              </div>
                                            </div>

                                             <div class="box-inner col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                  <p>Total Doctors</p>
                                                  <h3>{{ $totalDoctors }}</h3>
                                                </div>
                                                
                                              </a>
                                              </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 section table-list">
                                            <div class="panels panel-default">
                                                <div class="panel-title">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-shopping-cart"></i> Latest Doctor Orders
                                                    </div>
                                                </div>
                                                <!-- /.panel-heading -->
                                                <div class="panel-body" style="clear: both; overflow: scroll;">
                                                <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Date Added</th>
                                                  <th>Type</th>
                                                  <th>Name</th>
                                                  <th>Products</th>
                                                  <th>Quantity</th>
                                                  <th>Total</th>
                                                  <th>Status</th>
                                                  <th>Action</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                   if(count($doctorOrders) > 0)
                                                   {
                                                      foreach($doctorOrders as $o_key => $doctorOrder)
                                                      {
                                                        if(isset($doctorOrder->id) && $doctorOrder->role_id==3) {
                                                        $created_at = date("d-m-Y", strtotime($doctorOrder->created_at)); 
                                                        $type='Doctor';
                                                          ?>
                                                        <tr class="odd gradeX">
                                                          <td>{{ $doctorOrder->id }}</td>
                                                          <td>{{ $created_at }}</td>
                                                          <td>{{ $type }}</td>
                                                          <td>{{ Webkul\Customer\Models\Customer::CustomerName($doctorOrder->customer_id) }}</td>
                                                          <?php $orderProducts = DB::table('order_items')->where('order_id',$doctorOrder->id)->get(); 
                                                          $product_name=$product_qty=[];
                                                          foreach($orderProducts as $key => $orderProduct) {
                                                              $product_name[] = $orderProduct->name;
                                                              $product_qty[] = $orderProduct->qty_ordered;
                                                          } ?>
                                                          <td>{{ implode(",",$product_name)}}</td>
                                                          <td>{{ implode(",",$product_qty)}}</td>
                                                          <td>{{ (int)$doctorOrder->grand_total }}</td>
                                                          <td>{{ $doctorOrder->status }}</td>
                                                          <td class="action-menu">
                                                             <a href="{{ route('admin.sales.orders.view',$doctorOrder->id) }}" onclick="removeLink('Do you really want to do this?')"><i class="fa fa-eye"></i></a>
                                                          </td>
                                                        </tr>
                                                <?php 
                                                       }
                                                      //if($o_key == 4) exit; 
                                                     }
                                                     }
                                                     else{ ?>
                                                        <tr><td>No results Found</td></tr>
                                                     <?php   }  ?>  
                                                  </tbody>
                                              </table>
                                                </div>

                                                <!-- /.panel-body -->
                                            </div>
                                              <!-- /.panel -->
                                        </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                    </div>
                    
                    <div role="tabpanel" class="tab-pane fade in" id="dealer-details">
                        <div class="col-lg-12 table-list">
                            <div class="panel panel-default">
                                <!-- /.panel-heading -->
                                <div class="panel-body" style="clear: both; overflow: scroll;">
          <form method="POST" action="{{ route('admin.dealer.update', $customer->id) }}">

            <div class="page-content">

                <div class="form-container">
                    @csrf()

                    <input name="_method" type="hidden" value="PUT">
                    <input name="dealer_commission" type="hidden" value="{{$customer->dealer_commission}}">
                    <div class="form-fields col-md-12">
                        <div class="form-field col-md-12">
                           <div class="control-group col-md-3" :class="[errors.has('my_title') ? 'has-error' : '']">
                               <input type="file" name="upload_profile" id="upload-profile" class="upload-file" value="{{ old('upload_profile') }}">
                                <img class="upload-pic profile-pic" src="{!! bagisto_asset('images/dealer-profile.png') !!}">
                              </div>
                            <div class="col-md-9">
                            <div class="control-group pass cpass col-md-3" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option <?php if($customer->my_title == "Mr.") echo 'selected'; ?> value="Mr.">Mr.</option>
                                    <option <?php if($customer->my_title == "Mrs.") echo 'selected'; ?> value="Mrs.">Mrs.</option>
                                    <option <?php if($customer->my_title == "Mr. Dr..") echo 'selected'; ?> value="Mr. Dr.">Mr. Dr.</option>
                                    <option <?php if($customer->my_title == "Mrs. Dr.") echo 'selected'; ?> value="Mrs. Dr.">Mrs. Dr.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-md-5" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'" value="{{ $customer->first_name }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.first_name') }}&quot;">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group cpass col-md-4">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name" value="{{ $customer->last_name }}">
                            </div>
                             <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">{{ __('shop::app.customer.signup-form.phonenumber') }}</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ $customer->phone_number }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.phonenumber') }}&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>

                            <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ $customer->email }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
                        </div>
                      </div>
                        <div class="form-field col-md-12">
                           
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('country_id') ? 'has-error' : '']">
                                <label for="country_id" class="required">{{ __('shop::app.customer.signup-form.country') }}</label>
                                <input type="text" class="control" name="country_id" v-validate="'required'" disabled="disabled" value="India">
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                            <div class="control-group cpass col-md-6" :class="[errors.has('state_id') ? 'has-error' : '']">
                                <label for="state_id" class="required">State</label>
                                <?php $states = array();
                                  $states = DB::table('country_states')->where('country_id',101)->get();?>
                                  <select name="state_id" id="state" class="control" v-validate="'required'">
                                      <option value="">Choose your state...</option>
                                      <?php foreach($states as $key => $state) { ?>
                                        <option <?php if($state->id == $customer->state_id) echo 'selected'; ?> value="{{ $state->id }}">{{ $state->name }}</option>
                                      <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('state_id')">@{{ errors.first('state_id') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('city_id') ? 'has-error' : '']">
                                <label for="city" class="required">{{ __('shop::app.customer.signup-form.city') }}</label>
                                <?php $cities = array();
                                    $cities = DB::table('country_state_cities')->where('state_id',$customer->state_id)->get();?>
                                    <select name="city_id" id="city" class="control" v-validate="'required'">
                                        <option value="">Choose your city...</option>
                                        <?php foreach($cities as $key => $city) { ?>
                                            <option <?php if($city->id == $customer->city_id) echo 'selected'; ?> value="{{ $city->id }}">{{ $city->name }}</option>
                                        <?php } ?>
                                    </select>
                                <span class="control-error" v-if="errors.has('city_id')">@{{ errors.first('city_id') }}</span>
                            </div>
                              <div class="control-group cpass col-md-6" :class="[errors.has('pin_code') ? 'has-error' : '']">
                                    <label for="pin_code">Pin Code</label>
                                    <input type="text" class="control" name="pin_code" value="{{ $customer->pin_code }}">
                                    <span class="control-error" v-if="errors.has('pin_code')">@{{ errors.first('pin_code') }}</span>
                            </div>

                            <!-- <div class="control-group cpass col-md-6" :class="[errors.has('dealer_commission') ? 'has-error' : '']">
                                    <label for="state_id">Dealer Commission</label>
                                    <input type="text" class="control" name="dealer_commission" value="{{ $customer->dealer_commission }}">
                                    <span class="percent">%</span>
                                    <span class="control-error" v-if="errors.has('dealer_commission')">@{{ errors.first('dealer_commission') }}</span>
                            </div> -->
                        </div>

                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-4" :class="[errors.has('company_name') ? 'has-error' : '']">
                                    <label for="company_name">Company Name</label>
                                    <input type="text" class="control" name="company_name" value="{{ $customer->company_name }}">
                                    <span class="control-error" v-if="errors.has('company_name')">@{{ errors.first('company_name') }}</span>
                            </div>

                            <div class="control-group pass col-md-4" :class="[errors.has('gst_no') ? 'has-error' : '']">
                                    <label for="gst_no">GST No</label>
                                    <input type="text" class="control" name="gst_no" value="{{ $customer->gst_no }}">
                                    <span class="control-error" v-if="errors.has('gst_no')">@{{ errors.first('gst_no') }}</span>
                            </div>

                            <div class="control-group cpass col-md-4" :class="[errors.has('fda_licence_no') ? 'has-error' : '']">
                                    <label for="fda_licence_no">FDA Licence No</label>
                                    <input type="text" class="control" name="fda_licence_no" value="{{ $customer->fda_licence_no }}">
                                    <span class="control-error" v-if="errors.has('fda_licence_no')">@{{ errors.first('fda_licence_no') }}</span>
                            </div>
                        </div>
                            
                    <div>
                      <button class="btn btn-primary btn-lg" type="submit">Update</button>
                    </div>
                </div>
            </div>
        </div>
        </form>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>
                  </div>

                    <div role="tabpanel" class="tab-pane fade in" id="manage-doctors">
                        <div class="col-lg-12 table-list">
                            <div class="panel panel-default">
                                <div class="panel-title">
                                    <div class="panel-heading">
                                        <i class="fa fa-list"></i> Manage Doctors
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body" style="clear: both; overflow: scroll;">
                                <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>Name</th>
                                                  <th>Email</th>
                                                  <th>Phone Number</th>
                                                  <th>Dealer</th>
                                                  <th>Date</th>
                                                  <th>Actions</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                      foreach($doctorsUnderDealer as $key => $doctor)
                                                      {
                                                        if(isset($doctor->id)) {
                                                        $created_at = date("d-m-Y", strtotime($doctor->created_at)); ?>
                                                        <tr class="odd gradeX">
                                                          <td>{{ $doctor->first_name }} {{ $doctor->last_name }}</td>
                                                          <td>{{ $doctor->email }}</td>
                                                          <td>{{ $doctor->phone_number }}</td>
                                                          <td>{{  Webkul\Customer\Models\Customer::CustomerName($doctor->dealer_id) }}</td>
                                                          <td>{{ $created_at }}</td>
                                                          <td class="actions">
                                                            <div>
                                                                <a href="{{route('admin.doctor.edit', $doctor->id)}}"><span class="icon pencil-lg-icon"></span></a><a href="{{route('admin.doctor.delete', $doctor->id)}}">
                                                                    <span class="icon trash-icon"></span></a></div>
                                                          </td>
                                                        </tr>
                                                <?php 
                                                       }
                                                     }  ?>  
                                                  </tbody>
                                              </table>
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>

                   <div role="tabpanel" class="tab-pane fade in" id="payment-comm">
                      <div class="count-box section col-md-12 col-lg-12">
                            
                            <?php
                                $total_commission = 0;
                                $total_commison_paid=0;
                                $total_commison_pending=0;

                                foreach($doctorsUnderDealer as $doctor){
                                    
                                    $doctor_orders = DB::table('orders')
                                        ->where('customer_id', $doctor->id)
                                        ->whereIn('status', ["delivered", "completed"])
                                        ->where('total_comm_amount','!=', '0')
                                        ->orderBy('created_at', 'desc')
                                        ->get();
                                        $totalCommPaid=0;

                                        
                                    if(count($doctor_orders) > 0){
                                        $dealer_comm_amount=array();
                                        foreach ($doctor_orders as $key => $doctor_order) {
                                            $totalCommPaid = DB::table('dealer_commission')->where('order_id', $doctor_order->id)->where('dealer_id', $doctor->dealer_id)->where('doctor_id',$doctor->id)->sum('commission_paid_amount');
                                            $total_commison_paid += $totalCommPaid;

                                            $dealer_comm_amt = $doctor_order->total_comm_amount;
                                            $paid_commision = DB::table('dealer_commission')->where('order_id', $doctor_order->id)->where('dealer_id',$doctor->dealer_id)->first();
                                            $comm_paid_amount=0;
                                            if(isset($paid_commision)){
                                                $comm_paid_amount = $paid_commision->commission_paid_amount;
                                            }
                                            $dealer_comm_amount[] = $dealer_comm_amt;
                                        }
                                        $total_commission += array_sum($dealer_comm_amount);
                                    }
                                }

                                $total_commison_pending = $total_commission-$total_commison_paid;

                            ?>
                                <div style="display: flex; justify-content:flex-end; padding:10px">
                                  <input type="number" name="pay_amount" min="{{ 1 }}" min="{{ $total_commison_pending }}" style="margin-right: 20px;" @if($total_commison_pending==0) disabled @endif>
                                  <button class="btn btn-small btn-primary pay-all-doctors" data-dealerid="{{$customer->id}}" @if($total_commison_pending==0) disabled @endif>Pay Commission</button>
                                </div>
                                <div class="count-box section col-md-12 col-lg-12">
                                            <div class="box-inner col-lg-4 col-xs-6">
                                              <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                   <p>Total Commision</p>

                                                   <h3><i class="fa fa-inr"></i> <span>{{number_format($total_commission,2)  }}</span></h3>
                                                  </div>
                                                </a>
                                              </div>
                                            </div>
                                             <div class="box-inner col-lg-4 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                  <p>Total Commision Paid</p>
                                                  <h3><i class="fa fa-inr"></i> <span>{{number_format($total_commison_paid,2)}}</span></h3>
                                                </div>
                                                
                                              </a>
                                              </div>
                                            </div>
                                            <div class="box-inner col-lg-4 col-xs-6">
                                            
                                            <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                  <p>Total Commision Pending</p>
                                                  <h3><i class="fa fa-inr"></i> <span>{{number_format($total_commison_pending,2)}}</span></h3>
                                                </div>
                                                
                                              </a>
                                              </div>
                                            </div>
                                    </div>          
                      </div>


                      @foreach($doctorsUnderDealer as $dr_key => $doctor)
                        <?php 
                            $doctor_orders = DB::table('orders')
                                        ->where('customer_id', $doctor->id)
                                        ->whereIn('status', ["delivered", "completed"])
                                        ->where('total_comm_amount','!=', '0')
                                        ->orderBy('created_at', 'desc')
                                        ->get();
                                        $totalCommPaid=0;
                        ?>

                            
                          @if(count($doctor_orders) > 0)

                             <accordian :title="'{{ Webkul\Customer\Models\Customer::CustomerName($doctor->id) }}'" :active="false">
                                    <div slot="body">
                                        <div class="panels panel-default">
                                      <div class="panel-body" style="clear: both; overflow: scroll;">
                                        {{-- <div style="display: flex; justify-content:flex-end; padding:10px">
                                          <button class="btn btn-small btn-primary pay-all-orders" data-doctorid="{{$doctor->id}}">Pay all Orders</button>
                                        </div>    --}}
                              <?php 
                                    
                                    $dealer_comm_amount=array();
                                    $totalCommPaid = 0;
                                    $totalCommAmount = 0;
                                    foreach ($doctor_orders as $key => $doctor_order) {
                                        $totalCommPaid += DB::table('dealer_commission')->where('order_id', $doctor_order->id)->where('dealer_id', $doctor->dealer_id)->where('doctor_id',$doctor->id)->sum('commission_paid_amount');
                                        $totalCommAmount += $doctor_order->total_comm_amount;
                                        //$paid_commision = DB::table('dealer_commission')->where('order_id', $doctor_order->id)->where('dealer_id',$doctor->dealer_id)->first();
                                        //$comm_paid_amount=0;
                                        //if(isset($paid_commision)){
                                          //$comm_paid_amount = $paid_commision->commission_paid_amount;
                                        //}
                                        //$dealer_comm_amount[] = $dealer_comm_amt - $comm_paid_amount;
                                    }
                            //dd($dealer_comm_amount);

                            //$total_comm_amount = array_sum($dealer_comm_amount);
                            $total_comm_amount = $totalCommAmount - $totalCommPaid;
                    
                    
                               ?>
                                         <div class="count-box section col-md-12 col-lg-12">
                                            <div class="box-inner col-lg-4 col-xs-6">
                                              <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                   <p>Total Commission</p>

                                                   <h3><i class="fa fa-inr"></i> <span>{{ number_format($totalCommAmount,2) }}</span></h3>
                                                  </div>
                                                </a>
                                              </div>
                                            </div>
                                            <div class="box-inner col-lg-4 col-xs-6">
                                              <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                   <p>Total Commission Paid</p>

                                                   <h3><i class="fa fa-inr"></i> <span>{{ number_format($totalCommPaid,2) }}</span></h3>
                                                  </div>
                                                </a>
                                              </div>
                                            </div>
                                             <div class="box-inner col-lg-4 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box col-md-12">
                                                <a href="#">
                                                  <div class="inner">
                                                  <p>Total Commission to pay</p>
                                                  <h3><i class="fa fa-inr"></i> <span>{{ number_format($total_comm_amount,2) }} </span></h3>
                                                </div>
                                                
                                              </a>
                                              </div>
                                            </div>

                      </div>
                        <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Order Date</th>
                                                
                                                  <th>Order Total</th>
                                                  <th>Commission Amount</th>
                                                  <th>Commission paid</th>
                                                  <th>Order Status</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                @foreach($doctor_orders as $doctor_order)
                                  <?php $created_at = date("d-m-Y", strtotime($doctor_order->created_at)); ?>
                                                        <tr class="odd gradeX">
                                                          <td><a href="{{ route('admin.sales.orders.view',$doctor_order->id) }}">{{ $doctor_order->id }}</a></td>
                                                          <td>{{ $created_at }}</td>
                                                         
                                                        
                                                          <td><i class="fa fa-inr"></i> {{ number_format($doctor_order->grand_total,2) }}</td>

                                                           <?php
                        $dealer_comm_amt = $doctor_order->total_comm_amount;
                        $comm_paid_amount=0;
                        $commission_paid_amount = DB::table('dealer_commission')->where('order_id',$doctor_order->id)->where('dealer_id',$doctor->dealer_id)->sum('commission_paid_amount');
                        
                        if($commission_paid_amount > 0){
                            $comm_paid_amount = $commission_paid_amount;
                        }
                        //$dealer_comm_amt = $dealer_comm_amt - $comm_paid_amount;
                        ?>
                                                          <td><i class="fa fa-inr"></i><a class="dealer-comm-amt" data-order_id="{{$doctor_order->id}}" data-toggle="modal" data-target="#ShowCommissionDetails" href="">{{ number_format($dealer_comm_amt,2) }}</a></td>
                                                          <td><i class="fa fa-inr"></i><a class="dealer-comm-paid-history" data-order_id="{{$doctor_order->id}}" data-toggle="modal" data-target="#CommissionPaidHistory" href="">{{ number_format($comm_paid_amount,2) }}</a></td>
                                                          <td>{{ $doctor_order->status }}</td>
                                                        </tr>
                                                        @endforeach

                                                  </tbody>
                                              </table>
                                        </div>

                                    </div>

                                      </div>
                                  </accordian>
                                @endif
                              
                      @endforeach
                  
                </div>

                    <div role="tabpanel" class="tab-pane fade in" id="orders">

                      <div class="count-box section col-md-12 col-lg-12">
                          <div class="box-inner col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                 <p>Total Orders</p>
                                 <h3>{{ $doctorOrders->count() }}</h3>
                                </div>
                              </a>
                            </div>
                          </div>
                           <div class="box-inner col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                <p>Total Purchase</p>
                                <h3><i class="fa fa-inr"></i> <span>{{ $doctorOrders->sum('grand_total') }}</span> </h3>
                              </div>
                              
                            </a>
                            </div>
                          </div>
                      </div>


                        <div class="col-lg-12 table-list">
                            <div class="panel panel-default">
                                <div class="panel-title">
                                    <div class="panel-heading">
                                        <i class="fa fa-list"></i> Orders
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body" style="clear: both; overflow: scroll;">
                                   <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Date Added</th>
                                                  <th>Type</th>
                                                  <th>Name</th>
                                                  <th>Products</th>
                                                  <th>Quantity</th>
                                                  <th>Total</th>
                                                  <th>Status</th>
                                                  <th>Action</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                      foreach($doctorOrders as $o_key => $doctorOrder)
                                                      {
                                                        if(isset($doctorOrder->id)) {
                                                        $created_at = date("d-m-Y", strtotime($doctorOrder->created_at));
                                                        $type='Dealer';
                                                        if($doctorOrder->role_id==3) $type='Doctor'; ?>
                                                        <tr class="odd gradeX">
                                                          <td>{{ $doctorOrder->id }}</td>
                                                          <td>{{ $created_at }}</td>
                                                          <td>{{ $type }}</td>
                                                          <td>{{ Webkul\Customer\Models\Customer::CustomerName($doctorOrder->customer_id) }}</td>
                                                          <?php $orderProducts = DB::table('order_items')->where('order_id',$doctorOrder->id)->get(); 
                                                          $product_name=$product_qty=[];
                                                          foreach($orderProducts as $key => $orderProduct) {
                                                              $product_name[] = $orderProduct->name;
                                                              $product_qty[] = $orderProduct->qty_ordered;
                                                          } ?>
                                                          <td>{{ implode(",",$product_name)}}</td>
                                                          <td>{{ implode(",",$product_qty)}}</td>
                                                          <td>{{ number_format($doctorOrder->grand_total,2)  }}</td>
                                                          <td>{{ $doctorOrder->status }}</td>
                                                          <td class="action-menu">
                                                            <a href="{{ route('admin.sales.orders.view',$doctorOrder->id) }}" onclick="removeLink('Do you really want to do this?')"><i class="fa fa-eye"></i></a>
                                                          </td>
                                                        </tr>
                                                <?php 
                                                       }
                                                     }  ?>  
                                                  </tbody>
                                              </table>
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="purchase">
                    <div class="dashboard-section col-lg-12">
                       <div class="count-box section col-md-12 col-lg-12">
                          <div class="box-inner col-lg-3 col-xs-6">
                            <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                 <p>Total Orders</p>
                                 <h3>{{ $totalDealerOrders }}</h3>
                                </div>
                              </a>
                            </div>
                          </div>

                          <div class="box-inner col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box col-md-12">
                                <a href="#">
                                  <div class="inner">
                                  <p>Total Purchase</p>
                                  <h3><i class="fa fa-inr"></i> <span>{{ number_format($dealerSales, 2)}}</span> </h3>
                                </div>
                                
                              </a>
                            </div>
                          </div>

                          <div class="box-inner col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box col-md-12">
                                <a href="#">
                                  <div class="inner">
                                    <p>Paid Purchases</p>
                                    <h3><i class="fa fa-inr"></i> <span>{{ number_format(($dealerSales-$credit_pending_amount),2) }}</span> </h3>
                                  </div>                                
                              </a>
                            </div>
                          </div>

                          <div class="box-inner col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box col-md-12">
                                <a href="#">
                                  <div class="inner">
                                    <p>Pending Payment</p>
                                    <h3><i class="fa fa-inr"></i> <span>{{ number_format($credit_pending_amount, 2) }}</span> </h3>
                                  </div>                                
                              </a>
                            </div>
                          </div>
                      </div>

                      <div class="col-lg-12 section table-list">
                                  <div class="panels panel-default">
                                      <div class="panel-title">
                                          <div class="panel-heading">
                                            <i class="fa fa-shopping-cart"></i> All Orders
                                          </div>
                                      </div>
                                      <!-- /.panel-heading -->
                                      <div class="panel-body" style="clear: both; overflow: scroll;">
                        <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Date Added</th>
                                                  <th>Type</th>
                                                  <th>Name</th>
                                                  <th>Products</th>
                                                  <th>Quantity</th>
                                                  <th>Total</th>
                                                  <th>Status</th>
                                                  <th>Action</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php
                                                   if(count($dealerOrders))
                                                   {
                                                      foreach($dealerOrders as $o_key => $dealerOrder)
                                                      {
                                                        if(isset($dealerOrder->id)) {
                                                        $created_at = date("d-m-Y", strtotime($dealerOrder->created_at)); 
                                                        $type='Dealer';
                                                          ?>
                                                        <tr class="odd gradeX">
                                                          <td>{{ $dealerOrder->id }}</td>
                                                          <td>{{ $created_at }}</td>
                                                          <td>{{ $type }}</td>
                                                          <td>{{ Webkul\Customer\Models\Customer::CustomerName($dealerOrder->customer_id) }}</td>
                                                          <?php $orderProducts = DB::table('order_items')->where('order_id',$dealerOrder->id)->get(); 
                                                          $product_name=$product_qty=[];
                                                          foreach($orderProducts as $key => $orderProduct) {
                                                              $product_name[] = $orderProduct->name;
                                                              $product_qty[] = $orderProduct->qty_ordered;
                                                          } ?>
                                                          <td>{{ implode(",",$product_name)}}</td>
                                                          <td>{{ implode(",",$product_qty)}}</td>
                                                          <td>{{ (int)$dealerOrder->grand_total }}</td>
                                                          <td>{{ $dealerOrder->status }}</td>
                                                          <td class="action-menu">
                                                            <a href="{{ route('admin.sales.orders.view',$dealerOrder->id) }}" onclick="removeLink('Do you really want to do this?')"><i class="fa fa-eye"></i></a>
                                                          </td>
                                                        </tr>
                                                <?php 
                                                       }
                                                     }
                                                     }
                                                     else{ ?>
                                                      <tr><td colspan=9 class="text-center">No results found</td></tr>
                                                     <?php } ?>  
                                                  </tbody>
                                              </table>
                                        </div>

                                        <!-- /.panel-body -->
                                    </div>
                                      <!-- /.panel -->
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="credit_orders">
                      <div class="col-lg-12 table-list">
                          <div class="panel panel-default">
                              <div class="panel-title">
                                  <div class="panel-heading">
                                    <i class="fa fa-list"></i> Advance Payment Orders
                                  </div>
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body" style="clear: both; overflow: scroll;">
                                   <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>Order ID</th>
                                                  <th>Date Added</th>
                                                  <th>Name</th>
                                                  <th>Products</th>
                                                  <th>Quantity</th>
                                                  <th>Total</th>
                                                  <th>Status</th>
                                                  <th>Payment Method</th>
                                                  <th>Action</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                      foreach($creditOrders as $o_key => $creditOrder)
                                                      {
                                                        if(isset($creditOrder->id)) {
                                                        $created_at = date("d-m-Y", strtotime($creditOrder->created_at));
                                                         ?>
                                                        <tr class="odd gradeX">
                                                          <td>{{ $creditOrder->id }}</td>
                                                          <td>{{ $created_at }}</td>
                                                          <td>{{ Webkul\Customer\Models\Customer::CustomerName($creditOrder->customer_id) }}</td>
                                                          <?php $orderProducts = DB::table('order_items')->where('order_id',$creditOrder->id)->get(); 
                                                          $product_name=$product_qty=[];
                                                          foreach($orderProducts as $key => $orderProduct) {
                                                              $product_name[] = $orderProduct->name;
                                                              $product_qty[] = $orderProduct->qty_ordered;
                                                          } ?>
                                                          <td>{{ implode(",",$product_name)}}</td>
                                                          <td>{{ implode(",",$product_qty)}}</td>
                                                          <td>{{ (int)$creditOrder->grand_total }}</td>
                                                          <td>{{ $creditOrder->status }}</td>
                                                          <td>{{ $creditOrder->method }}</td>
                                                          <td class="action-menu">
                                                             <a href="{{ route('admin.sales.orders.view',$creditOrder->id) }}" onclick="removeLink('Do you really want to do this?')"><i class="fa fa-eye"></i></a>
                                                          </td>
                                                        </tr>
                                                <?php 
                                                       }
                                                     }  ?>  
                                                  </tbody>
                                              </table>
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="credit_payments">
                      <div class="count-box section col-md-12 col-lg-12">
                           <div class="box-inner col-lg-3 col-xs-6 mb-5">
                            <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                 <p>Credit Limit</p>
                                 <h3><i class="fa fa-inr"></i> <span>{{ number_format($credit_limit,2) }}</span></h3>
                                </div>
                              </a>
                            </div>
                          </div>

                          <div class="box-inner col-lg-3 col-xs-6 mb-5">
                                <div class="small-box col-md-12">
                                  <a href="#">
                                    <div class="inner">
                                      <p>Credit Limit Available</p>
                                      <h3><i class="fa fa-inr"></i> <span>{{ number_format($available_credit_limit, 2)}}</span> </h3>
                                    </div>
                                  </a>
                                </div>
                          </div>

                          @if($credit_pending_amount!=0)
                            <div class="box-inner col-lg-3 col-xs-6 mb-5">
                              <div class="small-box col-md-12">
                                <a href="#">
                                  <div class="inner">
                                  <p>Balance Due</p>
                                  <h3>- <i class="fa fa-inr"></i> <span>{{ number_format($credit_pending_amount,2) }}</span> </h3>
                                </div>
                                
                              </a>
                              </div>
                            </div> 
                          @else
                            <div class="box-inner col-lg-3 col-xs-6 mb-5">
                              <div class="small-box col-md-12">
                                <a href="#">
                                  <div class="inner">
                                    <p>Advance Balance</p>
                                    <h3><i class="fa fa-inr"></i> <span>{{ number_format($credit_balance,2) }}</span> </h3>
                                  </div>
                                </a>
                              </div>
                            </div>
                          @endif

                          
                          <div class="box-inner col-lg-3 col-xs-6 mb-5">
                            <div class="small-box col-md-12">
                              <a href="#">
                                <div class="inner">
                                  <p>Pending Approval</p>
                                  <h3><i class="fa fa-inr"></i> <span>{{ number_format($credit_pending,2)}}</span> </h3>
                                </div>
                              </a>
                            </div>
                          </div>
                      </div>
                      <div class="row" style="padding-right:1em; padding-bottom:2em">
                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#data_offline_purchase">Add Offline Purchase Due</button>
                      </div>
                    
                    </div>
                      <div class="col-md-12 col-sm-12">
                          
                          <form id="credit-payment-form" method="POST" action="{{ route('credit_payment.payviachequeandother') }}" enctype="multipart/form-data">
                                  <div class="page-content">
                                    <div class="form-container">
                                      @csrf()
                                       <div class="form-field inner-form col-md-12">
                                        <div class="control-group col-md-6 pass">
                                            <label class="required">Amount</label>
                                            <input type="number"  class="control pay_credit_amount" name="pay_credit_amount"  value="">
                                            <span class="control-error"></span>
                                        </div>
                                        <div class="control-group col-md-6 cpass">
                                            <label for="payment_type" class="required">Paying By</label>
                                            <select class="control payment_type" name="payment_type" class="control">
                                                <option value="2">Cheque Payment</option>
                                                <option value="3">Bank Transfer/NEFT/UPI</option>
                                                <option value="0">Other Payment</option>
                                            </select>
                                            <span class="control-error"></span>
                                        </div>
                                      </div>
                       
                                        <div class="form-field cheque-sec col-md-12">
                                          <div class="control-group col-md-6 pass">
                                            <label class="required">Cheque Number</label>
                                            <input type="text" class="control cheque_number" name="cheque_number"  value="">
                                            <span class="control-error"></span>
                                          </div>
                                          <div class="control-group col-md-6 cpass">
                                            <label>Cheque Image</label>
                                            <input type="file" name="cheque_image" class="img-thumb">                            
                                          </div>
                                        </div>

                                    <div class="form-field bank-neft-upi col-md-12"  style="display:none">
                                        <div class="control-group col-md-6 pass">
                                          <label class="required">Transation ID</label>
                                          <input type="text" class="control transation_number" name="transation_number"  value="">
                                          <span class="control-error"></span>
                                        </div>
                                        <div class="control-group col-md-6 cpass">
                                          <label>Transfer Receipt</label>
                                          <input type="file" name="receipt_image" class="img-thumb">
                                        </div>
                                    </div>

                                       <div class="form-field other-payment-sec col-md-12">
                                        <div class="control-group col-md-6 pass">
                                          <label class="required">Transaction ID</label>
                                          <input type="text"  v-validate="'required'" class="control transaction_id" name="transaction_id"  value="">
                                          <span class="control-error"></span>
                                        </div>
                                        <div class="control-group col-md-6 cpass">
                                          <label>Comment</label>
                                          <textarea class="form-control" cols="50" rows="10" id="transaction_comment" name="transaction_comment"></textarea>
                                        </div>
                                       </div>
                                       
                                      <input type="hidden" name="user_id" value="{{$customer->id}}">
                                      <input type="hidden" name="payment_id" class="payment_id" value="">
                                      <div class="control-group col-md-3 cpass">
                                          <button type="submit" class="btn amount-btn btn-lg btn-primary pay-via-cheque pay-credit-amount">
                                              Pay            
                                          </button>
                                      </div>
                                  </div>
                                </div>
                              </form>
                          </div>
                      <div class="col-lg-12 table-list">
                          <div class="panel panel-default">
                              <div class="panel-title">
                                  <div class="panel-heading">
                                    <i class="fa fa-list"></i> Payments
                                  </div>
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body" style="clear: both; overflow: scroll;">
                               

                                   <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>ID</th>
                                                  <th>Payment Type</th>
                                                  <th>Payment ID</th>
                                                  <th>Cheque Number</th>
                                                  <th>Cheque Image</th>
                                                  <th>Comments</th>
                                                  <th>Amount</th>
                                                  <th>Status</th>
                                                  <th>Paid By</th>
                                                  <th>Created Date</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php 
                                                      foreach($creditPayments as $o_key => $creditPayment)
                                                      {
                                                        if(isset($creditPayment->id)) {
                                                        $created_at = date("d-m-Y", strtotime($creditPayment->created_at));
                                                         ?>
                                                        <tr class="odd gradeX">
                                                          <td>{{ $creditPayment->id }}</td>
                                                          <td>{{[0=>'Other',1=>'Razorpay', 2=>'Cheque', 3=>'Bank Transfer/NEFT/UPI'][$creditPayment->payment_type]}}</td>
                                                          <td>{{ $creditPayment->payment_id }}</td>
                                                          <td>{{ $creditPayment->cheque_number}}</td>
                                                          <td>
                                                            @if($creditPayment->cheque_image)
                                                              <a target="_blank" href="{{url(bagisto_asset('images/credit_images/'.$creditPayment->cheque_image))}}" class="img-enlargeable">Click here</a>
                                                            @endif
                                                          </td>
                                                          <td>{{$creditPayment->comments}}</td>
                                                          <td>{{ $creditPayment->amount }}</td>
                                                          <td>
                                                            @if ($creditPayment->status == 0)
                                                              <span class="badge badge-md badge-warning">Pending</span>
                                                            @elseif ($creditPayment->status == 1)
                                                              <span class="badge badge-md badge-success" style="background-color: #007704;">Approved</span>
                                                            @elseif ($creditPayment->status == 2)
                                                              <span class="badge badge-md badge-danger"  style="background-color: #e40000;">Rejected</span>
                                                            @endif
                                                          </td>
                                                          <td>{{ Webkul\Customer\Models\Customer::customerRoleName($creditPayment->paid_by) }}</td>
                                                          <td>{{ $created_at }}</td>
                                                          
                                                        </tr>
                                                <?php 
                                                       }
                                                     }  ?>  
                                                  </tbody>
                                              </table>
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    

                    <div class="col-lg-12 table-list">
                          <div class="panel panel-default">
                              <div class="panel-title">
                                  <div class="panel-heading">
                                    <i class="fa fa-list"></i> Offline Payments Dues
                                  </div>
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body" style="clear: both; overflow: scroll;">
                               

                                   <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                              <tr>
                                                  <th>ID</th>
                                                  <th>Amount</th>
                                                  <th>Date</th>
                                                  <th>Order Number</th>
                                                  <th>Comments</th>
                                                  <th>Created Date</th>
                                              </tr>
                                                  </thead>
                                                  <tbody>
                                                    @foreach($offline_due as $offlinepayment)
                                                      <tr class="odd gradeX">
                                                        <td>{{ $offlinepayment->id }}</td>
                                                        <td>{{ $offlinepayment->amount }}</td>
                                                        <td>{{ date('d-m-Y', strtotime($offlinepayment->due_date)) }}</td>
                                                        <td>{{ $offlinepayment->order_number }}</td>
                                                        <td>{{ $offlinepayment->comments }}</td>
                                                        <td>{{ date('d-m-Y', strtotime($offlinepayment->created_at)) }}</td>
                                                      </tr>
                                                    @endforeach
                                                  </tbody>
                                              </table>
                                </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
 <div class="modal fade" id="ShowCommissionDetails" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Commission Details</h4>
        </div>
        <div class="modal-body col-md-12">
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>
   <div class="modal fade" id="CommissionPaidHistory" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Commission Paid History</h4>
        </div>
        <div class="modal-body col-md-12">
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>

<!-- Modal -->
<div class="modal fade" id="data_offline_purchase" tabindex="-1" role="dialog" aria-labelledby="data_offline_purchase_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="data_offline_purchase_label">Add Offline Purchase Due</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('credit_payment.addcreditdue')}}" id="credit_due_form">
          @csrf
          <input type="hidden" name="user_id" value="{{$customer->id}}">
          <div class="control-group" style="text-align: left; padding:2px 0">
            <label for="due_amount" class="col-form-label required" style="font-size: unset">Due Amount:</label>
            <input type="number" class="control" id="due_amount" name="credit_due_price">
          </div>
          <div class="control-group" style="text-align: left; padding:2px 0">
            <label for="due_date" class="col-form-label required" style="font-size: unset">Date:</label>
            <date>
                <input type="text" class="control" name="due_date"  value="{{date('Y-m-d')}}">
            </date> 
          </div>
          
          <div class="control-group" style="text-align: left; padding:2px 0">
            <label for="due_order_number" class="col-form-label required" style="font-size: unset">Order Number:</label>
            <input type="text" class="control" name="order_number" value="">
          </div>
          <div class="control-group" style="text-align: left; padding:2px 0">
            <label for="message_text" class="col-form-label required"  style="font-size: unset">Comment/Products:</label>
            <textarea class="form-control" id="message_text" name="credit_due_comment"></textarea>
          </div>
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary credit_due_submit">Save Purchase</button>
      </div>
    </div>
  </div>
</div>


@stop
@push('scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(e){
$(".profile-pic").click(function() {
    $("#upload-profile").click();
});

$("input[type='file']").change(function () {
var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'upload-profile')
                reader.onload = profileUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });


function profileUploaded(e) {
    $('.profile-pic').attr('src', e.target.result);
};

    $('body').find('.dealer-comm-amt').on('click', function(event){
            var order_id = $(this).data('order_id');
            $.ajax({
                type: "POST",
                url: "{{route('admin.dealer.comm_details')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    order_id: order_id
                },
                success: function(data) {
                    $('#ShowCommissionDetails').find('.modal-body').html(data.view);
                }
            });
    });

    $('body').find('.dealer-comm-paid-history').on('click', function(event){
            var order_id = $(this).data('order_id');
            $.ajax({
                type: "POST",
                url: "{{route('admin.dealer.comm_paid_history')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    order_id: order_id,
                    dealer_id: "{{ $customer->id }}"
                },
                success: function(data) {
                    $('#CommissionPaidHistory').find('.modal-body').html(data.view);
                }
            });
    }); 


});
</script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
 <script type="text/javascript">
        $(document).ready(function(){



$('body').on('change','.payment_type', function(){
  /*$('.other-payment-sec').css('display', 'none');
  $('.pay-credit-amount').removeClass('pay-via-other');
  $('.pay-credit-amount').addClass('pay-via-cheque');*/
  if($(this).val() == 2){
    $('.cheque-sec').css('display', 'block');
    $('.other-payment-sec').css('display', 'none');
    $('.bank-neft-upi').css('display', 'none');
    $('.pay-credit-amount').addClass('pay-via-cheque');
    $('.pay-credit-amount').removeClass('pay-via-other');
    $('.pay-credit-amount').removeClass('pay-via-net');
  }
  if($(this).val() == 3){
    $('.bank-neft-upi').css('display', 'block');
    $('.cheque-sec').css('display', 'none');
    $('.other-payment-sec').css('display', 'none');
    $('.pay-credit-amount').addClass('pay-via-net');
    $('.pay-credit-amount').removeClass('pay-via-other');
    $('.pay-credit-amount').removeClass('pay-via-cheque');
  }
  if($(this).val() == 0){
    $('.other-payment-sec').css('display', 'block');
    $('.bank-neft-upi').css('display', 'none');
    $('.cheque-sec').css('display', 'none');
    $('.pay-credit-amount').addClass('pay-via-other');
    $('.pay-credit-amount').removeClass('pay-via-cheque');
    $('.pay-credit-amount').removeClass('pay-via-net');
  }
});

    <?php /*
 function readIMG(input,image_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+image_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $('body').on('change','input[type=file]', function(){
      var image_id = $(this).parent().find('img').attr('id');
        readIMG(this,image_id);
    });



  $('body').on('click', '.cheque-sec > div > img', function(){
    $(this).parent().find('input[type=file]').trigger('click'); 
  });


  $('body').on('click', '.bank-neft-upi > div > img', function(){
    $(this).parent().find('input[type=file]').trigger('click'); 
  });


        function readIMG(input,image_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#'+image_id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        */ ?>
    $("#cheque-image").change(function(){
      readURL(this);
    });
            
  $('.pay-all-doctors').click(function(e){
    let pay_val = $(this).parent().find('input[name="pay_amount"]:first-child').val();

    //alert($(this).data('dealerid'));
    if(confirm("Do you like to pay all doctors?")){
      $.ajax({
        type: "POST",
        url: "{{route('admin.dealer.payall_doctors_commission')}}",
        data: {
          _token: "{{csrf_token()}}",
          dealer_id: $(this).data('dealerid'),
          pay_val: pay_val
        },
        success: function(data) {
          console.log(data);
          location.reload(true);
        }
      });
    }
  });


  $('#credit-payment-form').on('submit',function(e){
        $(this).find('.control-error').hide();
        var payment_type = $(this).find('.payment_type').val(); // 2 - cheque, 3 - bank, 0 - others
        var transaction_id = $(this).find('.transaction_id'); //others
        var transaction_num = $(this).find('.transation_number'); //bank
        var check_number = $(this).find('.cheque_number'); //check_number
        var pay_amount = $(this).find('.pay_credit_amount');

        function sayError(ele){
            ele.parent().find('.control-error').text('This field is required').show();
        }

        var validate = true;

        if(!pay_amount.val()){
            validate = false;
            sayError(pay_amount);
        }

        if(payment_type == 2){
            if(!check_number.val()){
                validate = false;
                sayError(check_number);
            }
        }

        if(payment_type == 3){
            if(!transaction_num.val()){
                validate = false;
                sayError(transaction_num);
            }
        }

        if(payment_type == 0){
            if(!transaction_id.val()){
                validate = false;
                sayError(transaction_id);
            }
        }

        if(!validate) e.preventDefault();
    });


              function imgClickFunction(e){
                e.preventDefault();
                var src = $(this).attr('src') ?? $(this).attr('href');
                var modal;
                function removeModal(){ modal.remove(); $('body').off('keyup.modal-close'); }
                modal = $('<div>').css({
                    background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
                    backgroundSize: 'contain',
                    width:'100%', height:'100%',
                    position:'fixed',
                    zIndex:'10000',
                    top:'0', left:'0',
                    cursor: 'pointer'
                }).click(function(){
                    removeModal();
                }).appendTo('body');

                $('body').on('keyup.modal-close', function(e){
                    if(e.key==='Escape'){
                        removeModal();
                    }
                });
            }

            $('.img-thumb').on('change',function(event) {
                let input = event.target;
                let reader = new FileReader();
                reader.onload = function(){
                    let dataURL = reader.result;
                    let output = $(`<img class='imgThumb'>`).hide();
                    output.attr('src', dataURL);
                    $(input).next('.imgThumb').remove();
                    $(input).after(output);
                    output.fadeIn();
                    output.addClass('img-enlargeable').click(imgClickFunction);
                };
                reader.readAsDataURL(input.files[0]);
            });

            $('.img-enlargeable').click(imgClickFunction);


            $('body').on('click', '.credit_due_submit', function(){
              let ele = $('#credit_due_form');
              ele.find('.control-error').remove();
              let validate = true;

              $(ele.find('input, textarea')).each(function(){
                if(!$(this).val() || $(this).val()==0){
                  $(this).after($('<span class="control-error" style="margin:10px 0; display:inline-block">This field is required</span>'));
                  validate = false;
                }                
              })
              
              if(validate) ele.submit();

            });
});
</script>
<style type="text/css">
    .mb-5{
        margin-bottom: 5px;
    }
    .imgThumb{
        width: 200px;
        margin: 20px 10px 0;
        cursor: pointer;
    }
</style>

@endpush

@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.dealers.dealers.edit-title') }}
@stop

@section('content')
    <div class="content">
        <form method="POST" id="dealerCreateForm" action="{{ route('admin.dealer.update', $customer->id) }}">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dealers') }}';"></i>

                        {{ __('admin::app.dealers.dealers.title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.dealers.dealers.save-btn-title') }}
                    </button>
                </div>
            </div><!--page header-->

            <div class="page-content">

                <div class="user-sec form-container">
                    @csrf()

                    <input name="_method" type="hidden" value="PUT">

                    <div class="form-fields col-md-12">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-3 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option <?php if($customer->my_title == "Mr.") echo 'selected'; ?> value="Mr.">Mr.</option>
                                    <option <?php if($customer->my_title == "Mrs.") echo 'selected'; ?> value="Mrs.">Mrs.</option>
                                    <option <?php if($customer->my_title == "Mr. Dr..") echo 'selected'; ?> value="Mr. Dr.">Mr. Dr.</option>
                                    <option <?php if($customer->my_title == "Mrs. Dr.") echo 'selected'; ?> value="Mrs. Dr.">Mrs. Dr.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-md-5" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'" value="{{ $customer->first_name }}" data-vv-as="&quot;First Name&quot;">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group col-md-4 cpass">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name" value="{{ $customer->last_name }}" data-vv-as="Last Name">
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">Mobile Number</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ $customer->phone_number }}" data-vv-as="&quot;Mobile Number&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>

                            <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ $customer->email }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
                        </div>

                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('country_id') ? 'has-error' : '']">
                                <label for="country_id" class="required">{{ __('shop::app.customer.signup-form.country') }}</label>
                                <input type="text" class="control" name="country_id" v-validate="'required'" disabled="disabled" value="India">
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                            <div class="control-group cpass col-md-6" :class="[errors.has('state_id') ? 'has-error' : '']">
                                <label for="state_id" class="required">State</label>
                                <?php $states = array();
                                $states = DB::table('country_states')->where('country_id',101)->get();?>
                                <select name="state_id" id="state" class="control" v-validate="'required'">
                                    <option value="">Choose your state...</option>
                                    <?php foreach($states as $key => $state) { ?>
                                        <option <?php if($state->id == $customer->state_id) echo 'selected'; ?> value="{{ $state->id }}">{{ $state->name }}</option>
                                    <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('state_id')">@{{ errors.first('state_id') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('city_id') ? 'has-error' : '']">
                                <label for="city" class="required">{{ __('shop::app.customer.signup-form.city') }}</label>
                                <?php $cities = array();
                                    $cities = DB::table('country_state_cities')->where('state_id',$customer->state_id)->get();?>
                                    <select name="city_id" id="city" class="control" v-validate="'required'">
                                        <option value="">Choose your city...</option>
                                        <?php foreach($cities as $key => $city) { ?>
                                            <option <?php if($city->id == $customer->city_id) echo 'selected'; ?> value="{{ $city->id }}">{{ $city->name }}</option>
                                        <?php } ?>
                                    </select>
                                <span class="control-error" v-if="errors.has('city_id')">@{{ errors.first('city_id') }}</span>
                            </div>
                            
                            <div class="control-group cpass col-md-6" :class="[errors.has('pin_code') ? 'has-error' : '']">
                                <label for="pin_code" class="required">Pincode</label>
                                <input type="text" class="control" name="pin_code" v-validate="'required'" value="{{ $customer->pin_code }}" data-vv-as="Pincode">
                                <span class="control-error" v-if="errors.has('pin_code')">@{{ errors.first('pin_code') }}</span>
                            </div>
                        </div>

                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-4" :class="[errors.has('permission_to_create_order') ? 'has-error' : '']">
                                <label for="permission_to_create_order" class="required">Allow them to add cart for their doctors</label>
                                <select name="permission_to_create_order" id="permission_to_create_order" class="control" v-validate="'required'">
                                        <option value="0" <?php if($customer->permission_to_create_order == 0) echo 'selected'; ?>>No</option>
                                        <option value="1" <?php if($customer->permission_to_create_order == 1) echo 'selected'; ?>>Yes</option>
                                </select>
                                <span class="control-error" v-if="errors.has('permission_to_create_order')">@{{ errors.first('permission_to_create_order') }}</span>
                            </div>
                            <div class="control-group col-md-4" :class="[errors.has('customer_group_id') ? 'has-error' : '']">
                                <label for="group">Customer Group</label>
                                <select name="customer_group_id" id="group" class="control" v-validate="'required'" data-vv-as="&quot;{{ "Group" }}&quot;">
                                    @foreach($customerGroup as $group)
                                    <option value="{{$group->id}}" @if($group->id==$customer->customer_group_id) selected @endif>{{$group->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="control-error" v-if="errors.has('customer_group_id')">@{{ errors.first('customer_group_id') }}</span>
                            </div>

                            @if(auth()->guard('admin')->user()->role_id == 1)
                                <div class="control-group cpass col-md-4">
                                    <label for="prepayment_available">Prepayment Available</label>
                                    <select name="prepayment_available" id="prepayment_available" class="control">
                                    <option value="0" <?php if($customer->prepayment_available == 0) echo 'selected'; ?>>No</option>
                                    <option value="1" <?php if($customer->prepayment_available == 1) echo 'selected'; ?>>Yes</option>
                                    </select>
                                </div>
                            @endif
                        </div>
                        
                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-4">
                                <label for="company_name">Company Name</label>
                                <input type="text" class="control" name="company_name" value="{{ $customer->company_name }}" data-vv-as="Company Name">
                            </div>
                            <div class="control-group pass col-md-4">
                                <label for="gst_no">GST No</label>
                                <input type="text" class="control" name="gst_no" value="{{$customer->gst_no}}" data-vv-as="GST No">
                            </div>
                            <div class="control-group cpass col-md-4">
                                <label for="fda_licence_no">FDA Licence No</label>
                                <input type="text" class="control" name="fda_licence_no" value="{{ $customer->fda_licence_no}}" data-vv-as="FDA Licence No">
                            </div>
                        </div> 
                        
                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-4">
                                <label for="credit_available">Show Credit limit & Advance payment</label>
                                <select name="credit_available" id="credit_available" class="control">
                                    <option value="0" <?php if($customer->credit_available == 0) echo 'selected'; ?>>No</option>
                                    <option value="1" <?php if($customer->credit_available == 1) echo 'selected'; ?>>Yes</option>
                                </select>
                            </div>

                            <div class="control-group pass col-md-4">
                                <label for="credit_limit">Credit limit</label>
                                <input type="text" class="control" name="credit_limit" value="{{ $customer->credit_limit ?? 0 }}" data-vv-as="Credit limit">
                            </div>

                            {{--  <div class="control-group pass col-md-3">
                                <label for="credit_used">Credit Used</label>
                                <input type="text" class="control" name="credit_used" value="{{ $customer->credit_used ?? 0 }}" data-vv-as="Credit Used">
                            </div> --}}
                            
                            <div class="control-group cpass col-md-4">
                                <label for="credit_balance">Advance</label>
                                <input type="text" class="control" name="credit_balance" value="{{ $customer->credit_balance }}">
                            </div>
                            
                        </div>
                        
                        <div class="discount-section">
                            <accordian :title="'Category Based Discount'" :active="true">
                                <div slot="body">
                                    <div class="discounts" id="disount_container"></div>
                                    <input type="hidden" name="discount_id" value="{{$categry_discount{0}->id ?? ''}}">
                                    <button type="button" class="add-discount pull-right btn btn-primary" name="add-more"><i class="fa fa-plus"></i></button>
                                </div>
                            </accordian>
                        </div>

                        <div class="dealer-comm-section">
                            <accordian :title="'Dealer Commision Section'" :active="true">
                                <div slot="body">
                                    <div class="dealer-comm" id="dealer_comm_container"></div>
                                    <button type="button" class="add-comm pull-right btn btn-primary" name="add-more">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </accordian>
                        </div>
                           
                        
                    </div>
                        {!! view_render_event('bagisto.shop.customers.signup_form_controls.after') !!}
                </div>
            </div><!--page content-->

            <div class="page-footer">
                <button type="submit" class="btn pull-right btn-lg btn-primary">
                    {{ __('admin::app.dealers.dealers.save-btn-title') }}
                </button>
            </div><!--page footer-->
        </form>
    </div>
@stop

@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/simTree.css')}}">
    <script type="text/javascript" src="{{asset('themes/default/assets/js/simTree.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('body').on('change','#state', function(){
                var stateID = $(this).val();
                if(stateID){
                    $.ajax({
                        type:"GET",
                        url:"{{url('customer/get-city-list')}}?state_id="+stateID,
                        success:function(res){
                            if(res){
                                $("#city").empty();
                                $.each(res,function(key,value){
                                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                                });
                            }else{
                                $("#city").empty();
                            }
                        }
                    });
                }else{
                    $("#city").empty();
                }
            });

            const category_tree = '{!! $categories !!}';
            const discounts_categories = {!! $all_discount !!};
            const commission_categories = {!! $all_commission !!};

            let treeCount = {
                discount : 0,
                comm :0
            }

            function addTree(type, dis=null){
                
                let cat_tree = JSON.parse(category_tree);

                if(type=='discount'){
                    var container = $('#disount_container');
                    var name="Discount";
                    var handle='discount';
                    var percentage = 'discount_per';
                }else if(type=='comm'){
                    var container = $('#dealer_comm_container');
                    var name="Commision";
                    var handle='dealer_comm';
                    var percentage = 'comm_percent';
                }

                let html_temp = $(`<div class="${type}-part form-field col-md-12">
                                        <div class="${type}-inner col-md-12">
                                            <div class="accordian active accordian_header">
                                                <button type="button" id="remove${treeCount[type]}" class="pull-right remove-me-${type}" ><i class="fa fa-times"></i></button>
                                                <div class="accordian-content">
                                                    <div class="category-part section">
                                                        <div class="categorieTrees" data-id=${treeCount[type]} data-type='${type}'>
                                                            <input type="hidden" name="${handle}[${treeCount[type]}][categories]" id="categories${treeCount[type]}_${type}" class="categorys_values">
                                                            <div id="categorieTree${treeCount[type]}_${type}"></div>
                                                        </div>
                                                    </div>

                                                    <div class="control-group ${type}-percnt col-md-12">
                                                        <label for="${type}_per">${name} (%)</label>
                                                        <input type="text" id="${percentage}" class="control" name="${handle}[${treeCount[type]}][${percentage}]" value="${dis?dis.percentage:''}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> `);

                container.append(html_temp);

                cat_tree = cat_tree.map((val)=>{
                    if(dis && dis.categorys.includes(val.id)) val.checked = true; 

                    if(val.pid) val.pid = type+treeCount[type]+'-'+val.pid;
                    val.id = type+treeCount[type]+'-'+val.id;
                    return val;
                });
                if(dis) $(`#categories${treeCount[type]}_${type}`).val(dis.categorys.map((val)=> type+treeCount[type]+'-'+val));
                $(`#categorieTree${treeCount['discount']}_discount, #categorieTree${treeCount['comm']}_comm`).simTree({
                    data: cat_tree,
                    check: true,
                    linkParent: true,
                    onChange: function(item){
                        const id = $(this['$el']).closest('.categorieTrees').data('id');
                        const type = $(this['$el']).closest('.categorieTrees').data('type');
                        console.log(id, type);
                        $(`#categories${id}_${type}`).val(item.filter((ele)=>ele.end==true).map((ele)=>ele.id));
                    }
                });
                treeCount[type]++;
            }

            function checkValidFields(type='discount'){
                let $check = true;
                
                let container = '';
                if(type=='discount'){
                    container = $('#disount_container');
                }else if(type=='comm'){
                    container = $('#dealer_comm_container');
                }

                container.find('.error-msg').remove();

                container.children().each(function(ele){
                    console.log($(this).find('.categorys_values').val());
                    if(!$(this).find('.categorys_values').val()){
                        $(this).find('.categorieTrees').append('<span class="error-msg">Please choose any categories in this tree!</span>');
                        $check = false;
                    };

                    if(! +$(this).find(`#${type}_per, #${type}_percent`).val()) {
                        $(this).find(`.${type}-percnt`).append('<span class="error-msg">This discount percentage is required!</span>');
                        $check = false;
                    }                   

                });
                return $check;
            }

            $(".add-discount, .add-comm").click(function(e){
                const type = $(this).hasClass('add-discount')?'discount':'comm';
                if(checkValidFields(type)) addTree(type);
                e.preventDefault();
            });

            $('body').on('click', '.remove-me-discount, .remove-me-comm', function(){
                const type_class = $(this).hasClass('remove-me-discount')? '.discount-part':'.comm-part';
                const type = $(this).hasClass('remove-me-discount')?'discount':'comm';
                
                if(confirm('Do you like to remove this category?')){
                    $(this).closest($(type_class)).remove();
                }   
            });

            $('#dealerCreateForm').on('submit', function(e){
                let check1 = !checkValidFields('discount');
                let check2 = !checkValidFields('comm');
                if(check1 || check2) e.preventDefault();
            });

            discounts_categories.forEach((dis)=> addTree('discount', dis));
            commission_categories.forEach((dis)=> addTree('comm', dis));           
        });
    </script>


    <style type="text/css">
        .accordian_container{
            clear: both;
            display: flex;
            align-items: center;
            align-content: center;
        }

        .accordian_header{
            border-top:1px solid #DEDEDE;
            position: relative;
        }

        .accordian_container>button{
            height: 30px;
            margin-left: 10px;
        }

        .remove-me-discount, .remove-me-comm{
            outline: none !important;
            border: none;
            border-radius: 50%;
            background-color: #ff4a4a;
            color: #FFF;
            position: absolute;
            right: 10px;
            top: 10px;
        }

        .add-discount, .add-comm{
            margin-right: 15px;
        }
        .error-msg{
            display: inline-block;
            margin:10px 3px;
            color: #8F0000;
            font-size: 1.5rem;
        }
    </style>
@endpush

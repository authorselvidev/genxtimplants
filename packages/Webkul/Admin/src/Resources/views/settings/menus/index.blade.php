@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.menus.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.settings.menus.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.menus.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.settings.menus.add-title') }}
                </a>
            </div>
        </div>

        <div class="page-content">

           @inject('menus','Webkul\Admin\DataGrids\MenusDataGrid')
            {!! $menus->render() !!}
        </div>
    </div>
@stop
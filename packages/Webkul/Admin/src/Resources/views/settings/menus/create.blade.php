@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.menus.add-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.menus.store') }}" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.settings.menus.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.menus.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.menus.general') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('menu_title') ? 'has-error' : '']">
                                <label for="menu-title" class="required">{{ __('admin::app.settings.menus.menu_title') }}</label>
                                <input v-validate="'required'" class="control" id="menu-title" name="menu_title" data-vv-as="&quot;{{ __('admin::app.settings.menus.menu_title') }}&quot;" v-code/>
                                <span class="control-error" v-if="errors.has('menu_title')">@{{ errors.first('menu_title') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="menu-link" class="required">{{ __('admin::app.settings.menus.menu_link') }}</label>
                                <input type="url" v-validate="'required'" class="control" id="menu-link" name="menu_link" data-vv-as="&quot;{{ __('admin::app.settings.menus.menu_link') }}&quot;"/>
                                <span class="control-error" v-if="errors.has('menu_link')">@{{ errors.first('menu_link') }}</span>
                            </div>
                        </div>
                    </accordian>

                </div>
            </div>
        </form>
    </div>
@stop
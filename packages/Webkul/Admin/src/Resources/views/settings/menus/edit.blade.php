@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.menus.edit-title') }}
@stop

@section('content')
    <div class="content">

        <form method="post" action="{{ route('admin.menus.update', $menu->id) }}">
            @csrf()
            <input name="_method" type="hidden" value="PATCH">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" ></i>

                        {{ __('admin::app.settings.menus.edit-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.menus.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    

                    <accordian :title="'{{ __('admin::app.settings.menus.general') }}'" :active="true">
                        <div slot="body">

                             <div class="control-group">
                                <label for="menu-title" class="required">{{ __('admin::app.settings.menus.menu_title') }}</label>
                                <input type="text" class="control" id="menu-title" name="menu_title" value="{{ $menu->menu_title }}"/>
                                <span class="control-error">@{{ errors.first('menu_title') }}</span>
                            </div>

                            <div class="control-group">
                                <label for="menu-link" class="required">{{ __('admin::app.settings.menus.menu_link') }}</label>
                                <input class="control" id="menu-link" name="menu_link" value="{{ $menu->menu_link }}"/>
                                <span class="control-error">@{{ errors.first('menu_link') }}</span>
                            </div>
                        </div>
                    </accordian>

                </div>
            </div>
        </form>
    </div>
@stop
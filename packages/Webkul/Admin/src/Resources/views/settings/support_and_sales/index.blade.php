@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.support_and_sales.title') }}
@stop

@section('content')
    <div class="content">
        
        <div class="page-header page-section">
            <div class="page-title">
                <h1>{{ __('admin::app.settings.support_and_sales.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.support_and_sales.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.settings.support_and_sales.add-title') }}
                </a>
            </div>

        </div>

        <div class="page-content">

           @inject('support','Webkul\Admin\DataGrids\SupportSalesDataGrid')
            {!! $support->render() !!}
        </div>
    </div>
@stop
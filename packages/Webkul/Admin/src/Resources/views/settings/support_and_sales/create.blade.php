@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.support_and_sales.add-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.support_and_sales.store') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.settings.support_and_sales.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.support_and_sales.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.support_and_sales.general') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('support_text') ? 'has-error' : '']">
                                <label for="support-text" class="required">{{ __('admin::app.settings.support_and_sales.support_text') }}</label>
                                <input v-validate="'required'" class="control" id="support-text" name="support_text" data-vv-as="&quot;{{ __('admin::app.settings.support_and_sales.support_text') }}&quot;"/>
                                <span class="control-error" v-if="errors.has('support_text')">@{{ errors.first('support_text') }}</span>
                            </div>

                             <div class="control-group">
                                <label for="support-image" class="required">{{ __('admin::app.settings.support_and_sales.support_image') }}</label>
                                <div>
                                    <button type="button" class="btn btn-primary select-files btn-sm">{{ __('admin::app.settings.support_and_sales.support_image') }}</button>
                                    <input name="support_image" type="file" id="support-image">
                                    <span class="control-error" v-if="errors.has('support_image')">@{{ errors.first('support_image') }}</span>
                                </div>
                            </div>
                            <div class="image-div"><img class="show-media" src=""></div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
<script>
   $(document).ready(function () {
    
   $(".select-files").click(function() {
        $("#support-image").click();
    });

    $('.show-media').css('display','none');
   $("input[type='file']").change(function () {
    var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'support-image')
                reader.onload = imageUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });


function imageUploaded(e) {
     $('.image-div').css('display','block');
    $('.show-media').css('display','block');
    $('.show-media').attr('src', e.target.result);
};

   });
</script>
@endpush
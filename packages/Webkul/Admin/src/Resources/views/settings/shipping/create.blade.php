@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.shipping.add-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.shipping.store') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.settings.shipping.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.shipping.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.shipping.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-field col-md-12">
                                <div class="control-group col-md-4" :class="[errors.has('min_weight') ? 'has-error' : '']">
                                    <label for="support-text" class="required">Min Weight</label>
                                    <input v-validate="'required'" class="control" id="min-weight" name="min_weight"/>
                                    <span class="control-error" v-if="errors.has('min_weight')">@{{ errors.first('min_weight') }}</span>
                                </div>
                                 <div class="control-group col-md-4" :class="[errors.has('max_weight') ? 'has-error' : '']">
                                    <label for="max-weight" class="required">Max Weight</label>
                                    <input v-validate="'required'" class="control" id="max-weight" name="max_weight"/>
                                    <span class="control-error" v-if="errors.has('max_weight')">@{{ errors.first('max_weight') }}</span>
                                </div>
                                <div class="control-group col-md-4" :class="[errors.has('shipping_amount') ? 'has-error' : '']">
                                    <label for="shipping-amount" class="required">Shipping Amount</label>
                                    <input v-validate="'required'" class="control" id="shipping-amount" name="shipping_amount"/>
                                    <span class="control-error" v-if="errors.has('shipping_amount')">@{{ errors.first('shipping_amount') }}</span>
                                </div>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
<script>
   $(document).ready(function () {
    
   $(".select-files").click(function() {
        $("#support-image").click();
    });

    $('.show-media').css('display','none');
   $("input[type='file']").change(function () {
    var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'support-image')
                reader.onload = imageUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });


function imageUploaded(e) {
     $('.image-div').css('display','block');
    $('.show-media').css('display','block');
    $('.show-media').attr('src', e.target.result);
};

   });
</script>
@endpush
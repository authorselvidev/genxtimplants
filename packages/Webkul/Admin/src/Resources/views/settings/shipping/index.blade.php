@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.shipping.title') }}
@stop

@section('content')
    <div class="content">
        
        <div class="page-header page-section">
            <div class="page-title">
                <h1>{{ __('admin::app.settings.shipping.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.shipping.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.settings.shipping.add-title') }}
                </a>
            </div>

        </div>

        <div class="page-content">

           @inject('shipping','Webkul\Admin\DataGrids\ShippingDataGrid')
            {!! $shipping->render() !!}
        </div>
    </div>
@stop
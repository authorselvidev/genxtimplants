@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.shipping.edit-title') }}
@stop

@section('content')
    <div class="content">

        <form method="post" action="{{ route('admin.shipping.update', $shipping->id) }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            @csrf()
            <input name="_method" type="hidden" value="PATCH">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" ></i>

                        {{ __('admin::app.settings.shipping.edit-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.shipping.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    <accordian :title="'{{ __('admin::app.settings.shipping.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-field col-md-12">
                                <div class="control-group col-md-4" :class="[errors.has('min_weight') ? 'has-error' : '']">
                                    <label for="support-text" class="required">Min Weight</label>
                                    <input v-validate="'required'" class="control" id="min-weight" name="min_weight" value="{{$shipping->min_weight}}"/>
                                    <span class="control-error" v-if="errors.has('min_weight')">@{{ errors.first('min_weight') }}</span>
                                </div>
                                 <div class="control-group col-md-4" :class="[errors.has('max_weight') ? 'has-error' : '']">
                                    <label for="max-weight" class="required">Max Weight</label>
                                    <input v-validate="'required'" class="control" id="max-weight" name="max_weight" value="{{$shipping->max_weight}}"/>
                                    <span class="control-error" v-if="errors.has('max_weight')">@{{ errors.first('max_weight') }}</span>
                                </div>
                                <div class="control-group col-md-4" :class="[errors.has('shipping_amount') ? 'has-error' : '']">
                                    <label for="shipping-amount" class="required">Shipping Amount</label>
                                    <input v-validate="'required'" class="control" id="shipping-amount" name="shipping_amount" value="{{$shipping->shipping_amount}}"/>
                                    <span class="control-error" v-if="errors.has('shipping_amount')">@{{ errors.first('shipping_amount') }}</span>
                                </div>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop
@push('scripts')
<script>
$(document).ready(function () {
    
   $(".select-files").click(function() {
        $("#support-image").click();
    });

   $("input[type='file']").change(function () {
    var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'support-image')
                reader.onload = imageUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });

    function imageUploaded(e) {
        $('.show-media').css('display','block');
        $('.show-media').attr('src', e.target.result);
    };

});
</script>
@endpush
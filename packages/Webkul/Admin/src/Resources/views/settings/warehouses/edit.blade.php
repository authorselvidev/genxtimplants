@extends('admin::layouts.content')

@section('page_title')
    Edit Warehouse
@stop

@section('content')
    <div class="content">

        <form method="post" action="{{ route('admin.warehouse.update', $warehouse->id) }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            @csrf()
            <input name="_method" type="hidden" value="PATCH">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" ></i>

                        Edit Warehouse
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Update Warehouse
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    <accordian :title="'{{ __('admin::app.settings.support_and_sales.general') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('code') ? 'has-error' : '']">
                                <label for="code" class="required">Code</label>
                                <input v-validate="'required'" class="control" id="code" name="code" value="{{ $warehouse->code }}"/>
                                <span class="control-error" v-if="errors.has('code')">@{{ errors.first('code') }}</span>
                            </div>

                             <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">Name</label>
                                <input v-validate="'required'" class="control" name="name" type="text" id="name" value="{{ $warehouse->name }}">
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('dealer_id') ? 'has-error' : '']">
                                <label for="dealer_id" class="required">Choose Dealer</label>
                                <select name="dealer_id" id="dealer_id" class="control" v-validate="'required'">
                                    <option value="">Select Dealer...</option>
                                    <?php foreach($dealers as $key => $dealer) { ?>
                                        <option <?php if($warehouse->dealer_id == $dealer->id)echo 'selected'; ?> value="{{ $dealer->id }}">{{ $dealer->first_name }} {{ $dealer->last_name }}</option>
                                    <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('dealer_id')">@{{ errors.first('dealer_id') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('address') ? 'has-error' : '']">
                                <label for="address" class="required">Address</label>
                                <textarea class="control" v-validate="'required'" name="address" id="address">{{ $warehouse->address}}</textarea>
                                <span class="control-error" v-if="errors.has('address')">@{{ errors.first('address') }}</span>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop
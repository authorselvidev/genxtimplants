@extends('admin::layouts.content')

@section('page_title')
    Create Warehouse
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.warehouse.store') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/warehouses') }}';"></i>

                        Create Warehouse
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save warehouse
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.support_and_sales.general') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('code') ? 'has-error' : '']">
                                <label for="code" class="required">Code</label>
                                <input v-validate="'required'" class="control" id="code" name="code" value="GXTW{{ mt_rand(000,999) }}"/>
                                <span class="control-error" v-if="errors.has('code')">@{{ errors.first('code') }}</span>
                            </div>

                             <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">Name</label>
                                <input v-validate="'required'" class="control" name="name" type="text" id="name">
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('dealer_id') ? 'has-error' : '']">
                                <label for="dealer_id" class="required">Choose Dealer</label>
                                <select name="dealer_id" id="dealer_id" class="control" v-validate="'required'">
                                    <option value="">Select Dealer...</option>
                                    <?php foreach($dealers as $key => $dealer) { ?>
                                        <option value="{{ $dealer->id }}">{{ $dealer->first_name }} {{ $dealer->last_name }}</option>
                                    <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('dealer_id')">@{{ errors.first('dealer_id') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('address') ? 'has-error' : '']">
                                <label for="address" class="required">Address</label>
                                <textarea class="control" v-validate="'required'" name="address" id="address"></textarea>
                                <span class="control-error" v-if="errors.has('address')">@{{ errors.first('address') }}</span>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
<script>
   $(document).ready(function () {
    
   $(".select-files").click(function() {
        $("#support-image").click();
    });

    $('.show-media').css('display','none');
   $("input[type='file']").change(function () {
    var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'support-image')
                reader.onload = imageUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });


function imageUploaded(e) {
     $('.image-div').css('display','block');
    $('.show-media').css('display','block');
    $('.show-media').attr('src', e.target.result);
};

   });
</script>
@endpush
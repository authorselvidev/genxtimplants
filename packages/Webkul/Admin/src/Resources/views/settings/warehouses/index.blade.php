@extends('admin::layouts.content')

@section('page_title')
    Warehouse
@stop

@section('content')
    <div class="content">
        <div class="page-header page-section">
            <div class="page-title">
                <h1>Warehouses</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.warehouse.create') }}" class="btn btn-lg btn-primary">
                    Create Warehouse
                </a>
            </div>
        </div>

        <div class="page-content">
           @inject('warehouse','Webkul\Admin\DataGrids\WarehouseDataGrid')
            {!! $warehouse->render() !!}
        </div>
    </div>
@stop
@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.pages.title') }}
@stop

@section('content')
    <div class="content">
        <div class="select-home">
            <form method="post" action="{{ route('admin.pages.home_page') }}" onSubmit="if(!confirm('Do you want to change the home page??')){return false;}">
                @csrf()
            <div class="">
                <label>Choose Home Page: </label>
                <select name="page_id">
                    <option value="">Choose Home Page</option>
                    @foreach($pages as $key => $page)
                        <option <?php if(isset($home_page) && $home_page->id == $page->id) echo 'selected'; ?> value="{{$page->id}}">{{ $page->page_name }}</option>
                    @endforeach
                </select>
                <input type="submit" class="btn btn-primary btn-sm" value="Submit">
            </div>
        </form>
        </div>




        <div class="page-header page-section">
            <div class="page-title">
                <h1>{{ __('admin::app.settings.pages.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.pages.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.settings.pages.add-title') }}
                </a>
            </div>

        </div>

        <div class="page-content">

           @inject('pages','Webkul\Admin\DataGrids\PagesDataGrid')
            {!! $pages->render() !!}
        </div>
    </div>
@stop
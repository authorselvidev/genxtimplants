@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.pages.add-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.pages.store') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.settings.pages.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.pages.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.pages.general') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('page_name') ? 'has-error' : '']">
                                <label for="menu-title" class="required">{{ __('admin::app.settings.pages.page_name') }}</label>
                                <input v-validate="'required'" class="control" id="page-name" name="page_name" data-vv-as="&quot;{{ __('admin::app.settings.pages.page_name') }}&quot;"/>
                                <span class="control-error" v-if="errors.has('page_name')">@{{ errors.first('page_name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('page_slug') ? 'has-error' : '']">
                                <label for="page-slug" class="required">{{ __('admin::app.settings.pages.page_slug') }}</label>
                                <input v-validate="'required'" class="control" id="page-slug" name="page_slug" v-slugify/>
                                <span class="control-error" v-if="errors.has('page_slug')">@{{ errors.first('page_slug') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('page_content') ? 'has-error' : '']">
                                <label for="page-content" class="required">{{ __('admin::app.settings.pages.page_content') }}</label>
                                <textarea v-validate="'required'" class="control" id="page-content" name="page_content" data-vv-as="&quot;page_content&quot;"></textarea>
                                 <input name="image" type="file" id="upload" class="hidden" style="display:none" onchange="">
                                <span class="control-error" v-if="errors.has('page_content')">@{{ errors.first('menu_link') }}</span>
                            </div>
                        </div>
                    </accordian>

                    <accordian :title="'{{ __('admin::app.settings.pages.meta_desc') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('meta_title') ? 'has-error' : '']">
                                <label for="meta-title" class="required">{{ __('admin::app.settings.pages.meta_title') }}</label>
                               <textarea v-validate="'required'" class="control" id="meta-title" name="meta_title" data-vv-as="&quot;meta-title&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_title')">@{{ errors.first('meta_title') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('meta_keyword') ? 'has-error' : '']">
                                <label for="meta-keyword">{{ __('admin::app.settings.pages.meta_keyword') }}</label>
                                <textarea class="control" id="meta-keyword" name="meta_keyword" data-vv-as="&quot;meta-keyword&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_keyword')">@{{ errors.first('meta_keyword') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('meta_description') ? 'has-error' : '']">
                                <label for="meta-description">{{ __('admin::app.settings.pages.meta_description') }}</label>
                                <textarea  class="control" id="meta-description" name="meta_description" data-vv-as="&quot;meta-description&quot;"></textarea>
                                <span class="control-error" v-if="errors.has('meta_description')">@{{ errors.first('meta_description') }}</span>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
    <script src="{{ asset('vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#channel-switcher, #locale-switcher').on('change', function (e) {
                $('#channel-switcher').val()
                var query = '?channel=' + $('#channel-switcher').val() + '&locale=' + $('#locale-switcher').val();

                window.location.href = "{{ route('admin.pages.create')  }}" + query;
            })

            tinymce.init({
                selector: 'textarea#page-content',
                height: 200,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
            });
 });
       </script>
@endpush
@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.media.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header page-section">
            <div class="page-title">
                <h1>{{ __('admin::app.settings.media.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.media.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.settings.media.add-title') }}
                </a>
            </div>

        </div>

        <div class="page-content media-page">
            @foreach($medias as $key => $media)
             <span class="media_inner" data-toggle="modal" data-target="#mediaPicture">
              <?php $media_file = 'uploads/'.$media->media_name;
                    if(strpos($media->media_type, 'pdf')) $media_file = 'pdflogo.jpg'; ?>
                <img  id="{{$media->id}}" class="media_file" src="{!! bagisto_asset('images/'.$media_file) !!}" type="{{ $media->media_type }}">
            </span>
            @endforeach
        </div>
    </div>



    <div class="modal fade" id="mediaPicture" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Media File</h4>
          
        </div>
        <div class="modal-body">
          <a href="" class="pull-right delete-btn btn btn-primary">Delete</a>
            <div class="popup_pic text-center"></div>
            <div class="media_url"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@stop

@push('scripts')
<script>
   $(document).ready(function() {
      $('.media_inner').click(function(e){
        
        var mediaUrl = $(this).find('.media_file').attr('src');
        var mediaID = $(this).find('.media_file').attr('id');
        var clicked_type = $(this).find('.media_file').attr('type');
        var route_name = "{{ route('admin.media.destroy', ':id') }}";            
            route_name = route_name.replace(':id', mediaID);
        $('.delete-btn').attr('href',route_name);
        $('.popup_pic').html('<img src="'+mediaUrl+'">');
        if(clicked_type.indexOf('pdf') != '-1')
          var mediaUrl = "{!! bagisto_asset('pdfs/uploads/'.$media->media_name) !!}";
        
        $('.media_url').html('<strong>Media URL:</strong> '+'<span>'+mediaUrl+'</span>');
      });

    });
   
</script>
@endpush
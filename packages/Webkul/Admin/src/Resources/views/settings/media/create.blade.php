@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.media.add-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" id="myform" action="{{ route('admin.media.store') }}" enctype="multipart/form-data">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.settings.media.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.media.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.media.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-group">
                                <label for="upload-media" class="required">{{ __('admin::app.settings.media.upload_media') }}</label>
                                <div>
                                    <button type="button" class="btn btn-primary select-files btn-sm">{{ __('admin::app.settings.media.select_files') }}</button>
                                    <input name="media_file" type="file" id="upload-media">
                                    <span class="control-error" v-if="errors.has('media_file')">@{{ errors.first('media_file') }}</span>
                                </div>
                            </div>
                            <div class="image-div"><img class="show-media" src=""></div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
    
<script>
   $(document).ready(function () {
    
   $(".select-files").click(function() {
        $("#upload-media").click();
    });

    $('.show-media').css('display','none');
   $("input[type='file']").change(function () {
    var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'upload-media')
                reader.onload = imageUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });


function imageUploaded(e) {
     $('.image-div').css('display','block');
    $('.show-media').css('display','block');
    $('.show-media').attr('src', e.target.result);
};

   });
</script>
@endpush
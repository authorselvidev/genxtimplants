@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.shipping_carriers.title') }}
@stop

@section('content')
    <div class="content">
        
        <div class="page-header page-section">
            <div class="page-title">
                <h1>{{ __('admin::app.settings.shipping_carriers.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.shippingcarrier.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.settings.shipping_carriers.add-title') }}
                </a>
            </div>

        </div>

        <div class="page-content">

           @inject('shipping_carrier','Webkul\Admin\DataGrids\ShippingCarrierDataGrid')
            {!! $shipping_carrier->render() !!}
        </div>
    </div>
@stop
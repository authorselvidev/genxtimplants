@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.shipping.add-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.shippingcarrier.store') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.settings.shipping.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.shipping.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <accordian :title="'{{ __('admin::app.settings.shipping.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-field col-md-12">
                                <div class="control-group col-md-4" :class="[errors.has('carrier_name') ? 'has-error' : '']">
                                    <label for="support-text" class="required">Shipping Carrier Name</label>
                                    <input v-validate="'required'" class="control" id="carrier-name" name="carrier_name"/>
                                    <span class="control-error" v-if="errors.has('carrier_name')">@{{ errors.first('carrier_name') }}</span>
                                </div>
                                 <div class="control-group col-md-4" :class="[errors.has('tracking_url') ? 'has-error' : '']">
                                    <label for="max-weight" class="required">Tracking URL</label>
                                    <input v-validate="'required'" class="control" id="tracking-url" name="tracking_url"/>
                                    <span class="control-error" v-if="errors.has('tracking_url')">@{{ errors.first('tracking_url') }}</span>
                                </div>
                                <div class="control-group col-md-4" :class="[errors.has('status') ? 'has-error' : '']">
                                    <label for="status" class="required">Shipping Carrier Status</label>
                                     <select  class="control" v-validate="'required'" name="status">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    <span class="control-error" v-if="errors.has('status')">@{{ errors.first('status') }}</span>
                                </div>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop


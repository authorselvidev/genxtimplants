@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.shipping.edit-title') }}
@stop

@section('content')
    <div class="content">

        <form method="post" action="{{ route('admin.shippingcarrier.update', $shipping_carrier->id) }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
            @csrf()
            <input name="_method" type="hidden" value="PATCH">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" ></i>

                        {{ __('admin::app.settings.shipping.edit-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.settings.shipping.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    <accordian :title="'{{ __('admin::app.settings.shipping.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-field col-md-12">
                                <div class="control-group col-md-4" :class="[errors.has('carrier_name') ? 'has-error' : '']">
                                    <label for="support-text" class="required">Shipping Carrier Name</label>
                                    <input v-validate="'required'" class="control" id="carrier-name" name="carrier_name" value="{{$shipping_carrier->name}}"/>
                                    <span class="control-error" v-if="errors.has('carrier_name')">@{{ errors.first('carrier_name') }}</span>
                                </div>
                                 <div class="control-group col-md-4" :class="[errors.has('tracking_url') ? 'has-error' : '']">
                                    <label for="max-weight" class="required">Tracking URL</label>
                                    <input v-validate="'required'" class="control" id="tracking-url" name="tracking_url" value="{{$shipping_carrier->tracking_url}}"/>
                                    <span class="control-error" v-if="errors.has('tracking_url')">@{{ errors.first('tracking_url') }}</span>
                                </div>
                                <div class="control-group col-md-4" :class="[errors.has('status') ? 'has-error' : '']">
                                    <label for="status" class="required">Shipping Carrier Status</label>
                                     <select  class="control" v-validate="'required'" name="status">
                                        <option value="1" @if($shipping_carrier->status ==1) {{'checked'}} @endif>Active</option>
                                        <option value="0" @if($shipping_carrier->status ==0) {{'checked'}} @endif>Inactive</option>
                                    </select>
                                    <span class="control-error" v-if="errors.has('status')">@{{ errors.first('status') }}</span>
                                </div>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop
@push('scripts')
<script>
$(document).ready(function () {
    
   $(".select-files").click(function() {
        $("#support-image").click();
    });

   $("input[type='file']").change(function () {
    var this_id = $(this).attr('id');
    if (this.files && this.files[0]) { 
            var reader = new FileReader();
            if(this_id == 'support-image')
                reader.onload = imageUploaded;
            reader.readAsDataURL(this.files[0]);
        }
    });

    function imageUploaded(e) {
        $('.show-media').css('display','block');
        $('.show-media').attr('src', e.target.result);
    };

});
</script>
@endpush
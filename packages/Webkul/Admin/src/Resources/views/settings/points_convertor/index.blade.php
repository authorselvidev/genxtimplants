@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.points_convertor.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header page-section">
            <div class="page-title">
                <h1>
                       <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                       Points Convertor
                    </h1>
                </div>
                
        </div>

        <div class="page-content">

            <div class="point-form col-md-12">
                <form method="POST" action="{{ route('admin.points_convertor.store') }}" @submit.prevent="onSubmit">
                    <div class="form-container">
                        @csrf()
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-8" :class="[errors.has('points') ? 'has-error' : '']">
                                <label for="name" class="required">
                                    1 Coin is
                                </label>
                                <input type="text" class="control" name="points" v-validate="'required'" value="{{ old('points') }}">
                                <span class="control-error" v-if="errors.has('points')">@{{ errors.first('points') }}</span>
                                <span class="points_text">Points</span>
                            </div>

                            <div class="control-group sub-btn col-md-4">
                                <button type="submit" class="btn btn-lg btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

           @inject('points_convertor','Webkul\Admin\DataGrids\PointsToCoinConversion')
            {!! $points_convertor->render() !!}
        </div>
    </div>
@stop
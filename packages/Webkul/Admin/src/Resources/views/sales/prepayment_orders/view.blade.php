@extends('admin::layouts.master')

@section('page_title')
    {{ __('admin::app.sales.orders.view-title', ['order_id' => $order->id]) }}
@stop

@section('content-wrapper')
<?php $customer_info = DB::table('users')->where('id',$order->customer_id)->first();
    $dealer_info = DB::table('users')->where('id',$customer_info->dealer_id)->first(); 
   // dd($customer_info);?>
    <div class="content full-page">

        <div class="page-header">

            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                    {{ __('admin::app.sales.orders.view-title', ['order_id' => $order->id]) }}
                </h1>
            </div>

            @if(auth()->guard('admin')->user()->role_id == 1 || auth()->guard('admin')->user()->role_id == 4)
            <div class="page-action">
                @if ($order->canCancel())
                    <a href="#cancelOrderModel" class="btn btn-lg btn-primary" data-toggle="modal">
                        {{ __('admin::app.sales.orders.cancel-btn-title') }}
                    </a>
                @endif
                <a class="btn btn-lg btn-primary"  data-toggle="modal" data-target="#approveOrder" href="">Approve</a>
            </div>
            @endif
        </div>

        <div class="page-content tab">
                    <div class="sale-container">
                        <accordian :title="'{{ __('admin::app.sales.orders.order-and-account') }}'" :active="true">
                           
                            <div class="rem-mop" slot="body">
                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.order-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.order-date') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->created_at }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.order-status') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->status_label }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.channel') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->channel_name }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.account-info') }}</span>
                                    </div>
                                    <?php $customer_name = DB::table('users as u')->leftjoin('orders as o', 'o.customer_id', 'u.id')
                                ->where('o.customer_id',$order->customer_id)->first(); 
                                 ?>
                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.customer-name') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                               {{ $customer_name->my_title }} {{ $customer_name->first_name }} {{ $customer_name->last_name }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.email') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->customer_email }}
                                            </span>
                                        </div>

                                        @if (! is_null($order->customer))
                                            <div class="rows">
                                                <span class="title col-md-5">
                                                    {{ __('admin::app.customers.customers.customer_group') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    {{ $order->customer->customerGroup['name'] }}
                                                </span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                </div>

                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.address') }}'" :active="true">
                            <div class="billing-addr" slot="body">
                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.billing-address') }}</span>
                                    </div>
                                    <div class="section-content">
                            <?php $get_order = DB::table('orders')->where('id',$order->id)->first();
                            //dd($get_order);
                            $billing_address = DB::table('user_addresses')->where('id',$get_order->billing_address)->latest()->first();
                            ?>
                                        @include ('admin::sales.address', ['address' => $billing_address])
                                    </div>
                                </div>
                            </div>

                                @if ($get_order->shipping_address)
                                <?php $shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->latest()->first(); ?>
                                    <div class="sale-section col-md-6">
                                        <div class="inner-sale">
                                        <div class="secton-title">
                                            <span>{{ __('admin::app.sales.orders.shipping-address') }}</span>
                                        </div>

                                        <div class="section-content">

                                            @include ('admin::sales.address', ['address' => $shipping_address])

                                        </div>
                                    </div>
                                </div>
                                @endif

                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.payment-and-shipping') }}'" :active="true">
                            <div slot="body">

                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.payment-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.payment-method') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                @if($order->payment[0]->method == 'paylaterwithcredit')
                                                    {{ "Pay with ".(($order->customer->role_id=='2')?'credit':'advance') }}
                                                @else
                                                    {{ core()->getConfigData('sales.paymentmethods.' . $order->payment[0]->method . '.title') }}
                                                @endif                                                
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.currency') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->order_currency_code }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.shipping-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                    @if($order->shipping_title)
                                            <div class="rows">
                                                <span class="title col-md-5">
                                                    {{ __('admin::app.sales.orders.shipping-method') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    {{ $order->shipping_title }}
                                                </span>
                                            </div>
                                        @endif

                                        @if($order->base_shipping_amount)
                                            <div class="rows">
                                                <span class="title col-md-5">
                                                    {{ __('admin::app.sales.orders.shipping-price') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    {{ core()->formatBasePrice($order->base_shipping_amount) }}
                                                </span>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                </div>
                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.products-ordered') }}'" :active="true">
                            <div slot="body">
                                <?php $discounted_subtotals = array_column($order->items->toArray(), 'discounted_subtotal');
                                                $discounted_subtotals = array_filter($discounted_subtotals); ?>
                                <div class="table pro-ord">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>{{ __('admin::app.sales.orders.SKU') }}</th>
                                                <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                                <th>{{ __('admin::app.sales.orders.price') }}</th>
                                                <th>{{ __('admin::app.sales.orders.item-status') }}</th>
                                                {{-- <th>{{ __('admin::app.sales.orders.subtotal') }}</th> --}}
                                                {{-- <th>{{ __('admin::app.sales.orders.tax-percent') }}</th> --}}
                                                {{-- <th>{{ __('admin::app.sales.orders.tax-amount') }}</th> --}}
                                                <th>Sub Total</th>
                                                @if(count($discounted_subtotals) > 0)
                                                    <th>Discounted Sub Total</th>
                                                @endif
                                                <th>{{ __('admin::app.sales.orders.grand-total') }} <br> (incl.taxes)</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            @foreach ($order->items as $item)
                                                <tr>
                                                    <td>
                                                        {{ $item->type == 'configurable' ? $item->child->sku : $item->sku }}
                                                    </td>
                                                    <td>
                                                        {{ $item->name }}

                                                        @if ($html = $item->getOptionDetailHtml())
                                                            <p>{{ $html }}</p>
                                                        @endif
                                                    </td>
                                                    <td>{{ core()->formatBasePrice($item->base_price) }}</td>
                                                    <td>
                                                        <span class="qty-row">
                                                            {{ $item->qty_ordered ? __('admin::app.sales.orders.item-ordered', ['qty_ordered' => $item->qty_ordered]) : '' }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_invoiced ? __('admin::app.sales.orders.item-invoice', ['qty_invoiced' => $item->qty_invoiced]) : '' }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_shipped ? __('admin::app.sales.orders.item-shipped', ['qty_shipped' => $item->qty_shipped]) : '' }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_canceled ? __('admin::app.sales.orders.item-canceled', ['qty_canceled' => $item->qty_canceled]) : '' }}
                                                        </span>
                                                    </td>
                                                    {{-- <td>{{ core()->formatBasePrice($item->base_total) }}</td> --}}
                                                    {{-- <td>{{ $item->tax_percent }}%</td> --}}
                                                    {{-- <td>{{ core()->formatBasePrice($item->base_tax_amount) }}</td> --}}
                                                    <td>{{ core()->formatBasePrice($item->base_total) }}</td>
                                                    <?php $discounted_subtotal=0; ?>
                                                    @if(count($discounted_subtotals) > 0)
                                                        @if(isset($item->discounted_subtotal))
                                                        <?php $discounted_subtotal = $item->discounted_subtotal; ?>
                                                            <td>{{ core()->formatBasePrice($item->discounted_subtotal) }}</td>
                                                        @else
                                                            <td>NA</td>
                                                        @endif
                                                    @endif
                                                    <?php $total_price_item = $item->total - $item->discount_amount;
                                                    if($discounted_subtotal != 0)
                                                        $total_price_item = $discounted_subtotal; 
                                                    
                                        $get_tax = DB::table('tax_rates')->where('state',$customer_info->state_id)->first(); 
                                        $item_tax = $total_price_item * ($get_tax->tax_rate / 100); 
                                        $price_with_tax = $total_price_item + $item_tax; ?> 
                                                    <td>{{ core()->formatBasePrice($price_with_tax) }}</td>
                                                </tr>
                                            @endforeach
                                    </table>
                                </div>

                                <table class="sale-summary">
                                    <tr>
                                        <td>{{ __('admin::app.sales.orders.subtotal') }}</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($order->base_sub_total) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal=0; ?>
                                    @if($order->mem_discount_amount == 0.0000 && $order->promo_code_amount == 0.0000 && $order->reward_coin == 0.0000 && $order->base_discount_amount != 0.0000)
                                    <tr>
                                        <td class="pull-left text-left">Special Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->base_discount_amount) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->base_discount_amount; ?>

                                    @endif

                                    @if($order->mem_discount_amount != 0.0000)
                                    <tr>
                                        <td class="pull-left text-left">Membership Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->mem_discount_amount) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Group:</td>
                                        <td>:</td>
                                        <td> {{ $order->customer->customerGroup['name'] }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->mem_discount_amount; ?>

                                    @endif

                                    @if($order->promo_code_amount != 0.0000)
                                    <tr>
                                        <td>Promo Code Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->promo_code_amount) }}</td>
                                    </tr>
                                    @if(isset($order->coupon_code) || $order->coupon_code != "")
                                    <tr>
                                        <td>Discount Code:</td>
                                        <td>:</td>
                                        <td> {{$order->coupon_code }}</td>
                                    </tr> 
                                    @endif
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->promo_code_amount; ?>
                                    @endif

                                    @if($order->reward_coin != 0.0000)
                                    <tr>
                                        <td>Reward Points:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->reward_coin) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->reward_coin; ?>
                                    @endif
                                    @if($discounted_subtotal != 0)
                                    <tr>
                                        <td>Discounted Subtotal</td>
                                        <td>:</td>
                                        <td>+ {{ core()->formatBasePrice($discounted_subtotal) }}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>{{ __('admin::app.sales.orders.shipping-handling') }}</td>
                                        <td>:</td>
                                        <td>+ {{ core()->formatBasePrice($order->base_shipping_amount) }}</td>
                                    </tr>

                                    <tr class="border">
                                        <td>{{ __('admin::app.sales.orders.tax') }}</td>
                                        <td>:</td>
                                        <td>+ {{ core()->formatBasePrice($order->tax_amount) }}</td>
                                    </tr>

                                    <tr class="bold">
                                        <td>{{ __('admin::app.sales.orders.grand-total') }}</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                    </tr>
                                    <?php $payment_paid = DB::table('payments')->where('order_id',$order->id)->first(); 
                                    $manual_payment = DB::table('manual_payment_entry')->where('order_id',$order->id)->sum('paid_amount');
                                    //dd($manual_payment) ?>
                                    @if($order->payment[0]->method == 'razorpay' && isset($payment_paid))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>- {{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                        </tr>
                                    @elseif($order->payment[0]->method == 'cashondelivery' && isset($manual_payment))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>- {{ core()->formatBasePrice($manual_payment) }}</td>
                                        </tr>
                                    @endif
                                    <!-- @if($order->status == 'completed')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>- {{ core()->formatBasePrice($order->base_grand_total_invoiced) }}</td>
                                        </tr>
                                    @endif  -->

                                    <tr class="bold">
                                        <td>{{ __('admin::app.sales.orders.total-refunded') }}</td>
                                        <td>:</td>
                                        
                                        <td>- {{ core()->formatBasePrice($order->base_grand_total_refunded) }}</td>
                                        
                                    </tr> 
                                   
                                 @if($order->payment[0]->method == 'cashondelivery' && isset($manual_payment))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                            <td> {{ core()->formatBasePrice($order->base_grand_total - $manual_payment)  }}</td>
                                        </tr>
                                    @elseif($order->payment[0]->method == 'razorpay' && isset($payment_paid))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice(0) }}</td>
                                        </tr> 
                                    @else
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                        </tr> 
                                     @endif
                                </table>

                            </div>
                        </accordian>

                    </div>
            
        </div>

    </div>

    <div class="modal fade" id="approveOrder" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Approve Order</h4>
        </div>
        <div class="modal-body col-md-12">
            <div class="change-order-status col-md-12">
                <form method="post" action="{{ route('admin.sales.prepayment_order.approve',$order->id)}}" @submit.prevent="onSubmit">
                    @csrf
                    <span class="col-md-12 ch-st">
                        <input type="hidden" name="order_id" value="{{$order->id}}">
                        @if(!empty($customer_info->dealer_id))
                            <div style="margin-bottom:25px;">
                                <input type="checkbox" id="approved_by_credit" name="approved_by_credit" class="approved_by_credit" value="1">
                                <label style="font-weight:normal;" for="approved_by_credit">Approve by the Dealer Payment</label>
                            </div>
                        @endif     
                        <div class="control-group payment-id">
                            <label class="required" >Transaction ID / Cheque Number</label>
                            <input type="text" name="payment_reference_id" class="control payment_reference_id" value="">
                            
                        </div>
                        <div class="control-group">
                            <label>Amount</label>
                            <input type="text" name="amount" class="control" disabled value="{{round($order->grand_total,2)}}">
                            <input type="hidden" name="amount" value="{{$order->grand_total}}">
                        </div>
                        <div class="control-group">
                            <label>Comments</label>
                            <textarea name="comments" class="control"></textarea>
                        </div>
                       

                    <input type="submit" name="approve_order" class="pull-right btn btn-primary approve-order" value="Approve">
                </form>
            </div>
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>

   <!-- Modal -->
   <div class="modal fade" id="cancelOrderModel" tabindex="-1" role="dialog" aria-labelledby="cancelOrder" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="cancelOrder">Cancel Order</h3>
        </div>
        <div class="modal-body">
          <form action="{{ route('admin.sales.orders.cancel', $order->id) }}" id="cancelForm" method="POST">
              @csrf
              <div class="control-group col-md-12">
                  <label class="required" for="cancelReason">Reason for Cancel</label>
                  <textarea class="control" id="cancelReason" name="cancel_reason"></textarea>
                  <span class="control-error">This field is required</span>
              </div>
          </form>
        </div>
        <div class="modal-footer" style="clear: both;">
          <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary btn-lg" id="cancel_order">Cancel order</button>
        </div>
      </div>
    </div>
  </div>

    <!-- Modal -->

@stop
@push('scripts')
<script type="text/javascript">
$(document).ready(function(){

    $('#cancel_order').click(function(e){
            const reason = $('#cancelForm');
            reason.find('.control-error').text('');
            if(reason.find('#cancelReason').val()){
                $('#cancelForm').submit();
                return;
            }
            reason.find('.control-error').text('This field is required').show();
            e.preventDefault();
        });


    $('body').find('.tracking-info').on('click', function(event){
            var carr_id = $(this).data('carr-id');
            var track_no = $(this).data('track-no');
            $('#UpdateTrackingInfo').find(".carr-id").val(carr_id);
            $('#UpdateTrackingInfo').find(".track-no").val(track_no);
    }); 

    $('body').on('change','#approved_by_credit',function () {
        $('body').find('.payment-id').css('display','block');
        $('body').find('.payment-id').find('.payment_reference_id').val('');
        if(this.checked){
            $('body').find('.payment-id').css('display','none');
            $('body').find('.payment-id').find('.payment_reference_id').val('0');
        }
 });
    $('body').on('click','.approve-order',function () {
        if ($('body').find('#approved_by_credit').prop(":checked")) {
            return true;
        }
        else{
            if($('body').find('.payment-id').find('.payment_reference_id').val() == ""){
                alert('Please enter Transaction ID / Cheque Number!');
                return false;
            }
        }
    });
    
    
});
       

</script>
@endpush

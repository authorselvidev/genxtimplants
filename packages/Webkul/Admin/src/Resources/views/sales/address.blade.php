

{{ str_replace('-',' ',$address->name) }}<br/>
{{ $address->address }}, {{ $address->address2 ? $address->address2 . ',' : '' }}<br/>
 {{ Webkul\Customer\Models\Cities::GetCityName($address->city) }}</br>
 {{ Webkul\Customer\Models\States::GetStateName($address->state) }}</br>
{{ Webkul\Customer\Models\Countries::GetCountryName($address->country) }} - {{ $address->postcode }}<br/><br/>
{{ __('shop::app.checkout.onepage.contact') }} : {{ $address->phone }} 
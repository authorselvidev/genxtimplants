@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.sales.orders.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.sales.orders.title') }}</h1>
            </div>

            <div class="page-action">
                <div class="export" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span>
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="page-content view-ord">
            @inject('orderGrid', 'Webkul\Admin\DataGrids\OrderDataGrid')
            {!! $orderGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>


    <div class="modal fade" id="changeOrderStatus" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Order Status</h4>
        </div>
        <div class="modal-body col-md-12">
            <div class="change-order-status col-md-12">
                <form method="post" action="{{ route('admin.change.order.status')}}" onsubmit="return confirm('Are you sure you want to change order status?');">
                    @csrf
                    <h5 class="text-center col-md-4">Change Order Status</h5>
                    <span class="col-md-1 text-center">:</span>
                    <span class="col-md-5 ch-st">
                        <input type="hidden" name="order_id" class="order_id" value="">
                    <input type="radio" id="delivered" name="order_status" class="orderstatus" value="delivered">
                    <label for="delivered">Delivered</label>
                    <input type="radio" id="completed" name="order_status" class="orderstatus" value="completed">
                    <label for="completed">Completed</label>
                </span>
                    <input type="submit" name="change_order_status" class="col-md-2 btn btn-primary change-status" value="Change" >
                </form>
            </div>
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>
@stop

@push('scripts')

<script type="text/x-template" id="export-form-template">
    <form method="POST" action="{{ route('admin.datagrid.export') }}">

        <div class="page-content">
            <div class="form-container">
                @csrf()

                <?php
                    $data = json_encode((array) $orderGrid);
                ?>

                <input type="hidden" name="gridData" value="{{ $data }}">
                <input type="hidden" name="file_name" value="Order">

                <div class="control-group">
                    <label for="format" class="required">
                        {{ __('admin::app.export.format') }}
                    </label>
                    <select name="format" class="control" v-validate="'required'">
                        <option value="xls">XLS</option>
                        <option value="csv">CSV</option>
                    </select>
                </div>

            </div>
        </div>

        <button type="submit" class="btn btn-lg btn-primary" @click="closeModal">
            {{ __('admin::app.export.export') }}
        </button>

    </form>
</script>

<script>
    Vue.component('export-form', {
        template: '#export-form-template',
        methods: {
            closeModal () {
                this.$parent.closeModal();
            }
        }
    });

      $(document).ready(function(){
        $('body').find('.update-status').on('click', function(event){
            var order_id = $(this).data('order_id');
            $('#changeOrderStatus').find(".order_id").val(order_id);
        }); 
    });
</script>

@endpush
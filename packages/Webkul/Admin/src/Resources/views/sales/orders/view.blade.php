@extends('admin::layouts.master')

@section('page_title')
    {{ __('admin::app.sales.orders.view-title', ['order_id' => $order->id]) }}
@stop

@section('content-wrapper')
<?php
    $customer_info = DB::table('users')->where('id',$order->customer_id)->first();
    $dealer_info = DB::table('users')->where('id',$customer_info->dealer_id)->first(); 
?>
    <div class="content full-page">


        <div class="page-header">
            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                    {{ __('admin::app.sales.orders.view-title', ['order_id' => $order->id]) }}
                </h1>
            </div>

            @if(auth()->guard('admin')->user()->role_id == 1 || auth()->guard('admin')->user()->role_id == 4)
            <div class="page-action">
                @if ($order->canCancel())
                    <a href="#cancelOrderModel" class="btn btn-lg btn-primary" data-toggle="modal">
                        {{ __('admin::app.sales.orders.cancel-btn-title') }}
                    </a>
                @endif

                @if ($order->canInvoice())
                    <a href="{{ route('admin.sales.invoices.create', $order->id) }}" class="btn btn-lg btn-primary">
                        {{ __('admin::app.sales.orders.invoice-btn-title') }}
                    </a>
                @endif

                @if ($order->canShip())
                    <a href="{{ route('admin.sales.shipments.create', $order->id) }}" class="btn btn-lg btn-primary">
                        {{ __('admin::app.sales.orders.shipment-btn-title') }}
                    </a>
                @endif
            </div>
            @endif
        </div>

        <div class="page-content tab">

            <tabs class="nav-tabs">
                <tab name="{{ __('admin::app.sales.orders.info') }}" :selected="true">
                    <div class="sale-container">

                        <accordian :title="'{{ __('admin::app.sales.orders.order-and-account') }}'" :active="true">
                           
                            <div class="rem-mop" slot="body">
                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.order-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.order-date') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->created_at }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.order-status') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->status_label }}
                                               
                                                @if(auth()->guard('admin')->user()->role['id'] == 1)
                                                    @if(($order->status != 'completed') && $order->status != 'canceled' && (auth()->guard('admin')->user()->role_id == 1 || auth()->guard('admin')->user()->role_id==4))
                                                        <a class="pull-right"  data-toggle="modal" data-target="#changeOrderStatus" href="">Change</a>
                                                    @endif
                                                @endif

                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.channel') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->channel_name }}
                                            </span>
                                        </div>

                                        @if($order->status == 'canceled')
                                            <div class="rows">
                                                <span class="title col-md-5">
                                                    Cancel Reason
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    <a href="#cancelReasonModel" data-toggle="modal">
                                                        View
                                                    </a>
                                                </span>
                                            </div>
                                        @endif
                                        
                                    </div>
                                </div>
                                </div>

                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.account-info') }}</span>
                                    </div>
                                    <?php $customer_name = DB::table('users as u')->leftjoin('orders as o', 'o.customer_id', 'u.id')
                                ->where('o.customer_id',$order->customer_id)->first(); 
                                 ?>
                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.customer-name') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $customer_name->my_title }} {{ $customer_name->first_name }} {{ $customer_name->last_name }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.email') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->customer_email }}
                                            </span>
                                        </div>

                                        @if (! is_null($order->customer))
                                            <div class="rows">
                                                <span class="title col-md-5">
                                                    {{ __('admin::app.customers.customers.customer_group') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    {{ $order->customer->customerGroup['name'] }}
                                                </span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                </div>

                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.address') }}'" :active="true">
                            <div class="billing-addr" slot="body">
                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.billing-address') }}</span>
                                    </div>
                                    <div class="section-content">
                            <?php $get_order = DB::table('orders')->where('id',$order->id)->first();
                            //dd($get_order);
                            $billing_address = DB::table('user_addresses')->where('id',$get_order->billing_address)->latest()->first();
                            ?>
                                        @include ('admin::sales.address', ['address' => $billing_address])
                                    </div>
                                </div>
                            </div>

                                @if ($get_order->shipping_address)
                                <?php $shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->latest()->first(); ?>
                                    <div class="sale-section col-md-6">
                                        <div class="inner-sale">
                                        <div class="secton-title">
                                            <span>{{ __('admin::app.sales.orders.shipping-address') }}</span>
                                        </div>

                                        <div class="section-content">

                                            @include ('admin::sales.address', ['address' => $shipping_address])

                                        </div>
                                    </div>
                                </div>
                                @endif

                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.payment-and-shipping') }}'" :active="true">
                            <div slot="body">

                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.payment-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.payment-method') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                
                                                @if($order->payment[0]->method == 'paylaterwithcredit')
                                                    {{ "Pay with ".(($order->customer->role_id=='2')?'credit':'advance') }}
                                                @else
                                                    {{ core()->getConfigData('sales.paymentmethods.' . $order->payment[0]->method . '.title') }}
                                                @endif
                                                
                                                @if(isset($prepayment) && $prepayment !=null && $prepayment->approved_by_credit==1)
                                                    (Dealers Payment Used)
                                                @endif
                                                
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.currency') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->order_currency_code }}
                                            </span>
                                        </div>

                                        @if(isset($prepayment) && $prepayment !=null)
                                            @if($prepayment->amount)
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        Amount
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ number_format($prepayment->amount,2) }}
                                                    </span>
                                                </div>
                                            @endif
                                            
                                            @if($prepayment->payment_reference)
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        TXN ID/Cheque Number
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ $prepayment->payment_reference }}
                                                    </span>
                                                </div>
                                            @endif
                                            
                                            @if($prepayment->comments && $prepayment->comments != '')
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        Comments
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ $prepayment->comments }}
                                                    </span>
                                                </div>
                                            @endif
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>

                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.shipping-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        @if($order->shipping_title)
                                            <div class="rows">
                                                <span class="title col-md-5">
                                                    {{ __('admin::app.sales.orders.shipping-method') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    {{ $order->shipping_title }}
                                                </span>
                                            </div>
                                        @endif

                                        @if($order->base_shipping_amount)
                                            <div class="rows">
                                                <span class="title col-md-5">
                                                    {{ __('admin::app.sales.orders.shipping-price') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    {{ core()->formatBasePrice($order->base_shipping_amount) }}
                                                </span>
                                            </div>
                                        @endif

                                        @if($shipment)
                                            @if (isset($inventory_source) && $inventory_source!=null)
                                                <div class="rows">
                                                    <span class="title col-md-5"> 
                                                        {{ __('admin::app.sales.shipments.inventory-source') }}
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6"> 
                                                        {{ $inventory_source->name }}
                                                    </span>
                                                </div>
                                            @endif

                                            <div class="rows">
                                                <span class="title col-md-5"> 
                                                    Shipping Carrier
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6"> 
                                                    <?php $get_carrier = DB::table('shipping_carriers')->where('id',$shipment->shipping_carrier_id)->first();?>
                                                    {{ $get_carrier->name or "" }}
                                                </span>
                                            </div>

                                            <div class="rows">
                                                <span class="title col-md-5"> 
                                                    {{ __('admin::app.sales.shipments.tracking-number') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6"> 
                                                    {{ $shipment->track_number }}
                                                </span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                </div>
                            </div>
                        </accordian>

                        <accordian :title="'{{ __('admin::app.sales.orders.products-ordered') }}'" :active="true">
                            <div slot="body">
                                <?php 
                                    /*$discounted_subtotals = array_column($order->items->toArray(), 'discounted_subtotal');
                                    $discounted_subtotals = array_filter($discounted_subtotals);

                                    $special_discounts = array_column($order->items->toArray(), 'discount_amount');
                                    $special_discounts = array_filter($special_discounts, function($value){
                                        return ($value != 0.000);
                                    });*/

                                    $special_discount_applied = ($order->mem_discount_amount == 0.0000 && $order->promo_code_amount == 0.0000 && $order->credit_discount_amount == 0.0000 && $order->reward_coin == 0.0000) && $order->base_discount_amount != 0.0000;
                                    $discoun_applied = $order->mem_discount_amount != 0.0000 || $order->promo_code_amount != 0.0000 || $order->credit_discount_amount != 0.0000 || $order->reward_coin != 0.0000;
                                    
                                ?>
                                <div class="table pro-ord">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>{{ __('admin::app.sales.orders.SKU') }}</th>
                                                <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                                <th>{{ __('admin::app.sales.orders.price') }}</th>
                                                <th>{{ __('admin::app.sales.orders.item-status') }}</th>
                                                {{-- <th>{{ __('admin::app.sales.orders.subtotal') }}</th> --}}
                                                {{-- <th>{{ __('admin::app.sales.orders.tax-percent') }}</th> --}}
                                                {{-- <th>{{ __('admin::app.sales.orders.tax-amount') }}</th> --}}
                                                <th>Sub Total</th>
                                                @if($discoun_applied==true || $special_discount_applied==true)
                                                    <th>Discounted Sub Total</th>
                                                @endif
                                                <th>{{ __('admin::app.sales.orders.grand-total') }} <br> (incl.taxes)</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            @foreach ($order->items as $item)
                                                <tr>
                                                    <td>
                                                        {{ $item->type == 'configurable' ? $item->child->sku : $item->sku }}
                                                    </td>
                                                    <td>
                                                        {{ $item->name }}

                                                        @if ($html = $item->getOptionDetailHtml())
                                                            <p>{{ $html }}</p>
                                                        @endif
                                                    </td>
                                                    <td>{{ core()->formatBasePrice($item->base_price) }}</td>
                                                    <td>
                                                        <span class="qty-row">
                                                            {{ $item->qty_ordered ? __('admin::app.sales.orders.item-ordered', ['qty_ordered' => $item->qty_ordered]) : '' }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_invoiced ? __('admin::app.sales.orders.item-invoice', ['qty_invoiced' => $item->qty_invoiced]) : '' }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_shipped ? __('admin::app.sales.orders.item-shipped', ['qty_shipped' => $item->qty_shipped]) : '' }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_canceled ? __('admin::app.sales.orders.item-canceled', ['qty_canceled' => $item->qty_canceled]) : '' }}
                                                        </span>
                                                    </td>
                                                    {{-- <td>{{ core()->formatBasePrice($item->base_total) }}</td> --}}
                                                    {{-- <td>{{ $item->tax_percent }}%</td> --}}
                                                    {{-- <td>{{ core()->formatBasePrice($item->base_tax_amount) }}</td> --}}
                                                    <td>{{ core()->formatBasePrice($item->base_total) }}</td>
                                                    <?php 
                                                        $discounted_subtotal=0;
                                                        $special_discount=0;
                                                    ?>
                                                    
                                                    @if ($discoun_applied==true)
                                                        @if(isset($item->discounted_subtotal)  && $item->discounted_subtotal !=0)
                                                            <?php 
                                                                $discounted_subtotal = $item->discounted_subtotal; 
                                                            ?>
                                                            <td>{{ core()->formatBasePrice($item->discounted_subtotal) }}</td>
                                                        @else
                                                            <td>NA</td>
                                                        @endif
                                                    @elseif($special_discount_applied==true)
                                                        @if(isset($item->discount_amount) && $item->discount_amount !=0)
                                                            <?php 
                                                                $special_discount = $item->base_total-$item->discount_amount; 
                                                            ?>
                                                            <td>{{ core()->formatBasePrice($special_discount) }}</td>
                                                        @else
                                                            <td>NA</td>
                                                        @endif
                                                    @endif
                                                    <?php 
                                                        /*
                                                            $total_price_item = $item->total - $item->discount_amount;
                                                            if($discounted_subtotal != 0)
                                                                $total_price_item = $discounted_subtotal; */
                                                                if($discoun_applied==true){
                                                                    $total_price_item = ($discounted_subtotal==0)? ($item->total) : $discounted_subtotal;
                                                                }else{
                                                                    $total_price_item = ($discounted_subtotal==0)? ($item->total - $item->discount_amount) : $discounted_subtotal;
                                                                }
                                                    
                                        $get_tax = DB::table('tax_rates')->where('state',$customer_info->state_id)->first(); 
                                        $item_tax = $total_price_item * ($get_tax->tax_rate / 100); 
                                        $price_with_tax = $total_price_item + $item_tax; ?> 
                                                    <td>{{ core()->formatBasePrice($price_with_tax) }}</td>
                                                </tr>
                                            @endforeach
                                    </table>
                                </div>

                                <table class="sale-summary">
                                    <tr>
                                        <td>{{ __('admin::app.sales.orders.subtotal') }}</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($order->base_sub_total) }}</td>
                                    </tr>

                                    <?php $discounted_subtotal=0; ?>
                                    @if($order->mem_discount_amount == 0.0000 && $order->promo_code_amount == 0.0000 && $order->credit_discount_amount == 0.0000 && $order->reward_coin == 0.0000 && $order->base_discount_amount != 0.0000)
                                    <tr>
                                        <td class="pull-left text-left">Special Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->base_discount_amount) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->base_discount_amount; ?>

                                    @endif

                                    @if($order->mem_discount_amount != 0.0000)
                                    <tr>
                                        <td class="pull-left text-left">Membership Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->mem_discount_amount) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Group:</td>
                                        <td>:</td>
                                        <td> {{ $order->customer->customerGroup['name'] }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->mem_discount_amount; ?>

                                    @endif

                                    @if($order->promo_code_amount != 0.0000)
                                        <tr>
                                            <td>Promo Code Discount:</td>
                                            <td>:</td>
                                            <td>- {{ core()->currency($order->promo_code_amount) }}</td>
                                        </tr>
                                        @if(isset($order->coupon_code) || $order->coupon_code != "")
                                            <tr>
                                                <td>Discount Code:</td>
                                                <td>:</td>
                                                <td> {{$order->coupon_code }}</td>
                                            </tr> 
                                        @endif
                                        <?php $discounted_subtotal = $order->base_sub_total - $order->promo_code_amount; ?>
                                    @endif

                                    @if($order->credit_discount_amount != 0.0000)
                                        <tr>
                                            <td>Advance Payment Discount:</td>
                                            <td>:</td>
                                            <td>- {{ core()->currency($order->credit_discount_amount) }}</td>
                                        </tr>
                                        @if(isset($order->coupon_code) || $order->coupon_code != "")
                                            <tr>
                                                <td>Discount Code:</td>
                                                <td>:</td>
                                                <td> {{$order->coupon_code }}</td>
                                            </tr> 
                                        @endif
                                        <?php $discounted_subtotal = $order->base_sub_total - $order->credit_discount_amount; ?>
                                    @endif

                                    @if($order->reward_coin != 0.0000)
                                    <tr>
                                        <td>Reward Points:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->reward_coin) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->reward_coin; ?>
                                    @endif
                                    @if($discounted_subtotal != 0)
                                    <tr>
                                        <td>Discounted Subtotal</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($discounted_subtotal) }}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>{{ __('admin::app.sales.orders.shipping-handling') }}</td>
                                        <td>:</td>
                                        <td>+ {{ core()->formatBasePrice($order->base_shipping_amount) }}</td>
                                    </tr>

                                    <tr class="border">
                                        <td>{{ __('admin::app.sales.orders.tax') }} (12%)</td>
                                        <td>:</td>
                                        <td>+ {{ core()->formatBasePrice($order->tax_amount) }}</td>
                                    </tr>

                                    <tr class="bold">
                                        <td>{{ __('admin::app.sales.orders.grand-total') }}</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                    </tr>
                                    <?php $payment_paid = DB::table('payments')->where('order_id',$order->id)->first(); 
                                    $manual_payment = DB::table('manual_payment_entry')->where('order_id',$order->id)->sum('paid_amount');
                                    //dd($manual_payment) ?>
                                    @if($order->payment[0]->method == 'razorpay' && isset($payment_paid))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                        </tr>
                                    @elseif($order->payment[0]->method == 'prepayment' || $order->payment[0]->method == 'paylaterwithcredit')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                        </tr>                                                           
                                    @elseif($order->payment[0]->method == 'cashondelivery' && isset($manual_payment))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>{{ core()->formatBasePrice($manual_payment) }}</td>
                                        </tr>
                                    @endif
                                    <!-- @if($order->status == 'completed')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>- {{ core()->formatBasePrice($order->base_grand_total_invoiced) }}</td>
                                        </tr>
                                    @endif  -->

                                    <tr class="bold">
                                        <td>{{ __('admin::app.sales.orders.total-refunded') }}</td>
                                        <td>:</td>
                                        
                                        <td>{{ core()->formatBasePrice($order->base_grand_total_refunded) }}</td>
                                        
                                    </tr> 
                                   
                                @if($order->payment[0]->method == 'cashondelivery' && isset($manual_payment))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                            <td> {{ core()->formatBasePrice($order->base_grand_total - $manual_payment)  }}</td>
                                        </tr>
                                @elseif($order->payment[0]->method == 'razorpay' && isset($payment_paid))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice(0) }}</td>
                                        </tr> 
                                        
                                @elseif($order->payment[0]->method == 'prepayment')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice(0) }}</td>
                                        </tr> 
                                @elseif($order->payment[0]->method == 'paylaterwithcredit')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice(0) }}</td>
                                        </tr> 
                                @else
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                        </tr> 
                                     @endif
                                </table>

                            </div>
                        </accordian>

                    </div>
                </tab>

                <tab name="{{ __('admin::app.sales.orders.invoices') }}">

                    <div class="table" style="padding: 20px 0">
                        <table>
                            <thead>
                                <tr>
                                    <th>{{ __('admin::app.sales.invoices.id') }}</th>
                                    <th>{{ __('admin::app.sales.invoices.date') }}</th>
                                    <th>{{ __('admin::app.sales.invoices.order-id') }}</th>
                                    <th>{{ __('admin::app.sales.invoices.customer-name') }}</th>
                                    <th>{{ __('admin::app.sales.invoices.status') }}</th>
                                    <th>{{ __('admin::app.sales.invoices.amount') }}</th>
                                    <th>{{ __('admin::app.sales.invoices.action') }}</th>
                                </tr>
                            </thead>

                            <tbody>
                              
                                @foreach ($order->invoices as $invoice)
                                <?php $customer_name = DB::table('users as u')->where('u.id',$order->customer_id)->first();
                                
                                 ?>
                                    <tr>
                                        <td>#{{ $invoice->id }}</td>
                                        <td>{{ $invoice->created_at }}</td>
                                        <td>#{{ $invoice->order->id }}</td>
                                        <td>{{ $customer_name->my_title }} {{ $customer_name->first_name }} {{ $customer_name->last_name }}</td>
                                        <td>{{ $order->status }}</td>
                                        <td>{{ core()->formatBasePrice($invoice->base_grand_total) }}</td>
                                        <td class="action">
                                            <a href="{{ route('admin.sales.invoices.view', $invoice->id) }}">
                                                <i class="icon eye-icon"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                @if (! $order->invoices->count())
                                    <tr>
                                        <td class="empty" colspan="7">{{ __('admin::app.common.no-result-found') }}</td>
                                    <tr>
                                @endif
                        </table>
                    </div>

                </tab>

                <tab name="{{ __('admin::app.sales.orders.shipments') }}">

                    <div class="table" style="padding: 20px 0">
                        <table>
                            <thead>
                                <tr>
                                    <th>{{ __('admin::app.sales.shipments.id') }}</th>
                                    <th>{{ __('admin::app.sales.shipments.date') }}</th>
                                    <th>{{ __('admin::app.sales.shipments.order-id') }}</th>
                                    <th>{{ __('admin::app.sales.shipments.order-date') }}</th>
                                    <th>{{ __('admin::app.sales.shipments.customer-name') }}</th>
                                    <th>{{ __('admin::app.sales.shipments.total-qty') }}</th>
                                    @if(auth()->guard('admin')->user()->role['id'] == 1)
                                        <th>{{ __('admin::app.sales.shipments.action') }}</th>
                                    @endif
                                </tr>
                            </thead>

                            <tbody>
                                
                                @foreach ($order->shipments as $shipment)
                                
                                <?php  $shipped_to = \DB::table('orders as o')
											->leftjoin('user_addresses as u_addr','o.shipping_address','u_addr.id')
											->where('u_addr.customer_id',$shipment->customer_id)
                                            ->where('o.id',$shipment->order_id)
                                            ->first(); 
                               
                                 ?>
                                    <tr>
                                        <td>#{{ $shipment->id }}</td>
                                        <td>{{ $shipment->created_at }}</td>
                                        <td>#{{ $shipment->order->id }}</td>
                                        <td>{{ $shipment->order->created_at }}</td>
                                        <td>{{ $customer_name->my_title }} {{ $customer_name->first_name }} {{ $customer_name->last_name }}</td>
                                        <td>{{ $shipment->total_qty }}</td>
                                        @if(auth()->guard('admin')->user()->role['id'] == 1)
                                            <td class="action">
                                                <a href="{{ route('admin.sales.shipments.view', $shipment->id) }}">
                                                    <i class="icon eye-icon"></i>
                                                </a>
                                                <a data-carr-id="{{ $shipment->shipping_carrier_id }}" data-track-no="{{ $shipment->track_number }}" class="tracking-info" data-toggle="modal" data-target="#UpdateTrackingInfo" href=""><i class="icon pencil-lg-icon"></i></a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach

                                @if (! $order->shipments->count())
                                    <tr>
                                        <td class="empty" colspan="7">{{ __('admin::app.common.no-result-found') }}</td>
                                    <tr>
                                @endif
                        </table>
                    </div>

                </tab>

                <?php if(auth()->guard('admin')->user()->role_id == 1) { ?>
                <tab name="Customer Payments">
                    <div class="sale-container">
                        <div class="paid-amount col-md-12">
                            <form method="post" action="">
                                @csrf
                                <div class="form-field inner-form col-md-8">
                                    <div class="control-group col-md-6 pass">
                                        <label for="paid_amount" class="required">Enter Amount</label>
                                        <input type="text" class="control" name="paid_amount"  value="{{ old('paid_amount') }}">
                                        <span class="control-error">@{{ errors.first('paid_amount') }}</span>
                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                        <input type="hidden" name="dealer_id" value="{{$order->customer_id}}">
                                    </div>
                                    <div class="control-group col-md-6 cpass">
                                        <button type="submit" class="btn amount-btn btn-lg btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                         <div class="col-lg-12 section table-list">
                                            <div class="panels panel-default">
                                                <div class="panel-title">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-shopping-cart"></i> Paid History
                                                    </div>
                                                </div>
                                                <!-- /.panel-heading -->
                                                <div class="panel-body" style="clear: both; overflow: scroll;">
                                                <table width="100%" class="data-tables table table-striped table-bordered table-hover" id="data-table">
                                          <thead>
                                            
                                              <tr>
                                                  <th>Paid Date</th>
                                                  <th>Paid Amount</th>
                                                  <th>Remaining Amount</th>
                                                  <th>Action</th>
                                                  
                                              </tr>
                                             
                                            </thead>
                                            <tbody>
                                               <?php 
                                                $sofar_paid='';
                                                
                                                 ?>
                                                @foreach($paid_history as $paid_info)
                                                <?php 
                                                $sofar_paid = '';
                                                $sofar_paid = DB::table('manual_payment_entry')->where('order_id',$order->id)->where('is_deleted',0)->where('created_at','<=',$paid_info->created_at)->sum('paid_amount');
                                               
                                                $remaining_amount= abs($order->grand_total - $sofar_paid);
                                                 ?>
                                                <tr>
                                                    <td>{{$paid_info->created_at}}</td>
                                                    <td>{{$paid_info->paid_amount}}</td>
                                                    <td>{{$remaining_amount}}</td>
                                                    <td class="action-menu">
                                                        <!-- <a href=""><span class="icon pencil-lg-icon"></span></a> -->
                                                        <a href="{{route('admin.sales.paid_entries.delete',$paid_info->id)}}"><span class="icon trash-icon"></span></a>
                                                    </td>
                                                </tr>
                                                 @endforeach
                                                 <?php 
                                                 $sofar_paid_amount = DB::table('manual_payment_entry')->where('is_deleted',0)->where('order_id',$order->id)->sum('paid_amount');
                                                 //dd($sofar_paid_amount);
                                                $paid_invoiced= DB::table('orders')->where('id',$order->id)->update(['grand_total_invoiced' => $sofar_paid_amount]);
                                                 ?>
                                            </tbody>
                                        </table>
                                    </div>

                                                <!-- /.panel-body -->
                                            </div>
                                              <!-- /.panel -->
                                        </div>

                                <!-- /.panel-body -->
                            </div>
                              <!-- /.panel -->
                   
                </tab>
                
                <tab name="Dealer Commission">
                    <div class="sale-container">
                        <div class="paid-amount col-md-12">

                  <?php 
                    $commission_paid_amount=$balance_comm_amount=$dealer_comm_amt=0;
                    $btn_label="Save";
                    $route_name = 'admin.sales.pay.commission';
                  ?>
                  @if(isset($dealer_info))
                  <?php
                  
                        $dealer_comm_amt = $order->total_comm_amount;
                        $commission_paid_amount = DB::table('dealer_commission')->where('order_id',$order->id)->where('dealer_id',$dealer_info->id)->sum('commission_paid_amount');
                        
                        if($commission_paid_amount > 0) {
                            $balance_comm_amount = $dealer_comm_amt - $commission_paid_amount;
                        }
                ?>


                <form method="post" action="{{ route($route_name,$order->id) }}">
                                @csrf
                                @if($dealer_comm_amt == 0) <span class="comm-notset">The commission for this dealer is not yet set. Please set commission by their <a href="{{route('admin.dealer.edit',$dealer_info->id)}}">profile here.</a></span>
                                @endif
                                <div class="form-field inner-form col-md-12">
                                    {{-- <div class="control-group col-md-3 pass">
                                        <label>Commission(%)</label>
                                        <input type="text" class="control" name="dealer_commmision_per"  value="{{ $dealer_comm_prcent }}" disabled>
                                    </div> --}}
                                     <div class="control-group col-md-3 pass">
                                        <label>Commission Amount(Rs.)</label>
                                        <input type="text" class="control" name="dealer_comm_amt"  value="{{ $dealer_comm_amt }}" disabled>
                                    </div>
                                    @if($commission_paid_amount > 0)
                                    <div class="control-group col-md-3 pass">
                                        <label>Commission paid(Rs.)</label>
                                        <input type="text" class="control" name="comm_paid"  value="{{ $commission_paid_amount }}" disabled>
                                    </div>
                                    <div class="control-group col-md-3 pass">
                                        <label>Commission to pay(Rs.)</label>
                                        <input type="text" class="control" name="balance_comm_amount"  value="{{ ($balance_comm_amount <= 0) ? 0 : $balance_comm_amount  }}" disabled>
                                    </div>
                                    @endif
                                    <div class="control-group col-md-3 pass" :class="[errors.has('commission_paid_amount') ? 'has-error' : '']">
                                        <label class="required">Enter Paid Commission (Rs.)</label>
                                        <input type="text"  v-validate="'required'" class="control" name="commission_paid_amount"  value="">
                                        <span class="control-error" v-if="errors.has('commission_paid_amount')">@{{ errors.first('commission_paid_amount') }}</span>
                                    </div>
                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <input type="hidden" name="dealer_id" value="{{$dealer_info->id}}">
                                    <input type="hidden" name="dealer_comm_amt" value="{{$dealer_comm_amt}}">
                                    <div class="control-group col-md-3 cpass">
                                        <button type="submit" class="btn amount-btn btn-lg btn-primary">
                                            {{ $btn_label }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                           @else
                                <span class="comm-notset">There is no dealer for this doctor.</span>
                           @endif
                        </div>
                    </div>

                </tab>
            <?php } ?>


                @if(in_array(auth()->guard('admin')->user()->role_id, [1,4]))
                    <?php $order_invoices = DB::table('order_invoice')->where('order_id', $order->id)->get(); ?>

                   
                    <tab name="Bills">
                        <div class="sale-container" style="padding:20px">
                             @if($order->status == 'completed' && count($order_invoices)<=0)
                                <span>No Invoice Available</span>
                             @endif

                            <form method="post"  class="bill_form" action="{{ route('admin.sales.orders.order-invoice') }}" style="display:flex; align-items:center;" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="order_id" value="{{$order->id}}">
                                <div class="file-list">
                                    <table>
                                        @if(isset($order_invoices) && count($order_invoices)>0)
                                            @foreach($order_invoices as $invoice_file)
                                                <tr>
                                                    <td>File: <strong>{{$invoice_file->file_name}}</strong></td>
                                                    <td>
                                                        <a class="btn btn-sm btn-primary btn-download" href="{{route('admin.sales.orders.order-invoice-download', $invoice_file->id)}}">Download</a>
                                                        @if($order->status != 'completed')
                                                            <button class="btn btn-sm btn-primary btn-delete" style="margin-left:10px;" data-invoice_id="{{$invoice_file->id}}" data-order_id="{{ $order->id }}">Delete</button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                        @if($order->status != 'completed')
                                            <tr>
                                                <td><input type="file" name="invoice_file" id="invoice file" accept="application/pdf,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"></td>
                                                <td>
                                                    <input type="submit" name="type" class="btn btn-sm btn-primary btn-sendinvoice" value="Send Invoice" data-order_id="{{ $order->id }}">
                                                    <input type="submit" name="type" class="btn btn-sm btn-primary btn-order" value="Complete Order" data-order_id="{{ $order->id }}">
                                                </td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </form>
                        </div>
                    </tab>
                @endif
            </tabs>
        </div>
    </div>

    <div class="modal fade" id="changeOrderStatus" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Order Status</h4>
        </div>
        <div class="modal-body col-md-12">
            <div class="change-order-status col-md-12">
                <form method="post" action="{{ route('admin.change.order.status')}}" onsubmit="return confirm('Are you sure you want to change order status?');">
                    @csrf
                    <h5 class="text-center col-md-4">Change Order Status</h5>
                    <span class="col-md-1 text-center">:</span>
                    <span class="col-md-5 ch-st">
                        <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="radio" id="delivered" name="order_status" class="orderstatus" value="delivered">
                    <label for="delivered">Delivered</label>
                    <input type="radio" id="completed" name="order_status" class="orderstatus" value="completed">
                    <label for="completed">Completed</label>
                </span>
                    <input type="submit" name="change_order_status" class="col-md-2 btn btn-primary change-status" value="Change" >
                </form>
            </div>
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>

    <div class="modal fade" id="UpdateTrackingInfo" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Tracking Details</h4>
        </div>
        <div class="modal-body col-md-12">
            <div class="change-order-status col-md-12">
                <form method="post" action="{{ route('admin.update.shipment.tracking', $order->id)}}" onsubmit="return confirm('Are you sure you want to update shipment tracking info?');">
                    @csrf
                    <h3>Tracking Information</h3>
                    <div class="tracking sale-section col-md-12">
                        <div class="">
                            <?php $shipping_carriers = DB::table('shipping_carriers')->where('status',1)->where('is_deleted',0)->get(); ?>
                            <div class="form-field section-content col-md-12">
                                <div class="control-group col-md-6" :class="[errors.has('shipment[shipping_carrier_id]') ? 'has-error' : '']">
                                    <label class="required" for="shipment[shipping_carrier_id]">Shipping Carriers</label>
                                    <select class="control carr-id" v-validate="'required'" name="shipment[shipping_carrier_id]">
                                        @foreach($shipping_carriers as $shipping_carrier)
                                            <option value="{{$shipping_carrier->id}}">{{$shipping_carrier->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="control-error" v-if="errors.has('shipment[shipping_carrier_id]')">@{{ errors.first('shipment[shipping_carrier_id]') }}</span>
                                </div>

                                <div class="control-group col-md-6" :class="[errors.has('shipment[track_number]') ? 'has-error' : '']">
                                    <label class="required" for="shipment[track_number]">{{ __('admin::app.sales.shipments.tracking-number') }}</label>
                                    <input type="text" v-validate="'required'" class="control track-no" id="shipment[track_number]" name="shipment[track_number]" data-vv-as="&quot;{{ __('admin::app.sales.shipments.tracking-number') }}&quot;"/>
                                    <span class="control-error" v-if="errors.has('shipment[track_number]')">@{{ errors.first('shipment[track_number]') }}</span>
                                </div>
                            </div>
                        </div>
                        </div>
                    <input type="submit" name="change_order_status" class="col-md-2 btn btn-primary change-status" value="Update" >
                </form>
            </div>
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>



  <!-- Modal -->
    <div class="modal fade" id="cancelOrderModel" tabindex="-1" role="dialog" aria-labelledby="cancelOrder" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="cancelOrder">Cancel Order</h3>
          </div>
          <div class="modal-body">
            <form action="{{ route('admin.sales.orders.cancel', $order->id) }}" id="cancelForm" method="POST">
                @csrf
                <div class="control-group col-md-12">
                    <label class="required" for="cancelReason">Reason for Cancel</label>
                    <textarea class="control" id="cancelReason" name="cancel_reason"></textarea>
                    <span class="control-error">This field is required</span>
                </div>
            </form>
          </div>
          <div class="modal-footer" style="clear: both;">
            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary btn-lg" id="cancel_order">Cancel order</button>
          </div>
        </div>
      </div>
    </div>

      <!-- Modal -->

    @if($order->status == "canceled")
        <div class="modal fade" id="cancelReasonModel" tabindex="-1" role="dialog" aria-labelledby="cancelReason" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="cancelOrder">Reason for Cancelation</h4>
              </div>
              <div class="modal-body">
                 <span style="font-size: 16px">{{  $order->remarks }}</span>
              </div>
              <div class="modal-footer" style="clear: both;">
                <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    @endif



@stop
@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#cancel_order').click(function(e){
            const reason = $('#cancelForm');
            reason.find('.control-error').text('');
            if(reason.find('#cancelReason').val()){
                $('#cancelForm').submit();
                return;
            }
            reason.find('.control-error').text('This field is required').show();
            e.preventDefault();
        });

        $('body').find('.tracking-info').on('click', function(event){
                var carr_id = $(this).data('carr-id');
                var track_no = $(this).data('track-no');
                $('#UpdateTrackingInfo').find(".carr-id").val(carr_id);
                $('#UpdateTrackingInfo').find(".track-no").val(track_no);
        });

        const order_route = '{{route('admin.sales.orders.order-invoice')}}';

        $('.bill_form').on('click', '.btn-delete', function(e){
            let invoice_id = $(this).data('invoice_id');
            let order_id = $(this).data('order_id');
            let type = 'Delete';
            $.ajax({
                url : order_route,
                method: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    type: type,
                    order_id : order_id,
                    invoice_id : invoice_id,
                },
                success: function(res){
                    console.log(res);
                    location.reload(true);
                },
                error:function(res){
                    console.log(res);
                    //location.reload(true);
                }
            });
            e.preventDefault();
        });


    });
</script>
<style type="text/css">
    .file-list td{
        padding: 10px 5px;
    }
    .modal-header{
        position: relative;
    }
</style>
@endpush
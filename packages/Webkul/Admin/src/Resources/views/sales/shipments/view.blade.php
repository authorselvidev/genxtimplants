@extends('admin::layouts.master')

@section('page_title')
    {{ __('admin::app.sales.shipments.view-title', ['shipment_id' => $shipment->id]) }}
@stop

@section('content-wrapper')
    <?php $order = $shipment->order; 
    ?>

    <div class="content full-page">
        <div class="page-header">
            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                    {{ __('admin::app.sales.shipments.view-title', ['shipment_id' => $shipment->id]) }}
                </h1>
            </div>

            <div class="page-action">
            </div>
        </div>

        <div class="page-content">
            <div class="sale-container">

                <accordian :title="'{{ __('admin::app.sales.orders.order-and-account') }}'" :active="true">
                    <div class="col-md-12" slot="body">

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                            <div class="secton-title">
                                <span>{{ __('admin::app.sales.orders.order-info') }}</span>
                            </div>

                            <div class="section-content">
                                <div class="rows">
                                    <span class="title col-md-5"> 
                                        {{ __('admin::app.sales.shipments.order-id') }}
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        <a href="{{ route('admin.sales.orders.view', $order->id) }}">#{{ $order->id }}</a>
                                    </span>
                                </div>

                                <div class="rows">
                                    <span class="title col-md-5"> 
                                        {{ __('admin::app.sales.orders.order-date') }}
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6"> 
                                        {{ $order->created_at }}
                                    </span>
                                </div>

                                <div class="rows">
                                    <span class="title col-md-5">
                                        {{ __('admin::app.sales.orders.order-status') }}
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        {{ $order->status_label }}
                                    </span>
                                </div>

                                <div class="rows">
                                    <span class="title col-md-5">
                                        {{ __('admin::app.sales.orders.channel') }}
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        {{ $order->channel_name }}
                                    </span>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                            <div class="secton-title">
                                <span>{{ __('admin::app.sales.orders.account-info') }}</span>
                            </div>

                            <div class="section-content">
                                <div class="rows">
                                    <span class="title col-md-5"> 
                                        {{ __('admin::app.sales.orders.customer-name') }}
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        <?php $customer_title = DB::table('users')->where('email',$order->customer_email)->value('my_title'); ?>
                                      {{ $customer_title }} {{ $order->customer_first_name}} {{ $order->customer_last_name}}
                                    </span>
                                </div>

                                <div class="rows">
                                    <span class="title col-md-5"> 
                                        {{ __('admin::app.sales.orders.email') }}
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6"> 
                                        {{ $order->customer_email}}
                                    </span>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
                </accordian>

                <accordian :title="'{{ __('admin::app.sales.orders.address') }}'" :active="true">
                    <div class="billing-addr col-md-12" slot="body">

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                                <div class="secton-title">
                                    <span>{{ __('admin::app.sales.orders.billing-address') }}</span>
                                </div>
                                <?php $billing_address = DB::table('user_addresses')->where('id',$shipment->billingAdress)->first();  ?>
                                <div class="section-content">
                                    @include ('admin::sales.address', ['address' => $billing_address])
                                </div>
                            </div>
                        </div>

                        @if ($shipment->shippingAdress)
                            <div class="sale-section col-md-6">
                                <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.shipping-address') }}</span>
                                    </div>
                                    <?php $shipping_address = DB::table('user_addresses')->where('id',$shipment->shippingAdress)->first();  ?>
                                    <div class="section-content">
                                        @include ('admin::sales.address', ['address' => $shipping_address])
                                    </div>
                                </div>
                            </div>
                        @endif
                    
                    </div>
                </accordian>

                <accordian :title="'{{ __('admin::app.sales.orders.payment-and-shipping') }}'" :active="true">
                    <div class="col-md-12" slot="body">

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                                <div class="secton-title">
                                    <span>{{ __('admin::app.sales.orders.payment-info') }}</span>
                                </div>

                                <div class="section-content">
                                    <div class="rows">
                                        <span class="title col-md-5"> 
                                            {{ __('admin::app.sales.orders.payment-method') }}
                                        </span>
                                        <span class="text-center col-md-1">:</span>
                                        <span class="value col-md-6"> 
                                            @if($order->payment[0]->method == 'paylaterwithcredit')
                                                {{ "Pay with ".(($order->customer->role_id=='2')?'credit':'advance') }}
                                            @else
                                                {{ core()->getConfigData('sales.paymentmethods.' . $order->payment[0]->method . '.title') }}
                                            @endif

                                            @if(isset($prepayment) && $prepayment !=null && $prepayment->approved_by_credit==1)
                                                (Dealer Payment used)
                                            @endif
                                        </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.currency') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->order_currency_code }}
                                            </span>
                                        </div>

                                        @if(isset($prepayment) && $prepayment !=null)
                                            @if($prepayment->amount)
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        Amount
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ $prepayment->amount }}
                                                    </span>
                                                </div>
                                            @endif
                                            
                                            @if($prepayment->payment_reference)
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        TXN ID/Cheque Number
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ $prepayment->payment_reference }}
                                                    </span>
                                                </div>
                                            @endif
                                            
                                            @if($prepayment->comments && $prepayment->comments != '')
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        Comments
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ $prepayment->comments }}
                                                    </span>
                                                </div>
                                            @endif
                                        @endif
                                </div>
                            </div>
                        </div>

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                            <div class="secton-title">
                                <span>{{ __('admin::app.sales.orders.shipping-info') }}</span>
                            </div>

                            <div class="section-content">
                                @if($order->shipping_title)
                                    <div class="rows">
                                        <span class="title col-md-5"> 
                                            {{ __('admin::app.sales.orders.shipping-method') }}
                                        </span>
                                        <span class="text-center col-md-1">:</span>
                                        <span class="value col-md-6"> 
                                            {{ $order->shipping_title }}
                                        </span>
                                    </div>
                                @endif

                                @if($order->base_shipping_amount)
                                    <div class="rows">
                                        <span class="title col-md-5"> 
                                            {{ __('admin::app.sales.orders.shipping-price') }}
                                        </span>
                                        <span class="text-center col-md-1">:</span>
                                        <span class="value col-md-6"> 
                                            {{ core()->formatBasePrice($order->base_shipping_amount) }}
                                        </span>
                                    </div>
                                @endif

                                @if ($shipment->inventory_source && $shipment->inventory_source->name)
                                    <div class="rows">
                                        <span class="title col-md-5"> 
                                            {{ __('admin::app.sales.shipments.inventory-source') }}
                                        </span>
                                        <span class="text-center col-md-1">:</span>
                                        <span class="value col-md-6"> 
                                            {{ $shipment->inventory_source->name }}
                                        </span>
                                    </div>
                                @endif

                                <div class="rows">
                                    <span class="title col-md-5"> 
                                        Shipping Carrier
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6"> 
                                        <?php $get_carrier = DB::table('shipping_carriers')->where('id',$shipment->shipping_carrier_id)->first();?>
                                        {{ $get_carrier->name or "" }}
                                    </span>
                                </div>

                                <div class="rows">
                                    <span class="title col-md-5"> 
                                        {{ __('admin::app.sales.shipments.tracking-number') }}
                                    </span>
                                    <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6"> 
                                        {{ $shipment->track_number }}
                                    </span>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </accordian>

                <accordian :title="'{{ __('admin::app.sales.orders.products-ordered') }}'" :active="true">
                    <div slot="body">

                        <div class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>{{ __('admin::app.sales.orders.SKU') }}</th>
                                        <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                        <th>{{ __('admin::app.sales.orders.qty') }}</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach ($shipment->items as $item)
                                        <tr>
                                            <td>{{ $item->sku }}</td>
                                            <td>
                                                {{ $item->name }}

                                                @if ($html = $item->getOptionDetailHtml())
                                                    <p>{{ $html }}</p>
                                                @endif
                                            </td>
                                            <td>{{ $item->qty }}</td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </accordian>

            </div>
        </div>
    </div>
@stop

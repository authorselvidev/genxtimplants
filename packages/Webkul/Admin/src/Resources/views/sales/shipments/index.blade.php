@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.sales.shipments.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.sales.shipments.title') }}</h1>
            </div>

            <div class="page-action">
                <div class="export" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span>
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="page-content">
            @inject('orderShipmentsGrid', 'Webkul\Admin\DataGrids\OrderShipmentsDataGrid')
            {!! $orderShipmentsGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
     <div class="modal fade" id="UpdateTrackingInfo" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content col-md-12">
        <div class="modal-header col-md-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Tracking Details</h4>
        </div>
        <div class="modal-body col-md-12">
            <div class="change-order-status col-md-12">
                {{-- {{ route('admin.update.shipment.tracking', $order->id)}} --}}
                <form method="post" action="" onsubmit="return confirm('Are you sure you want to update shipment tracking info?');">
                    @csrf
                    <h3>Tracking Information</h3>
                    <div class="tracking sale-section col-md-12">
                        <?php $shipping_carriers = DB::table('shipping_carriers')->where('status',1)->where('is_deleted',0)->get(); ?>
                            <div class="">
                                <div class="form-field section-content col-md-12">
                                <div class="control-group col-md-6" :class="[errors.has('shipment[shipping_carrier_id]') ? 'has-error' : '']">
                                        <label class="required" for="shipment[shipping_carrier_id]">Shipping Carriers</label>
                                        <select class="control carr-id" v-validate="'required'" name="shipment[shipping_carrier_id]" data-vv-as="&quot;Shipping Carriers&quot;">
                                            @foreach($shipping_carriers as $shipping_carrier)
                                            <option value="{{$shipping_carrier->id}}">{{$shipping_carrier->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="control-error" v-if="errors.has('shipment[shipping_carrier_id]')">@{{ errors.first('shipment[shipping_carrier_id]') }}</span>
                                    </div>

                                    <div class="control-group col-md-6" :class="[errors.has('shipment[track_number]') ? 'has-error' : '']">
                                        <label class="required" for="shipment[track_number]">{{ __('admin::app.sales.shipments.tracking-number') }}</label>
                                        <input type="text" v-validate="'required'" class="control track-no" id="shipment[track_number]" name="shipment[track_number]" data-vv-as="&quot;{{ __('admin::app.sales.shipments.tracking-number') }}&quot;"/>
                                        <span class="control-error" v-if="errors.has('shipment[track_number]')">@{{ errors.first('shipment[track_number]') }}</span>
                                    </div>
                            </div>
                        </div>
                        </div>
                    <input type="submit" name="change_order_status" class="col-md-2 btn btn-primary change-status" value="Update" >
                </form>
            </div>
        </div>
        <div class="modal-footer col-md-12">
          <!-- <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button> -->
        </div>
      </div>
    </div>
  </div>
@stop

@push('scripts')

<script type="text/x-template" id="export-form-template">
    <form method="POST" action="{{ route('admin.datagrid.export') }}">

        <div class="page-content">
            <div class="form-container">
                @csrf()

                <?php
                    $data = json_encode((array) $orderShipmentsGrid);
                ?>

                <input type="hidden" name="gridData" value="{{ $data }}">
                <input type="hidden" name="file_name" value="Shipment">

                <div class="control-group">
                    <label for="format" class="required">
                        {{ __('admin::app.export.format') }}
                    </label>
                    <select name="format" class="control" v-validate="'required'">
                        <option value="xls">XLS</option>
                        <option value="csv">CSV</option>
                    </select>
                </div>

            </div>
        </div>

        <button type="submit" class="btn btn-lg btn-primary" @click="closeModal">
            {{ __('admin::app.export.export') }}
        </button>

    </form>
</script>

<script>
    Vue.component('export-form', {
        template: '#export-form-template',
        methods: {
            closeModal () {
                this.$parent.closeModal();
            }
        }
    });

    $(document).ready(function(){
        $('body').find('.tracking-info').on('click', function(event){
            var order_id = $(this).data('order_id');
            var track_route = "{{ route('admin.update.shipment.tracking', ':order_id')}}"
            track_route = track_route.replace(':order_id', order_id);
            $('#UpdateTrackingInfo').find('form').attr('action',track_route);
            var carr_id = $(this).data('carr-id');
            var track_no = $(this).data('track-no');
            $('#UpdateTrackingInfo').find(".carr-id").val(carr_id);
            $('#UpdateTrackingInfo').find(".track-no").val(track_no);
        }); 
    });
</script>

@endpush

@extends('admin::layouts.master')

@section('page_title')
    {{ __('admin::app.sales.invoices.view-title', ['invoice_id' => $invoice->id]) }}
@stop

@section('content-wrapper')

    <?php $order = $invoice->order;
    $customer_info = DB::table('users')->where('id',$order->customer_id)->first();
    $dealer_info = DB::table('users')->where('id',$customer_info->dealer_id)->first(); 
   // dd($customer_info);
    //dd($invoice->order->payment); ?>

    <div class="content full-page">
        <div class="page-header">
            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                    {{ __('admin::app.sales.invoices.view-title', ['invoice_id' => $invoice->id]) }}
                </h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.sales.invoices.print', $invoice->id) }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.sales.invoices.print') }}
                </a>
            </div>
        </div>

        <div class="page-content">
            <div class="sale-container">

                <accordian :title="'{{ __('admin::app.sales.orders.order-and-account') }}'" :active="true">
                    <div class="col-md-12" slot="body">

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                            <div class="secton-title">
                                <span>{{ __('admin::app.sales.orders.order-info') }}</span>
                            </div>

                            <div class="section-content">
                                <div class="rows">
                                    <span class="title col-md-5">
                                        {{ __('admin::app.sales.invoices.order-id') }}
                                    </span>
                                     <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        <a href="{{ route('admin.sales.orders.view', $order->id) }}">#{{ $order->id }}</a>
                                    </span>
                                </div>

                                <div class="rows">
                                    <span class="title col-md-5">
                                        {{ __('admin::app.sales.orders.order-date') }}
                                    </span>
                                     <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        {{ $order->created_at }}
                                    </span>
                                </div>

                                <div class="rows">
                                    <span class="title col-md-5">
                                        {{ __('admin::app.sales.orders.order-status') }}
                                    </span>
                                     <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        {{ $order->status_label }}
                                    </span>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                            <div class="secton-title">
                                <span>{{ __('admin::app.sales.orders.account-info') }}</span>
                            </div>
                              <?php $customer = DB::table('users as u')->leftjoin('orders as o', 'o.customer_id', 'u.id')
                                ->where('o.customer_id',$order->customer_id)->first();
                                 ?>
                            <div class="section-content">
                                <div class="rows">
                                    <span class="title col-md-5">
                                        {{ __('admin::app.sales.orders.customer-name') }}
                                    </span>
                                     <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        {{ $customer->my_title }} {{ $customer->first_name }} {{ $customer->last_name }}
                                    </span>
                                </div>

                                <div class="rows">
                                    <span class="title col-md-5">
                                        {{ __('admin::app.sales.orders.email') }}
                                    </span>
                                     <span class="text-center col-md-1">:</span>
                                    <span class="value col-md-6">
                                        {{ $customer->email }}
                                    </span>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
                </accordian>

                <accordian :title="'{{ __('admin::app.sales.orders.address') }}'" :active="true">
                    <div class="billing-addr col-md-12" slot="body">

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                            <div class="secton-title">
                                <span>{{ __('admin::app.sales.orders.billing-address') }}</span>
                            </div>

                            <div class="section-content">

                            <?php $get_order = DB::table('orders')->where('id',$order->id)->first();
                            //dd($get_order);
                            $billing_address = DB::table('user_addresses')->where('id',$get_order->billing_address)->latest()->first();
                            ?>
                                        @include ('admin::sales.address', ['address' => $billing_address])

                            </div>
                        </div>
                        </div>

                        @if ($get_order->shipping_address)
                        <?php $shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->latest()->first(); ?>
                            <div class="sale-section col-md-6">
                                <div class="inner-sale">
                                <div class="secton-title">
                                    <span>{{ __('admin::app.sales.orders.shipping-address') }}</span>
                                </div>

                                <div class="section-content">

                                        @include ('admin::sales.address', ['address' => $shipping_address])

                                </div>
                                </div>
                            </div>
                        @endif

                    </div>
                </accordian>

                <accordian :title="'{{ __('admin::app.sales.orders.payment-and-shipping') }}'" :active="true">
                    <div class="col-md-12" slot="body">

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                                <div class="secton-title">
                                    <span>{{ __('admin::app.sales.orders.payment-info') }}</span>
                                </div>

                                <div class="section-content">
                                    <div class="rows">
                                        <span class="title col-md-5">
                                            {{ __('admin::app.sales.orders.payment-method') }}
                                        </span>
                                        <span class="text-center col-md-1">:</span>
                                        <span class="value col-md-6">
                                            @if($order->payment[0]->method == 'paylaterwithcredit')
                                                {{ "Pay with ".(($order->customer->role_id=='2')?'credit':'advance') }}
                                            @else
                                                {{ core()->getConfigData('sales.paymentmethods.' . $order->payment[0]->method . '.title') }}
                                            @endif

                                            @if(isset($prepayment) && $prepayment !=null && $prepayment->approved_by_credit==1)
                                                    (Dealer Payment used)
                                            @endif
                                        </span>
                                    </div>

                                    <div class="rows">
                                        <span class="title col-md-5">
                                            {{ __('admin::app.sales.orders.currency') }}
                                        </span>
                                        <span class="text-center col-md-1">:</span>
                                        <span class="value col-md-6">
                                            {{ $order->order_currency_code }}
                                        </span>
                                    </div>
                                    

                                    @if(isset($prepayment) && $prepayment !=null)
                                            @if($prepayment->amount)
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        Amount
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ $prepayment->amount }}
                                                    </span>
                                                </div>
                                            @endif
                                            
                                            @if($prepayment->payment_reference)
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        TXN ID/Cheque Number
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ $prepayment->payment_reference }}
                                                    </span>
                                                </div>
                                            @endif
                                            
                                            @if($prepayment->comments && $prepayment->comments != '')
                                                <div class="rows">
                                                    <span class="title col-md-5">
                                                        Comments
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6">
                                                        {{ $prepayment->comments }}
                                                    </span>
                                                </div>
                                            @endif
                                        @endif
                                </div>
                            </div>
                        </div>

                        <div class="sale-section col-md-6">
                            <div class="inner-sale">
                                <div class="secton-title">
                                    <span>{{ __('admin::app.sales.orders.shipping-info') }}</span>
                                </div>

                                <div class="section-content">
                                    @if($order->shipping_title)
                                        <div class="rows">
                                                <span class="title col-md-5">
                                                    {{ __('admin::app.sales.orders.shipping-method') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    {{ $order->shipping_title }}
                                                </span>
                                            </div>
                                        @endif

                                        @if($order->base_shipping_amount)
                                            <div class="rows">
                                                <span class="title col-md-5">
                                                    {{ __('admin::app.sales.orders.shipping-price') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6">
                                                    {{ core()->formatBasePrice($order->base_shipping_amount) }}
                                                </span>
                                            </div>
                                        @endif

                                        @if($shipment)
                                            @if ($inventory_source)
                                                <div class="rows">
                                                    <span class="title col-md-5"> 
                                                        {{ __('admin::app.sales.shipments.inventory-source') }}
                                                    </span>
                                                    <span class="text-center col-md-1">:</span>
                                                    <span class="value col-md-6"> 
                                                        {{ $inventory_source->name }}
                                                    </span>
                                                </div>
                                            @endif

                                            <div class="rows">
                                                <span class="title col-md-5"> 
                                                    Shipping Carrier
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6"> 
                                                    <?php $get_carrier = DB::table('shipping_carriers')->where('id',$shipment->shipping_carrier_id)->first();?>
                                                    {{ $get_carrier->name or "" }}
                                                </span>
                                            </div>

                                            <div class="rows">
                                                <span class="title col-md-5"> 
                                                    {{ __('admin::app.sales.shipments.tracking-number') }}
                                                </span>
                                                <span class="text-center col-md-1">:</span>
                                                <span class="value col-md-6"> 
                                                    {{ $shipment->track_number }}
                                                </span>
                                            </div>
                                        @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </accordian>

                <accordian :title="'{{ __('admin::app.sales.orders.products-ordered') }}'" :active="true">
                    <div slot="body">
                        <?php $discounted_subtotals=array_column($order->items->toArray(), 'discounted_subtotal');
                        //dd($discounted_subtotals);
                            $discounted_subtotals = array_filter($discounted_subtotals); ?>
                        <div class="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>{{ __('admin::app.sales.orders.SKU') }}</th>
                                        <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                        <th>{{ __('admin::app.sales.orders.price') }}</th>
                                        <th>{{ __('admin::app.sales.orders.qty') }}</th>
                                        <th>Lot Number<br>(Num, Qty, Exp.date)</th> {{-- New --}}
                                        <th>{{ __('admin::app.sales.orders.subtotal') }}</th>
                                        @if(count($discounted_subtotals) > 0)
                                            <th>Discounted Sub Total</th>
                                        @endif
                                        <th>{{ __('admin::app.sales.orders.grand-total') }} <br> (incl.taxes)</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach ($invoice->items as $item)
                                        <tr>
                                            <td>{{ $item->child ? $item->child->sku : $item->sku }}</td>
                                            <td>
                                                {{ $item->name }}

                                                @if ($html = $item->getOptionDetailHtml())
                                                    <p>{{ $html }}</p>
                                                @endif
                                            </td>
                                            <td>{{ core()->formatBasePrice($item->base_price) }}</td>
                                            <td>{{ $item->qty }}</td>
                                            <?php
													$lot = DB::table('lot_items')->where('invoice_items_id', $item->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get();
												?>
												<td>
													@if(count($lot)>0)
														@foreach($lot as $key => $lot_item)
															{{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}},
															{{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}},
															{{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
														@endforeach
													@else
														-
													@endif
												</td> {{-- New --}}
                                            
                                            <td>{{ core()->formatBasePrice($item->base_total) }}</td>
                                            @if(count($discounted_subtotals) > 0)
                                            <?php $discounted_subtotal = DB::table('order_items')->where('id',$item->order_item_id)->value('discounted_subtotal'); ?>
                                                @if(isset($discounted_subtotal))
                                                    <td>{{ core()->formatBasePrice($discounted_subtotal) }}</td>
                                                @else
                                                    <td>NA</td>
                                                @endif
                                            @endif

                                             <?php $total_price_item = $item->base_total;
                                                    if(isset($discounted_subtotal))
                                                        $total_price_item = $discounted_subtotal; 
                                                    
                                        $get_tax = DB::table('tax_rates')->where('state',$customer_info->state_id)->first(); 
                                        $item_tax = $total_price_item * ($get_tax->tax_rate / 100); 
                                        $price_with_tax = $total_price_item + $item_tax; ?> 
                                                    <td>{{ core()->formatBasePrice($price_with_tax) }}</td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>

                        <table class="sale-summary">
                            <tr>
                                <td>{{ __('admin::app.sales.orders.subtotal') }}</td>
                                <td>:</td>
                                <td>{{ core()->formatBasePrice($invoice->base_sub_total) }}</td>
                            </tr>


                            <?php $discounted_subtotal=0; ?>
                            @if($order->mem_discount_amount == 0.0000 && $order->promo_code_amount == 0.0000 && $order->credit_discount_amount == 0.0000 && $order->reward_coin == 0.0000 && $order->base_discount_amount != 0.0000)
                                <tr>
                                    <td class="pull-left text-left">Special Discount:</td>
                                    <td>:</td>
                                    <td>- {{ core()->currency($order->base_discount_amount) }}</td>
                                </tr>
                                <?php $discounted_subtotal = $order->base_sub_total - $order->base_discount_amount; ?>
                            @endif

                            @if($order->mem_discount_amount != 0.0000)
                                <tr>
                                    <td class="pull-left text-left">Membership Discount:</td>
                                    <td>:</td>
                                    <td>- {{ core()->currency($order->mem_discount_amount) }}</td>
                                </tr>
                                <tr>
                                    <td>Group:</td>
                                    <td>:</td>
                                    <td> {{ $order->customer->customerGroup['name'] }}</td>
                                </tr>
                                <?php $discounted_subtotal = $order->base_sub_total - $order->mem_discount_amount; ?>
                            @endif

                            @if($order->promo_code_amount != 0.0000)
                                <tr>
                                    <td>Promo Code Discount:</td>
                                    <td>:</td>
                                    <td>- {{ core()->currency($order->promo_code_amount) }}</td>
                                </tr>
                                @if(isset($order->coupon_code) || $order->coupon_code != "")
                                    <tr>
                                        <td>Discount Code:</td>
                                        <td>:</td>
                                        <td> {{$order->coupon_code }}</td>
                                    </tr> 
                                @endif
                                <?php $discounted_subtotal = $order->base_sub_total - $order->promo_code_amount; ?>
                            @endif

                            @if($order->credit_discount_amount != 0.0000)
                                <tr>
                                    <td>Advance Payment Discount:</td>
                                    <td>:</td>
                                    <td>- {{ core()->currency($order->credit_discount_amount) }}</td>
                                </tr>
                                @if(isset($order->coupon_code) || $order->coupon_code != "")
                                    <tr>
                                        <td>Discount Code:</td>
                                        <td>:</td>
                                        <td> {{$order->coupon_code }}</td>
                                    </tr> 
                                @endif
                                <?php $discounted_subtotal = $order->base_sub_total - $order->credit_discount_amount; ?>
                            @endif

                            

                            @if($order->reward_coin !=null || $order->reward_coin != 0.0000)
                                <tr>
                                    <td>Reward Points:</td>
                                    <td>:</td>
                                    <td>- {{ core()->currency($order->reward_coin) }}</td>
                                </tr>
                                <?php $discounted_subtotal = $order->base_sub_total - $order->reward_coin; ?>
                            @endif

                            @if($discounted_subtotal != 0)
                                <tr>
                                    <td>Discounted Subtotal</td>
                                    <td>:</td>
                                    <td>+ {{ core()->formatBasePrice($discounted_subtotal) }}</td>
                                </tr>
                            @endif

                            <tr>
                                <td>{{ __('admin::app.sales.orders.shipping-handling') }}</td>
                                <td>:</td>
                                <td>+ {{ core()->formatBasePrice($order->base_shipping_amount) }}</td>
                            </tr>

                            <tr>
                                <td>{{ __('admin::app.sales.orders.tax') }} (12%)</td>
                                <td>:</td>
                                <td>+ {{ core()->formatBasePrice($invoice->tax_amount) }}</td>
                            </tr>

                            <tr class="bold">
                                <td>{{ __('admin::app.sales.orders.grand-total') }}</td>
                                <td>:</td>
                                <td>{{ core()->formatBasePrice($invoice->base_grand_total) }}</td>
                            </tr>
                        </table>

                    </div>
                </accordian>

            </div>
        </div>

    </div>
@stop

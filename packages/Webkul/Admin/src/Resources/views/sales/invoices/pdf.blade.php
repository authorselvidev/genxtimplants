<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
	<meta http-equiv="Cache-control" content="no-cache">
    <meta charset="utf-8">
    <title>GenXT</title>
<style type="text/css">
.clearfix:after{content:"";display:table;clear:both}
a{color:#0087C3;text-decoration:none}
body{position:relative;width:100%;height:29.7cm;margin:0 auto;color:#222;background:#FFF;font-family:Arial,sans-serif;font-size:1em}
header{padding:0 0 20px;margin-bottom:20px;border-bottom:1px solid #AAA}
#logo{float:left;width:55%}
#logo img{padding:20px 20px 0;height:100px}
#company{float:right;width:40%;}
#company h2{font-size:1.4em;margin:0 0 15px;text-align:center;}
#company label{width:50%;float:right;font-weight:bold;}
#company span{width:48%;float:left;text-align: right}
#details{margin-bottom:50px}
#client{float:left;width:45%}
#client .to{color:#777}
h2.address{background:#ddd;}
h2{font-size:1.4em;margin:0 0 5px}
h3.name{font-size:1.3em;font-weight:400;margin:0}
#invoice{float:right;width:45%text-align:right}
#invoice h1{color:#0087C3;line-height:1em;font-weight:400;margin:0 0 10px}
#invoice .date{font-size:1.1em;color:#777}
table{width:100%;border-collapse:collapse;border-spacing:0;margin-bottom:20px}
table th,table td{padding:10px;background:#EEE;text-align:center;border-bottom:1px solid #FFF}
table th{white-space:nowrap;font-weight:400}
table td h3{color:#0a97cf;font-size:1.2em;font-weight:400;margin:0 0 .2em}
table .no{background:#dddddd}
table .desc{text-align:left}
table .unit{background:#DDD}
table .total{background:#0a97cf;color:#FFF}
table td.unit,table td.qty,table td.total{font-size:1.2em}
table tbody tr:last-child td{border:none}
table tfoot td{padding:10px 20px;background:#FFF;border-bottom:none;font-size:1.2em;white-space:nowrap;border-top:1px solid #AAA}
table tfoot tr:first-child td{border-top:none}
table tfoot tr:last-child td{color:#0a97cf;font-size:1.4em;border-bottom:1px solid #0a97cf}
table tfoot tr td:first-child{border:none}
#notices{margin:50px 0 0;padding-left:6px;text-align:center;}
#notices div{margin:10px 0}
.tick-box {
    border: 2px solid #333;
    vertical-align: middle;
}
</style>
  </head>
  <body>
    <?php $get_order = DB::table('orders')->where('id',$invoice->order_id)->first(); 
     $customer = DB::table('users')->where('id',$order->customer_id)->first(); ?>
    <header class="clearfix">
      <div id="logo">
        <img src="https://genxtimplants.com/themes/default/assets/images/genxtimplants-logo.png">
      </div>
      <div id="company">
        <h2 class="name">Delivery Note</h2>
		<div><span>{{ __('admin::app.sales.invoices.order-date') }} : </span> &nbsp; <label>{{ $invoice->created_at->format('M d, Y') }}</label></div>
        <div><span>Order ID : </span> &nbsp; <label>{{ $invoice->order_id }}</label></div>
        <div><span>Delivery Note : </span> &nbsp; <label>{{ $invoice->id }}</label></div>
        <?php  $dispatch_date = '-';
                if(isset($shipment))
            $dispatch_date = date('M d, Y',strtotime($shipment->created_at)); ?>
        <div><span>Dispatch Date : </span> &nbsp; <label>{{ $dispatch_date }}</label></div>
        <div><span>{{ __('admin::app.sales.orders.payment-method') }} : </span> &nbsp; <label>{{ core()->getConfigData('sales.paymentmethods.' . $invoice->order->payment[0]->method . '.title') }}</label></div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <h2 class="address">Shipping Address</h2>
          <?php  
                $shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->latest()->first(); ?>

            {{ str_replace('-',' ',$shipping_address->name) }}<br/>
            {{ $customer->clinic_name }} <br/>
            {{ $shipping_address->address }}, {{ $shipping_address->address2 ? $shipping_address->address2 . ',' : '' }}<br/>
            {{ Webkul\Customer\Models\Cities::GetCityName($shipping_address->city) }}<br/>
            {{ Webkul\Customer\Models\States::GetStateName($shipping_address->state) }}<br/>
            {{ Webkul\Customer\Models\Countries::GetCountryName($shipping_address->country) }} - {{ $shipping_address->postcode }}<br/><br/>
            Mobile No : {{ $shipping_address->phone }}  <br/>
          <div class="email">Email : <a href="mailto:{{$order->customer_email}}">{{$order->customer_email}}</a></div>
        </div>
        <div id="invoice">
          <h2 class="address">Billing Address</h2>
           <?php $billing_address = DB::table('user_addresses')->where('id',$get_order->billing_address)->latest()->first(); ?>
               
            {{  str_replace('-',' ',$billing_address->name) }}<br/>
            {{  $customer->clinic_name }} <br/>
            {{ $billing_address->address }}, {{ $billing_address->address2 ? $billing_address->address2 . ',' : '' }}<br/>
            {{ Webkul\Customer\Models\Cities::GetCityName($billing_address->city) }}<br/>
            {{ Webkul\Customer\Models\States::GetStateName($billing_address->state) }}<br/>
            {{ Webkul\Customer\Models\Countries::GetCountryName($billing_address->country) }} - {{ $billing_address->postcode }}<br/><br/>
            Mobile No : {{ $billing_address->phone }} <br/>
          <div class="email">Email : <a href="mailto:{{$order->customer_email}}">{{$order->customer_email}}</a></div>
        </div>
      </div>
      {{-- <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">SR.NO</th>
            <th class="desc">PRODUCT NAME</th>
            <th class="unit">ORDERED</th>
            <th class="qty">DELIVERED</th>
            <th class="total">TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="no">01</td>
            <td class="desc"><h3>Product 1</h3></td>
            <td class="unit">5</td>
            <td class="qty">4</td>
            <td class="total">1,200.00</td>
          </tr>
          <tr>
            <td class="no">02</td>
            <td class="desc"><h3>Product 2</h3></td>
            <td class="unit">7</td>
            <td class="qty">7</td>
            <td class="total">2,200.00</td>
          </tr>
          <tr>
            <td class="no">03</td>
            <td class="desc"><h3>Product 3</h3></td>
            <td class="unit">5</td>
            <td class="qty">4</td>
            <td class="total">1,200.00</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">GRAND TOTAL</td>
            <td>$6,500.00</td>
          </tr>
        </tfoot>
      </table> --}}

      <div class="table items">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                <th>Lot Number<br>(Num, Qty, Exp.date)</th>
                                <th>Ordered</th>
                                <th>Delivered</th>
                               {{--  <th>{{ __('admin::app.sales.orders.subtotal') }}</th>
                                <th>{{ __('admin::app.sales.orders.tax-amount') }}</th> --}}
                            </tr>
                        </thead>

                        <tbody>
                            <?php $srno = 1; $total_ordered=$total_shipped=0; ?>
                            @foreach ($order->items as $item)
                                <tr>
                                    <td>{{ $srno }}</td>
                                    <td>
                                        {{ $item->name }}
                                    </td>
                                    <td>
                                        <?php 
                                            $lot = DB::table('lot_items')->where('invoice_items_id', 
                                                DB::table('invoice_items')->where('order_item_id', $item->id)->addSelect('id')->first()->id)
                                                ->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get();
                                            ?>
                                            @if(count($lot)>0)
                                                @foreach($lot as $key => $lot_item)
                                                    {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}},
                                                    {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}},
                                                    {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
                                                @endforeach
                                            @else
                                                -
                                            @endif
                                    </td>
                                    <td>{{ $item->qty_ordered }}</td>
                                    <td>{{ $item->qty_shipped }}</td>
                                    {{-- <td>INR {{ $item->base_total }}</td>
                                    <td>INR {{ $item->base_tax_amount }}</td> --}}
                                </tr>
                                <?php $total_ordered += $item->qty_ordered; ?>
                                <?php $total_shipped += $item->qty_shipped;
                                $srno++; ?>
                            @endforeach

                        </tbody>
                        <tfoot>
                          <tr>
                            {{-- <td colspan="2"></td> --}}
                            <td colspan="2">TOTAL</td>
                            <td>{{$total_ordered}}</td>
                            <td>{{$total_shipped}}</td>
                          </tr>
                        </tfoot>
                    </table>
                </div>
      <div id="notices">
        <div><strong>Prosthetic Accessories :</strong>  &nbsp; &nbsp; <span class="tick-box">&nbsp;&nbsp;&nbsp;&nbsp;</span> Included &nbsp; &nbsp; &nbsp; <span class="tick-box">&nbsp;&nbsp;&nbsp;&nbsp;</span> Not Included</div>
		<div>TRA :______ &nbsp; &nbsp; BOP:______   &nbsp; &nbsp; ANA:_______</div>
        <div>This is computer generated Delivery Note No signature Required</div>
	    <div><strong>Thank you for your business!</strong></div>
      </div>
    </main>

<!--
				<div class="row sec2">
					<div class="col-md-6">
					{{-- {{ __('admin::app.sales.invoices.ship-to') }} --}}
						<ul>
							<li><h3>Shipping Address</h3></li>
							<li>First Name</li>
							<li>Clinic Name</li>
							<li>
								<?php  $get_order = DB::table('orders')->where('id',$invoice->order_id)->first();
								$shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->latest()->first(); ?>
								@include ('admin::sales.address', ['address' => $shipping_address])
								{{-- <p>{{ $invoice->order->shipping_address->name }}</p>
								<p>{{ $invoice->order->shipping_address->address1 }}, {{ $invoice->order->shipping_address->address2 ? $invoice->order->shipping_address->address2 . ',' : '' }}</p>
								<p>{{ $invoice->order->shipping_address->city }}</p>
								<p>{{ $invoice->order->shipping_address->state }}</p>
								<p>{{ country()->name($invoice->order->shipping_address->country) }} {{ $invoice->order->shipping_address->postcode }}</p> --}}
								{{-- {{ __('shop::app.checkout.onepage.contact') }} : {{ $invoice->order->shipping_address->phone }}  --}}
							</li>
						</ul>
					</div>
					<div class="col-md-6">
						{{-- {{ __('admin::app.sales.invoices.bill-to') }} --}}
						<ul class="pull-right">
							<li><h3>Billing Address</h3></li>
							<li>
							<?php
                            //dd($get_order);
                            $billing_address = DB::table('user_addresses')->where('id',$get_order->billing_address)->latest()->first(); ?>
							@include ('admin::sales.address', ['address' => $billing_address])
                              {{-- <p>{{ $invoice->order->billing_address->name }}</p>
								<p>{{ $invoice->order->billing_address->address1 }}, {{ $invoice->order->billing_address->address2 ? $invoice->order->billing_address->address2 . ',' : '' }}</p>
								<p>{{ $invoice->order->billing_address->city }}</p>
								<p>{{ $invoice->order->billing_address->state }}</p>
								<p>{{ country()->name($invoice->order->billing_address->country) }} {{ $invoice->order->billing_address->postcode }}</p> --}}
								{{--  {{ __('shop::app.checkout.onepage.contact') }} : {{ $invoice->order->billing_address->phone }}  --}}
							</li>
						</ul>           
					</div>
				</div>
				
                <div class="table items">
                    <table>
                        <thead>
                            <tr>
                                <th>{{ __('admin::app.sales.orders.SKU') }}</th>
                                <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                <th>{{ __('admin::app.sales.orders.price') }}</th>
                                <th>{{ __('admin::app.sales.orders.qty') }}</th>
                                <th>{{ __('admin::app.sales.orders.subtotal') }}</th>
                                <th>{{ __('admin::app.sales.orders.tax-amount') }}</th>
                                <th>{{ __('admin::app.sales.orders.grand-total') }}</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($invoice->items as $item)
                                <tr>
                                    <td>{{ $item->child ? $item->child->sku : $item->sku }}</td>
                                    <td>
                                        {{ $item->name }}

                                        @if ($html = $item->getOptionDetailHtml())
                                            <p>{{ $html }}</p>
                                        @endif
                                    </td>
                                    <td>INR {{ $item->base_price }}</td>
                                    <td>{{ $item->qty }}</td>
                                    <td>INR {{ $item->base_total }}</td>
                                    <td>INR {{ $item->base_tax_amount }}</td>
                                    <td>INR {{ $item->base_total + $item->base_tax_amount }}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
				
                <table class="sale-summary">
                    <tr>
                        <td>{{ __('admin::app.sales.orders.subtotal') }}</td>
                        <td>-</td>
                        <td>INR {{ $invoice->base_sub_total }}</td>
                    </tr>

                    <tr>
                        <td>{{ __('admin::app.sales.orders.shipping-handling') }}</td>
                        <td>-</td>
                        <td>INR {{ $invoice->base_shipping_amount }}</td>
                    </tr>

                    <tr>
                        <td>{{ __('admin::app.sales.orders.tax') }}</td>
                        <td>-</td>
                        <td>INR {{ $invoice->base_tax_amount }}</td>
                    </tr>

                    <tr class="bold">
                        <td>{{ __('admin::app.sales.orders.grand-total') }}</td>
                        <td>-</td>
                        <td>INR {{ $invoice->base_grand_total }}</td>
                    </tr>
                </table>	
	
  </body>
</html>

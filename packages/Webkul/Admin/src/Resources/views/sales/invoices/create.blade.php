@extends('admin::layouts.master')

@section('page_title')
    {{ __('admin::app.sales.invoices.add-title') }}
@stop

@section('content-wrapper')
    <div class="content full-page">
        <form method="POST" action="{{ route('admin.sales.invoices.store', $order->id) }}">
            @csrf()
            
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.sales.invoices.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.sales.invoices.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="sale-container">

                    <accordian :title="'{{ __('admin::app.sales.orders.order-and-account') }}'" :active="true">
                        <div class="col-md-12" slot="body">

                            <div class="sale-section col-md-6">
                                <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.order-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5"> 
                                                {{ __('admin::app.sales.invoices.order-id') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                <a href="{{ route('admin.sales.orders.view', $order->id) }}">#{{ $order->id }}</a>
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5"> 
                                                {{ __('admin::app.sales.orders.order-date') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6"> 
                                                {{ $order->created_at }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.order-status') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6">
                                                {{ $order->status_label }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5">
                                                {{ __('admin::app.sales.orders.channel') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>  
                                            <span class="value col-md-6">
                                                {{ $order->channel_name }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sale-section col-md-6">
                                <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.account-info') }}</span>
                                    </div>
                                    <?php $customer = DB::table('users as u')->leftjoin('orders as o', 'o.customer_id', 'u.id')
                                        ->where('o.customer_id',$order->customer_id)->first();
                                 ?>
                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5"> 
                                                {{ __('admin::app.sales.orders.customer-name') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6"> 
                                                {{ $customer->my_title }} {{ $customer->first_name }} {{ $customer->last_name }}
                                            </span>
                                        </div>
                                        <div class="rows">
                                            <span class="title col-md-5"> 
                                                {{ __('admin::app.sales.orders.email') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6"> 
                                                {{ $customer->email }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </accordian>

                    <accordian :title="'{{ __('admin::app.sales.orders.address') }}'" :active="true">
                        <div class="col-md-12 billing-addr" slot="body">

                            <div class="sale-section col-md-6">
                                <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.billing-address') }}</span>
                                    </div>

                                    <div class="section-content">
                                     <?php $get_order = DB::table('orders')->where('id',$order->id)->first();
                                        //dd($get_order);
                                        $billing_address = DB::table('user_addresses')->where('id',$get_order->billing_address)->latest()->first();
                                ?>   
                                        @include ('admin::sales.address', ['address' => $billing_address])
                                        
                                    </div>
                                </div>
                            </div>

                            @if ($get_order->shipping_address)
                            <?php $shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->latest()->first(); ?>
                                <div class="sale-section col-md-6">
                                    <div class="inner-sale">
                                        <div class="secton-title">
                                            <span>{{ __('admin::app.sales.orders.shipping-address') }}</span>
                                        </div>

                                        <div class="section-content">
                                        
                                            @include ('admin::sales.address', ['address' => $shipping_address])
                                            
                                        </div>
                                    </div>
                                </div>
                            @endif
                        
                        </div>
                    </accordian>

                    <accordian :title="'{{ __('admin::app.sales.orders.payment-and-shipping') }}'" :active="true">
                        <div class="col-md-12" slot="body">

                            <div class="sale-section col-md-6">
                                <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.payment-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5"> 
                                                {{ __('admin::app.sales.orders.payment-method') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6"> 
                                                @if($order->payment[0]->method == 'paylaterwithcredit')
                                                    {{ "Pay with ".(($order->customer->role_id=='2')?'credit':'advance') }}
                                                @else
                                                    {{ core()->getConfigData('sales.paymentmethods.' . $order->payment[0]->method . '.title') }}
                                                @endif
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5"> 
                                                {{ __('admin::app.sales.orders.currency') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6"> 
                                                {{ $order->order_currency_code }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sale-section col-md-6">
                                <div class="inner-sale">
                                    <div class="secton-title">
                                        <span>{{ __('admin::app.sales.orders.shipping-info') }}</span>
                                    </div>

                                    <div class="section-content">
                                        <div class="rows">
                                            <span class="title col-md-5"> 
                                                {{ __('admin::app.sales.orders.shipping-method') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6"> 
                                                {{ $order->shipping_title }}
                                            </span>
                                        </div>

                                        <div class="rows">
                                            <span class="title col-md-5"> 
                                                {{ __('admin::app.sales.orders.shipping-price') }}
                                            </span>
                                            <span class="text-center col-md-1">:</span>
                                            <span class="value col-md-6"> 
                                                {{ core()->formatBasePrice($order->base_shipping_amount) }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </accordian>

                    <accordian :title="'{{ __('admin::app.sales.orders.products-ordered') }}'" :active="true">
                        <div slot="body">

                            <div class="table">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>{{ __('admin::app.sales.orders.SKU') }}</th>
                                            <th>{{ __('admin::app.sales.orders.product-name') }}</th>
                                            <th>{{ __('admin::app.sales.invoices.qty-ordered') }}</th>
                                            <th>{{ __('admin::app.sales.invoices.qty-to-invoice') }}</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        @foreach ($order->items as $item)
                                            @if ($item->qty_to_invoice > 0)
                                                <tr>
                                                    <td>{{ $item->type == 'configurable' ? $item->child->sku : $item->sku }}</td>
                                                    <td>
                                                        {{ $item->name }}

                                                        @if ($html = $item->getOptionDetailHtml())
                                                            <p>{{ $html }}</p>
                                                        @endif
                                                    </td>
                                                    <td>{{ $item->qty_ordered }}</td>
                                            
                                                    <td>
                                                        <div class="control-group" id="group_id_{{ $item->id }}" :class="[errors.has('invoice[items][{{ $item->id }}]') ? 'has-error' : '']">
                                                            <input type="text" v-validate="'required|numeric|min:0'" style="margin-bottom: 5px" class="control invoice_qty" id="invoice[items][{{ $item->id }}]" name="invoice[items][{{ $item->id }}]" value="{{ $item->qty_to_invoice }}" data-vv-as="&quot;{{ __('admin::app.sales.invoices.qty-to-invoice') }}&quot;"/>
                                                            <div class="product_lot_list" data-id={{$item->id}} data-qty={{ $item->qty_to_invoice }}>
                                                                <div class="products_lot">
                                                                    <span class="product_close fa fa-times"></span>
                                                                    <div class="input-group">
                                                                        <label for="lot[{{$item->id}}][0]">Lot number:</label>
                                                                        <input type="text"  class="control" id="lot[{{$item->id}}][0]" name="lot[{{$item->id}}][0]">
                                                                    </div>
                                                                    <div class="input-group">
                                                                        <label for="qty[{{$item->id}}][0]">Quantity:</label>
                                                                        <input type="number" class="control qty" id="qty[{{$item->id}}][0]" name="qty[{{$item->id}}][0]" min=1 max="{{ $item->qty_to_invoice }}" value="{{$item->qty_to_invoice}}">
                                                                    </div>

                                                                    <div class="input-append date date-group input-group" id="dpMonths" data-date="{{date('m/Y')}}" data-date-format="mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                                                        <label for="expiry[{{$item->id}}][0]">Expiry date:</label>
                                                                        <input name="expiry[{{$item->id}}][0]" id="expiry[{{$item->id}}][0]" class="span2 add-on control" size="16" type="text" value="{{date('m/Y')}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button class="add_lot btn btn-sm btn-success">+</button>
                                                            <span class="control-error" v-if="errors.has('invoice[items][{{ $item->id }}]')">
                                                                @verbatim
                                                                    {{ errors.first('invoice[items][<?php echo $item->id ?>]') }}
                                                                @endverbatim
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </accordian>

                </div>
            </div>
        </form>
    </div>
@stop


@push('scripts')
<style type="text/css">

    .datepicker {
      top: 0;
      left: 0;
      padding: 4px;
      margin-top: 1px;
      -webkit-border-radius: 4px;
      -moz-border-radius: 4px;
      border-radius: 4px;
      
    }
    .datepicker:before {
      content: '';
      display: inline-block;
      border-left: 7px solid transparent;
      border-right: 7px solid transparent;
      border-bottom: 7px solid #ccc;
      border-bottom-color: rgba(0, 0, 0, 0.2);
      position: absolute;
      top: -7px;
      left: 6px;
    }
    .datepicker:after {
      content: '';
      display: inline-block;
      border-left: 6px solid transparent;
      border-right: 6px solid transparent;
      border-bottom: 6px solid #ffffff;
      position: absolute;
      top: -6px;
      left: 7px;
    }
    .datepicker > div {
      display: none;
    }
    .datepicker table {
      width: 100%;
      margin: 0;
    }
    .datepicker td,
    .datepicker th {
      text-align: center;
      width: 20px;
      height: 20px;
      -webkit-border-radius: 4px;
      -moz-border-radius: 4px;
      border-radius: 4px;
    }
    .datepicker td.day:hover {
      background: #eeeeee;
      cursor: pointer;
    }
    .datepicker td.day.disabled {
      color: #eeeeee;
    }
    .datepicker td.old,
    .datepicker td.new {
      color: #999999;
    }
    .datepicker td.active,
    .datepicker td.active:hover {
      color: #ffffff;
      background-color: #42c3fa;
      background-image: -moz-linear-gradient(top, #42c3fa, #1395cd);
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#42c3fa), to(#1395cd));
      background-image: -webkit-linear-gradient(top, #42c3fa, #1395cd);
      background-image: -o-linear-gradient(top, #42c3fa, #1395cd);
      background-image: linear-gradient(to bottom, #42c3fa, #1395cd);
      background-repeat: repeat-x;
      border-color: #1395cd #1395cd #42c3fa;
      border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
      *background-color: #1395cd;
      color: #fff;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    }
    .datepicker td.active:hover,
    .datepicker td.active:hover:hover,
    .datepicker td.active:focus,
    .datepicker td.active:hover:focus,
    .datepicker td.active:active,
    .datepicker td.active:hover:active,
    .datepicker td.active.active,
    .datepicker td.active:hover.active,
    .datepicker td.active.disabled,
    .datepicker td.active:hover.disabled,
    .datepicker td.active[disabled],
    .datepicker td.active:hover[disabled] {
      color: #ffffff;
      background-color: #0044cc;
      *background-color: #003bb3;
    }
    .datepicker td.active:active,
    .datepicker td.active:hover:active,
    .datepicker td.active.active,
    .datepicker td.active:hover.active {
      background-color: #003399 \9;
    }
    .datepicker td span {
      display: block;
      width: 47px;
      height: 40px;
      line-height: 40px;
      float: left;
      margin: 2px;
      cursor: pointer;
      -webkit-border-radius: 4px;
      -moz-border-radius: 4px;
      border-radius: 4px;
    }
    .datepicker td span:hover {
      background: #eeeeee;
    }
    .datepicker td span.active {
      color: #ffffff;
      background-color: #42c3fa;
      background-image: -moz-linear-gradient(top, #42c3fa, #1395cd);
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#42c3fa), to(#1395cd));
      background-image: -webkit-linear-gradient(top, #42c3fa, #1395cd);
      background-image: -o-linear-gradient(top, #42c3fa, #1395cd);
      background-image: linear-gradient(to bottom, #42c3fa, #1395cd);
      background-repeat: repeat-x;
      border-color: #1395cd #1395cd #42c3fa;
      border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
      *background-color: #42c3fa;
      color: #fff;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    }
    .datepicker td span.active:hover,
    .datepicker td span.active:focus,
    .datepicker td span.active:active,
    .datepicker td span.active.active,
    .datepicker td span.active.disabled,
    .datepicker td span.active[disabled] {
      color: #ffffff;
      background-color: #42c3fa;
      *background-color: #42c3fa;
    }
    .datepicker td span.active:active,
    .datepicker td span.active.active {
      background-color: #003399 \9;
    }
    .datepicker td span.old {
      color: #999999;
    }
    .datepicker th.switch {
      width: 145px;
    }
    .datepicker th.next,
    .datepicker th.prev {
      font-size: 21px;
    }
    .datepicker thead tr:first-child th {
      cursor: pointer;
    }
    .datepicker thead tr:first-child th:hover {
      background: #eeeeee;
    }
    .input-append.date .add-on i,
    .input-prepend.date .add-on i {
      display: block;
      cursor: pointer;
      width: 16px;
      height: 16px;
    }

    .products_lot{
        display: flex;
        padding: 5px 0px;
        margin: 5px 0px;
        position: relative;
    }

    .add_lot{
        margin-top: 10px;
        float:right;
    }

    .input-group{
        margin-right: 10px;
        width: 100%;
        display: inline-block;
        min-width: 60px;
    }

    .input-group label, .date-group label{
        text-align: left;
        font-weight: 500;
        font-weight: 14px;
        width: auto;
    }
    .date-group{
        margin-right: 0px;
        min-width: 150px;
        width: 100%;
    }

    .product_close{
        float:right;
        color: #F00;
        cursor: pointer;
        opacity: .6;
        order: 100;
        font-size: 1.6rem;
        margin-top: 2px;
        align-self: flex-start;
    }

    .product_close:hover{
        color: #F00;
        opacity: 1;
    }

    .error{
        color: #ff3939;
    }

    .control-error {
        display: block !important;
        width: 100% !important;
        margin: 0px !important;
        text-align: left  !important;
    }

    @media(max-width: 495px){
        .products_lot{
            margin: 10px 0px;
            flex-direction: column;
            border-top: 1px solid #AAA;
        }
        .input-group, .date-group{
            margin-right: 0px;
            margin-top: 10px;
        }
        .product_close{
            order: 0;
            align-self: flex-end;
        }
    }
</style>
@endpush

@push('scripts')
    <script type="text/javascript">
         
        !function( $ ) {
            
            var Datepicker = function(element, options){
                this.element = $(element);
                this.format = DPGlobal.parseFormat(options.format||this.element.data('date-format')||'mm/dd/yyyy');
                this.picker = $(DPGlobal.template)
                                    .appendTo('body')
                                    .on({
                                        click: $.proxy(this.click, this)//,
                                        //mousedown: $.proxy(this.mousedown, this)
                                    });
                this.isInput = this.element.is('input');
                this.component = this.element.is('.date') ? this.element.find('.add-on') : false;
                
                if (this.isInput) {
                    this.element.on({
                        focus: $.proxy(this.show, this),
                        //blur: $.proxy(this.hide, this),
                        keyup: $.proxy(this.update, this)
                    });
                } else {
                    if (this.component){
                        this.component.on('click', $.proxy(this.show, this));
                    } else {
                        this.element.on('click', $.proxy(this.show, this));
                    }
                }
            
                this.minViewMode = options.minViewMode||this.element.data('date-minviewmode')||0;
                if (typeof this.minViewMode === 'string') {
                    switch (this.minViewMode) {
                        case 'months':
                            this.minViewMode = 1;
                            break;
                        case 'years':
                            this.minViewMode = 2;
                            break;
                        default:
                            this.minViewMode = 0;
                            break;
                    }
                }
                this.viewMode = options.viewMode||this.element.data('date-viewmode')||0;
                if (typeof this.viewMode === 'string') {
                    switch (this.viewMode) {
                        case 'months':
                            this.viewMode = 1;
                            break;
                        case 'years':
                            this.viewMode = 2;
                            break;
                        default:
                            this.viewMode = 0;
                            break;
                    }
                }
                this.startViewMode = this.viewMode;
                this.weekStart = options.weekStart||this.element.data('date-weekstart')||0;
                this.weekEnd = this.weekStart === 0 ? 6 : this.weekStart - 1;
                this.onRender = options.onRender;
                this.fillDow();
                this.fillMonths();
                this.update();
                this.showMode();
            };
            
            Datepicker.prototype = {
                constructor: Datepicker,
                
                show: function(e) {
                    this.picker.show();
                    this.height = this.component ? this.component.outerHeight() : this.element.outerHeight();
                    this.place();
                    $(window).on('resize', $.proxy(this.place, this));
                    if (e ) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    if (!this.isInput) {
                    }
                    var that = this;
                    $(document).on('mousedown', function(ev){
                        if ($(ev.target).closest('.datepicker').length == 0) {
                            that.hide();
                        }
                    });
                    this.element.trigger({
                        type: 'show',
                        date: this.date
                    });
                },
                
                hide: function(){
                    this.picker.hide();
                    $(window).off('resize', this.place);
                    this.viewMode = this.startViewMode;
                    this.showMode();
                    if (!this.isInput) {
                        $(document).off('mousedown', this.hide);
                    }
                    //this.set();
                    this.element.trigger({
                        type: 'hide',
                        date: this.date
                    });
                },
                
                set: function() {
                    var formated = DPGlobal.formatDate(this.date, this.format);
                    if (!this.isInput) {
                        if (this.component){
                            this.element.find('input').prop('value', formated);
                        }
                        this.element.data('date', formated);
                    } else {
                        this.element.prop('value', formated);
                    }
                },
                
                setValue: function(newDate) {
                    if (typeof newDate === 'string') {
                        this.date = DPGlobal.parseDate(newDate, this.format);
                    } else {
                        this.date = new Date(newDate);
                    }
                    this.set();
                    this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
                    this.fill();
                },
                
                place: function(){
                    var offset = this.component ? this.component.offset() : this.element.offset();
                    this.picker.css({
                        top: offset.top + this.height,
                        left: offset.left
                    });
                },
                
                update: function(newDate){
                    this.date = DPGlobal.parseDate(
                        typeof newDate === 'string' ? newDate : (this.isInput ? this.element.prop('value') : this.element.data('date')),
                        this.format
                    );
                    this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
                    this.fill();
                },
                
                fillDow: function(){
                    var dowCnt = this.weekStart;
                    var html = '<tr>';
                    while (dowCnt < this.weekStart + 7) {
                        html += '<th class="dow">'+DPGlobal.dates.daysMin[(dowCnt++)%7]+'</th>';
                    }
                    html += '</tr>';
                    this.picker.find('.datepicker-days thead').append(html);
                },
                
                fillMonths: function(){
                    var html = '';
                    var i = 0
                    while (i < 12) {
                        html += '<span class="month">'+DPGlobal.dates.monthsShort[i++]+'</span>';
                    }
                    this.picker.find('.datepicker-months td').append(html);
                },
                
                fill: function() {
                    var d = new Date(this.viewDate),
                        year = d.getFullYear(),
                        month = d.getMonth(),
                        currentDate = this.date.valueOf();
                    this.picker.find('.datepicker-days th:eq(1)')
                                .text(DPGlobal.dates.months[month]+' '+year);
                    var prevMonth = new Date(year, month-1, 28,0,0,0,0),
                        day = DPGlobal.getDaysInMonth(prevMonth.getFullYear(), prevMonth.getMonth());
                    prevMonth.setDate(day);
                    prevMonth.setDate(day - (prevMonth.getDay() - this.weekStart + 7)%7);
                    var nextMonth = new Date(prevMonth);
                    nextMonth.setDate(nextMonth.getDate() + 42);
                    nextMonth = nextMonth.valueOf();
                    var html = [];
                    var clsName,
                        prevY,
                        prevM;
                    while(prevMonth.valueOf() < nextMonth) {
                        if (prevMonth.getDay() === this.weekStart) {
                            html.push('<tr>');
                        }
                        clsName = this.onRender(prevMonth);
                        prevY = prevMonth.getFullYear();
                        prevM = prevMonth.getMonth();
                        if ((prevM < month &&  prevY === year) ||  prevY < year) {
                            clsName += ' old';
                        } else if ((prevM > month && prevY === year) || prevY > year) {
                            clsName += ' new';
                        }
                        if (prevMonth.valueOf() === currentDate) {
                            clsName += ' active';
                        }
                        html.push('<td class="day '+clsName+'">'+prevMonth.getDate() + '</td>');
                        if (prevMonth.getDay() === this.weekEnd) {
                            html.push('</tr>');
                        }
                        prevMonth.setDate(prevMonth.getDate()+1);
                    }
                    this.picker.find('.datepicker-days tbody').empty().append(html.join(''));
                    var currentYear = this.date.getFullYear();
                    
                    var months = this.picker.find('.datepicker-months')
                                .find('th:eq(1)')
                                    .text(year)
                                    .end()
                                .find('span').removeClass('active');
                    if (currentYear === year) {
                        months.eq(this.date.getMonth()).addClass('active');
                    }
                    
                    html = '';
                    year = parseInt(year/10, 10) * 10;
                    var yearCont = this.picker.find('.datepicker-years')
                                        .find('th:eq(1)')
                                            .text(year + '-' + (year + 9))
                                            .end()
                                        .find('td');
                    year -= 1;
                    for (var i = -1; i < 11; i++) {
                        html += '<span class="year'+(i === -1 || i === 10 ? ' old' : '')+(currentYear === year ? ' active' : '')+'">'+year+'</span>';
                        year += 1;
                    }
                    yearCont.html(html);
                },
                
                click: function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    var target = $(e.target).closest('span, td, th');
                    if (target.length === 1) {
                        switch(target[0].nodeName.toLowerCase()) {
                            case 'th':
                                switch(target[0].className) {
                                    case 'switch':
                                        this.showMode(1);
                                        break;
                                    case 'prev':
                                    case 'next':
                                        this.viewDate['set'+DPGlobal.modes[this.viewMode].navFnc].call(
                                            this.viewDate,
                                            this.viewDate['get'+DPGlobal.modes[this.viewMode].navFnc].call(this.viewDate) + 
                                            DPGlobal.modes[this.viewMode].navStep * (target[0].className === 'prev' ? -1 : 1)
                                        );
                                        this.fill();
                                        this.set();
                                        break;
                                }
                                break;
                            case 'span':
                                if (target.is('.month')) {
                                    var month = target.parent().find('span').index(target);
                                    this.viewDate.setMonth(month);
                                } else {
                                    var year = parseInt(target.text(), 10)||0;
                                    this.viewDate.setFullYear(year);
                                }
                                if (this.viewMode !== 0) {
                                    this.date = new Date(this.viewDate);
                                    this.element.trigger({
                                        type: 'changeDate',
                                        date: this.date,
                                        viewMode: DPGlobal.modes[this.viewMode].clsName
                                    });
                                }
                                this.showMode(-1);
                                this.fill();
                                this.set();
                                break;
                            case 'td':
                                if (target.is('.day') && !target.is('.disabled')){
                                    var day = parseInt(target.text(), 10)||1;
                                    var month = this.viewDate.getMonth();
                                    if (target.is('.old')) {
                                        month -= 1;
                                    } else if (target.is('.new')) {
                                        month += 1;
                                    }
                                    var year = this.viewDate.getFullYear();
                                    this.date = new Date(year, month, day,0,0,0,0);
                                    this.viewDate = new Date(year, month, Math.min(28, day),0,0,0,0);
                                    this.fill();
                                    this.set();
                                    this.element.trigger({
                                        type: 'changeDate',
                                        date: this.date,
                                        viewMode: DPGlobal.modes[this.viewMode].clsName
                                    });
                                }
                                break;
                        }
                    }
                },
                
                mousedown: function(e){
                    e.stopPropagation();
                    e.preventDefault();
                },
                
                showMode: function(dir) {
                    if (dir) {
                        this.viewMode = Math.max(this.minViewMode, Math.min(2, this.viewMode + dir));
                    }
                    this.picker.find('>div').hide().filter('.datepicker-'+DPGlobal.modes[this.viewMode].clsName).show();
                }
            };
            
            $.fn.datepicker = function ( option, val ) {
                return this.each(function () {
                    var $this = $(this),
                        data = $this.data('datepicker'),
                        options = typeof option === 'object' && option;
                    if (!data) {
                        $this.data('datepicker', (data = new Datepicker(this, $.extend({}, $.fn.datepicker.defaults,options))));
                    }
                    if (typeof option === 'string') data[option](val);
                });
            };

            $.fn.datepicker.defaults = {
                onRender: function(date) {
                    return '';
                }
            };
            $.fn.datepicker.Constructor = Datepicker;
            
            var DPGlobal = {
                modes: [
                    {
                        clsName: 'days',
                        navFnc: 'Month',
                        navStep: 1
                    },
                    {
                        clsName: 'months',
                        navFnc: 'FullYear',
                        navStep: 1
                    },
                    {
                        clsName: 'years',
                        navFnc: 'FullYear',
                        navStep: 10
                }],
                dates:{
                    days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                    daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                    daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
                    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
                },
                isLeapYear: function (year) {
                    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))
                },
                getDaysInMonth: function (year, month) {
                    return [31, (DPGlobal.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
                },
                parseFormat: function(format){
                    var separator = format.match(/[.\/\-\s].*?/),
                        parts = format.split(/\W+/);
                    if (!separator || !parts || parts.length === 0){
                        throw new Error("Invalid date format.");
                    }
                    return {separator: separator, parts: parts};
                },
                parseDate: function(date, format) {
                    var parts = date.split(format.separator),
                        date = new Date(),
                        val;
                    date.setHours(0);
                    date.setMinutes(0);
                    date.setSeconds(0);
                    date.setMilliseconds(0);
                    if (parts.length === format.parts.length) {
                        var year = date.getFullYear(), day = date.getDate(), month = date.getMonth();
                        for (var i=0, cnt = format.parts.length; i < cnt; i++) {
                            val = parseInt(parts[i], 10)||1;
                            switch(format.parts[i]) {
                                case 'dd':
                                case 'd':
                                    day = val;
                                    date.setDate(val);
                                    break;
                                case 'mm':
                                case 'm':
                                    month = val - 1;
                                    date.setMonth(val - 1);
                                    break;
                                case 'yy':
                                    year = 2000 + val;
                                    date.setFullYear(2000 + val);
                                    break;
                                case 'yyyy':
                                    year = val;
                                    date.setFullYear(val);
                                    break;
                            }
                        }
                        date = new Date(year, month, day, 0 ,0 ,0);
                    }
                    return date;
                },
                formatDate: function(date, format){
                    var val = {
                        d: date.getDate(),
                        m: date.getMonth() + 1,
                        yy: date.getFullYear().toString().substring(2),
                        yyyy: date.getFullYear()
                    };
                    val.dd = (val.d < 10 ? '0' : '') + val.d;
                    val.mm = (val.m < 10 ? '0' : '') + val.m;
                    var date = [];
                    for (var i=0, cnt = format.parts.length; i < cnt; i++) {
                        date.push(val[format.parts[i]]);
                    }
                    return date.join(format.separator);
                },
                headTemplate: '<thead>'+
                                    '<tr>'+
                                        '<th class="prev">&lsaquo;</th>'+
                                        '<th colspan="5" class="switch"></th>'+
                                        '<th class="next">&rsaquo;</th>'+
                                    '</tr>'+
                                '</thead>',
                contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>'
            };
            DPGlobal.template = '<div class="datepicker dropdown-menu">'+
                                    '<div class="datepicker-days">'+
                                        '<table class=" table-condensed">'+
                                            DPGlobal.headTemplate+
                                            '<tbody></tbody>'+
                                        '</table>'+
                                    '</div>'+
                                    '<div class="datepicker-months">'+
                                        '<table class="table-condensed">'+
                                            DPGlobal.headTemplate+
                                            DPGlobal.contTemplate+
                                        '</table>'+
                                    '</div>'+
                                    '<div class="datepicker-years">'+
                                        '<table class="table-condensed">'+
                                            DPGlobal.headTemplate+
                                            DPGlobal.contTemplate+
                                        '</table>'+
                                    '</div>'+
                                '</div>';

        }( window.jQuery );
    </script>
@endpush


@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.add_lot').click(function(e){
                let item = $(this).parent().find('.product_lot_list');
                let id=item.data("id");
                let invoice_val = item.parent().find('.invoice_qty').val();
                let qty=parseInt(invoice_val) - Array.from(item.find('.qty')).reduce((total, val)=>parseInt(val.value)+total,0);
                item.find('.error').remove();
                if(!Array.from(item.find('.qty')).some(function(val){return val.value=='0'})){
                    if(qty>0){
                        let count=item.children().length;
                        let date = (new Date().getMonth()+1+'').padStart(2, '0')+'/'+(new Date().getFullYear());
                        var component = {
                            template: `<div class="products_lot">
                                        <span class="product_close fa fa-times"></span>
                                        <div class="input-group">
                                            <label for="lot[${id}][${count}]">Lot number:</label>
                                            <input type="text" class="control" id="lot[${id}][${count}]" name="lot[${id}][${count}]"/>
                                        </div>

                                        <div class="input-group">
                                            <label for="qty[${id}][${count}]">Quantity:</label>
                                            <input type="number" class="control qty" id="qty[${id}][${count}]" name="qty[${id}][${count}]" min=1 max="${qty}" value="${qty}">
                                        </div>

                                        <div class="input-append date date-group input-group" id="dpMonths" data-date="${date}" data-date-format="mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                            <label for="expiry[${id}][${count}]">Expiry date:</label>
                                            <input name="expiry[${id}][${count}]" id="expiry[${id}][${count}]" class="span2 add-on control" size="16" type="text" value="${date}" readonly>
                                        </div>
                                    </div>`
                        };

                        Vue.component('my-component', component);
                        var myComponent = Vue.extend(component);
                        var component = new myComponent().$mount();

                        item.append(component.$el);
                    }else{
                        item.append('<span class="error">Item quantity exceeded</span>');
                    }
                }else{
                    item.append('<span class="error">Quantity can\'t be zero</span>');
                }
                e.preventDefault();
            });

            $('.product_lot_list').on('click', '.product_close', function(e) {
                $(this).parent().parent().parent().find('.error').remove();
                $(this).parent().remove();
                e.preventDefault();
            });

            $('.product_lot_list').on('click', '#dpMonths', function(e) {
                $(this).datepicker();
                e.preventDefault();
            });

            $('.product_lot_list').on('input', 'input', function(e) {
                $(this).next('span').remove();
            });
            let invalid = false;
            $('form').submit(function(event){
                invalid = false;
                $('.error').remove();

                $('.control-error').remove();

                $('.products_lot').each(function(){
                    let invoice_qty = $(this).parent().parent().find('.invoice_qty').val();
                    let qty=parseInt(invoice_qty) - Array.from($(this).parent().find('.qty')).reduce((total, val)=>parseInt(val.value)+total,0);
                    if(qty<0){
                        $(this).parent().find('.error').remove();
                        $(this).parent().append('<span class="error">Item quantity exceeded</span>');
                        invalid = true;
                    }
                });

                $('.product_lot_list input').each(function(){
                    if($(this).val().length<=0){
                        $(this).after('<span class="control-error">This field is required.</span>');
                        invalid = true;
                        console.log(invalid);
                    }
                });
                if(invalid) event.preventDefault();
            });
        });
    </script>
@endpush


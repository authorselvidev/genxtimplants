@extends('admin::layouts.content')

@section('page_title', 'Dealer Report')

@section('content')
    <div class="content">
        <div class="page-header page-section">
            <div class="page-title">
                <h1>Dealer Report</h1>
            </div>
            <div class="page-action">
            </div>
        </div>

        <div class="report_form block-container col-md-12">
            <form method="POST" action="{{route('admin.reports.commissionexport')}}" id="filterForm">
                @csrf
                <div class="col-md-12 row">
                    <div class="col-md-4 ">
                        <label for="dealer">Dealer's:</label>
                        <select name="dealers" id="dealer" class="form-control" placeholder="Select a Dealer...">
                            @foreach($dealers as $dealer)
                                <option value="{{$dealer->id}}">{{$dealer->id}} - {{$dealer->first_name}} {{$dealer->last_name}}</option>
                            @endforeach                            
                        </select>
                    </div>
                </div>

                <div class="report_action col-md-12">
                    {{-- <input type="submit" id="sub_btn" class="btn btn-sm btn-primary sum_bt" name="report_form" value="Submit"/> --}}
                    <input type="submit" class="btn btn-sm btn-primary sum_bt" name="report_form" value="View"/>
                </div>
            </form>
        </div>
        <div class="block-container col-md-12">
            <div class="btn-wrapper">
                <button class="btn btn-sm btn-primary down_btn">Download (CSV)</button>
                <button class="btn btn-sm btn-primary down_xls">Download (XLS)</button>
            </div>
            <div class="loader"></div>
            <div class="sheet-container">
                
            </div>
        </div>
    </div>
</div>
@stop


@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/fSelect.css')}}">
    <script type="text/javascript" src="{{asset('themes/default/assets/js/fSelect.js')}}"></script>
    <script type="text/javascript" src="{{asset('themes/default/assets/js/table2excel.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            var curXPos = 0, curYPos = 0, curDown = false;

            $('.sheet-container').mousedown(function(m){
                $(this).css('cursor', 'grabbing');
                curDown = true;
                curYPos = m.pageY;
                curXPos = m.pageX;
            });

            $('.sheet-container').mouseup(function(){
                $(this).css('cursor', 'grab');
                curDown = false;
            });

            $('.sheet-container').mousemove(function(m){
                if(curDown === true){
                    $('.sheet-container').scrollTop($('.sheet-container').scrollTop() + (curYPos - m.pageY)); 
                    $('.sheet-container').scrollLeft($('.sheet-container').scrollLeft() + (curXPos - m.pageX));
                }
            });

            const globalSelect = {
                numDisplayed: 3,
                overflowText: '{n} selected',
                noResultsText: 'No results found',
                searchText: 'Search',
                showSearch: true,
                selectAll: false,
            }

            $('select#dealer').fSelect(Object.assign({
                placeholder: "Select Dealer's"
            }, globalSelect));
          

            var jsonReport = '';

            $('.block-container').on('click', '.down_btn', function(){
                var csvData = jsonReport.csv.replace(/(<([^>]+)>)/ig, '');
                csvData = csvData.replaceAll('NA', '0');
                var ele =$(`<a href="${'data:text/csv;charset=utf-8,'+encodeURI(csvData)}" target="_blank" download="${'Dealer_Report.csv'}"></a>`)[0].click();
            });


            $('.block-container').on('click', '.down_xls', function(){
                $(".sheet-container table").table2excel({
                    filename: "Dealer_Report.xls"
                });
            });


            $('#filterForm').on('submit', function(e){
                var validate = false;
                $('.err').remove();
                const elem = [$('#dealer')];

                elem.forEach(function(ele){
                    if(ele.val().length<=0 && !ele.is('[disabled]')){
                        const err = $(`<span class="err">This field is required!</span>`);
                        $(ele).after(err);
                        validate=true;
                    }
                });

                if(!validate){
                    $('.btn-wrapper').hide();
                    $('.sheet-container').empty();
                    $.ajax({
                            method: "POST",
                            url: "{{ route('admin.reports.userexport') }}",
                            async:true,
                            beforeSend:function() {
                                $(".loader").show();
                                $('.btn-wrapper').hide();
                            },
                            data:{
                                _token: '{!! csrf_token() !!}',
                                dealers: $('#dealer').val()
                            },
                            success:function(tableData){
                                    console.log(tableData);
                                    $('.btn-wrapper').css('display', 'flex');
                                    jsonReport = tableData;
                                    $('.sheet-container').append(`<table></table>`);
                                    var order_table = tableData.csv.split(/\r?\n|\r/);
                                    var table = $('.sheet-container table');

                                    order_table.forEach(function(tableRow){
                                        let row = $('<tr></tr>');
                                        tableRow.split(',').forEach(function(cellData){
                                            if(cellData.includes('<th>')){
                                                row.append($(cellData));
                                            }else{
                                                if(cellData.includes('skip')){
                                                    row.append($(`<td style="border:none;text-align:left">${cellData}</td>`));
                                                }
                                                else if(cellData.includes('<h')){
                                                    row.append($(`<td colspan="3" style="border:none;text-align:left">${cellData}</td>`));
                                                }else{
                                                    row.append($(`<td>${cellData}</td>`));
                                                }
                                            }
                                        });
                                        table.append(row);
                                    });
                                    $(".loader").hide();
                            },
                            error:function(msg){
                                console.log(msg);
                                $('.sheet-container').empty().append(`<span class="err" style="text-align:center">Something went wrong. Please try again</span>`);
                                $(".loader").hide();
                            }
                        });
                }
                e.preventDefault();
            });
        });
    </script>
@endpush

@push('scripts')
    <style type="text/css">
        .block-container {
            display: flex;
            flex-direction: column;
            padding: 15px 5px;
            border-radius: 3px;
            border: 1px solid rgba(0,0,0,.2);
            margin-bottom:15px;
        }

        .report_form {
            padding-left: 0px;
            padding-right: 0px;
        }

        .report_action {
            padding: 10px 10px 0;
            display: flex;
            justify-content: flex-end;
            outline: none !important;
            margin-top: 20px;
        }

        .report_action>* {
            margin: 0 5px;
            border:none;
        }

        .date input {
            padding: 8px;
            background-color: #FFF;
            border: 1px solid #ddd;
            border-radius: 5px;
            width: 100%;
        }

        label.required::after {
            content: "*";
            color: #fc6868;
            font-weight: 700;
            display: inline-block;
        }

        label[for] {
            margin-top:8px;
        }

        .btn-wrapper{
            margin:10px 30px 10px auto;
            display: none;
            justify-content: space-between;
        }

        .down_btn, .down_xls{
            margin: 0 10px;
        }

        .row {
            margin:10px auto;
        }

        .sheet-container{
            overflow: auto;
            cursor: grab;
            padding:10px 30px;
        }

        .err{
            color: #BE3737;
            display: block;
            background-color:rgba(0,0,0,0) !important;
            padding: 5px;
        }

        table{
            margin: 10px auto;
            border-collapse: collapse;
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none; /* Safari */
            -khtml-user-select: none; /* Konqueror HTML */
            -moz-user-select: none; /* Old versions of Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none;
        }

        table td, table th{
            border:1px solid #AAA;
            padding: 10px 10px 5px;
            text-align: center;
            font-weight: normal;
            white-space:nowrap;
        }

        table th{
            background-color: #22A3DA !important;
            color:#FFF !important;
        }

        .loader {
            border: 5px solid #f3f3f3;
            border-radius: 50%;
            border-top: 5px solid #3498db;
            width: 30px;
            display:none;
            margin:0 auto;
            height: 30px;
            -webkit-animation: spin .7s linear infinite;
            animation: spin .7s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endpush
@extends('admin::layouts.content')

@section('page_title')
    Orders Reports - GenXT
@stop

@section('content')
    <div class="content">
        <div class="page-header page-section">
            <div class="page-title">
                <h1>Orders Reports</h1>
            </div>
            <div class="page-action">
            </div>
        </div>

        <div class="report_form container">
            <form method="POST" action="{{route('admin.reports.export')}}">
                @csrf
                <div>
                    <div style="margin-bottom: 20px;">
                        <div class="control-group date">
                            <date>
                                <label for="start_date">From:</label>
                                <input type="text" class="control" id="start_date" name="start_date" value="{{date('2020-01-01')}}"/>
                            </date>
                        </div>
                        <div class="control-group date">
                            <date>
                                <label for="end_date">To:</label>
                                <input type="text" class="control" id="end_date" name="end_date" value="{{date('Y-m-d')}}"/>
                            </date>
                        </div>
                    </div>
                    <div>
                        <span style="margin-right: 10px">
                            <label for="doctor_csv">Doctor</label>
                            <input type="checkbox" name="role_doctor" value="1" id="doctor_csv" checked>
                        </span>
                        <span >
                            <label for="dealer_csv">Dealer</label>
                            <input type="checkbox" name="role_dealer" value="1" id="dealer_csv" checked>
                        </span>
                    </div>

                </div>
                <div class="report_action">
                    <input type="submit" id="sub_btn" class="btn btn-sm btn-primary sum_bt" name="report_form" value="Submit"/>
                    <input type="submit" class="btn btn-sm btn-primary sum_bt" name="report_form" value="Export (CSV)"/>
                </div>
            </form>
        </div>
        <div class="page-content" style="overflow-x: auto;">
           @inject('reports','Webkul\Admin\DataGrids\ReportsDataGrid')
            {!! $reports->render() !!}
        </div>
    </div>
    </div>
@stop


@push('scripts')
    <script>
        $(document).ready(function(){
            const urlParams = new URLSearchParams(window.location.search);
            if(urlParams.get('order_date[gte]')){
                $('#start_date').val(urlParams.get('order_date[gte]'));
            }
            if(urlParams.get('order_date[lte]')){
                $('#end_date').val(urlParams.get('order_date[lte]'));
            }
            if(urlParams.get('role[eq]')){
                let role = urlParams.get('role[eq]');
                if(role=='3'){
                    $("#doctor_csv").prop('checked', true);
                    $("#dealer_csv").removeAttr('checked');
                }
                if(role=='2'){
                    $("#dealer_csv").prop('checked', true);
                    $("#doctor_csv").removeAttr('checked');
                }
            }

            function checkCheckBox(){
                if($("#doctor_csv").prop('checked') == false && $("#dealer_csv").prop('checked') == false){
                    $('.sum_bt').prop('disabled', true);
                }else{
                    $('.sum_bt').prop('disabled', false);
                }
            }
            
            $("#doctor_csv").change(checkCheckBox);
            $("#dealer_csv").change(checkCheckBox);

            $("#sub_btn").click(function(e){
                /*order_date[gte]=2020-12-29 & order_date[lte]=2021-01-04 &role[eq]=2*/
                let filter = {};
                if($("#doctor_csv").prop('checked') == true || $("#dealer_csv").prop('checked') == true){
                    if($("#doctor_csv").prop('checked') && !$("#dealer_csv").prop('checked')) {
                        filter['role[eq]'] = 3;
                    }

                    if($("#dealer_csv").prop('checked') && !$("#doctor_csv").prop('checked')){
                        filter['role[eq]'] = 2;
                    }
                }

                filter['order_date[gte]'] = $('#start_date').val();
                filter['order_date[lte]'] = $('#end_date').val();

                let string = "";
                for(let key in filter){
                    string+= key+'='+filter[key]+'&'
                }
                string = string.substr(0, string.length-1);
                window.location.href = "?"+string;
                e.preventDefault();
            });

            $('.filter-row-two').attr('style', 'display:none !important;');
        });
    </script>
@endpush

@push('scripts')
    <style type="text/css">
        .report_form{
            max-width: 350px;
            width: 100%;
            margin-bottom: 20px;
            font-size: 15px;
        }

        .report_action{
            display: flex;
            width: 100%;
            margin-top: 10px;
        }

        .report_action *{
            outline: none !important;
            margin-right:10px;

        }

        .control-group{
            margin-bottom: 10px;
        }
    </style>
@endpush
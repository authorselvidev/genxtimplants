@extends('admin::layouts.content')

@section('page_title')
    Orders Report - GenXT
@stop

@section('content')
    <div class="content">
        <div class="page-header page-section">
            <div class="page-title">
                <h1>Orders Report</h1>
            </div>
            <div class="page-action">
            </div>
        </div>

        <div class="report_form block-container col-md-12">
            <form method="POST" action="{{-- {{route('admin.reports.orderexport')}} --}}" id="filterForm">
                @csrf
                <div class="col-md-12 row">
                    <div class="col-md-4 ">
                        <label for="role" class="required">User Role:</label>
                        <select name="role[]" id="role" class="form-control" multiple="multiple">
                            <option value="2">Dealer</option>
                            <option value="3">Doctor</option>
                        </select>
                    </div>
                    <div class="col-md-4 ">
                        <label for="dealer">Dealer's:</label>
                        <select name="dealers[]" id="dealer" class="form-control" multiple="multiple">
                            <?php
                                $dealers = \DB::table('users')
                                        ->where('is_deleted', 0)
                                        ->where('role_id', 2)
                                        ->where('is_verified', 1)
                                        ->where('is_approved', 1)
                                        ->get();
                                ?>
                                @foreach($dealers as $dealer)
                                    <option value="{{$dealer->id}}">{{$dealer->id}} - {{$dealer->first_name}} {{$dealer->last_name}}</option>
                                @endforeach
                            
                        </select>
                    </div>
                    <div class="col-md-4 ">
                        <label for="doctor">Doctor's:</label>
                        <select name="doctor[]" id="doctor" class="form-control" multiple="multiple" disabled>
                                <?php
                                    $doctors = \DB::table('users')
                                        ->where('is_deleted',0)
                                        ->where('is_verified',1)
                                        ->where('is_approved',1)
                                        ->where('role_id',3)->get();
                                ?>
                                @foreach($doctors as $doctor)
                                    <option value="{{$doctor->id}}" data-dealer="{{$doctor->dealer_id}}">{{$doctor->id}} - {{$doctor->first_name}} {{$doctor->last_name}}</option>
                                @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="col-md-3 ">
                        <label for="discount">Discount's:</label>
                        <select name="discount[]" id="discount" class="form-control" multiple="multiple">
                            <option value="1">No Discount</option>
                            <option value="2">Membership</option>
                            <option value="3">Promo Code</option>
                            <option value="4">Promotions</option>
                            {{-- <option value="5">Special Discount</option> --}}
                        </select>
                    </div>
                    <div class="col-md-3 ">
                        <label for="coupon_code">Discount Code:</label>
                        <select name="coupon[]" id="coupon_code" class="form-control" multiple="multiple">
                            <optgroup label="Membership">
                                <?php
                                    $mdiscounts = \DB::table('discounts')->where('is_deleted',0)->where('discount_type',2)->get();
                                ?>
                                @foreach($mdiscounts as $mship)
                                    <option data-distype="2" value="{{$mship->id}}">{{$mship->coupon_code}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Promo Code">
                                <?php
                                    $pcdiscounts = \DB::table('discounts')->where('is_deleted',0)->where('discount_type',3)->get();
                                ?>
                                @foreach($pcdiscounts as $pcode)
                                    <option data-distype="3" value="{{$pcode->id}}">{{$pcode->coupon_code}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Promotions">
                                <?php
                                    $pdiscounts = \DB::table('discounts')->where('is_deleted',0)->where('discount_type',4)->get();
                                ?>
                                @foreach($pdiscounts as $promo)
                                    <option  data-distype="4" value="{{$promo->id}}">{{$promo->coupon_code}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-md-3 date ">
                        <label for="start_date" class="required">From:</label><br>
                        <date>
                            <input type="text" class="control" id="start_date" name="start_date" value="{{date('2020-01-01')}}"/>
                        </date>
                    </div>
                    <div class="col-md-3 date ">
                        <label for="end_date" class="required">To:</label><br>
                        <date>
                            <input type="text" class="control" id="end_date" name="end_date" value="{{date('Y-m-d')}}"/>
                        </date>
                    </div>
                </div>
                {{--
                    <div class="col-md-12 row">
                        <div class="col-md-3">
                            <label for="inverval_type" class="required">Interval:</label><br>
                            <select name="interval_type" id="interval_type" class="form-control">
                                <option value="1" selected>By Date</option>
                                <option value="2">By Month</option>
                            </select>
                        </div> 
                    </div>
                --}}
                <div class="col-md-12 row" style="display: flex; flex-direction: column; align-items: flex-start;padding:0 30px;">
                    <label for="products" class="required">Categories and Products:</label>
                    <input type="hidden" name="productsSku[]" id="products">
                    <div id="productTree"></div>
                </div>

                <div class="report_action col-md-12">
                    {{-- <input type="submit" id="sub_btn" class="btn btn-sm btn-primary sum_bt" name="report_form" value="Submit"/> --}}
                    <input type="submit" class="btn btn-sm btn-primary sum_bt" name="report_form" value="View"/>
                </div>
            </form>
        </div>
        <div class="block-container col-md-12">
            <div class="btn-wrapper">
                <button class="btn btn-sm btn-primary down_btn">Download (CSV)</button>
                <button class="btn btn-sm btn-primary down_xls">Download (XLS)</button>
            </div>
            <div class="loader"></div>
            <div class="sheet-container">
                
            </div>
        </div>
    </div>
</div>
@stop


@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/fSelect.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/simTree.css')}}">
    <script type="text/javascript" src="{{asset('themes/default/assets/js/fSelect.js')}}"></script>

    <script type="text/javascript" src="{{asset('themes/default/assets/js/simTree.js')}}"></script>
    <script type="text/javascript" src="{{asset('themes/default/assets/js/table2excel.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            var curXPos = 0, curYPos = 0, curDown = false;

            $('.sheet-container').mousedown(function(m){
                $(this).css('cursor', 'grabbing');
                curDown = true;
                curYPos = m.pageY;
                curXPos = m.pageX;
            });

            $('.sheet-container').mouseup(function(){
                $(this).css('cursor', 'grab');
                curDown = false;
            });

            $('.sheet-container').mousemove(function(m){
                if(curDown === true){
                    $('.sheet-container').scrollTop($('.sheet-container').scrollTop() + (curYPos - m.pageY)); 
                    $('.sheet-container').scrollLeft($('.sheet-container').scrollLeft() + (curXPos - m.pageX));
                }
            });

            $('.block-container').on('click', '.down_xls', function(){
                $(".sheet-container table").table2excel({
                    filename: "Orders_Reports.xls"
                });
            });

            const globalSelect = {
                numDisplayed: 3,
                overflowText: '{n} selected',
                noResultsText: 'No results found',
                searchText: 'Search',
                showSearch: true,
            }

            $('select[multiple]#role').fSelect(Object.assign({
                placeholder: "Select User's role",
                functionSelect : function(){
                    const option = $(this).data('value');
                    const selected = $(this).hasClass('selected');
                    let wrap;
                    let select;
                    if(option==2){
                        wrap = $('#dealer').closest('.fs-wrap');
                    }else if(option==3){
                        if(selected){
                            wrap =  $('#doctor').prop("disabled", true).closest('.fs-wrap');
                        }else{
                            wrap = $('#doctor').prop("disabled", false).closest('.fs-wrap');
                        }
                    }

                    if(selected){
                        wrap.find('.select_all').trigger('click').prop('checked',false);
                        if(option==3){
                            wrap.toggleClass('fs-disabled', true);
                        }
                        wrap.find('.fs-option').toggleClass('selected', false);
                        select= wrap.find('select').val([]);
                        wrap.prev('label').toggleClass('required', false);
                        select.fSelect('reloadDropdownLabel').change();
                    }else{
                        wrap.toggleClass('fs-disabled', false);
                        wrap.find('.select_all').trigger('click');
                        wrap.prev('label').toggleClass('required', true);
                    }

                    $(document).trigger('fs:changed', wrap);
                }
            }, globalSelect));

            $('select[multiple]#dealer').fSelect(Object.assign({
                placeholder: "Select Dealer's",
                functionSelect : function(){
                    const selected = !$(this).hasClass('selected');
                    if($('select[multiple]#role').val().includes("3")){
                        options =  $('#doctor').closest('.fs-wrap').find(`.fs-option[data-doctor="${$(this).data('value')}"]`);
                        options.each(function(){
                            let optSelect = $(this).hasClass('selected');
                            if((selected && !optSelect)|| (!selected && optSelect)) {
                                $(this).trigger('click');
                            }
                        });
                    }
                }
            }, globalSelect));

            $('select[multiple]#doctor').fSelect(Object.assign({
                placeholder: "Select Doctor's"
            }, globalSelect));

            $('select[multiple]#products').fSelect(Object.assign({
                placeholder: "Select Product's"
            }, globalSelect));

            $('select[multiple]#discount').fSelect(Object.assign({
                placeholder: "Select Discounts's",
                selectAll:false,
                functionSelect: function(){
                    const selected = !$(this).hasClass('selected');
                    if( $(this).data('value')==1 ){
                        if(selected){
                            let wrap = $(this).closest('.fs-wrap');
                            wrap.find('select').val([]);
                            wrap.find('.fs-option:not([data-value="1"])').toggleClass('selected', false);
                            wrap.find('.fs-option:not([data-value="1"])').toggleClass('disabled', true);

                            wrap = $('#coupon_code').prop("disabled", true).closest('.fs-wrap');
                            wrap.toggleClass('fs-disabled', true);
                            wrap.find('.fs-option').toggleClass('selected', false);
                            select= wrap.find('select').val([]);
                            wrap.prev('label').toggleClass('required', false);
                            select.fSelect('reloadDropdownLabel').change();
                            $(document).trigger('fs:changed', wrap);
                        }else{
                            let wrap = $(this).closest('.fs-wrap');
                            wrap.find('select').val([]);
                            wrap.find('.fs-option:not([data-value="1"])').toggleClass('disabled', false);

                            wrap = $('#coupon_code').prop("disabled", false).closest('.fs-wrap');
                            wrap.toggleClass('fs-disabled', false);
                            $(document).trigger('fs:changed', wrap);
                        }
                    }else{
                        options = $('#coupon_code').closest('.fs-wrap').find(`.fs-option[data-distype="${$(this).data('value')}"]`);
                        options.each(function(){
                            let optSelect = $(this).hasClass('selected');
                            if((selected && !optSelect)|| (!selected && optSelect)) {
                                $(this).trigger('click');
                            }
                        });
                    }
                }
            }, globalSelect));

            $('select[multiple]#coupon_code').fSelect(Object.assign({
                placeholder: "Select Discount Code's"
            }, globalSelect));

            let tree = simTree({
                el: '#productTree',
                data: {!! $items !!},
                check: true,
                linkParent: true,
                onChange: function(item){
                    $('input#products').val(item.map((ele)=>ele.sku).filter((val)=>val));
                }
            });

            $('select#interval_type').fSelect(Object.assign({
                placeholder: "Select intervel type",
                selectAll: false,
            }, globalSelect));


            var jsonReport = '';

            $('.block-container').on('click', '.down_btn', function(){
                var csvData= ',From,'+jsonReport.from+"\n";
                csvData+= 'To,'+jsonReport.to+"\n\n";
                csvData+= jsonReport.csv.replace(/(<([^>]+)>)/ig, '');
                csvData = csvData.replaceAll('NA', '0');
                var ele =$(`<a href="${'data:text/csv;charset=utf-8'+encodeURI(csvData)}" target="_blank" download="${'Reports_'+jsonReport.from+'-'+jsonReport.to+'.csv'}"></a>`)[0].click();
            });


            $('#filterForm').on('submit', function(e){
                var validate = false;
                $('.err').remove();
                const elem = [$('#role'),
                              $('#dealer'), 
                              $('#doctor'),  
                              $('#start_date'), 
                              $('#end_date'), 
                              $('#products')];

                elem.forEach(function(ele){
                    if(ele.val().length<=0 && !ele.is('[disabled]')){
                        if(ele.is('#dealer') && !$('#role').val().includes('2')){
                            return;
                        }
                        const err = $(`<span class="err">This field is required!</span>`);
                        if($(ele).prop('id') == 'products'){
                            $(ele).next('#productTree').after(err);
                        }else{
                            $(ele).after(err);
                        }
                        validate=true;
                    }
                });



                if(!validate){
                    $('.btn-wrapper').hide();
                    $('.sheet-container').empty();
                    $.ajax({
                            method: "POST",
                            url: "{{ route('admin.reports.orderexport') }}",
                            async:true,
                            beforeSend:function() {
                                $(".loader").show();
                            },
                            data:{
                                    _token: '{!! csrf_token() !!}',
                                    role: $('#role').val()?$('#role').val():[],
                                    dealers: $('#dealer').val()?$('#dealer').val():[],
                                    doctor: $('#doctor').val()?$('#doctor').val():[],
                                    discount: $('#discount').val()?$('#discount').val():[],
                                    coupon: $('#coupon_code').val()?$('#coupon_code').val():[],
                                    start_date: $('#start_date').val(),
                                    end_date: $('#end_date').val(),
                                    productsSku: $('#products').val()? $('#products').val().split(',') : [],
                                },
                            success:function(tableData){
                                    $('.btn-wrapper').css('display', 'flex');
                                    jsonReport = tableData;

                                    $('.sheet-container').append(`<table>
                                                    <tr>
                                                        <td style="border:none;text-align:left">From</td>
                                                        <td style="border:none;text-align:left" colspan="3">${tableData.from}</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border:none;text-align:left">To</td>
                                                        <td style="border:none;text-align:left" colspan="3">${tableData.to}</td>
                                                    </tr>
                                                </table>`);
                                    var order_table = tableData.csv.split(/\r?\n|\r/);
                                    var table = $('.sheet-container table');

                                    order_table.forEach(function(tableRow){
                                        let row = $('<tr></tr>');
                                        tableRow.split(',').forEach(function(cellData){
                                            if(cellData.includes('<th>')){
                                                row.append($(cellData));
                                            }else{
                                                if(cellData.includes('<h3>')){
                                                    row.append($(`<td colspan="3" style="border:none;text-align:left">${cellData}</td>`));
                                                }else{
                                                    row.append($(`<td>${cellData}</td>`));
                                                }
                                            }
                                        });
                                        table.append(row);
                                    });
                                    $(".loader").hide();
                            },
                            error:function(msg){
                                console.log(msg);
                                $('.sheet-container').empty().append(`<span class="err" style="text-align:center">Something went wrong. Please try again</span>`);
                                $(".loader").hide();
                            }
                        });
                }
                e.preventDefault();
            });
        });
    </script>
@endpush

@push('scripts')
    <style type="text/css">
        .block-container {
            display: flex;
            flex-direction: column;
            padding: 15px 5px;
            border-radius: 3px;
            border: 1px solid rgba(0,0,0,.2);
            margin-bottom:15px;
        }

        .report_form {
            padding-left: 0px;
            padding-right: 0px;
        }

        .report_action {
            padding: 10px 10px 0;
            display: flex;
            justify-content: flex-end;
            outline: none !important;
            margin-top: 20px;
        }

        .report_action>* {
            margin: 0 5px;
            border:none;
        }

        .date input {
            padding: 8px;
            background-color: #FFF;
            border: 1px solid #ddd;
            border-radius: 5px;
            width: 100%;
        }

        label.required::after {
            content: "*";
            color: #fc6868;
            font-weight: 700;
            display: inline-block;
        }

        label[for] {
            margin-top:8px;
        }

        .btn-wrapper{
            margin:10px 30px 10px auto;
            display: none;
            justify-content: space-between;
        }

        .down_btn, .down_xls{
            margin: 0 10px;
        }

        .row {
            margin:10px auto;
        }

        .sheet-container{
            overflow: auto;
            cursor: grab;
            padding:10px 30px;
        }

        .err{
            color: #BE3737;
            display: block;
            background-color:rgba(0,0,0,0) !important;
            padding: 5px;
        }

        table{
            margin: 10px auto;
            border-collapse: collapse;
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none; /* Safari */
            -khtml-user-select: none; /* Konqueror HTML */
            -moz-user-select: none; /* Old versions of Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none;
        }

        table td, table th{
            border:1px solid #AAA;
            padding: 10px 10px 5px;
            text-align: center;
            font-weight: normal;
            white-space:nowrap;
        }

        table th{
            background-color: #22A3DA !important;
            color:#FFF !important;
        }

        .loader {
            border: 5px solid #f3f3f3;
            border-radius: 50%;
            border-top: 5px solid #3498db;
            width: 30px;
            display:none;
            margin:0 auto;
            height: 30px;
            -webkit-animation: spin .7s linear infinite;
            animation: spin .7s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endpush

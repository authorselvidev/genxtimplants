@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.profile.index.title') }}
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">
<div class="account-content">

    @include('shop::customers.account.partials.sidemenu')

    <div class="account-layout">

        <div class="account-head">

            <span class="back-icon"><a href="{{ route('admin.account.index') }}"><i class="icon icon-menu-back"></i></a></span>

            <span class="account-heading">{{ __('shop::app.customer.account.profile.index.title') }}</span>

            <?php 
            $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
            if(empty($user))
                $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
     		$total_loyalty_points = DB::table('reward_points as rp')
                                        ->where('rp.points_credited',1)
                                        ->where('rp.points_expired',0)
                                        ->where('rp.user_id',auth()->guard('admin')->user()->id)
                                        ->sum('reward_points');
                $used_loyalty_points = DB::table('orders')
                                        ->where('customer_id',auth()->guard('admin')->user()->id)
                                        ->sum('reward_points_used');
                $available_points=0;
                if($total_loyalty_points > 0)                       
                    $available_points = abs($total_loyalty_points - $used_loyalty_points);

                $total_sales = DB::table('orders')->where('customer_id',$user->id)->where('status','completed')->sum('grand_total');
            ?>
            <span class="loyalty-points">
                <span class="account">Your Reward Points:</span> <span>{{$available_points}}</span>
                
            </span>

            <div class="horizontal-rule"></div>
        </div>

         {!! view_render_event('bagisto.shop.customers.account.profile.view.before', ['user' => $user]) !!}

        <div class="account-table-content">
            <span class="account-action"><a href="{{ route('admin.profile.edit') }}">{{ __('shop::app.customer.account.profile.index.edit') }}</a></span>
            <div class="profile_sec">
				<p><label>Full Name</label> {{ $user->first_name }} {{ $user->last_name }}</p>
				<p><label>{{ __('shop::app.customer.account.profile.email') }}</label> {{ $user->email }}</p>
				<p><label>Phone Number</label> {{ $user->phone_number }}</p>
			@if($user->role_id == 3)
				<p><label>{{ __('shop::app.customer.signup-form.clinicname') }}</label> {{ $user->clinic_name }}</p>
				<p><label>Clinic Registered Number</label> {{ $user->clinic_number }}</p>
				<p><label>Clinic Address</label> {{ $user->clinic_address }}</p>
			@endif
				<p><label>Country</label> India</p>
				<p><label>State</label> {{ Webkul\Customer\Models\States::GetStateName($user->state_id) }}</p>
				<p><label>City</label> {{ Webkul\Customer\Models\Cities::GetCityName($user->city_id) }}</p>
				<p><label>Pincode</label> {{ $user->pin_code }}</p>
                @if(!empty($user->invite_code))
				    <p><label>Invite code</label>
                        <span>{{ $user->invite_code }}</span>
                        <span class="fa fa-clone inv-copy" aria-hidden="true" data-clipboard-text="{{ $user->invite_code }}"></span></p>
                    <p>
                        <label>Invite URL</label>
                        <span class="i-url">{{ route('dealer.invite.code',$user->invite_code) }}</span>
                        <span class="fa fa-clone btn-copy" aria-hidden="true" data-clipboard-text="{{ route('dealer.invite.code',$user->invite_code) }}"></span>
                    </p>
                @endif
                <p><label>Group Name</label> {{ Webkul\Customer\Models\CustomerGroup::CustomerGroupName($user->customer_group_id) }}</p>
				<p><label>Group Points (Order Total)</label> {{ Webkul\Customer\Models\CustomerGroup::CustomerGroupAmountRange($user->customer_group_id) }}</p>
                <p><label>Total Sales</label> <i class="fa fa-inr"></i> {{ number_format($total_sales,2) }}</p>

                @if($user->role_id == 2)
                    @if(!empty($user->company_name))
                        <p><label>Company Name</label> {{ $user->company_name }}</p>
                    @endif

                    @if(!empty($user->gst_no))
                        <p><label>GST Number</label> {{ $user->gst_no }}</p>
                    @endif

                    @if(!empty($user->fda_licence_no))
                        <p><label>FDA Licence No</label> {{ $user->fda_licence_no }}</p>
                    @endif
                @endif
			</div>
        </div>

         {!! view_render_event('bagisto.shop.customers.account.profile.view.after', ['user' => $user]) !!}

    </div>
</div>
</div>
</div>
@endsection
@push('scripts')
   <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        })

        const afterCopy = function(e){
            var target = $(e.trigger);
            $(e.trigger).append($('<span class="copied-tip">Copied</span>').hide().fadeIn(100));
            setTimeout(function(){
                target.find('.copied-tip').fadeOut(200, function(){$(this).remove()});
            },2000);
        }
        var clipboard1 = new ClipboardJS('.btn-copy');
        var clipboard2 = new ClipboardJS('.inv-copy');
        clipboard1.on('success', afterCopy);
        clipboard2.on('success', afterCopy);

    </script>
    <style type="text/css">
        .btn-copy, .inv-copy{
            cursor: pointer;
            display: inline-block;
            margin-left: 5px;
            color: #0995CD;
            position: relative;
        }

        .copied-tip{
            position: absolute;
            font-size: 1.3rem;
            padding: 6px 9px;
            top: -29px;
            color: #EEE;
            left: 50%;
            transform: translateX(-50%);
            background-color: rgba(0, 0, 0,.7);
            font-family: arial, sans-serif;
            border-radius: 3px;
            z-index: 1000;
            box-shadow: 1.5px 1.5px 2px rgba(0, 0, 0, .3);
        }
    </style>
@endpush

<?php
    $status = array('0' => 'False', '1' => 'True');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta charset="utf-8">
    <title>GenXT</title>
<style type="text/css">
.clearfix:after {
    content: "";
    display: table;
    clear: both
}

a {
    color: #0087C3;
    text-decoration: none
}

body {
    position: relative;
    width: 100%;
    height: 29.7cm;
    margin: 0 auto;
    color: #222;
    background: #FFF;
    font-family: Arial, sans-serif;
    font-size: .8rem;
}

header {
    padding: 0 0px 20px;
    margin-bottom: 20px;
    position: relative;
    border-bottom: 1px solid #AAA
}

#logo {
    float: left;
    width: 55%;
    display:inline-block;
}

#logo img {
    padding: 20px 20px 0;
    height: 50px
}

.date_header{
    margin-top: 35px;
    display: inline-block;
    float: right;
}

.heading{
    text-align: center;
}

.failed_heading{
    position: absolute;
    margin-top: 30px;
    transform: translateX(-50%);
    font-weight: 400;
    font-size: 1.3rem;
    text-transform: uppercase;
}

h2 {
    font-size: 1.4em;
    margin: 0 0 5px
}

table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

table th,
table td {
    padding: 10px;
    text-align: center;
    border:1px solid #DFDFDF !important;
}

table th {
    background: #EEE;
    white-space: nowrap;
    font-weight: 400
}

table tbody tr:last-child td {
    border: none
}

table tfoot td {
    padding: 10px 20px;
    background: #FFF;
    border-bottom: none;
    font-size: 1.2em;
    white-space: nowrap;
    border-top: 1px solid #AAA
}

table tfoot tr:first-child td {
    border-top: none
}

table tfoot tr:last-child td {
    color: #0a97cf;
    font-size: 1.4em;
    border-bottom: 1px solid #0a97cf
}

table tfoot tr td:first-child {
    border: none
}

</style>
  </head>
  <body>
     <header class="clearfix">
      <div id="logo">
        <img src="https://genxtimplants.com/themes/default/assets/images/genxtimplants-logo.png">
      </div>
      <span class="failed_heading">Return Implants</span>
      <div>
          <span class="date_header">Date: {{date('d-m-Y', strtotime($failed_implant->created_at))}}</span>
      </div>
    </header>
    <main>
        <section>
            <h4 class="heading">Order Details</h4>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Order Type</th>
                    <th>User's Name</th>
                    <th>Email</th>
                    <th>Mobile Number</th>
                    <th>User Type</th>
                </tr>
                <tr>
                    <td>{{($failed_implant->order_type =='on')?'Online':'Offline'}}</td>
                    <td>{{$user->my_title.' '.$user->first_name.' '.$user->last_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td>{{array(
                            '1' => 'Admin',
                            '2' => 'Dealer',
                            '3' => 'Doctor',
                            '4' => 'Store Manager')[$user->role_id]}}</td>
                </tr>
            </table>

            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Return Implant status</th>
                    @if($failed_implant->order_type=="on")
                        <th>Order ID</th>
                    @else
                        <th>Order Number</th>
                    @endif
                        <th>Item Name</th>

                    <th>Quantity</th>

                    @if($failed_implant->order_type=="on")
                        <th>Lot Number:</th>
                    @endif
                </tr>
                <tr>
                    <td>{{$failed_implant->status}}</td>
                    @if($failed_implant->order_type=="on")
                            <td>{{ $order->id }}</td>
                            <td>{{ $order_items->name }}</td>
                    @else
                        <td>{{ $failed_implant->order_number }}</td>
                        <td>{{ $failed_implant->order_item }}</td>
                    @endif

                    <td>{{$failed_implant->qty}}</td>

                    @if($failed_implant->order_type=="on")
                        <td>
                            <?php
                                $lot = ($invoice_item_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_item_id->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get()):[];
                                    ?>

                                @if(count($lot)>0)
                                    @foreach($lot as $key => $lot_item)
                                        {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}} ,
                                        {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}} ,
                                        {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m',  strtotime($lot_item->lot_expiry)):'-'}}<br>
                                    @endforeach
                                @else
                                    -
                                @endif
                        </td>
                    @endif
                </tr>
            </table>

            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Replacement Product</th>
                    <th>Replacement Quandity</th>
                    <th>Product Code(or label)</th>
                    <th>LOT #</th>
                    <th>Comments</th>
                </tr>
                <tr>
                    <?php
                        $product = \DB::table('product_flat')->where('product_id', $failed_implant->replacement_product_id)->first();
                    ?>
                    
                    <td>{{ $product->name ?? '' }}</td>
                    <td>{{ ($failed_implant->order_type=="on")?$failed_implant->replace_qty:$failed_implant->replace_offline_qty }}</td>
                    <td>{{ $failed_implant->implant_code }}</td>
                    <td>{{ $failed_implant->lot }}</td>
                    <td>{{ $failed_implant->comments }}</td>
                </tr>
            </table>
        </section>
        @if($failed_implant->item_status == 0 && $failed_implant->is_lessper==1 )
         <section>
            <h4 class="heading">Failure Details</h4>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Bone Type</th>
                    <th>Flapless</th>
                    <th>Immediate Implant</th>
                    <th>Immediate Load</th>
                    <th>Implantation Date</th>
                    <th>Implant Removal Date</th>
                </tr>
                <tr>
                    <td>{{$failed_implant->bone_type}}</td>
                    <td>{{$status[$failed_implant->flapless]}}</td>
                    <td>{{$status[$failed_implant->immediate_implant]}}</td>
                    <td>{{$status[$failed_implant->immediate_load]}}</td>
                    <td>{{date('Y-m-d', strtotime($failed_implant->implant_date))}}</td>
                    <td>{{date('Y-m-d', strtotime($failed_implant->implant_removal_date))}}</td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Implant Removal Location</th>
                    <th>Other reason for Failure</th>
                </tr>
                <tr>
                    <td>{{$failed_implant->remove_location}}</td>
                    <td>{{$failed_implant->other_reason_failure}}</td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Reasons for Failure</th>
                </tr>
                <tr>
                    <td>{{$failed_implant->reason}}</td>
                </tr>
            </table>
        </section>
         <section>
            <h4 class="heading">History Details</h4>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Normal</th>
                    <th>Smoker</th>
                    <th>Hypertension</th>
                    <th>Cardiac Problems</th>
                </tr>
                <tr>
                    <td>{{$failed_implant->patient_age}}</td>
                    <td>{{$failed_implant->gender}}</td>
                    <td>{{$status[$failed_implant->normal_history]}}</td>
                    <td>{{$status[$failed_implant->smoker_history]}}</td>
                    <td>{{$status[$failed_implant->hypertension_history]}}</td>
                    <td>{{$status[$failed_implant->cardiac_problems_history]}}</td>
                </tr>
            </table>

            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Diabetes</th>
                    <th>Alcoholism</th>
                    <th>Trauma</th>
                    <th>Cancer</th>
                    <th>Others (Specify)</th>
                </tr>
                <tr>
                    <td>{{$status[$failed_implant->diabetes_history]}}</td>
                    <td>{{$status[$failed_implant->alcoholism_history]}}</td>
                    <td>{{$status[$failed_implant->trauma_history]}}</td>
                    <td>{{$status[$failed_implant->cancer_history]}}</td>
                    <td>{{$failed_implant->others}}</td>
                </tr>
            </table>
        </section>
        @endif
    </main>
  </body>
  </html>

@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.order.index.page-title') }}
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">
    <div class="account-content">
        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">

            <div class="account-head mb-10">
                <span class="back-icon"><a href="{{ route('admin.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
                <span class="account-heading">
                    {{ __('shop::app.customer.account.order.index.title') }}
                </span>

                <div class="horizontal-rule"></div>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.orders.list.before', ['orders' => $orders]) !!}

            <div class="account-items-list">

                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <th style="width: 120px;"> {{ __('shop::app.customer.account.order.index.order_id') }}</th>
                                <th> {{ __('shop::app.customer.account.order.index.date')  }} </th>
                                <th> {{ __('shop::app.customer.account.order.index.total') }} </th>
                                <th> {{ __('shop::app.customer.account.order.index.status')}} </th>
								<th> Action </th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($orders as $order)

                                <tr>
                                    <td data-value="{{  __('shop::app.customer.account.order.index.order_id') }}">
                                        <a href="{{ route('admin.orders.view', base64_encode($order->id)) }}">
                                            #{{ $order->id }}
                                        </a>
                                    </td>

                                    <td data-value="{{ __('shop::app.customer.account.order.index.date') }}">{{ core()->formatDate($order->created_at, 'd M Y') }}</td>

                                    <td data-value="{{ __('shop::app.customer.account.order.index.total') }}">
                                        {{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}
                                    </td>

                                    <td data-value="{{  __('shop::app.customer.account.order.index.status') }}">
                                        @if($order->status == 'processing')
                                            <span class="badge badge-md badge-success" style="background-color: #212427;">Processing</span>
                                        @elseif ($order->status == "shipped")
                                            <span class="badge badge-md badge-success" style="background-color: #2D7172;">Shipped</span>
                                        @elseif ($order->status == 'delivered')
                                            <span class="badge badge-md badge-success" style="background-color: #004177;">Delivered</span>
                                        @elseif ($order->status == 'completed')
                                            <span class="badge badge-md badge-success" style="background-color: #007704;">Completed</span>
                                        @elseif ($order->status == "canceled")
                                            <span class="badge badge-md badge-danger" style="background-color: #e40000;">Canceled</span>
                                        @elseif ($order->status == "closed")
                                            <span class="badge badge-md badge-info">Closed</span>
                                        @elseif ($order->status == "pending")
                                            <span class="badge badge-md badge-warning">Pending</span>
                                        @elseif ($order->status == "confirmed")
                                            <span class="badge badge-md badge-warning" style="background-color: #1395CD">Confirmed</span>
                                        @elseif ($order->status == "pending_payment")
                                            <span class="badge badge-md badge-warning">Pending Payment</span>
                                        @elseif ($order->status == "fraud")
                                            <span class="badge badge-md badge-danger">Fraud</span>
                                        @endif
                                    </td>
									
                                    <td data-value="Action">
                                        <a href="{{ route('admin.orders.view', base64_encode($order->id)) }}">
                                            View
                                        </a>
                                    </td>
								</tr>

                            @endforeach

                            @if (! $orders->count())
                                <tr>
                                    <td class="empty" colspan="4">{{ __('admin::app.common.no-result-found') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

                @if (!$orders->count())
                    <div class="responsive-empty">{{ __('admin::app.common.no-result-found') }}</div>
                @endif

            </div>

            {!! view_render_event('bagisto.shop.customers.account.orders.list.after', ['orders' => $orders]) !!}

        </div>

    </div>
</div>
</div>
@endsection

@push('scripts')
    <script>
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        })
    </script>
@endpush
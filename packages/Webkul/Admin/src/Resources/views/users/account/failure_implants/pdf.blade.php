<?php
    $status = array('0' => 'False', '1' => 'True');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta charset="utf-8">
    <title>GenXT</title>
<style type="text/css">
.clearfix:after {
    content: "";
    display: table;
    clear: both
}

a {
    color: #0087C3;
    text-decoration: none
}

body {
    position: relative;
    width: 100%;
    height: 29.7cm;
    margin: 0 auto;
    color: #222;
    background: #FFF;
    font-family: Arial, sans-serif;
    font-size: .8rem;
}

header {
    padding: 0 0px 20px;
    margin-bottom: 20px;
    position: relative;
    border-bottom: 1px solid #AAA
}

#logo {
    float: left;
    width: 55%;
    display:inline-block;
}

#logo img {
    padding: 20px 20px 0;
    height: 50px
}

.date_header{
    margin-top: 35px;
    display: inline-block;
    float: right;
}

.heading{
    text-align: center;
}

.failed_heading{
    position: absolute;
    margin-top: 30px;
    transform: translateX(-50%);
    font-weight: 400;
    font-size: 1.3rem;
    text-transform: uppercase;
}

h2 {
    font-size: 1.4em;
    margin: 0 0 5px
}

table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

table th,
table td {
    padding: 10px;
    text-align: center;
    border:1px solid #DFDFDF !important;
}

table th {
    background: #EEE;
    white-space: nowrap;
    font-weight: 400
}

table tbody tr:last-child td {
    border: none
}

table tfoot td {
    padding: 10px 20px;
    background: #FFF;
    border-bottom: none;
    font-size: 1.2em;
    white-space: nowrap;
    border-top: 1px solid #AAA
}

table tfoot tr:first-child td {
    border-top: none
}

table tfoot tr:last-child td {
    color: #0a97cf;
    font-size: 1.4em;
    border-bottom: 1px solid #0a97cf
}

table tfoot tr td:first-child {
    border: none
}

</style>
  </head>
  <body>
     <header class="clearfix">
      <div id="logo">
        <img src="https://genxtimplants.com/themes/default/assets/images/genxtimplants-logo.png">
      </div>
      <span class="failed_heading">Return Implants</span>
      <div>
          <span class="date_header">Date: {{date('d-m-Y', strtotime($failed_implant->created_at))}}</span>
      </div>
    </header>
    <main>
        <section>
            <h4 class="heading">Order Details</h4>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <th>User's Name</th>
                    <th>Email</th>
                    <th>Mobile Number</th>
                    <th>User Type</th>
                    @if(!empty($user->dealer_id))
                        <th>Dealer Name</th>
                    @endif
                </tr>
                <tr>
                    
                    <td>{{$user->my_title.' '.$user->first_name.' '.$user->last_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td>{{array(
                            '1' => 'Admin',
                            '2' => 'Dealer',
                            '3' => 'Doctor',
                            '4' => 'Store Manager')[$user->role_id]}}</td>
                    @if(!empty($user->dealer_id))
                        <?php $dealer = DB::table('users')->where('id',$user->dealer_id)->first(); ?>
                        <td>{{ $dealer->my_title.' '.$dealer->first_name.' '.$dealer->last_name }}</td>
                    @endif
                </tr>
            </table>

            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Order Type</th>
                    <th>Return Implant status</th>
                    <th>Product Code(or label)</th>
                    <th>LOT #</th>
                    <th>Item Status</th>       
                </tr>
                <tr>
                    <td>{{($failed_implant->order_type =='on')?'Online':'Offline'}}</td>
                    <td>{{ $failed_implant->status }}</td>
                    <td>{{ $failed_implant->implant_code }}</td>
                    <td>{{ $failed_implant->lot }}</td>
                    <td>{{($failed_implant->item_status==0)?'Used':'Unused'}}</td>                    
                </tr>
            </table>

            <table border="0" cellspacing="0" cellpadding="0">
                <tr>                    
                    <th>Clinic Name</th>
                    <th>Clinic Phone</th>
                    <th>Clinic Address</th>                                              
                    <th>Membership group</th>
                </tr>
                <tr>                    
                    <td>{{ $user->clinic_name }}</td>
                    <td>{{ $user->clinic_number }}</td>
                    <td style="width: 200px">{{ $user->clinic_address }}</td>                    
                    <td>{{ Webkul\Customer\Models\CustomerGroup::CustomerGroupName($user->customer_group_id) }}</td>
                </tr>
            </table>

            @if(!empty($failed_implant->comments))
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>                    
                        <th>Comments</th>                    
                    </tr>
                    <tr>                    
                        <td>{{ $failed_implant->comments }}</td>
                    </tr>
                </table>
            @endif

            <h4 class="heading">Return Items</h4>
            @if($failed_implant->order_type =='on')
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th scope="col">Order ID</th>
                            <th scope="col">Return Product</th>
                            <th scope="col">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($return_implants_return as $impnt)
                            <tr>
                                <td scope="row">#{{$impnt->order}}</td>
                                
                                <td>{{ DB::table('order_items')->where('id', $impnt->item)->value('name') }}</td>
                                <td>{{$impnt->quantity}}</td>
                            </tr>   
                        @endforeach                                                                     
                    </tbody>                                    
                </table>
            @else
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th scope="col">Order number</th>
                            <th scope="col">Return Product</th>
                            <th scope="col">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($return_implants_return as $impnt)
                            <tr>
                                <td scope="row">#{{$impnt->order}}</td>                                                
                                <td>{{ $impnt->item }}</td>
                                <td>{{$impnt->quantity}}</td>
                            </tr>   
                        @endforeach                                                                     
                    </tbody>                                    
                </table>
            @endif

            <h4 class="heading">Replacement Items</h4>                       
            <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th scope="col">Replacement Item</th>
                        <th scope="col">Replace Quantity</th>
                    </tr>
                </thead>
                <tbody>                                    
                    @foreach ($return_implants_replace as $impnt)
                        <tr>
                            <td scope="row">{{  \DB::table('product_flat')->where('product_id',$impnt->item)->value('name')}}</td>
                            <td>{{$impnt->quantity}}</td>
                        </tr>   
                    @endforeach                                                                     
                </tbody>                                    
            </table>
        </section>       
    </main>
  </body>
  </html>

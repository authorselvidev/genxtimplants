@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.address.index.page-title') }}
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">

<div class="account-content">

    @include('shop::customers.account.partials.sidemenu')

    <div class="account-layout">

        <div class="account-head">
            <span class="back-icon"><a href="{{ route('admin.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
            <span class="account-heading">{{ __('shop::app.customer.account.address.index.title') }}</span>

            @if (! $addresses->isEmpty())
                <span class="account-action">
                    <a href="{{ route('admin.address.create') }}"><i class="fa fa-plus"></i> {{ __('shop::app.customer.account.address.index.add') }}</a>
                </span>
            @else
                <span></span>
            @endif
            <div class="horizontal-rule"></div>
        </div>

        {!! view_render_event('bagisto.shop.customers.account.address.list.before', ['addresses' => $addresses]) !!}

        <div class="account-table-content">
            @if ($addresses->isEmpty())
                <div>{{ __('shop::app.customer.account.address.index.empty') }}</div>
                <br/>
                <a href="{{ route('admin.address.create') }}">{{ __('shop::app.customer.account.address.index.add') }}</a>
            @else
                <div class="address-holder">
                    @foreach ($addresses as $address)
                        <div class="address-card-1">
                            <div class="details">
                                <span class="bold">{{ str_replace('-',' ',$address->name) }}</span>
                                </br>
                                {{ $address->address }}</br>
                                 {{ Webkul\Customer\Models\Cities::GetCityName($address->city)}}</br> {{ Webkul\Customer\Models\States::GetStateName($address->state)}}, India</br>
                                {{ $address->postcode }}</br></br>
                                {{ __('shop::app.customer.account.address.index.contact') }} : {{ $address->phone }}

                                <div class="control-links mt-20">
                                    <span>
                                        <a href="{{ route('admin.address.edit', base64_encode($address->id)) }}">
                                            {{ __('shop::app.customer.account.address.index.edit') }}
                                        </a>
                                    </span>

                                    <span>
                                        <a href="{{ route('admin.address.delete', base64_encode($address->id)) }}" onclick="deleteAddress('{{ __('shop::app.customer.account.address.index.confirm-delete') }}')">
                                            {{ __('shop::app.customer.account.address.index.delete') }}
                                        </a>
                                    </span>
                                </div>

                                @if ($address->default_address)
                                    <span class="default-address badge badge-md badge-success"  style="background-color: #007704;">{{ __('shop::app.customer.account.address.index.default') }}</span>
                                @else
                                    <div class="make-default">
                                        <a href="{{ route('admin.make.default.address', base64_encode($address->id)) }}" class="btn btn-md btn-primary">{{ __('shop::app.customer.account.address.index.make-default') }}</a>
                                    </div>
                                @endif

                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>

        {!! view_render_event('bagisto.shop.customers.account.address.list.after', ['addresses' => $addresses]) !!}
    </div>
</div>
</div>
</div>
@endsection

@push('scripts')
    <script>
        function deleteAddress(message) {
            if (!confirm(message))
            event.preventDefault();
        }

        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        })
    </script>
@endpush

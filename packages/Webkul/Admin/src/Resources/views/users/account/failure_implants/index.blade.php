@extends('shop::layouts.master')

@section('page_title')
    Return Implants
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">
    <div class="account-content">
        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">

            <div class="account-head mb-10">
                <span class="back-icon">
                    <a href="{{ route('admin.account.index') }}">
                        <i class="icon angle-left-icon back-link"></i>
                    </a>
                </span>
                <span class="account-heading">
                    Return Implants
                </span>

                <div class="horizontal-rule"></div>
            </div>

            <div class="account-items-list">
                <?php  $i=1; ?>
                    <div class="wizard">                        
                        <div class="tab-content">
                            <div class="tab-pane active" role="tabpanel" id="step1">
                                <div class="table">
                                    <a class="btn btn-primary btn-sm create_btn" href="{{route('admin.failure.implants.create')}}">Create</a>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th> Sl.No </th>
                                                <th> Status </th>
                                                <th> Order Mode </th>
                                                <th> Condition </th>
                                                <th> Created Date </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach ($failure_implants as $item)
                                                <tr>  
                                                    <td>{{ $i++ }}</td>                                                  
                                                    <td>
                                                        @if($item->status == 'Processing')
                                                            <span class="badge badge-md badge-success" style="background-color: #212427;">Processing</span>
                                                        @elseif ($item->status == 'Completed')
                                                            <span class="badge badge-md badge-success" style="background-color: #007704;">Completed</span>
                                                        @elseif ($item->status == 'Approved')
                                                            <span class="badge badge-md badge-success" style="background-color: #1395CD">Approved</span>
                                                        @elseif ($item->status == 'Rejected')
                                                            <span class="badge badge-md badge-danger" style="background-color: #e40000;">Rejected</span>
                                                        @elseif ($item->status == 'Pending')
                                                            <span class="badge badge-md badge-warning">Pending</span>
                                                        @endif
                                                    </td>
                                                    <td>{{($item->order_type =='on')?'Online':'Offline'}}</td>
                                                    <td>{{ [0=>'Used', 1=>'Unused'][$item->item_status] }}</td>
                                                    <td>{{date('d M Y', strtotime($item->created_at))}}</td>
                                                    <td style="font-size: 16px">
                                                        <a href="{{ route('admin.failure.implants.view', $item->id) }}">
                                                            <i class="icon eye-icon"></i>
                                                        </a>
                                                        @if($item->status == 'Pending'|| $item->status == 'Approved' || $item->status == 'Processing' )
                                                            <a href="{{ route('admin.failed-implants.print', $item->id) }}" style="color: #999">
                                                                <i class="fa fa-print"></i>
                                                            </a>
                                                        @endif

                                                        @if($item->status == 'Pending')
                                                        <a href="{{route('admin.failure.implants.edit', $item->id)}}">
                                                            <i class="icon pencil-lg-icon"></i>
                                                        </a>
                                                        @endif
                                                        @if($item->status == 'Pending')
                                                            <a href="{{ route('admin.failure.implants.delete', $item->id) }}" id="deleteOption">
                                                                <i class="icon trash-icon"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach

                                            @if (!$failure_implants->count())
                                                <tr>
                                                    <td class="empty" colspan="6">{{ __('admin::app.common.no-result-found') }}</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    
                                </div>
                                <p class="print_notice">
                                    <em><strong>Note: Please print the PDF form and send along with the implants to the following address</strong><br></em>
                                        Regenics,<br>
                                        Plot No.11, 1st Floor, Nanda Dental Care Premier Building, Lane M,<br>
                                        Near Westin Hotel, <br>
                                        Mundhwa Road, <br>
                                        Pune - 411001<br><br>     
                                        For any queries please Whatsapp : +918484088331 <br>                               
                                </p>

                                @if (!$failure_implants->count())
                                    <div class="responsive-empty">{{ __('admin::app.common.no-result-found') }}</div>
                                @endif

                            </div>                            
                            <div class="clearfix"></div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>  

    @if(Session::has('returncreated'))
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Return Implant</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                
                    <div class="modal-body">
                        <p class="print_notice">
                            <em><strong>Note: Please print the PDF form and send along with the implants to the following address</strong><br></em>
                            <br>
                            Regenics,<br>
                            Plot No.11, 1st Floor, Nanda Dental Care Premier Building, Lane M,<br>
                            Near Westin Hotel, <br>
                            Mundhwa Road, <br>
                            Pune - 411001<br><br>       
                            For any queries please Whatsapp : +918484088331 <br>                                                            
                        </p>
                    </div>
                
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@push('scripts')
    <style type="text/css">
        .create_btn{
            display: inline-block;
            float:right;
            font-size: 1.5rem;
            margin-bottom: 5px;
            margin: 8px;
        }

        .print_notice{
            display: block;
            padding: 5px 10px;
            margin-top:10px;
            color: #444;
            font-size: 1.5rem;
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;

        }

        .icon{
            width: 18px !important;
            height: 18px !important;
        }

        #order_item, .selectize-input{
            max-width: 320px !important;
            width: 100% !important;
        }

        .active .round-tab{
            color: #0995CD;
        }

		button.first_page{
            display: flex !important;
            align-items: center !important;
            overflow: hidden;
        }

        .spinning{
            margin-left: 10px;
            display:inline-block;
            border: 2px solid #f3f3f3;
            border-radius: 50%;
            border-top: 2px solid #3498db;
            padding: 6px;
            -webkit-animation: loadingSpin 1.2s linear infinite;
            animation: loadingSpin 1.2s linear infinite;
        }

        .imgThumb{
            width: 100px;
            margin: 10px 10px 0;
            cursor: pointer;
        }

        @-webkit-keyframes loadingSpin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }


        @keyframes loadingSpin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        @media(max-width: 768px){
            .nav-tabs a{
                text-align: center;
                border:1px solid #fff !important;
            }

            .active .round-tab{
                border-bottom: 1px solid #0995CD;
            }
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });

            @if(Session::has('returncreated'))
                $(window).on('load', function() {
                    $('#exampleModal').modal('show');
                });
            @endif

            $('body').on('click', '#deleteOption', function(e){
                if(!confirm('Do you like to Delete'))  e.preventDefault();
            });
        });
    </script>
@endpush

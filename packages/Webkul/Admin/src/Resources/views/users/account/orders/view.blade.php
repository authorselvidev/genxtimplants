@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.order.view.page-tile', ['order_id' => $order->id]) }}
@endsection

@section('content-wrapper')
    <div class="profile-page">
            <div class="main-container-wrapper">

    <div class="account-content">
        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">

            <div class="account-head">
                <span class="back-icon"><a href="{{ route('admin.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
                <span class="account-heading">
                    {{ __('shop::app.customer.account.order.view.page-tile', ['order_id' => $order->id]) }}
                </span>
                <span></span>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.orders.view.before', ['order' => $order]) !!}

            <div class="sale-container view-ord">

                <tabs>
                     @if ($order->invoices->count() || ($order->invoices->count()))
                     <tab name="{{ __('shop::app.customer.account.order.view.info') }}" :selected="true"> @endif

                        <div class="sale-section">
                            <div class="section-content">
                                <div>
                                    <span class="title">
                                        <strong>{{ __('shop::app.customer.account.order.view.placed-on') }}:</strong> &nbsp;
                                    </span>

                                    <span class="value">
                                        {{ core()->formatDate($order->created_at, 'd M Y') }}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="sale-section">
                            <div class="secton-title">
                                <h4>{{ __('shop::app.customer.account.order.view.products-ordered') }}</h4>
                            </div>

                            <div class="account-items-list section-content">
                                <div class="table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                                <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                                <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                                <th>{{ __('shop::app.customer.account.order.view.item-status') }}</th>
                                                <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                                <th>{{ __('shop::app.customer.account.order.view.tax-percent') }}</th>
                                                <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                                                <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            @foreach ($order->items as $item)
                                                <tr>
                                                    <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}">
                                                        {{ $item->type == 'configurable' ? $item->child->sku : $item->sku }}
                                                    </td>
                                                    <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">{{ $item->name }}</td>
                                                    <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">{{ core()->formatPrice($item->price, $order->order_currency_code) }}</td>
                                                    <td data-value="{{ __('shop::app.customer.account.order.view.item-status') }}">
                                                        <span class="qty-row">
                                                            {{ __('shop::app.customer.account.order.view.item-ordered', ['qty_ordered' => $item->qty_ordered]) }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_invoiced ? __('shop::app.customer.account.order.view.item-invoice', ['qty_invoiced' => $item->qty_invoiced]) : '' }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_shipped ? __('shop::app.customer.account.order.view.item-shipped', ['qty_shipped' => $item->qty_shipped]) : '' }}
                                                        </span>

                                                        <span class="qty-row">
                                                            {{ $item->qty_canceled ? __('shop::app.customer.account.order.view.item-canceled', ['qty_canceled' => $item->qty_canceled]) : '' }}
                                                        </span>
                                                    </td>
                                                    <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">{{ core()->formatPrice($item->total, $order->order_currency_code) }}</td>
                                                    <td data-value="{{ __('shop::app.customer.account.order.view.tax-percent') }}">{{ number_format($item->tax_percent, 2) }}%</td>
                                                    <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">{{ core()->formatPrice($item->tax_amount, $order->order_currency_code) }}</td>
                                                    <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">{{ core()->formatPrice($item->total + $item->tax_amount, $order->order_currency_code) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                </div>

                                @if($order->status == 'canceled')
                                    <div>
                                        <h4>Reason for cancelation</h4>
                                        <p style="margin-left: 5px;display: inline-block; font-size: 16px">{{ $order->remarks }}</p>
                                    </div>
                                @endif

                                <div class="totals">
                                    <table class="sale-summary">
                                        <tbody>
                                            <tr>
                                                <td>{{ __('shop::app.customer.account.order.view.subtotal') }}</td>
                                                <td>:</td>
                                                <td>{{ core()->formatPrice($order->sub_total, $order->order_currency_code) }}</td>
                                            </tr>


                                              <?php $discounted_subtotal=0; ?>
                                    @if($order->mem_discount_amount == 0.0000 && $order->promo_code_amount == 0.0000 && $order->credit_discount_amount == 0.0000 && $order->reward_coin == 0.0000 && $order->base_discount_amount != 0.0000)
                                    <tr>
                                        <td class="pull-left text-left">Special Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->base_discount_amount) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->base_discount_amount; ?>

                                    @endif

                                    @if($order->mem_discount_amount != 0.0000)
                                    <tr>
                                        <td class="pull-left text-left">Membership Discount:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->mem_discount_amount) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Group:</td>
                                        <td>:</td>
                                        <td> {{ $order->customer->customerGroup['name'] }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->mem_discount_amount; ?>

                                    @endif

                                    @if($order->credit_discount_amount != 0.0000)
                                        <tr>
                                            <td>Advance Payment Discount:</td>
                                            <td>:</td>
                                            <td>- {{ core()->currency($order->credit_discount_amount) }}</td>
                                        </tr>
                                        @if(isset($order->coupon_code) || $order->coupon_code != "")
                                            <tr>
                                                <td>Discount Code:</td>
                                                <td>:</td>
                                                <td> {{$order->coupon_code }}</td>
                                            </tr> 
                                        @endif
                                        <?php $discounted_subtotal = $order->base_sub_total - $order->credit_discount_amount; ?>
                                    @endif

                                    @if($order->promo_code_amount != 0.0000)
                                        <tr>
                                            <td>Promo Code Discount:</td>
                                            <td>:</td>
                                            <td>- {{ core()->currency($order->promo_code_amount) }}</td>
                                        </tr>
                                        @if(isset($order->coupon_code) || $order->coupon_code != "")
                                            <tr>
                                                <td>Discount Code:</td>
                                                <td>:</td>
                                                <td> {{$order->coupon_code }}</td>
                                            </tr> 
                                        @endif
                                        <?php $discounted_subtotal = $order->base_sub_total - $order->promo_code_amount; ?>
                                    @endif

                                    @if($order->reward_coin != 0.0000)
                                    <tr>
                                        <td>Reward Points:</td>
                                        <td>:</td>
                                        <td>- {{ core()->currency($order->reward_coin) }}</td>
                                    </tr>
                                    <?php $discounted_subtotal = $order->base_sub_total - $order->reward_coin; ?>
                                    @endif
                                    @if($discounted_subtotal != 0)
                                    <tr>
                                        <td>Discounted Subtotal</td>
                                        <td>:</td>
                                        <td>{{ core()->formatBasePrice($discounted_subtotal) }}</td>
                                    </tr>
                                    @endif


                                            <tr>
                                                <td>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</td>
                                                <td>:</td>
                                                <td>{{ core()->formatPrice($order->shipping_amount, $order->order_currency_code) }}</td>
                                            </tr>

                                            <tr class="border">
                                                <td>{{ __('shop::app.customer.account.order.view.tax') }} (12%)</td>
                                                <td>:</td>
                                                <td>{{ core()->formatBasePrice($order->tax_amount) }}</td>
                                            </tr>
											
                                            <tr class="bold">
                                                <td>{{ __('shop::app.customer.account.order.view.grand-total') }}</td>
                                                <td>:</td>
                                                <td>{{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}</td>
                                            </tr>

                                            <?php $payment_paid = DB::table('payments')->where('order_id',$order->id)->first(); 
                                    $manual_payment = DB::table('manual_payment_entry')->where('order_id',$order->id)->sum('paid_amount');
                                    //dd($manual_payment) ?>
                                    @if($order->payment[0]->method == 'razorpay' && isset($payment_paid))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                        </tr>
                                    @elseif($order->payment[0]->method == 'prepayment'|| $order->payment[0]->method == 'paylaterwithcredit')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                        </tr>                                    
                                    @elseif($order->payment[0]->method == 'cashondelivery' && isset($manual_payment))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>{{ core()->formatBasePrice($manual_payment) }}</td>
                                        </tr>
                                    @endif
                                    <!-- @if($order->status == 'completed')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-paid') }}</td>
                                            <td>:</td>
                                            <td>- {{ core()->formatBasePrice($order->base_grand_total_invoiced) }}</td>
                                        </tr>
                                    @endif  -->

                                    <tr class="bold">
                                        <td>{{ __('admin::app.sales.orders.total-refunded') }}</td>
                                        <td>:</td>
                                        
                                        <td>{{ core()->formatBasePrice($order->base_grand_total_refunded) }}</td>
                                        
                                    </tr> 
                                   
                                 @if($order->payment[0]->method == 'cashondelivery' && isset($manual_payment))
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                            <td>{{ core()->formatBasePrice($order->base_grand_total - $manual_payment)  }}</td>
                                        </tr>
                                    @elseif($order->payment[0]->method == 'razorpay' && isset($payment_paid) || $order->payment[0]->method == 'prepayment')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice(0) }}</td>
                                        </tr> 
                                    @elseif($order->payment[0]->method == 'paylaterwithcredit')
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice(0) }}</td>
                                        </tr> 
                                    @else
                                        <tr class="bold">
                                            <td>{{ __('admin::app.sales.orders.total-due') }}</td>
                                            <td>:</td>
                                             <!--<td>{{ core()->formatBasePrice($order->base_grand_total - $order->grand_total_invoiced) }}</td> -->
                                            <td>{{ core()->formatBasePrice($order->base_grand_total) }}</td>
                                        </tr> 
                                     @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @if ($order->invoices->count() || ($order->invoices->count())) </tab> @endif

                    @if ($order->invoices->count())
                        <tab name="{{ __('shop::app.customer.account.order.view.invoices') }}">

                            @foreach ($order->invoices as $invoice)

                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>{{ __('shop::app.customer.account.order.view.individual-invoice', ['invoice_id' => $invoice->id]) }}</span>

                                        <a href="{{ route('admin.orders.print', base64_encode($invoice->id)) }}" class="pull-right">
                                            {{ __('shop::app.customer.account.order.view.print') }}
                                        </a>
                                    </div>

                                    <div class="section-content">
                                        <div class="table">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                                        <th>Lot Number<br>(Num, Qty, Exp.date)</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    @foreach ($invoice->items as $item)
                                                        <tr>
                                                            <td>{{ $item->child ? $item->child->sku : $item->sku }}</td>
                                                            <td>{{ $item->name }}</td>
                                                            <td>{{ core()->formatPrice($item->price, $order->order_currency_code) }}</td>
                                                            <td>{{ $item->qty }}</td>
                                                            <?php
                                                                $invoice_item_id =  DB::table('invoice_items')->where('order_item_id', $item->order_item_id)->addSelect('id')->first();
                                                                $lot = ($invoice_item_id)?DB::table('lot_items')->where('invoice_items_id', $invoice_item_id->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get():[];
                                                            ?>
                                                            <td>
                                                                @if(count($lot)>0)
                                                                    @foreach($lot as $key => $lot_item)
                                                                        {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}},
                                                                        {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}},
                                                                        {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
                                                                    @endforeach
                                                                @else
                                                                    -
                                                                @endif
                                                            </td>
                                                            <td>{{ core()->formatPrice($item->total, $order->order_currency_code) }}</td>
                                                            <td>{{ core()->formatPrice($item->tax_amount, $order->order_currency_code) }}</td>
                                                            <td>{{ core()->formatPrice($item->total + $item->tax_amount, $order->order_currency_code) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        @foreach ($invoice->items as $item)
                                            <table class="responsive-table">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.SKU') }}
                                                        </td>
                                                        <td>{{ $item->child ? $item->child->sku : $item->sku }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.product-name') }}
                                                        </td>
                                                        <td>{{ $item->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.price') }}
                                                        </td>
                                                        <td>
                                                            {{ core()->formatPrice($item->price, $order->order_currency_code) }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.qty') }}
                                                        </td>
                                                        <td>{{ $item->qty }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ __('shop::app.customer.account.order.view.subtotal') }}</td>
                                                        <td>
                                                            {{ core()->formatPrice($item->total, $order->order_currency_code) }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.tax-amount') }}
                                                        </td>
                                                        <td>{{ core()->formatPrice($item->tax_amount, $order->order_currency_code) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.grand-total') }}
                                                        </td>
                                                        <td>
                                                            {{ core()->formatPrice($item->total + $item->tax_amount, $order->order_currency_code) }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        @endforeach

                                        <div class="totals">
                                            <table class="sale-summary">
                                                <tr>
                                                    <td>{{ __('shop::app.customer.account.order.view.subtotal') }}</td>
                                                    <td>-</td>
                                                    <td>{{ core()->formatPrice($invoice->sub_total, $order->order_currency_code) }}</td>
                                                </tr>

                                                <tr>
                                                    <td>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</td>
                                                    <td>-</td>
                                                    <td>{{ core()->formatPrice($order->shipping_amount, $order->order_currency_code) }}</td>
                                                </tr>

                                                <tr>
                                                    <td>{{ __('shop::app.customer.account.order.view.tax') }}</td>
                                                    <td>-</td>
                                                    <td>{{ core()->formatPrice($invoice->tax_amount, $order->order_currency_code) }}</td>
                                                </tr>

                                                <tr class="bold">
                                                    <td>{{ __('shop::app.customer.account.order.view.grand-total') }}</td>
                                                    <td>-</td>
                                                    <td>{{ core()->formatPrice($invoice->grand_total, $order->order_currency_code) }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                        </tab>
                    @endif

                    @if ($order->shipments->count())
                        <tab name="{{ __('shop::app.customer.account.order.view.shipments') }}">

                            @foreach ($order->shipments as $shipment)

                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>{{ __('shop::app.customer.account.order.view.individual-shipment', ['shipment_id' => $shipment->id]) }}</span>
                                    </div>

                                    <div class="section-content">

                                        <div class="table">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                                        <th>Lot Number<br>(Num, Qty, Exp.date)</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    @foreach ($shipment->items as $item)

                                                        <tr>
                                                            <td>{{ $item->sku }}</td>
                                                            <td>{{ $item->name }}</td>
                                                            <?php
                                                            $invoice_item_id = DB::table('invoice_items')->where('order_item_id', $item->order_item_id)->addSelect('id')->first();
                                                            $lot = ($invoice_item_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_item_id->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get()):[];
                                                            ?>
                                                            <td>
                                                                @if(count($lot)>0)
                                                                    @foreach($lot as $key => $lot_item)
                                                                        {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}},
                                                                        {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}},
                                                                        {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
                                                                    @endforeach
                                                                @else
                                                                    -
                                                                @endif
                                                            </td>
                                                            <td>{{ $item->qty }}</td>
                                                        </tr>

                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>

                                        @foreach ($shipment->items as $item)
                                            <table class="responsive-table">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.SKU') }}
                                                        </td>
                                                        <td>{{ $item->sku }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.product-name') }}
                                                        </td>
                                                        <td>{{ $item->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ __('shop::app.customer.account.order.view.qty') }}
                                                        </td>
                                                        <td> {{ $item->qty }} </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        @endforeach
                                        <?php $shipping_carrier = DB::table('shipping_carriers')->where('id',$shipment->shipping_carrier_id)->first(); ?>
                                        @if($shipping_carrier)
                                        <div class="carrier-info">
                                            <div>
                                            <label>Carrier Name:</label>
                                            <span>{{$shipping_carrier->name}}</span>
                                        </div>
                                        <div>
                                            <label>Tracking URL:</label>
                                            <span>{{$shipping_carrier->tracking_url}}</span>
                                        </div>
                                        <div>
                                            <label>Tracking Number:</label>
                                            <span>{{$shipment->track_number}}</span>
                                        </div>
                                        </div>
                                        @endif

                                    </div>
                                </div>

                            @endforeach

                        </tab>
                    @endif
                </tabs>

                <div class="sale-section">
                    <div class="section-content" style="border-bottom: 0">
                        <div class="order-box-container">
                            <div class="box">
                                <div class="box-title">
                                    {{ __('shop::app.customer.account.order.view.shipping-address') }}
                                </div>

                                <div class="box-content">
                                    <?php
                                        $get_order = DB::table('orders')->where('id',$order->id)->first();
                                        //dd($get_order);
                                        $billing_address = DB::table('user_addresses')->where('id',$get_order->billing_address)->latest()->first();
                                    ?>
                                    @if(isset($billing_address)) 
                                    @include ('admin::sales.address', ['address' => $billing_address])
                                    @endif

                                </div>
                            </div>

                            <div class="box">
                                <div class="box-title">
                                    {{ __('shop::app.customer.account.order.view.billing-address') }}
                                </div>

                                <div class="box-content">
                                    <?php
                                        $get_order = DB::table('orders')->where('id',$order->id)->first();
                                        //dd($get_order);
                                        $shipping_address = DB::table('user_addresses')->where('id',$get_order->shipping_address)->latest()->first();
                                    ?>
                                    @if(isset($shipping_address)) 
                                        @include ('admin::sales.address', ['address' => $shipping_address])
                                    @endif

                                </div>
                            </div>

                            @if($order->shipping_title)
                                <div class="box">
                                    <div class="box-title">
                                        {{ __('shop::app.customer.account.order.view.shipping-method') }}
                                    </div>

                                    <div class="box-content">
                                        {{ $order->shipping_title }}
                                    </div>
                                </div>
                            @endif



                            <div class="box">
                                <div class="box-title">
                                    {{ __('shop::app.customer.account.order.view.payment-method') }}
                                </div>

                                <div class="box-content">
                                    @if($order->payment[0]->method == 'paylaterwithcredit')
                                        {{ "Pay with ".(($order->customer->role_id=='2')?'credit':'advance') }}
                                    @else
                                        {{ core()->getConfigData('sales.paymentmethods.' . $order->payment[0]->method . '.title') }}
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            {!! view_render_event('bagisto.shop.customers.account.orders.view.after', ['order' => $order]) !!}

        </div>

    </div>
</div>
</div>

@endsection

@push('scripts')
    <style type="text/css">
        h4{
          font-family: Arial, sans-serif !important;
          color: #0995CD;
          font-size: 22px;
          margin-bottom: none;
          text-transform: capitalize;
          text-align: left;
        }        
    </style>
    <script>
        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        })
    </script>
@endpush

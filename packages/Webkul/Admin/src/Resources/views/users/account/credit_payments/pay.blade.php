@extends('shop::layouts.master')

@section('page_title')
    Payments
@endsection

@section('content-wrapper')
<?php
    $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
    if(empty($user))
        $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
?>

<div class="profile-page">
    <div class="main-container-wrapper">
        <div class="account-content">
            @include('shop::customers.account.partials.sidemenu')

            <div class="account-layout">

                <?php 
                    $role_name = [0=>'Other', 2=>'dealer', 3=>'customer'][$user->role_id];
                    $route_name = $role_name.'.credit_payment.payviacheque';

                    $credit_pending_amount=$available_credit_limit=0;
                    $credit_pending_amount = $user->credit_used - $user->credit_paid;
                    $available_credit_limit = $user->credit_limit - $credit_pending_amount;
                ?>

                <div class="card-container">
                    <div class="col-md-12 wedget-container">
                        <div class="balance-card col-md-3">
                           <div class="title">Credit Limit</div>
                           <div class="data"><i class="fa fa-inr"></i> {{ number_format($user->credit_limit,2) }}</div>
                        </div>

                        @if($credit_pending_amount!=0) 
                            <div class="balance-card col-md-3">
                                <div class="title">Balance Due</div>
                                <div class="data">- <i class="fa fa-inr"></i> {{ number_format($credit_pending_amount,2)}}</div>
                            </div>
                        @else
                            <div class="balance-card col-md-4">
                                <div class="title">Advance Balance</div>
                                <div class="data"><i class="fa fa-inr"></i> {{ number_format($user->credit_balance,2) }}</div>
                            </div>
                        @endif

                        <div class="balance-card col-md-3">
                            <div class="title">Credit Limit Available</div>
                            <div class="data"><i class="fa fa-inr"></i>  {{ number_format($available_credit_limit,2)}}</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        
                        <div class="balance-card col-md-4">
                            <div class="title">Pending Approval</div>
                            <div class="data"><i class="fa fa-inr"></i>  {{ number_format($credit_pending,2)}}</div>
                        </div>
                    </div>
                </div>

                <h4>Submit Payment Information</h4><br/>
                  <form method="post" id="credit-payment-form" action="{{ route($route_name) }}" enctype="multipart/form-data" >     
                  @csrf
                       <div class="form-field inner-form col-md-12">

                        <div class="control-group col-md-6 pass">
                            <label class="required">Amount</label>
                            <input type="number"  class="control pay_credit_amount" name="pay_credit_amount" step="any">
                            <span class="control-error"></span>
                        </div>

                        <div class="control-group col-md-6 pass cpass">
                                <label for="payment_type" class="required">Paying By</label>
                                <select class="control payment_type" name="payment_type"  class="control">
                                    <option value="2">Cheque</option>
                                    <option value="3">Bank Transfer/NEFT/UPI</option>
                                </select>
                                <span class="control-error"></span>
                            </div>
                         </div>
                        <div class="form-field cheque-sec col-md-12">
                          <div class="control-group col-md-6 pass">
                            <label class="required">Cheque Number</label>
                            <input type="text" class="control cheque_number" name="cheque_number"  value="">
                            <span class="control-error"></span>
                          </div>
                          <div class="control-group col-md-6 pass">
                            <label>Cheque Image</label>
                            <input type="file" name="cheque_image" class="img-thumb">                            
                          </div>

                         </div>

                        <div class="form-field bank-neft-upi col-md-12">
                            <div class="control-group col-md-6 pass">
                              <label class="required">Transation ID</label>
                              <input type="text"  v-validate="'required'" class="control transation_number" name="transation_number"  value="">
                              <span class="control-error"></span>
                            </div>
                            <div class="control-group col-md-6 pass">
                              <label>Transfer Receipt</label>
                              <input type="file" name="receipt_image" class="img-thumb">
                            </div>
                        </div>
                        
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <input type="hidden" name="payment_id" class="payment_id" value="">

                        <div class="control-group col-md-12 pass cpass">
                            <label>Notes</label>
                            <textarea class="control notes col-md-12 " style="width:100%; border: 2px solid #c7c7c7;" name="notes"></textarea>
                        </div>

                        <div class="control-group col-md-3 cpass">
                            <button type="submit" class="btn amount-btn btn-lg pay-via-cheque btn-primary pay-credit-amount">Submit for approval</button>
                        </div>
                </form>

                <br/>

                @if(count($credit_payment)>0)
                <br style="clear:both"/>
                <h4 style="clear:both">Payment History</h4><br/>
                <div class="table" style="overflow-x:unset;">
                    <table style="margin-bottom: 20px;">
                        <thead>
                            <tr>
                                <th>Payment Type</th>
                                <th>TXN Number</th>
                                <th>Image</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Created Date</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($credit_payment as $payment)
                              <tr>
                                <td>
                                    <span>{{[0=>'Other',1=>'Razorpay', 2=>'Cheque', 3=>'Bank Transfer/NEFT/UPI'][$payment->payment_type]}}</span>
                                </td>
                                <td>
                                    <span>
                                  @if($payment->cheque_number)
                                    {{$payment->cheque_number}}
                                  @else
                                    -
                                  @endif
                                  </span>
                                </td>
                                <td>
                                    <span>
                                    @if($payment->cheque_image)
                                      <a target="_blank" class="img-enlargeable" href="{{url(bagisto_asset('images/credit_images/'.$payment->cheque_image))}}">Click here</a>
                                    @else
                                      -
                                    @endif
                                    </span>
                                </td>
                                <td><span>@if(DB::table('users')->where('id', $payment->paid_by)->value('role_id')==1)*@endif{{ $payment->amount }}</span></td>
                                <td>
                                    @if ($payment->status == 0)
                                      <span class="badge badge-md badge-warning">Pending</span>
                                    @elseif ($payment->status == 1)
                                      <span class="badge badge-md badge-success"  style="background-color: #007704;">Approved</span>
                                    @elseif ($payment->status == 2)
                                      <span class="badge badge-md badge-danger" style="background-color: #e40000;">Rejected</span>
                                    @endif
                                </td>
                                <td>{{date("d-m-Y", strtotime($payment->created_at)) }}</td>
                              </tr>
                            @endforeach                            
                        </tbody>
                    </table>
                    {{ $credit_payment->links() }}
                    <span style="color:red">* Added by admin</span>
                  </div>
                  <br/>
                  @endif
              </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $(document).ready(function(){

        $('.content-container').css({
            'background': '#F1F3F6',
        });
        
        $('body').on('change','.payment_type', function(){
          $('.cheque-sec, .bank-neft-upi').css('display', 'none');
          $('.pay-credit-amount').removeClass('pay-via-cheque');
          $('.pay-credit-amount').removeClass('pay-via-bank');

          if($(this).val() == 2){
            $('.cheque-sec').css('display', 'block');            
            $('.pay-credit-amount').removeClass('pay-via-bank');
            $('.pay-credit-amount').addClass('pay-via-cheque');
          }else if($(this).val() ==3){
            $('.bank-neft-upi').css('display', 'block');
            $('.pay-credit-amount').removeClass('pay-via-cheque');
            $('.pay-credit-amount').addClass('pay-via-bank');
          }
        });


        $('#credit-payment-form').on('submit',function(e){
            $(this).find('.control-error').hide();
            var payment_type = $(this).find('.payment_type').val(); // 2 - cheque, 3 - bank, 0 - others
            var transaction_id = $(this).find('.transaction_id'); //others
            var transaction_num = $(this).find('.transation_number'); //bank
            var check_number = $(this).find('.cheque_number'); //check_number
            var pay_amount = $(this).find('.pay_credit_amount');

            function sayError(ele){
                ele.parent().find('.control-error').text('This field is required').show();
            }

            var validate = true;

            if(!pay_amount.val()){
                validate = false;
                sayError(pay_amount);
            }

            if(payment_type == 2){
                if(!check_number.val()){
                    validate = false;
                    sayError(check_number);
                }
            }

            if(payment_type == 3){
                if(!transaction_num.val()){
                    validate = false;
                    sayError(transaction_num);
                }
            }

            if(payment_type == 0){
                if(!transaction_id.val()){
                    validate = false;
                    sayError(transaction_id);
                }
            }

            if(!validate){
                e.preventDefault();
            }else{
                $('button').prop('disabled', true);
            }
        });

        <?php /*
        function readIMG(input,image_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#'+image_id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('body').on('change','input[type=file]', function(){
          var image_id = $(this).parent().find('img').attr('id');
            readIMG(this,image_id);
        });
        
  
        $('body').on('click', '.cheque-sec > div > img', function(){
            $(this).parent().find('input[type=file]').trigger('click'); 
        });

        $('body').on('click', '.bank-neft-upi > div > img', function(){
            $(this).parent().find('input[type=file]').trigger('click'); 
        }); */ ?>


        function imgClickFunction(e){
                e.preventDefault();
                var src = $(this).attr('src') ?? $(this).attr('href');
                var modal;
                function removeModal(){ modal.remove(); $('body').off('keyup.modal-close'); }
                modal = $('<div>').css({
                    background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
                    backgroundSize: 'contain',
                    width:'100%', height:'100%',
                    position:'fixed',
                    zIndex:'10000',
                    top:'0', left:'0',
                    cursor: 'pointer'
                }).click(function(){
                    removeModal();
                }).appendTo('body');

                $('body').on('keyup.modal-close', function(e){
                    if(e.key==='Escape'){
                        removeModal();
                    }
                });
            }

            $('.img-thumb').on('change',function(event) {
                let input = event.target;
                let reader = new FileReader();
                reader.onload = function(){
                    let dataURL = reader.result;
                    let output = $(`<img class='imgThumb'>`).hide();
                    output.attr('src', dataURL);
                    $(input).next('.imgThumb').remove();
                    $(input).after(output);
                    output.fadeIn();
                    output.addClass('img-enlargeable').click(imgClickFunction);
                };
                reader.readAsDataURL(input.files[0]);
            });

            $('.img-enlargeable').click(imgClickFunction);
    });
</script>
<style type="text/css">
    .bank-neft-upi{    
        display:none;
    }
    
    h4{
      font-family: Arial, sans-serif !important;
      color: #0995CD;
      font-size: 22px;
      margin-bottom: none;
      text-transform: capitalize;
      text-align: left;
    }

    .wedget-container{
        display: flex;
        justify-content: space-between;

    }

    .balance-card{
      font-family: Arial, sans-serif !important;
      display: inline-block;
      padding:15px;      
      margin:0 20px 40px 0px;
      width: 250px;
      text-align: left;
      border: 1px solid #ddd;      
    }

    .balance-card .title{
      font-size: 1.9rem;
      color: #0995CD;
      font-weight:normal;
      padding: 2px 0;
    }

    .balance-card .data{
      font-size: 1.9rem;
      font-weight: normal;
      color: #3F3F3F;
      padding: 2px 0;
    }

    .balance-card .data i{
        font-size: 1.6rem;
    }

    table{
      padding: 0 20px !important;
    }

    .table .next{
        position: unset;
        margin-top: 0px;
    }

    td, th{
      padding: 15px 5px !important;
    }

    .imgThumb{
        width: 200px;
        margin: 20px 10px 0;
        cursor: pointer;
    }

</style>
@endpush


@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.address.index.page-title') }}
@endsection

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">

<div class="account-content">

    @include('shop::customers.account.partials.sidemenu')
 <div class="account-layout">
    <form method="post" action="{{ route('admin.address.changedAddress') }}" @submit.prevent="onSubmit">
        @csrf
    <input type="hidden" name="addr_type" value="{{$_GET['type']}}">
   
        <div class="account-table-content">
                <div class="address-holder">
                    @foreach ($addresses as $address)
                        <div class="address-card-1">
                            
                            <input type="hidden" name="id" value="{{ $address->customer_id }}">
                            <input required type="radio" name="change_address" value="{{$address->id}}">
                            <div class="details">
                                <span class="bold" style="padding:2px 10px 0;">{{ str_replace('-',' ',$address->name) }}</span>
                                </br>
                                {{ $address->address }}</br>
                                 {{ Webkul\Customer\Models\Cities::GetCityName($address->city)}}</br> {{ Webkul\Customer\Models\States::GetStateName($address->state)}}, India</br>
                                {{ $address->postcode }}</br></br>
                                {{ __('shop::app.customer.account.address.index.contact') }} : {{ $address->phone }}
                            </div>
                        </div>
                    @endforeach
                </div>
        </div>
        <div class="button-group">
            <input class="btn btn-primary btn-lg" type="submit" value="Change Address">
        </div>
        {!! view_render_event('bagisto.shop.customers.account.address.list.after', ['addresses' => $addresses]) !!}
   
    </form>
     </div>
</div>
</div>
</div>
@endsection

@push('scripts')
    <script>
        function deleteAddress(message) {
            if (!confirm(message))
            event.preventDefault();
        }

        $(function(){
            $('.content-container').css({
                'background': '#F1F3F6',
            });
        });
    </script>
@endpush

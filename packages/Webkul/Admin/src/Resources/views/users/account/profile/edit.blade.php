@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.profile.edit-profile.page-title') }}
@endsection

@section('content-wrapper')
<div class="profile-page">
        <div class="main-container-wrapper">

    <div class="account-content">

        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">

            <div class="account-head mb-10">
                <span class="back-icon"><a href="{{ route('admin.account.index') }}"><i class="icon icon-menu-back"></i></a></span>

                <span class="account-heading">{{ __('shop::app.customer.account.profile.edit-profile.title') }}</span>

                <span></span>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.profile.edit.before', ['user' => $user]) !!}

            <form method="post" action="{{ route('admin.profile.update') }}" @submit.prevent="onSubmit">

                <div class="edit-form profile">
                    @csrf

                    {!! view_render_event('bagisto.shop.customers.account.profile.edit_form_controls.before', ['user' => $user]) !!}

                    

               <div class="form-fieldss">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-3 pass cpass" :class="[errors.has('my_title') ? 'has-error' : '']">
                                <label for="my_title" class="required">{{ __('shop::app.customer.signup-form.mytitle') }}</label>
                                <select name="my_title" class="control" v-validate="'required'">
                                    <option <?php if($user->my_title == "Mr.") echo 'selected'; ?> value="Mr.">Mr.</option>
                                    <option <?php if($user->my_title == "Mrs.") echo 'selected'; ?> value="Mrs.">Mrs.</option>
                                    <option <?php if($user->my_title == "Mr. Dr..") echo 'selected'; ?> value="Mr. Dr.">Mr. Dr.</option>
                                    <option <?php if($user->my_title == "Mrs. Dr.") echo 'selected'; ?> value="Mrs. Dr.">Mrs. Dr.</option>
                                    <option <?php if($user->my_title == "Mr. Dr. Prof.") echo 'selected'; ?> value="Mr. Dr. Prof.">Mr. Dr. Prof.</option>
                                    <option <?php if($user->my_title == "Mrs. Dr. Prof.") echo 'selected'; ?> value="Mrs. Dr. Prof.">Mrs. Dr. Prof.</option>
                                </select>
                                <span class="control-error" v-if="errors.has('my_title')">@{{ errors.first('my_title') }}</span>
                            </div>
                            <div class="control-group col-md-5" :class="[errors.has('first_name') ? 'has-error' : '']">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="control" name="first_name" v-validate="'required'" value="{{ $user->first_name }}" data-vv-as="&quot;First Name&quot;">
                                <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                            </div>
                            <div class="control-group cpass col-md-4">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="control" name="last_name"value="{{ $user->last_name }}">
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-6 pass" :class="[errors.has('phone_number') ? 'has-error' : '']">
                                <label for="phone_number" class="required">{{ __('shop::app.customer.signup-form.phonenumber') }}</label>
                                <input type="text" class="control" name="phone_number" v-validate="'required'" value="{{ $user->phone_number }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.phonenumber') }}&quot;">
                                <span class="control-error" v-if="errors.has('phone_number')">@{{ errors.first('phone_number') }}</span>
                            </div>

                            <div class="control-group col-md-6 cpass" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="email" v-validate="'required|email'" value="{{ $user->email }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                            </div>
                        </div>

                        
                    
                        @if($user->role_id == 3)
                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('clinic_name') ? 'has-error' : '']">
                                <label for="clinic_name" class="required">{{ __('shop::app.customer.signup-form.clinicname') }}</label>
                                <input type="text" class="control" name="clinic_name" v-validate="'required'" value="{{ $user->clinic_name }}">
                                <span class="control-error" v-if="errors.has('clinic_name')">@{{ errors.first('clinic_name') }}</span>
                            </div>
                            <div class="control-group col-md-6 cpass">
                                <label for="clinic_number">Clinic Phone Number</label>
                                <input type="text" class="control" name="clinic_number" value="{{ $user->clinic_number }}">
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-12 pass cpass" :class="[errors.has('clinic_address') ? 'has-error' : '']">
                                <label for="clinic_address" class="required">{{ __('shop::app.customer.signup-form.clinicaddr') }}</label>
                                <textarea class="control" name="clinic_address" v-validate="'required'">{{ $user->clinic_address }}</textarea>
                                <span class="control-error" v-if="errors.has('clinic_address')">@{{ errors.first('clinic_address') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
							<div class="control-group col-md-12 pass cpass" :class="[errors.has('dental_license_no') ? 'has-error' : '']">
								<label for="dental_license_no" class="required">Dental License Number</label>
								<input type="text" class="control" name="dental_license_no" value="{{ $user->dental_license_no }}" v-validate="'required'" >
								<span class="control-error" v-if="errors.has('dental_license_no')">@{{ errors.first('dental_license_no') }}</span>
							</div>
						</div>
                        @endif

                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('country_id') ? 'has-error' : '']">
                                <label for="country_id" class="required">{{ __('shop::app.customer.signup-form.country') }}</label>
                                <input type="text" class="control" name="country_id" v-validate="'required'" disabled="disabled" value="India">
                                <span class="control-error" v-if="errors.has('country_id')">@{{ errors.first('country_id') }}</span>
                            </div>
                            <div class="control-group cpass col-md-6" :class="[errors.has('state_id') ? 'has-error' : '']">
                                <label for="state_id" class="required">State</label>
                                <?php $states = array();
                                $states = DB::table('country_states')->where('country_id',101)->get();?>
                                <select name="state_id" id="state" class="control" v-validate="'required'">
                                    <option value="">Choose your state...</option>
                                    <?php foreach($states as $key => $state) { ?>
                                        <option <?php if($state->id == $user->state_id) echo 'selected'; ?> value="{{ $state->id }}">{{ $state->name }}</option>
                                    <?php } ?>
                                </select>
                                <span class="control-error" v-if="errors.has('state_id')">@{{ errors.first('state_id') }}</span>
                            </div>
                        </div>
                        <div class="form-field col-md-12">
                            <div class="control-group pass col-md-6" :class="[errors.has('city_id') ? 'has-error' : '']">
                                <label for="city" class="required">{{ __('shop::app.customer.signup-form.city') }}</label>
                                <?php $cities = array();
                                    $cities = DB::table('country_state_cities')->where('state_id',$user->state_id)->get();?>
                                    <select name="city_id" id="city" class="control" v-validate="'required'">
                                        <option value="">Choose your city...</option>
                                        <?php foreach($cities as $key => $city) { ?>
                                            <option <?php if($city->id == $user->city_id) echo 'selected'; ?> value="{{ $city->id }}">{{ $city->name }}</option>
                                        <?php } ?>
                                    </select>
                                <span class="control-error" v-if="errors.has('city_id')">@{{ errors.first('city_id') }}</span>
                            </div>

                            <div class="control-group cpass col-md-6" :class="[errors.has('pin_code') ? 'has-error' : '']">
                                <label for="pin_code" class="required">Pincode</label>
                                <input type="text" class="control" name="pin_code" data-vv-as="Pincode" value="{{ $user->pin_code }}">
                                <span class="control-error" v-if="errors.has('pin_code')">@{{ errors.first('pin_code') }}</span>
                            </div>
                        </div>

                        @if($user->role_id == 2)
                            <div class="form-field col-md-12">
                                <div class="control-group pass col-md-4" :class="[errors.has('company_name') ? 'has-error' : '']">
                                    <label for="company_name">Company Name</label>
                                    <input type="text" class="control" name="company_name" data-vv-as="&quot;Company Name&quot;" value="{{ $user->company_name }}">
                                    <span class="control-error" v-if="errors.has('company_name')">@{{ errors.first('company_name') }}</span>
                                </div>

                                <div class="control-group pass col-md-4" :class="[errors.has('gst_no') ? 'has-error' : '']">
                                    <label for="gst_no">GST Number</label>
                                    <input type="text" class="control" name="gst_no" data-vv-as="&quot;GST Number&quot;" value="{{ $user->gst_no }}">
                                    <span class="control-error" v-if="errors.has('gst_no')">@{{ errors.first('gst_no') }}</span>
                                </div>

                                <div class="control-group cpass col-md-4" :class="[errors.has('fda_licence_no') ? 'has-error' : '']">
                                    <label for="fda_licence_no">FDA Licence No</label>
                                    <input type="text" class="control" name="fda_licence_no" data-vv-as="&quot;FDA Licence No&quot;" value="{{ $user->fda_licence_no }}">
                                    <span class="control-error" v-if="errors.has('fda_licence_no')">@{{ errors.first('fda_licence_no') }}</span>
                                </div>                                                                                    
                            </div>
                        @endif

                        <div class="form-field col-md-12">

                            <div class="control-group pass col-md-4" :class="[errors.has('oldpassword') ? 'has-error' : '']">
                                <label for="password">{{ __('shop::app.customer.account.profile.opassword') }}</label>
                                <input type="password" class="control" name="oldpassword" autocomplete="false" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.opassword') }}&quot;" v-validate="'min:6'">
                                <span class="control-error" v-if="errors.has('oldpassword')">@{{ errors.first('oldpassword') }}</span>
                            </div>

                            <div class="control-group col-md-4 pass" :class="[errors.has('password') ? 'has-error' : '']">
                                <label for="password">New Password</label>

                                <input type="password" id="password" class="control" name="password" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.password') }}&quot;" v-validate="'min:6'">
                                <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span>
                            </div>

                            <div class="control-group col-md-4 cpass" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                                <label for="password">{{ __('shop::app.customer.account.profile.cpassword') }}</label>

                                <input type="password" id="password_confirmation" class="control" name="password_confirmation" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.cpassword') }}&quot;" v-validate="'min:6|confirmed:password'">
                                <span class="control-error" v-if="errors.has('password_confirmation')">@{{ errors.first('password_confirmation') }}</span>
                            </div>
                        </div>
                   
                    </div>
           

                
                
            </div>

                    <div class="button-group">
                        <input class="btn btn-primary btn-lg" type="submit" value="{{ __('shop::app.customer.account.profile.submit') }}">
                    </div>
               

            </form>
    </div>
            {!! view_render_event('bagisto.shop.customers.account.profile.edit.after', ['user' => $user]) !!}

        </div>

    </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function(){
    $('.content-container').css({
        'background': '#F1F3F6',
    });
$('body').on('change','#state', function(){
    
    var stateID = $(this).val(); 
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('customer/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
  });
});
</script>
@endpush
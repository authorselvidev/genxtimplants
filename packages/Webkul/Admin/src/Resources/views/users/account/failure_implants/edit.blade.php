@extends('shop::layouts.master')

@section('page_title')
    Return Implants
@stop

@section('content-wrapper')
<div class="profile-page">
        <div class="main-container-wrapper">

    <div class="account-content">

        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">
            <div class="account-head mb-10">
                <span class="back-icon"><a href="{{ route('admin.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
                <span class="account-heading">Edit Return Implants</span>                
            </div>
            <form method="post" id="form_failure" action="{{route('admin.failure.implants.update', $failed_implant->id )}}" files="true" enctype="multipart/form-data">
                @csrf
                <div class="form-fieldss failure-form col-md-12">
                    <section>
                    <div class="wizard">
                        <form role="form">                             
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel" id="step1">                                    
                                    <hr>
                                    <div class="form-field col-md-12">
                                        <div class="control-group col-md-126 pass cpass">
                                            <label for="email" class="required">Email</label>
                                            <input type="email" name="email" class="form-control form-required" value="{{ $user_details->email }}" placeholder="Your answer">
                                            
                                        </div>
                                    </div>
                                    <div class="form-field col-md-12">
                                            <div class="control-group">
                                                <label for="first_name" class="required">Doctor's Name</label>
                                                <input type="text" name="doctor_name" class="form-control form-required" value="{{ $user_details->first_name.' '.$user_details->last_name }}" placeholder="Your answer">
                                                
                                            </div>
                                    </div>
                                    <div class="form-field col-md-12">
                                            <div class="control-group">
                                                <label for="last_name" class="required">Mobile Number</label>
                                                <input type="text" name="mobile_number" class="form-control form-required" value="{{ $user_details->phone_number }}">
                                                
                                            </div>
                                    </div>
                                    <div class="form-field col-md-12">
                                            <div class="control-group">
                                                <label for="last_name" class="required">Detailed Clinic Address</label>
                                                <textarea class="form-control form-required " name="clinic_address" rows="4" placeholder="Your answer">{{ $user_details->clinic_address }}</textarea>
                                                
                                            </div>
                                    </div>
                                    <div class="order_type">
                                        <label style="margin-left: 15px;">Order Mode:</label>
                                        <div class="form-field col-md-12" style="margin-bottom: 10px;">
                                            <input type="radio" name="order_type" value="on" id="online" @if($failed_implant->order_type == "on") checked @endif> <label for="online">Online</label>
                                            <input type="radio" name="order_type" value="off" id="offline" @if($failed_implant->order_type == "off") checked @endif> <label for="offline">Offline</label>
                                        </div>
                                        <div class="form-field col-md-12 online_order @if($failed_implant->order_type == "on"){{'activeOrder'}}@endif" @if($failed_implant->order_type == "off") style="display:none" @endif>
                                            <div class="row">
                                                <div class="return_container_online ele_container">

                                                </div>
                                            </div>
                                            <div class="return_incrementer_online">
                                                <span class="inc_return_online btn btn-sm btn-primary">Add</span>
                                            </div>

                                            <div class="row">
                                                <div class="replacement_container_online ele_container">

                                                </div>
                                            </div>
                                            <div class="replacement_incrementer_online">
                                                <span class="inc_replace_online btn btn-sm btn-primary">Add</span>
                                            </div>
                                        </div>

                                        <div class="form-field col-md-12 offline_order @if($failed_implant->order_type == "off"){{'activeOrder'}}@endif" @if($failed_implant->order_type == "on") style="display:none" @endif>
                                            <div class="row">
                                                <div class="return_container_offline ele_container"></div>
                                            </div>
                                            <div class="return_incrementer_offline">
                                                <span class="inc_return_offline btn btn-sm btn-primary">Add</span>
                                            </div>
                                            <div class="row">
                                                <div class="replacement_container_offline ele_container"></div>
                                            </div>
                                            <div class="replacement_incrementer_offline">
                                                <span class="inc_replace_offline btn btn-sm btn-primary">Add</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-field col-md-12">
                                        <div class="control-group cpass">
                                            <label for="last_name" class="required">Reason for Replacement</label>
                                            <input type="text" name="replacement_reason" class="form-control form-required" value="{{$failed_implant->replacement_reason}}" placeholder="Your answer">
                                            
                                        </div>
                                    </div>

                                    <div class="form-field col-md-12">
                                        <div class="control-group cpass">
                                            <label for="last_name" class="required">Item Status</label>                                            
                                            <label><input type="radio" name="item_status" value="0" @if($failed_implant->item_status==0){{'checked'}}@endif>Used</label>
                                            <label><input type="radio" name="item_status" value="1" @if($failed_implant->item_status==1){{'checked'}}@endif>Unused</label>
                                        </div>
                                    </div>

                                    <div class="form-field col-md-12">
                                        <div class="control-group cpass">
                                            <label for="last_name">GenXT Implant Product Code(or label)</label>
                                            <input type="text" name="implant_code" class="form-control" value="{{$failed_implant->implant_code}}" placeholder="Your answer">
                                            
                                        </div>
                                    </div>
                                    <div class="form-field col-md-12">
                                        <div class="control-group cpass">
                                            <label for="last_name">LOT #</label>
                                            <input type="text" name="lot" id="lot" class="form-control" value="{{$failed_implant->lot}}" placeholder="Your answer">
                                            
                                        </div>
                                    </div>                                            
                                    <input type="hidden" class="form1-validation-count" value="0">
                                    <ul class="list-inline pull-right">
                                        <li>
                                            <button type="button" class="btn btn-primary btn-info-full submit_btn">
                                                <span>Submit</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                </div>
            </form>
         </div>

        </div>

    </div>
</div>    
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/fSelect.css')}}">
    <script type="text/javascript" src="{{asset('themes/default/assets/js/fSelect.js')}}"></script>    
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css"/>
    <script type="text/javascript">
        $(document).ready(function () {
            function validateForm1(check=null){

                $('.error-msg').remove();
                var check_form_valid=true;
                
                $(`#step1 .form-required,
                    .activeOrder select.validate,
                    .activeOrder input.validate,
                    .activeOrder textarea.validate`).each(function() {
                    if ($(this).val().length<=0){
                        $(this).after('<span class="error-msg text-danger">This field is required!</span>');
                        check_form_valid = false;
                    }else{
                        $(this).parent().find('.error-msg').text('');

                        check_form_valid = ($('.activeOrder input[max]').filter(function () {
                            if(stat=parseInt($(this).val())>parseInt($(this).attr('max'))){
                                $(this).after(`<span class="error-msg text-danger">Maximum allowed quantity ${$(this).attr('max')}!</span>`);                                
                                check_form_valid = false;
                            }
                            return stat;
                        }).length!=0)?false:check_form_valid;

                        check_form_valid = ($('.activeOrder input[min]').filter(function () {
                            if(stat=parseInt($(this).val())=='0'){                                
                                $(this).after(`<span class="error-msg text-danger">Zero not allowed!</span>`);
                                check_form_valid = false;
                            }
                            return stat;
                        }).length!=0)?false:check_form_valid;
                    }
                });                                                

                if (check_form_valid==true&& check==null) {
                    $('button[type="button"]').css('pointer-events','none');
                    $('button[type="button"].first_page').animate({
                        width:'+=20px'
                    },100, function(){
                        $(this).append('<i class="spinning" style="display:none"></i>');
                        $('.spinning').fadeIn(100);
                    });
                    $("#form_failure").submit();
                }             

                return check_form_valid;
            }                     

            $('body').on('click', '.submit_btn', function(e){
                if(!validateForm1()){
                    e.preventDefault();
                }
            }); 

            $('.content-container').css({
                'background': '#F1F3F6',
            });                        

            $('form').find('input[name=order_type]').change(function(){
                const stat = $(this).val();
                const order_on = $('.order_type .online_order');
                const order_off = $('.order_type .offline_order');
                $('#replace_qty, #replace_offline_qty').val('');
                if(stat=='on'){
                    order_on.addClass('activeOrder').show();
                    order_off.removeClass('activeOrder').hide();
                }else if(stat=='off'){
                    order_on.removeClass('activeOrder').hide();
                    order_off.addClass('activeOrder').show();
                }
            });
            
            let count_return =1;
            let count_replace =1;

            function addReturnItemOnline(data=null){

                let html = $(`<div class="return_item remove_ele" style="clear:both">
                                <div class="col-md-4 pright">
                                    <div class="control-group order_control">
                                        <label for="order_item" class="required">Order</label>
                                        <select class='form-control order_item validate' name="return_item[${count_return}][order]" id="order_item_${count_return}" placeholder="Select a Order...">
                                            <option value="">Select a Order...</option>
                                            @foreach($orders as $order)
                                                <option value="{{$order->id}}" data-data='{"order":"<?php echo($order->id) ?>" }'>Order #{{$order->id}}</option>
                                            @endforeach
                                        </select>                                        
                                    </div>
                                </div>

                                <div class="col-md-4 pright">
                                    <div class="control-group order_control order_item">
                                        <label for="order_item" class="required">Return Product</label>
                                        <select class='form-control order_item validate' name="return_item[${count_return}][item]" id="order_product_${count_return}" placeholder="Select a Order..." ${ (!data)?'disabled':'' }>
                                            <option value="">Select a Order...</option>
                                            @foreach($orders as $order)
                                                @foreach($order->items as $item)
                                                    <?php
                                                        $invoice_items_id = DB::table('invoice_items')->where('order_item_id', DB::table('order_items')->where('id', $item->id)->first()->id)->first();
                                                        $lot_items = ($invoice_items_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_items_id->id)->addSelect('lot_number')->get()):[];
                                                        $lot_string = [];                                                        
                                                        if(count($lot_items)>0){
                                                            foreach($lot_items as $lot){
                                                                array_push($lot_string, $lot->lot_number);
                                                            }
                                                        }
                                                        $lot_string = join($lot_string,', ');                                                        
                                                    ?>
                                                    <option data-count="{{$item->qty_ordered}}" data-lot="{{$lot_string}}"  data-order="{{$order->id}}" value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 qty_count${count_return} pright">
                                    <div class="control-group">
                                        <label for="online_qty${count_return}" class="required">Return Quantity</label>
                                        <input type="number" min="1" name="return_item[${count_return}][qty]" id="online_qty${count_return}" class="form-control form-active validate" value="${ ((data)?data.quantity:'')   }" placeholder="Qty">
                                    </div>
                                </div>
                                <span class="text-danger remove_btn">&times;</span>
                            </div> `);

                if(data){
                    html.find(`#order_item_${count_return}>option[value="${data.order}"]`).prop('selected', true);
                    html.find(`#order_product_${count_return}>option[value="${data.item}"]`).prop('selected', true);                    
                }

                $('.return_container_online').append(html);

                $(`#order_item_${count_return}`).fSelect(Object.assign({
                    placeholder: "Select Order"
                }, {
                    numDisplayed: 3,
                    overflowText: '{n} selected',
                    noResultsText: 'No results found',
                    searchText: 'Search',
                    showSearch: true,
                    selectAll: false,
                    functionSelect : function(){
                        const option = $(this).data('value');
                        let wrap = $(this).closest('.return_item').find('.order_item .fs-wrap');
                        wrap.find('.fs-option').hide();
                        if(!option){
                            wrap.toggleClass('fs-disabled', true);
                            wrap.find('.fs-option').toggleClass('selected', false);
                            select= wrap.find('select').val([]);                            
                            select.prop('disabled', true);
                            select.fSelect('reloadDropdownLabel').change();
                        }else{
                            wrap.find('.fs-option').toggleClass('selected', false);
                            wrap.toggleClass('fs-disabled', false);
                            select= wrap.find('select').val([]);
                            select.prop('disabled', false);
                            select.fSelect('reloadDropdownLabel').change();
                            wrap.find(`.fs-option[data-order="${option}"]`).show();
                        }      
                        
                        $(document).trigger('fs:changed', wrap);
                    }
                }));

                $(`#order_product_${count_return}`).fSelect(Object.assign({
                    placeholder: "Select Order"
                }, {
                    numDisplayed: 3,
                    overflowText: '{n} selected',
                    noResultsText: 'No results found',
                    searchText: 'Search',
                    showSearch: true,
                    selectAll: false,
                }));

                count_return++;
            }

            function addReplaceItemOnline(data=null){
                console.log(data);
                let html = $(`<div class="replace_item remove_ele" style="clear:both">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="order_item" class="required">Replacement Item</label>
                                        <select class='form-control validate' name="replace_item[${count_replace}][item]" id="replacement_item_online${count_replace}" placeholder="Select a Item...">
                                            <option value="">Select a Item...</option>
                                            @foreach($products as $key => $product)
                                                <?php $product_ordered_inventory = DB::table('product_ordered_inventories')->where('product_id',$product->product_id)->first();
                                                      $product_ordered_qty = (isset($product_ordered_inventory)) ? $product_ordered_inventory->qty : 0;
                                                      $product_available_qty = $product->qty - $product_ordered_qty; ?>

                                                @if($product_available_qty > 0)
                                                    <option data-data='{"product_id":<?php echo($product->product_id) ?>, "qty_available": <?php echo($product_available_qty) ?>}' value="{{$product->product_id}}">{{$product->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 qty_count">
                                    <div class="control-group">
                                        <label for="replace_qty${count_replace}" class="required">Replace Quantity</label>
                                        <input type="number" min="1" name="replace_item[${count_replace}][qty]" id="replace_qty${count_replace}" class="form-control form-active validate" value="${((data)? data.quantity:'')}" placeholder="Qty">
                                    </div>
                                </div>
                                <span class="text-danger remove_btn">&times;</span>
                            </div>`);                

                if(data){
                    html.find(`#replacement_item_online${count_replace}>option[value="${data.item}"]`).prop('selected', true);
                }

                $('.replacement_container_online').append(html);


                $(`#replacement_item_online${count_replace}`).fSelect(Object.assign({
                    placeholder: "Select Replace Item"
                }, {
                    numDisplayed: 3,
                    overflowText: '{n} selected',
                    noResultsText: 'No results found',
                    searchText: 'Search',
                    showSearch: true,
                    selectAll: false,
                }));
                
                count_replace++;
            }

            let count_return_off =1;
            let count_replace_off =1;

            function addReturnItemOffline(data=null){
                let html = `<div class="return_item_offline remove_ele" style="clear:both">
                                <div class="col-md-4 pright">
                                    <div class="control-group">
                                        <label for="order_number${count_return_off}" class="required">Offline Order number:</label>
                                        <input type="text" name="replace_item_offline[${count_return_off}][order]" id="order_number${count_return_off}" class="form-control form-active" value="${ ((data)? data.order:'')  }" placeholder="Order number">
                                    </div>
                                </div>

                                <div class="col-md-4 qty_count">
                                    <div class="control-group">
                                        <label for="product_name${count_return_off}" class="required">Return Product:</label>
                                        <input type="text" name="replace_item_offline[${count_return_off}][item]" id="product_name${count_return_off}" class="form-control form-active validate" value="${  ((data)? data.item:'')}"  placeholder="Product name">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="control-group">
                                        <label for="replace_offline_qty${count_return_off}" class="required">Return Quantity</label>
                                        <input type="number" min="1" name="replace_item_offline[${count_return_off}][qty]" id="replace_offline_qty${count_return_off}" class="form-control form-active validate" value="${ ((data)? data.quantity:'')  }" placeholder="Qty">
                                    </div>
                                </div>
                                <span class="text-danger remove_btn">&times;</span>
                            </div>`;
                $('.return_container_offline').append(html);
                count_return_off++;
            }

            function addReplaceItemOffline(data=null){
                let html = $(`<div class="return_item_offline remove_ele" style="clear:both">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label for="order_item" class="required">Replacement Item</label>
                                        <select class='form-control validate' name="return_item_offline[${count_replace_off}][item]" id="replacement_item_offline${count_replace_off}" placeholder="Select a Item...">
                                            <option value="">Select a Item...</option>
                                            @foreach($products as $key => $product)
                                                <?php
                                                    $product_ordered_inventory = DB::table('product_ordered_inventories')->where('product_id',$product->product_id)->first();
                                                    $product_ordered_qty = (isset($product_ordered_inventory)) ? $product_ordered_inventory->qty : 0;
                                                    $product_available_qty = $product->qty - $product_ordered_qty;
                                                ?>
                                                @if($product_available_qty > 0)
                                                    <option data-data='{"product_id":<?php echo($product->product_id) ?>, "qty_available": <?php echo($product_available_qty) ?>}' value="{{$product->product_id}}">{{$product->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 qty_count">
                                    <div class="control-group">
                                        <label for="replace_qty${count_replace}" class="required">Replace Quantity</label>
                                        <input type="number" min="1" name="return_item_offline[${count_replace_off}][qty]" id="replace_qty${count_replace}" class="form-control form-active validate" value="${ ((data)? data.quantity:'') }" placeholder="Qty">
                                    </div>
                                </div>
                                <span class="text-danger remove_btn">&times;</span>
                            </div>`);
                
                if(data){
                    html.find(`#replacement_item_offline${count_replace_off}>option[value="${data.item}"]`).prop('selected', true);
                }
                
                $('.replacement_container_offline').append(html);

                $(`#replacement_item_offline${count_replace_off}`).fSelect(Object.assign({
                    placeholder: "Select Replace Item"
                }, {
                    numDisplayed: 3,
                    overflowText: '{n} selected',
                    noResultsText: 'No results found',
                    searchText: 'Search',
                    showSearch: true,
                    selectAll: false,
                }));

                count_replace_off++;
            }            

            $('body').on('click', '.inc_return_online', function(){
                addReturnItemOnline();
            });

            $('body').on('click', '.inc_replace_online', function(){
                addReplaceItemOnline();
            });

            $('body').on('click', '.inc_return_offline', function(){
                addReturnItemOffline();
            });

            $('body').on('click', '.inc_replace_offline', function(){
                addReplaceItemOffline();
            });

            $('body').on('click','.remove_btn', function(){
                let container = $(this).closest('.ele_container');
                $(this).closest('.remove_ele').remove();               
                
                if(container.hasClass('return_container_online')){
                    if(container.find('.remove_ele').length==0) addReturnItemOnline();
                }else if(container.hasClass('replacement_container_online')){
                    if(container.find('.remove_ele').length==0) addReplaceItemOnline();
                }else if(container.hasClass('return_container_offline')){
                    if(container.find('.remove_ele').length==0) addReturnItemOffline();
                }else if(container.hasClass('replacement_container_offline')){
                    if(container.find('.remove_ele').length==0) addReplaceItemOffline();
                }

            });

            let json_data = JSON.parse(`{!! json_encode($return_implants_items) !!}`);
            let return_imp = json_data.filter((ele) => ele.type==1);
            let replace_imp = json_data.filter((ele) => ele.type==2);
            let type = "{{$failed_implant->order_type}}";
            if(type=='on'){
                return_imp.forEach(function(ele){
                    addReturnItemOnline(ele);                    
                });
                replace_imp.forEach(function(ele){
                    addReplaceItemOnline(ele);
                });
                addReturnItemOffline();
                addReplaceItemOffline();
            }else if(type=='off'){
                return_imp.forEach(function(ele){
                    addReturnItemOffline(ele);
                });
                replace_imp.forEach(function(ele){
                    addReplaceItemOffline(ele);
                });
                addReturnItemOnline();
                addReplaceItemOnline();
            }

            
        });
    </script>
    <style type="text/css">
        .order_item, .selectize-input{
            width: 100% !important;
        }

        .remove_btn{
            font-size: 2rem;
            position: absolute;
            right: 5px;
            top:5px;
            cursor: pointer;
        }        

        .remove_ele{
            position: relative;
        }

        .order_control{
            margin-bottom: 0px;
        }

        .active .round-tab{
            color: #0995CD;
        }

		button.third_page{
            display: flex !important;
            align-items: center !important;
            overflow: hidden;
        }

        .spinning{
            margin-left: 10px;
            display:inline-block;
            border: 2px solid #f3f3f3;
            border-radius: 50%;
            border-top: 2px solid #3498db;
            padding: 6px;
            -webkit-animation: loadingSpin 1.2s linear infinite;
            animation: loadingSpin 1.2s linear infinite;
        }

        .pright{
            padding-right:20px !important;
        }

        .imgThumb{
            width: 100px;
            margin: 10px 10px 0;
            cursor: pointer;
        }

        .error-msg{
            background-color:unset !important;
            display: inline-block;
            margin-top: 4px;
        }

        .return_incrementer_online, .replacement_incrementer_online,
        .return_incrementer_offline, .replacement_incrementer_offline  {
            display: flex;
            justify-content: flex-end;
        }

        @-webkit-keyframes loadingSpin {
            0% { -webkit-transform: rotate(0deg); } 
            100% { -webkit-transform: rotate(360deg); }
        }


        @keyframes loadingSpin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        @media(max-width: 768px){
            .nav-tabs a{
                text-align: center;
                border:1px solid #fff !important;
            }

            .active .round-tab{
                border-bottom: 1px solid #0995CD;
            }
        }
    </style>
@endpush

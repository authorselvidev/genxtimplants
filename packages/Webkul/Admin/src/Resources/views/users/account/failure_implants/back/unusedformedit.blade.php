@extends('shop::layouts.master')

@section('page_title')
    Return Implants
@stop

@section('content-wrapper')
<div class="profile-page">
        <div class="main-container-wrapper">

    <div class="account-content">

        @include('shop::customers.account.partials.sidemenu')

        <div class="account-layout">
            <div class="account-head mb-10">
                <span class="back-icon"><a href="{{ route('customer.account.index') }}"><i class="icon icon-menu-back"></i></a></span>
                <span class="account-heading">Unopened Return Implants Edit</span>
                <span></span>
            </div>
            <form method="post" id="form_failure" action="{{route('admin.failed-implants.unusedupdate', $failed_implant->id)}}" files="true" enctype="multipart/form-data" >
            @csrf
                <div class="form-fieldss failure-form col-md-12">
                    <section>
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="disabled active" style="pointer-events: none;">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                        <span class="round-tab">
                                            Personal Details
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <form role="form">
                            <input type="hidden" name="item_status"value="1">
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <h3>Step 1</h3>
                                    <hr>
                                            <div class="form-field col-md-12">
                                                <div class="control-group col-md-126 pass cpass">
                                                    <label for="email" class="required">Email</label>
                                                    <input type="email" name="email" class="form-control form-required" value="{{ $user_details->email }}" placeholder="Your answer">
                                                    <span class="error-msg text-danger"></span>
                                                </div>
                                            </div>
                                            <div class="form-field col-md-12">
                                                    <div class="control-group">
                                                        <label for="first_name" class="required">Doctor's Name</label>
                                                        <input type="text" name="doctor_name" class="form-control form-required" value="{{ $user_details->first_name.' '.$user_details->last_name }}" placeholder="Your answer">
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>
                                            <div class="form-field col-md-12">
                                                    <div class="control-group">
                                                        <label for="last_name" class="required">Mobile Number</label>
                                                        <input type="text" name="mobile_number" class="form-control form-required" value="{{ $user_details->phone_number }}">
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>
                                            <div class="form-field col-md-12">
                                                    <div class="control-group">
                                                        <label for="last_name" class="required">Detailed Clinic Address</label>
                                                        <textarea class="form-control form-required" name="clinic_address" rows="4" placeholder="Your answer">{{ $user_details->clinic_address }}</textarea>
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>
                                            <div class="order_type">
                                                <label style="margin-left: 15px;">Order Mode:</label>
                                                <div class="form-field col-md-12" style="margin-bottom: 10px;">
                                                    <input type="radio" name="order_type" value="on" id="online" @if($failed_implant->order_type == "on") checked @endif> <label for="online">Online</label>
                                                    <input type="radio" name="order_type" value="off" id="offline" @if($failed_implant->order_type == "off") checked @endif> <label for="offline">Offline</label>
                                                </div>
                                                @if($failed_implant->order_type == "on")
                                                    <div class="form-field col-md-12 online_order activeOrder">
                                                @else
                                                    <div class="form-field col-md-12 online_order" style="display: none">
                                                @endif
                                                        <div class="col-md-6 pright" style="padding: 0px;">
                                                            <div class="control-group cpass">
                                                                <label for="order_item" class="required">Order Item</label>
                                                                <select class='form-control' name="order_item" id="order_item" placeholder="Select a Order...">
                                                                    <option value="">Select a Order...</option>
                                                                    @foreach($orders as $order)
                                                                        @foreach($order->items as $item)

                                                                            <?php
                                                                                $invoice_items_id = DB::table('invoice_items')->where('order_item_id', DB::table('order_items')->where('id', $item->id)->first()->id)->first();
                                                                                $lot_items = ($invoice_items_id)?(DB::table('lot_items')->where('invoice_items_id', $invoice_items_id->id)->addSelect('lot_number')->get()):[];
                                                                                $lot_string = [];

                                                                                if(count($lot_items)>0){
                                                                                    foreach($lot_items as $lot)
                                                                                        array_push($lot_string, $lot->lot_number);
                                                                                }
                                                                                $lot_string = join($lot_string,', ');
                                                                            ?>
                                                                            <option data-data='{"count":<?php echo($item->qty_ordered) ?>, "lot_number": "<?php echo($lot_string) ?>" }'  value="{{$item->id}}" @if($item->id == $failed_implant->order_item_id) selected @endif>Order #{{$order->id}} - {{$item->name}}</option>
                                                                        @endforeach
                                                                    @endforeach
                                                                </select>
                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6" style="padding: 0px;">
                                                            <div class="control-group cpass">
                                                                <label for="order_item" class="required">Replacement Item</label>
                                                                <select class='form-control' name="replacement_item_online" id="replacement_item_online" placeholder="Select a Item...">
                                                                    <option value="">Select a Item...</option>
                                                                    @foreach($products as $key => $product)
                                                                        <?php
                                                                            $product_ordered_inventory = DB::table('product_ordered_inventories')->where('product_id',$product->product_id)->first();
                                                                            $product_ordered_qty = (isset($product_ordered_inventory)) ? $product_ordered_inventory->qty : 0;
                                                                            $product_available_qty = $product->qty - $product_ordered_qty;
                                                                        ?>
                                                                        @if($product_available_qty > 0)
                                                                            <option data-data='{"product_id":<?php echo($product->product_id) ?>, "qty_available": <?php echo($product_available_qty) ?>}' value="{{$product->product_id}}" @if($product->product_id == $failed_implant->replacement_product_id) selected @endif>{{$product->name}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>

                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 qty_count pright" style="padding: 0px;">
                                                            <div class="control-group">
                                                                <label for="online_qty" class="required">Order Quantity</label>
                                                                <input type="number" min="1" name="order_qty" id="online_qty" class="form-control form-active" value="@if($failed_implant->order_type == 'on'){{$failed_implant->qty}}@endif" placeholder="Qty" readonly>
                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 qty_count" style="padding: 0px;">
                                                            <div class="control-group">
                                                                <label for="replace_qty" class="required">Replace Quantity</label>
                                                                <input type="number" min="1" name="replace_qty" id="replace_qty" class="form-control form-active" value="{{$failed_implant->replace_qty}}" placeholder="Qty">
                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                        </div>
                                                </div>
                                                @if($failed_implant->order_type == "off")
                                                    <div class="form-field col-md-12 offline_order activeOrder">
                                                @else
                                                    <div class="form-field col-md-12 offline_order" style="display: none">
                                                @endif
                                                    <div class="col-md-6 pright" style="padding: 0px; ">
                                                        <div class="control-group">
                                                            <label for="order_number" class="required">Order number:</label>
                                                            <input type="text" name="order_number" class="form-control form-active" placeholder="Order number" value="@if($failed_implant->order_type == 'off'){{$failed_implant->order_number}}@endif">
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 qty_count" style="padding: 0px;">
                                                        <div class="control-group">
                                                            <label for="product_name" class="required">Product Name:</label>
                                                            <input type="text" name="product_name" id="product_name" class="form-control form-active" placeholder="product_name" value="@if($failed_implant->order_type == 'off'){{$failed_implant->order_item}}@endif">
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 pright" style="padding: 0px;">
                                                        <div class="control-group">
                                                            <label for="order_item" class="required">Replacement Item</label>
                                                            <select class='form-control' name="replacement_item_offline" id="replacement_item_offline" placeholder="Select a Item...">
                                                                <option value="">Select a Item...</option>
                                                                @foreach($products as $key => $product)
                                                                    <?php
                                                                        $product_ordered_inventory = DB::table('product_ordered_inventories')->where('product_id',$product->product_id)->first();
                                                                        $product_ordered_qty = (isset($product_ordered_inventory)) ? $product_ordered_inventory->qty : 0;
                                                                        $product_available_qty = $product->qty - $product_ordered_qty;
                                                                    ?>
                                                                    @if($product_available_qty > 0)
                                                                        <option data-data='{"product_id":<?php echo($product->product_id) ?>, "qty_available": <?php echo($product_available_qty) ?>}' value="{{$product->product_id}}" @if($product->product_id == $failed_implant->replacement_product_id) selected @endif>{{$product->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 qty_count" style="padding: 0px;">
                                                        <div class="control-group">
                                                            <label for="offline_qty" class="required">Order Quantity</label>
                                                            <input type="number" min="1" name="order_qty_offline" id="offline_qty" class="form-control form-active" value="@if($failed_implant->order_type == 'off'){{$failed_implant->qty}}@endif" placeholder="Qty">
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>                                                       
                                                    
                                                    <div class="col-md-12" style="padding: 0px;clear:both">
                                                        <div class="control-group">
                                                            <label for="order_item" class="required">Replace Quantity</label>
                                                            <input type="number" min="1" name="replace_offline_qty" id="replace_offline_qty" class="form-control form-active" value="{{$failed_implant->replace_offline_qty}}" placeholder="Qty">
                                                            <span class="error-msg text-danger"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-field col-md-12">
                                                <div class="control-group cpass">
                                                    <label for="last_name" class="required">Reason for Replacement</label>
                                                    <input type="text" name="replacement_reason" class="form-control form-required" value="{{$failed_implant->replacement_reason}}" placeholder="Your answer">
                                                    <span class="error-msg text-danger"></span>
                                                </div>
                                            </div>

                                            <div class="form-field col-md-12">
                                                    <div class="control-group cpass">
                                                        <label for="last_name" class="required">GenXT Implant Product Code(or label)</label>
                                                        <input type="text" name="implant_code" class="form-control form-required" value="{{$failed_implant->implant_code}}" placeholder="Your answer">
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>

                                            <div class="form-field col-md-12">
                                                    <div class="control-group cpass">
                                                        <label for="last_name" class="required">LOT #</label>
                                                        <input type="text" name="lot" id="lot" class="form-control form-required" value="{{$failed_implant->lot}}" placeholder="Your answer">
                                                        <span class="error-msg text-danger"></span>
                                                    </div>
                                            </div>
                                            <input type="hidden" class="form1-validation-count" value="0">
                                            <ul class="list-inline pull-right">
                                                <li><button type="button" class="btn btn-primary btn-info-full next-step first_page">
                                                    <span>Submit</span>
                                                    </button>
                                                </li>
                                            </ul>
                                </div>                                
                                    
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </section>
                </div>
            </form>
         </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function validateForm1(check=null) {
        var check_form_valid=true;
      $('#step1 .form-required, .activeOrder #order_item, .activeOrder .form-active, .activeOrder #replacement_item_online, .activeOrder #replacement_item_offline ').each(function(response,key) {
        if ($(this).val() === ''){
            $(this).parent().find('.error-msg').text('This field is required');
        }else{
            $(this).parent().find('.error-msg').text('');
            check_form_valid = ($('.activeOrder input[max]').filter(function () {
                if(stat=parseInt($(this).val())>parseInt($(this).attr('max'))){
                    $(this).parent().find('.error-msg').text(`Maximum allowed quantity ${$(this).attr('max')}`);
                }
                return stat;
            }).length!=0)?false:check_form_valid;

            check_form_valid = ($('.activeOrder input[min]').filter(function () {
                if(stat=parseInt($(this).val())=='0'){
                    $(this).parent().find('.error-msg').text(`Zero not allowed`);
                }
                return stat;
            }).length!=0)?false:check_form_valid;
        }
      });

      check_form_valid = ($('#step1 .form-required, .activeOrder #order_item, .activeOrder .form-active, .activeOrder #replacement_item_online, .activeOrder #replacement_item_offline').filter(function (){return $.trim($(this).val()).length == 0}).length!=0)?false:check_form_valid;
        if (check_form_valid==true&& check==null) {
            $('button[type="button"]').css('pointer-events','none');
            $('button[type="button"].first_page').animate({
                width:'+=20px'
            },100, function(){
                $(this).append('<i class="spinning" style="display:none"></i>');
                $('.spinning').fadeIn(100);
            });
            $("#form_failure").submit();
        }
        return check_form_valid;
    }

    $(document).ready(function () {
        $(document).on('click', '.next-step', function(event) {
            
            var current_tab=$(this).attr('class').split(' ').pop();
            if (current_tab=="first_page") {
                validateForm1();
            }
        });

        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

</script>
@endsection




@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css"/>
    <script type="text/javascript">
          $(document).ready(function () {
            $('.content-container').css({
                'background': '#F1F3F6',
            });
            $('#order_item').selectize({
                sortField: 'text',

                render: {
                    option: function (data, escape) {
                        return `<div class="option" data-count="${escape(data.count)}">${escape(data.text)}</div>`;
                    }
                },

                onChange: function(val) {
                    var data = this.options[val];
                    $('input#lot').val(data.lot_number);                    
                    let qty = $('.qty_count');
                    qty.find('input#online_qty').attr('max',data.count).val(data.count);
                    $('#replace_qty').val(data.count);
                    qty.show();
                }
            });

            $('#replacement_item_online').selectize({
                sortField: 'text',
                render: {
                    option: function (data, escape) {
                        return `<div class="option" data-product_id="${escape(data.product_id)}" data-qty_available="${data.qty_available}">${escape(data.text)}</div>`;
                    }
                },
            });


            $('#replacement_item_offline').selectize({
                sortField: 'text',
                render: {
                    option: function (data, escape) {
                        return `<div class="option" data-product_id="${escape(data.product_id)}" data-qty_available="${data.qty_available}">${escape(data.text)}</div>`;
                    }
                },
            });

            $('form').find('input[name=order_type]').change(function(){
                const stat= $(this).val();
                const order_on = $('.order_type .online_order');
                const order_off = $('.order_type .offline_order');
                $('#replace_qty, #replace_offline_qty').val('');
                if(stat=='on'){
                    order_on.addClass('activeOrder').show();
                    order_off.removeClass('activeOrder').hide();
                }else if(stat=='off'){
                    order_on.removeClass('activeOrder').hide();
                    order_off.addClass('activeOrder').show();
                }
            });            

            $('.img-thumb').on('change',function(event) {
                let input = event.target;
                let reader = new FileReader();
                reader.onload = function(){
                    let dataURL = reader.result;
                    let output = $(`<img class='imgThumb'>`).hide();
                    output.attr('src', dataURL);
                    $(input).next('.imgThumb').remove();
                    $(input).after(output);
                    output.fadeIn();
                    output.addClass('img-enlargeable').click(function(){
                        var src = $(this).attr('src');
                        var modal;
                        function removeModal(){ modal.remove(); $('body').off('keyup.modal-close'); }
                        modal = $('<div>').css({
                            background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
                            backgroundSize: 'contain',
                            width:'100%', height:'100%',
                            position:'fixed',
                            zIndex:'10000',
                            top:'0', left:'0',
                            cursor: 'pointer'
                        }).click(function(){
                            removeModal();
                        }).appendTo('body');
                        $('body').on('keyup.modal-close', function(e){
                          if(e.key==='Escape'){
                            removeModal();
                        }
                        });
                    });
                };
                reader.readAsDataURL(input.files[0]);
            });

             $('body').on('change', '#replace_qty', function(e){
                let order_qty = parseInt($('#online_qty').val());
                let replace_qty = parseInt($(this).val());
                if(order_qty < replace_qty) {
                    alert('Replacement Quantity not exceed order quantity');
                    $(this).val(order_qty);
                    return false;                    
                }                
            });

            $('body').on('change', '#replace_offline_qty', function(e){
                let order_qty = parseInt($('#offline_qty').val());
                let replace_qty = parseInt($(this).val());
                if(order_qty < replace_qty) {
                    alert('Replacement Quantity not exceed order quantity');
                    $(this).val(order_qty);
                    return false;                    
                }                
            });     
        });
    </script>
    <style type="text/css">
        #order_item, .selectize-input{
            width: 100% !important;
        }

        .order_control{
            margin-bottom: 0px;
        }

        .active .round-tab{
            color: #0995CD;
        }

		button.third_page{
            display: flex !important;
            align-items: center !important;
            overflow: hidden;
        }

        .spinning{
            margin-left: 10px;
            display:inline-block;
            border: 2px solid #f3f3f3;
            border-radius: 50%;
            border-top: 2px solid #3498db;
            padding: 6px;
            -webkit-animation: loadingSpin 1.2s linear infinite;
            animation: loadingSpin 1.2s linear infinite;
        }

        .pright{
            padding-right:20px !important;
        }

        .imgThumb{
            width: 100px;
            margin: 10px 10px 0;
            cursor: pointer;
        }

        @-webkit-keyframes loadingSpin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }


        @keyframes loadingSpin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        @media(max-width: 768px){
            .nav-tabs a{
                text-align: center;
                border:1px solid #fff !important;
            }

            .active .round-tab{
                border-bottom: 1px solid #0995CD;
            }
        }
    </style>
@endpush

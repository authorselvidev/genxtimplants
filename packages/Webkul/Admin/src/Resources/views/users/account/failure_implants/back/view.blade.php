@extends('shop::layouts.master')

@section('page_title')
    Return Implants
@stop

@section('content-wrapper')
<div class="profile-page">
    <div class="main-container-wrapper">

        <div class="account-content">

            @include('shop::customers.account.partials.sidemenu')

            <div class="account-layout">
                <div class="account-head mb-10">
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                    <span class="account-heading">Return Implant</span>
                    @if($failed_implant->status == 'Approved' || $failed_implant->status == 'Processing' )
                        <a href="{{route('admin.failed-implants.print', $failed_implant->id)}}" class="btn btn-primary" style="float: right;">Print</a>
                        <br>
                    @endif
                </div>

                <?php
                    $status = array('0'=>'False', '1'=>'True');
                ?>

                <tabs>
                    <tab name="Order Details" :selected="true">
                        <div class="table_content">
                            <table>
                                <tr>
                                    <td class="title">Return Implant status</td>
                                    <td class="text-center">:</td>
                                    <td class="value">
                                        {{$failed_implant->status}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="title">GenXT Comments</td>
                                    <td class="text-center">:</td>
                                    <td class="value">
                                        <span>{{$failed_implant->comments}}</span>
                                    </td>
                                </tr>

                                @if($failed_implant->order_type =='off')
                                  <tr>
                                      <td class="title">Order number</td>
                                      <td class="text-center">:</td>
                                      <td class="value">
                                          <span>{{$failed_implant->order_number}}</span>
                                      </td>
                                  </tr>
                                @endif

                                @if(isset($order))
                                  <tr>
                                    <td class="title">Order ID</td>
                                    <td class="text-center">:</td>
                                    <td class="value"> <a href="{{route('customer.orders.view', base64_encode($order->id))}}" target="_blank"> #{{ $order->id }} </a></td>
                                  </tr>
                                @endif

                                <tr>
                                  <td class="title">Item Name</td>
                                  <td class="text-center">:</td>
                                  <td class="value">
                                    @if(isset($order_items))
                                      {{ $order_items->name }}
                                    @else
                                      {{ $failed_implant->order_item }}
                                    @endif
                                  </td>
                                </tr>

                                <tr>
                                  <td class="title">Replacement Item name</td>
                                  <td class="text-center">:</td>
                                  <td class="value">{{$product->name ?? ''}}</td>
                                </tr>

                                <tr>
                                    <td class="title">Replacement Reason</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->replacement_reason ?? ''}}</td>
                                </tr>

                                <tr>
                                  <td class="title">Order Mode:</td>
                                  <td class="text-center">:</td>
                                  <td class="value"> {{($failed_implant->order_type =='on')?'Online':'Offline'}} </td>
                                </tr>

                                <tr>
                                  <td class="title">Quantity</td>
                                  <td class="text-center">:</td>
                                  <td class="value">{{$failed_implant->qty}}</td>
                                </tr>

                                @if(isset($invoice_item_id))
                                    <tr>
                                        <td class="title">Lot Number</td>
                                        <td class="text-center">:</td>
                                        <td class="value">
                                            <?php
                                                $lot = DB::table('lot_items')->where('invoice_items_id', $invoice_item_id->id)->addSelect('lot_number', 'lot_quantity', 'lot_expiry')->get();
                                            ?>

                                            @if(count($lot)>0)
                                                @foreach($lot as $key => $lot_item)
                                                    {{(isset($lot_item->lot_number) and !empty($lot_item->lot_number))?$lot_item->lot_number:'-'}} ,
                                                    {{(isset($lot_item->lot_quantity) and !empty($lot_item->lot_quantity))?$lot_item->lot_quantity:'-'}} ,
                                                    {{(isset($lot_item->lot_expiry) and !empty($lot_item->lot_expiry))?date('Y-m', strtotime($lot_item->lot_expiry)):'-'}}<br>
                                                @endforeach
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                @endif

                                <tr>
                                    <td class="title">Product Code(or label)</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->implant_code}}</td>
                                </tr>

                                <tr>
                                    <td class="title">LOT #</td>
                                    <td class="text-center">:</td>
                                    <td class="value">{{$failed_implant->lot}}</td>
                                </tr>

                            </table>
                        </div>
                    </tab>

                    @if($failed_implant->item_status ==0)
                        <tab name="Failure Details">
                            <div class="table_content">
                                <table>
                                    <tr>
                                        <td class="title">Bone Type</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->bone_type}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Flapless</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->flapless]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Immediate Implant</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->immediate_implant]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Immediate Load</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->immediate_load]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Implantation Date</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{date('Y-m-d', strtotime($failed_implant->implant_date))}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Implant Removal Date</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{date('Y-m-d', strtotime($failed_implant->implant_removal_date))}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Implant Removal Location</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->remove_location}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Reasons for Failure</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->reason}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Other reason for Failure<br>/ Case Outcome</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->other_reason_failure}}</td>
                                    </tr>
                                </table>
                            </div>

                        </tab>

                        <tab name="History Details">
                            <div class="table_content">
                                <table>
                                    <tr>
                                        <td class="title">Age</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->patient_age}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Gender</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->gender}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Normal</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->normal_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Smoker</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->smoker_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Hypertension</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->hypertension_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Cardiac Problems</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->cardiac_problems_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Diabetes</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->diabetes_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Alcoholism</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->alcoholism_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Trauma</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->trauma_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Cancer</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$status[$failed_implant->cancer_history]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Others (Specify)</td>
                                        <td class="text-center">:</td>
                                        <td class="value">{{$failed_implant->others}}</td>
                                    </tr>
                                    <tr>
                                        <td class="title">Xray of the implant when placed</td>
                                        <td class="text-center">:</td>
                                        <td class="value"><form method="POST" action="{{route('admin.failed-implants.download')}}">
                                            @method("POST")
                                            @csrf
                                            <input type="hidden" name="type" value="before">
                                            <input type="hidden" name="file" value="{{$failed_implant->xray_implant}}">
                                            <button type="submit" class="btn btn-sm btn-primary down_btn">
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                                <span>&nbsp;Download</span>
                                            </button>
                                        </form></td>
                                    </tr>
                                    <tr>
                                        <td class="title">Xray showing the failed implant,<br>just before the implant is removed</td>
                                        <td class="text-center">:</td>
                                        <td class="value"><form method="POST" action="{{route('admin.failed-implants.download')}}">
                                            @method("POST")
                                            @csrf
                                            <input type="hidden" name="type" value="after">
                                            <input type="hidden" name="file" value="{{$failed_implant->before_implant_removed}}">
                                            <button type="submit" class="btn btn-sm btn-primary down_btn">
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                                <span>&nbsp;Download</span>
                                            </button>
                                        </form></td>
                                    </tr>
                                </table>
                            </div>
                        </tab>
                    @endif
                </tabs>
            </div>
        </div>

    </div>
</div>
@endsection

@push('scripts')
  <style type="text/css">
    .angle-left-icon{
        width: 20px !important;
        height: 20px !important;
        margin-right: 10px;
    }

    .table_content{
        margin-left: 20px;
    }

    .down_btn{
        font-size: 1.3rem;
    }

    .back-link{
        cursor: pointer;
    }
  </style>

  <script>
      $(function(){
          $('.content-container').css({
              'background': '#F1F3F6',
        });
      })
  </script>
@endpush

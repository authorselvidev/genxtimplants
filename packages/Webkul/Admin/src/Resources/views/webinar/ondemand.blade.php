@extends('admin::layouts.content')

@section('page_title')
    Edit - On-Demand Webinar
@stop

@section('content')
    <div class="content">
        <form method="post" class="save_all" action="{{ route('admin.webinar.storeondemand') }}">
            @csrf()
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link"></i>
                        Edit - On-Demand Webinar
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">Save</button>
                </div>
            </div>

            <div class="page-content">
                <button class="btn btn-sm btn-primary new_category" style="float: right;">Create Category</button>
                <div class="form-container category_container">

                @foreach($category as $cat)

                    <div class="accordian_container" data-category="{{$cat->category}}">
                        <accordian :title="'{{$cat->category}}'">
                            <div slot="body">
                                <div class="form-field col-md-12">
                                    <button class="btn btn-sm btn-primary video_new" style="float: right; margin:5px">New video</button>
                                    <?php
                                        $videos = DB::table("webinars")->where('is_live', 0)->where('category', $cat->category)->get();
                                    ?>
                                    <div class="category_item_container">
                                        @foreach($videos as $video)
                                            <div class="category_item clearfix" data-id="{{$video->id}}">
                                                <table>
                                                    <tr>
                                                        <td><label for="webinar-name-{{$video->id}}" class="required">Video Name:</label></td>
                                                        <td>
                                                            <input type="text" class="control" id="video-name-{{$video->id}}" name="video_name" value="{{ old('video_name', $video->name ?? '') }}"/>
                                                            <span class="control-error"></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><label for="video-description-{{$video->id}}" class="required">Video Description:</label></td>
                                                        <td>
                                                            <textarea class="control" id="video-description-{{$video->id}}" name="video_description">{{ old('video_description', $video->description ?? '') }}</textarea> 
                                                            <span class="control-error"></span>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><label for="video-url-{{$video->id}}" class="required">Video URL:</label></td>
                                                        <td>
                                                            <input type="text"  class="control" id="video-url-{{$video->id}}" name="video_url" value="{{ old('video_url', $video->video_url ?? '') }}"/>
                                                            <span class="control-error"></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <button class="btn btn-sm btn-danger delete_video" style="float: right; margin-right: 10px">Delete</button>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        </accordian>    
                        <button class="btn btn-sm btn-danger delete_category">&times;</button>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </form>
    </div>
@stop
@push('scripts')
    <style type="text/css">
        .accordian_container{
            clear: both;
            display: flex;
            align-items: center;
            align-content: center;
        }

        .accordian_container>button{
            height: 30px;
            margin-left: 10px;
        }

        .category_item{
            border:1px solid rgba(0,0,0,.1);
            padding: 20px;
            margin-top: 10px;
        }

        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }

        .clearfix{
            clear: both;
        }

        table, input, textarea{
            width: 100%;
        }

        input, textarea{
            border:1px solid rgba(0,0,0,.15) !important;
            padding: 5px;
        }

        td{
            padding: 10px;
        }

        tr td:first-child{
            width: 180px;
        }

        .control-error{
            margin:0px;
            margin-top: 5px;
        }

        label{
            font-weight: normal;
        }

        input[type="checkbox"]{
            width: 0%;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){

            function validateFields(ele){
                let status = false;
                if($(ele).val()==''){
                    $(ele).next('.control-error').text("This field is required").show();
                    status = true;
                }else{
                    /*if(ele.is('input[name="video_url"]')){
                        console.log(ele);
                    }*/
                    $(ele).next('.control-error').text("").hide();
                }
                return status;
            }

            $('.page-content').on('focusout', 'input, textarea', function(){
                validateFields($(this));
            });

            $('.page-content').on('click', '.new_category', function(e){
                e.preventDefault();
                const cat = prompt("Enter the new category name");
                if(cat){
                    $.ajax({
                        url: "{{ route('admin.webinar.ondemand.createcate') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "category": cat,
                        },
                        success: function(res){
                            let cat_element = `
                            <div data-category="${cat}" class="accordian_container">
                               <div class="accordian active">
                                  <div class="accordian-header">
                                     ${cat}
                                     <i class="icon accordian-up-icon"></i>
                                  </div>
                                  <div class="accordian-content">
                                     <div>
                                        <div class="form-field col-md-12">
                                           <button class="btn btn-sm btn-primary video_new" style="float: right; margin: 5px;">New video</button>
                                           <div class="category_item_container">
                                               <div data-id="${res.id}" class="category_item clearfix">
                                                  <table>
                                                     <tbody>
                                                        <tr>
                                                           <td><label for="webinar-name-${res.id}" class="required">Video Name:</label></td>
                                                           <td>
                                                              <input type="text" id="video-name-${res.id}" name="video_name" class="control">
                                                              <span class="control-error"></span>
                                                           </td>
                                                        </tr>
                                                        <tr>
                                                           <td><label for="video-description-${res.id}" class="required">Video Description:</label></td>
                                                           <td>
                                                              <textarea id="video-description-${res.id}" name="video_description" class="control"></textarea>
                                                              <span class="control-error"></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                           <td><label for="video-url-${res.id}" class="required">Video URL:</label></td>
                                                           <td>
                                                              <input type="text" id="video-url-${res.id}" name="video_url" value="" class="control">
                                                              <span class="control-error"></span>
                                                           </td>
                                                        </tr>
                                                     </tbody>
                                                  </table>
                                                  <button class="btn btn-sm btn-danger delete_video" style="float: right; margin-right: 10px;">Delete</button>
                                               </div>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                               <button class="btn btn-sm btn-danger delete_category">×</button>
                            </div>`;
                            $('.category_container').prepend($(cat_element));
                        }
                    });
                }
            });

            $('.page-content').on('click', '.video_new', function(e){
                e.preventDefault();
                const ele = $(this).closest('.accordian_container');
                const cat = ele.data('category');
                let state = false;
                ele.find('input, textarea').each(function(){
                    state = validateFields($(this))?true:state;
                });
                if(!state){
                    $.ajax({
                        url: "{{ route('admin.webinar.ondemand.newvideo') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "category": cat
                        },
                        success: function(res){
                            let item_html = `<div data-id="${res.id}" class="category_item clearfix">
                                                  <table>
                                                     <tbody>
                                                        <tr>
                                                           <td><label for="webinar-name-${res.id}" class="required">Video Name:</label></td>
                                                           <td>
                                                              <input type="text" id="video-name-${res.id}" name="video_name" value="" class="control">
                                                              <span class="control-error"></span>
                                                           </td>
                                                        </tr>
                                                        <tr>
                                                           <td><label for="video-description-${res.id}" class="required">Video Description:</label></td>
                                                           <td>
                                                              <textarea id="video-description-${res.id}" name="video_description" class="control"></textarea>
                                                              <span class="control-error"></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                           <td><label for="video-url-${res.id}" class="required">Video URL:</label></td>
                                                           <td>
                                                              <input type="text" id="video-url-${res.id}" name="video_url" value="" class="control">
                                                              <span class="control-error"></span>
                                                           </td>
                                                        </tr>
                                                     </tbody>
                                                  </table>
                                                  <button class="btn btn-sm btn-danger delete_video" style="float: right; margin-right: 10px;">Delete</button>
                                               </div>`;
                            
                            $('.category_item_container').append($(item_html));
                        }
                    });
                }
            });

            $('.page-content').on('click', '.delete_category', function(e){
                e.preventDefault();
                if(confirm("Do you like to delete this category")){
                    const cat_ele = $(this).closest('.accordian_container')
                    const cat = cat_ele.data('category');
                    $.ajax({
                        url: "{{ route('admin.webinar.ondemand.deletecate') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "category": cat,
                        },
                        success: function(res){
                            cat_ele.remove();
                        }
                    });
                }
            });

            $('.page-content').on('click', '.delete_video', function(e){
                e.preventDefault();
                if(confirm("Do you like to delete this video")){
                    const cat_ele = $(this).closest('.category_item');
                    const id = cat_ele.data('id');
                    $.ajax({
                        url: "{{ route('admin.webinar.ondemand.deletevideo') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                        },
                        success: function(res){
                            cat_ele.remove();
                        }
                    });
                }
            });


            $('form.save_all').on('submit', function(e){
                e.preventDefault();
                
                const ele = $('.category_container');

                let state = false;
                ele.find('input, textarea').each(function(){
                    currentState = validateFields($(this));
                    state = currentState?true:state;
                    if(currentState){
                        $(this).closest('.accordian').addClass('active');
                    }
                });


                if(!state){
                    let data = {};
                    $('.category_item').each(function(){
                        const id = $(this).data('id');
                        data[id] = {
                            "name": $(this).find('#video-name-'+id).val(),
                            "description": $(this).find('#video-description-'+id).val(),
                            "video_url" : $(this).find('#video-url-'+id).val()
                        };
                    });

                    $.ajax({
                        url: "{{ route('admin.webinar.storeondemand') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "category": JSON.stringify(data)
                        },

                        success: function(res){
                            window.location.reload(true);
                        }
                    });
                }
                e.preventDefault();
            });            
        });                
    </script>
@endpush
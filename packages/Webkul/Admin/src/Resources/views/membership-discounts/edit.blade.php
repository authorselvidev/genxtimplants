@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.discounts.edit-title') }}
@stop

@section('content')
    <div class="content">

        <form method="post" id="membershipForm" action="{{ route('admin.membership-discount.update', $discount->id) }}">
            @csrf()
            <input name="_method" type="hidden" value="PATCH">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('admin::app.discounts.edit-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.discounts.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    
                <?php $doctor_groups = DB::table('user_groups')->where('id','<>',1)->get(); 
                $product_discount = DB::table('discounts')->get();
               ?>
                <accordian :title="'{{ __('admin::app.discounts.general') }}'" :active="true">
                    <div slot="body">
                        <div class="form-field col-md-12">
                            <div class="control-group col-md-6" :class="[errors.has('discount_title') ? 'has-error' : '']">
                                <label class="required" for="discount_title">Discount Title</label>
                                <input type="text" v-validate="'required'" id="discount_title" class="control" name="discount_title" value="{{ $discount->discount_title }}" data-vv-as="Discount Title">
                                <span class="control-error" v-if="errors.has('discount_title')">@{{ errors.first('discount_title') }}</span>
                            </div>
                            <div class="control-group col-md-6" :class="[errors.has('discount_description') ? 'has-error' : '']">
                                <label class="required" for="discount_description">Discount Description</label>
                                <textarea id="discount_description" v-validate="'required'" class="control" name="discount_description" data-vv-as="Discount Description">{{ $discount->discount_description }}</textarea>
                                <span class="control-error" v-if="errors.has('discount_description')">@{{ errors.first('discount_description') }}</span>
                            </div>
                        </div>
                    </div>
                </accordian>

                <accordian :title="'Discount Details'" :active="true">
                    <div slot="body">
                        <div class="discount-part-main form-field col-md-12">
                            <div class="discount-inner  group-list col-md-12">
                                <div class="control-group col-md-4" :class="[errors.has('doctor_group') ? 'has-error' : '']">
                                    <label for="doctor_group" class="required"> Doctor Groups</label>
                                    <select v-validate="'required'" class="control doctor_group" id="doctor_group" name="doctor_group" data-vv-as="Doctor Groups">
                                        @foreach($doctor_groups as $key => $doctor_group)
                                            <option <?php if($discount->customer_group_id == $doctor_group->id) echo 'selected'; ?> value="{{ $doctor_group->id }}">{{ $doctor_group->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="control-error" v-if="errors.has('doctor_group')">@{{ errors.first('doctor_group') }}</span>
                                </div>
                                <div class="control-group col-md-4" :class="[errors.has('discount_per') ? 'has-error' : '']">
                                    <label for="select_doctors" class="required">Select Doctors</label>
                                    <select v-validate="'required'" class="control select_doctors" id="for_all_users" name="for_all_users" data-vv-as="Select Doctors">
                                        <option <?php if($discount->for_all_users == 1) echo 'selected'; ?> value="1"> All</option>
                                        <option <?php if($discount->for_all_users == 0) echo 'selected'; ?> value="0"> Custom</option>
                                    </select>
                                    <span class="control-error" v-if="errors.has('for_all_users')">@{{ errors.first('for_all_users') }}</span>
                                </div>
                                <div class="control-group col-md-4" :class="[errors.has('minimum_order_amount') ? 'has-error' : '']">
                                    <label for="minimum_order_amount" class="required">Min Order Total</label>
                                    <input v-validate="'required'" type="text" class="control" id="minimum_order_amount" name="minimum_order_amount" value="{{ $discount->minimum_order_amount}}" data-vv-as="Min Order Total">
                                    <span class="control-error" v-if="errors.has('minimum_order_amount')">@{{ errors.first('minimum_order_amount') }}</span>
                                </div>
                            </div>

                            <div class="discount-section col-md-12">
                                <div class="discounts" id="disount_container"></div>
                                <input type="hidden" name="discount_id" value="{{$discount->id}}">
                                <button type="button" class="add-discount pull-right btn btn-primary" name="add-more"><i class="fa fa-plus"></i></button>
                            </div>

                            <div class="discount-inner col-md-12">
                                <div class="control-group col-md-4">
                                    <label for="coupon_code">Discount Code</label>
                                    <input type="text" id="coupon_code" class="control" name="coupon_code" value="{{ $discount->coupon_code }}" data-vv-as="Discount Code">
                                </div>
                                 <div class="control-group col-md-3" :class="[errors.has('dealer_commission') ? 'has-error' : '']">
                                <label class="required" for="dealer_commission">Dealer Commission (%)</label>
                                <input type="text" v-validate="'required'" id="dealer_commission" class="control" name="dealer_commission" value="{{ $discount->dealer_commission }}" data-vv-as="Dealer Commission">
                                <span class="control-error" v-if="errors.has('dealer_commission')">@{{ errors.first('dealer_commission') }}</span>
                            </div>
                                <div class="control-group col-md-4">
                                    <label for="date_start">Date Start</label>
                                    <date>
                                        <input type="text" class="control date-start" name="date_start" value="{{ $discount->date_start }}" data-vv-as="Date Start">
                                    </date> 
                                </div>
                                <div class="control-group col-md-4">
                                    <label for="date_end">Date End</label>
                                    <date>
                                        <input type="text" class="control date-end" name="date_end" value="{{ $discount->date_end }}" data-vv-as="Date End">
                                    </date>
                                </div>
                            </div>
                            <textarea id="doctors_by_groups"></textarea>
                        </div>
                            <div class="doctor-sec">
                                <?php
                                foreach($doctor_groups as $key => $each_group)
                                {
                                    if($each_group->id != 1)
                                        $list_doctors = DB::table('users')->where('role_id',3)->where('customer_group_id',$each_group->id)->get();
                                    $group_mem = array();
                                    $full_name='';
                                    foreach($list_doctors as $key => $list_doctor){
                                        $full_name = $list_doctor->first_name.' '.$list_doctor->last_name;
                                        $group_mem[] = array('id' => $list_doctor->id, 'name' => $full_name);
                                    }
                                    $all_group[] = array('customer_group_id' =>$each_group->id, 'group_mem' => $group_mem);
                                    echo '<ul class="doctor-grp" id="doctor-group-'.$each_group->id.'"></ul>';
                                } 
                                //dd($all_group);
                                ?>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/simTree.css')}}">
    <script type="text/javascript" src="{{asset('themes/default/assets/js/simTree.js')}}"></script>

    <script>
        $(document).ready(function () {
            (function( $ ){
            $.fn.multipleInput = function() {
                return this.each(function() {
                    var group_count = '{{ count($all_group) }}';
                    var all_group = [];
                    var all_group = <?php echo json_encode($all_group); ?>;
                    $list='';
                    $list = $('.doctor-sec');
                    $.each(all_group,function( index, value ) {
                        //alert(JSON.stringify(value));
                        $.each(value['group_mem'],function( ind, val ) {
                            $list.find('#doctor-group-'+value['customer_group_id']).append($('<li class="multipleInput-doctor"><span>'+val["name"]+'</span><input type="hidden" name="doctors_list['+value['customer_group_id']+'][]" value="'+val["id"]+'"></li>')
                                    .append($('<a href="#" class="multipleInput-close" title="Remove"><i class="fa fa-times"></i></a>')
                                        ));
                        });
                    });

                    var $container = $('<div class="multipleInput-container col-md-12" />');
                    var clonehtml=$('<div class="doctor-sec-pgm">'+$('.doctor-sec').html()+'</div>');
                    $container.append(clonehtml).insertAfter($('.discount-inner.group-list'));
                    $('.doctor-sec').remove();
                    return $(this).hide();
                });
            };
        })( jQuery );


        $('body').on('click','.multipleInput-close',function(e) {
            $(this).parent().remove();
        });

        $('#doctors_by_groups').multipleInput();

        var parent_row =$('body').find('.select_doctors').parent().parent();
            var doctor_group = parent_row.find('.doctor_group :selected').val();
            parent_row.parent().find('.doctor-grp').removeClass('active')
            if($('.select_doctors :selected').val() == 0){
                parent_row.parent().find('#doctor-group-'+doctor_group).addClass('active')
            }

            

        $('body').on('change','.select_doctors', function(e){
            var parent_row = $(this).parent().parent();
            var doctor_group = parent_row.find('.doctor_group :selected').val();
            parent_row.parent().find('.doctor-grp').removeClass('active')
            if($(this).val() == 0){
                parent_row.parent().find('#doctor-group-'+doctor_group).addClass('active')
            }
        });



        $('body').on('change','.doctor_group', function(e){
            var parent_row = $(this).parent().parent();
            var select_doctors = parent_row.find('.select_doctors :selected').val();
            parent_row.parent().find('.doctor-grp').removeClass('active');
            if(select_doctors == 0)
                parent_row.parent().find('#doctor-group-'+$(this).val()).addClass('active');
        });        

        

        $('body').on('click', '.remove-me', function(){
            if(confirm('Do you like to remove this category?')){
                $(this).closest('.discount-part').remove();

                if(!$('.discount-part').length) addTree();
            }
        });

        const category_tree = '{!! $items !!}';
        const discounts_categories = {!! $all_discount !!};
        let treeCount =0;

        function addTree(dis=null){
            let cat_tree = JSON.parse(category_tree);

            let html_temp = $(`<div class="discount-part form-field col-md-12">
                                        <div class="discount-inner col-md-12">
                                            <div class="accordian active">
                                                <div class="accordian-header">Category Based Discount
                                                    <button type="button" id="remove${treeCount}" class="pull-right remove-me" ><i class="fa fa-times"></i></button>
                                                </div>
                                                <div class="accordian-content">
                                                    <div class="category-part section">
                                                        <div class="categorieTrees" data-id=${treeCount}>
                                                            <input type="hidden" name="discount[${treeCount}][categories]" id="categories${treeCount}" class="categorys_values">
                                                            <div id="categorieTree${treeCount}"></div>
                                                        </div>
                                                    </div>

                                                    <div class="control-group discount-percnt col-md-12">
                                                        <label for="discount_per">Discount (%)</label>
                                                        <input type="text" id="discount_per" class="control" name="discount[${treeCount}][discount_per]" value="${dis?dis.percentage:''}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> `);

            $('#disount_container').append(html_temp);

            cat_tree = cat_tree.map((val)=>{
                if(dis && dis.categorys.includes(val.id)) val.checked = true; 

                if(val.pid) val.pid = treeCount+'-'+val.pid;
                    val.id = treeCount+'-'+val.id;
                    return val;
                });

            if(dis) $(`#categories${treeCount}`).val(dis.categorys.map((val)=> treeCount+'-'+val));
            
            $(`#categorieTree${treeCount}`).simTree({
                data: cat_tree,
                check: true,
                linkParent: true,
                onChange: function(item){
                    const id = $(this['$el']).closest('.categorieTrees').data('id');
                    $(`#categories${id}`).val(item.filter((ele)=>ele.end==true).map((ele)=>ele.id));
                }
            });
            treeCount++;
        }

        function checkValidFields(){
            let $check = true;
            let discounts_container = $('#disount_container');
            discounts_container.find('.error-msg').remove();


            discounts_container.children().each(function(ele){
                if(!$(this).find('.categorys_values').val()){
                    $(this).find('.categorieTrees').append('<span class="error-msg">Please choose any categories in this tree!</span>');
                    $check = false;
                };

                if(! +$(this).find('#discount_per').val()) {
                    $(this).find('.discount-percnt').append('<span class="error-msg">This discount percentage is required!</span>');
                    $check = false;  
                }
            });
            return $check;
        }

        $(".add-discount").click(function(e){
            if(checkValidFields()) addTree();
            e.preventDefault();
        });
        discounts_categories.forEach((dis)=> addTree(dis));
        $('#membershipForm').on('submit', function(e){
            if(!checkValidFields()) e.preventDefault();
        });
    });
    </script>
    <style type="text/css">
        .remove-me, .add-discount{
            border: none;
            outline: none !important;
        }

        .remove-me{
            border-radius: 50%;
            background-color: #ff4a4a;
            color: #FFF;
        }

        .add-discount{
            margin-right: 15px;
        }
        .error-msg{
            display: inline-block;
            margin:10px 3px;
            color: #8F0000;
            font-size: 1.5rem;
        }
    </style>
    @endpush

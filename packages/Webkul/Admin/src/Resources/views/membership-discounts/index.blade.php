@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.discounts.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.discounts.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.membership-discount.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.discounts.add-title') }}
                </a>
            </div>
        </div>

        <div class="page-content">

           @inject('discounts','Webkul\Admin\DataGrids\MemberShipDiscountDataGrid')
            {!! $discounts->render() !!}
        </div>
    </div>
@stop
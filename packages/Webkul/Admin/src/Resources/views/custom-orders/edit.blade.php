@extends('admin::layouts.content')

@section('page_title')
    Edit Custom Orders
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.sales.custom_orders.update',$customer->id) }}" style="position: relative;">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/doctors') }}';"></i>

                        Custom Orders

                        {{ Config::get('carrier.social.facebook.url') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary order-submit">
                        Save
                    </button>
                </div>
            </div><!--page header-->
            <?php
                $icount = 0;
            ?>
            <div class="page-content">

                <div class="product-sec form-container">
                    @csrf()
                    <div class="products form-fields col-md-12" style="width: 100%;">
                        @if(isset($get_cart->items) && count($get_cart->items) > 0)
                                <input type="hidden" class="count_items" value="{{count($get_cart->items)}}">

                                
                                @foreach($get_cart->items as $o_key => $cart_item)
                                    
                                    <?php
                                        $created_at = date("d-m-Y", strtotime($get_cart->created_at));
                                        $icount = $o_key;
                                    ?>
                                    <div id="product{{$o_key}}" class="form-field col-md-12 doc-ff">
                                        <div class="product-inner col-md-12">
                                            <div class="control-group pass cpass col-md-6 col-xs-3">
                                                <label for="products[{{$o_key}}][id]" class="required">Products</label>
                                                <select class="myselect form-control" name="products[{{$o_key}}][id]">
                                                   
                                                    @foreach($products as $key => $product)
                                                        <?php 
                                                            $product_ordered_inventory = DB::table('product_ordered_inventories')->where('product_id',$product->product_id)->first();
                                                            $product_ordered_qty = (isset($product_ordered_inventory)) ? $product_ordered_inventory->qty : 0;
                                                            $product_available_qty = $product->qty - $product_ordered_qty;
                                                        ?>
                                                        <option @if($product_available_qty <= 0) {{'disabled'}} @endif @if($cart_item->product_id == $product->product_id) {{'selected'}} @endif value="{{$product->product_id}}">{{$product->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="control-group product-qty col-md-3 col-xs-3">
                                                <label for="products[{{$o_key}}][qty]" class="required">Quantity</label>
                                                <input type="number" min="0" class="form-control prod-quantity control" name="products[{{$o_key}}][qty]" value="{{ $cart_item->quantity }}">
                                                <div class="qty-error" style="display:none;color:red">The entered quantity is not available!</div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                        @endif
                    </div>
                    <div class="" style="bottom: 8px; left: 50%; transform: translateX(-50%); position:absolute;">
                        <input type="hidden" class="customer_id" name="customer_id" value="{{$customer->id}}">
                        <button type="button" class="add-product pull-right btn btn-primary" name="add-more"><i class="fa fa-plus"></i></button>
                    </div>

                </div>
            </div>
             <div class="page-footer">
                <button type="submit" class="btn btn-lg pull-right btn-primary order-submit">
                    Save
                </button>
            </div><!--page footer-->
        </form>
    </div>
@endsection
@push('scripts')
<script>
$(document).ready(function(){
    $('form').on('submit', function(e){
        $('.error-item').remove();
        $('.prod-quantity').each(function(){
            if($(this).val()=="" || $(this).val()==0 || $(this).val().length==0){
                $(this).after('<span class="error-item" style="color:red">Enter the valid quantity</span>');
                e.preventDefault();
            }
        });

        let products = [];
        $('.myselect').each(function(){
            if(products.includes($(this).val())){
                $(this).after('<span class="error-item" style="color:red">Products already selected</span>');
                e.preventDefault();
            }else{
                products.push($(this).val());    
            }            
        });
    });

    var count = {{++$icount}};
    $(".add-product").click(function(e){
        e.preventDefault();
        $('.error-item').remove();
        var clone=$('#product0').clone();
        clone.attr('id', 'product' + count).appendTo(".products");
        clone.find('.myselect').attr('name', 'products['+count+'][id]');
        clone.find('.product-qty > input').attr('name', 'products['+count+'][qty]');
        clone.find('.product-qty > input').val('');
        clone.find('.product-qty > input').attr('style', '');
        clone.find('.qty-error').attr('style', 'display: none;color:red;');
        //clone.find('.qty-error').html('');
        clone.find('.product-inner').append('<div class="col-xs-2 dt-ord"><button id="remove'+count+'" class="btn pull-right btn-danger remove-me" ><i class="fa fa-minus"></i></button></div>');
        count = count + 1;
    });

    $('body').on('click', '.remove-me', function(e){
        e.preventDefault();
        var remove_id=$(this).attr("id").match(/\d+/);
        var product_div = $('body').find('.products');
        product_div.find('#product'+remove_id).remove();
        var product_inner = product_div.find('.product-inner');
        $(product_inner).each(function(e){
            /*alert($(this).hasClass('no-stock'));*/
            if($(this).hasClass('no-stock') == true){
                $('.order-submit').attr('disabled',true);
                return false;
            }
            else
                $('.order-submit').attr('disabled',false);
        });
    });

    $('body').on('keyup','.prod-quantity',function(e){
    var this_var = $(this);
    this_var.next().css('display','none');
    this_var.attr('style', '');
    var quantity = $(this).val();
    var product_id = $(this).parents('.product-inner').find('.myselect :selected').val();
    var customer_id = $('.customer_id').val();
    //alert(customer_id);
    var route_name = "{{route('admin.sales.custom_orders.stock_check')}}";
    var product_div = $('body').find('.products');


    //route_name = route_name.replace(':id',customer_id);
    
        $.ajax({
            type: "POST",
            url: route_name,
            data: {"_token":'{{csrf_token()}}',"product_id": product_id,"quantity":quantity},
            success: function(result){
                //alert(result.success);
                if(result.success == false){
                    this_var.attr('style', 'border: 1px solid red !important');
                    this_var.next().css('display','block');
                    this_var.parents('.product-inner').addClass('no-stock');
                    $('.order-submit').attr('disabled',true);
                }
                else{
                    this_var.parents('.product-inner').removeClass('no-stock');
                    $('.order-submit').attr('disabled',false);
                }

            },
            error  : function(jqXHR, textStatus, errorThrown) { 
                console.log(textStatus);
                }
        });

        var product_inner = product_div.find('.product-inner');
        $(product_inner).each(function(e){
            if($(this).hasClass('no-stock') == true){
                $('.order-submit').attr('disabled',true);
                return false;
            }
    });


    });

    $('body').on('change','.myselect',function(e){
        $(this).parents('.product-inner').find('.prod-quantity').val("");
        $(this).parents('.product-inner').find('.prod-quantity').attr("style","");
        $(this).parents('.product-inner').find('.qty-error').attr("style","display: none;color:red;");
    });

});

</script>
@endpush


@extends('admin::layouts.content')

@section('page_title')
    Manage Cart
@stop

@section('content')
  <div class="content">
    <div class="page-header">
      <div class="page-title">
        <h1>
          <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/doctors/'.$customer->id) }}';"></i>Cart items {{ Config::get('carrier.social.facebook.url') }}
        </h1>
      </div>

      <div class="page-action" style="display: flex; flex-wrap: wrap;">
        <a href="{{ route('admin.sales.custom_orders.create',$customer->id) }}" class="btn btn-lg btn-primary">Add Items to Cart</a>
        <a href="javascript:void(0)" class="btn btn-lg btn-primary"  data-toggle="modal" data-target="#availableDiscounts" @if(!isset($get_cart->items) || $get_cart->discount_type!=0 ) disabled @endif>Apply Offers</a>
        <a href="javascript:void(0)" class="btn btn-lg btn-primary"  data-toggle="modal" data-target="#applyRewardModel" @if(!isset($get_cart->items) || $get_cart->discount_type!=0) disabled @endif>Apply Reward</a>
        <a href="{{ route('admin.sales.custom_orders.edit',$customer->id) }}" class="btn btn-lg btn-primary" @if(!isset($get_cart->items)) disabled @endif>Edit cart items</a>
        <a href='{{ route('admin.sales.custom_orders.clear_cart', $customer->id) }}' class="clear-cart btn btn-lg btn-danger" style="color: white;" @if(!isset($get_cart->items)) disabled @endif>Clear cart</a>
      </div>
    </div><!--page header-->

    <div class="page-content section table-list show-doc">
      <div class="panels panel-default">
        <div class="panel-body lat-ord" style="clear: both; overflow: scroll;">
          <div class="table">
            <table class="table">
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Sub Total</th>
                      {{-- {{dd($get_cart->toArray())}} --}}
                      @if(isset($get_cart) && (($get_cart->promocode_discount != 0 || $get_cart->membership_discount !=0 || $get_cart->credit_discount != 0) || (($get_cart->promocode_discount == 0 && $get_cart->membership_discount ==0 && $get_cart->reward_discount == 0 && $get_cart->credit_discount == 0) && $get_cart->discount != 0.0000)))
                        <th>Discount</th>
                      @endif
                      <th>Tax Amount</th>
                      <th>Grand Total</th>
                      <th>Role</th>
                      <th>Action</th>
                </tr>
              </thead>

              <tbody>
                  @if(isset($get_cart) && isset($get_cart->items) && count($get_cart->items) > 0)
                      @foreach($get_cart->items as $o_key => $cart_item)
                        <?php 
                          //dd($get_cart->items);
                          $created_at = date("d-m-Y", strtotime($get_cart->created_at));
                          $created_by = $get_cart->customer_id;
                          $discount_price = 0;
                          if(isset($get_cart->created_by))
                            $created_by = $get_cart->created_by;
                        ?>
                        <tr class="odd gradeX">
                          <td data-value="Product">{{ Webkul\Product\Models\ProductFlat::ProductName($cart_item->product_id) }}</td>
                          <td data-value="Price">{{ number_format($cart_item->price,2) }}</td>
                          <td data-value="Quantity">{{ $cart_item->quantity }}</td>
                          <td data-value="Sub Total">{{ number_format($cart_item->total,2) }}</td>


                          @if($get_cart->promocode_discount != 0 || $get_cart->membership_discount !=0 || $get_cart->credit_discount != 0)
                            @if($cart_item->discount_applied == 1)
                              <?php
                                $product_price = ($cart_item->total);
                                $discount_price = $cart_item->discount_applied_amount;
                              ?>

                              @if($discount_price != 0)
                                <td>{{ core()->currency($discount_price) }}</td>
                              @endif
                            @else
                              <td>NA</td>
                            @endif
                          @elseif(($get_cart->promocode_discount == 0 && $get_cart->membership_discount ==0 && $get_cart->reward_discount == 0 && $get_cart->credit_discount == 0 ) && ($get_cart->discount != 0.0000))
                            @if ($cart_item->discount_applied == 0 && $get_cart->discount != 0.0000)
                              <?php 
                                $product_price = ($cart_item->total - $cart_item->discount_amount);
                                $discount_price = $cart_item->discount_amount;
                              ?>

                              @if($cart_item->discount_amount != 0)
                                <td>{{ core()->currency($cart_item->discount_amount) }}</td>
                              @else
                                <td>NA</td>
                              @endif
                            @else
                              <td>NA</td>
                            @endif
                          @endif
                          <td data-value="Tax Amount">{{ number_format($cart_item->tax_amount,2) }}</td>
                          <td data-value="Grand Total">{{ number_format($cart_item->total + $cart_item->tax_amount - $discount_price,2)}}</td>
                          <td data-value="Role">{{ Webkul\Customer\Models\Customer::CustomerRoleName($created_by) }}</td>
                          <td class="action-menu">
                            <span class="remove">
                              <a href="{{ url('/admin/custom-orders/'.$customer->id.'/clear-cart-item/'.$cart_item->id) }}" onclick="removeLink('Do you really want to do this?')"><i class="fa fa-trash"></i></a>
                            </span>
                          </td>
                        </tr>
                      @endforeach
                  @else
                    <tr><td class="text-center" colspan=9>No results Found</td></tr>
                  @endif
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.panel-body -->
        @if(isset($get_cart))
          <div class="pull-right cart_total_details">
            <table style="max-width: 400px;">
              <tr>
                <td>Sub Total</td>
                <td>:</td>
                <td>{{ core()->currency($get_cart->sub_total) }}</td>
              </tr>
              <?php $discounted_subtotal=0; ?>
              @if($get_cart->discount != 0.0000 && ($get_cart->promocode_discount ==0 && $get_cart->reward_discount==0 && $get_cart->membership_discount==0 && $get_cart->credit_discount==0))
                <tr>
                  <td>Special Price:</td>
                  <td>:</td>
                  <td>{{ core()->currency($get_cart->sub_total - $get_cart->discount) }}</td>
                </tr>
                <?php $discounted_subtotal = round($get_cart->base_sub_total - $get_cart->base_discount,2); ?>
              @endif

              @if(isset($membership_details))
                <tr>
                  <td>Membership Discount</td>
                  <td>:</td>
                  <td>- {{ core()->currency($membership_details['mem_discount_price']) }}</td>
                </tr>
                <?php $discounted_subtotal = round($get_cart->base_sub_total - $get_cart->membership_discount,2); ?>
              @endif

              @if(isset($promocode_details))
                <tr>
                  <td>Promo Code Discount</td>
                  <td>:</td>
                  <td>- {{ core()->currency ($promocode_details['promo_code_price']) }}</td>
                </tr>
                <?php $discounted_subtotal = round($get_cart->base_sub_total - $get_cart->promocode_discount,2); ?>
              @endif

              @if(isset($credit_discount))
                <tr>
                  <td>Advance Payment Discount</td>
                  <td>:</td>
                  <td>- {{ core()->currency($credit_discount['credit_discount_price']) }}</td>
                </tr>
                <?php $discounted_subtotal = round($get_cart->base_sub_total - $get_cart->credit_discount,2); ?>
              @endif

              @if(isset($reward_details))
                <tr>
                    <td>Reward Points</td>
                    <td>:</td>
                    <td>- {{ core()->currency($reward_details) }}</td>
                </tr>
                <?php $discounted_subtotal = round($get_cart->base_sub_total - $get_cart->reward_discount,2); ?>              
              @endif

              @if($discounted_subtotal != 0)
                <tr>
                    <td>Discounted Subtotal</td>
                    <td>:</td>
                    <td>{{ core()->formatBasePrice($discounted_subtotal) }}</td>
                </tr>
              @endif

              <tr>
                <td>Shipping Cost</td>
                <td>:</td>
                <td>+ {{ core()->formatBasePrice($shipping_amount) }}</td>
              </tr>

              @if($customer->state_id == 22)
                <tr>
                  <td>CGST</td>
                  <td>:</td>
                  <td>+ {{ number_format($tax_amount/2 ,2) }}</td>
                </tr>
                <tr>
                  <td>SGST</td>
                  <td>:</td>
                  <td>+ {{ number_format($tax_amount/2 ,2) }}</td>
                </tr>
              @else
                <tr>
                  <td>IGST</td>
                  <td>:</td>
                  <td>+ {{ number_format($tax_amount,2) }}</td>
                </tr>
              @endif

              <tr>
                  <td><strong>Grand Total</strong></td>
                  <td><strong>:</strong></td>
                  <td><strong>{{ core()->currency($total_amount)}}</strong></td>
              </tr>
            </table>        
          </div>

        @endif
      </div>

      <!-- /.panel -->
      <div class="page-action pull-left" style="display: flex;flex-direction:column;align-items:flex-start; margin-top:10px">
        @if(isset($get_cart) && $get_cart->discount_type != 0)
          <span><strong>{{$discount_type[$get_cart->discount_type] ?? 'Discount'}} : {{($get_cart->discount_type==6)? $get_cart->reward_points_applied.' Points' : $discount->coupon_code }}</strong></span>        
          <a href="{{route('admin.sales.custom_orders.remove_cart_discount', $get_cart->id)}}" class="clear-discount btn btn-sm btn-danger" style="color: white; margin-top:10px" @if(!isset($get_cart->items)) disabled @endif>Remove</a>        
        @endif
      </div>

      <div class="page-action pull-right" style="clear: both;margin-top: 10px;">
          <a href="{{route('admin.sales.custom_orders.send-email', $customer->id)}}" class="clear-discount btn btn-sm btn-primary" @if(!isset($get_cart->items)) disabled @endif>Send mail</a>
      </div>
    </div>
  </div>


  <div class="modal fade" id="availableDiscounts" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Available Offers</h4>
        </div>
        <div class="modal-body">
          <?php

          if(isset($get_cart) && isset($get_cart->items) && count($get_cart->items) > 0){
            $types = ['mem' => 2, 'promo'=>3, 'credit'=>5, 'promotion'=>4];
              $discount_present_flag = 0;
              foreach($types as $dis => $type){
                if($type==2 && $customer->role_id == 2 || ($type==5 && !($customer->role_id != 1 && $customer->role_id != 4 && $paylaterwithcredit->value == 1 && $customer->credit_available && ($total_amount <= $customer->credit_balance)))) continue;

                $coupons = Webkul\Shop\Http\Controllers\OnepageController::ListAllCoupons($type, $customer->id);

                if(!empty($coupons['allOffers'])){
                  $discount_present_flag =1;
                  echo '<h3>';
                    switch($dis){
                      case 'mem':
                        echo 'Membership Discount';
                        break;
                      case 'promo':
                        echo 'Promo Code Discount';
                        break;
                      case 'credit':
                        echo 'Advance Payment Discount';
                        break;
                      case 'promotion':
                        echo 'Promotions Discount';
                        break;
                      default:
                        echo 'Discount';
                        break;
                    }
                  echo '</h3>';
                  echo '<div style="padding-left:10px; border-bottom: 1px solid #ddd;">'.$coupons['allOffers'].'</div>';
                }
              }
              if($discount_present_flag==0) echo '<h4 style="text-align:center">No discounts available!</h4>';
          }
            
          ?>
        </div>

        <div class="modal-footer" style="border-top:unset">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="applyRewardModel" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Apply Reward</h4>
        </div>
        <div class="modal-body">
            <div class="reward_points control-group">
              <table>
                <tr>
                  <td>Available Points</td>
                  <td>:</td>
                  <td>{{$availableRewardPoints}} Points</td>
                </tr>
                <tr>
                  <td>Enter Points</td>
                  <td>:</td>
                  <td>
                      <input class="form-control points_val" type="number" min="1" data-max="{{$availableRewardPoints}}" data-vv-as="Enter Points" max="{{$availableRewardPoints}}" name="reward_points" @if($availableRewardPoints==0) disabled @endif>
                      <span class="control-error" style="display:none;margin-left:0px">The field is required.</span>
                  </td>                  
                </tr>
              </table>
              <button type="submit" class="btn btn-sm btn-primary btn-apply">Apply</button>
            </div>
        </div>
        <div class="modal-footer" style="border-top:unset">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>      
    </div>
  </div>
@endsection

@push('scripts')

  <script>
    $(document).ready(function(){
      $('body').on('click','.clear-cart', function(e){
        if($(this).attr('disabled')=='disabled' || !confirm('Are you sure you want to clear the cart?')){
          e.preventDefault();
        }
      });
      const user_id = {{$customer->id}};

      $('.apply-membership').click(function(){
            $('#availableDiscounts').modal('hide');
            var discount_id = $(this).parent().find('.discount-id').val();
            var discount_type = $(this).parent().find('.discount-type').val();
            var discount_per = $(this).parent().find('.discount-per').val();
            var promo_code_price = $('body').find('.promo-cde-price').val();
            var coupon_code = $(this).parent().find('.coupon-code').val();

            $.ajax({
                url: "{{route('shop.checkout.apply.membership')}}",
                data: { _token:"{{ csrf_token() }}", discountId:discount_id,discountType:discount_type,discountPer:discount_per,couponCode:coupon_code, user_id:user_id},
                type: 'POST',
                dataType: "json",
                success: function(result){
                    location.reload(true);
                },
                error: function(xhr, textStatus, errorThrown) {
                    console.log(xhr.responseText);
                }
            });
        });


      $('.apply-promocode').click(function(){
            $('#availableDiscounts').modal('hide'); 
            var discount_id = $(this).parent().find('.discount-id').val();
            var discount_type = $(this).parent().find('.discount-type').val();
            var discount_per = $(this).parent().find('.discount-per').val();
            var mem_discount_price = $('body').find('.mem-discunt-price').val();
            var coupon_code = $(this).parent().find('.coupon-code').val();
            var apply_membership = $(this).parent().find('.apply-membershp').val();
            var earn_points = $(this).parent().find('.earn-point').val();
            
            $.ajax({ 
                url: "{{route('shop.checkout.apply.promocode')}}",
                data: { _token:"{{ csrf_token() }}",discountId:discount_id,discountType:discount_type,memDiscountPrice:mem_discount_price,discountPer:discount_per,couponCode:coupon_code,applyMembership:apply_membership,earnPoints:earn_points, user_id:user_id},
                type: 'POST',
                dataType: "json",
                success: function(result)
                {
                    location.reload(true);
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                }
            });
        });


        $('.apply-creditdiscount').click(function(){
            $('#availableDiscounts').modal('hide'); 
            var discount_id = $(this).parent().find('.discount-id').val();
            var discount_type = $(this).parent().find('.discount-type').val();
            var discount_per = $(this).parent().find('.discount-per').val();
            var mem_discount_price = $('body').find('.mem-discunt-price').val();
            var coupon_code = $(this).parent().find('.coupon-code').val();
            var apply_membership = $(this).parent().find('.apply-membershp').val();
            var earn_points = $(this).parent().find('.earn-point').val();
            
            $.ajax({ 
                url: "{{route('shop.checkout.apply.credit-discount')}}",
                data: { _token:"{{ csrf_token() }}",discountId:discount_id,discountType:discount_type,memDiscountPrice:mem_discount_price,discountPer:discount_per,couponCode:coupon_code,applyMembership:apply_membership,earnPoints:earn_points, user_id:user_id},
                type: 'POST',
                dataType: "json",
                success: function(result)
                {
                    location.reload(true);
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                }
            });
        });

        $('.reward_points').on('click', '.btn-apply', function(){
          const root = $(this).closest('.reward_points');
          root.find('.control-error').hide();
          let reward_val = root.find('.points_val');
          if(!reward_val.val() || reward_val.val()==0){
            root.find('.control-error').text('This field is required').show();
          }else if(reward_val.data('max')<reward_val.val()){
            root.find('.control-error').text('Points limit exceeds').show();
          }else{
            $.ajax({
              url: "{{route('shop.checkout.apply.reward')}}",
              data: { _token:"{{ csrf_token() }}", rewardPoints:reward_val.val(), user_id:user_id},
              type: 'POST',
              dataType: "json",
              success: function(result){
                if(result.success){
                  location.reload(true);
                }
              },
              error: function(xhr, textStatus, errorThrown) {
                    alert('You exceeds your point limit!');
                }
            });
          }
        });
    });
  </script>
  <style types="text/css">
    a[disabled]{
      pointer-events: none;
    }
    .offer-code{border: 1px solid #daceb7;background-color: #fffae6;min-width:105px;width: 19%;text-align: center;padding: 3px;}
    .offer-section li{margin:0 0 12px;}
    .text-promotion{padding: 8px 0;float: left;font-size: 16px;}

    .offer-section .apply-offer{border:1px solid #20A9EA;background:none;color:#20A9EA;}
    .offer-section .apply-offer:hover{color:#fff;background:#20A9EA;}
    .offer-section .title{font-weight:bold;}
    .offer-section {padding:1.2em 0;}

    .offer-section .btn {
      box-shadow: 5px 5px 8px 0px rgba(0, 0, 0, 0.3);
      border-radius: 4px !important;
      border: 0 !important;
      padding: 12px 20px 8px !important;
      transition: 0.8s;
    }

    .reward_points td{
      padding: 5px 10px;
    }

    .reward_points .btn-apply{
      margin-left: 10px;
    }

    .cart_total_details td{
      padding: 5px 10px;
      text-align: left;
    }

    .cart_total_details tr td:last-child{
      text-align: right;
    }
    .page-action a{
      color:#FFF;
      margin-bottom: 5px;
    }
  </style>
@endpush

@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.promo_codes.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.promo_codes.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.promo-code.create') }}" class="btn btn-lg btn-primary">
                    {{ __('admin::app.promo_codes.add-title') }}
                </a>
            </div>
        </div>

        <div class="page-content">

           @inject('promo_codes','Webkul\Admin\DataGrids\PromoCodeDataGrid')
            {!! $promo_codes->render() !!}
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(document).ready(function(){
        $('body').on('click', '.fa-toggle-on', function(e){
            if(!confirm('Are you sure, you want to disable')){
                e.preventDefault();
            }
        });
    });
</script>
@endpush

@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.promo_codes.add-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" id="promocodeForm"  action="{{ route('admin.promo-code.store') }}">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/promo-codes') }}';"></i>

                        {{ __('admin::app.promo_codes.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.promo_codes.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                <?php $doctor_groups = DB::table('user_groups')->where('id','<>',1)->get(); 
                $product_discount = DB::table('discounts')->get();
               ?>
                    <accordian :title="'{{ __('admin::app.promo_codes.general') }}'" :active="true">
                        <div slot="body">
                            <div class="form-field col-md-12">
                                <div class="control-group col-md-6" :class="[errors.has('discount_title') ? 'has-error' : '']">
                                    <label class="required" for="discount_title">Promo Code Title</label>
                                    <input type="text" v-validate="'required'" id="discount_title" class="control" name="discount_title" value="{{old('discount_title')}}" data-vv-as="Promo Code Title">
                                    <span class="control-error" v-if="errors.has('discount_title')">@{{ errors.first('discount_title') }}</span>
                                </div>
                                <div class="control-group col-md-6" :class="[errors.has('discount_description') ? 'has-error' : '']">
                                    <label class="required" for="discount_description">Promo Code Description</label>
                                    <textarea id="discount_description" v-validate="'required'" class="control" name="discount_description" data-vv-as="Promo Code Description">{{ old('discount_description')}}</textarea>
                                    <span class="control-error" v-if="errors.has('discount_description')">@{{ errors.first('discount_description') }}</span>
                                </div>
                            </div>
                        </div>
                    </accordian>
                    
                    <accordian :title="'Discount Details'" :active="true">
                        <div slot="body">
                            <div class="check-discount">
                                <div class="control-group col-md-12 @if ($errors->has('for_whom')) {{'has-error'}} @endif">
                                    <span class="col-md-5"><label for="for_whom" class="required">For Whom</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                    <span class="doctor_ck"><input type="checkbox" class="control" id="for_whom_doctor" name="for_whom[]" value="3"><label for="for_whom_doctor">Doctor</label></span>
                                    <span class="dealer_ck"><input type="checkbox" class="control" id="for_whom_dealer" name="for_whom[]" value="2"><label for="for_whom_dealer">Dealer</label></span>
                                    @if ($errors->has('for_whom'))
                                        <span class="control-error">
                                            {{ $errors->first('for_whom') }}
                                        </span>
                                    @endif
                                </div>
                                </div>

                                <div class="control-group col-md-12" :class="[errors.has('apply_membership') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="apply_membership" class="required">Apply Membership Discount</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                    <span class="doctor_ck"><input v-validate="'required'" type="radio" class="control" id="membership_yes" name="apply_membership" value="1" data-vv-as="Apply Membership Discount"><label for="membership_yes" >Yes</label></span>
                                    <span class="dealer_ck"><input v-validate="'required'" type="radio" class="control" id="membership_no" name="apply_membership" value="0" data-vv-as="Apply Membership Discount"><label for="membership_no" >No</label></span>
                                    <span class="control-error" v-if="errors.has('apply_membership')">@{{ errors.first('apply_membership') }}</span>
                                </div>
                                </div>

                                <div class="control-group col-md-12" :class="[errors.has('earn_points') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="earn_points" class="required">Earn Loyalty Points</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
										<span class="doctor_ck"><input v-validate="'required'" type="radio" class="control" id="earn_points_yes" name="earn_points" value="1" data-vv-as="Earn Loyalty Points"><label for="earn_points_yes">Yes</label></span>
										<span class="dealer_ck"><input v-validate="'required'" type="radio" class="control" id="earn_points_no" name="earn_points" value="0" data-vv-as="Earn Loyalty Points"><label for="earn_points_no">No</label></span>
										<span class="control-error" v-if="errors.has('earn_points')">@{{ errors.first('earn_points') }}</span>
									</div>
                                </div>
                                
                                <div class="control-group col-md-12" :class="[errors.has('discount_usage_limit') ? 'has-error' : '']">
                                    <span class="col-md-5"><label for="discount_usage_limit" class="required">How many times a doctor/dealer can use?</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <input type="text" v-validate="'required'" id="discount_usage_limit" class="control" name="discount_usage_limit" value="{{old('discount_usage_limit')}}" data-vv-as="How many times">
                                        <span class="control-error" v-if="errors.has('discount_usage_limit')">@{{ errors.first('discount_usage_limit') }}</span>
                                    </div>
                                </div>

                                <div class="control-group col-md-12" :class="[errors.has('dealer_commission') ? 'has-error' : '']">
                                    <span class="col-md-5"><label class="required" for="dealer_commission">Dealer Commission (%)</label></span>
                                    <span class="text-center col-md-1">:</span>
                                    <div class="col-md-6">
                                        <input type="text" v-validate="'required'" id="dealer_commission" class="control" name="dealer_commission" value="{{old('dealer_commission')}}" data-vv-as="Dealer Commission">
                                        <span class="control-error" v-if="errors.has('dealer_commission')">@{{ errors.first('dealer_commission') }}</span>
                                    </div>
                                </div>
                            </div>
                           <div class="discount-part-main form-field col-md-12">
                            <div class="discount-inner group-list col-md-12"></div>

                            <div class="discount-section col-md-12">
                                <div class="discounts" id="disount_container"></div>                                
                                <button type="button" class="add-discount pull-right btn btn-primary" name="add-more"><i class="fa fa-plus"></i></button>
                            </div>

                            <div class="discount-inner col-md-12">
                                <div class="control-group col-md-3" :class="[errors.has('minimum_order_amount') ? 'has-error' : '']">
                                    <label for="minimum_order_amount" class="required">Min Order Total</label>
                                    <input v-validate="'required'" type="text" class="control" id="minimum_order_amount" name="minimum_order_amount" value="{{old('minimum_order_amount') ?? 0}}" data-vv-as="Min Order Total">
                                    <span class="control-error" v-if="errors.has('minimum_order_amount')">@{{ errors.first('minimum_order_amount') }}</span>
                                </div>
                                <?php $custom_code = mt_rand(10000, 99999);
                                    $gxt_custom_code = "GXT".$custom_code; ?>
                                <div class="control-group col-md-3" :class="[errors.has('coupon_code') ? 'has-error' : '']">
                                    <label class="required" for="coupon_code">Discount Code</label>
                                    <input v-validate="'required'" type="text" id="coupon_code" class="control" name="coupon_code" value="{{ $gxt_custom_code }}" data-vv-as="Discount Code">
                                    <span class="control-error" v-if="errors.has('coupon_code')">@{{ errors.first('coupon_code') }}</span>
                                </div>
                                <div class="control-group col-md-3">
                                    <label for="date_start">Date Start</label>
                                    <date>
                                        <input type="text" class="control date-start" name="date_start" value="" data-vv-as="Date Start">
                                    </date> 
                                </div>
                                <div class="control-group col-md-3">
                                    <label for="date_end">Date End</label>
                                    <date>
                                        <input type="text" class="control date-end" name="date_end" value="" data-vv-as="Date End">
                                    </date>
                                </div>
                            </div>
                        </div>
                        
                            <!-- <div class="doctor-sec">
                                <?php
                                foreach($doctor_groups as $key => $each_group)
                                {
                                    if($each_group->id == 1)
                                        $list_doctors = DB::table('users')->where('role_id',3)->get();
                                    else
                                        $list_doctors = DB::table('users')->where('role_id',3)->where('customer_group_id',$each_group->id)->get();
                                    $group_mem = array();
                                    $full_name='';
                                    foreach($list_doctors as $key => $list_doctor){
                                    $full_name = $list_doctor->first_name.' '.$list_doctor->last_name;

                                        $group_mem[] = array('id' => $list_doctor->id, 'name' => $full_name);
                                    }
                                    $all_group[] = $group_mem;
                                    echo '<ul class="doctor-grp" id="doctor-group-'.$each_group->id.'"></ul>';
                                } 
                                //dd($all_group);
                                ?>
                            </div> -->
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop


@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('themes/default/assets/css/simTree.css')}}">
    <script type="text/javascript" src="{{asset('themes/default/assets/js/simTree.js')}}"></script>

<script>
$(document).ready(function () {
    $('body').on('click', '.remove-me', function(){
        if(confirm('Do you like to remove this category?')){
            $(this).closest('.discount-part').remove();
        }
        if(!$('.discount-part').length) addTree();
    });

     const category_tree = '{!! $items !!}';
     let treeCount =0;

     function addTree(){
                let cat_tree = JSON.parse(category_tree);

                let html_temp = $(`<div class="discount-part form-field col-md-12">
                                            <div class="discount-inner col-md-12">
                                                <div class="accordian active">
                                                    <div class="accordian-header">Category Based Discount
                                                        <button type="button" id="remove${treeCount}" class="pull-right remove-me" ><i class="fa fa-times"></i></button>
                                                    </div>
                                                    <div class="accordian-content">
                                                        <div class="category-part section">
                                                            <div class="categorieTrees" data-id=${treeCount}>
                                                                <input type="hidden" name="discount[${treeCount}][categories]" id="categories${treeCount}" class="categorys_values">
                                                                <div id="categorieTree${treeCount}"></div>
                                                            </div>
                                                        </div>

                                                        <div class="control-group discount-percnt col-md-12">
                                                            <label for="discount_per">Discount (%)</label>
                                                            <input type="text" id="discount_per" class="control" name="discount[${treeCount}][discount_per]" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> `);

                $('#disount_container').append(html_temp);

                cat_tree = cat_tree.map((val)=>{
                    if(val.pid) val.pid = treeCount+'-'+val.pid;
                    val.id = treeCount+'-'+val.id;
                    return val;
                });                
                
                $(`#categorieTree${treeCount}`).simTree({
                    data: cat_tree,
                    check: true,
                    linkParent: true,
                    onChange: function(item){
                        const id = $(this['$el']).closest('.categorieTrees').data('id');
                        $(`#categories${id}`).val(item.filter((ele)=>ele.end==true).map((ele)=>ele.id));
                    }
                });
                treeCount++;
            }

            function checkValidFields(){
                let $check = true;
                let discounts_container = $('#disount_container');
                discounts_container.find('.error-msg').remove();


                discounts_container.children().each(function(ele){
                    if(!$(this).find('.categorys_values').val()){
                        $(this).find('.categorieTrees').append('<span class="error-msg">Please choose any categories in this tree!</span>');
                        $check = false;
                    };

                    if(! +$(this).find('#discount_per').val()) {
                        $(this).find('.discount-percnt').append('<span class="error-msg">This discount percentage is required!</span>');
                        $check = false;  
                    }
                });
                return $check;
            }

            $(".add-discount").click(function(e){
                if(checkValidFields()) addTree();
                e.preventDefault();
            });

            $('#promocodeForm').on('submit', function(e){
                if(!checkValidFields()) e.preventDefault();
            });

            addTree();
});
</script>
    <style type="text/css">
        .remove-me, .add-discount{
            border: none;
            outline: none !important;
        }

        .remove-me{
            border-radius: 50%;
            background-color: #ff4a4a;
            color: #FFF;
        }

        .add-discount{
            margin-right: 15px;
        }
        
        .error-msg{
            display: inline-block;
            margin:10px 3px;
            color: #8F0000;
            font-size: 1.5rem;
        }
    </style>
@endpush

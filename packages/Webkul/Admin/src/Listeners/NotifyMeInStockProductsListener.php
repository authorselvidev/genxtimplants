<?php

namespace Webkul\Admin\Listeners;

use Webkul\Admin\Events\NotifyMeInStockProducts;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use Webkul\Admin\Mail\NotifyMeEmail;
use DB;

class NotifyMeInStockProductsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderEmailTdlResults  $event
     * @return void
     */
    public function handle(NotifyMeInStockProducts $event)
    {
         $notify_customers = DB::table('notify_me')->where('notify_sent',0)->get();
         $notify_info = array();
         foreach($notify_customers as $key => $customer){
            $customer_info = DB::table('users')->where('id',$customer->customer_id)->first();
            $product_info = DB::table('products_grid')->where('product_id',$customer->product_id)->first();
            $notify_info['email'] = $customer_info->email;
            $notify_info['first_name'] = $customer_info->first_name;
            $notify_info['product_name'] = $product_info->name;
            $mail_sent = Mail::send(new NotifyMeEmail($notify_info));

            DB::table('notify_me')->where('id',$customer->id)->update(['notify_sent'=>1]);
         }
       
    }
}

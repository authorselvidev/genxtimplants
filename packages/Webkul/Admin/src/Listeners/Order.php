<?php

namespace Webkul\Admin\Listeners;

use Illuminate\Support\Facades\Mail;
use Webkul\Admin\Mail\NewOrderNotification;
use Webkul\Admin\Mail\NewInvoiceNotification;
use Webkul\Admin\Mail\NewShipmentNotification;
use DB;
use Illuminate\Support\Facades\Log;


/**
 * Order event handler
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class Order {

    /**
     * @param mixed $order
     *
     * Send new order confirmation mail to the customer
     */
    public function sendNewOrderMail($order)
    {
        try {
           Log::info($order->customer_email); 
            $admins = DB::table('users')->whereIn('role_id',[1,4])->where('status',1)->get();
            $doctor = DB::table('users')->where('id', $order->customer_id)->first();
            $subject = ($order->payment_method == 'prepayment')?'New PrePayment Order Received':'New Order Received';
            foreach($admins as $key =>$admin){
                $user_id = $admin->id;
                $mail_sent = Mail::to($admin->email)->send(new NewOrderNotification($order,$subject,$user_id));
            }

            if($doctor->dealer_id != null && $doctor->dealer_id != 0 && $order->payment_method == "prepayment"){
                $dealer = DB::table('users')->where('id', $doctor->dealer_id)->first();
                Mail::to($dealer->email)->send(new NewOrderNotification($order,$subject,$doctor->dealer_id));
            }

            //Mail::to($order->customer_email)->subject('New Order Confirmation!')->send(new NewOrderNotification($order));
            
            $subject = 'Your GenXT WebX order – Status: Confirmed';
            $user_id = $order->customer_id;
            

            Mail::to($order->customer_email, $order->customer_first_name)->send(new NewOrderNotification($order,$subject,$user_id));
            

        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param mixed $invoice
     *
     * Send new invoice mail to the customer
     */
    public function sendNewInvoiceMail($invoice)
    {
        try {
            Mail::send(new NewInvoiceNotification($invoice));
        } catch (\Exception $e) {

        }
    }

    /**
     * @param mixed $shipment
     *
     * Send new shipment mail to the customer
     */
    public function sendNewShipmentMail($shipment)
    {
        try {
            Mail::send(new NewShipmentNotification($shipment));
        } catch (\Exception $e) {

        }
    }

    /**
     * @param mixed $shipment
     *
     * Send new shipment mail to the customer
     */
    public function updateProductInventory($order)
    {
    }
}

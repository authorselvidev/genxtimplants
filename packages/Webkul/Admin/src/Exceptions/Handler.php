<?php

namespace Webkul\Admin\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\PDOException;
use Illuminate\Database\Eloquent\ErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use Mail;

class Handler extends ExceptionHandler
{

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof Exception) {

            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
            if(empty($user))
                $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
                
            $e = FlattenException::create($exception);
            $handler = new SymfonyExceptionHandler();
            $data['exception'] = $handler->getHtml($e);
            $data['request_data'] = json_encode($request->all());
            $data['request_url'] = $request->fullUrl();
            $data['request_ip'] = $request->ip();
            $data['request_method'] = $request->method();
            $data['request_user_agent'] = $request->header('User-Agent');
            $data['request_session'] = json_encode(session()->all());
            
            if($user!=null){
                $data['request_user'] = $user;
                $role_list = ['1'=>'Admin', '2'=>'Dealer', '3'=>'Doctor', '4'=>'Stock'];
                $data['role'] = $role_list[$user->role_id];
            }

            $mail_to_dev  = ['preethi@authorselvi.com', 'deepak@authorselvi.com', 'rajesh.settu@gmail.com', 'baki@authorselvi.com'];
            $mail_to_dev = (!strcmp(request()->getHost(), 'genxtimplants.com'))? $mail_to_dev : [];
            Mail::send('shop::emails.admin.exception-email',  $data, function ($message) use ($mail_to_dev) {
                $message->to($mail_to_dev)->subject('GenXT - Exception has Occured');
            });
        }

        $path = $this->isAdminUri() ? 'admin' : 'shop';

        if ($exception instanceof HttpException) {
            $statusCode = $exception->getStatusCode();
            $statusCode = in_array($statusCode, [401, 403, 404, 503]) ? $statusCode : 500;
            return $this->response($path, $statusCode);
        } else if ($exception instanceof ModelNotFoundException) {
            return $this->response($path, 404);
        } else if ($exception instanceof PDOException) {
            return $this->response($path, 500);
        }

        return parent::render($request, $exception);
    }

    private function isAdminUri()
    {
        return strpos($_SERVER['REQUEST_URI'], 'admin') !== false ? true : false;
    }

    private function response($path, $statusCode)
    {
        return response()->view("{$path}::errors.{$statusCode}", [], $statusCode);
    }

}

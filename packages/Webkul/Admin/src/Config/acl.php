<?php

return [
    [
        'key' => 'dashboard',
        'name' => 'admin::app.acl.dashboard',
        'route' => 'admin.dashboard.index',
        'sort' => 1
    ], [
        'key' => 'sales',
        'name' => 'admin::app.acl.sales',
        'route' => 'admin.sales.orders.index',
        'sort' => 2
    ], [
        'key' => 'sales.orders',
        'name' => 'admin::app.acl.orders',
        'route' => 'admin.sales.orders.index',
        'sort' => 1
    ], [
        'key' => 'sales.invoices',
        'name' => 'admin::app.acl.invoices',
        'route' => 'admin.sales.invoices.index',
        'sort' => 2
    ],
     [
        'key' => 'sales.shipments',
        'name' => 'admin::app.acl.shipments',
        'route' => 'admin.sales.shipments.index',
        'sort' => 3
    ],
    [
        'key' => 'sales.prepayment_orders',
        'name' => 'admin::app.layouts.prepayment_orders',
        'route' => 'admin.sales.prepayment_order.index',
        'sort' => 4,
        'icon-class' => '',
    ], [
        'key' => 'catalog',
        'name' => 'admin::app.acl.catalog',
        'route' => 'admin.catalog.index',
        'sort' => 3
    ], [
        'key' => 'catalog.products',
        'name' => 'admin::app.acl.products',
        'route' => 'admin.catalog.products.index',
        'sort' => 1
    ], [
        'key' => 'catalog.categories',
        'name' => 'admin::app.acl.categories',
        'route' => 'admin.catalog.categories.index',
        'sort' => 2
    ], [
        'key' => 'catalog.attributes',
        'name' => 'admin::app.acl.attributes',
        'route' => 'admin.catalog.attributes.index',
        'sort' => 3
    ], [
        'key' => 'catalog.families',
        'name' => 'admin::app.acl.attribute-families',
        'route' => 'admin.catalog.families.index',
        'sort' => 4
    ],
    [
        'key' => 'discounts',
        'name' => 'admin::app.acl.discounts',
        'route' => 'admin.membership-discount.index',
        'sort' => 4
    ],
    [
        'key' => 'promo_codes',
        'name' => 'admin::app.acl.promo_codes',
        'route' => 'admin.promo-code.index',
        'sort' => 5
    ],
    [
        'key' => 'promotion',
        'name' => 'admin::app.acl.promotion',
        'route' => 'admin.promotion.index',
        'sort' => 6
    ],
   /* [
        'key' => 'dealer_commissions',
        'name' => 'admin::app.acl.dealer_commissions',
        'route' => 'admin.dealer_commission.index',
        'sort' => 7
    ],*/
    [
        'key' => 'events',
        'name' => 'admin::app.acl.events',
        'route' => 'admin.events.index',
        'sort' => 8
    ], 
    [
        'key' => 'courses',
        'name' => 'admin::app.acl.courses',
        'route' => 'admin.courses.index',
        'sort' => 9
    ],
    [
        'key' => 'customers',
        'name' => 'admin::app.acl.customers',
        'route' => 'admin.doctor.index',
        'sort' => 10
    ], [
        'key' => 'customers.customers',
        'name' => 'admin::app.acl.customers',
        'route' => 'admin.doctor.index',
        'sort' => 1
    ], [
        'key' => 'customers.groups',
        'name' => 'admin::app.acl.groups',
        'route' => 'admin.groups.index',
        'sort' => 2
    ], [
        'key' => 'customers.reviews',
        'name' => 'admin::app.acl.reviews',
        'route' => 'admin.customer.reviews.index',
        'sort' => 3
    ],
      [
        'key' => 'credit_payments_approvals',
        'name' => 'Payment Approvals',
        'route' => 'admin.credit_payment.approvals',
        'sort' => 11,
        'icon-class' => 'fa fa-user',
    ],
      [
        'key' => 'credit_payments',
        'name' => 'Advance Payments',
        'route' => 'dealer.credit_payment.pay',
        'sort' => 12,
        'icon-class' => 'fa fa-user',
    ], [
        'key' => 'configuration',
        'name' => 'admin::app.acl.configure',
        'route' => 'admin.configuration.index',
        'sort' => 13
    ], [
        'key' => 'settings',
        'name' => 'admin::app.acl.settings',
        'route' => 'admin.users.index',
        'sort' => 14
    ], [
        'key' => 'settings.locales',
        'name' => 'admin::app.acl.locales',
        'route' => 'admin.locales.index',
        'sort' => 1
    ], [
        'key' => 'settings.currencies',
        'name' => 'admin::app.acl.currencies',
        'route' => 'admin.currencies.index',
        'sort' => 2
    ],
     [
        'key' => 'settings.shipping',
        'name' => 'admin::app.acl.shipping',
        'route' => 'admin.shipping.index',
        'sort' => 3
    ], 
     [
        'key' => 'settings.shipping_carriers',
        'name' => 'admin::app.acl.shipping_carriers',
        'route' => 'admin.shippingcarrier.index',
        'sort' => 4
    ], [
        'key' => 'settings.exchange_rates',
        'name' => 'admin::app.acl.exchange-rates',
        'route' => 'admin.exchange_rates.index',
        'sort' => 5
    ], [
        'key' => 'settings.inventory_sources',
        'name' => 'admin::app.acl.inventory-sources',
        'route' => 'admin.inventory_sources.index',
        'sort' => 6
    ], [
        'key' => 'settings.warehouses',
        'name' => 'admin::app.acl.warehouses',
        'route' => 'admin.warehouse.index',
        'sort' => 7
    ],[
        'key' => 'settings.channels',
        'name' => 'admin::app.acl.channels',
        'route' => 'admin.channels.index',
        'sort' => 8
    ], [
        'key' => 'settings.users',
        'name' => 'admin::app.acl.users',
        'route' => 'admin.users.index',
        'sort' => 9
    ], [
        'key' => 'settings.users.users',
        'name' => 'admin::app.acl.users',
        'route' => 'admin.users.index',
        'sort' => 1
    ], [
        'key' => 'settings.users.roles',
        'name' => 'admin::app.acl.roles',
        'route' => 'admin.roles.index',
        'sort' => 1
    ], [
        'key' => 'settings.sliders',
        'name' => 'admin::app.acl.sliders',
        'route' => 'admin.sliders.index',
        'sort' => 10
    ],
    [
        'key' => 'settings.menus',
        'name' => 'admin::app.acl.menus',
        'route' => 'admin.menus.index',
        'sort' => 11
    ],
    [
        'key' => 'settings.pages',
        'name' => 'admin::app.acl.pages',
        'route' => 'admin.pages.index',
        'sort' => 12
    ],
    [
        'key' => 'settings.points_convertor',
        'name' => 'admin::app.acl.points_convertor',
        'route' => 'admin.points_convertor.index',
        'sort' => 13
    ],
    [
        'key' => 'settings.taxes',
        'name' => 'admin::app.acl.taxes',
        'route' => 'admin.tax-categories.index',
        'sort' => 14
    ],
    [
        'key' => 'settings.taxes.tax-categories',
        'name' => 'admin::app.acl.tax-categories',
        'route' => 'admin.tax-categories.index',
        'sort' => 1
    ], [
        'key' => 'settings.taxes.tax-rates',
        'name' => 'admin::app.acl.tax-rates',
        'route' => 'admin.tax-rates.index',
        'sort' => 2
    ]
];

?>

<?php

/*
Route::get('updateprice', function(){
    $product = \DB::table('product_flat')->addSelect('id','sku', 'price', 'product_id')->where('sku','!=',null)->get();
    foreach($product as $pd){
        \DB::table('product_attribute_values')->where('product_id', $pd->id)->where('attribute_id',11)->update([
            'float_value'=> $pd->price,
        ]);
    }

});
*/

Route::group(['middleware' => ['web']], function () {
     Route::prefix('dealer')->group(function () {
        Route::get('/login', 'Webkul\User\Http\Controllers\SessionController@create')->defaults('_config', [
            'view' => 'admin::users.sessions.create'
        ])->name('admin.session.create');

        //login post route to admin auth controller
        Route::post('/login', 'Webkul\User\Http\Controllers\SessionController@store')->defaults('_config', [
            'redirect' => 'admin.dashboard.index'
        ])->name('admin.session.store');

     });
    Route::prefix('admin')->group(function () {

        Route::get('/', 'Webkul\Admin\Http\Controllers\Controller@redirectToLogin');

        // Login Routes
        Route::get('/login', 'Webkul\User\Http\Controllers\SessionController@create')->defaults('_config', [
            'view' => 'admin::users.sessions.create'
        ])->name('admin.session.create');

        /*Route::get('/testgrid', 'Webkul\Admin\Http\Controllers\DataGridController@testGrid')->defaults('_config', [
            'view' => 'admin::catalog.categories.test'
        ]);*/

        //login post route to admin auth controller
        Route::post('/login', 'Webkul\User\Http\Controllers\SessionController@store')->defaults('_config', [
            'redirect' => 'admin.dashboard.index'
        ])->name('admin.session.store');

        // Forget Password Routes
        Route::get('/forget-password', 'Webkul\User\Http\Controllers\ForgetPasswordController@create')->defaults('_config', [
            'view' => 'admin::users.forget-password.create'
        ])->name('admin.forget-password.create');

        Route::post('/forget-password', 'Webkul\User\Http\Controllers\ForgetPasswordController@store')->name('admin.forget-password.store');

        // Reset Password Routes
        Route::get('/reset-password/{token}', 'Webkul\User\Http\Controllers\ResetPasswordController@create')->defaults('_config', [
            'view' => 'admin::users.reset-password.create'
        ])->name('admin.reset-password.create');

        Route::post('/reset-password', 'Webkul\User\Http\Controllers\ResetPasswordController@store')->defaults('_config', [
            'redirect' => 'admin.dashboard.index'
        ])->name('admin.reset-password.store');


        // Admin Routes
        Route::group(['middleware' => ['admin']], function () {



            // Auth Routes
       // Route::group(['middleware' => ['customer','admin']], function () {


            //Customer Wishlist add
            Route::get('wishlist/add/{id}', 'Webkul\User\Http\Controllers\WishlistController@add')->name('admin.wishlist.add');

            //Customer Wishlist remove
            Route::get('wishlist/remove/{id}', 'Webkul\User\Http\Controllers\WishlistController@remove')->name('admin.wishlist.remove');

            //Customer Wishlist remove
            Route::get('wishlist/removeall', 'Webkul\User\Http\Controllers\WishlistController@removeAll')->name('admin.wishlist.removeall');

            //Customer Wishlist move to cart
            Route::get('wishlist/move/{id}', 'Webkul\User\Http\Controllers\WishlistController@move')->name('admin.wishlist.move');

             //admin credit payment
            Route::get('payments/approvals','Webkul\Admin\Http\Controllers\CreditPaymentsController@approvals')->defaults('_config', [
                    'view' => 'admin::credit_payments.approvals'
                ])->name('admin.credit_payment.approvals');

            Route::get('payments/approvals/approve/{id}', 'Webkul\Admin\Http\Controllers\CreditPaymentsController@approve')->defaults('_config', [
                'redirect' => 'admin.credit_payment.approvals'
            ])->name('admin.credit_payment.approve');

            Route::post('payments/approvals/reject', 'Webkul\Admin\Http\Controllers\CreditPaymentsController@reject')->defaults('_config', [
                'redirect' => 'admin.credit_payment.approvals'
            ])->name('admin.credit_payment.reject');

            Route::post('payments', 'Webkul\Admin\Http\Controllers\CreditPaymentsController@PayViaChequeAndOther')->defaults('_config', [
                    'redirect' => 'admin.dealer.index'
                ])->name('credit_payment.payviachequeandother');

            Route::post('payments/razorpay', 'Webkul\Admin\Http\Controllers\CreditPaymentsController@PayViaRazorpay')->name('credit_payment.payviarazorpay');

            Route::post('payments/addedcreditdue', 'Webkul\Admin\Http\Controllers\CreditDiscountController@addCreditDue')->name('credit_payment.addcreditdue');



            Route::get('commission/{id}', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@show')->defaults('_config', [
                'view' => 'admin::dealers.dealer_commission'
            ])->name('admin.dealer.commission_show');

            //customer account
            Route::prefix('account')->group(function () {
                //Customer Dashboard Route
                Route::get('index', 'Webkul\User\Http\Controllers\AccountController@index')->defaults('_config', [
                    'view' => 'admin::users.account.index'
                ])->name('admin.account.index');

                //Customer Profile Show
                Route::get('profile', 'Webkul\User\Http\Controllers\UserController@profile_index')->defaults('_config', [
                'view' => 'admin::users.account.profile.index'
                ])->name('admin.profile.index');

                //Customer Profile Edit Form Show
                Route::get('profile/edit', 'Webkul\User\Http\Controllers\UserController@editIndex')->defaults('_config', [
                    'view' => 'admin::users.account.profile.edit'
                ])->name('admin.profile.edit');

                //Customer Profile Edit Form Store
                Route::post('profile/edit', 'Webkul\User\Http\Controllers\UserController@updateProfile')->defaults('_config', [
                    'redirect' => 'admin.profile.index'
                ])->name('admin.profile.update');
                /*  Profile Routes Ends Here  */

                /*    Routes for Addresses   */
                //Customer Address Show
                Route::get('addresses', 'Webkul\User\Http\Controllers\AddressController@index')->defaults('_config', [
                    'view' => 'admin::users.account.address.index'
                ])->name('admin.address.index');

                //Customer Address Create Form Show
                Route::get('addresses/create', 'Webkul\User\Http\Controllers\AddressController@create')->defaults('_config', [
                    'view' => 'admin::users.account.address.create'
                ])->name('admin.address.create');

                Route::get('addresses/change', 'Webkul\User\Http\Controllers\AddressController@change')->defaults('_config', [
                    'view' => 'admin::users.account.address.change'
                ])->name('admin.address.change');

                Route::post('addresses/change', 'Webkul\User\Http\Controllers\AddressController@changedAddress')->defaults('_config', [
                    'redirect' => 'shop.checkout.onepage.index'
                ])->name('admin.address.changedAddress');

                //Customer Address Create Form Store
                Route::post('addresses/create', 'Webkul\User\Http\Controllers\AddressController@store')->defaults('_config', [
                    'view' => 'admin::users.account.address.address',
                    'redirect' => 'admin.address.index'
                ])->name('admin.address.store');

                //Customer Address Edit Form Show
                Route::get('addresses/edit/{id}', 'Webkul\User\Http\Controllers\AddressController@edit')->defaults('_config', [
                    'view' => 'admin::users.account.address.edit'
                ])->name('admin.address.edit');

                //Customer Address Edit Form Store
                Route::put('addresses/edit/{id}', 'Webkul\User\Http\Controllers\AddressController@update')->defaults('_config', [
                    'redirect' => 'admin.address.index'
                ])->name('admin.address.update');

                //Customer Address Make Default
                Route::get('addresses/default/{id}', 'Webkul\User\Http\Controllers\AddressController@makeDefault')->name('admin.make.default.address');

                //Customer Address Delete
                Route::get('addresses/delete/{id}', 'Webkul\User\Http\Controllers\AddressController@destroy')->name('admin.address.delete');


                //dealer credit frontend
                Route::get('make-payments','Webkul\User\Http\Controllers\CreditPaymentsController@pay')->defaults('_config', [
                    'view' => 'admin::users.account.credit_payments.pay'
                ])->name('dealer.credit_payment.pay');

                Route::post('make-payments', 'Webkul\User\Http\Controllers\CreditPaymentsController@PayViaCheque')->defaults('_config', [
                    'redirect' => 'dealer.credit_payment.pay'
                ])->name('dealer.credit_payment.payviacheque');

                Route::post('/make-payments/razorpay', 'Webkul\User\Http\Controllers\CreditPaymentsController@PayViaRazorpay')->name('dealer.credit_payment.payviarazorpay');

                
                /* Wishlist route */
                //Customer wishlist(listing)
                Route::get('wishlist', 'Webkul\User\Http\Controllers\WishlistController@index')->defaults('_config', [
                    'view' => 'admin::users.account.wishlist.wishlist'
                ])->name('admin.wishlist.index');

                Route::get('my-offers','Webkul\User\Http\Controllers\MyOffersController@index')->defaults('_config', [
                    'view' => 'admin::users.account.my_offers.index'
                ])->name('customer.myoffers');
                
                /* Failure Implants */

                Route::get('return-implants','Webkul\User\Http\Controllers\FailureImplantsController@index')->defaults('_config', [
                    'view' => 'admin::users.account.failure_implants.index'
                ])->name('admin.failure.implants.index');

                Route::get('return-implants/create','Webkul\User\Http\Controllers\FailureImplantsController@create')->defaults('_config', [
                    'view' => 'admin::users.account.failure_implants.create'
                ])->name('admin.failure.implants.create');

                Route::post('return-implants/store', 'Webkul\User\Http\Controllers\FailureImplantsController@store')->name('admin.failure.implants.store');
                
                Route::get('return-implants/edit/{id}','Webkul\User\Http\Controllers\FailureImplantsController@edit')->defaults('_config', [
                    'view' => 'admin::users.account.failure_implants.edit'
                ])->name('admin.failure.implants.edit');

                Route::post('return-implants/update/{id}', 'Webkul\User\Http\Controllers\FailureImplantsController@update')->name('admin.failure.implants.update');

                Route::get('return-implants/view/{id}','Webkul\User\Http\Controllers\FailureImplantsController@show')->defaults('_config', [
                    'view' => 'admin::users.account.failure_implants.view'
                ])->name('admin.failure.implants.view');     
                
                Route::get('return-implants/delete/{id}','Webkul\User\Http\Controllers\FailureImplantsController@destroy')->name('admin.failure.implants.delete');                
                Route::get('return-implants/print/{id}', 'Webkul\User\Http\Controllers\FailureImplantsController@print')->name('admin.failed-implants.print');                
                Route::POST('return-implants/download', 'Webkul\User\Http\Controllers\FailureImplantsController@download')->name('admin.failed-implants.download');


                // Route::get('failure-implants','Webkul\User\Http\Controllers\FailureImplantsController@index')->defaults('_config', [
                //     'view' => 'admin::users.account.failure_implants.index'
                //     ])->name('admin.failure.implants.index');
                    
                // Route::get('failure-implants/view/{id}','Webkul\User\Http\Controllers\FailureImplantsController@show')->defaults('_config', [
                //     'view' => 'admin::users.account.failure_implants.view'
                //     ])->name('admin.failure.implants.view');
                    
                // Route::get('failure-implants/usedform','Webkul\User\Http\Controllers\FailureImplantsController@usedform')->defaults('_config', [
                //     'view' => 'admin::users.account.failure_implants.usedform'
                //     ])->name('admin.failure.implants.usedform');
                
                // Route::get('failure-implants/usededit/{id}','Webkul\User\Http\Controllers\FailureImplantsController@edit')->defaults('_config', [
                //     'view' => 'admin::users.account.failure_implants.usedformedit'
                //     ])->name('admin.failure.implants.usededit');
                    
                // Route::POST('failed-implants/usedupdate/{id}', 'Webkul\User\Http\Controllers\FailureImplantsController@usedupdate')->name('admin.failed-implants.usedupdate');
                
                
                // Route::post('failure-implants-send/usedform','Webkul\User\Http\Controllers\FailureImplantsController@usedstore')->name('admin.failure.implants.usedsend');
                
                // Route::get('failure-implants/unusedform','Webkul\User\Http\Controllers\FailureImplantsController@unusedform')->defaults('_config', [
                //     'view' => 'admin::users.account.failure_implants.unusedform'
                //     ])->name('admin.failure.implants.unusedform');
                    
                // Route::get('failure-implants/unusededit/{id}','Webkul\User\Http\Controllers\FailureImplantsController@edit')->defaults('_config', [
                //     'view' => 'admin::users.account.failure_implants.unusedformedit'
                //     ])->name('admin.failure.implants.unusededit');
                    
                // Route::POST('failed-implants/unusedupdate/{id}', 'Webkul\User\Http\Controllers\FailureImplantsController@unusedupdate')->name('admin.failed-implants.unusedupdate');
                
                // Route::post('failure-implants-send/unusedform', 'Webkul\User\Http\Controllers\FailureImplantsController@unusedstore')->name('admin.failure.implants.unusedsend');                

                /* Webinar */

                Route::get('webinar/ondemand', 'Webkul\Admin\Http\Controllers\WebinarController@ondemand')->defaults('_config', [
                    'view' => 'admin::webinar.ondemand'
                ])->name('admin.webinar.ondemand');

                Route::post('webinar/ondemand/create', 'Webkul\Admin\Http\Controllers\WebinarController@createCategory')->defaults('_config', [
                    'redirect' => 'admin::webinar.ondemand.create'
                ])->name('admin.webinar.ondemand.createcate');

                Route::post('webinar/ondemand/delete', 'Webkul\Admin\Http\Controllers\WebinarController@ondemandDeleteCategory')->defaults('_config', [
                    'redirect' => 'admin::webinar.ondemand'
                ])->name('admin.webinar.ondemand.deletecate');

                Route::post('webinar/ondemand/deletevideo', 'Webkul\Admin\Http\Controllers\WebinarController@ondemandDeletevideo')->defaults('_config', [
                    'redirect' => 'admin::webinar.ondemand'
                ])->name('admin.webinar.ondemand.deletevideo');

                Route::post('webinar/ondemand/createvideo', 'Webkul\Admin\Http\Controllers\WebinarController@ondemandNewVideo')->defaults('_config', [
                    'redirect' => 'admin::webinar.ondemand'
                ])->name('admin.webinar.ondemand.newvideo');

                Route::post('webinar/ondemand/save', 'Webkul\Admin\Http\Controllers\WebinarController@storeondemand')->name('admin.webinar.storeondemand');

                //dealer
                Route::get('ondemandwebinar', 'Webkul\Customer\Http\Controllers\WebinarController@ondemand')->defaults('_config', [
                    'view' => 'shop::customers.account.webinar.ondemandwebinar'
                ])->name('webinar.ondemand');

                Route::get('webinar', 'Webkul\Customer\Http\Controllers\WebinarController@webinar')->defaults('_config', [
                    'view' => 'shop::customers.account.webinar.webinar'
                ])->name('webinar.index');


                Route::get('webinar/zoomview/{meet}', 'Webkul\Customer\Http\Controllers\WebinarController@liveview')->defaults('_config', [
                    'view' => 'shop::customers.account.webinar.view'
                ])->name('webinar.zoomview');

                Route::get('webinar/zoomlive', function(){
                    return view('shop::customers.account.webinar.live');
                })->name('webinar.zoomview.live');
                

                /* Orders route */
                //Customer orders(listing)
                Route::get('orders', 'Webkul\User\Http\Controllers\OrderController@index')->defaults('_config', [
                    'view' => 'admin::users.account.orders.index'
                ])->name('admin.orders.index');

                //Customer orders view summary and status
                Route::get('orders/view/{id}', 'Webkul\User\Http\Controllers\OrderController@view')->defaults('_config', [
                    'view' => 'admin::users.account.orders.view'
                ])->name('admin.orders.view');

                //Prints invoice
                Route::get('orders/print/{id}', 'Webkul\User\Http\Controllers\OrderController@print')->defaults('_config', [
                    'view' => 'admin::users.account.orders.print'
                ])->name('admin.orders.print');

                /* Reviews route */
                //Customer reviews
                Route::get('reviews', 'Webkul\User\Http\Controllers\ReviewController@index')->defaults('_config', [
                    'view' => 'admin::users.account.reviews.index'
                ])->name('admin.reviews.index');

                //Customer review delete
                Route::get('reviews/delete/{id}', 'Webkul\User\Http\Controllers\ReviewController@destroy')->defaults('_config', [
                    'redirect' => 'admin.reviews.index'
                ])->name('admin.review.delete');

                 //Customer all review delete
                Route::get('reviews/all-delete', 'Webkul\User\Http\Controllers\ReviewController@deleteAll')->defaults('_config', [
                    'redirect' => 'admin.reviews.index'
                ])->name('admin.review.deleteall');
            });
       // });


            Route::get('/logout', 'Webkul\User\Http\Controllers\SessionController@destroy')->defaults('_config', [
                'redirect' => 'admin.session.create'
            ])->name('admin.session.destroy');

            // Dashboard Route
            Route::get('dashboard', 'Webkul\Admin\Http\Controllers\DashboardController@index')->defaults('_config', [
                'view' => 'admin::dashboard.index'
            ])->name('admin.dashboard.index');

            
            //Customers Management Routes
            Route::get('doctors', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@index')->defaults('_config', [
                'view' => 'admin::doctors.index'
            ])->name('admin.doctor.index');

            Route::get('doctors/create', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@create')->defaults('_config',[
                'view' => 'admin::doctors.create'
            ])->name('admin.doctor.create');

            Route::post('doctors/create', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@store')->defaults('_config',[
                'redirect' => 'admin.doctor.index'
            ])->name('admin.doctor.store');

            Route::get('doctors/edit/{id}', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@edit')->defaults('_config',[
                'view' => 'admin::doctors.edit'
            ])->name('admin.doctor.edit');

            Route::put('doctors/edit/{id}', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@update')->defaults('_config', [
                'redirect' => 'admin.doctor.index'
            ])->name('admin.doctor.update');

            

            Route::get('doctors/show/{id}','Webkul\Admin\Http\Controllers\Doctor\DoctorController@show')->defaults('_config', [
                'view' => 'admin::doctors.show'
            ])->name('admin.doctor.show');

            Route::get('doctors/delete/{id}', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@destroy')->name('admin.doctor.delete');

            Route::get('doctors/doctor-approvals', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@DoctorApprovals')->defaults('_config', [
                'view' => 'admin::doctors.doctor-approvals.index'
            ])->name('admin.doctor.approval');

            Route::get('doctors/doctor-approvals/approve/{id}', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@approve')->defaults('_config', [
                'redirect' => 'admin.doctor.index'
            ])->name('admin.doctor.approve');

            Route::get('doctors/doctor-approvals/reject/{id}', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@reject')->defaults('_config', [
                'redirect' => 'admin.doctor.index'
            ])->name('admin.doctor.reject');

            /* */
            Route::get('doctors/unverified', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@unverified')->defaults('_config', [
                'view' => 'admin::doctors.unverified.index'
            ])->name('admin.doctor.unverified.index');

            Route::get('doctors/unverified/verify/{id}', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@verify')->defaults('_config', [
                'redirect' => 'admin.doctor.unverified.index'
            ])->name('admin.doctors.unverified.verify');

            Route::get('doctors/unverified/notify/{id}', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@notify')->defaults('_config', [
                'redirect' => 'admin.doctor.unverified.index'
            ])->name('admin.doctors.unverified.notify');

            Route::get('doctors/unverified/delete/{id}', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@delete')->defaults('_config', [
                'redirect' => 'admin.doctor.unverified.index'
            ])->name('admin.doctor.unverified.delete');

            Route::post('doctors/unverified/massdelete', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@massDelete')->defaults('_config', [
                'redirect' => 'admin.doctor.unverified.index'
            ])->name('admin.doctor.unverified.massDelete');

            //doctors massdelete
            Route::post('doctors/massdelete', 'Webkul\Admin\Http\Controllers\Doctor\DoctorController@massDestroy')->defaults('_config', [
                    'redirect' => 'admin.doctor.index'
            ])->name('admin.doctor.massdelete');

            Route::get('reviews', 'Webkul\Product\Http\Controllers\ReviewController@index')->defaults('_config',[
                'view' => 'admin::customers.reviews.index'
            ])->name('admin.customer.review.index');

            //Dealers Management Routes
            Route::get('dealers', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@index')->defaults('_config', [
                'view' => 'admin::dealers.index'
            ])->name('admin.dealer.index');

            Route::get('dealers/create', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@create')->defaults('_config',[
                'view' => 'admin::dealers.create'
            ])->name('admin.dealer.create');

            Route::post('dealers/create', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@store')->defaults('_config',[
                'redirect' => 'admin.dealer.index'
            ])->name('admin.dealer.store');

            Route::get('dealers/edit/{id}', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@edit')->defaults('_config',[
                'view' => 'admin::dealers.edit'
            ])->name('admin.dealer.edit');

            Route::put('dealers/edit/{id}', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@update')->defaults('_config', [
                'redirect' => 'admin.dealer.index'
            ])->name('admin.dealer.update');

            Route::get('dealers/delete/{id}', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@destroy')->name('admin.dealer.delete');

            Route::get('dealers/show/{id}', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@show')->defaults('_config', [
                'view' => 'admin::dealers.show'
            ])->name('admin.dealer.show');

            //doctors massdelete
            Route::post('dealers/massdelete', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@massDestroy')->defaults('_config', [
                    'redirect' => 'admin.dealer.index'
            ])->name('admin.dealer.massdelete');

            Route::post('dealers/comm_details', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@ShowCommissionDetails')->defaults('_config', [
                    'redirect' => 'admin.dealer.show'
            ])->name('admin.dealer.comm_details');
            
            Route::post('dealers/comm_paid_history', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@CommissionPaidHistory')->defaults('_config', [
                    'redirect' => 'admin.dealer.show'
            ])->name('admin.dealer.comm_paid_history');

            //Dealer Commission pay
            Route::post('dealers/payalldoctors', 'Webkul\Admin\Http\Controllers\Dealer\DealerController@payAllDoctorsCommission')->name('admin.dealer.payall_doctors_commission');

            //dealer commission history
            Route::post('dealers/commission',['as' =>'admin.dealer.commission', 'uses' =>'Webkul\Admin\Http\Controllers\Dealer\DealerController@StoreDealerCommission']);

            Route::post('orders/change-status', 'Webkul\Admin\Http\Controllers\Sales\OrderController@ChangeOrderStatus')->defaults('_config', [
                    'redirect' => 'admin.sales.orders.index'
            ])->name('admin.change.order.status');


            //Reports 
            Route::get('reports/order', 'Webkul\Admin\Http\Controllers\OrderReportsController@index')->defaults('_config', [
                'view' => 'admin::reports.order'
            ])->name('admin.reports.order');
            Route::POST('orderreq', 'Webkul\Admin\Http\Controllers\OrderReportsController@export')->name('admin.reports.orderexport');

            Route::get('reports/stock', 'Webkul\Admin\Http\Controllers\StockReportsController@index')->defaults('_config', [
                'view' => 'admin::reports.stock'
            ])->name('admin.reports.stock');
            Route::POST('stockreq', 'Webkul\Admin\Http\Controllers\StockReportsController@export')->name('admin.reports.stockexport');
			
			Route::get('reports/commission', 'Webkul\Admin\Http\Controllers\CommissionReportsController@index')->defaults('_config', [
                'view' => 'admin::reports.commission'
            ])->name('admin.reports.commission');
            Route::POST('commissionreq', 'Webkul\Admin\Http\Controllers\CommissionReportsController@export')->name('admin.reports.commissionexport');

            Route::get('reports/dealer', 'Webkul\Admin\Http\Controllers\UserReportController@index')->defaults('_config', [
                'view' => 'admin::reports.userreport'
            ])->name('admin.reports.user');

            Route::POST('userreq', 'Webkul\Admin\Http\Controllers\UserReportController@export')->name('admin.reports.userexport');
            
             //warehouse Management Routes
            Route::get('warehouses', 'Webkul\Admin\Http\Controllers\WarehouseController@index')->defaults('_config', [
                'view' => 'admin::settings.warehouses.index'
            ])->name('admin.warehouse.index');

            Route::get('warehouses/create', 'Webkul\Admin\Http\Controllers\WarehouseController@create')->defaults('_config',[
                'view' => 'admin::settings.warehouses.create'
            ])->name('admin.warehouse.create');

            Route::post('warehouses/create', 'Webkul\Admin\Http\Controllers\WarehouseController@store')->defaults('_config',[
                'redirect' => 'admin.warehouse.index'
            ])->name('admin.warehouse.store');

            Route::get('warehouses/edit/{id}', 'Webkul\Admin\Http\Controllers\WarehouseController@edit')->defaults('_config',[
                'view' => 'admin::settings.warehouses.edit'
            ])->name('admin.warehouse.edit');

            Route::patch('warehouses/edit/{id}', 'Webkul\Admin\Http\Controllers\WarehouseController@update')->defaults('_config', [
                'redirect' => 'admin.warehouse.index'
            ])->name('admin.warehouse.update');

            Route::get('warehouses/delete/{id}', 'Webkul\Admin\Http\Controllers\WarehouseController@destroy')->name('admin.warehouse.destroy');

            Route::get('warehouses/show/{id}', 'Webkul\Admin\Http\Controllers\WarehouseController@show')->defaults('_config', [
                'view' => 'admin::settings.warehouses.show'
            ])->name('admin.warehouse.show');

            //doctors massdelete
            Route::post('warehouses/massdelete', 'Webkul\Admin\Http\Controllers\WarehouseController@massDestroy')->defaults('_config', [
                    'redirect' => 'admin.warehouse.index'
            ])->name('admin.warehouse.massdelete');




            Route::get('reviews', 'Webkul\Product\Http\Controllers\ReviewController@index')->defaults('_config',[
                'view' => 'admin::customers.reviews.index'
            ])->name('admin.customer.review.index');

            // Configuration routes
            Route::get('configuration/{slug?}/{slug2?}', 'Webkul\Admin\Http\Controllers\ConfigurationController@index')->defaults('_config', [
                'view' => 'admin::configuration.index'
            ])->name('admin.configuration.index');

            Route::post('configuration/{slug?}/{slug2?}', 'Webkul\Admin\Http\Controllers\ConfigurationController@store')->defaults('_config', [
                'redirect' => 'admin.configuration.index'
            ])->name('admin.configuration.index.store');

            Route::get('configuration/{slug?}/{slug2?}/{path}', 'Webkul\Admin\Http\Controllers\ConfigurationController@download')->defaults('_config', [
                'redirect' => 'admin.configuration.index'
            ])->name('admin.configuration.download');

            // Reviews Routes
            Route::get('reviews/edit/{id}', 'Webkul\Product\Http\Controllers\ReviewController@edit')->defaults('_config',[
                'view' => 'admin::customers.reviews.edit'
            ])->name('admin.customer.review.edit');

            Route::put('reviews/edit/{id}', 'Webkul\Product\Http\Controllers\ReviewController@update')->defaults('_config', [
                'redirect' => 'admin.customer.review.index'
            ])->name('admin.customer.review.update');

            Route::get('reviews/delete/{id}', 'Webkul\Product\Http\Controllers\ReviewController@destroy')->defaults('_config', [
                'redirect' => 'admin.customer.review.index'
            ])->name('admin.customer.review.delete');

            //mass destroy
            Route::post('reviews/massdestroy', 'Webkul\Product\Http\Controllers\ReviewController@massDestroy')->defaults('_config', [
                'redirect' => 'admin.customer.review.index'
            ])->name('admin.customer.review.massdelete');

            //mass update
            Route::post('reviews/massupdate', 'Webkul\Product\Http\Controllers\ReviewController@massUpdate')->defaults('_config', [
                'redirect' => 'admin.customer.review.index'
            ])->name('admin.customer.review.massupdate');

            // Customer Groups Routes
            Route::get('groups', 'Webkul\Admin\Http\Controllers\UserGroupController@index')->defaults('_config',[
                'view' => 'admin::doctors.groups.index'
            ])->name('admin.groups.index');

            Route::get('groups/create', 'Webkul\Admin\Http\Controllers\UserGroupController@create')->defaults('_config',[
                'view' => 'admin::doctors.groups.create'
            ])->name('admin.groups.create');

            Route::post('groups/create', 'Webkul\Admin\Http\Controllers\UserGroupController@store')->defaults('_config',[
                'redirect' => 'admin.groups.index'
            ])->name('admin.groups.store');

            Route::get('groups/edit/{id}', 'Webkul\Admin\Http\Controllers\UserGroupController@edit')->defaults('_config',[
                'view' => 'admin::doctors.groups.edit'
            ])->name('admin.groups.edit');

            Route::put('groups/edit/{id}', 'Webkul\Admin\Http\Controllers\UserGroupController@update')->defaults('_config',[
                'redirect' => 'admin.groups.index'
            ])->name('admin.groups.update');

            Route::get('groups/delete/{id}', 'Webkul\Admin\Http\Controllers\UserGroupController@destroy')->name('admin.groups.delete');

             //custom-orders
            Route::get('/custom-orders/', 'Webkul\Admin\Http\Controllers\CustomOrderController@index')->defaults('_config', [
                    'view' => 'admin::custom-orders.index'
                ])->name('admin.sales.custom_orders.index');

            Route::get('/custom-orders/{id}/create', 'Webkul\Admin\Http\Controllers\CustomOrderController@create')->defaults('_config', [
                    'view' => 'admin::custom-orders.create'
                ])->name('admin.sales.custom_orders.create');

            Route::post('/custom-orders/{id}/create', 'Webkul\Admin\Http\Controllers\CustomOrderController@store')->defaults('_config', [
                    'redirect' => 'admin::custom-orders.index'
                ])->name('admin.sales.custom_orders.store');

            Route::post('/custom-orders/stock-check', 'Webkul\Admin\Http\Controllers\CustomOrderController@stockAvailabilityCheck')->name('admin.sales.custom_orders.stock_check');

            Route::get('/custom-orders/{id}/edit', 'Webkul\Admin\Http\Controllers\CustomOrderController@edit')->defaults('_config', [
                    'view' => 'admin::custom-orders.edit'
                ])->name('admin.sales.custom_orders.edit');

            Route::post('/custom-orders/{id}/edit', 'Webkul\Admin\Http\Controllers\CustomOrderController@update')->defaults('_config', [
                    'redirect' => 'admin::custom-orders.index'
                ])->name('admin.sales.custom_orders.update');

            //manage shopping cart
            Route::get('/custom-orders/{id}/cart', 'Webkul\Admin\Http\Controllers\CustomOrderController@ManageCart')->defaults('_config', [
                    'view' => 'admin::custom-orders.manage-shopping-cart'
                ])->name('admin.sales.custom_orders.cart');

            Route::get('/custom-orders/sendmail/{id}', 'Webkul\Admin\Http\Controllers\CustomOrderController@sendCartMail')->name('admin.sales.custom_orders.send-email');

            Route::get('/custom-orders/{id}/clear-cart-item/{item_id}', 'Webkul\Admin\Http\Controllers\CustomOrderController@ClearCartItem')->name('admin.sales.custom_orders.clear_cart_item');

            Route::get('/custom-orders/{id}/clear-cart', 'Webkul\Admin\Http\Controllers\CustomOrderController@ClearCart')->name('admin.sales.custom_orders.clear_cart');
            Route::get('/custom-orders/remove-cart-discount/{id}', 'Webkul\Admin\Http\Controllers\CustomOrderController@removeCartDiscount')->name('admin.sales.custom_orders.remove_cart_discount');

            // Sales Routes
            Route::prefix('sales')->group(function () {
                // Sales Order Routes
                Route::get('/orders', 'Webkul\Admin\Http\Controllers\Sales\OrderController@index')->defaults('_config', [
                    'view' => 'admin::sales.orders.index'
                ])->name('admin.sales.orders.index');

                Route::get('/orders/view/{id}', 'Webkul\Admin\Http\Controllers\Sales\OrderController@view')->defaults('_config', [
                    'view' => 'admin::sales.orders.view'
                ])->name('admin.sales.orders.view');

                Route::POST('/orders/cancel/{id}', 'Webkul\Admin\Http\Controllers\Sales\OrderController@cancel')->defaults('_config', [
                    'view' => 'admin::sales.orders.cancel'
                ])->name('admin.sales.orders.cancel');

                Route::post('/orders/order-invoice' ,'Webkul\Admin\Http\Controllers\Sales\OrderController@orderInvoice')
                    ->name('admin.sales.orders.order-invoice');

                Route::get('/orders/order-invoice/download/{id}' ,'Webkul\Admin\Http\Controllers\Sales\OrderController@orderInvoiceDownload')
                    ->name('admin.sales.orders.order-invoice-download');

            //prepayment orders
                Route::get('/prepayment-orders', 'Webkul\Admin\Http\Controllers\Sales\PrePaymentOrderController@index')->defaults('_config', [
                    'view' => 'admin::sales.prepayment_orders.index'
                ])->name('admin.sales.prepayment_order.index');

                Route::get('/prepayment-orders/view/{id}', 'Webkul\Admin\Http\Controllers\Sales\PrePaymentOrderController@view')->defaults('_config', [
                    'view' => 'admin::sales.prepayment_orders.view'
                ])->name('admin.sales.prepayment_order.view');

                Route::post('/prepayment-orders/view/{id}/approve', 'Webkul\Admin\Http\Controllers\Sales\PrePaymentOrderController@approveOrder')->defaults('_config', [
                    'redirect' => 'admin.sales.prepayment_order.index'
                ])->name('admin.sales.prepayment_order.approve');


                // Sales Invoices Routes
                Route::get('/invoices', 'Webkul\Admin\Http\Controllers\Sales\InvoiceController@index')->defaults('_config', [
                    'view' => 'admin::sales.invoices.index'
                ])->name('admin.sales.invoices.index');

                Route::get('/invoices/create/{order_id}', 'Webkul\Admin\Http\Controllers\Sales\InvoiceController@create')->defaults('_config', [
                    'view' => 'admin::sales.invoices.create'
                ])->name('admin.sales.invoices.create');

                Route::post('/invoices/create/{order_id}', 'Webkul\Admin\Http\Controllers\Sales\InvoiceController@store')->defaults('_config', [
                    'redirect' => 'admin.sales.orders.view'
                ])->name('admin.sales.invoices.store');

                Route::get('/invoices/view/{id}', 'Webkul\Admin\Http\Controllers\Sales\InvoiceController@view')->defaults('_config', [
                    'view' => 'admin::sales.invoices.view'
                ])->name('admin.sales.invoices.view');

                Route::get('/invoices/print/{id}', 'Webkul\Admin\Http\Controllers\Sales\InvoiceController@print')->defaults('_config', [
                    'view' => 'admin::sales.invoices.print'
                ])->name('admin.sales.invoices.print');


                // Sales Shipments Routes
                Route::get('/shipments', 'Webkul\Admin\Http\Controllers\Sales\ShipmentController@index')->defaults('_config', [
                    'view' => 'admin::sales.shipments.index'
                ])->name('admin.sales.shipments.index');

                Route::get('/shipments/create/{order_id}', 'Webkul\Admin\Http\Controllers\Sales\ShipmentController@create')->defaults('_config', [
                    'view' => 'admin::sales.shipments.create'
                ])->name('admin.sales.shipments.create');

                Route::post('/shipments/create/{order_id}', 'Webkul\Admin\Http\Controllers\Sales\ShipmentController@store')->defaults('_config', [
                    'redirect' => 'admin.sales.orders.view'
                ])->name('admin.sales.shipments.store');

                Route::get('/shipments/view/{id}', 'Webkul\Admin\Http\Controllers\Sales\ShipmentController@view')->defaults('_config', [
                    'view' => 'admin::sales.shipments.view'
                ])->name('admin.sales.shipments.view');

                Route::post('orders/update-tracking-info/{order_id}', 'Webkul\Admin\Http\Controllers\Sales\ShipmentController@UpdateTrackingInfo')->name('admin.update.shipment.tracking');

                 //Dealer commission
                Route::post('/orders/pay-comm/{id}', 'Webkul\Admin\Http\Controllers\Sales\OrderController@PayDealerCommission')->defaults('_config', [
                    'redirect' => 'admin.sales.orders.index'
                ])->name('admin.sales.pay.commission');

                Route::post('/orders/update-comm/{id}', 'Webkul\Admin\Http\Controllers\Sales\OrderController@UpdateDealerCommission')->defaults('_config', [
                    'redirect' => 'admin.sales.orders.index'
                ])->name('admin.sales.update.commission');
            });


                //sales Payment manual entry
                Route::post('/pay-amount/{id}', 'Webkul\Admin\Http\Controllers\Sales\AmountPaidController@store')->defaults('_config', [
                    'redirect' => 'admin.sales.paid_entries.index'
                ])->name('admin.sales.paid_entries.store');

                Route::get('/paid-entries/view/{id}', 'Webkul\Admin\Http\Controllers\Sales\AmountPaidController@view')->defaults('_config', [
                    'view' => 'admin::sales.paid-entries.view'
                ])->name('admin.sales.paid_entries.view');
                Route::get('/paid-entries/delete/{id}', 'Webkul\Admin\Http\Controllers\Sales\AmountPaidController@destroy')->name('admin.sales.paid_entries.delete');

               

                //Dealer Commission History
                Route::get('/dealer-commissions', 'Webkul\Admin\Http\Controllers\Dealer\DealerCommissionController@index')->defaults('_config', [
                    'view' => 'admin::dealer-commissions.index'
                ])->name('admin.dealer_commission.index');
              
                Route::get('/dealer-commissions/show/{dealer_id}', 'Webkul\Admin\Http\Controllers\Dealer\DealerCommissionController@show')->defaults('_config', [
                    'view' => 'admin::dealer-commissions.show'
                ])->name('admin.dealer_commission.show');



            // Catalog Routes
            Route::prefix('catalog')->group(function () {
                Route::get('/sync', 'Webkul\Product\Http\Controllers\ProductController@sync');

                // Catalog Product Routes
                Route::get('/products', 'Webkul\Product\Http\Controllers\ProductController@index')->defaults('_config', [
                    'view' => 'admin::catalog.products.index'
                ])->name('admin.catalog.products.index');

                Route::get('/products/create', 'Webkul\Product\Http\Controllers\ProductController@create')->defaults('_config', [
                    'view' => 'admin::catalog.products.create'
                ])->name('admin.catalog.products.create');

                Route::post('/products/create', 'Webkul\Product\Http\Controllers\ProductController@store')->defaults('_config', [
                    'redirect' => 'admin.catalog.products.edit'
                ])->name('admin.catalog.products.store');

                Route::get('/products/show/{id}', 'Webkul\Product\Http\Controllers\ProductController@show')->defaults('_config', [
                    'view' => 'admin::catalog.products.show'
                ])->name('admin.catalog.products.show');

                Route::get('/products/edit/{id}', 'Webkul\Product\Http\Controllers\ProductController@edit')->defaults('_config', [
                    'view' => 'admin::catalog.products.edit'
                ])->name('admin.catalog.products.edit');

                Route::put('/products/edit/{id}', 'Webkul\Product\Http\Controllers\ProductController@update')->defaults('_config', [
                    'redirect' => 'admin.catalog.products.index'
                ])->name('admin.catalog.products.update');

                //product delete
                Route::get('/products/delete/{id}', 'Webkul\Product\Http\Controllers\ProductController@destroy')->name('admin.catalog.products.delete');

                //product massaction
                Route::post('products/massaction', 'Webkul\Product\Http\Controllers\ProductController@massActionHandler')->name('admin.catalog.products.massaction');

                //product massdelete
                Route::post('products/massdelete', 'Webkul\Product\Http\Controllers\ProductController@massDestroy')->defaults('_config', [
                    'redirect' => 'admin.catalog.products.index'
                ])->name('admin.catalog.products.massdelete');

                //product massupdate
                Route::post('products/massupdate', 'Webkul\Product\Http\Controllers\ProductController@massUpdate')->defaults('_config', [
                    'redirect' => 'admin.catalog.products.index'
                ])->name('admin.catalog.products.massupdate');

                //quantity update
                 Route::post('products/update-qty/{product_id}', 'Webkul\Product\Http\Controllers\ProductController@UpdateQuantityInfo')->name('admin.update.product.quantity');


                // Catalog Category Routes
                Route::get('/categories', 'Webkul\Category\Http\Controllers\CategoryController@index')->defaults('_config', [
                    'view' => 'admin::catalog.categories.index'
                ])->name('admin.catalog.categories.index');

                Route::get('/categories/create', 'Webkul\Category\Http\Controllers\CategoryController@create')->defaults('_config', [
                    'view' => 'admin::catalog.categories.create'
                ])->name('admin.catalog.categories.create');

                Route::post('/categories/create', 'Webkul\Category\Http\Controllers\CategoryController@store')->defaults('_config', [
                    'redirect' => 'admin.catalog.categories.index'
                ])->name('admin.catalog.categories.store');

                Route::get('/categories/edit/{id}', 'Webkul\Category\Http\Controllers\CategoryController@edit')->defaults('_config', [
                    'view' => 'admin::catalog.categories.edit'
                ])->name('admin.catalog.categories.edit');

                Route::put('/categories/edit/{id}', 'Webkul\Category\Http\Controllers\CategoryController@update')->defaults('_config', [
                    'redirect' => 'admin.catalog.categories.index'
                ])->name('admin.catalog.categories.update');

                Route::get('/categories/delete/{id}', 'Webkul\Category\Http\Controllers\CategoryController@destroy')->name('admin.catalog.categories.delete');


                // Catalog Attribute Routes
                Route::get('/attributes', 'Webkul\Attribute\Http\Controllers\AttributeController@index')->defaults('_config', [
                    'view' => 'admin::catalog.attributes.index'
                ])->name('admin.catalog.attributes.index');

                Route::get('/attributes/create', 'Webkul\Attribute\Http\Controllers\AttributeController@create')->defaults('_config', [
                    'view' => 'admin::catalog.attributes.create'
                ])->name('admin.catalog.attributes.create');

                Route::post('/attributes/create', 'Webkul\Attribute\Http\Controllers\AttributeController@store')->defaults('_config', [
                    'redirect' => 'admin.catalog.attributes.index'
                ])->name('admin.catalog.attributes.store');

                Route::get('/attributes/edit/{id}', 'Webkul\Attribute\Http\Controllers\AttributeController@edit')->defaults('_config', [
                    'view' => 'admin::catalog.attributes.edit'
                ])->name('admin.catalog.attributes.edit');

                Route::put('/attributes/edit/{id}', 'Webkul\Attribute\Http\Controllers\AttributeController@update')->defaults('_config', [
                    'redirect' => 'admin.catalog.attributes.index'
                ])->name('admin.catalog.attributes.update');

                Route::get('/attributes/delete/{id}', 'Webkul\Attribute\Http\Controllers\AttributeController@destroy')->name('admin.catalog.attributes.delete');

                Route::post('/attributes/massdelete', 'Webkul\Attribute\Http\Controllers\AttributeController@massDestroy')->name('admin.catalog.attributes.massdelete');

                // Catalog Family Routes
                Route::get('/families', 'Webkul\Attribute\Http\Controllers\AttributeFamilyController@index')->defaults('_config', [
                    'view' => 'admin::catalog.families.index'
                ])->name('admin.catalog.families.index');

                Route::get('/families/create', 'Webkul\Attribute\Http\Controllers\AttributeFamilyController@create')->defaults('_config', [
                    'view' => 'admin::catalog.families.create'
                ])->name('admin.catalog.families.create');

                Route::post('/families/create', 'Webkul\Attribute\Http\Controllers\AttributeFamilyController@store')->defaults('_config', [
                    'redirect' => 'admin.catalog.families.index'
                ])->name('admin.catalog.families.store');

                Route::get('/families/edit/{id}', 'Webkul\Attribute\Http\Controllers\AttributeFamilyController@edit')->defaults('_config', [
                    'view' => 'admin::catalog.families.edit'
                ])->name('admin.catalog.families.edit');

                Route::put('/families/edit/{id}', 'Webkul\Attribute\Http\Controllers\AttributeFamilyController@update')->defaults('_config', [
                    'redirect' => 'admin.catalog.families.index'
                ])->name('admin.catalog.families.update');

                Route::get('/families/delete/{id}', 'Webkul\Attribute\Http\Controllers\AttributeFamilyController@destroy')->name('admin.catalog.families.delete');
                
                //Admin Failed Implants Routes
                
                Route::get('/return-implants', 'Webkul\Admin\Http\Controllers\FailedImplantsController@index')->defaults('_config', [
                    'view' => 'admin::catalog.failed-implants.index'
                ])->name('admin.catalog.failed-implants.index');
                
                Route::get('/return-implants/approval', 'Webkul\Admin\Http\Controllers\FailedImplantsController@approvals')->defaults('_config', [
                    'view' => 'admin::catalog.failed-implants.index'
                ])->name('admin.catalog.failed-implants.approval');
                
                Route::get('/return-implants/rejected', 'Webkul\Admin\Http\Controllers\FailedImplantsController@rejected')->defaults('_config', [
                    'view' => 'admin::catalog.failed-implants.index'
                ])->name('admin.catalog.failed-implants.rejected');
                
                //Route::get('/failed-implants/delete/{id}', 'Webkul\Admin\Http\Controllers\FailedImplantsController@destroy')->name('admin.catalog.failed-implants.delete');
                
                Route::get('/return-implants/view/{id}', 'Webkul\Admin\Http\Controllers\FailedImplantsController@show')->defaults('_config', ['view' => 'admin::catalog.failed-implants.view'])->name('admin.catalog.failed-implants.view');
                
                Route::get('/return-implants/view/approved/{id}', 'Webkul\Admin\Http\Controllers\FailedImplantsController@show')->defaults('_config', ['view' => 'admin::catalog.failed-implants.view'])->name('admin.catalog.failed-implants.view');
                
                Route::POST('/return-implants/download', 'Webkul\Admin\Http\Controllers\FailedImplantsController@download')->name('admin.catalog.failed-implants.download');
                
                Route::POST('/return-implants/update', 'Webkul\Admin\Http\Controllers\FailedImplantsController@update')->name('admin.catalog.failed-implants.update');
            });

            // User Routes
            //datagrid for backend users
            Route::get('/users', 'Webkul\User\Http\Controllers\UserController@index')->defaults('_config', [
                'view' => 'admin::users.users.index'
            ])->name('admin.users.index');

            //create backend user get
            Route::get('/users/create', 'Webkul\User\Http\Controllers\UserController@create')->defaults('_config', [
                'view' => 'admin::users.users.create'
            ])->name('admin.users.create');

            //create backend user post
            Route::post('/users/create', 'Webkul\User\Http\Controllers\UserController@store')->defaults('_config', [
                'redirect' => 'admin.users.index'
            ])->name('admin.users.store');

            //delete backend user view
            Route::get('/users/edit/{id}', 'Webkul\User\Http\Controllers\UserController@edit')->defaults('_config', [
                'view' => 'admin::users.users.edit'
            ])->name('admin.users.edit');

            //edit backend user submit
            Route::put('/users/edit/{id}', 'Webkul\User\Http\Controllers\UserController@update')->defaults('_config', [
                'redirect' => 'admin.users.index'
            ])->name('admin.users.update');

            //delete backend user
            Route::get('/users/delete/{id}', 'Webkul\User\Http\Controllers\UserController@destroy')->name('admin.users.delete');

            Route::post('/confirm/destroy', 'Webkul\User\Http\Controllers\UserController@destroySelf')->defaults('_config', [
                'redirect' => 'admin.users.index'
            ])->name('admin.users.confirm.destroy');

            // User Role Routes
            Route::get('/roles', 'Webkul\User\Http\Controllers\RoleController@index')->defaults('_config', [
                'view' => 'admin::users.roles.index'
            ])->name('admin.roles.index');

            Route::get('/roles/create', 'Webkul\User\Http\Controllers\RoleController@create')->defaults('_config', [
                'view' => 'admin::users.roles.create'
            ])->name('admin.roles.create');

            Route::post('/roles/create', 'Webkul\User\Http\Controllers\RoleController@store')->defaults('_config', [
                'redirect' => 'admin.roles.index'
            ])->name('admin.roles.store');

            Route::get('/roles/edit/{id}', 'Webkul\User\Http\Controllers\RoleController@edit')->defaults('_config', [
                'view' => 'admin::users.roles.edit'
            ])->name('admin.roles.edit');

            Route::put('/roles/edit/{id}', 'Webkul\User\Http\Controllers\RoleController@update')->defaults('_config', [
                'redirect' => 'admin.roles.index'
            ])->name('admin.roles.update');


            // Locale Routes
            Route::get('/locales', 'Webkul\Core\Http\Controllers\LocaleController@index')->defaults('_config', [
                'view' => 'admin::settings.locales.index'
            ])->name('admin.locales.index');

            Route::get('/locales/create', 'Webkul\Core\Http\Controllers\LocaleController@create')->defaults('_config', [
                'view' => 'admin::settings.locales.create'
            ])->name('admin.locales.create');

            Route::post('/locales/create', 'Webkul\Core\Http\Controllers\LocaleController@store')->defaults('_config', [
                'redirect' => 'admin.locales.index'
            ])->name('admin.locales.store');

            Route::get('/locales/edit/{id}', 'Webkul\Core\Http\Controllers\LocaleController@edit')->defaults('_config', [
                'view' => 'admin::settings.locales.edit'
            ])->name('admin.locales.edit');

            Route::put('/locales/edit/{id}', 'Webkul\Core\Http\Controllers\LocaleController@update')->defaults('_config', [
                'redirect' => 'admin.locales.index'
            ])->name('admin.locales.update');

            Route::get('/locales/delete/{id}', 'Webkul\Core\Http\Controllers\LocaleController@destroy')->name('admin.locales.delete');


            // Currency Routes
            Route::get('/currencies', 'Webkul\Core\Http\Controllers\CurrencyController@index')->defaults('_config', [
                'view' => 'admin::settings.currencies.index'
            ])->name('admin.currencies.index');

            Route::get('/currencies/create', 'Webkul\Core\Http\Controllers\CurrencyController@create')->defaults('_config', [
                'view' => 'admin::settings.currencies.create'
            ])->name('admin.currencies.create');

            Route::post('/currencies/create', 'Webkul\Core\Http\Controllers\CurrencyController@store')->defaults('_config', [
                'redirect' => 'admin.currencies.index'
            ])->name('admin.currencies.store');

            Route::get('/currencies/edit/{id}', 'Webkul\Core\Http\Controllers\CurrencyController@edit')->defaults('_config', [
                'view' => 'admin::settings.currencies.edit'
            ])->name('admin.currencies.edit');

            Route::put('/currencies/edit/{id}', 'Webkul\Core\Http\Controllers\CurrencyController@update')->defaults('_config', [
                'redirect' => 'admin.currencies.index'
            ])->name('admin.currencies.update');

            Route::get('/currencies/delete/{id}', 'Webkul\Core\Http\Controllers\CurrencyController@destroy')->name('admin.currencies.delete');

            Route::post('/currencies/massdelete', 'Webkul\Core\Http\Controllers\CurrencyController@massDestroy')->name('admin.currencies.massdelete');


            // Exchange Rates Routes
            Route::get('/exchange_rates', 'Webkul\Core\Http\Controllers\ExchangeRateController@index')->defaults('_config', [
                'view' => 'admin::settings.exchange_rates.index'
            ])->name('admin.exchange_rates.index');

            Route::get('/exchange_rates/create', 'Webkul\Core\Http\Controllers\ExchangeRateController@create')->defaults('_config', [
                'view' => 'admin::settings.exchange_rates.create'
            ])->name('admin.exchange_rates.create');

            Route::post('/exchange_rates/create', 'Webkul\Core\Http\Controllers\ExchangeRateController@store')->defaults('_config', [
                'redirect' => 'admin.exchange_rates.index'
            ])->name('admin.exchange_rates.store');

            Route::get('/exchange_rates/edit/{id}', 'Webkul\Core\Http\Controllers\ExchangeRateController@edit')->defaults('_config', [
                'view' => 'admin::settings.exchange_rates.edit'
            ])->name('admin.exchange_rates.edit');

            Route::put('/exchange_rates/edit/{id}', 'Webkul\Core\Http\Controllers\ExchangeRateController@update')->defaults('_config', [
                'redirect' => 'admin.exchange_rates.index'
            ])->name('admin.exchange_rates.update');

            Route::get('/exchange_rates/delete/{id}', 'Webkul\Core\Http\Controllers\ExchangeRateController@destroy')->name('admin.exchange_rates.delete');


            // Inventory Source Routes
            Route::get('/inventory_sources', 'Webkul\Inventory\Http\Controllers\InventorySourceController@index')->defaults('_config', [
                'view' => 'admin::settings.inventory_sources.index'
            ])->name('admin.inventory_sources.index');

            Route::get('/inventory_sources/create', 'Webkul\Inventory\Http\Controllers\InventorySourceController@create')->defaults('_config', [
                'view' => 'admin::settings.inventory_sources.create'
            ])->name('admin.inventory_sources.create');

            Route::post('/inventory_sources/create', 'Webkul\Inventory\Http\Controllers\InventorySourceController@store')->defaults('_config', [
                'redirect' => 'admin.inventory_sources.index'
            ])->name('admin.inventory_sources.store');

            Route::get('/inventory_sources/edit/{id}', 'Webkul\Inventory\Http\Controllers\InventorySourceController@edit')->defaults('_config', [
                'view' => 'admin::settings.inventory_sources.edit'
            ])->name('admin.inventory_sources.edit');

            Route::put('/inventory_sources/edit/{id}', 'Webkul\Inventory\Http\Controllers\InventorySourceController@update')->defaults('_config', [
                'redirect' => 'admin.inventory_sources.index'
            ])->name('admin.inventory_sources.update');

            Route::get('/inventory_sources/delete/{id}', 'Webkul\Inventory\Http\Controllers\InventorySourceController@destroy')->name('admin.inventory_sources.delete');

            // Channel Routes
            Route::get('/channels', 'Webkul\Core\Http\Controllers\ChannelController@index')->defaults('_config', [
                'view' => 'admin::settings.channels.index'
            ])->name('admin.channels.index');

            Route::get('/channels/create', 'Webkul\Core\Http\Controllers\ChannelController@create')->defaults('_config', [
                'view' => 'admin::settings.channels.create'
            ])->name('admin.channels.create');

            Route::post('/channels/create', 'Webkul\Core\Http\Controllers\ChannelController@store')->defaults('_config', [
                'redirect' => 'admin.channels.index'
            ])->name('admin.channels.store');

            Route::get('/channels/edit/{id}', 'Webkul\Core\Http\Controllers\ChannelController@edit')->defaults('_config', [
                'view' => 'admin::settings.channels.edit'
            ])->name('admin.channels.edit');

            Route::put('/channels/edit/{id}', 'Webkul\Core\Http\Controllers\ChannelController@update')->defaults('_config', [
                'redirect' => 'admin.channels.index'
            ])->name('admin.channels.update');

            Route::get('/channels/delete/{id}', 'Webkul\Core\Http\Controllers\ChannelController@destroy')->name('admin.channels.delete');


            // Admin Profile route
            Route::get('/account', 'Webkul\User\Http\Controllers\AccountController@edit')->defaults('_config', [
                'view' => 'admin::account.edit'
            ])->name('admin.account.edit');

            Route::put('/account', 'Webkul\User\Http\Controllers\AccountController@update')->name('admin.account.update');

            //API Authorizations
            // Route::get('/api/clients', 'Webkul\Admin\Http\Controllers\AuthorizationController@show')->defaults('_config', [
            //     'view' => 'admin::apiauth.client'
            //  ])->name('admin.index.oauth.client');

            //  //view an OAuth API Client
            //  Route::get('/api/clients/view/{id}', 'Webkul\Admin\Http\Controllers\AuthorizationController@view')->defaults('_config', [
            //     'view' => 'admin::apiauth.view'
            //  ])->name('admin.view.oauth.client');

            // //edit an OAuth API Client
            // Route::get('/api/clients/delete/{id}', 'Webkul\Admin\Http\Controllers\AuthorizationController@delete')->defaults('_config', [
            //     'view' => 'admin::apiauth.edit'
            // ])->name('admin.delete.oauth.client');


            // Admin Store Front Settings Route
            Route::get('/subscribers','Webkul\Core\Http\Controllers\SubscriptionController@index')->defaults('_config',[
                'view' => 'admin::customers.subscribers.index'
            ])->name('admin.customers.subscribers.index');

            //destroy a newsletter subscription item
            Route::get('subscribers/delete/{id}', 'Webkul\Core\Http\Controllers\SubscriptionController@destroy')->name('admin.customers.subscribers.delete');

            Route::get('subscribers/edit/{id}', 'Webkul\Core\Http\Controllers\SubscriptionController@edit')->defaults('_config', [
                'view' => 'admin::customers.subscribers.edit'
            ])->name('admin.customers.subscribers.edit');

            Route::put('subscribers/update/{id}', 'Webkul\Core\Http\Controllers\SubscriptionController@update')->defaults('_config', [
                'redirect' => 'admin.customers.subscribers.index'
            ])->name('admin.customers.subscribers.update');

            //slider index
            Route::get('/slider','Webkul\Shop\Http\Controllers\SliderController@index')->defaults('_config',[
                'view' => 'admin::settings.sliders.index'
            ])->name('admin.sliders.index');

            //slider create show
            Route::get('slider/create','Webkul\Shop\Http\Controllers\SliderController@create')->defaults('_config',[
                'view' => 'admin::settings.sliders.create'
            ])->name('admin.sliders.create');

            //slider create show
            Route::post('slider/create','Webkul\Shop\Http\Controllers\SliderController@store')->defaults('_config',[
                'redirect' => 'admin.sliders.index'
            ])->name('admin.sliders.store');

            //slider edit show
            Route::get('slider/edit/{id}','Webkul\Shop\Http\Controllers\SliderController@edit')->defaults('_config',[
                'view' => 'admin::settings.sliders.edit'
            ])->name('admin.sliders.edit');

            //slider edit update
            Route::post('slider/edit/{id}','Webkul\Shop\Http\Controllers\SliderController@update')->defaults('_config',[
                'redirect' => 'admin.sliders.index'
            ])->name('admin.sliders.update');

            //destroy a slider item
            Route::get('slider/delete/{id}', 'Webkul\Shop\Http\Controllers\SliderController@destroy')->name('admin.sliders.delete');

            //menu routes
            Route::get('/menus',['as' =>'admin.menus.index', 'uses' =>'Webkul\Admin\Http\Controllers\MenusController@index']);

            Route::get('menus/create',['as' =>'admin.menus.create', 'uses' =>'Webkul\Admin\Http\Controllers\MenusController@create']);

             Route::post('menus/create',['as' =>'admin.menus.store', 'uses' =>'Webkul\Admin\Http\Controllers\MenusController@store']);

             Route::get('menus/edit/{id}',['as' =>'admin.menus.edit', 'uses' =>'Webkul\Admin\Http\Controllers\MenusController@edit']);

             Route::patch('menus/edit/{id}',['as' =>'admin.menus.update', 'uses' =>'Webkul\Admin\Http\Controllers\MenusController@update']);


             Route::get('menus/delete/{id}',['as' =>'admin.menus.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\MenusController@destroy']);

            //membership discounts
            Route::get('membership-discounts', 'Webkul\Admin\Http\Controllers\MemberShipDiscountController@index')->defaults('_config', [
                'view' => 'admin::membership-discounts.index'
            ])->name('admin.membership-discount.index');

            Route::get('membership-discounts/create',['as' =>'admin.membership-discount.create', 'uses' =>'Webkul\Admin\Http\Controllers\MemberShipDiscountController@create']);

             Route::post('membership-discounts/create',['as' =>'admin.membership-discount.store', 'uses' =>'Webkul\Admin\Http\Controllers\MemberShipDiscountController@store']);

             Route::get('membership-discounts/edit/{id}',['as' =>'admin.membership-discount.edit', 'uses' =>'Webkul\Admin\Http\Controllers\MemberShipDiscountController@edit']);

             Route::patch('membership-discounts/edit/{id}',['as' =>'admin.membership-discount.update', 'uses' =>'Webkul\Admin\Http\Controllers\MemberShipDiscountController@update']);


             Route::get('membership-discounts/delete/{id}',['as' =>'admin.membership-discount.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\MemberShipDiscountController@destroy']);

             Route::get('membership-discounts/status/change',['as' =>'admin.membership-discount.changeStatus', 'uses' =>'Webkul\Admin\Http\Controllers\MemberShipDiscountController@changeStatus']);


            //promo code discounts
            Route::get('promo-codes', 'Webkul\Admin\Http\Controllers\PromoCodeController@index')->defaults('_config', [
                'view' => 'admin::promo-codes.index'
            ])->name('admin.promo-code.index');

            Route::get('promo-codes/create',['as' =>'admin.promo-code.create', 'uses' =>'Webkul\Admin\Http\Controllers\PromoCodeController@create']);

             Route::post('promo-codes/create',['as' =>'admin.promo-code.store', 'uses' =>'Webkul\Admin\Http\Controllers\PromoCodeController@store']);

             Route::get('promo-codes/edit/{id}',['as' =>'admin.promo-code.edit', 'uses' =>'Webkul\Admin\Http\Controllers\PromoCodeController@edit']);

             Route::patch('promo-codes/edit/{id}',['as' =>'admin.promo-code.update', 'uses' =>'Webkul\Admin\Http\Controllers\PromoCodeController@update']);


             Route::get('promo-codes/delete/{id}',['as' =>'admin.promo-code.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\PromoCodeController@destroy']);

             Route::get('promo-codes/status/change',['as' =>'admin.promo-code.status', 'uses' =>'Webkul\Admin\Http\Controllers\PromoCodeController@changeStatus']);

            //payment discount discounts
            Route::get('payment-discount', 'Webkul\Admin\Http\Controllers\CreditDiscountController@index')->name('admin.credit-discount.index');

            Route::get('payment-discount/create' , 'Webkul\Admin\Http\Controllers\CreditDiscountController@create')->name('admin.credit-discount.create');
            
            Route::post('payment-discount/create' , 'Webkul\Admin\Http\Controllers\CreditDiscountController@store')->name('admin.credit-discount.store');
            
            Route::get('payment-discount/edit/{id}', 'Webkul\Admin\Http\Controllers\CreditDiscountController@edit')->name('admin.credit-discount.edit');

            Route::get('payment-discount/status/change', 'Webkul\Admin\Http\Controllers\CreditDiscountController@changeStatus')->name('admin.credit-discount.status');

            Route::patch('payment-discount/edit/{id}', 'Webkul\Admin\Http\Controllers\CreditDiscountController@update')->name('admin.credit-discount.update');

            Route::get('payment-discount/delete/{id}','Webkul\Admin\Http\Controllers\CreditDiscountController@destroy')->name('admin.credit-discount.destroy');


            //promotion discounts
            Route::get('promotions', 'Webkul\Admin\Http\Controllers\PromotionController@index')->defaults('_config', [
                'view' => 'admin::promotions.index'
            ])->name('admin.promotion.index');

            Route::get('promotions/create',['as' =>'admin.promotion.create', 'uses' =>'Webkul\Admin\Http\Controllers\PromotionController@create']);

             Route::post('promotions/create',['as' =>'admin.promotion.store', 'uses' =>'Webkul\Admin\Http\Controllers\PromotionController@store']);

             Route::get('promotions/edit/{id}',['as' =>'admin.promotion.edit', 'uses' =>'Webkul\Admin\Http\Controllers\PromotionController@edit']);

             Route::patch('promotions/edit/{id}',['as' =>'admin.promotion.update', 'uses' =>'Webkul\Admin\Http\Controllers\PromotionController@update']);


             Route::get('promotions/delete/{id}',['as' =>'admin.promotion.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\PromotionController@destroy']);



             //pages
             Route::get('/pages',['as' =>'admin.pages.index', 'uses' =>'Webkul\Admin\Http\Controllers\PagesController@index']);

            Route::get('pages/create',['as' =>'admin.pages.create', 'uses' =>'Webkul\Admin\Http\Controllers\PagesController@create']);

             Route::post('pages/create',['as' =>'admin.pages.store', 'uses' =>'Webkul\Admin\Http\Controllers\PagesController@store']);

             Route::get('pages/edit/{id}',['as' =>'admin.pages.edit', 'uses' =>'Webkul\Admin\Http\Controllers\PagesController@edit']);

             Route::patch('pages/edit/{id}',['as' =>'admin.pages.update', 'uses' =>'Webkul\Admin\Http\Controllers\PagesController@update']);


             Route::get('pages/delete/{id}',['as' =>'admin.pages.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\PagesController@destroy']);

             Route::post('/pages/change',['as' =>'admin.pages.home_page', 'uses' =>'Webkul\Admin\Http\Controllers\PagesController@ChangeHomePage']);

            Route::post('/pages/uploadImage',['as' =>'admin.pages.upload', 'uses' =>'Webkul\Admin\Http\Controllers\PagesController@imageUploader']);



            //events
             Route::get('/events',['as' =>'admin.events.index', 'uses' =>'Webkul\Admin\Http\Controllers\EventsController@index']);

            Route::get('events/create',['as' =>'admin.events.create', 'uses' =>'Webkul\Admin\Http\Controllers\EventsController@create']);

             Route::post('events/create',['as' =>'admin.events.store', 'uses' =>'Webkul\Admin\Http\Controllers\EventsController@store']);

             Route::get('events/edit/{id}',['as' =>'admin.events.edit', 'uses' =>'Webkul\Admin\Http\Controllers\EventsController@edit']);

             Route::patch('events/edit/{id}',['as' =>'admin.events.update', 'uses' =>'Webkul\Admin\Http\Controllers\EventsController@update']);


             Route::get('events/delete/{id}',['as' =>'admin.events.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\EventsController@destroy']);


              //courses
            Route::get('/courses',['as' =>'admin.courses.index', 'uses' =>'Webkul\Admin\Http\Controllers\CoursesController@index']);

            Route::get('courses/create',['as' =>'admin.courses.create', 'uses' =>'Webkul\Admin\Http\Controllers\CoursesController@create']);

             Route::post('courses/create',['as' =>'admin.courses.store', 'uses' =>'Webkul\Admin\Http\Controllers\CoursesController@store']);

             Route::get('courses/edit/{id}',['as' =>'admin.courses.edit', 'uses' =>'Webkul\Admin\Http\Controllers\CoursesController@edit']);

             Route::patch('courses/edit/{id}',['as' =>'admin.courses.update', 'uses' =>'Webkul\Admin\Http\Controllers\CoursesController@update']);


            Route::get('courses/delete/{id}',['as' =>'admin.courses.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\CoursesController@destroy']);

            Route::get('courses/image-delete/{image_id}',['as' =>'admin.courses.delete', 'uses' =>'Webkul\Admin\Http\Controllers\CoursesController@deleteImages']);




            //points_convertor
            Route::get('/points_convertor',['as' =>'admin.points_convertor.index', 'uses' =>'Webkul\Admin\Http\Controllers\PointsConvertorController@index']);
           
             Route::post('points_convertor/create',['as' =>'admin.points_convertor.store', 'uses' =>'Webkul\Admin\Http\Controllers\PointsConvertorController@store']);


             //shipping rates
             Route::get('/shipping',['as' =>'admin.shipping.index', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingController@index']);

            Route::get('shipping/create',['as' =>'admin.shipping.create', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingController@create']);

             Route::post('shipping/create',['as' =>'admin.shipping.store', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingController@store']);

             Route::get('shipping/edit/{id}',['as' =>'admin.shipping.edit', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingController@edit']);

             Route::patch('shipping/edit/{id}',['as' =>'admin.shipping.update', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingController@update']);


             Route::get('shipping/delete/{id}',['as' =>'admin.shipping.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingController@destroy']);



            //shipping carriers
              Route::get('/shipping-carriers',['as' =>'admin.shippingcarrier.index', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingCarrierController@index']);

            Route::get('shipping-carriers/create',['as' =>'admin.shippingcarrier.create', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingCarrierController@create']);

             Route::post('shipping-carriers/create',['as' =>'admin.shippingcarrier.store', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingCarrierController@store']);

             Route::get('shipping-carriers/edit/{id}',['as' =>'admin.shippingcarrier.edit', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingCarrierController@edit']);

             Route::patch('shipping-carriers/edit/{id}',['as' =>'admin.shippingcarrier.update', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingCarrierController@update']);


             Route::get('shipping-carriers/delete/{id}',['as' =>'admin.shippingcarrier.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\ShippingCarrierController@destroy']);


            //support and sales
             Route::get('/support_and_sales',['as' =>'admin.support_and_sales.index', 'uses' =>'Webkul\Admin\Http\Controllers\SupportAndSalesController@index']);

            Route::get('support_and_sales/create',['as' =>'admin.support_and_sales.create', 'uses' =>'Webkul\Admin\Http\Controllers\SupportAndSalesController@create']);

             Route::post('support_and_sales/create',['as' =>'admin.support_and_sales.store', 'uses' =>'Webkul\Admin\Http\Controllers\SupportAndSalesController@store']);

             Route::get('support_and_sales/edit/{id}',['as' =>'admin.support_and_sales.edit', 'uses' =>'Webkul\Admin\Http\Controllers\SupportAndSalesController@edit']);

             Route::patch('support_and_sales/edit/{id}',['as' =>'admin.support_and_sales.update', 'uses' =>'Webkul\Admin\Http\Controllers\SupportAndSalesController@update']);


             Route::get('support_and_sales/delete/{id}',['as' =>'admin.support_and_sales.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\SupportAndSalesController@destroy']);


            //media
             Route::get('/media',['as' =>'admin.media.index', 'uses' =>'Webkul\Admin\Http\Controllers\MediaController@index']);

            Route::get('media/create',['as' =>'admin.media.create', 'uses' =>'Webkul\Admin\Http\Controllers\MediaController@create']);

             Route::post('media/create',['as' =>'admin.media.store', 'uses' =>'Webkul\Admin\Http\Controllers\MediaController@store']);


             Route::get('media/delete/{id}',['as' =>'admin.media.destroy', 'uses' =>'Webkul\Admin\Http\Controllers\MediaController@destroy']);

            //tax routes
            Route::get('/tax-categories', 'Webkul\Tax\Http\Controllers\TaxController@index')->defaults('_config', [
                'view' => 'admin::tax.tax-categories.index'
            ])->name('admin.tax-categories.index');


            // tax category routes
            Route::get('/tax-categories/create', 'Webkul\Tax\Http\Controllers\TaxCategoryController@show')->defaults('_config', [
                'view' => 'admin::tax.tax-categories.create'
            ])->name('admin.tax-categories.show');

            Route::post('/tax-categories/create', 'Webkul\Tax\Http\Controllers\TaxCategoryController@create')->defaults('_config', [
                'redirect' => 'admin.tax-categories.index'
            ])->name('admin.tax-categories.create');

            Route::get('/tax-categories/edit/{id}', 'Webkul\Tax\Http\Controllers\TaxCategoryController@edit')->defaults('_config', [
                'view' => 'admin::tax.tax-categories.edit'
            ])->name('admin.tax-categories.edit');

            Route::put('/tax-categories/edit/{id}', 'Webkul\Tax\Http\Controllers\TaxCategoryController@update')->defaults('_config', [
                'redirect' => 'admin.tax-categories.index'
            ])->name('admin.tax-categories.update');

            Route::get('/tax-categories/delete/{id}', 'Webkul\Tax\Http\Controllers\TaxCategoryController@destroy')->name('admin.tax-categories.delete');
            //tax category ends


            //tax rate
            Route::get('tax-rates', 'Webkul\Tax\Http\Controllers\TaxRateController@index')->defaults('_config', [
                'view' => 'admin::tax.tax-rates.index'
            ])->name('admin.tax-rates.index');

            Route::get('tax-rates/create', 'Webkul\Tax\Http\Controllers\TaxRateController@show')->defaults('_config', [
                'view' => 'admin::tax.tax-rates.create'
            ])->name('admin.tax-rates.show');

            Route::post('tax-rates/create', 'Webkul\Tax\Http\Controllers\TaxRateController@create')->defaults('_config', [
                'redirect' => 'admin.tax-rates.index'
            ])->name('admin.tax-rates.create');

            Route::get('tax-rates/edit/{id}', 'Webkul\Tax\Http\Controllers\TaxRateController@edit')->defaults('_config', [
                'view' => 'admin::tax.tax-rates.edit'
            ])->name('admin.tax-rates.store');

            Route::put('tax-rates/update/{id}', 'Webkul\Tax\Http\Controllers\TaxRateController@update')->defaults('_config', [
                'redirect' => 'admin.tax-rates.index'
            ])->name('admin.tax-rates.update');

            Route::get('/tax-rates/delete/{id}', 'Webkul\Tax\Http\Controllers\TaxRateController@destroy')->name('admin.tax-rates.delete');
            //tax rate ends

            //DataGrid Export
            Route::post('admin/export', 'Webkul\Admin\Http\Controllers\ExportController@export')->name('admin.datagrid.export');

        });
    });
});

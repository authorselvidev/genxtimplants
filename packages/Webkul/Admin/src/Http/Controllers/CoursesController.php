<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$courses = DB::table('courses')->where('is_deleted',0)->get();
        return view('admin::courses.index',compact('courses',$courses));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //dd($request->all());
       $this->validate(request(), [
            'course_name' => 'string|required',
            'course_slug' => 'required',
            'course_content' => 'required',
        ]);

        $course_id = DB::table('courses')->insertGetId(['name' => $request->get('course_name'),
        									'slug' => $request->get('course_slug'),
                                            'course_date' => $request->get('course_date'),
                                            'venue' => $request->get('course_venue'),
                                            'contact' => $request->get('course_contact'),
                                            'course_highlights' => $request->get('course_highlights'),
                                            'description' => $request->get('course_content'),
                                            'faculty' => $request->get('course_faculty'),
                                            'review' => $request->get('course_review'),
                                            'register_form' => ($request->get('course_reg') !=null)?1:0,
                                            'start_date' => $request->get('start_date'),
                                            'end_date' => $request->get('end_date')
                                        ]);

        //dd($course_id);
          if($request->hasfile('course_images_set_1'))
         {

            foreach($request->file('course_images_set_1') as $course_image)
            {
                $name=$course_image->getClientOriginalName();
                $course_image->move(public_path().'/images/', $name);  
                $upload_images= DB::table('course_images')->insert([
                                        'slider_set' => 1,
                                        'course_id' => $course_id,
                                        'name' => $name,
                                        'created_at' => date('Y-m-d H:i:s'),
                ]);
            }
         }

         if($request->hasfile('course_images_set_2'))
         {

            foreach($request->file('course_images_set_2') as $course_image)
            {
                $name=$course_image->getClientOriginalName();
                $course_image->move(public_path().'/images/', $name);  
                $upload_images= DB::table('course_images')->insert([
                                        'slider_set' => 2,
                                        'course_id' => $course_id,
                                        'name' => $name,
                                        'created_at' => date('Y-m-d H:i:s'),
                ]);
            }
         }

        

        if ($request->hasFile('course_image')) {
            $image = $request->file('course_image');
            $image_name = $image->getClientOriginalName();
            $destinationPath = public_path('/themes/default/assets/images/courses/');
            $image->move($destinationPath, $image_name);

            DB::table('courses')->where('id', $course_id)->update(['image' => $image_name]);
        }

        if ($course_id)
            session()->flash('success', 'Course created Successfully!!');
        else
            session()->flash('success', 'Course cannot be created');

        return redirect()->route('admin.courses.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 

       $course = DB::table('courses')->where('id',$id)->first();
      
        return view('admin::courses.edit')->with('course', $course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
         $this->validate(request(), [
            'course_name' => 'string|required',
            'course_slug' => 'required',
            'course_content' => 'required',
        ]);

       
        if ($request->hasFile('course_image')) {
            $image = $request->file('course_image');
            $image_name = $image->getClientOriginalName();
            $destinationPath = public_path('/themes/default/assets/images/courses/');
            $image->move($destinationPath, $image_name);

            DB::table('courses')->where('id', $id)->update(['image' => $image_name]);
        }

       //dd($course_id);
          if($request->hasfile('course_images_set_1'))
         {

            foreach($request->file('course_images_set_1') as $course_image)
            {
                $name=$id.'_'.$course_image->getClientOriginalName();
                $course_image->move(public_path().'/themes/default/assets/images/courses/', $name);  
                $upload_images= DB::table('course_images')->insert([
                                        'slider_set' => 1,
                                        'course_id' => $id,
                                        'name' => $name,
                                        'created_at' => date('Y-m-d H:i:s'),
                ]);
            }
         }

         if($request->hasfile('course_images_set_2'))
         {

            foreach($request->file('course_images_set_2') as $course_image)
            {
                $name=$id.'_'.$course_image->getClientOriginalName();
                $course_image->move(public_path().'/themes/default/assets/images/courses/', $name);  
                $upload_images= DB::table('course_images')->insert([
                                        'slider_set' => 2,
                                        'course_id' => $id,
                                        'name' => $name,
                                        'created_at' => date('Y-m-d H:i:s'),
                ]);
            }
         }



        $result = DB::table('courses')->where('id', $id)->update([
                                            'name' => $request->get('course_name'),
        									'slug' => $request->get('course_slug'),
                                            'course_date' => $request->get('course_date'),
                                            'venue' => $request->get('course_venue'),
                                            'contact' => $request->get('course_contact'),
                                            'course_highlights' => $request->get('course_highlights'),
                                            'description' => $request->get('course_content'),
                                            'faculty' => $request->get('course_faculty'),
                                            'review' => $request->get('course_review'),
                                            'register_form' => ($request->get('course_reg') !=null)?1:0,
                                            'start_date' => $request->get('start_date'),
                                            'end_date' => $request->get('end_date'),
                                        ]);
        
        session()->flash('success', 'Course updated Successfully!');

        return redirect()->route('admin.courses.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = DB::table('courses')->where('id',$id)->update(['is_deleted'=>1]);
        if ($delete)
            session()->flash('success', 'Course Successfully Deleted!');
        else
            session()->flash('success', 'Course Cannot deleted!');

        return redirect()->route('admin.courses.index');
 
    }
    public function deleteImages($image_id)
    {
        $get_course =  DB::table('course_images')->where('id',$image_id)->first();
        $count_images=0;
        $delete = DB::table('course_images')->where('id',$image_id)->delete();
        if(isset($get_course))
            $count_images = DB::table('course_images')->where('course_id',$get_course->course_id)->where('slider_set',$get_course->slider_set)->count();
       return response()->json(['count_images' => $count_images]);
    }
    
}

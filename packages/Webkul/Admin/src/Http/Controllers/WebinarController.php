<?php

namespace Webkul\Admin\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Redirect;
use Session;
use DB;

class WebinarController extends Controller
{
    public function __construct(){
        $this->_config = request('_config');
    }
    
    public function ondemand(){
        $data['category'] = DB::table('webinars')->where('is_live', 0)->groupBy('category')->addSelect('category')->get();
        return view($this->_config['view'], $data);
    }

    public function storeondemand(Request $request){
        $input = $request->all();
        $input["category"] = json_decode($input["category"]);
        foreach ($input["category"] as $id => $data) {
            DB::table('webinars')->where('id', $id)->update([
                'name'          => $data->name,
                'description'   => $data->description,
                'video_url'     => $data->video_url,
                'is_active'     => 1,
                'created_at'    => date('Y-m-d h:i:s'),
            ]);
        }
        return redirect()->back();
    }

    public function ondemandNewVideo(Request $request){
        $item_id = DB::table('webinars')->insertGetId(['is_live'=> 0, 'category' => $request->category]);
        return Response::json(['id' => $item_id]);
    }

    public function createCategory(Request $request){
        $item_id = DB::table('webinars')->insertGetId([
            'is_live'   =>  0,
            'category'  =>  $request->category
        ]);
        return Response::json(['id' => $item_id]);
    }

    public function ondemandDeleteCategory(Request $request){
        DB::table('webinars')->where('category', $request->category)->delete();
        return redirect()->back();
    }

    public function ondemandDeletevideo(Request $request){
        DB::table('webinars')->where('id', $request->id)->delete();
        return redirect()->back();
    }
}

<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Admin\Exports\DataGridExport;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Product\Repositories\ProductRepository as Product;
use Excel;
use DB;


class OrderReportsController extends Controller
{


    public function __construct(Category $category, Product $product){
        $this->middleware('admin');
        $this->_config = request('_config');
        $this->category = $category;
        $this->product = $product;
    }


    public function index(){
        $items = array();

        foreach($this->category->getCategoryTree() as $cat){ //n-array preorder TreeTraversal algorithm
                if($cat!=null){
                    $stack = array();
                    array_push($stack, $cat);

                    while(count($stack)>0){
                        $temp = array_pop($stack);
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'name' => $temp->name));
                        $childrens = $temp->children;
                        if(count($childrens)<=0){
                            foreach($this->product->findAllByCategory($temp->id) as $product){
                                array_push($items, array('id' => intval($product->id)*100000,
                                                         'sku' => $product->sku,
                                                         'pid' => $temp->id,
                                                         'name' => $product->name));
                            }
                        }else{
                            for($i=count($childrens)-1; $i>=0; $i--) {
                                array_push($stack, $childrens[$i]);
                            }
                        }
                    }
                }
            }

        $data["items"] = json_encode($items);
        return view($this->_config['view'], $data);
    }

    public function export(Request $request)
    {
        $input = $request->all();
        $output = '';

        if(!isset($input['dealers']) or !in_array(2, $input["role"])){
            $input['dealers']=[];
        }

        if(!isset($input['doctor'])  or !in_array(3, $input["role"])){
            $input['doctor']=[];
        }
        $no_discount = isset($input['discount']) && ($input['discount'][0]==1);


        $query = DB::table('orders')->join('users', 'users.id', 'orders.customer_id');

        if($no_discount){
            $query = $query->where('coupon_code', '')->where('base_discount_amount', 0.0000);
        }else if(isset($input['coupon']) && count($input['coupon'])>0 ){
            $query = $query->whereIn('coupon_code', function($query) use ($input){
                $query->select('coupon_code')
                      ->from('discounts')
                      ->whereIn('id', $input['coupon']);
                });
        }

        $query = $query->whereIn('users.role_id', $input['role'])
            ->whereIn('users.id', array_merge($input['dealers'], $input['doctor']))
            ->whereIn('orders.status', ['completed', 'delivered'])
            ->whereBetween('orders.created_at', [date($input['start_date']), date('Y-m-d', strtotime("+1 day",strtotime($input['end_date'])))]);

        $orders = DB::table('orders')
                        ->whereIn('id', DB::table('order_items')
                                ->whereIn('order_id', $query->addSelect('orders.id')->get()->map(function($val){
                                    return $val->id;
                                })->toArray())
                                ->whereIn('sku', $input['productsSku'])
                                ->addSelect('order_id')
                                ->get()->map(function($val){
                                    return $val->order_id;
                                }))
                        ->get()->toArray();


        $dealer = array();
        $doctor = array();
        $dealerTotal = 0;
        $doctorTotal = 0;

        if($no_discount){
            $header = array( "<th><b>Order ID</b></th>",
                             "<th><b>Customer Name</b></th>",
                             "<th><b>Email</b></th>",
                             "<th><b>Order Date</b></th>",
                             "<th><b>Item Name</b></th>",
                             "<th><b>Price</b></th>",
                             "<th><b>Total Quantity</b></th>",
                             "<th><b>Subtotal</b></th>",
                             "<th><b>Tax 12%</b></th>",
                             "<th><b>Total</b></th>",
                             "<th><b>Shipping & Handling</b></th>",
                             "<th><b>Reward Points</b></th>",
                             "<th><b>Grand Total</b></th>\n");
        }else{

            $header = array( "<th><b>Order ID</b></th>",
                             "<th><b>Customer Name</b></th>",
                             "<th><b>Email</b></th>",
                             "<th><b>Order Date</b></th>",
                             "<th><b>Item Name</b></th>",
                             "<th><b>Price</b></th>",
                             "<th><b>Total Quantity</b></th>",
                             "<th><b>Discount Code</b></th>",
                             "<th><b>Subtotal</b></th>",
                             "<th><b>Discount</b></th>",
                             "<th><b>Discounted Subtotal</b></th>",
                             "<th><b>Tax 12%</b></th>",
                             "<th><b>Total</b></th>",
                             "<th><b>Shipping & Handling</b></th>",
                             "<th><b>Reward Points</b></th>",
                             "<th><b>Grand Total</b></th>\n");
        }

        foreach ($orders as $order) {
            $special_discount_applied = ($order->mem_discount_amount == 0.0000 && $order->promo_code_amount == 0.0000 && $order->reward_coin == 0.0000) && $order->base_discount_amount != 0.0000;

            $user  = DB::table('users')->where('id', $order->customer_id)->first();
            $get_tax = DB::table('tax_rates')->where('state',$user->state_id)->first();
            $order_items = DB::table('order_items')->where('order_id', $order->id)->get();
            $head = true;
            $total = 0;

            foreach ($order_items as $item) {
                if($special_discount_applied == true){
                    $item_discount_applied = (isset($item->discount_amount) && $item->discount_amount !=0);
                    $discount = ($item_discount_applied==true)?number_format($item->discount_amount, 2, '.', ""):'NA';
                    $discounted_subtotal = ($item_discount_applied==true)?number_format(($item->base_total - $item->discount_amount), 2, '.', ""):'NA';
                }else{
                    $item_discount_applied = ($item->discounted_subtotal!=null);
                    $discount = ($item_discount_applied==true)?number_format(($item->base_total - $item->discounted_subtotal), 2, '.', ""):'NA';
                    $discounted_subtotal = ($item_discount_applied==true)?number_format(($item->discounted_subtotal), 2, '.', ""):'NA';
                }
                
                $total_price_item = $item->total - $item->discount_amount;
                if($discounted_subtotal != 0){
                    $total_price_item = $discounted_subtotal;
                }
                $get_tax = DB::table('tax_rates')->where('state',$user->state_id)->first();
                $item_tax = $total_price_item * ($get_tax->tax_rate / 100);
                $price_with_tax = $total_price_item + $item_tax;


                if($head == true){
                    if($no_discount){
                        $row = array(
                            $item->order_id, // Order ID
                            $user->my_title.' '.$user->first_name.' '.$user->last_name, // Customer Name
                            $order->customer_email, // Email
                            date('d/m/Y', strtotime($item->created_at)), //Order Date
                            $item->name, //Item Name
                            number_format($item->base_price, 2, '.', ""), // Price
                            $item->qty_ordered, //Total Quantity
                            number_format($item->base_total, 2, '.', ""), //Sub Total
                            number_format($item_tax, 2, '.', ""), // Tax
                            number_format($price_with_tax, 2, '.', ""), // Total,
                            '', // Ship
                            '', // Grand Total
                        );
                    }else{
                        $row = array(
                            $item->order_id, // Order ID
                            $user->my_title.' '.$user->first_name.' '.$user->last_name, // Customer Name
                            $order->customer_email, // Email
                            date('d/m/Y', strtotime($item->created_at)), //Order Date
                            $item->name, //Item Name
                            number_format($item->base_price, 2, '.', ""), // Price
                            $item->qty_ordered, //Total Quantity
                            ($item->discounted_subtotal!=null)?$order->coupon_code:'', // Coupon Code
                            number_format($item->base_total, 2, '.', ""), //Sub Total
                            $discount, //Discount
                            $discounted_subtotal, //Discounted Sub Total
                            number_format($item_tax, 2, '.', ""), // Tax
                            number_format($price_with_tax, 2, '.', ""), // Total,
                            '', // Ship
                            '', // Reward Points
                            '', // Grand Total
                        );                        
                    }
                }else{
                    if($no_discount){
                        $row = array(
                            '', // Order ID
                            '', // Customer Name
                            '', // Email
                            '', //Order Date
                            $item->name, //Item Name
                            number_format($item->base_price, 2, '.', ""), // Price
                            $item->qty_ordered, //Total Quantity
                            number_format($item->base_total, 2, '.', ""), //Sub Total
                            number_format($item_tax, 2, '.', ""), // Tax
                            number_format($price_with_tax, 2, '.', ""), // Total
                            '', // Ship
                            '', // Reward Points
                            '', // Grand Total
                        );
                    }else{
                        $row = array(
                            '', // Order ID
                            '', // Customer Name
                            '', // Email
                            '', //Order Date
                            $item->name, //Item Name
                            number_format($item->base_price, 2, '.', ""), // Price
                            $item->qty_ordered, //Total Quantity
                            ($item->discounted_subtotal!=null)?$order->coupon_code:'', // Coupon Code
                            number_format($item->base_total, 2, '.', ""), //Sub Total
                            $discount, //Discount
                            $discounted_subtotal, //Discounted Sub Total
                            number_format($item_tax, 2, '.', ""), // Tax
                            number_format($price_with_tax, 2, '.', ""), // Grand Total
                            '', // Ship
                            '', // Reward Points
                            '', // Grand Total
                        );                        
                    }
                }

                $total+= $price_with_tax;
                $head = false;

                if($user->role_id == 2){
                    array_push($dealer, implode(', ', $row));
                }else if($user->role_id == 3){
                    array_push($doctor, implode(', ', $row));
                }
            }
            
            $reward_points = number_format($order->reward_coin, 2, '.', '');
            $grand_total = number_format($order->base_grand_total, 2, '.', '');
            $ship = number_format($order->base_shipping_amount + $order->base_shipping_amount*($get_tax->tax_rate / 100), 2, '.', '');
            $total = number_format($total, 2, '.', '');

            if($no_discount){
                $total_row = array('', '', '', '', '', '', '', '', '<b>Total</b>', '<b>'.$total.'</b>', '<b>'.$ship.'</b>', '<b>'.$reward_points.'</b>', '<b>'.$grand_total.'</b>');
                $end_row = array('', '', '', '', '', '', '', '', '', '', '','', '');
            }else{
                $total_row = array('', '', '', '', '', '', '', '', '', '', '', '<b>Total</b>', '<b>'.$total.'</b>', '<b>'.$ship.'</b>', '<b>'.$reward_points.'</b>', '<b>'.$grand_total.'</b>');   
                $end_row = array('', '', '', '', '', '', '', '', '', '', '', '', '', '','', '');   
            }

            if($user->role_id == 2){
                array_push($dealer, implode(', ', $total_row), implode(', ', $end_row));
            }else if($user->role_id == 3){
                array_push($doctor, implode(', ', $total_row), implode(', ', $end_row));
            }

        }

        if(in_array(3, $input["role"])) {
            $output .= "<h3>Doctor Orders</h3>\n";
            $output .= implode(', ', $header);
            $output .= implode("\n", $doctor);
            if(in_array(2, $input["role"])){
                $output .= "\n";
            }
        }

        if(in_array(2, $input["role"])) {
            $output .= "<h3>Dealer Orders</h3>\n";
            $output .= implode(', ', $header);
            $output .= implode("\n", $dealer);
        }

        return Response::json(array(
            "from"  => $input['start_date'],
            "to"    => $input['end_date'],
            "csv"   => $output)
        );
    }
}
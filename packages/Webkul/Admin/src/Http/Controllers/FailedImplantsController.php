<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Admin\Exports\DataGridExport;
use Excel;
use Mail;
use DB;
use PDF;
use Redirect;

class FailedImplantsController extends Controller
{    
    public function __construct(){
        $this->middleware('admin');
        $this->_config = request('_config');
    }

    public function index(){
        $myFilter = ['Processing', 'Completed', 'Approved'];
        return view($this->_config['view'], compact('myFilter'));
    }

    public function approvals(){
        $myFilter = ['Pending'];
        return view($this->_config['view'], compact('myFilter'));
    }

    public function rejected(){
        $myFilter = ['Rejected'];
        return view($this->_config['view'], compact('myFilter'));
    }

    public function show($id){
        $data['failed_implant'] = DB::table('failed_implants')->where('id', $id)->where('is_deleted', 0)->first();
        $data['return_implants_return'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->where('type',1)->get();        
        $data['return_implants_replace'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->where('type',2)->get();        
        $data['user'] = DB::table('users')->where('id', $data['failed_implant']->user_id)->first();

        return view($this->_config['view'], $data);
    }

    /*    
        public function destroy($id){
            DB::table('failed_implants')->where('id', $id)->update(['is_deleted'=>1]);
            return redirect()->route('admin.catalog.failed-implants.index');
        }
    */

    public function download(Request $data){
        if($data->type == 'before'){
            $url="failed_implants/xray_implant/".$data->file;
        }else if($data->type == 'after'){
            $url="failed_implants/before_implant_removed/".$data->file;
        }else{
            return "Invalid request";
        }

        if(Storage::exists($url)){
            return Storage::download($url);
        }else{
            session()->flash('error','File not found on the server');
            return Redirect::back();
        }
    }

    public function update(Request $request){
        $failed_implant = DB::table('failed_implants')
            ->where('id', $request->failed_id)
            ->where('is_deleted', 0)
            ->update(['status' => $request->failed_status, 
                      'comments' => $request->comments]);

        $data['failed_implant'] =DB::table('failed_implants')
                            ->where('id', $request->failed_id)
                            ->where('is_deleted', 0)
                            ->first();

        $data['return_implants_return'] = DB::table('return_implants_items')->where('failed_implants_id', $data['failed_implant']->id)->where('type',1)->get();        
        $data['return_implants_replace'] = DB::table('return_implants_items')->where('failed_implants_id', $data['failed_implant']->id)->where('type',2)->get();        

        $user = DB::table('users')->where('id', $data['failed_implant']->user_id)->first(); 
        
        $data['user'] = $user; 
        
        if($request->failed_status == "Completed"){
            if($request->refund_amount!=0){
                \DB::table('reward_points')->insert([
                    'user_id' => $data['failed_implant']->user_id,
                    'reward_points' => $request->refund_amount * 10,
                    'points_credited' => 1, 
                    'points_expired' => 0,
                    'order_id' => 0,
                ]);
            }            
        }        
        if($request->failed_status == "Approved"){
            $pdf = PDF::loadView('shop::customers.account.failure_implants.pdf', $data)->setPaper('a4');
            Mail::send('shop::emails.customer.failed-implants-approved', $data, function ($message) use ($user, $pdf){
                $message->from('info@genxtimplants.com', 'GenXT');
                $message->to($user->email)->subject('Return Implants - Approved');
                $message->attachData($pdf->output(), 'return-implant.pdf');
            });
        }else{
            Mail::send('shop::emails.customer.failed-implants-status', $data, function ($message) use ($user, $request){    
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($user->email)->subject('Return Implants - '.$request->failed_status);
                });
        }

        /*return view('shop::emails.customer.failed-implants-status', compact('failed_implant', 'invoice_item_id', 'user', 'order'));*/

        session()->flash('success','Status updated successfully');

        if(isset($request->return_view) and $request->return_view=1){
            return redirect()->route('admin.catalog.failed-implants.view', $request->failed_id);
        }
        
        return redirect()->route('admin.catalog.failed-implants.index');
    }
}


<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Webkul\Core\Repositories\ChannelRepository as Channel;
use DB;
use Webkul\Admin\Http\Controllers\Controller;
use Mail;

class CreditPaymentsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Channel $channel)
    {
        $this->_config = request('_config');
        $this->middleware('admin');
    }


    public function index()
    {
        $courses = DB::table('courses')->where('is_deleted',0)->get();
        return view('admin::courses.index',compact('courses',$courses));
    }


    public function approvals()
    {
        return view($this->_config['view']);
    }


    public function approve(Request $request,$id)
    {
        $credit_detail = DB::table('credit_payments')->where('id', $id)->first();
        $credit_user = DB::table('users')->where('id', $credit_detail->user_id)->first();

        if(!in_array($credit_user->role_id, [2, 3])){
            session()->flash('error', 'User role invalid!');
            return redirect()->back();
        }

        if($credit_detail){
            DB::table('credit_payments')
                ->where('id', $id)
                ->update(['status'=>1]);


            $credit_detail = DB::table('credit_payments')->where('id', $id)->first();

            $credit_limit = $credit_user->credit_limit;
            $credit_paid  = $credit_user->credit_paid;
            $credit_used  = $credit_user->credit_used;
            $due_amount =  $credit_paid - $credit_used;

            $credit_array = array();

            if($due_amount < 0){ // Due payment
                $due_amount = abs($due_amount);
                if($due_amount >= $credit_detail->amount){  // Paying due amount
                    $credit_array = ['credit_paid' => $credit_paid + $credit_detail->amount];
                }else{  // Paying due with excess amount
                    $credit_array = ['credit_paid' => $credit_paid + $due_amount, 'credit_balance' => $credit_detail->amount - $due_amount];
                }
            }else{ // Paying excess amount
                $credit_balance = $credit_user->credit_balance + $credit_detail->amount;
                $credit_array = ['credit_balance' => $credit_balance];
            }
            
            DB::table('users')->where('id', $credit_user->id)
                              ->update($credit_array);

            $full_name = $credit_user->first_name.' '.$credit_user->last_name;

            $sms_message = 'Hello Dr '.$credit_user->first_name.', your GenXT cheque payment request is Approved.';
            $sms_info = array();
            $sms_info['sms_numbers'] = $credit_user->phone_number;
            $sms_info['sms_message'] = $sms_message;     
            $sent_sms=view('shop::send-sms',$sms_info)->render();

            Mail::send('shop::emails.credit_payments.credit_status_email', compact('credit_detail','credit_user'), function ($message) use($credit_user) {
                $message->from('info@genxtimplants.com', 'GenXT');
                $message->to($credit_user->email)->subject('GenXT - Payment Status!');
            });
        }
        return redirect()->route($this->_config['redirect']);
    }

    public function reject(Request $request)
    {
        DB::table('credit_payments')
            ->where('id', $request->credit_id)
            ->update(['status'=>2,'rejected_comment'=>$request->reject_comment]);

        
        $credit_detail = DB::table('credit_payments')->where('id', $request->credit_id)->first();
        $credit_user = DB::table('users')->where('id', $credit_detail->user_id)->first();

        Mail::send('shop::emails.credit_payments.credit_status_email', compact('credit_detail','credit_user'), function ($message) use($credit_user) {
            $message->from('info@genxtimplants.com', 'GenXT');
            $message->to($credit_user->email)->subject('GenXT - Payment Status!');
        });                    

        return redirect()->route($this->_config['redirect']);
    }

    public function PayViaChequeAndOther(Request $request)
    {

        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";


        $image_name="";

        

        if($request->payment_type ==2 && $request->hasfile('cheque_image')){
            $cheque_image = $request->file('cheque_image');
            $image_name=$cheque_image->getClientOriginalName();
            $image_name = str_replace(' ','_',$image_name);
            $cheque_image->move(public_path().'/themes/default/assets/images/credit_images/', $image_name);

        }else if($request->payment_type ==3 && $request->hasfile('receipt_image')){
            $cheque_image = $request->file('receipt_image');
            $image_name=$cheque_image->getClientOriginalName();
            $image_name = str_replace(' ','_',$image_name);
            $cheque_image->move(public_path().'/themes/default/assets/images/credit_images/', $image_name);
        }

        $credit_user = \DB::table('users')->where('id',$request->user_id)->first();

        $data = [  'user_id' => $request->user_id,
                   'amount' => $request->pay_credit_amount,
                   'payment_type' => $request->payment_type,
                   'cheque_number' =>  ($request->payment_type ==2 )? $request->cheque_number : $request->transation_number,
                   'cheque_image' => $image_name,
                   'payment_id' => $request->transaction_id,
                   'comments' => $request->transaction_comment,
                   'paid_by' => $user->id,
                   'status' => 1,
                   'created_at' => date('Y-m-d H:i:s')];

        $credit_limit = $credit_user->credit_limit;
        $credit_paid  = $credit_user->credit_paid;
        $credit_used  = $credit_user->credit_used;
        $due_amount =  $credit_paid - $credit_used;

        $getId = \DB::table('credit_payments')->insertGetId($data);
        $credit_detail = \DB::table('credit_payments')->where('id',$getId)->first();

        $credit_array = array();

        if($due_amount < 0){ // Due payment
            $due_amount = abs($due_amount);
            if($due_amount >= $credit_detail->amount){  // Paying due amount
                $credit_array = ['credit_paid' => $credit_paid + $credit_detail->amount];
            }else{  // Paying due with excess amount
                $credit_array = ['credit_paid' => $credit_paid + $due_amount, 'credit_balance' => $credit_detail->amount - $due_amount];
            }
        }else{ // Paying excess amount
            $credit_balance = $credit_user->credit_balance + $credit_detail->amount;
            $credit_array = ['credit_balance' => $credit_balance];
        }

        DB::table('users')->where('id', $credit_user->id)
                              ->update($credit_array);

        $admin = \DB::table('users')->where('id',1)->first();

        Mail::send('shop::emails.credit_payments.credit_status_email', compact('credit_detail','credit_user'), function ($message) use($credit_user){
            $message->from('info@genxtimplants.com', 'GenXT');
            $message->to($credit_user->email)->subject('GenXT - Payment Status!');
        }); 

        session()->flash('success', 'Payment has been added to the user!');

        return redirect()->back();  

    }


    public function PayViaRazorpay(Request $request)
    {
        
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
        $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        $input = $request->all();

        $api = new Api('rzp_test_TnrA3GDy7AVcfS', 'g1LZZd5cfeta9ISGqogbhnVi');
        $payment = $api->payment->fetch($input['payment_id']);
        if(count($input)  && !empty($input['payment_id'])) {
            try {
                $response=$api->payment->fetch($input['payment_id'])->capture(array('amount'=>$payment['amount'])); 
            }
            catch (\Exception $e) {
                return  $e->getMessage();
                return redirect()->back();
            }
        }

        $data = [
                    'user_id' => $request->user_id,
                    'payment_id' => $request->payment_id,
                    'amount' => $request->amount,
                    'payment_type' => 'razorpay',
                    'paid_by' => $user->id,
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                ];

        $already_paid =$user->credit_paid;
        $user->credit_paid =  $already_paid + $request->amount;
        $user->save();

        $getId = \DB::table('credit_payments')->insertGetId($data);  
        $arr = array('payment_id' => $request->payment_id, 'msg' => 'Payment successfully credited', 'status' => true);
        session()->flash('success', 'Your Payment has been made successfully!');
        return Response()->json($arr);   
    }


}

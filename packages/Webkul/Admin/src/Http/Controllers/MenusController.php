<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin::settings.menus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::settings.menus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'menu_title' => 'string|required',
            'menu_link' => 'required',
        ]);

        //dd($request->get('menu_title'));
        //$result = $this->menus->save(request()->all());

        $result = DB::table('menus')->insert(['menu_title' => $request->get('menu_title'),
                                            'menu_link' => $request->get('menu_link')]);

        if ($result)
            session()->flash('success', trans('admin::app.settings.menus.created-success'));
        else
            session()->flash('success', trans('admin::app.settings.menus.created-fail'));

        return redirect()->route('admin.menus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $menu = DB::table('menus')->where('id',$id)->first();
        return view('admin::settings.menus.edit')->with('menu', $menu);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $this->validate(request(), [
            'menu_title' => 'string|required',
            'menu_link' => 'required',
        ]);

        //dd($request->get('menu_title'));
        //$result = $this->menus->save(request()->all());

        $result = DB::table('menus')->where('id', $id)->update(['menu_title' => $request->get('menu_title'),
                                            'menu_link' => $request->get('menu_link')]);

        if ($result)
            session()->flash('success', trans('admin::app.settings.menus.update-success'));
        else
            session()->flash('success', trans('admin::app.settings.menus.update-fail'));

        return redirect()->route('admin.menus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $delete = DB::table('menus')->where('id',$id)->update(['is_active'=>0]);
        if ($delete)
            session()->flash('success', trans('admin::app.settings.menus.delete-success'));
        else
            session()->flash('success', trans('admin::app.settings.menus.delete-fail'));

        return redirect()->route('admin.menus.index');
    }
}

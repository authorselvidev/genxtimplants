<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Webkul\Sales\Repositories\OrderRepository as Order;
use Webkul\Sales\Repositories\OrderItemRepository as OrderItem;
use Webkul\Customer\Repositories\CustomerRepository as Customer;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Product\Repositories\ProductInventoryRepository as ProductInventory;

/**
 * Dashboard controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $order;

    /**
     * OrderItemRepository object
     *
     * @var array
     */
    protected $orderItem;

    /**
     * CustomerRepository object
     *
     * @var array
     */
    protected $customer;

    /**
     * ProductInventoryRepository object
     *
     * @var array
     */
    protected $productInventory;

    /**
     * string object
     *
     * @var array
     */
    protected $startDate;

    /**
     * string object
     *
     * @var array
     */
    protected $lastStartDate;

    /**
     * string object
     *
     * @var array
     */
    protected $endDate;

    /**
     * string object
     *
     * @var array
     */
    protected $lastEndDate;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Sales\Repositories\OrderRepository              $order
     * @param  Webkul\Sales\Repositories\OrderItemRepository          $orderItem
     * @param  Webkul\Customer\Repositories\CustomerRepository        $customer
     * @param  Webkul\Product\Repositories\ProductInventoryRepository $productInventory
     * @return void
     */
    public function __construct(
        Order $order,
        OrderItem $orderItem,
        Customer $customer,
        Category $category,
        ProductInventory $productInventory
    ) {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->order = $order;

        $this->orderItem = $orderItem;

        $this->category = $category;

        $this->customer = $customer;

        $this->productInventory = $productInventory;
    }

    public function getPercentageChange($previous, $current)
    {
        if (!$previous)
            return $current ? 100 : 0;

        return ($current - $previous) / $previous * 100;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->setStartEndDate();

        $user_role = auth()->guard('admin')->user()->role_id;
        $get_doctors = DB::table('users')
            ->where('is_deleted', 0)
            ->where('is_approved', 1)
            ->where('role_id', 3);
        if ($user_role == 2)
            $get_doctors->where('users.dealer_id', auth()->guard('admin')->user()->id);

        $total_doctors = $get_doctors->count();

        $current_doctors = $get_doctors->where('users.created_at', '>=', $this->startDate)
            ->where('users.created_at', '<=', $this->endDate)
            ->count();

        $get_orders = DB::table('orders as o')
            ->where('o.created_at', '>=', $this->startDate)
            ->where('o.created_at', '<=', $this->endDate)
            ->leftjoin('users as u', 'u.id', 'o.customer_id')
            ->where('u.is_deleted', 0);

        $get_preorder = DB::table('orders as o')
            ->where('o.created_at', '>=', $this->lastStartDate)
            ->where('o.created_at', '<=', $this->lastEndDate)
            ->leftjoin('users as u', 'u.id', 'o.customer_id')
            ->where('u.is_deleted', 0);            

        if ($user_role == 2) $get_orders->where('u.dealer_id', auth()->guard('admin')->user()->id);

        $total_orders = $get_orders->count();
        $total_sales = $get_orders->sum('grand_total');
        $total_avg_sales = $get_orders->avg('grand_total');


        $recent_orders = $get_orders->where('o.created_at', '>=', $this->startDate)
            ->where('o.created_at', '<=', $this->endDate);

        //dd($recent_orders);
        $current_orders = $recent_orders->count();

        $current_sales = $recent_orders->sum('grand_total');

        $avg_sales = $recent_orders->avg('grand_total');

        //dd($total_orders);

        $pre_current_orders = $get_preorder->count();

        $pre_current_sales = $get_preorder->sum('grand_total');

        $pre_avg_sales = $get_preorder->avg('grand_total');


        // dd($this->lastStartDate, $this->lastEndDate,  $this->startDate, $this->endDate);

        $statistics = [
            'prevStartDate' => $this->lastStartDate,
            'prevEndDate' => $this->lastEndDate,
            'total_doctors' => [
                'previous' => $previous = $this->customer->scopeQuery(function ($query) {
                    return $query->where('users.created_at', '>=', $this->lastStartDate)
                        ->where('users.created_at', '<=', $this->lastEndDate)
                        ->where('role_id', 3);
                })->count(),
                'current' => $current_doctors,
                'total' => $total_doctors,
                'progress' => $this->getPercentageChange($previous, $current_doctors)
            ],
            'total_orders' =>  [
                'previous' => $previous = $pre_current_orders,
                'current' => $current_orders,
                'total' => $total_orders,
                'progress' => $this->getPercentageChange($previous, $current_orders)
            ],
            'total_sales' =>  [
                'previous' =>  $previous = $pre_current_sales,
                'current' => $current_sales,
                'total' => $total_sales,
                'progress' => $this->getPercentageChange($previous, $current_sales)
            ],
            'avg_sales' =>  [
                'previous' => $previous = $pre_avg_sales,
                'current' => $avg_sales,
                'total' => $total_avg_sales,
                'progress' => $this->getPercentageChange($previous, $avg_sales)
            ],
            'top_selling_categories' => $this->getTopSellingCategories(),
            'top_selling_products' => $this->getTopSellingProducts(),
            'customer_with_most_sales' => $this->getCustomerWithMostSales(),
            'stock_threshold' => $this->getStockThreshold(),
        ];

        foreach (core()->getTimeInterval($this->startDate, $this->endDate) as $interval) {
            $statistics['sale_graph']['label'][] = $interval['start']->format('d M');

            $total = $this->order->scopeQuery(function ($query) use ($interval, $user_role) {
                if ($user_role == 2) {
                    return $query->leftjoin('users', 'users.id', 'orders.customer_id')
                        ->where('users.dealer_id', auth()->guard('admin')->user()->id)
                        ->where('orders.created_at', '>=', $interval['start'])
                        ->where('orders.created_at', '<=', $interval['end']);
                }
                return $query->where('orders.created_at', '>=', $interval['start'])
                    ->where('orders.created_at', '<=', $interval['end']);
            })->sum('base_grand_total');

            $statistics['sale_graph']['total'][] = $total;
            $statistics['sale_graph']['formated_total'][] = core()->formatBasePrice($total);
        }

        return view($this->_config['view'], compact('statistics'))->with(['startDate' => $this->startDate, 'endDate' => $this->endDate]);
    }

    /**
     * Returns the list of top selling categories
     *
     * @return mixed
     */
    public function getTopSellingCategories()
    {
        $user_role = auth()->guard('admin')->user()->role_id;

        $items = array();

        foreach ($this->category->getCategoryTree() as $cat) {
            if ($cat != null) {
                $stack = array();
                array_push($stack, $cat);

                while (count($stack) > 0) {
                    $temp = array_pop($stack);
                    $childrens = $temp->children;

                    if (count($childrens) <= 0) {
                        array_push($items, $temp->id);
                    } else {
                        for ($i = count($childrens) - 1; $i >= 0; $i--) {
                            array_push($stack, $childrens[$i]);
                        }
                    }
                }
            }
        }

        $query = $this->orderItem->getModel()
            ->leftJoin('products', 'order_items.product_id', 'products.id')
            ->leftJoin('product_categories', 'products.id', 'product_categories.product_id')
            ->leftJoin('categories', 'product_categories.category_id', 'categories.id')
            ->leftJoin('category_translations', 'categories.id', 'category_translations.category_id')
            ->leftjoin('orders', 'order_items.order_id', 'orders.id')
            ->leftjoin('users', 'users.id', 'orders.customer_id');
        if ($user_role == 2) {
            $query->where('users.dealer_id', auth()->guard('admin')->user()->id);
        }
        $query = $query->where('category_translations.locale', app()->getLocale())
            ->whereIn('categories.id', $items)
            ->where('order_items.created_at', '>=', $this->startDate)
            ->where('order_items.created_at', '<=', $this->endDate)
            ->where('order_items.qty_ordered', '>', 0)
            ->addSelect(DB::raw('SUM(qty_ordered) as total_qty_ordered'))
            ->addSelect(DB::raw('COUNT(products.id) as total_products'))
            ->addSelect('order_items.id', 'categories.id as category_id', 'category_translations.name')
            ->groupBy('category_id')
            ->orderBy('total_qty_ordered', 'DESC')
            ->limit(5)
            ->get();

        return $query;
    }

    /**
     * Return stock threshold.
     *
     * @return mixed
     */
    public function getStockThreshold()
    {
        return $this->productInventory->getModel()
            ->leftJoin('products', 'product_inventories.product_id', 'products.id')
            ->select(DB::raw('SUM(qty) as total_qty'))
            ->addSelect('product_inventories.product_id')
            ->where('products.type', '!=', 'configurable')
            ->groupBy('product_id')
            ->orderBy('total_qty', 'ASC')
            ->limit(5)
            ->get();
    }

    /**
     * Returns top selling products
     * @return mixed
     */
    public function getTopSellingProducts()
    {
        $user_role = auth()->guard('admin')->user()->role_id;

        $query = $this->orderItem->getModel()
            ->select(DB::raw('SUM(qty_ordered) as total_qty_ordered'))
            ->addSelect('order_items.id', 'product_id', 'product_type', 'name')
            ->leftjoin('orders', 'order_items.order_id', 'orders.id')
            ->leftjoin('users', 'users.id', 'orders.customer_id');
        if ($user_role == 2) {
            $query->where('users.dealer_id', auth()->guard('admin')->user()->id);
        }
        $query = $query->where('order_items.created_at', '>=', $this->startDate)
            ->where('order_items.created_at', '<=', $this->endDate)
            ->whereNull('parent_id')
            ->groupBy('product_id')
            ->orderBy('total_qty_ordered', 'DESC')
            ->limit(5)
            ->get();

        return $query;
    }

    /**
     * Returns top selling products
     *
     * @return mixed
     */
    public function getCustomerWithMostSales()
    {
        $user_role = auth()->guard('admin')->user()->role_id;
        $query = $this->order->getModel()
            ->select(DB::raw('SUM(base_grand_total) as total_base_grand_total'))
            ->addSelect(DB::raw('COUNT(orders.id) as total_orders'))
            ->addSelect('orders.id', 'customer_id', 'customer_email', 'customer_first_name', 'customer_last_name')
            ->leftjoin('users', 'users.id', 'orders.customer_id');
        if ($user_role == 2) {
            $query->where('users.dealer_id', auth()->guard('admin')->user()->id);
        }
        $query = $query->where('orders.created_at', '>=', $this->startDate)
            ->where('orders.created_at', '<=', $this->endDate)
            ->groupBy('customer_email')
            ->orderBy('total_base_grand_total', 'DESC')
            ->limit(5)
            ->get();

        return $query;
    }

    /**
     * Sets start and end date
     *
     * @return void
     */
    public function setStartEndDate()
    {

        $this->startDate = request()->get('start')
            ? Carbon::createFromTimeString(request()->get('start') . " 00:00:01")
            : Carbon::createFromTimeString(Carbon::now()->subDays(30)->format('Y-m-d') . " 00:00:01");

        $this->endDate = request()->get('end')
            ? Carbon::createFromTimeString(request()->get('end') . " 23:59:59")
            : Carbon::now();

        if ($this->endDate > Carbon::now())
            $this->endDate = Carbon::now();

        $this->lastStartDate = clone $this->startDate;
        $this->lastEndDate = clone $this->startDate;
        $this->lastEndDate->subSecond(2);
        
        $this->lastStartDate->subDays($this->startDate->diffInDays($this->endDate));

        // http://localhost/genxt-live/admin/dashboard?start=2021-12-01&end=2022-01-31
        // dd($this->lastStartDate); // 61 

        // $this->lastEndDate->subDays($this->lastStartDate->diffInDays($this->lastEndDate));
    }
}

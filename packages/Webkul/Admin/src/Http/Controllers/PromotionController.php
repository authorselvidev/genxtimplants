<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Webkul\Category\Repositories\CategoryRepository as Category;


class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected $category;

    public function __construct(Category $category)
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->category = $category;


    }


    public function index()
    {
        return view('admin::promotions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->getCategoryTree();
        return view('admin::promotions.create',compact('customerGroup','channelName','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate(request(), [
            'min_order_qty' => 'required',
            'free_products_count' => 'required',
            'discount_usage_limit' => 'required',
            'discount_title' => 'required',
            'discount_description' => 'required',
            'coupon_code' => 'required',
            'promotion_ordered_categories' => 'required|Array',
            'promotion_free_categories' => 'required|Array'
        ]);
        //dd($request->all());
        $date_start=$date_end=null;
        if($request->get('date_start') != "")
            $date_start = date('Y-m-d H:i:s', strtotime($request->get('date_start')));
        if($request->get('date_end') != "")
            $date_end = date('Y-m-d H:i:s', strtotime($request->get('date_end')));

        $for_whom = $request->get('for_whom');
        if(count($for_whom) == 2) $for_whom[0] = 0;

         $insert_discount_id = DB::table('discounts')->insertGetId([
                                        'discount_type' => 4,
                                        'for_all_users' =>1,
                                        'date_start' => $date_start,
                                        'date_end' => $date_end,
                                        'coupon_code' => $request->get('coupon_code'),
                                        'discount_title' => $request->get('discount_title'),
                                        'discount_description' => $request->get('discount_description'),
                                        'discount_usage_limit' => $request->get('discount_usage_limit')
                                   ]);


        $insert_promotion_id = DB::table('promotions')->insertGetId([
                                        'discount_id' => $insert_discount_id,
                                        'min_order_qty' => $request->get('min_order_qty'),
                                        'free_products_count' => $request->get('free_products_count'),
                                        'for_whom' =>  $for_whom[0],
                                        'earn_points' => $request->get('earn_points'),
                                        'created_at' => date('Y-m-d H:i:s')
                                   ]);
        if($insert_promotion_id)
        {
            foreach($request->get('promotion_ordered_categories') as $or_key => $category)
            {
                $insert_ordered_category = DB::table('promotion_ordered_category')->insert([
                                                'discount_id' => $insert_discount_id,
                                                'promotion_id' => $insert_promotion_id,
                                                'category_id' => $category,
                                        ]);
            }
            foreach($request->get('promotion_free_categories') as $fr_key => $category)
            {
                $insert_free_category = DB::table('promotion_free_product_category')->insert([
                                                'discount_id' => $insert_discount_id,
                                                'promotion_id' => $insert_promotion_id,
                                                'category_id' => $category,
                                        ]);
            }

            session()->flash('success', trans('admin::app.discounts.created-success'));
        }
        else
            session()->flash('success', trans('admin::app.discounts.created-fail'));
        
        return redirect()->route('admin.promotion.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $categories = $this->category->getCategoryTree();
        $discount = DB::table('discounts')->where('id',$id)->first();
        $promotion = DB::table('promotions')->where('discount_id',$id)->first();
        return view('admin::promotions.edit',compact('categories','discount','promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //dd($request->all());
       $this->validate(request(), [
            'discount_title' => 'required',
            'discount_description' => 'required',
            'min_order_qty' => 'required',
            'free_products_count' => 'required',
            'discount_usage_limit' => 'required',
            'coupon_code' => 'required',
            'promotion_ordered_categories' => 'required|Array',
            'promotion_free_categories' => 'required|Array'
        ]);

       $date_start=$date_end=null;
        if($request->get('date_start') != "")
            $date_start = date('Y-m-d H:i:s', strtotime($request->get('date_start')));
        if($request->get('date_end') != "")
            $date_end = date('Y-m-d H:i:s', strtotime($request->get('date_end')));
        
        $for_whom = $request->get('for_whom');
        if(count($for_whom) == 2) $for_whom[0] = 0;

         $update_discount = DB::table('discounts')->where('id',$id)->update([
                                        'for_all_users' =>1,
                                        'date_start' => $date_start,
                                        'date_end' => $date_end,
                                        'coupon_code' => $request->get('coupon_code'),
                                        'discount_title' => $request->get('discount_title'),
                                        'discount_description' => $request->get('discount_description'),
                                        'discount_usage_limit' => $request->get('discount_usage_limit')
                                   ]);


        $update_promotion = DB::table('promotions')->where('discount_id',$id)->update([
                                        'min_order_qty' => $request->get('min_order_qty'),
                                        'free_products_count' => $request->get('free_products_count'),
                                        'earn_points' => $request->get('earn_points'),
                                        'for_whom' =>  $for_whom[0]
                                   ]);
        $get_promotion = DB::table('promotions')->where('discount_id',$id)->first();
    
            $delete_ordered_category = DB::table('promotion_ordered_category')->where('promotion_id',$get_promotion->id)->delete();
            $delete_free_category = DB::table('promotion_free_product_category')->where('promotion_id',$get_promotion->id)->delete();
            foreach($request->get('promotion_ordered_categories') as $or_key => $category)
            {
                $insert_ordered_category = DB::table('promotion_ordered_category')->insert([
                                                'discount_id' => $id,
                                                'promotion_id' => $get_promotion->id,
                                                'category_id' => $category,
                                        ]);
            }
            foreach($request->get('promotion_free_categories') as $fr_key => $category)
            {
                $insert_free_category = DB::table('promotion_free_product_category')->insert([
                                                'discount_id' => $id,
                                                'promotion_id' => $get_promotion->id,
                                                'category_id' => $category,
                                        ]);
            }
                
            session()->flash('success', trans('admin::app.promotions.update-success'));
        

        return redirect()->route('admin.promotion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $delete_dis = DB::table('discounts')->where('id',$id)->update(['is_deleted'=>1]);
        $delete = DB::table('promotions')->where('discount_id',$id)->update(['is_deleted'=>1]);
        if ($delete)
            session()->flash('success', trans('admin::app.discounts.delete-success'));
        else
            session()->flash('success', trans('admin::app.disocunts.delete-fail'));

        return redirect()->route('admin.promotion.index');
    }
}

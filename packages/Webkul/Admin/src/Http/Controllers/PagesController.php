<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$pages = DB::table('pages')->where('is_deleted',0)->get();
    	$home_page = DB::table('pages')->where('is_deleted',0)->where('is_home',1)->first();
        return view('admin::settings.pages.index')->with(['pages' => $pages, 'home_page' => $home_page
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::settings.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	//dd($request->all());
       $this->validate(request(), [
            'page_name' => 'string|required',
            'page_slug' => 'required',
            'page_content' => 'required',
        ]);

        $result = DB::table('pages')->insert(['page_name' => $request->get('page_name'),
        									'page_slug' => $request->get('page_slug'),
                                            'page_content' => $request->get('page_content'),
                                            'meta_title' => $request->get('meta_title'),
                                            'meta_keyword' => $request->get('meta_keyword'),
                                            'meta_description' => $request->get('meta_description')
                                        ]);

        if ($result)
            session()->flash('success', trans('admin::app.settings.pages.created-success'));
        else
            session()->flash('success', trans('admin::app.settings.pages.created-fail'));

        return redirect()->route('admin.pages.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
       $page = DB::table('pages')->where('id',$id)->first();
        return view('admin::settings.pages.edit')->with('page', $page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate(request(), [
            'page_name' => 'string|required',
            'page_slug' => 'required',
            'page_content' => 'required',
        ]);

        $result = DB::table('pages')->where('id', $id)->update(['page_name' => $request->get('page_name'),
        									'page_slug' => $request->get('page_slug'),
                                            'page_content' => $request->get('page_content'),
                                            'meta_title' => $request->get('meta_title'),
                                            'meta_keyword' => $request->get('meta_keyword'),
                                            'meta_description' => $request->get('meta_description')
                                        ]);
        if ($result)
            session()->flash('success', trans('admin::app.settings.pages.update-success'));
        else
            session()->flash('success', trans('admin::app.settings.pages.update-fail'));

        return redirect()->route('admin.pages.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = DB::table('pages')->where('id',$id)->update(['is_deleted'=>1]);
        if ($delete)
            session()->flash('success', trans('admin::app.settings.pages.delete-success'));
        else
            session()->flash('success', trans('admin::app.settings.pages.delete-fail'));

        return redirect()->route('admin.pages.index');
 
    }


    public function ChangeHomePage(Request $request)
    {
    	//dd($request->all());
    	$get_home_page = DB::table('pages')->where('is_deleted',0)->where('is_home',1)->first();
    	if($get_home_page)
    		$change = DB::table('pages')->where('id',$get_home_page->id)->update(['is_home' =>0]);

    	$update_home_page = DB::table('pages')->where('id',$request->get('page_id'))->update(['is_home' =>1]);

    	return redirect()->route('admin.pages.index');

    }

    public function imageUploader(Request $request)
    {
        print_r($request->all());
        exit;
        $file=$request->file('image');
        $path= bagisto_asset('images').'/'.$file->getClientOriginalName();
        $imgpath=$file->move(bagisto_asset('images/'),$file->getClientOriginalName());
        $fileNameToStore= $path;
        dd($fileNameToStore);


        return json_encode(['location' => $fileNameToStore]); 
    }
}

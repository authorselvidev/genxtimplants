<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Sales\Repositories\OrderRepository as Order;
use Webkul\Sales\Repositories\OrderItemRepository as OrderItem;
use Cart;
use Mail;
use DB;
use Carbon\Carbon;
use Webkul\Checkout\Repositories\CartRepository;
use Webkul\Checkout\Repositories\CartItemRepository;
use Webkul\Product\Repositories\ProductRepository;

/**
 * Sales Order controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CustomOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    protected $cart;

    /**
     * CartItemRepository model
     *
     * @var mixed
     */
    protected $cartItem;

    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $order;
    protected $orderItem;
    protected $product;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Sales\Repositories\OrderRepository  $order
     * @return void
     */
    public function __construct(Order $order, OrderItem $orderItem, CartRepository $cart,
        CartItemRepository $cartItem,ProductRepository $product)
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->order = $order;
        $this->orderItem = $orderItem;
         $this->cart = $cart;

        $this->cartItem = $cartItem;

         $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    public function create($id)
    {
        $customer = \DB::table('users')->where('id',$id)->first();
        $products = \DB::table('product_flat')
                    ->leftjoin('products','products.id','product_flat.product_id')
                    ->leftjoin('product_inventories','product_inventories.product_id','product_flat.product_id')
                    ->where('products.is_deleted',0)
                    ->get();
        //dd($products);
        return view($this->_config['view'],compact('customer','products'));
    }


    public function stockAvailabilityCheck(Request $request)
    {
        $product = $this->product->findOneByField('id', $request->product_id);
            if ($product->haveSufficientQuantity($request->quantity)) {
                $success = true;
            }
            else
                $success=false;
            return response()->json(['success' => $success]);
        //dd($request->all());
    }


    public function store($id, Request $request)
    {
        
        $products = $request->get('products');
        foreach ($products as $key => $product) {
            $discount_price =0;
            $prodCategories = \DB::table('product_categories')->where('product_id', $product["id"])->pluck('category_id')->toArray();
            $get_price = \DB::table('products_grid')->where('product_id', $product["id"])->first()->price;

            $get_discount = \DB::table('discount_users as du')
                            ->where('du.user_id', $request->customer_id)
                            ->leftjoin('discount_category as dc','dc.discount_id','du.discount_id')
                            ->leftjoin('discounts as d','d.id','du.discount_id')
                            ->where('d.discount_type',1)
                            ->whereIn('dc.category_id', $prodCategories)
                            ->get();
            

            if(count($get_discount) > 0){
                $discount_per = $get_discount[0]->discount;
                $discount_price = $get_price *($discount_per / 100);
            }


            /*$discounts = \DB::table('discounts as d')
                                ->where('d.is_deleted',0)
                                ->leftjoin('discount_users as du','du.discount_id','d.id')
                                ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                               ->where('d.discount_type', 1)
                               ->where('du.user_id', $request->customer_id);

            $discount_categories = $discounts->pluck('category_id')->toArray();
            $temp_offers = $discounts->get();
            $same_categories = array_intersect($discount_categories, $prodCategories);
            $get_discount_per="";
            if(count($prodCategories) == count($same_categories)){
                foreach ($discount_categories as $key => $discount_category) {
                   if(!in_array($discount_category, $prodCategories)) {
                        unset($discount_categories[$key]);
                        $temp_offers->forget($key);
                    }
                }

                $get_discount_per = $temp_offers->pluck('discount')->toArray();
                $counted_dis = array_count_values($get_discount_per);
                arsort($counted_dis);
                $get_discount_per = key($counted_dis);
            }

            $discounted_price_with_tax="";
            if($get_discount_per!="")
            {
                $discount_per = $get_discount_per;
                $discount_price = $get_price *($discount_per / 100);
            }*/


            $product_id = $product['id'];
            $data = array(  'product'   => $product_id,
                            'quantity'  => $product['qty'],
                            'discount'  => $discount_price,
                            'customer_id' => $request->customer_id);

            $product = $this->product->findOneByField('id', $product_id);
            if ($product->haveSufficientQuantity($data['quantity'])) {
                
                Cart::add($product_id,$data);
                Cart::collectTotals($data);
            }else{
                session()->flash('warning', trans('shop::app.checkout.cart.quantity.inventory_warning'));
            }
        }

        $this->removeCartDiscount(cart()->getCart($data)->id);

        $user = \DB::table('users')->where('id', $request->customer_id)->first();
        $data['user'] = $user;

        return redirect()->route('admin.sales.custom_orders.cart',$id);
    }

    public function ManageCart($id)
    {
        session()->forget('membership_details');
        session()->forget('promocode_details');
        session()->forget('credit_discount_details');
        session()->forget('reward_details');
        session()->forget('promotion_details');

        $data = array('customer_id' => $id);
        // cart()->collectTotals($data);
        // dd(1);
        $get_cart = cart()->getCart($data);
        $dataView = array();
        $customer = \DB::table('users')->where('id',$id)->first();
        $dataView['customer'] = $customer;
        
        $paylaterwithcredit = DB::table('core_config')->where('code', 'sales.paymentmethods.paylaterwithcredit.active')->first();
        $dataView['paylaterwithcredit'] = $paylaterwithcredit;

        if(isset($get_cart) && isset($get_cart->items)){
            $product_weights=$product_categories=$get_categories=array();
            $prod_quantity = $get_cart->items_qty; 
            $users_table = \DB::table('users')->where('id',$customer->id)->first();
            $role_id = $users_table->role_id;
            $shipping_amount = 0;
            
            $list_offers = DB::table('discounts as d')
                                        ->where('d.is_deleted',0)
                                        ->leftjoin('promotions as pm','pm.discount_id','d.id')
                                        ->leftjoin('promotion_ordered_category as pc','pc.discount_id','d.id')
                                        ->where('pm.min_order_qty','<=',$prod_quantity)
                                        ->whereIn('pm.for_whom',[0,$role_id])
                                        ->whereIn('pc.category_id',$product_categories)
                                        ->where('d.discount_type',4)->get();

            foreach ($get_cart->items as $item)
                $product_weights[] = $item->total_weight;

            $total_product_weights = array_sum($product_weights);
            $get_weight_ranges = DB::table('shipping')->where('status',1)->get();
            foreach($get_weight_ranges as $label => $get_weight_range)
            {
                if($total_product_weights >= $get_weight_range->min_weight && $total_product_weights <= $get_weight_range->max_weight)
                {
                    $shipping_amount = $get_weight_range->shipping_amount;
                    break;
                }
            }
            $discount_price = 0;

            if($get_cart->discount_type == 2){ //membership
                $membership_details = [
                    'mem_discount_price' =>DB::table('cart_items')->where('cart_id', $get_cart->id)->where('discount_applied',1)->sum('discount_applied_amount')
                ];

                $dataView['membership_details'] = $membership_details;

                $discount_price = $membership_details['mem_discount_price'];

            }else if($get_cart->discount_type == 3){ //promocode
                $promocode_details = [
                    'promo_code_price' =>DB::table('cart_items')->where('cart_id', $get_cart->id)->where('discount_applied',1)->sum('discount_applied_amount')
                ];

                $discount_price = $promocode_details['promo_code_price'];
                $dataView['promocode_details'] = $promocode_details;
            }else if($get_cart->discount_type == 5){ // credit discount

                $credit_discount = [
                    'credit_discount_price' =>DB::table('cart_items')->where('cart_id', $get_cart->id)->where('discount_applied',1)->sum('discount_applied_amount')
                ];

                $discount_price = $credit_discount['credit_discount_price'];
                $dataView['credit_discount'] = $credit_discount;
            }else if($get_cart->discount_type ==6){

                $get_point = DB::table('points_and_commission_history')
                                ->where('type',1)
                                ->where('status',1)
                                ->first();
                
                $dataView['reward_details'] = $get_cart->reward_points_applied / $get_point->value;
                $discount_price = $dataView['reward_details'];
            }

            if($discount_price ==0){
                $get_price = abs($get_cart->sub_total - $get_cart->discount);
            }else{
                $get_price = abs($get_cart->sub_total - $discount_price);
            }

            $sub_total_shipping = $get_price + $shipping_amount;
            $get_tax = DB::table('tax_rates')->where('state',$customer->state_id)->first();
            $tax_amount = $sub_total_shipping * ($get_tax->tax_rate / 100);
            $total_amount = $sub_total_shipping + $tax_amount;
            $total_amount = round($total_amount,2);
            $dataView['total_amount'] = $total_amount;
            $dataView['tax_amount'] = $tax_amount;
    
            $get_cart = cart()->getCart($data);
            if( $get_cart->discount_type!=0 ){
                $discount = DB::table('discounts')->where('id', $get_cart->discount_id)->first();
                $dataView['discount'] = $discount;
                $dataView['discount_type'] = [2=>'Membership Discount', 3=>'Promo Code Discount', 4=>'Promotion Discount', 5=>'Credit Discount', 6=>'Reward Applied'];
            }
            
            $dataView['shipping_amount'] = $shipping_amount;
        }
        $dataView['get_cart'] = cart()->getCart($data);



        DB::table('reward_points')->where('user_id', $customer->id)
                                   ->where('points_credited',1)
                                   ->where('updated_date','<=', Carbon::now()->subDays(365))
                                   ->update(['points_expired' => 1]);

        $allRewardPoints = DB::table('reward_points')
                            ->where('user_id',$customer->id)
                            ->where('points_credited',1)
                            ->where('points_expired',0)
                            ->sum('reward_points');
        
        $usedRewardPoints =  DB::table('orders')
                            ->where('customer_id',$customer->id)
                            ->sum('reward_points_used');

        $availableRewardPoints=0;
        if($allRewardPoints > 0)
            $availableRewardPoints = abs($allRewardPoints - $usedRewardPoints);

        

        $dataView['availableRewardPoints'] = $availableRewardPoints;

        return view($this->_config['view'], $dataView);
    }

    public function removeCartDiscount($id){
        $cart = DB::table('cart')->where('id', $id)->first();
        if(isset($cart) && isset($cart->customer_id)){
            $customer = DB::table('users')->where('id', $cart->customer_id)->first();            
            DB::table('cart_items')->where('cart_id', $id)->update(['discount_applied'=>0,'discount_percentage'=>null,'discount_id'=>null,'discounted_subtotal'=>null,'discount_applied_amount'=>null]);            
            DB::table('cart')->where('id', $id)->update([
                'membership_discount' => 0,
                'promotion_applied' => 0,
                'discount_id' => 0,
                'discount_type' => 0,
                'promocode_discount' => 0,
                'credit_discount'=>0,
                'discount_percentage'=>0,
                'reward_discount'=>0,
                'reward_points_applied'=>0,
            ]);

            session()->forget('membership_details');
            session()->forget('promocode_details');
            session()->forget('credit_discount_details');
            session()->forget('reward_details');
            session()->forget('promotion_details');

            cart()->collectTotals(['customer_id'=>$customer->id]);
        }

        return redirect()->back();
    }

    public function ClearCart($id)
    {
        $data = ['customer_id' => $id];
        $cart = Cart::getCart($data);
        foreach ($cart->items as $key => $item) Cart::removeItem($item->id, $data);
        return redirect()->back();
    }

    public function ClearCartItem($id,$item_id)
    {
        $data = array('customer_id' => $id);
        $customer = \DB::table('users')->where('id',$id)->first();
        $cart = Cart::getCart($data);
        Cart::removeItem($item_id,$data);
        $this->removeCartDiscount($cart->id);
        return redirect()->route('admin.sales.custom_orders.cart',$id);
    }

    public function edit($id)
    {
        $data = array('customer_id' => $id);
        $customer = \DB::table('users')->where('id',$id)->first();
        $products = \DB::table('product_flat')
                    ->leftjoin('products','products.id','product_flat.product_id')
                    ->leftjoin('product_inventories','product_inventories.product_id','product_flat.product_id')
                    ->where('products.is_deleted',0)
                    ->get();
        $get_cart = Cart::getCart($data);
        //dd($get_cart->items);
        return view($this->_config['view'],compact('customer','get_cart','products'));
    }

    public function update($id, Request $request)
    {
        //dd($request->all());
        $cust_data = array('customer_id' => $id);
        $customer = \DB::table('users')->where('id',$id)->first();
        $get_cart = Cart::getCart($cust_data);

        $products = $request->get('products');
        foreach ($products as $key => $product) {
            $discount_price =0;
            $prodCategories = \DB::table('product_categories')->where('product_id', $product["id"])->pluck('category_id')->toArray();
            $get_price = \DB::table('products_grid')->where('product_id', $product["id"])->first()->price;

            $get_discount = \DB::table('discount_users as du')
                            ->where('du.user_id', $request->customer_id)
                            ->leftjoin('discount_category as dc','dc.discount_id','du.discount_id')
                            ->leftjoin('discounts as d','d.id','du.discount_id')
                            ->where('d.discount_type',1)
                            ->whereIn('dc.category_id', $prodCategories)
                            ->get();
            

            if(count($get_discount) > 0){
                $discount_per = $get_discount[0]->discount;
                $discount_price = $get_price *($discount_per / 100);
            }
            /*$discount_price =0;
            $prodCategories = \DB::table('product_categories')->where('product_id', $product["id"])->pluck('category_id')->toArray();
            $get_price = \DB::table('products_grid')->where('product_id', $product["id"])->first()->price;

            $discounts = \DB::table('discounts as d')
                                ->where('d.is_deleted',0)
                                ->leftjoin('discount_users as du','du.discount_id','d.id')
                                ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                               ->where('d.discount_type',1)
                               ->where('du.user_id', $request->customer_id);

            $discount_categories = $discounts->pluck('category_id')->toArray();
            $temp_offers = $discounts->get();
            $same_categories = array_intersect($discount_categories, $prodCategories);
            $get_discount_per="";
            if(count($prodCategories) == count($same_categories)){
                foreach ($discount_categories as $key => $discount_category) {
                   if(!in_array($discount_category, $prodCategories)) {
                        unset($discount_categories[$key]);
                        $temp_offers->forget($key);
                    }
                }

                $get_discount_per = $temp_offers->pluck('discount')->toArray();
                $counted_dis = array_count_values($get_discount_per);
                arsort($counted_dis);
                $get_discount_per = key($counted_dis);
            }

            $discounted_price_with_tax="";
            if($get_discount_per!="")
            {
                $discount_per = $get_discount_per;
                $discount_price = $get_price *($discount_per / 100);
            }*/
       
            $product_id = $product['id'];
            $data = array('product' => $product_id,'quantity' => $product['qty'], 'discount' => $discount_price, 'customer_id' => $request->customer_id);
            $product = $this->product->findOneByField('id', $product_id);
            if ($product->haveSufficientQuantity($data['quantity'])) {
                foreach ($get_cart->items as $key => $item) {
                    if($item->product_id == $product_id && $item->quantity != $data['quantity'])
                        Cart::updateItem($product_id,$data,$item->id);
                }
                Cart::add($product_id,$data);
                Cart::collectTotals($data);

            }
            else
                session()->flash('warning', trans('shop::app.checkout.cart.quantity.inventory_warning'));
        }

        $this->removeCartDiscount(cart()->getCart($data)->id);
        return redirect()->route('admin.sales.custom_orders.cart',$id);
    }

    /**
     * Show the view for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $order = $this->order->find($id);
        $paid_history = \DB::table('manual_payment_entry')->where('order_id',$id)->where('is_deleted',0)->latest()->get();
        return view($this->_config['view'], compact('order','paid_history'));
    }

    /**
     * Cancel action for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $result = $this->order->cancel($id);
        
        if ($result) {
            session()->flash('success', trans('Order canceled successfully.'));
        } else {
            session()->flash('error', trans('Order can not be canceled.'));
        }

        return redirect()->back();
    }

    public function ChangeOrderStatus(Request $request)
    {
       //dd($request->all());
       if($request->get('order_status') != null)
       {
            $order_id = $request->get('order_id');
            $order = $this->order->find($order_id);
            if($order->status == 'shipped')
            {
                $get_shipped_items = \DB::table('order_items')->where('order_id',$order_id)->get();
                foreach($get_shipped_items as $key => $get_shipped_item)
                {
                    $changed_status = \DB::table('order_items')
                                            ->where('order_id',$order_id)
                                            ->where('qty_shipped',$get_shipped_item->qty_shipped)
                                            ->update(['qty_delivered' => $get_shipped_item->qty_shipped]);
                }
                //$this->order->updateOrderStatus($order);
               
            }
            $order->status = $request->get('order_status');
            $order->save();
            
            if($request->get('order_status') == 'completed'){
                $total_orders_amount = \DB::table('orders')->where('customer_id',$order->customer_id)->where('status','completed')->sum('grand_total');
                //dd($order->customer_id,$total_orders_amount);
                $customer_group = \DB::table('user_groups')->where('order_amount_from', '<=',$total_orders_amount)
                                ->where('order_amount_to','>=', $total_orders_amount)->first();
                if(isset($customer_group))
                    $user_group = \DB::table('users')->where('id',$order->customer_id)->update(['customer_group_id' => $customer_group->id]);

                $points_credit = \DB::table('reward_points')->where('order_id',$order_id)
                                                ->update(['points_credited' => 1]);
                }
            
       }
       return redirect()->back();
    }


    public function PayDealerCommission(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'order_id' => 'required',
            'dealer_id' => 'required',
            'commission_paid_amount' => 'required'
        ]);
        
            $paid_amount = \DB::table('dealer_commission')->insert([
                                        'order_id' => $request->get('order_id'),
                                        'dealer_id' => $request->get('dealer_id'),
                                        'commission_paid_amount' => $request->get('commission_paid_amount'),
                                        'created_at' => date('Y-m-d H:i:s')
                                    ]);

            session()->flash('success', 'Commission has been added');
        
        return redirect()->route('admin.sales.orders.view', $request->get('order_id'));
    }

    public function UpdateDealerCommission(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'order_id' => 'required',
            'dealer_id' => 'required',
            'commission_paid_amount' => 'required'
        ]);
        
            $paid_amount = \DB::table('dealer_commission')->where('order_id',$request->get('order_id'))->update([
                    'commission_paid_amount' => $request->get('commission_paid_amount')
                                    ]);

            session()->flash('success', 'Commission has been updated');
        
        return redirect()->route('admin.sales.orders.view', $request->get('order_id'));
    }

    public function sendCartMail($id)
    {
        $user = \DB::table('users')->where('id', $id)->first();
        $data['user'] = $user;

        Mail::send('shop::emails.customer.custom-order', $data, function ($message) use ($user){
            $message->from('info@genxtimplants.com', 'GenXT');
            $message->to($user->email)->subject('Dental implants items added to your cart - GenXT');
        });

        session()->flash('success', 'Cart link sended successfully');
        return redirect()->back();
    }
}

<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ShippingCarrierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$shipping_carriers = DB::table('shipping_carriers')->where('status',1)->get();
        return view('admin::settings.shipping_carriers.index')->with(['shipping_carriers' => $shipping_carriers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::settings.shipping_carriers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
       
        $this->validate(request(), [
            'carrier_name' => 'required',
            'tracking_url' => 'required',
            'status' => 'required'
       ]);

        
            $insert_shipping_carriers = DB::table('shipping_carriers')->insert([
                                                'name' => $request->get('carrier_name'),
                                                'tracking_url' => $request->get('tracking_url'),
                                                'status' => $request->get('status'),
                                                'created_at' => date('Y-m-d H:i:s'),
                                                ]);
            if ($insert_shipping_carriers)
                session()->flash('success', 'New shipping carrier has been created!');
            else
                session()->flash('success', '');   
           
    
        return redirect()->route('admin.shippingcarrier.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $shipping_carrier = DB::table('shipping_carriers')->where('id',$id)->first();
        return view('admin::settings.shipping_carriers.edit')->with('shipping_carrier', $shipping_carrier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate(request(), [
            'carrier_name' => 'required',
            'tracking_url' => 'required',
            'status' => 'required'
       ]);

        $update_shipping = DB::table('shipping_carriers')->where('id',$id)->update([
                                           'name' => $request->get('carrier_name'),
                                            'tracking_url' => $request->get('tracking_url'),
                                            'status' => $request->get('status'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        ]);
        if ($update_shipping)
            session()->flash('success', 'Shipping Carrier updated successfully!');
        else
            session()->flash('success', 'Shipping Carrier cannot updated!');
        

        return redirect()->route('admin.shippingcarrier.index'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = DB::table('shipping_carriers')->where('id',$id)->update(['is_deleted'=>1]);
        if ($delete)
            session()->flash('success', 'Shipping Carrier deleted successfully!');
        else
            session()->flash('success', 'Shipping Carrier cannot deleted!');

        return redirect()->route('admin.shippingcarrier.index');
 
    }

}

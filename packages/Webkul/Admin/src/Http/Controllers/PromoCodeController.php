<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Webkul\Category\Repositories\CategoryRepository as Category;


class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected $category;

    public function __construct(Category $category)
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->category = $category;
    }


    public function index()
    {
        return view('admin::promo-codes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["items"] = $this->getDiscountTree();
        return view('admin::promo-codes.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate(request(), [
            'minimum_order_amount' => 'required',
            'for_whom'    => 'required|array|min:1',
            'for_whom.*'  => 'required',
            'discount_title' => 'required',
            'discount_usage_limit' => 'required',
            'discount_description' => 'required',
            'coupon_code' => 'required',
            'discount.*.categories' => 'required',
            'discount.*.discount_per' => 'required'
        ]);
        //dd($request->all());
        
        $date_start=$date_end=null;
        if($request->get('date_start') != "")
            $date_start = date('Y-m-d H:i:s', strtotime($request->get('date_start')));
        if($request->get('date_end') != "")
            $date_end = date('Y-m-d H:i:s', strtotime($request->get('date_end')));
        $discount_categories = array_values(request()->get('discount')?? []);

        $insert_discount_id = DB::table('discounts')->insertGetId([
                                        'discount_type' => 3,
                                        'for_all_users' =>1,
                                        'minimum_order_amount'=>$request->get('minimum_order_amount'),
                                        'date_start' => $date_start,
                                        'date_end' => $date_end,
                                        'coupon_code' => $request->get('coupon_code'),
                                        'dealer_commission' => $request->get('dealer_commission'),
                                        'discount_title' => $request->get('discount_title'),
                                        'discount_description' => $request->get('discount_description'),
                                        'discount_usage_limit' => $request->get('discount_usage_limit')
                                   ]);
        if($insert_discount_id)
        {
            foreach($discount_categories as $d_key => $discount_category)
            {


                $dis_cat = explode("," , $discount_category['categories']);
                $dis_cat = array_map(function($val){
                    return explode('-', $val)[1];
                }, $dis_cat);

                foreach($dis_cat as $d_key => $category)
                {
                    $insert_discount_doctors = DB::table('discount_category')->insert([
                                                'discount_id' => $insert_discount_id,
                                                'category_id' => $category,
                                                'discount' => $discount_category['discount_per']
                                        ]);
                }
            }
            //dd($request->get('for_whom'));
            $for_whom = $request->get('for_whom');
            if(count($for_whom) == 2) $for_whom[0] = 0;
            $insert_promo_code = DB::table('promo_codes')->insert([
                                            'discount_id' => $insert_discount_id,
                                            'apply_membership' => $request->get('apply_membership'),
                                            'earn_points' => $request->get('earn_points'),
                                            'for_whom' =>  $for_whom[0],
                                            
                                        ]);

            session()->flash('success', trans('admin::app.discounts.created-success'));
        }
        else
            session()->flash('success', trans('admin::app.discounts.created-fail'));
        
        return redirect()->route('admin.promo-code.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        
        $discount = DB::table('discounts')->where('id',$id)->first();

        $all_discount = [];
        $category_discounts = DB::table('discount_category')
                                            ->where('discount_id',$discount->id)
                                            ->select('discount')->get()->unique();
        foreach($category_discounts as $category_discount){
            $discount_category = DB::table('discount_category')
                                    ->where('discount_id', $discount->id)
                                    ->where('discount',$category_discount->discount)
                                    ->pluck('category_id')->toArray();

            if(count($discount_category) > 0) {
                $all_discount[] = [
                    'categorys'=> array_values($discount_category),
                    'percentage'=>$category_discount->discount
                ];
            }
        }

        $data['discount'] = $discount;
        $data["all_discount"] = json_encode($all_discount);
        $data['promo_codes'] = DB::table('promo_codes')->where('discount_id',$id)->first();
        $data["items"] = $this->getDiscountTree();
        return view('admin::promo-codes.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       $this->validate(request(), [
            'minimum_order_amount' => 'required',
            'for_whom'    => 'required|array|min:1',
            'for_whom.*'  => 'required',
            'discount_title' => 'required',
            'discount_description' => 'required',
            'discount_usage_limit' => 'required',
            'coupon_code' => 'required',
            'discount.*.categories' => 'required',
            'discount.*.discount_per' => 'required'
        ]);
       
        //dd($request->all()); 
        $date_start=$date_end=null;
        if($request->get('date_start') != "")
            $date_start = date('Y-m-d H:i:s', strtotime($request->get('date_start')));
        if($request->get('date_end') != "")
            $date_end = date('Y-m-d H:i:s', strtotime($request->get('date_end')));


        $delete_discount_users = DB::table('discount_users')
                                ->where('discount_id',$id)->delete();


        $discount_categories = array_values(request()->get('discount')?? []);
        $update_discount = DB::table('discounts')->where('id',$id)->update([
                                        'for_all_users' =>1,
                                        'minimum_order_amount'=>$request->get('minimum_order_amount'),
                                        'date_start' => $date_start,
                                        'date_end' => $date_end,
                                        'coupon_code' => $request->get('coupon_code'),
                                        'dealer_commission' => $request->get('dealer_commission'),
                                        'discount_title' => $request->get('discount_title'),
                                        'discount_description' => $request->get('discount_description'),
                                        'discount_usage_limit' => $request->get('discount_usage_limit')
                                   ]);
    
            $delete_discount_category = DB::table('discount_category')->where('discount_id',$id)->delete();
            foreach($discount_categories as $d_key => $discount_category)
            {

                $dis_cat = explode("," , $discount_category['categories']);
                
                $dis_cat = array_map(function($val){
                    return explode('-', $val)[1];
                }, $dis_cat);
            
                foreach($dis_cat as $d_key => $category)
                {
                    $insert_discount_doctors = DB::table('discount_category')->insert([
                                                'discount_id' => $id,
                                                'category_id' => $category,
                                                'discount' => $discount_category['discount_per']
                                        ]);
                }
                
            }

            $for_whom = $request->get('for_whom');
            if(count($for_whom) == 2) $for_whom[0] = 0;
             $insert_promo_code = DB::table('promo_codes')->where('discount_id',$id)->update([
                                            'apply_membership' => $request->get('apply_membership'),
                                            'earn_points' => $request->get('earn_points'),
                                            'for_whom' =>  $for_whom[0]
                                        ]);

            session()->flash('success', trans('admin::app.discounts.update-success'));
        

        return redirect()->route('admin.promo-code.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $delete = DB::table('discounts')->where('id',$id)->update(['is_deleted'=>1]);
        DB::table('promo_codes')->where('discount_id',$id)->delete();

        if ($delete)
            session()->flash('success', trans('admin::app.discounts.delete-success'));
        else
            session()->flash('success', trans('admin::app.disocunts.delete-fail'));

        return redirect()->route('admin.promo-code.index');
    }

    public function changeStatus(Request $request){
        $discount_promo = \DB::table('discounts')->where('id', $request->id)->update(['active'=>($request->state==1)?0:1]);         
        return redirect()->back();
    }

    private function getDiscountTree(){
        $items = array();

        foreach($this->category->getCategoryTree() as $cat){ //n-array preorder TreeTraversal algorithm
            if($cat!=null){
                $stack = array();
                array_push($stack, $cat);

                while(count($stack)>0){
                    $temp = array_pop($stack);
                    $childrens = $temp->children;
                    if(count($childrens)<=0){
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'end'=> true,
                                                 'name' => $temp->name));
                    }else{
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'end'=> false,
                                                 'name' => $temp->name));
                        for($i=count($childrens)-1; $i>=0; $i--){
                            array_push($stack, $childrens[$i]);
                        }
                    }
                }
            }
        }
        return json_encode($items);
    }
}

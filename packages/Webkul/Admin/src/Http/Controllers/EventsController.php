<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$events = DB::table('events')->where('is_deleted',0)->get();
        return view('admin::events.index',compact('events',$events));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //dd($request->all());
       $this->validate(request(), [
            'event_name' => 'string|required',
            'event_slug' => 'required',
            'event_content' => 'required',
        ]);

        $result = DB::table('events')->insert(['name' => $request->get('event_name'),
        									'slug' => $request->get('event_slug'),
                                            'description' => $request->get('event_content'),
                                            'start_date' => $request->get('start_date'),
                                            'end_date' => $request->get('end_date')
                                        ]);

        if ($result)
            session()->flash('success', 'Event created Successfully!!');
        else
            session()->flash('success', 'Event cannot be created');

        return redirect()->route('admin.events.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
       $event = DB::table('events')->where('id',$id)->first();
        return view('admin::events.edit')->with('event', $event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
         $this->validate(request(), [
            'event_name' => 'string|required',
            'event_slug' => 'required',
            'event_content' => 'required',
        ]);

        $result = DB::table('events')->where('id', $id)->update([
                                            'name' => $request->get('event_name'),
        									'slug' => $request->get('event_slug'),
                                            'description' => $request->get('event_content'),
                                            'start_date' => $request->get('start_date'),
                                            'end_date' => $request->get('end_date')
                                        ]);
        if ($result)
            session()->flash('success', 'Event updated Successfully!');
        else
            session()->flash('success', 'Event cannot be updated!');

        return redirect()->route('admin.events.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = DB::table('events')->where('id',$id)->update(['is_deleted'=>1]);
        if ($delete)
            session()->flash('success', trans('admin::app.settings.pages.delete-success'));
        else
            session()->flash('success', trans('admin::app.settings.pages.delete-fail'));

        return redirect()->route('admin.events.index');
 
    }
    
}

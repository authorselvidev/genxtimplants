<?php

namespace Webkul\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Admin\Exports\DataGridExport;
use Excel;
use DB;

/**
 * Export controlller
 *
 * @author    Rahul Shukla <rahulshukla.symfony517@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct(){
        $this->middleware('admin');
        $this->_config = request('_config');
    }

    public function index(){
        return view($this->_config['view']);
    }

    /**
     * function for export datagrid
     *
     * @return \Illuminate\Http\Response
    */
    public function export(Request $request)
    {
        $roleIn = array();

        if(isset($request->role_doctor)){
            array_push($roleIn, 3);
        }

        if(isset($request->role_dealer)){
            array_push($roleIn, 2);
        }

        $query = DB::table('users')->join('orders', 'orders.customer_id', 'users.id')->addSelect(
            'orders.id as Order ID',
            DB::raw('CONCAT(users.my_title, " ", users.first_name, " ",users.last_name) AS \'Customer Name\''), 
            'users.role_id as Role',
            'orders.status as Order Status',
            'users.email as Email', 
            'orders.total_qty_ordered  as Total Quantity',
            DB::raw('DATE(orders.created_at) as \'Order Date\''),
            'orders.base_sub_total as Sub Total',
            'orders.mem_discount_amount as Membership Discount',
            'orders.coupon_code as Discount Code',
            'orders.promo_code_amount as Promo Code',
            'orders.reward_coin as Reward Point',
            'orders.base_discount_amount as Special Discount',
            'orders.tax_amount as Tax (12%)',
            'orders.base_shipping_amount as Shipping & Handling',
            'orders.base_grand_total as Grand Total')->whereBetween('orders.created_at', [date($request->start_date), date('Y-m-d', strtotime("+1 day".$request->end_date))])->whereIn('users.role_id',$roleIn)->get()->toArray();
            $output = "Orders Report\n";

            $output .= implode(',', array(
                'From', $request->start_date."\n",
            ));

            $output .= implode(',', array(
                'To', $request->end_date."\n\n"
            ));


            $output .= implode(",", array("Order ID", "Customer Name", "Role", "Order Status", "Email", "Total Quantity", "Order Date", "Sub Total", "Membership Discount (-)", "Discount Code (-)", "Promo Code (-)", "Reward Point (-)", "Special Discount (-)", "Tax (12%)", "Shipping & Handling", "Grand Total\n"));

            $roles = array(
                '1' => 'Admin',
                '2' => 'Dealer',
                '3' => 'Doctor',
                '4' => 'Store Manager');

            foreach($query as $row){
                $output .=  implode(",", array(
                    $row->{'Order ID'},
                    $row->{'Customer Name'}, 
                    $roles[$row->{'Role'}],
                    ucwords($row->{'Order Status'}), 
                    $row->{'Email'},
                    $row->{'Total Quantity'}, 
                    $row->{'Order Date'}, 
                    round($row->{'Sub Total'},2), 
                    round($row->{'Membership Discount'},2),
                    $row->{'Discount Code'},
                    round($row->{'Promo Code'},2),
                    round($row->{'Reward Point'},2),
                    round($row->{'Special Discount'},2),
                    round($row->{'Tax (12%)'},2),
                    round($row->{'Shipping & Handling'},2),
                    round($row->{'Grand Total'},2)."\n",                
                ));
            }

            $headers = array(
                'Content-Type' => 'text/csv; charset=utf-8',
                'Content-Disposition' => 'attachment; filename=Report-'.$request->start_date.'-'.$request->end_date.'.csv',
                'Pragma' => 'no-cache',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Expires' => '0');
            return Response::make(rtrim($output, "\n"), 200, $headers);
    }
}
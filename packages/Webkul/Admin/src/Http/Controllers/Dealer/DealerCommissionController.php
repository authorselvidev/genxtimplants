<?php

namespace Webkul\Admin\Http\Controllers\Dealer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Customer\Repositories\CustomerRepository as Customer;
use Webkul\Customer\Models\States as State;
use Webkul\Customer\Models\Cities as Cities;
use Webkul\Customer\Repositories\CustomerGroupRepository as CustomerGroup;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Core\Repositories\ChannelRepository as Channel;
use Webkul\Customer\Mail\AccountCreationMail;
use Mail;
use DB;

/**
 * Customer controlller
 *
 * @author    Rahul Shukla <rahulshukla.symfony517@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DealerCommissionController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var array
     */
    protected $customer;

    protected $category;
     /**
     * CustomerGroupRepository object
     *
     * @var array
     */
    protected $customerGroup;

     /**
     * ChannelRepository object
     *
     * @var array
     */
    protected $channel;

     /**
     * Create a new controller instance.
     *
     * @param Webkul\Customer\Repositories\CustomerRepository as customer;
     * @param Webkul\Customer\Repositories\CustomerGroupRepository as customerGroup;
     * @param Webkul\Core\Repositories\ChannelRepository as Channel;
     * @return void
     */
    public function __construct(Customer $customer, CustomerGroup $customerGroup, Channel $channel,Category $category)
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->customer = $customer;
        $this->category = $category;

        $this->customerGroup = $customerGroup;

        $this->channel = $channel;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view($this->_config['view']);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customerGroup = $this->customerGroup->all();

        $channelName = $this->channel->all();
        $categories = $this->category->getCategoryTree();

        //dd($categories);

        return view($this->_config['view'],compact('customerGroup','channelName','categories'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        
    }


    public function show($id)
    {
       $dealer = $this->customer->findOneWhere(['id'=>$id]);
       $doctors_under_dealer = DB::table('users')->where('dealer_id',$id)->pluck('id')->toArray();
       $doctor_orders = DB::table('orders as o')
                        ->whereIn('o.customer_id',$doctors_under_dealer)->get();

        $total_orders_value = DB::table('orders')
                        ->whereIn('customer_id',$doctors_under_dealer)->sum('grand_total');
       //dd($doctors_under_dealer,$total_orders_value);
       $dealer_commissions = DB::table('dealer_commission')->where('dealer_id',$id)->get();
        $total_comm_paid = DB::table('dealer_commission')->where('dealer_id',$id)->sum('commission_paid_amount');
        $total_dealer_comm_amt=array();
        foreach ($doctor_orders as $key => $doctor_order) {
           $grandTotal = $doctor_order->grand_total;
        $total_dealer_comm_amt[] = ($dealer->dealer_commission / 100) * $grandTotal;
        }
        $balance_commission_to_pay = array_sum($total_dealer_comm_amt) - $total_comm_paid;

        return view($this->_config['view'],compact('dealer','dealer_commissions','total_comm_paid','doctors_under_dealer','doctor_orders','total_orders_value','balance_commission_to_pay'));
       
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->customer->delete($id);

        session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Customer']));

        return redirect()->back();
    }

   

    /*public function StoreDealerCommission(Request $request)
    {
       $get_last_record = DB::table('points_to_coin_conversion')->where('type',2)->first();
        $update_status = DB::table('points_to_coin_conversion')
                                ->where('closed_date',null)
                                ->update(['status' => 0,'closed_date' => date('Y-m-d H:i:s')]);

        $insert_points= DB::table('points_to_coin_conversion')->insert([
                                            'type' => 2,
                                            'value' => $request->get('commission'),
                                            'created_date' => date('Y-m-d H:i:s')
                                        ]);

        if ($insert_points)
            session()->flash('success','Dealer Commission Updated Successfully!');
        else
            session()->flash('success', 'Dealer Commission cannot be Updated!');
        
        

        return redirect()->route('admin.dealer.index');
    }*/
}

<?php

namespace Webkul\Admin\Http\Controllers\Dealer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Customer\Repositories\CustomerRepository as Customer;
use Webkul\Customer\Models\States as State;
use Webkul\Customer\Models\Cities as Cities;
use Webkul\Customer\Repositories\CustomerGroupRepository as CustomerGroup;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Core\Repositories\ChannelRepository as Channel;
use Webkul\Customer\Mail\AccountCreationMail;
use Illuminate\Validation\Rule;
use Mail;
use DB;

/**
 * Customer controlller
 *
 * @author    Rahul Shukla <rahulshukla.symfony517@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DealerController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var array
     */
    protected $customer;

    protected $category;
     /**
     * CustomerGroupRepository object
     *
     * @var array
     */
    protected $customerGroup;

     /**
     * ChannelRepository object
     *
     * @var array
     */
    protected $channel;

     /**
     * Create a new controller instance.
     *
     * @param Webkul\Customer\Repositories\CustomerRepository as customer;
     * @param Webkul\Customer\Repositories\CustomerGroupRepository as customerGroup;
     * @param Webkul\Core\Repositories\ChannelRepository as Channel;
     * @return void
     */
    public function __construct(Customer $customer, CustomerGroup $customerGroup, Channel $channel,Category $category)
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->customer = $customer;
        $this->category = $category;

        $this->customerGroup = $customerGroup;

        $this->channel = $channel;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view($this->_config['view']);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['customerGroup'] = DB::table('user_groups')->where('is_user_defined','!=', 0)->addSelect('id', 'name')->get();
        $data['channelName'] = $this->channel->all();
        $data['categories'] = $this->getDiscountTree();
        return view($this->_config['view'], $data);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //dd(request()->all());
        $this->validate(request(), [
            'my_title' => 'required',
            'first_name' => 'string|required',
            'email' => 'email|required|unique:users,email',
            'phone_number' => 'required|numeric',
            'state_id' => 'required',
            'city_id' => 'required',
            'customer_group_id' => 'required',
            'pin_code' => 'required'
        ]);

        $data=request()->all();
        //$data['customer_group_id'] = 5;
        //$data['user_type'] = 0;
        $data['country_id'] = 101;
        $data['is_approved'] = 1;
        $data['role_id'] = 2;
        //$data['password'] = $password;
        $data['channel_id'] = core()->getCurrentChannel()->id;
        $data['is_verified'] = 1;
        $verificationData['token'] = md5(uniqid(rand(), true));
        $data['token'] = $verificationData['token'];
        $full_name = $data['first_name'].' '.$data['last_name'];
        $dealer_created = $this->customer->create($data);
        $discount_categories = array_values(request()->get('discount')?? []);
        $dealer_commissions = array_values(request()->get('dealer_comm')?? []);

        

        if(isset($dealer_commissions[0]['categories']) && $dealer_commissions[0]['comm_percent'] != '')
        {
            foreach($dealer_commissions as $d_key => $dealer_commission)
            {
                $deal_comm = explode("," , $dealer_commission['categories']);
                $deal_comm = array_map(function($val){
                    return explode('-', $val)[1];
                }, $deal_comm);                        

                foreach($deal_comm as $dc_key => $category_id)
                {
                    $insert_dealer_commissions = DB::table('dealer_commissions')->insert([
                                                'dealer_id' => $dealer_created->id,
                                                'category_id' => $category_id,
                                                'commission_percent' => $dealer_commission['comm_percent']
                                            ]);
                }
            }
        }

        if(isset($discount_categories[0]['categories']) && !empty($discount_categories[0]['discount_per']))
        {
            $custom_code = mt_rand(10000, 99999);
            $gxt_custom_code = "GXT".$custom_code;
            $insert_discount_id = DB::table('discounts')->insertGetId([
                                            'customer_group_id' => $dealer_created->customer_group_id,
                                            'for_all_users' => 0,
                                            'discount_type' =>1,
                                            'coupon_code' => $gxt_custom_code
                                       ]);
            if($insert_discount_id)
            {
                foreach($discount_categories as $d_key => $discount_category)
                {
                        $dis_cat = explode("," , $discount_category['categories']);
                        $dis_cat = array_map(function($val){
                            return explode('-', $val)[1];
                        }, $dis_cat);
                        
                   
                        foreach($dis_cat as $d_key => $category_id)
                        {
                            $insert_discount_category = DB::table('discount_category')->insert([
                                                        'discount_id' => $insert_discount_id,
                                                        'category_id' => $category_id,
                                                        'discount' => $discount_category['discount_per']
                                                ]);
                        }
                        session()->flash('success', trans('admin::app.discounts.created-success'));
                }

                $insert_discount_users = DB::table('discount_users')->insert([
                                                    'discount_id' => $insert_discount_id,
                                                    'user_id' => $dealer_created->id,
                                                ]);
            }
        }
        

        $dealer_name = "";
        
        $data['set_password_url'] = url('customer/set-user-password/'.$data["token"]);
        if($dealer_created)
        {
            Mail::to($data['email'])->send(new AccountCreationMail($full_name, $dealer_name, $data['email'],$data['set_password_url']));
        }

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Customer']));

        return redirect()->route($this->_config['redirect']);
    }


    public function show($id)
    {
        $customer = $this->customer->findOneWhere(['id'=>$id]);
        $totalDoctorOrders = DB::table('orders')
                                        ->leftJoin('users', 'users.id', '=', 'orders.customer_id')
                                        ->where('orders.status','!=', 'canceled')
                                        ->where('users.dealer_id',$id)
                                        ->count();

        $totalDealerOrders = DB::table('orders')
                                        ->where('status','!=', 'canceled')
                                        ->where('customer_id',$id)->count();

        $doctorSales = DB::table('orders')->leftJoin('users', 'users.id', '=', 'orders.customer_id')
                                            ->where('orders.status','!=', 'canceled')
                                            ->where('users.dealer_id',$id)
                                            ->sum('grand_total');

        $dealerSales = DB::table('orders')
                                    ->where('status','!=', 'canceled')
                                    ->where('customer_id',$id)->sum('grand_total');


        $totalDoctors = DB::table('users')->where('dealer_id',$id)->count();
        $doctorsUnderDealer = DB::table('users')->where('dealer_id',$id)->where('is_deleted',0)->get();
        $doctorOrders = DB::table('users')->leftJoin('orders', 'users.id', '=', 'orders.customer_id')
                                            ->where('users.dealer_id',$id)
                                            ->where('orders.id','!=', null)
                                            ->orderBy('orders.created_at','desc')
                                            ->get();
        $dealerOrders = DB::table('orders')->where('customer_id',$id)
                                            ->orderBy('created_at','desc')
                                            ->get();

        $creditOrders = DB::table('orders')->where('customer_id',$id)
                                        ->leftJoin('order_payment', 'orders.id', '=', 'order_payment.order_id')
                                            ->where('order_payment.method','paylaterwithcredit')
                                            ->orderBy('orders.created_at','desc')
                                            ->get();
        $creditPayments = DB::table('credit_payments')->where('user_id',$id)
                                            ->where('status',1)
                                            ->orderBy('created_at','desc')
                                            ->get();

        $total_comm_paid = DB::table('dealer_commission')->where('dealer_id',$id)->sum('commission_paid_amount');
        //dd($totalDealerOrders);

        $credit_limit = DB::table('users')->where('id',$id)->value('credit_limit');
        $credit_used = DB::table('users')->where('id',$id)->value('credit_used');
        $credit_paid = DB::table('users')->where('id',$id)->value('credit_paid');
        $credit_pending_amount = $credit_used - $credit_paid;
        $available_credit_limit = $credit_limit - $credit_pending_amount;
        $credit_balance = DB::table('users')->where('id',$id)->value('credit_balance');
        $credit_pending = DB::table('credit_payments')
                                    ->where('user_id', $id)
                                    ->where('status', 0)
                                    ->sum('amount');
        
        $offline_due = DB::table('credit_payments_due')->where('user_id',  $id)->get();        

        $customerGroup = $this->customerGroup->all();
        $channelName = $this->channel->all();        

       return view($this->_config['view'], compact('customer', 'customerGroup', 'channelName',
                                                  'totalDoctorOrders','totalDealerOrders','doctorOrders',
                                                  'dealerOrders','creditOrders','totalDoctors',
                                                  'doctorSales','dealerSales','doctorsUnderDealer',
                                                  'total_comm_paid','credit_limit','available_credit_limit',
                                                  'credit_pending_amount','creditPayments', 'credit_balance', 'credit_pending', 'offline_due'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = $this->customer->findOneWhere(['id'=>$id]);

        $all_commission = [];
        $dealer_commissions = DB::table('dealer_commissions as dc')
                                        ->where('dc.dealer_id',$customer->id)
                                        ->groupBy('commission_percent')
                                        ->pluck('commission_percent'); 

        
        foreach($dealer_commissions as $dealer_commission){
            $comm_category = DB::table('dealer_commissions as dc')
                                    ->where('dc.dealer_id',$customer->id)
                                    ->where('commission_percent',$dealer_commission)
                                    ->pluck('category_id')->toArray();

            if(count($comm_category) > 0) {
                    $all_commission[] = [
                        'categorys'=> array_values($comm_category),
                        'percentage'=>$dealer_commission
                    ];
            }
        }

        $all_discount = [];
        $categry_discounts = DB::table('discounts as d')
                                                ->where('d.discount_type',1)
                                                ->leftjoin('discount_users as du','du.discount_id','d.id')
                                                ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                                ->where('du.user_id',$customer->id)
                                                ->select('dc.discount','d.id')
                                                ->groupBy('dc.discount')
                                                ->get()->unique();

        foreach($categry_discounts as $category_discount){
            $discount_category = DB::table('discount_category')
                                        ->where('discount_id', $category_discount->id)
                                        ->where('discount',$category_discount->discount)
                                        ->pluck('category_id')->toArray();

            if(count($discount_category) > 0) {
                $all_discount[] = [
                    'categorys'=> array_values($discount_category),
                    'percentage'=>$category_discount->discount
                ];
            }
        }

        $data['customerGroup'] = DB::table('user_groups')->where('is_user_defined','!=', 0)->addSelect('id', 'name')->get();
        #$data['categories'] = $this->category->getCategoryTree();
        $data['categories'] = $this->getDiscountTree();
        $data['channelName'] = $this->channel->all();
        $data['categry_discount'] = $categry_discounts;
        $data['customer'] = $customer;
        $data['all_commission'] = json_encode($all_commission);
        $data['all_discount'] = json_encode($all_discount);
        
        return view($this->_config['view'], $data);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->validate(request(), [
            'my_title' => 'required',
            'first_name' => 'string|required',
            'email' => ['email', 'required', Rule::unique('users')->ignore($id)],
            'phone_number' => 'required|numeric',
            'state_id' => 'required',
            'city_id' => 'required'
        ]);

        $data=request()->all();
        //$data['user_type'] = 0;
        $data['country_id'] = 101;
        //$data['password'] = $password;
        $data['channel_id'] = core()->getCurrentChannel()->id;
        
        $get_user = DB::table('users')->where('id', $id)->first();

        if($this->customer->findOneWhere(['id'=>$id])->customer_group_id != $request->customer_group_id){
            \DB::table('users')->where('id', $id)->update([ 'group_changed_by' => auth()->guard('admin')->user()->id,
															'group_changed_at' => date('Y-m-d H-i-s')]);
        }
        
        $this->customer->update($data,$id);
        $discount_categories = array_values(request()->get('discount')?? []);
        $dealer_commissions = array_values(request()->get('dealer_comm')?? []);
        //dd($dealer_commissions);

        DB::table('dealer_commissions')->where('dealer_id',$id)->delete();
        $discount_id = $request->get('discount_id');

        if($discount_id){
			DB::table('discounts')->where('id', $discount_id)->update([
								'customer_group_id' => $get_user->customer_group_id,
								'for_all_users' => 0,
								'discount_type' =>1 ]);
            DB::table('discount_category')->where('discount_id',$discount_id)->delete();
            DB::table('discount_users')->where('discount_id',$discount_id)->delete();
        }else{
			$discount_id = DB::table('discounts')->insertGetId([
								'customer_group_id' => $get_user->customer_group_id,
								'for_all_users' => 0,
								'discount_type' =>1 ]);
								
		}

        if(isset($dealer_commissions[0]['categories']) && $dealer_commissions[0]['comm_percent'] != '')
        {
            foreach($dealer_commissions as $d_key => $dealer_commission)
            {
                $deal_comm = explode("," , $dealer_commission['categories']);
                $deal_comm = array_map(function($val){
                    return explode('-', $val)[1];
                }, $deal_comm);

                foreach($deal_comm as $dc_key => $category_id)
                {
                    $insert_dealer_commissions = DB::table('dealer_commissions')->insert([
                                                'dealer_id' => $id,
                                                'category_id' => $category_id,
                                                'commission_percent' => $dealer_commission['comm_percent']
                                                ]);
                }
            }
        }

        if(isset($discount_categories[0]['categories']) && !empty($discount_categories[0]['discount_per']))
        {
                foreach($discount_categories as $d_key => $discount_category)
                {

                    $dis_cat = explode("," , $discount_category['categories']);

                    $dis_cat = array_map(function($val){
                        return explode('-', $val)[1];
                    }, $dis_cat);
                        
                    foreach($dis_cat as $d_key => $category)
                    {
                        $insert_discount_doctors = DB::table('discount_category')->insert([
                                                    'discount_id' => $discount_id,
                                                    'category_id' => $category,
                                                    'discount' => $discount_category['discount_per']
                                            ]);
                    }
                    
                }

                $insert_discount_users = DB::table('discount_users')->insert([
                                                    'discount_id' => $discount_id,
                                                    'user_id' => $id,
                                                ]);

                
            
        }
        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Customer']));
        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->customer->delete($id);

        session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Customer']));

        return redirect()->back();
    }

    public function massDestroy()
    {
        $dealerIds = explode(',', request()->input('indexes'));

        foreach ($dealerIds as $dealerId) {
            $delete =  DB::table('users')->where('id',$dealerId)->update(['is_deleted' => 1]);
        }

        session()->flash('success', trans('admin::app.response.delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /*public function StoreDealerCommission(Request $request)
    {
       $get_last_record = DB::table('points_and_commission_history')->where('type',2)->first();
        $update_status = DB::table('points_and_commission_history')
                                ->where('closed_date',null)
                                ->update(['status' => 0,'closed_date' => date('Y-m-d H:i:s')]);

        $insert_points= DB::table('points_and_commission_history')->insert([
                                            'type' => 2,
                                            'value' => $request->get('commission'),
                                            'created_date' => date('Y-m-d H:i:s')
                                        ]);

        if ($insert_points)
            session()->flash('success','Dealer Commission Updated Successfully!');
        else
            session()->flash('success', 'Dealer Commission cannot be Updated!');
        
        

        return redirect()->route('admin.dealer.index');
    }*/

    public function ShowCommissionDetails(Request $request)
    {
        $response=$data=array();
        $data['order_items'] = DB::table('order_items')->where('order_id',$request->order_id)->get();
        $data['order'] = DB::table('orders')->where('id',$request->order_id)->first();
        $view_rend=view('admin::dealers.list_comm_details',$data)->render();
        $response['view'] = $view_rend;
        return response()->json($response);
    }

    public function CommissionPaidHistory(Request $request)
    {
        $response=$data=array();
        $data['commission_paid_history'] = DB::table('dealer_commission')->where('order_id',$request->order_id)->where('dealer_id',$request->dealer_id)->get();
        $data['total_paid'] = DB::table('dealer_commission')->where('order_id',$request->order_id)->where('dealer_id',$request->dealer_id)->sum('commission_paid_amount');
        $view_rend=view('admin::dealers.comm_paid_history',$data)->render();
        $response['view'] = $view_rend;
        return response()->json($response);
    }


    public function payAllDoctorsCommission(Request $request)
    {
        $pay_val = $request->pay_val;
        
        $total_value = 0;
        if($pay_val <=0){
            session()->flash('error', "Invalid Commission Amount");
            return;
        }

        $dealer_id = $request->dealer_id;
        $doctors = DB::table('users')->where('dealer_id', $dealer_id)->where('is_deleted',0)->addSelect('id')->get();
        
        $orders_ids = array();
        
        foreach($doctors as $doc){
            $doctor_orders = DB::table('orders')
                                    ->where('customer_id', $doc->id)
                                    ->whereIn('status', ["delivered", "completed"])
                                    ->where('commission_paid', 0)
                                    ->get();
            if(count($doctor_orders) > 0){
                foreach($doctor_orders as $order){
                    $commission_paid_amount = DB::table('dealer_commission')->where('order_id', $order->id)->where('dealer_id', $dealer_id)->where('doctor_id', $doc->id)->sum('commission_paid_amount');                    
                    $total_value += $order->total_comm_amount - $commission_paid_amount;
                    $orders_ids[$order->id] = array();
                    $orders_ids[$order->id]['doctor'] = $doc->id;                    
                }
            }
        }

        if($total_value<$pay_val){
            session()->flash('error', "Invalid Commission Amount");
            return;
        }
        
        ksort($orders_ids);
        
        foreach($orders_ids as $id => $order_data){
            $order = DB::table('orders')->where('id', $id)->first();
            $dealer_comm_amt = $order->total_comm_amount;
            $commission_paid_amount = DB::table('dealer_commission')->where('order_id', $order->id)->where('dealer_id', $dealer_id)->where('doctor_id', $order_data['doctor'])->sum('commission_paid_amount');
            $balance_comm_amount = $dealer_comm_amt - $commission_paid_amount;
            if($pay_val>=$balance_comm_amount){
                DB::table('dealer_commission')->insert([
                    'order_id' => $order->id,
                    'dealer_id' => $dealer_id,
                    'doctor_id' => $order_data['doctor'],
                    'commission_paid_amount' => $balance_comm_amount,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
                DB::table('orders')->where('id', $order->id)->update(['commission_paid'=>1]);
                $pay_val -= $balance_comm_amount;
            }else if($pay_val < $balance_comm_amount && $pay_val>0){
                DB::table('dealer_commission')->insert([
                    'order_id' => $order->id,
                    'dealer_id' => $dealer_id,
                    'doctor_id' => $order_data['doctor'],
                    'commission_paid_amount' => $pay_val,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
                
                if($balance_comm_amount == $pay_val){
                    DB::table('orders')->where('id', $order->id)->update(['commission_paid'=>1]);
                }
                $pay_val -= $pay_val;
                session()->flash('success', "Commission paid successfully");
                return;
            }else if($pay_val<=0){
                session()->flash('success', "Commission paid successfully");
                return;
            }            
        }
    }

    private function getDiscountTree(){
        $items = array();

        foreach($this->category->getCategoryTree() as $cat){ //n-array preorder TreeTraversal algorithm
            if($cat!=null){
                $stack = array();
                array_push($stack, $cat);

                while(count($stack)>0){
                    $temp = array_pop($stack);
                    $childrens = $temp->children;
                    if(count($childrens)<=0){
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'end'=> true,
                                                 'name' => $temp->name));
                    }else{
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'end'=> false,
                                                 'name' => $temp->name));
                        for($i=count($childrens)-1; $i>=0; $i--){
                            array_push($stack, $childrens[$i]);
                        }
                    }
                }
            }
        }
        return json_encode($items);
    }
}

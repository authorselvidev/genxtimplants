<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SupportAndSalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$support_and_sales = DB::table('support_and_sales')->where('is_deleted',0)->get();
        return view('admin::settings.support_and_sales.index')->with(['support_and_sales' => $support_and_sales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::settings.support_and_sales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$picture =$request->file('support_image');
        if($request->hasFile('support_image'))
        {
            $pic_basename=$picture->getClientOriginalName();
            $pic_type=$picture->getMimeType();
            $trimmed_img = str_replace(' ', '-', strtolower($pic_basename));
            $picture->move(public_path('themes/default/assets/images/'), $trimmed_img);
            $insert_support = DB::table('support_and_sales')->insert([
                                            'support_text' => $request->get('support_text'),
                                            'support_image' => $trimmed_img,
                                            'created_at' => date('Y-m-d H:i:s')
                                        ]);
            if ($insert_support)
            session()->flash('success', trans('admin::app.settings.support_and_sales.created-success'));
        else
            session()->flash('success', trans('admin::app.settings.support_and_sales.created-fail'));
        }

        

        return redirect()->route('admin.support_and_sales.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $support = DB::table('support_and_sales')->where('id',$id)->first();
        return view('admin::settings.support_and_sales.edit')->with('support', $support);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         $picture =$request->file('support_image');
        if($request->hasFile('support_image'))
        {
            $pic_basename=$picture->getClientOriginalName();
            $pic_type=$picture->getMimeType();
            $trimmed_img = str_replace(' ', '-', strtolower($pic_basename));
            $picture->move(public_path('themes/default/assets/images/'), $trimmed_img);
            $insert_support = DB::table('support_and_sales')->where('id',$id)->update([
                                            'support_text' => $request->get('support_text'),
                                            'support_image' => $trimmed_img
                                        ]);
        }
        else
        {
            //dd($id);
            $insert_support = DB::table('support_and_sales')->where('id',$id)->update([
                                            'support_text' => $request->get('support_text')
                                        ]);
        }

        if ($insert_support)
            session()->flash('success', trans('admin::app.settings.support_and_sales.update-success'));
        else
            session()->flash('success', trans('admin::app.settings.support_and_sales.update-fail'));
        

        return redirect()->route('admin.support_and_sales.index'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = DB::table('support_and_sales')->where('id',$id)->update(['is_deleted'=>1]);
        if ($delete)
            session()->flash('success', trans('admin::app.support_and_sales.pages.delete-success'));
        else
            session()->flash('success', trans('admin::app.support_and_sales.pages.delete-fail'));

        return redirect()->route('admin.support_and_sales.index');
 
    }

}

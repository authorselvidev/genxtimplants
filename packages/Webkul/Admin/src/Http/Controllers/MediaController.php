<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use File;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$medias = DB::table('media')->where('is_deleted',0)->get();
        return view('admin::settings.media.index')->with(['medias' => $medias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::settings.media.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$this->validate(request(), [
            'media_file' => 'mimes:jpeg,jpg,bmp,png,gif,svg,pdf',
        ]);

        $media_file =$request->file('media_file');
        if($request->hasFile('media_file'))
        {
            $media_basename=$media_file->getClientOriginalName();
            $media_type=$media_file->getMimeType();
            $trimmed_name = str_replace(' ', '-', strtolower($media_basename));
            $dir_name = 'images';
            if(strpos($media_type,'pdf')) $dir_name = 'pdfs';
            $media_file->move(public_path('themes/default/assets/'.$dir_name.'/uploads/'), $trimmed_name);
            $insert_media = DB::table('media')->insert([
                                            'media_name' => $trimmed_name,
                                            'media_type' => $media_type,
                                            'uploaded_date' => date('Y-m-d H:i:s')
                                        ]);
        }
                
       /*$this->validate(request(), [
            'image' => 'mimes:jpeg,bmp,png,gif,svg,pdf',
        ]);

        $result = DB::table('pages')->insert(['page_name' => $request->get('page_name'),
        									'page_slug' => $request->get('page_slug'),
                                            'page_content' => $request->get('page_content'),
                                            'meta_title' => $request->get('meta_title'),
                                            'meta_keyword' => $request->get('meta_keyword'),
                                            'meta_description' => $request->get('meta_description')
                                        ]);

        if ($result)
            session()->flash('success', trans('admin::app.settings.pages.created-success'));
        else
            session()->flash('success', trans('admin::app.settings.pages.created-fail'));*/

        return redirect()->route('admin.media.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $get_media = DB::table('media')->where('id',$id)->first();
        $dir_name = 'images';
        if(strpos($get_media->media_type, 'pdf')) $dir_name = 'pdfs';
        $media_path = public_path('/themes/default/assets').'/'.$dir_name.'/uploads/'.$get_media->media_name;

        if(File::exists($media_path))
        {
            File::delete($media_path);
            $delete_media = DB::table('media')->where('id',$id)->delete();
            session()->flash('success', 'The selected media has been deleted!');
        }
        else
            session()->flash('success', 'The media has not be deleted!');

        return redirect()->route('admin.media.index');
 
    }

}

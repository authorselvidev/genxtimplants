<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Admin\Exports\DataGridExport;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Product\Repositories\ProductRepository as Product;
use Excel;
use DB;


class UserReportController extends Controller
{
    public function __construct(Category $category, Product $product){
        $this->middleware('admin');
        $this->_config = request('_config');
        $this->category = $category;
        $this->product = $product;
    }


    public function index(){
        $data['dealers'] = \DB::table('users')
                        ->where('is_deleted', 0)
                        ->where('role_id', 2)
                        ->where('is_verified', 1)
                        ->where('is_approved', 1)
                        ->get();        
        
        return view($this->_config['view'], $data);
    }


    public function export(Request $request)
    {
        $input = $request->all();
        $table_data = array();
        $credit_order_total = 0;
        $output = '';
        $dealer = DB::table('users')->where('id', $input['dealers'])->first();

        $dealer_orders = DB::table('orders as o')
                            ->leftjoin('order_payment as op', 'op.order_id', 'o.id')
                            ->where('o.customer_id', $input['dealers'])
                            ->where('o.status','!=', 'canceled')
                            ->orderByDesc('o.created_at')
                            ->addSelect('o.id as id')
                            ->addSelect('o.status as status')
                            ->addSelect('o.created_at as created_at')
                            ->addSelect('o.base_sub_total as base_sub_total')
                            ->addSelect('o.base_shipping_amount as base_shipping_amount')
                            ->addSelect('o.mem_discount_amount as mem_discount_amount')
                            ->addSelect('o.credit_discount_amount as credit_discount_amount')
                            ->addSelect('o.promo_code_amount as promo_code_amount')
                            ->addSelect('o.reward_coin as reward_coin')
                            ->addSelect('o.base_discount_amount as base_discount_amount')
                            ->addSelect('o.base_tax_amount as base_tax_amount')
                            ->addSelect('o.base_grand_total as base_grand_total')
                            ->addSelect('op.method as payment_method')
                            ->get();

        $dealer_credit_paid = DB::table('credit_payments')->where('user_id', $input['dealers'])->where('status', 1)->orderByDesc('created_at')->get();
        $offline_payment_order  = DB::table('credit_payments_due')->where('user_id', $input['dealers'])->sum('amount');
        
        $payment_data = [
            'razorpay' => [],
            'bank_transfer' => [],
            'total_orders' => [],
        ];

        $total_creditorders=0;

        foreach($dealer_orders as $order){
            $data = [
                'order_id' => $order->id,
                'date' => $order->created_at,
                'amount' => $order->base_grand_total,
            ];

            if($order->payment_method=="razorpay") $payment_data['razorpay'][] = $data;

            if($order->payment_method=="paylaterwithcredit") $total_creditorders+= $order->base_grand_total;
            
            $payment_data['total_orders'][] = $data;
        }

        foreach($dealer_credit_paid as $bank_transfer){
            $data = [
                'date' => $bank_transfer->created_at,
                'amount' => $bank_transfer->amount,
            ];

            if($bank_transfer->payment_type == 1){
                $payment_data['razorpay'][] = $data;
            }else{
                $payment_data['bank_transfer'][] = $data;
            }
        }

        // function date_sort($a, $b) {
        //     return strtotime($a) - strtotime($b);
        // }
        // usort($arr, "date_sort");
        
        array_push($table_data, implode(',', array('<h5 skip>Dealer name :</h5>', '<h5>'.$dealer->my_title." ".$dealer->first_name." ".$dealer->last_name.'</h5>' )));
        array_push($table_data, implode(',', array('<h5 skip>Dealer email :</h5>', '<h5>'.$dealer->email."</h5>" )));

        $razorpay_total = 0;
        array_push($table_data, implode(',', array('<h3>Payments Made via Razorpay</h3>')));
        array_push($table_data, implode(',', array(
            "<th><b>Date</b></th>",
            "<th><b>Amount</b></th>",
        )));
        foreach($payment_data['razorpay'] as $razorpay_payments){
            array_push($table_data, implode(',', [
                date("d-m-Y", strtotime($razorpay_payments['date'])),
                number_format($razorpay_payments['amount'], 2, '.', ""),
            ]));
            $razorpay_total += $razorpay_payments['amount'];
        }
        array_push($table_data, implode(',', array('<b>Total</b>', '<b>'.number_format($razorpay_total, 2, '.', "")."</b>" )));

        $neft_bank_transfer = 0;
        array_push($table_data, implode(',', array('<h3>Payments Made via NEFT/Bank Transfer</h3>')));
        array_push($table_data, implode(',', array(
            "<th><b>Date</b></th>",
            "<th><b>Amount</b></th>",
        )));
        foreach($payment_data['bank_transfer'] as $bank_transfer){
            array_push($table_data, implode(',', [
                date("d-m-Y", strtotime($bank_transfer['date'])),
                number_format($bank_transfer['amount'], 2, '.', ""),
            ]));
            $neft_bank_transfer += $bank_transfer['amount'];
        }
        array_push($table_data, implode(',', array('<b>Total</b>', '<b>'.number_format($neft_bank_transfer, 2, '.', "")."</b>" )));


        $order_total = 0;
        array_push($table_data, implode(',', array('<h3>Totals Online Orders</h3>')));
        array_push($table_data, implode(',', array(
            "<th><b>Order ID</b></th>",
            "<th><b>Date</b></th>",
            "<th><b>Amount</b></th>",
        )));

        foreach($payment_data['total_orders'] as $orders){
            array_push($table_data, implode(',', [
                $orders['order_id'],
                date("d-m-Y", strtotime($orders['date'])),
                number_format($orders['amount'], 2, '.', ""),
            ]));
            $order_total += $orders['amount'];
        }
        array_push($table_data, implode(',', array(',<b>Total</b>', '<b>'.number_format($order_total, 2, '.', "")."</b>" )));
        array_push($table_data, implode(',', array('<h3>Account Summary</h3>')));
        
        array_push($table_data, implode(',', [
            '<h5 skip>Totals payment via Razor pay</h5>',
            '<h5 skip>'.number_format($razorpay_total, 2, '.', "").'</h5>',
        ]));

        array_push($table_data, implode(',', [
            '<h5 skip>Totals payment via NEFT/Bank Transfer</h5>',
            '<h5 skip>'.number_format($neft_bank_transfer, 2, '.', "").'</h5>',
        ]));

        $total_payment = $razorpay_total+$neft_bank_transfer;

        array_push($table_data, implode(',', [
            '<h5 skip><b>Total Payments Made</b></h5>',
            '<h5 skip><b>'.number_format($total_payment, 2, '.', "").'</b></h5>',
        ]));

        
        $total_online_orders  = $order_total;
        $total_offline_orders = $dealer->credit_used - $total_creditorders;        
        $total_orders  = $total_online_orders+((($total_offline_orders<=0)?0:$total_offline_orders)+$offline_payment_order);

        array_push($table_data, implode(',', [
            '<h5 skip>Total Online Orders</h5>',
            '<h5 skip>'.number_format($total_online_orders, 2, '.', "").'</h5>',
        ]));

        array_push($table_data, implode(',', [
            '<h5 skip>Total Offline Orders</h5>',
            '<h5 skip>'.number_format(((($total_offline_orders<=0)?0:$total_offline_orders)+$offline_payment_order), 2, '.', "").'</h5>',
        ]));

        array_push($table_data, implode(',', [
            '<h5 skip><b>Total Orders</b></h5>',
            '<h5 skip><b>'.number_format($total_orders, 2, '.', "").'</b></h5>',
        ]));

        array_push($table_data, implode(',', [
            '<h5 skip><b>Balance Due in Account</b></h5>',
            '<h5 skip><b>'.number_format(($total_payment-$total_orders), 2, '.', "").'</b></h5>',
        ]));
        
        $output = implode("\n", $table_data); 
        return Response::json(array("csv"   => $output));
    }
}


<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Admin\Exports\DataGridExport;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Product\Repositories\ProductRepository as Product;
use Excel;
use DB;


class CommissionReportsController extends Controller
{
    public function __construct(Category $category, Product $product){
        $this->middleware('admin');
        $this->_config = request('_config');
        $this->category = $category;
        $this->product = $product;
    }


    public function index(){
        return view($this->_config['view']);
    }

    private function getRootCategory($category_id){
        /*$category_id = DB::table('product_categories')->where('product_id', $id)->orderByDesc('category_id')->addSelect('category_id')->first()->category_id;*/
        $category = $this->category->FindCategoryNameById($category_id);

        while ($category->parent_id != null)
            $category  = $this->category->FindCategoryNameById($category->parent_id);

        return $category;
    }

    public function export(Request $request)
    {
        $input = $request->all();
        $output = '';

        $user_orders = DB::table('users as u')
                        ->leftjoin('orders as o', 'o.customer_id', 'u.id')
                        ->whereIn('dealer_id', $input['dealers'])
                        ->whereIn('o.status', ['completed', 'delivered'])
                        ->where('o.total_comm_amount','!=', '0')
                        ->get();

        $header = array(
                    "<th><b>Order ID</b></th>",
                    "<th><b>Order Status</b></th>",
                    "<th><b>Order Date</b></th>",
                    "<th><b>Dealer Name</b></th>",
                    "<th><b>Dealer Email</b></th>",
                    "<th><b>Doctor Name</b></th>",
                    "<th><b>Doctor Email</b></th>",
                    "<th><b>Product</b></th>",
                    "<th><b>Price</b></th>",
                    "<th><b>Quantity</b></th>",
                    "<th><b>Total</b></th>",
                    "<th><b>Commission (%)</b></th>",
                    "<th><b>Commission Amount</b></th>",
                    "<th><b>Total Commission</b></th>",
                    "<th><b>Commission Paid</b></th>",
                    "<th><b>Commission Pending</b></th>");

        $table_data = array();
        array_push($table_data, implode(',', $header));

        $total_commition = 0;
        $total_commition_paid =0;
        $total_commition_pending = 0;
        $total_commition = 0;
        foreach ($user_orders as $o_key => $order) {
            $dealer = DB::table('users')->where('id', $order->dealer_id)->first();
            $order_head = true;
            $order_items = DB::table('order_items')->where('order_id',$order->id)->get();
            foreach ($order_items as $ot_key => $item) {
                if($order_head){
                    $row = array(
                        $order->id,
                        ucwords($order->status),
                        date('d-m-Y', strtotime($order->created_at)),
                        $dealer->my_title.' '.$dealer->first_name.' '.$dealer->last_name,
                        $dealer->email,
                        $order->my_title.' '.$order->first_name.' '.$order->last_name,
                        $order->email,
                        $item->name,
                        number_format($item->price, 2, '.', ""),
                        $item->qty_ordered,
                        number_format($item->base_total, 2, '.', ""),
                        $item->dealer_comm_percent,
                        number_format($item->dealer_comm_amount, 2, '.', ""),
                    );
                    $order_head= false;
                }else{
                    $row = array(
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        $item->name,
                        number_format($item->price, 2, '.', ""),
                        $item->qty_ordered,
                        number_format($item->base_total, 2, '.', ""),
                        $item->dealer_comm_percent,
                        number_format($item->dealer_comm_amount, 2, '.', ""),
                    );
                }

                if($ot_key == count($order_items)-1){
                    $commition_paid_amount = DB::table('dealer_commission')
                                                ->where('order_id',$order->id)
                                                ->where('dealer_id',$dealer->id)
                                                ->sum('commission_paid_amount');

                    $pending_amount = $order->total_comm_amount - $commition_paid_amount;

                    array_push($row, number_format($order->total_comm_amount, 2, '.', ''),
                                        number_format($commition_paid_amount, 2, '.', ''),
                                        number_format($pending_amount, 2, '.', ''));

                    $total_commition += $order->total_comm_amount;
                    $total_commition_paid += $commition_paid_amount;
                    $total_commition_pending += $pending_amount;
                }else{
                    array_push($row, '', '', '');
                }
                array_push($table_data, implode(',', $row));
            }

            if($o_key == count($user_orders)-1){
                $total_commition = number_format($total_commition, 2, '.', '');
                $total_commition_paid = number_format($total_commition_paid, 2, '.', '');
                $total_commition_pending = number_format($total_commition_pending, 2, '.', '');
                array_push($table_data, implode(',', ['', '', '', '', '', '', '', '', '', '', '', '', '<b>Total</b>', '<b>'.$total_commition.'</b>', '<b>'.$total_commition_paid.'</b>', '<b>'.$total_commition_pending.'</b>']));
                array_push($table_data, implode(',', ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']));
            }
            array_push($table_data, implode(',', ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']));
        }

        $output = implode("\n", $table_data);
        return Response::json(array("csv"   => $output));
    }
}
<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use Webkul\Category\Repositories\CategoryRepository as Category;


class CreditDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected $category;

    public function __construct(Category $category)
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->category = $category;
    }

    public function index(){
        return view('admin::credit-discounts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = $this->category->getCategoryTree();
        $data['credit_users'] = $this->getCreditUsers();
        $data["items"] = $this->getDiscountTree();
        return view('admin::credit-discounts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'minimum_order_amount' => 'required',
            'discount_title' => 'required',
            'discount_description' => 'required',
            'coupon_code' => 'required',
            'discount.*.categories' => 'required',
            'discount.*.discount_per' => 'required',
            // 'date_start' => 'required',
            // 'date_end' => 'required',
        ]);
        // if($request->get('date_start') != "" && $request->get('date_end') != ""){
        //     if(strtotime($request->get('date_start')) > strtotime($request->get('date_end'))){
        //         session()->flash('warning', 'Date Invalid!');
        //         return redirect()->back();
        //     }
        // }

        $date_start=$date_end=null;
        if($request->get('date_start') != ""){
            $date_start = date('Y-m-d H:i:s', strtotime($request->get('date_start')));
        }


        if($request->get('date_end') != ""){
            $date_end = date('Y-m-d H:i:s', strtotime($request->get('date_end')));
        }

        

        $discount_categories = array_values(request()->get('discount') ?? []);
        
        $insert_discount_id = DB::table('discounts')->insertGetId([
                                        'discount_type' => 5,
                                        'for_all_users' => ($request->get('for_all_users')!=0)?1:0,
                                        'minimum_order_amount'=>$request->get('minimum_order_amount'),
                                        'date_start' => $date_start,
                                        'date_end' => $date_end,
                                        'coupon_code' => $request->get('coupon_code'),
                                        'dealer_commission' => $request->get('dealer_commission'),
                                        'discount_title' => $request->get('discount_title'),
                                        'discount_description' => $request->get('discount_description'),
                                        'discount_usage_limit' => $request->get('discount_usage_limit')
                                   ]);

        if($insert_discount_id)
        {
            foreach($discount_categories as $d_key => $discount_category)
            {
                $dis_cat = explode("," , $discount_category['categories']);
                $dis_cat = array_map(function($val){
                    return explode('-', $val)[1];
                }, $dis_cat);

                foreach($dis_cat as $d_key => $category)
                {
                    $insert_discount_doctors = DB::table('discount_category')->insert([
                                                'discount_id' => $insert_discount_id,
                                                'category_id' => $category,
                                                'discount' => $discount_category['discount_per']
                                        ]);
                }
            }

            DB::table('discount_users')->where('discount_id', $insert_discount_id)->delete();

            $whoom = ($request->get('for_all_users')==2 || $request->get('for_all_users')==3)?$request->get('for_all_users'):0;

            $insert_promo_code = DB::table('promo_codes')->insert([
                                            'discount_id' => $insert_discount_id,
                                            'apply_membership' => 1,
                                            'earn_points' => $request->get('earn_points'),
                                            'for_whom' =>  $whoom,                                            
                                        ]);

            if($request->get('for_all_users') == 0)
            {
                $doctor_lists = array_unique($request->get('users_list'));

                foreach($doctor_lists as $doctor_list){
                    $insert_discount_users = DB::table('discount_users')->insert([
                        'discount_id' => $insert_discount_id,
                        'user_id' => $doctor_list ]);
                }
            }


            session()->flash('success', trans('admin::app.discounts.created-success'));
        }
        else
            session()->flash('success', trans('admin::app.discounts.created-fail'));
        
        return redirect()->route('admin.credit-discount.index');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $discount = DB::table('discounts')->where('id',$id)->first();
        $data['credit_users'] = $this->getCreditUsers();
        $data['promo_codes'] = DB::table('promo_codes')->where('discount_id',$id)->first();
        $data['user_discount']  = DB::table('discount_users')
                                        ->where('discount_id', $id)
                                        ->pluck('user_id')
                                        ->toArray();
        $all_discount = [];
        $category_discounts = DB::table('discount_category')
                                            ->where('discount_id',$discount->id)
                                            ->select('discount')->get()->unique();
        foreach($category_discounts as $category_discount){
            $discount_category = DB::table('discount_category')
                                    ->where('discount_id', $discount->id)
                                    ->where('discount',$category_discount->discount)
                                    ->pluck('category_id')->toArray();

            if(count($discount_category) > 0) {
                $all_discount[] = [
                    'categorys'=> array_values($discount_category),
                    'percentage'=>$category_discount->discount
                ];
            }
        }

        $data["discount"] = $discount;
        $data["items"] = $this->getDiscountTree();
        $data["all_discount"] = json_encode($all_discount);
        return view('admin::credit-discounts.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate(request(), [
            'minimum_order_amount' => 'required',
            'discount_title' => 'required',
            'discount_description' => 'required',
            'coupon_code' => 'required',
            'discount.*.categories' => 'required',
            'discount.*.discount_per' => 'required',
            // 'date_start' => 'required',
            // 'date_end' => 'required',
        ]);

        //dd($request->all()); 
        // if($request->get('date_start') != "" && $request->get('date_end') != ""){
        //     if(strtotime($request->get('date_start')) > strtotime($request->get('date_end'))){
        //         session()->flash('warning', 'Date Invalid!');
        //         return redirect()->back();
        //     }
        // }
        
        $date_start=$date_end=null;
        if(!empty($request->get('date_start')))
            $date_start = date('Y-m-d H:i:s', strtotime($request->get('date_start')));
        if(!empty($request->get('date_end')))
            $date_end = date('Y-m-d H:i:s', strtotime($request->get('date_end')));

        $delete_discount_users = DB::table('discount_users')
                                            ->where('discount_id',$id)
                                            ->delete();


        $discount_categories = array_values(request()->get('discount')?? []);

        $update_discount = DB::table('discounts')->where('id',$id)->update([
                                        'for_all_users' => ($request->get('for_all_users')!=0)?1:0,
                                        'minimum_order_amount'=>$request->get('minimum_order_amount'),
                                        'date_start' => $date_start,
                                        'date_end' => $date_end,
                                        'coupon_code' => $request->get('coupon_code'),
                                        'dealer_commission' => $request->get('dealer_commission'),
                                        'discount_title' => $request->get('discount_title'),
                                        'discount_description' => $request->get('discount_description'),
                                        'discount_usage_limit' => $request->get('discount_usage_limit')
                                    ]);

        if($request->get('for_all_users') == 0)
        {
            $doctor_lists = array_unique($request->get('users_list'));

            foreach($doctor_lists as $doctor_list){
                $insert_discount_users = DB::table('discount_users')->insert([
                    'discount_id' => $id,
                    'user_id' => $doctor_list ]);
            }
        }

    
        $delete_discount_category = DB::table('discount_category')->where('discount_id',$id)->delete();
        foreach($discount_categories as $d_key => $discount_category)
        {
            $dis_cat = explode("," , $discount_category['categories']);
                $dis_cat = array_map(function($val){
                    return explode('-', $val)[1];
                }, $dis_cat);
                
            foreach($dis_cat as $d_key => $category){
                $insert_discount_doctors = DB::table('discount_category')->insert([
                                                            'discount_id' => $id,
                                                            'category_id' => $category,
                                                            'discount' => $discount_category['discount_per']
                                                        ]);
                }
            }
            $whoom = ($request->get('for_all_users')==2 || $request->get('for_all_users')==3)?$request->get('for_all_users'):0;

            $insert_promo_code = DB::table('promo_codes')->where('discount_id',$id)->update([
                    'apply_membership' => 1,
                    'earn_points' => $request->get('earn_points'),
                    'for_whom' =>  $whoom]);

            session()->flash('success', trans('admin::app.discounts.update-success'));

        return redirect()->route('admin.credit-discount.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = DB::table('discounts')->where('id',$id)->update(['is_deleted'=>1]);
        //DB::table('promo_codes')->where('discount_id',$id)->delete();
        DB::table('discount_category')->where('discount_id', $id)->delete();
        DB::table('discount_users')->where('discount_id', $id)->delete();

        if ($delete)
            session()->flash('success', trans('admin::app.discounts.delete-success'));
        else
            session()->flash('success', trans('admin::app.disocunts.delete-fail'));

        return redirect()->route('admin.credit-discount.index');
    }

    public function changeStatus(Request $request){
        DB::table('discounts')->where('id', $request->id)->update(['active'=>($request->state==1)?0:1]);
        return redirect()->route('admin.credit-discount.index');
    }

    public function addCreditDue(Request $request){
        //dd($request->all());
        $user = DB::table('users')->where('id', $request->user_id)->first();
        
        DB::table('credit_payments_due')->insert([
            'amount' => $request->credit_due_price,
            'user_id' => $request->user_id,
            'due_date' => $request->due_date,
            'order_number'=> $request->order_number,
            'comments'=> $request->credit_due_comment
        ]);
        
        $user_db = array();        
        $order_amount = $request->credit_due_price;
        $user_balance = $user->credit_balance;
        $user_credit  = $user->credit_limit - ($user->credit_used - $user->credit_paid);
        
        if($user_balance > $order_amount){
            $user_db = ['credit_balance' => ($user_balance-$order_amount)];
        }else if($user_balance <= 0){
            $user_db = ['credit_used' => $user->credit_used+$order_amount];
        }else{
            $user_db = ['credit_used' => $user->credit_used+($order_amount-$user_balance), 'credit_balance' => 0];
        }
        
        DB::table('users')->where('id', $request->user_id)->update($user_db);
        $user = DB::table('users')->where('id', $request->user_id)->first();

        $data['offline_balance'] = $request->all();
        $data['user'] = $user;

        Mail::send('shop::emails.credit_payments.offline_balance', $data, function($message) use ($user){
            $message->from('info@genxtimplants.com', 'GenXT');
            $message->to($user->email)->subject('Offline Payment due debited from your Credit/Advance - GenXT');
        });


        session()->flash('success', 'Due Added');
        return redirect()->back();
    }


    ///Private Functions

    private function getDiscountTree(){
        $items = array();

        foreach($this->category->getCategoryTree() as $cat){ //n-array preorder TreeTraversal algorithm
            if($cat!=null){
                $stack = array();
                array_push($stack, $cat);

                while(count($stack)>0){
                    $temp = array_pop($stack);
                    $childrens = $temp->children;
                    if(count($childrens)<=0){
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'end'=> true,
                                                 'name' => $temp->name));
                    }else{
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'end'=> false,
                                                 'name' => $temp->name));
                        for($i=count($childrens)-1; $i>=0; $i--){
                            array_push($stack, $childrens[$i]);
                        }
                    }
                }
            }
        }
        return json_encode($items);
    }


    private function getCreditUsers(){

        $credit_users = DB::table('users as u')
                                ->leftjoin('credit_payments as cp' , 'u.id', 'cp.user_id')
                                ->whereNotNull('cp.user_id')
                                ->where('cp.status', 1)
                                ->addSelect('u.id as user_id', 
                                        DB::raw('CONCAT(u.my_title, " ", u.first_name, " ", u.last_name) as user_name'), 
                                        'u.email as user_email', 
                                        'u.role_id as user_role', 
                                        'u.credit_balance as user_credit',
                                        DB::raw('SUM(cp.amount) as total_credit'),
                                        DB::raw('MAX(cp.created_at) as last_credit'),
                                        DB::raw('(u.credit_paid - u.credit_used) as credit_due'))
                                ->groupBy('user_id')
                                ->get();

        return $credit_users;
    }


}

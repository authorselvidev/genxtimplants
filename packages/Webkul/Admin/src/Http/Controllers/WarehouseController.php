<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Customer\Repositories\CustomerRepository as Customer;
use Webkul\Customer\Models\States as State;
use Webkul\Customer\Models\Cities as Cities;
use Webkul\Customer\Repositories\CustomerGroupRepository as CustomerGroup;
use Webkul\Core\Repositories\ChannelRepository as Channel;
use Webkul\Customer\Mail\AccountCreationMail;
use Mail;
use DB;

/**
 * Customer controlller
 *
 * @author    Rahul Shukla <rahulshukla.symfony517@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class WarehouseController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var array
     */
    protected $customer;

     /**
     * CustomerGroupRepository object
     *
     * @var array
     */
    protected $customerGroup;

     /**
     * ChannelRepository object
     *
     * @var array
     */
    protected $channel;

     /**
     * Create a new controller instance.
     *
     * @param Webkul\Customer\Repositories\CustomerRepository as customer;
     * @param Webkul\Customer\Repositories\CustomerGroupRepository as customerGroup;
     * @param Webkul\Core\Repositories\ChannelRepository as Channel;
     * @return void
     */
    public function __construct(Customer $customer, CustomerGroup $customerGroup, Channel $channel)
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->customer = $customer;

        $this->customerGroup = $customerGroup;

        $this->channel = $channel;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view($this->_config['view']);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dealers = DB::table('users')->where('role_id',2)->where('is_deleted',0)->get();
        return view($this->_config['view'],compact('dealers'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all());
        $this->validate(request(), [
            'code' => 'required',
            'name' => 'required',
            'dealer_id' => 'required',
            'address' => 'required'
        ]);

        $insert_warehouse_id = DB::table('warehouses')->insertGetId([
                                        'code' => $request->get('code'),
                                        'name' => $request->get('name'),
                                        'dealer_id' => $request->get('dealer_id'),
                                        'address' => $request->get('address'),
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s'),
                                    ]);
        if($insert_warehouse_id)
            session()->flash('success', 'Warehouse created successfully!');
        else
            session()->flash('success', 'Warehouse not created properly!');

        return redirect()->route($this->_config['redirect']);
    }


    public function show($id)
    {
        $customer = $this->customer->findOneWhere(['id'=>$id]);
        $totalOrders = DB::table('orders')->leftJoin('users', 'users.id', '=', 'orders.customer_id')
                                            ->where('users.dealer_id',$id)
                                            ->count();
        $totalSales = DB::table('orders')->leftJoin('users', 'users.id', '=', 'orders.customer_id')
                                            ->where('users.dealer_id',$id)
                                            ->sum('grand_total');                                  
        $totalDoctors = DB::table('users')->where('dealer_id',$id)->count();
        $doctorsUnderDealer = DB::table('users')->where('dealer_id',$id)->where('is_deleted',0)->get();
        $latestOrders = DB::table('users')->leftJoin('orders', 'users.id', '=', 'orders.customer_id')
                                            ->where('users.dealer_id',$id)
                                            ->orderBy('orders.created_at','desc')
                                            ->get();
                                            

        $customerGroup = $this->customerGroup->all();
        $channelName = $this->channel->all();

        return view($this->_config['view'],compact('customer', 'customerGroup', 'channelName','totalOrders','totalDoctors','totalSales','latestOrders','doctorsUnderDealer'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warehouse = DB::table('warehouses')->where('id',$id)->first();
        $dealers = DB::table('users')->where('role_id',2)->where('is_deleted',0)->get();
        return view($this->_config['view'],compact('warehouse', 'dealers'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //dd($request->all());
        $this->validate(request(), [
            'code' => 'required',
            'name' => 'required',
            'dealer_id' => 'required',
            'address' => 'required'
        ]);

        $update_warehouse = DB::table('warehouses')->where('id',$id)->update([
                                        'code' => $request->get('code'),
                                        'name' => $request->get('name'),
                                        'dealer_id' => $request->get('dealer_id'),
                                        'address' => $request->get('address'),
                                        'updated_at' => date('Y-m-d H:i:s'),
                                    ]);
        if($update_warehouse)
            session()->flash('success', 'Warehouse updated successfully!');
        else
            session()->flash('success', 'Warehouse not updated properly!');

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = DB::table('warehouses')->where('id',$id)->update(['is_deleted' => 1]);
        if($delete)
            session()->flash('success', 'Warehouse deleted successfully!');

        return redirect()->back();
    }

    public function massDestroy()
    {
        $dealerIds = explode(',', request()->input('indexes'));

        foreach ($dealerIds as $dealerId) {
            $delete =  DB::table('users')->where('id',$dealerId)->update(['is_deleted' => 1]);
        }

        session()->flash('success', trans('admin::app.response.delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

}

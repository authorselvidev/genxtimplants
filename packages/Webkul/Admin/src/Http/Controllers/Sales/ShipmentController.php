<?php

namespace Webkul\Admin\Http\Controllers\Sales;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Sales\Repositories\ShipmentRepository as Shipment;
use Webkul\Sales\Repositories\OrderRepository as Order;
use Webkul\Sales\Repositories\OrderItemRepository as OrderItem;

/**
 * Sales Shipment controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * ShipmentRepository object
     *
     * @var mixed
     */
    protected $shipment;

    /**
     * OrderRepository object
     *
     * @var mixed
     */
    protected $order;

    /**
     * OrderItemRepository object
     *
     * @var mixed
     */
    protected $orderItem;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Sales\Repositories\ShipmentRepository  $shipment
     * @param  Webkul\Sales\Repositories\OrderRepository     $order
     * @param  Webkul\Sales\Repositories\OrderitemRepository $orderItem
     * @return void
     */
    public function __construct(
        Shipment $shipment,
        Order $order,
        OrderItem $orderItem
    )
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->order = $order;

        $this->orderItem = $orderItem;

        $this->shipment = $shipment;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $orderId
     * @return \Illuminate\Http\Response
     */
    public function create($orderId)
    {
        $order = $this->order->find($orderId);
        

        if (! $order->channel || !$order->canShip()) {
            session()->flash('error', 'Shipment can not be created for this order.');

            return redirect()->back();
        }

        return view($this->_config['view'], compact('order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $orderId
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $orderId)
    {
        //dd($request->shipment['track_number']);
        $order = $this->order->find($orderId);

        if (! $order->canShip()) {
            session()->flash('error', 'Order shipment creation is not allowed.');

            return redirect()->back();
        }

        $this->validate(request(), [
            'shipment.source' => 'required',
            'shipment.items.*.*' => 'required|numeric|min:0',
        ]);

        $data = request()->all();

        if (! $this->isInventoryValidate($data)) {
            session()->flash('error', 'Requested quantity is invalid or not available.');

            return redirect()->back();
        }

        $this->shipment->create(array_merge($data, ['order_id' => $orderId]));
        
        session()->flash('success', 'Shipment created successfully.');

        return redirect()->route($this->_config['redirect'], $orderId);
    }

    /**
     * Checks if requested quantity available or not
     *
     * @param array $data
     * @return boolean
     */
    public function isInventoryValidate(&$data)
    {
        $valid = false;

        foreach ($data['shipment']['items'] as $itemId => $inventorySource) {
            if ($qty = $inventorySource[$data['shipment']['source']]) {
                $orderItem = $this->orderItem->find($itemId);

                $product = ($orderItem->type == 'configurable')
                        ? $orderItem->child->product
                        : $orderItem->product;

                $availableQty = $product->inventories()
                        ->where('inventory_source_id', $data['shipment']['source'])
                        ->sum('qty');

                //if ($orderItem->qty_to_ship < $qty || $availableQty < $qty) {
                if ($availableQty < $qty) {
                    return false;
                }

                $valid = true;
            } else {
                unset($data['shipment']['items'][$itemId]);
            }
        }

        return $valid;
    }

    /**
     * Show the view for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function view($id)
    {
        $shipment = $this->shipment->find($id);
        $order = \DB::table('orders')->where('id',$shipment['order_id'])->first();
        $shipment['billingAdress'] = $order->billing_address;
        $shipment['shippingAdress'] = $order->shipping_address;
        $data['shipment'] = $shipment;

        $payment = \DB::table('order_payment')->where('order_id', $order->id)->first();
        if($payment->method == 'prepayment'){
            $data['prepayment'] = \DB::table('prepayments')->where('order_id', $order->id)->first();
        }
        return view($this->_config['view'], $data);
    }


    public function UpdateTrackingInfo($order_id, Request $request)
    {
        $data = $request->all();
        $order = \DB::table('orders')->where('id',$order_id)->first();
        $shipment = \DB::table('shipments')->where('order_id',$order_id)->update([
                                'shipping_carrier_id' => $data['shipment']['shipping_carrier_id'],
                                'track_number' => $data['shipment']['track_number']
                            ]);
        $customer_info = \DB::table('users')->where('id',$order->customer_id)->first();
        if($data['shipment']['shipping_carrier_id'] != ""&& $data['shipment']['track_number']!=""){
            $get_carrier = \DB::table('shipping_carriers')->where('id',$data['shipment']['shipping_carrier_id'])->first();
            $message = 'Hello Dr '.$customer_info->first_name.', your order was shipped yesterday with '.$get_carrier->name.', Tracking no. '.$data['shipment']['track_number'].'.';
            $sms_info = array();
            $sms_info['sms_numbers'] = $customer_info->phone_number;
            $sms_info['sms_message'] = $message;     
            $sent_sms=view('shop::send-sms',$sms_info)->render();
        }

        

        if($shipment)
            session()->flash('success', 'Shipment Tracking updated successfully!.');
       else
            session()->flash('success', 'Shipment Tracking not updated!.');

        return redirect()->back();



    }
}

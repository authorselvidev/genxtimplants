<?php

namespace Webkul\Admin\Http\Controllers\Sales;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Sales\Repositories\OrderRepository as Order;
use Webkul\Sales\Repositories\OrderItemRepository as OrderItem;
use Webkul\Admin\Mail\PrepaymentOrderApprove;
use DB;
use PDF;
use Mail;
use Storage;
use Redirect;
use Session;

/**
 * Sales Order controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class PrePaymentOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $order;
    protected $orderItem;
    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Sales\Repositories\OrderRepository  $order
     * @return void
     */
    public function __construct(Order $order, OrderItem $orderItem)
    {
        $this->middleware('admin');
        $this->_config = request('_config');
        $this->order = $order;
        $this->orderItem = $orderItem;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the view for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $order = $this->order->find($id);
        $paid_history = \DB::table('manual_payment_entry')->where('order_id',$id)->where('is_deleted',0)->latest()->get();
        return view($this->_config['view'], compact('order','paid_history'));
    }

    /**
     * Cancel action for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $result = $this->order->cancel($id);
        
        if ($result) {
            session()->flash('success', trans('Order canceled successfully.'));
        } else {
            session()->flash('error', trans('Order can not be canceled.'));
        }

        return redirect()->back();
    }

    public function approveOrder($id,Request $request)
    {
        // dd($request->all());
        $order = $this->order->find($id);
        $user = \DB::table('users')->where('id',$order->customer_id)->first();
        $approved_by_credit = (isset($request->approved_by_credit)) ? 1 : 0;
        $approveOrder=false;
        if($approved_by_credit == 0)
        {
            $approveOrder=true;
            $order->status = 'confirmed';
            $order->save();
        }
        else{
            $dealer_id = \DB::table('users')->where('id',$order->customer_id)->value('dealer_id');
            $dealer = \DB::table('users')->where('id',$dealer_id)->first();


            $user_db = array();
            $order_amount = $order->grand_total;
            $dealer_balance = $dealer->credit_balance;
            $dealer_credit  = $dealer->credit_limit - ($dealer->credit_used - $dealer->credit_paid);

            if($dealer_credit >= $request->amount){
                if($dealer_balance > $order_amount){
                    $user_db = ['credit_balance' => ($dealer_balance-$order_amount)];
                }else if($dealer_balance <= 0){
                    $user_db = ['credit_used' => $dealer->credit_used+$order_amount];
                }else{
                    $user_db = ['credit_used' => $dealer->credit_used+($order_amount-$dealer_balance), 'credit_balance' => 0];
                }
                DB::table('users')->where('id', $dealer->id)->update($user_db);

                $order->status = 'confirmed';
                $order->save();
                $approveOrder=true;
            }
            else{
                session()->flash('error', trans('Dealer does not have enough credit limit!'));
                return redirect()->route('admin.sales.prepayment_order.view',$id);
            }
        }
        if($approveOrder==true)
        {
            $prepayments = \DB::table('prepayments')->insert([
                                        'order_id' => $id,
                                        'user_id' => $order->customer_id,
                                        'amount' => $request->amount,
                                        'approved_by_credit' => $approved_by_credit,
                                        'payment_reference' => $request->payment_reference_id,
                                        'comments' => $request->comments,
                                        'created_at' => date('Y-m-d H:i:s'),
                                    ]);            
            
            Mail::to($user->email)->send(new PrepaymentOrderApprove($order, $user));
        }
        session()->flash('success', 'Order has been approved!');
        return redirect()->route($this->_config['redirect']);
    }    

    public function getOrderPDF($id){
        $url='orders/pdf/'.'Genxt_bill_no_'.$id.'.pdf';

        if(!Storage::exists($url)){
            Session::flash('error','File not found on the server');
            return Redirect::back();
        }

        return Storage::download($url);
    }
}

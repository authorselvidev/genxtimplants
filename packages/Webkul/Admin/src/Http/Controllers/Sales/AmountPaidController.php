<?php

namespace Webkul\Admin\Http\Controllers\Sales;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Sales\Repositories\ShipmentRepository as Shipment;
use Webkul\Sales\Repositories\OrderRepository as Order;
use Webkul\Sales\Repositories\OrderItemRepository as OrderItem;
use Validator;

/**
 * Sales Shipment controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class AmountPaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * ShipmentRepository object
     *
     * @var mixed
     */
    protected $shipment;

    /**
     * OrderRepository object
     *
     * @var mixed
     */
    protected $order;

    /**
     * OrderItemRepository object
     *
     * @var mixed
     */
    protected $orderItem;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Sales\Repositories\ShipmentRepository  $shipment
     * @param  Webkul\Sales\Repositories\OrderRepository     $order
     * @param  Webkul\Sales\Repositories\OrderitemRepository $orderItem
     * @return void
     */
    public function __construct(
        Shipment $shipment,
        Order $order,
        OrderItem $orderItem
    )
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->order = $order;

        $this->orderItem = $orderItem;

        $this->shipment = $shipment;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for creating a new resource.
     *
     * @param int $orderId
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param int $orderId
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $orderId)
    {

        $validator = Validator::make($request->all(), [
            'paid_amount' => 'required'
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Please enter payment value');
           
        }
        
        else{
            $paid_amount = \DB::table('manual_payment_entry')->insert([
                                        'order_id' => $request->get('order_id'),
                                        'dealer_id' => $request->get('dealer_id'),
                                        'paid_amount' => $request->get('paid_amount'),
                                        'created_at' => date('Y-m-d H:i:s')
                                    ]);
            session()->flash('success', 'Payment has been added');
        }
        return redirect()->route('admin.sales.orders.view', $request->get('order_id'));
       
    }
    public function destroy($id='')
    {
        $get_order = \DB::table('manual_payment_entry')->where('id',$id)->first();
        $destroy =\DB::table('manual_payment_entry')->where('id',$id)->update(['is_deleted'=>1]);
        session()->flash('success', 'Payment has been deleted!');
        return redirect()->route('admin.sales.orders.view', $get_order->order_id);
    }

}

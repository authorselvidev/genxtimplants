<?php

namespace Webkul\Admin\Http\Controllers\Sales;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Sales\Repositories\OrderRepository as Order;
use Webkul\Sales\Repositories\OrderItemRepository as OrderItem;
use Webkul\Admin\Mail\OrderCancelNotification;
use Webkul\Customer\Models\CustomerGroup;
use Carbon\Carbon;
use DB;
use PDF;
use Mail;
use Storage;
use Redirect;
use Session;

/**
 * Sales Order controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $order;
    protected $orderItem;
    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Sales\Repositories\OrderRepository  $order
     * @return void
     */
    public function __construct(Order $order, OrderItem $orderItem)
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->order = $order;
        $this->orderItem = $orderItem;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the view for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $data['order'] = $this->order->find($id);
        $data['paid_history'] = \DB::table('manual_payment_entry')->where('order_id',$id)->where('is_deleted',0)->latest()->get();
        
        $data['shipment'] = \DB::table('shipments')->where('order_id', $id)->first();
        
        if($data['shipment'])
            $data['inventory_source'] = \DB::table('inventory_sources')->where('id', $data['shipment']->inventory_source_id)->first();

        if($data['order']->payment[0]->method == 'prepayment')
            $data['prepayment'] = DB::table('prepayments')->where('order_id', $id)->first();


        return view($this->_config['view'], $data);
    }

    /**
     * Cancel action for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, $id)
    {
        //dd($request->all(), $id);
        $result = $this->order->cancel($id);
        DB::table('orders')->where('id', $id)->update([
            'remarks' => $request->cancel_reason ?? ''
        ]);
        
        $order = $this->order->find($id);

        $user = \DB::table('users')->where('id',$order->customer_id)->first();
        Mail::to($user->email)->send(new OrderCancelNotification($order, $user));

        if ($result) {
            session()->flash('success', trans('Order canceled successfully.'));
        } else {
            session()->flash('error', trans('Order can not be canceled.'));
        }

        return redirect()->back();
    }

    public function ChangeOrderStatus(Request $request)
    {
       //dd($request->all());
       if($request->get('order_status') != null)
       {
            $order_id = $request->get('order_id');
            $order = $this->order->find($order_id);
            if($order->status == 'shipped')
            {
                $get_shipped_items = \DB::table('order_items')->where('order_id',$order_id)->get();
                foreach($get_shipped_items as $key => $get_shipped_item)
                {
                    $changed_status = \DB::table('order_items')
                                            ->where('order_id',$order_id)
                                            ->where('qty_shipped',$get_shipped_item->qty_shipped)
                                            ->update(['qty_delivered' => $get_shipped_item->qty_shipped]);
                }
                //$this->order->updateOrderStatus($order);
               
            }
            $order->status = $request->get('order_status');
            $order->save();
            
            if($request->get('order_status') == 'completed'){
                $total_orders_amount = \DB::table('orders')->where('customer_id',$order->customer_id)->where('status','completed')->sum('grand_total');
                $user_current_group = \DB::table('users')->where('id',$order->customer_id)->addSelect('customer_group_id')->first()->customer_group_id;
                $user_group_amount = \DB::table('user_groups')->where('id', $user_current_group)->first()->order_amount_from;

                if(intval($total_orders_amount) >= intval($user_group_amount)) {
                    $customer_group = \DB::table('user_groups')->where('order_amount_from', '<=',$total_orders_amount)->where('order_amount_to','>=', $total_orders_amount)->first();
                    if(isset($customer_group)){
                        $user_group = \DB::table('users')->where('id', $order->customer_id)->update(['customer_group_id' => $customer_group->id]);
                        
                        \DB::table('users')->where('id', $order->customer_id)
                            ->update([  'group_changed_by' => 0,
                                        'group_changed_at' => date('Y-m-d H-i-s')]);
                    }
                }
                $points_credit = \DB::table('reward_points')->where('order_id',$order_id)->update(['points_credited' => 1, 'updated_date'=> Carbon::now()]);
            
            }
            return redirect()->back();
        }
    }

    public function PayDealerCommission(Request $request)
    {
        //dd($request->all());
        $rules = [
            'order_id' => 'required',
            'dealer_id' => 'required',
            'commission_paid_amount' => 'required'
        ];
        
        $order = \DB::table('orders')->where('id',$request->get('order_id'))->first();
         $commission_paid_amount = \DB::table('dealer_commission')->where('order_id',$request->get('order_id'))->where('dealer_id',$request->get('dealer_id'))->sum('commission_paid_amount');
         $total_comm_amount = $commission_paid_amount + $request->get('commission_paid_amount');
        
        if($order->total_comm_amount < $total_comm_amount){
             session()->flash('error', 'You have entered wrong amount!');
             return redirect()->route('admin.sales.orders.view', $request->get('order_id'));
        }
        $request->validate($rules);
            $commission_paid_id = \DB::table('dealer_commission')->insertGetId([
                                        'order_id' => $request->get('order_id'),
                                        'dealer_id' => $request->get('dealer_id'),
                                        'doctor_id' => $order->customer_id,
                                        'commission_paid_amount' => $request->get('commission_paid_amount'),
                                        'created_at' => date('Y-m-d H:i:s')
                                    ]);
            
            $dealer_commission = \DB::table('dealer_commission')->where('id', $commission_paid_id)->first();
            if($request->get('dealer_comm_amt') == $dealer_commission->commission_paid_amount){
                $update_order = \DB::table('orders')->where('id',$request->get('order_id'))->update(['commission_paid'=>1]);
            }

            session()->flash('success', 'Commission has been added');
        
        return redirect()->route('admin.sales.orders.view', $request->get('order_id'));
    }

    public function UpdateDealerCommission(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'order_id' => 'required',
            'dealer_id' => 'required',
            'commission_paid_amount' => 'required'
        ]);
        
            $paid_amount = \DB::table('dealer_commission')->where('order_id',$request->get('order_id'))->update([
                    'commission_paid_amount' => $request->get('commission_paid_amount')
                                    ]);

            session()->flash('success', 'Commission has been updated');
        
        return redirect()->route('admin.sales.orders.view', $request->get('order_id'));
    }

    public function orderInvoice(Request $request){


        $order = $this->order->find($request->get('order_id'));

        switch($request->get('type')){
            case "Send Invoice":
                    if(!$request->file('invoice_file')){
                        session()->flash('error', 'Invoice file not choosen !');
                        return redirect()->back();
                    }

                    $order_invoice_file = $request->file('invoice_file');
                    $rand = date('Y-m-d-His').rand(1, 9999);
                    $order_invoice_file_name = '';
                    
                    if(isset($order_invoice_file) && $order_invoice_file->getClientOriginalName()) {
                        $order_invoice_file_name = pathinfo($order_invoice_file->getClientOriginalName(), PATHINFO_FILENAME).'-'.$rand.'.'.$order_invoice_file->getClientOriginalExtension();
                        $order_invoice_file->storeAs('order_invoice/invoices', $order_invoice_file_name);
                    }

                    DB::table('order_invoice')->insert([
                        'order_id' => $request->get('order_id'),
                        'file_name' => $order_invoice_file_name,
                    ]);


                    $this->sendInvoiceMail([
                        'order' => $order,
                        'order_invoice_file_name'=> $order_invoice_file_name,
                        'order_invoice_file'=> $order_invoice_file,
                        'state'=>'invoice'
                    ]);
                    \Session::flash('success','Invoice Successfully send');
                break;

            case 'Complete Order':

                    if($order->status != "completed"){
                        $total_orders_amount = \DB::table('orders')->where('customer_id', $order->customer_id)->where('status', 'completed')->sum('grand_total');
                        $user_current_group = \DB::table('users')->where('id',$order->customer_id)->addSelect('customer_group_id')->first()->customer_group_id;
                        $user_group_amount = \DB::table('user_groups')->where('id', $user_current_group)->first()->order_amount_from;

                        if(intval($total_orders_amount) >= intval($user_group_amount)){
                            $customer_group = \DB::table('user_groups')->where('order_amount_from', '<=',$total_orders_amount)->where('order_amount_to','>=', $total_orders_amount)->first();
                            if(isset($customer_group)){
                                $user_group = \DB::table('users')->where('id', $order->customer_id)->update(['customer_group_id' => $customer_group->id]);
                                \DB::table('users')->where('id', $order->customer_id)
                                                   ->update([ 'group_changed_by' => 0,
                                                        'group_changed_at' => date('Y-m-d H-i-s') ]);
                            }
                        }

                        $points_credit = \DB::table('reward_points')->where('order_id',$request->get('order_id'))->update(['points_credited' => 1, 'updated_date'=> Carbon::now()]);
                        $order->status = 'completed';
                        $order->save();
                    }

                    \Session::flash('success','Order Completed Successfully');
                    $this->sendInvoiceMail([
                        'order' => $order,
                        'state'=>'order'
                    ]);                    
                break;
            case 'Delete':
                    $invoice_id = $request->get('invoice_id');
                    DB::table('order_invoice')->where('id', $invoice_id)->delete();
                    \Session::flash('success','Successfully deleted');
                break;
            
        }
        return redirect()->route('admin.sales.orders.view', $request->get('order_id'));
    }

    public function orderInvoiceDownload($id)
    {
        $invoice_db = DB::table('order_invoice')->where('id', $id)->first();
        $url='order_invoice/invoices/'.$invoice_db->file_name;

        if(\Storage::exists($url)){
            return \Storage::download($url);
        }else{
            \Session::flash('error','File not found on the server');
            return Redirect::back();
        }
    }


    public function sendInvoiceMail($postData=""){
        $order = $postData['order'];
        
        $orders = \DB::table('orders')->where('customer_id', $order->customer_id)->where('status', 'completed');
        $customer = \DB::table('users')->where('id',$order->customer_id)->first();
        $data['customer'] = $customer;
        $data['order'] = $order;
        $data['calculated_from_date'] = date('d/m/Y', strtotime($orders->orderBy('created_at', 'ASC')->first()->created_at ?? date('Y-m-d')) );
        $total_orders_amount_year = \DB::table('orders')->where('customer_id', $order->customer_id)->whereYear('created_at','=',date('Y'))->where('status', 'completed')->sum('grand_total');
        $data['total_orders_amount_year'] = number_format($total_orders_amount_year, 2);
                    
        $total_loyalty_points = DB::table('reward_points as rp')
                                ->where('rp.points_credited',1)
                                ->where('rp.points_expired',0)
                                ->where('rp.user_id',$order->customer_id)
                                ->sum('reward_points');

        $used_loyalty_points = DB::table('orders')
                                ->where('customer_id',$order->customer_id)
                                ->sum('reward_points_used');

        $available_points=0;
        if($total_loyalty_points > 0)                       
            $available_points = abs($total_loyalty_points - $used_loyalty_points);
        $data['reward_points'] = $available_points;
                    
        if($customer->role_id == 3){ //doctor
            $member_ship_db = DB::table('discounts')
                                ->where('discount_type',2)
                                ->where('product_id',null)
                                ->where('is_deleted',0)
                                ->addSelect('id', 'discount_title', 'discount_description', 'customer_group_id', 'minimum_order_amount', 'coupon_code')->get();

            $member_ship = array();

            foreach ($member_ship_db as $key => $value) {
                $arr = explode('-', CustomerGroup::CustomerGroupAmountRange($value->customer_group_id));

                $member_ship[$key] =  array("id" => $value->id,
                                            "discount_title"=> $value->discount_title,
                                            "discount_description" => $value->discount_description,
                                            "customer_group_id" => $value->customer_group_id,
                                            "minimum_order_amount" => $value->minimum_order_amount,
                                            "coupon_code" => $value->coupon_code,
                                            "minval" =>  intval(preg_replace('/[^0-9]/', '', $arr[0])),
                                            "maxval" => intval(preg_replace('/[^0-9]/', '', $arr[1])));
            }

            usort($member_ship, function($a,$b){ return $a['minval']-$b['minval'];});
            $customer_group = $data['customer']->customer_group_id;

            $current_group_offers = array_values(array_filter($member_ship, function($key) use ($customer_group){
                return $key['customer_group_id'] == $customer_group;
            }))[0] ?? null;

            $data['current_group_offers'] = $current_group_offers;

            $current_group_maxval = $current_group_offers['maxval'];                    

            $next_group_offers = array_values(array_filter($member_ship, function($key) use ($current_group_maxval){
                return $key['minval'] > $current_group_maxval;
            }))[0] ?? null;

            $data['next_group_offers'] = $next_group_offers;

            if($postData['state']=='invoice'){
                $order_invoice_file = $postData['order_invoice_file'];
                $data['order_completed'] = false;
                Mail::send('shop::emails.sales.bill_invoice_doctor', $data, function ($message) use ($customer, $order_invoice_file, $order, $postData){
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($customer->email)->subject('Order Invoice - Order #'.$order->id);
                    $message->attach($order_invoice_file->getRealPath(), array('as' => $order_invoice_file->getClientOriginalName(),'mime' => $order_invoice_file->getMimeType()));
                });
            }else{
                $data['order_completed'] = true;
                Mail::send('shop::emails.sales.bill_invoice_doctor', $data, function ($message) use ($customer, $order, $postData){
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($customer->email)->subject('Order Completed - Order #'.$order->id);
                });

            }
                        
        }else{

            $data['credit_payment'] = DB::table('credit_payments')->where('user_id', $customer->id)
                                ->orderBy('created_at','desc')
                                ->paginate(10);

            $data['credit_pending'] = DB::table('credit_payments')
                                ->where('user_id', $customer->id)
                                ->where('status', 0)
                                ->sum('amount');

            $credit_pending_amount = $customer->credit_used - $customer->credit_paid;
            $available_credit_limit = $customer->credit_limit - $credit_pending_amount;
            $data['credit_pending_amount'] = $credit_pending_amount;
            $data['available_credit_limit'] = $available_credit_limit;

            if($postData['state']=='invoice'){
                $data['order_completed'] = false;
                $order_invoice_file = $postData['order_invoice_file'];
                Mail::send('shop::emails.sales.bill_invoice_dealer', $data, function ($message) use ($customer, $order_invoice_file, $order, $postData){
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($customer->email)->subject('Order Invoice - Order #'.$order->id);
                    $message->attach($order_invoice_file->getRealPath(), array('as' => $order_invoice_file->getClientOriginalName(),'mime' => $order_invoice_file->getMimeType()));
                });
            }else{
                $data['order_completed'] = true;
                Mail::send('shop::emails.sales.bill_invoice_dealer', $data, function ($message) use ($customer, $order, $postData){
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($customer->email)->subject('Order Completed - Order #'.$order->id);
                });
            }
        }
    }
}

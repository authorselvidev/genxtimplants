<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PointsConvertorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$points_convertor = DB::table('points_and_commission_history')->get();
        return view('admin::settings.points_convertor.index')->with(['points_convertor' => $points_convertor]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $get_last_record = DB::table('points_and_commission_history')->first();
        $update_status = DB::table('points_and_commission_history')
                                ->where('closed_date',null)
                                ->update(['status' => 0,'closed_date' => date('Y-m-d H:i:s')]);

        $insert_points= DB::table('points_and_commission_history')->insert([
                                            'type' => 1,
                                            'value' => $request->get('points'),
                                            'created_date' => date('Y-m-d H:i:s')
                                        ]);

        if ($insert_points)
            session()->flash('success', trans('admin::app.settings.points_convertor.update-success'));
        else
            session()->flash('success', trans('admin::app.settings.points_convertor.update-fail'));
        
        

        return redirect()->route('admin.points_convertor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
 
    }


    
}

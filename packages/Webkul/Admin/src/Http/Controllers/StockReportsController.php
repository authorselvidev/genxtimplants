<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Admin\Exports\DataGridExport;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Product\Repositories\ProductRepository as Product;
use Excel;
use DB;


class StockReportsController extends Controller
{


    public function __construct(Category $category, Product $product){
        $this->middleware('admin');
        $this->_config = request('_config');
        $this->category = $category;
        $this->product = $product;
    }


    public function index(){
        $items = array();

        foreach($this->category->getCategoryTree() as $cat){ //n-array preorder TreeTraversal algorithm
                if($cat!=null){
                    $stack = array();
                    array_push($stack, $cat);

                    while(count($stack)>0){
                        $temp = array_pop($stack);
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'name' => $temp->name));
                        $childrens = $temp->children;
                        if(count($childrens)<=0){
                            foreach($this->product->findAllByCategory($temp->id) as $product){
                                array_push($items, array('id' => intval($product->id)*100000,
                                                         'sku' => $temp->id.'-'.$product->sku,
                                                         'pid' => $temp->id,
                                                         'name' => $product->name));
                            }
                        }else{
                            for($i=count($childrens)-1; $i>=0; $i--) {
                                array_push($stack, $childrens[$i]);
                            }
                        }
                    }
                }
            }
        $data["items"] = json_encode($items);
        return view($this->_config['view'], $data);
    }

    private function getRootCategory($category_id){
        /*$category_id = DB::table('product_categories')->where('product_id', $id)->orderByDesc('category_id')->addSelect('category_id')->first()->category_id;*/
        $category = $this->category->FindCategoryNameById($category_id);

        while ($category->parent_id != null)
            $category  = $this->category->FindCategoryNameById($category->parent_id);

        return $category;
    }

    public function export(Request $request)
    {
        $input = $request->all();
        /*$input['productsSku'] = explode(',', $input['productsSku'][0]);*/

        $skus = array_map(function($val){
            return explode('-', $val)[1];
        }, $input['productsSku']);
        
        $output = '';

        $selected_products = DB::table('product_flat')->whereIn('sku', $skus)->get();

        $grid = array();
        foreach ($input['productsSku'] as $product) {
            $cate_id = explode('-', $product)[0];
            $product_sku = explode('-', $product)[1];

            $root_category =  $this->getRootCategory($cate_id);
            if(!isset($grid[$root_category->name])){
                $grid[$root_category->name] = array();
            }
            $product_name = DB::table('product_flat')->where('sku', $product_sku)->first()->name;
            $grid[$root_category->name][$product_name] = array();
        }

        $query = DB::table('orders')
                    ->join('users', 'users.id', 'orders.customer_id')
                    ->whereIn('orders.status', ['completed', 'delivered'])
                    ->whereBetween('orders.created_at', [date($input['start_date']),
                        date('Y-m-d', strtotime("+1 day",strtotime($input['end_date'])))]);

        $orderQuery =  DB::table('order_items')
                        ->whereIn('order_id', $query->addSelect('orders.id')->get()->map(function($val){
                            return $val->id;
                        })->toArray())
                        ->whereIn('sku', $skus)
                        ->select('id','name', 'sku', DB::raw("created_at as day"),
                                                     DB::raw('SUM(qty_ordered) as total'),
                                                     DB::raw('count(*) as count'))
                        ->groupBy('sku', DB::raw('MONTH(created_at)'), DB::raw('YEAR(created_at)'))
                        ->get();

        foreach ($orderQuery as $product) {
            $product_str = array_values(array_filter($input['productsSku'], function($val) use ($product){
                return explode('-', $val)[1] == $product->sku;
            }))[0];
            $root_category = $this->getRootCategory(explode('-', $product_str)[0])->name;
            

            $interval ='';

            if($input["interval_type"]==2){
                $interval = date('M Y', strtotime($product->day));
            }else if($input["interval_type"]==1){
                $interval = date('d/m/Y', strtotime($product->day));
            }

        
            if(isset($grid[$root_category][$product->name])){
                if(isset($grid[$root_category][$product->name][$interval])){
                    $grid[$root_category][$product->name][$interval] += $product->total;
                }else{
                    $grid[$root_category][$product->name][$interval] =  $product->total;
                }
            }else{
                $grid[$root_category][$product->name] = array($interval => $product->total);
            }
        }

        $start = strtotime($input["start_date"]);
        $end = strtotime($input["end_date"]);


        if($input["interval_type"]==2){ // By month
            $output .= '<h5>From:</h5>,<h5>'.date('m-Y', $start)."</h5>\n";
            $output .= '<h5>To:</h5>,<h5>'.date('m-Y', $end)."</h5>";
            $header = array();
            $total_sup = '';
            for($month = $start;$month <= $end;$month = strtotime("+1 month", $month)){
                array_push($header, '<th><b>'.date('M Y', $month).'</b></th>');
                //$total_sup .=',';
            }
            array_push($header, '<th><b>Grand Total</b></th>');

            foreach ($grid as $product => $items){
                $output .= "\n".'<h3>'.$product.'</h3>'."\n";
                $total = 0;
                $each_month_total = array();
                $output .= '<th><b>Product</b></th>,'.implode(',' ,$header)."\n";
                foreach ($items as $item => $data) {
                    $ary_month = array();
                    for($month = $start; $month <= $end; $month = strtotime("+1 month", $month)){
                        $time_key = date('M Y',$month);
                        array_push($ary_month,  $data[$time_key] ?? '0');
                        if(!isset($each_month_total[$time_key])) {
                            $each_month_total[$time_key] = array();
                        }
                        array_push($each_month_total[$time_key], $data[$time_key] ?? '0');
                    }
                    $output .= $item.','.implode(',' ,$ary_month);
                    $product_total = array_reduce($ary_month, function($val1, $val2){
                        return $val1+$val2;
                    }, 0);
                    $total += $product_total;
                    $output .= ",".$product_total."\n";
                }
                $output .= "<b>Total</b>,";

                foreach($each_month_total as $key => $value){
                    $output .= '<b>'.array_reduce($value, function($val1, $val2){
                        return $val1+$val2;
                    }, 0).'</b>,';
                }

                $output .= "<b>".$total."</b>";
            }
        }else if($input["interval_type"]==1){ // By date
            $output .= '<h5>From:</h5>,<h5>'.date('d/m/Y', $start)."</h5>\n";
            $output .= '<h5>To:</h5>,<h5>'.date('d/m/Y', $end)."</h5>";
            $header = array();
            $total_sup = '';
            for($month=$start; $month<=$end; $month=strtotime("+1 day", $month)){
                array_push($header, '<th><b>'.date('d/m/Y', $month).'</b></th>');
                /*$total_sup .=',';*/
            }
            array_push($header, '<th><b>Grand Total</b></th>');

            
            foreach ($grid as $product => $items){
                $output .= "\n".'<h3>'.$product.'</h3>'."\n";
                $total = 0;
                $each_day_total = array();
                $output .= '<th><b>Product</b></th>,'.implode(',' ,$header)."\n";
                foreach ($items as $item => $data){
                    $ary_date = array();
                    for($month = $start; $month <= $end; $month = strtotime("+1 day", $month)){
                        $time_key = date('d/m/Y',$month);
                        array_push($ary_date,  $data[$time_key] ?? '0');
                        if(!isset($each_day_total[$time_key])) {
                            $each_day_total[$time_key] = array();
                        }
                        array_push($each_day_total[$time_key], $data[$time_key] ?? '0');
                    }
                    $output .= $item.','.implode(',' ,$ary_date);
                    $product_total = array_reduce($ary_date, function($val1, $val2){
                        return $val1+$val2;
                    }, 0);
                    $total += $product_total;
                    $output .= ",".$product_total."\n";
                }
                $output .= "<b>Total</b>,";
                foreach($each_day_total as $key => $value){
                    $output .= '<b>'.array_reduce($value, function($val1, $val2){
                        return $val1+$val2;
                    }, 0).'</b>,';
                }
                $output .= "<b>".$total."</b>";
            }
        }

        return Response::json(array(
            "from" => date($input['start_date']),
            "to" => date($input['end_date']),
            "csv"   => $output));
    }
}
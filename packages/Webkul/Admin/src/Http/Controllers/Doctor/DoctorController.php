<?php

namespace Webkul\Admin\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Customer\Repositories\CustomerRepository as Customer;
use Webkul\Customer\Models\States as States;
use Webkul\Customer\Models\Cities as Cities;
use Webkul\Customer\Repositories\CustomerGroupRepository as CustomerGroup;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Core\Repositories\ChannelRepository as Channel;
use Webkul\Customer\Mail\AccountCreationMail;
use Webkul\Customer\Mail\VerificationEmail;
use Webkul\Customer\Mail\WaitingForApproval;
use Webkul\Customer\Mail\UserSignup;
use Webkul\Customer\Models\Countries;
use Webkul\Checkout\Facades\Cart;
use Illuminate\Validation\Rule;
use Mail;
use DB;
use Cookie;


/**
 * Customer controlller
 *
 * @author    Rahul Shukla <rahulshukla.symfony517@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DoctorController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var array
     */
    protected $customer;
    protected $category;
     /**
     * CustomerGroupRepository object
     *
     * @var array
     */
    protected $customerGroup;

     /**
     * ChannelRepository object
     *
     * @var array
     */
    protected $channel;

     /**
     * Create a new controller instance.
     *
     * @param Webkul\Customer\Repositories\CustomerRepository as customer;
     * @param Webkul\Customer\Repositories\CustomerGroupRepository as customerGroup;
     * @param Webkul\Core\Repositories\ChannelRepository as Channel;
     * @return void
     */
    public function __construct(Customer $customer, CustomerGroup $customerGroup, Channel $channel,Category $category)
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->customer = $customer;
        $this->category = $category;
        $this->customerGroup = $customerGroup;

        $this->channel = $channel;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view($this->_config['view']);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        /*$customerGroup = $this->customerGroup->all();*/
        $data['customerGroup'] = DB::table('user_groups')->where('is_user_defined','!=', 0)->addSelect('id', 'name')->get();
        $data['channelName'] = $this->channel->all();
        $data['categories'] = $this->getDiscountTree();
        return view($this->_config['view'], $data);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate(request(), [
            'my_title' => 'required',
            'first_name' => 'string|required',
            'email' => 'email|required|unique:temp_users,email|unique:users,email',
            'phone_number' => 'required|numeric',
            'dental_license_no' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'clinic_name'=> 'required',
            'clinic_address' => 'required',
            'customer_group_id' => 'required',
            'pin_code' => 'required'
                    ],
                    [
                       'email.unique' => 'The "Email" has already been taken.'
                    ]
                );

        $data=request()->all();
        
        if(auth()->guard('admin')->user()->role_id == 2)
            $data['dealer_id'] = auth()->guard('admin')->user()->id;

        
        $password = bcrypt(rand(100000,10000000));
        //$data['user_type'] = 1;
        $data['country_id'] = 101;
        //Admin will approve the doctor even dealer added the doctor
        //$data['is_approved'] = 1;
        $data['role_id'] = 3;
        $data['customer_group_id'] = $request->group_id; //group_id
        //$data['password'] = $password;
        $data['channel_id'] = core()->getCurrentChannel()->id;
        $data['is_verified'] = 0;
        $verification['token'] = md5(uniqid(rand(), true));
        $data['token'] = $verification['token'];


        //$doctor_created = $this->customer->create($data);
        $temp_doctor_id = DB::table('temp_users')->insertGetId([
        												'my_title' => $request->my_title,
        												'first_name' => $request->first_name,
        												'last_name' => $request->last_name,
        												'email' => $request->email,
        												'phone_number' => $request->phone_number,
        												'clinic_name' => $request->clinic_name,
        												'clinic_address' => $request->clinic_address,
        												'clinic_number' => $request->clinic_number,
        												'dental_license_no' => $request->dental_license_no,
        												'dealer_id' => $data['dealer_id'],
        												'country_id' => 101,
        												'state_id' => $request->state_id,
        												'city_id' => $request->city_id,
        												'pin_code' => $request->pin_code,
        												'customer_group_id' => $request->customer_group_id,
        												'role_id' => 3,
        												'token' => md5(uniqid(rand(), true)),
        												'created_by' => auth()->guard('admin')->user()->id,
        												'created_at' => date('Y-m-d H:i:s'),
    												]);

        $temp_doctor = DB::table('temp_users')->where('id',$temp_doctor_id)->first();
        $full_name = $data['first_name'].' '.$data['last_name'];
        $discount_categories = array_values(request()->get('discount')?? []);

        if(isset($discount_categories) && !empty($discount_categories[0]['discount_per']))
            {

            $custom_code = mt_rand(10000, 99999);
            $gxt_custom_code = "GXT".$custom_code;
            $insert_discount_id = DB::table('discounts')->insertGetId([
                                            'customer_group_id' => $temp_doctor->customer_group_id,
                                            'for_all_users' => 0,
                                            'discount_type' =>1,
                                            'coupon_code' => $gxt_custom_code
                                       ]);
            if($insert_discount_id)
            {
                foreach($discount_categories as $d_key => $discount_category)
                {
                        $dis_cat = explode("," , $discount_category['categories']);
                        $dis_cat = array_map(function($val){
                            return explode('-', $val)[1];
                        }, $dis_cat);
                        
                   
                        foreach($dis_cat as $d_key => $category_id)
                        {
                            $insert_discount_category = DB::table('discount_category')->insert([
                                                        'discount_id' => $insert_discount_id,
                                                        'category_id' => $category_id,
                                                        'discount' => $discount_category['discount_per']
                                                ]);
                        }
                        session()->flash('success', trans('admin::app.discounts.created-success'));
                }

                $insert_discount_users = DB::table('discount_users')->insert([
                                                    'discount_id' => $insert_discount_id,
                                                    'user_id' => $temp_doctor_id,
                                                ]);
            }
            DB::table('temp_users')->where('id',$temp_doctor_id)->update(['discount_id' => $insert_discount_id]);
        }
       
           $verificationData['token'] = $data['token'];
           $verificationData['first_name'] = $data['first_name'];
           $verificationData['email'] = $data['email'];
           $verificationData['created_by'] = auth()->guard('admin')->user()->first_name;
           $verificationData['id'] = $temp_doctor_id;

        //$customer = $this->customer->findOneByField('email', $data['email']);

        //$this->customer->update(['token' => $verificationData['token']], $customer->id);

        try {

            session()->put('customer',$temp_doctor);
            $account_info = array();
            $account_reg_url = route('customer.register.index').'?id='.$verificationData['id'];
            $message = "Hello Dr ".$temp_doctor->first_name.", please click on ".$account_reg_url." to verify your Genxtimplants.com account created by ".auth()->guard('admin')->user()->first_name.".";
            //$message = nl2br("Hello Dr test, please click on link to verify your Genxtimplants.com account created by dealer name. \n - GenXT Dental Implants");
           //dd($message);
            $account_info['sms_numbers'] = '91'.$temp_doctor->phone_number;
            $account_info['sms_message'] = $message;
            $sent_sms=view('shop::send-sms',$account_info)->render();
            //dd($sent_sms);
            $verification_mail = Mail::send(new VerificationEmail($verificationData));
            //dd($verification_mail);
            if (Cookie::has('enable-resend')) {
                \Cookie::queue(\Cookie::forget('enable-resend'));
            }

            if (Cookie::has('email-for-resend')) {
                \Cookie::queue(\Cookie::forget('email-for-resend'));
            }
        } catch(\Exception $e) {
            dd($e);
            session()->flash('success', trans('shop::app.customer.signup-form.verification-not-sent'));

            return redirect()->back();
        }

        session()->flash('success', trans('shop::app.customer.signup-form.verification-sent'));
      
        return redirect()->route($this->_config['redirect']);
    }
    

    public function show($id)
    {
        $customer = $this->customer->findOneWhere(['id'=>$id]);
        $totalDoctorOrders = DB::table('orders')->where('customer_id',$id)->count();
        $doctorSales = DB::table('orders')->where('customer_id',$id)->sum('grand_total');
        $doctorOrders = DB::table('orders')->where('customer_id',$id)
                                            ->orderBy('created_at','desc')
                                            ->get();
        //dd($totalDealerOrders);
        $customerGroup = $this->customerGroup->all();
        $channelName = $this->channel->all();

        $creditOrders = DB::table('orders')->where('customer_id',$id)
                                        ->leftJoin('order_payment', 'orders.id', '=', 'order_payment.order_id')
                                            ->where('order_payment.method','paylaterwithcredit')
                                            ->orderBy('orders.created_at','desc')
                                            ->get();

        $creditPayments = DB::table('credit_payments')->where('user_id',$id)
                                            ->where('status',1)
                                            ->orderBy('created_at','desc')
                                            ->get();

        $credit_pending = DB::table('credit_payments')
                                    ->where('user_id', $id)
                                    ->where('status', 0)
                                    ->sum('amount');

        $credit_balance = DB::table('users')->where('id',$id)->value('credit_balance');


        return view($this->_config['view'],compact('customer', 'customerGroup', 'channelName','totalDoctorOrders','doctorOrders','doctorSales', 'creditOrders', 'creditPayments', 'credit_pending', 'credit_balance'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $all_discount = [];
        $customer = $this->customer->findOneWhere(['id'=>$id]);
        $categry_discounts = DB::table('discounts as d')
                                                ->where('d.discount_type',1)
                                                ->leftjoin('discount_users as du','du.discount_id','d.id')
                                                ->leftjoin('discount_category as dc','dc.discount_id','d.id')
                                                ->where('du.user_id',$customer->id)
                                                ->select('dc.discount','d.id')
                                                ->groupBy('dc.discount')
                                                ->get()->unique();

        foreach($categry_discounts as $category_discount){
            $discount_category = DB::table('discount_category')
                                        ->where('discount_id', $category_discount->id)
                                        ->where('discount',$category_discount->discount)
                                        ->pluck('category_id')->toArray();

            if(count($discount_category) > 0) {
                $all_discount[] = [
                    'categorys'=> array_values($discount_category),
                    'percentage'=>$category_discount->discount
                ];
            }
        }

        /*$customerGroup = $this->customerGroup->all();*/
        $data['customer'] =$customer;
        $data['customerGroup'] = DB::table('user_groups')->where('is_user_defined','!=', 0)->addSelect('id', 'name')->get();
        $data['all_discount'] =  json_encode($all_discount);
        $data['categories'] = $this->getDiscountTree();
        $data['categry_discount'] = $categry_discounts;
        $data['channelName'] = $this->channel->all();
        return view($this->_config['view'], $data);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->validate(request(), [
            'my_title' => 'required',
            'first_name' => 'string|required',
            'email' => ['email', 'required', Rule::unique('users')->ignore($id)],
            'phone_number' => 'required|numeric',
            'dental_license_no' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'clinic_name'=> 'required',
            'clinic_address' => 'required',
            'pin_code' => 'required',
            'customer_group_id' => 'required',
        ]);

        $data=request()->all();

        //$data['user_type'] = 1;
        $data['country_id'] = 101;
        $data['channel_id'] = core()->getCurrentChannel()->id;

        if(auth()->guard('admin')->user()->role_id == 2)
            $data['dealer_id'] = auth()->guard('admin')->user()->id;
            
		if($this->customer->findOneWhere(['id'=>$id])->customer_group_id != $request->customer_group_id){
            \DB::table('users')->where('id', $id)->update(['group_changed_by' => auth()->guard('admin')->user()->id,
														   'group_changed_at' => date('Y-m-d H-i-s')]);
        }

        $this->customer->update($data,$id);

        $discount_categories = array_values(request()->get('discount') ?? []);

        $discount_id = $request->get('discount_id');
        if(!$discount_id){
            $discount_id = DB::table('discounts')->insertGetId([
                                                'customer_group_id' => $request->customer_group_id,
                                                'for_all_users' => 0,
                                                'discount_type' =>1
                                           ]);

        }else{
            DB::table('discounts')->where('id', $discount_id)->update([
                                                'customer_group_id' => $request->customer_group_id,
                                                'for_all_users' => 0,
                                                'discount_type' =>1
                                           ]);
            $delete_discount_category = DB::table('discount_category')->where('discount_id',$discount_id)->delete();

            $delete_discount_users = DB::table('discount_users')
                                            ->where('user_id',$id)
                                            ->where('discount_id',$discount_id)->delete();
        }
        // $delete_discount = DB::table('discounts')->where('id',$discount_id)->delete();        

        if(isset($discount_categories[0]['categories']) && !empty($discount_categories[0]['discount_per']))
        {
            $get_user = DB::table('users')->where('id', $id)->first();
            
                foreach($discount_categories as $d_key => $discount_category)
                {
                    $dis_cat = explode("," , $discount_category['categories']);
                    $dis_cat = array_map(function($val){
                        return explode('-', $val)[1];
                    }, $dis_cat);
                    
                    foreach($dis_cat as $d_key => $category)
                    {
                        DB::table('discount_category')->insert([
                                                    'discount_id' => $discount_id,
                                                    'category_id' => $category,
                                                    'discount' => $discount_category['discount_per']
                                            ]);
                    }
                    
                }
                DB::table('discount_users')->insert([
                                                    'discount_id' => $discount_id,
                                                    'user_id' => $id,
                                                ]);
            
        }
        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Customer']));
        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$delete =  DB::table('users')->where('id',$id)->update(['is_deleted' => 1]);
        $this->customer->delete($id);
        session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Customer']));
        return redirect()->back();
    }

    public function massDestroy()
    {
        $doctorIds = explode(',', request()->input('indexes'));

        foreach ($doctorIds as $doctorId) {
            $delete =  DB::table('users')->where('id',$doctorId)->delete();
        }

        session()->flash('success', trans('admin::app.response.delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

    public function DoctorApprovals()
    {
        return view($this->_config['view']);
    }


    public function approve(Request $request, $id)
    {
        $user_detail = DB::table('users')->where('id', $id)->where('is_approved',0)->where('is_rejected',0)->where('is_deleted',0)->first();
        //dd($user_detail);
         $token = $user_detail->token;
        if($token == null) $token = md5(uniqid(rand(), true));
            
        if($user_detail)
        {

          $approve_user = DB::table('users')
                            ->where('id', $id)
                            ->update(['is_approved' => 1,
                                        'token' => $token ]);
        $full_name = $user_detail->first_name.' '.$user_detail->last_name;
        $set_password_url = url('customer/set-user-password/'.$token);
        $dealer_name="";

        $message = 'Hello Dr '.$user_detail->first_name.', your GenXT WebX account is Approved. Click '.$set_password_url.' to create password';
        $sms_info = array();
        $sms_info['sms_numbers'] = $user_detail->phone_number;
        $sms_info['sms_message'] = $message;     
        $sent_sms=view('shop::send-sms',$sms_info)->render();

        if(isset($user_detail->dealer_id)) {
            $dealer_info = DB::table('users')->where('id',$user_detail->dealer_id)->first();
            $dealer_name = $dealer_info->first_name.' '.$dealer_info->last_name;
        }
            Mail::to($user_detail->email)->send(new AccountCreationMail($full_name, $user_detail->email,$dealer_name,$set_password_url));
        }
        return redirect()->route($this->_config['redirect']);
    }

    public function reject(Request $request, $id)
    {
        $user_detail = DB::table('users')->where('id', $id)->where('is_approved',0)->where('is_deleted',0)->first();
        $token = $user_detail->token;
        if($token == null) $token = md5(uniqid(rand(), true));
            $reject_user = DB::table('users')
                            ->where('id', $id)
                            ->update(['is_approved' => 0,
                                        'is_rejected' =>1,
                                        'token' => $token ]);
        return redirect()->route($this->_config['redirect']);
    }
    
    public function unverified(){
        return view($this->_config['view']);
    }

    public function verify($id){
        $user = DB::table('users')->where('id', $id)->first();

        $data['my_title']          = $user->my_title;
        $data['first_name']        = $user->first_name;
        $data['last_name']         = $user->last_name ;
        $data['email']             = $user->email;
        $data['phone_number']      = $user->phone_number; 
        $data['pin_code']          = $user->pin_code; 
        $data['clinic_name']       = $user->clinic_name;
        $data['clinic_address']    = $user->clinic_address;
        $data['clinic_number']     = $user->clinic_number;
        $data['dental_license_no'] = $user->dental_license_no;

        $data['country_name'] = Countries::GetCountryName($user->country_id);
        $data['state_name'] = States::GetStateName($user->state_id);
        $data['city_name'] = Cities::GetCityName($user->city_id);
        

        DB::table('users')->where('id', $id)->update([ 'is_verified' => 1,
                        'token' => NULL,
                        'verify_otp' => NULL,
                        'verified_by' => auth()->guard('admin')->user()->id]);


            $account_info=array();
            $message = 'Hello Dr '.$user->first_name.', your details are successfully submitted to GenXT WebX. We will let you know once your account is active.';
            $account_info['sms_numbers'] = '91'.$user->phone_number;
            $account_info['sms_message'] = $message;
            $sent_sms=view('shop::send-sms',$account_info)->render();
            DB::table('temp_users')->where('email',$user->email)->delete();

            Mail::to($user->email)->send(new WaitingForApproval($user->first_name));

            $admins = DB::table('users')->where('role_id',1)->where('status',1)->get();
            foreach($admins as $key =>$admin)
            {
                $mail_sent = Mail::to($admin->email)->send(new UserSignup($data));   
            }

        session()->flash('success', 'User verfied Successfully');
        return redirect()->route($this->_config['redirect']);
    }
    
    public function notify($id){
        $user = DB::table('users')->where('id', $id)->first();
        if($user->is_verified==0){
            $verify_url = route('check.otp', $user->id);

            $message = "Hi, your registration process is not completed, Please click on the link below to continue the registration process\n\n$verify_url";
            $sms_info['sms_numbers'] = $user->phone_number;
            $sms_info['sms_message'] = $message;     
            $sent_sms=view('shop::send-sms',$sms_info)->render();

            Mail::send('shop::emails.pending-registration', compact('verify_url', 'user'), function ($message) use($user) {
                $message->from('info@genxtimplants.com', 'GenXT');
                $message->to($user->email)->subject('GenXT - Registration Incomplete!');
            });

            session()->flash('success', 'User Notified Successfully');
        }
        return redirect()->back();
    }

    public function delete($id){
        DB::table('users')->where('id', $id)->delete();
        session()->flash('success', 'User Deleted Successfully');
        return redirect()->route($this->_config['redirect']);
    }

    public function massDelete(Request $request){
        $doctorIds = explode(',', request()->input('indexes'));
        foreach ($doctorIds as $doctorId) {
            $delete =  DB::table('users')->where('id',$doctorId)->delete();
        }
        session()->flash('success', 'Users Deleted Successfully');
        return redirect()->route($this->_config['redirect']);
    }


    private function getDiscountTree(){
        $items = array();

        foreach($this->category->getCategoryTree() as $cat){ //n-array preorder TreeTraversal algorithm
            if($cat!=null){
                $stack = array();
                array_push($stack, $cat);

                while(count($stack)>0){
                    $temp = array_pop($stack);
                    $childrens = $temp->children;
                    if(count($childrens)<=0){
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'end'=> true,
                                                 'name' => $temp->name));
                    }else{
                        array_push($items, array('id' => $temp->id,
                                                 'pid' => ($temp->parent_id!=null)?$temp->parent_id:'',
                                                 'end'=> false,
                                                 'name' => $temp->name));
                        for($i=count($childrens)-1; $i>=0; $i--){
                            array_push($stack, $childrens[$i]);
                        }
                    }
                }
            }
        }
        return json_encode($items);
    }
}

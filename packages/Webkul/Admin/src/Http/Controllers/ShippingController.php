<?php

namespace Webkul\Admin\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$shipping = DB::table('shipping')->where('status',1)->get();
        return view('admin::settings.shipping.index')->with(['shipping' => $shipping]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::settings.shipping.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate(request(), [
            'min_weight' => 'required',
            'max_weight' => 'required',
            'shipping_amount' => 'required'
       ]);

        //dd($request->all());
        $get_weights = DB::table('shipping')->where('status',1)->get();
        $already_exists = false;
        foreach($get_weights as $key => $get_weight)
        {
            if((($request->get('min_weight') >= $get_weight->min_weight) && ($request->get('max_weight') <= $get_weight->max_weight)))
            {
                session()->flash('success', 'Please enter the weight than the maximum weight already given!');
                $already_exists = true;
                break; 
            }
        }
        //dd($already_exists);
        if(!$already_exists)
        { 
            $insert_shipping = DB::table('shipping')->insert([
                                                'min_weight' => $request->get('min_weight'),
                                                'max_weight' => $request->get('max_weight'),
                                                'shipping_amount' => $request->get('shipping_amount'),
                                                'created_at' => date('Y-m-d H:i:s'),
                                                ]);
            if ($insert_shipping)
                session()->flash('success', trans('admin::app.settings.shipping.created-success'));
            else
                session()->flash('success', trans('admin::app.settings.shipping.created-fail'));   
        }      
    
        return redirect()->route('admin.shipping.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $shipping = DB::table('shipping')->where('id',$id)->first();
        return view('admin::settings.shipping.edit')->with('shipping', $shipping);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate(request(), [
            'min_weight' => 'required',
            'max_weight' => 'required',
            'shipping_amount' => 'required'
       ]);

        $update_shipping = DB::table('shipping')->where('id',$id)->update([
                                            'min_weight' => $request->get('min_weight'),
                                            'max_weight' => $request->get('max_weight'),
                                            'shipping_amount' => $request->get('shipping_amount'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        ]);
        if ($update_shipping)
            session()->flash('success', trans('admin::app.settings.shipping.update-success'));
        else
            session()->flash('success', trans('admin::app.settings.shipping.update-fail'));
        

        return redirect()->route('admin.shipping.index'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = DB::table('shipping')->where('id',$id)->update(['status'=>0]);
        if ($delete)
            session()->flash('success', trans('admin::app.settings.shipping.delete-success'));
        else
            session()->flash('success', trans('admin::app.settings.shipping.delete-fail'));

        return redirect()->route('admin.shipping.index');
 
    }

}

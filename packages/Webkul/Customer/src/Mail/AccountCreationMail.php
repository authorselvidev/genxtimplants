<?php

namespace Webkul\Customer\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountCreationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($full_name,$email,$dealer_name, $set_password_url)
    {
        $this->full_name = $full_name;
        $this->email = $email;
        $this->dealer_name = $dealer_name;
        $this->set_password_url = $set_password_url;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@genxtimplants.com', 'GenXT')->view('shop::emails.customer.account-creation',['full_name'=>$this->full_name,'email'=>$this->email, 'dealer_name' => $this->dealer_name, 'set_password_url' => $this->set_password_url])->subject('Your Account is Active. Set Your Password.');
    }
}

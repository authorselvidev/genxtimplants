<?php

namespace Webkul\Customer\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Webkul\Customer\Models\CustomerGroup;
use Webkul\Checkout\Models\Cart;
use Webkul\Sales\Models\Order;
use Webkul\Customer\Models\Wishlist;
use Webkul\Product\Models\ProductReview;
use Webkul\User\Models\Role;

use Webkul\Customer\Notifications\CustomerResetPassword;
use DB;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $fillable = ['my_title', 'first_name', 'last_name', 'channel_id', 'phone_number', 'email', 'password', 'clinic_name', 'user_type', 'clinic_address', 'clinic_number', 'state_id', 'city_id', 'country_id', 'pin_code', 'dental_license_no', 'dealer_id', 'dealer_commission', 'credit_available', 'is_approved', 'role_id', 'verify_otp', 'super_admin', 'customer_group_id', 'is_verified', 'token', 'created_by', 'permission_to_create_order', 'prepayment_available', 'company_name', 'gst_no', 'fda_licence_no', 'credit_balance', 'credit_limit', 'credit_used'];

    protected $hidden = ['password', 'remember_token'];

    /**
     * Get the customer full name.
     */
    public function getNameAttribute() {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    static function CustomerName($id='')
    {
        $first_name=DB::table('users')->where('id',$id)->value('first_name');
        $last_name=DB::table('users')->where('id',$id)->value('last_name');
        $my_title=DB::table('users')->where('id',$id)->value('my_title');
        $customer_name = $my_title.' '.$first_name.' '.$last_name;
        return $customer_name;
    }
    /**
     * Get the customer group that owns the customer.
     */
    public function customerGroup()
    {
        return $this->belongsTo(CustomerGroup::class);
    }

    static function customerGroupId($id)
    {
        $customer_group_id=DB::table('users')->where('id',$id)->value('customer_group_id');

        return $customer_group_id;
    }

    static function customerRoleId($id)
    {
        $customer_role_id=DB::table('users')->where('id',$id)->value('role_id');

        return $customer_role_id;
    }

    static function customerRoleName($id)
    {
        $customer_role_id=DB::table('users')->where('id',$id)->value('role_id');
        $customer_role_name=DB::table('roles')->where('id',$customer_role_id)->value('name');

        return $customer_role_name;
    }

    /**
    * Send the password reset notification.
    *
    * @param  string  $token
    * @return void
    */
   /* public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPassword($token));
    }
*/
    /**
     * Get the customer address that owns the customer.
     */
    public function addresses()
    {
        return $this->hasMany(CustomerAddress::class, 'customer_id');
    }

    /**
     * Get default customer address that owns the customer.
     */
    public function default_address()
    {
        return $this->hasOne(CustomerAddress::class, 'customer_id')->where('default_address', 1);
    }

    /**
     * Customer's relation with wishlist items
     */
    public function wishlist_items() {
        return $this->hasMany(Wishlist::class, 'customer_id');
    }

    /**
     * get all cart inactive cart instance of a customer
     */
    public function all_carts() {
        return $this->hasMany(Cart::class, 'customer_id');
    }

    /**
     * get inactive cart inactive cart instance of a customer
     */
    public function inactive_carts() {
        return $this->hasMany(Cart::class, 'customer_id')->where('is_active', 0);
    }

    /**
     * get active cart inactive cart instance of a customer
     */
    public function active_carts() {
        return $this->hasMany(Cart::class, 'customer_id')->where('is_active', 1);
    }

    /**
     * get all reviews of a customer
    */
    public function all_reviews() {
        return $this->hasMany(ProductReview::class, 'customer_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
    * Send the password reset notification.
    *
    * @param  string  $token
    * @return void
    */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPassword($token));
    }

    /**
     * Checks if admin has permission to perform certain action.
     *
     * @param  String  $permission
     * @return Boolean
     */
    public function hasPermission($permission)
    {
        return in_array($permission, $this->role->permissions);
    }
}

<?php
namespace Webkul\Customer\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $table = 'user_addresses';

    protected $fillable = ['customer_id' ,'address', 'country', 'state', 'city', 'postcode','my_title','name','last_name','phone', 'default_address','delivery_selected','billing_selected'];
}

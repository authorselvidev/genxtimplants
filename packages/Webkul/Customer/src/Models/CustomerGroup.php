<?php
namespace Webkul\Customer\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Customer\Models\Customer;
use DB;

class CustomerGroup extends Model
{
    protected $table = 'user_groups';

    protected $fillable = ['name','points_needed','order_amount_from','order_amount_to','is_user_defined'];

    /**
     * Get the customer for this group.
    */
    public function customer()
    {
        return $this->hasMany(Customer::class);
    }

    static function CustomerGroupName($id='')
    {
    	$customer_group_name=DB::table('user_groups')->where('id',$id)->value('name');

    	return $customer_group_name;
    }

    static function CustomerGroupAmountRange($id='')
    {
        $customer_group_from=DB::table('user_groups')->where('id',$id)->value('order_amount_from');
        $customer_group_to=DB::table('user_groups')->where('id',$id)->value('order_amount_to');
        $customer_group_points = '₹'.number_format($customer_group_from,0).' - ₹'.number_format($customer_group_to,0);
        return $customer_group_points;
    }
   
}

<?php

namespace Webkul\Customer\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Sales\Repositories\OrderRepository;
use Illuminate\Support\Facades\Storage;
use Mail;
use Redirect;
use Session;
use PDF;
use DB;
/**
 * Home page controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
 class FailureImplantsController extends controller{
	protected $_config;

    public function __construct(OrderRepository $orders){
        $this->_config = request('_config');
        $this->orders = $orders;
        $this->user_details = auth()->guard('customer')->user();
    }

    public function index(){
        $data["failure_implants_used"] = DB::table('failed_implants')
                            ->where('is_deleted', 0)
                            ->where('user_id', $this->user_details->id)
                            ->where('item_status', 0)
                            ->latest()->get();
        $data["failure_implants_notused"] = DB::table('failed_implants')
                            ->where('is_deleted', 0)
                            ->where('user_id', $this->user_details->id)
                            ->where('item_status', 1)
                            ->latest()->get();
        return view($this->_config['view'], $data);
    }

	public function usedform()
	{
		$data=array();

        $orders = $this->orders->findWhere([
            'customer_id' => $this->user_details->id,
        ])->whereIn('status', ['completed','delivered'])->sortByDesc('id');

        $products = \DB::table('product_flat')
                ->leftjoin('products','products.id','product_flat.product_id')
                ->leftjoin('product_inventories','product_inventories.product_id','product_flat.product_id')
                ->where('products.is_deleted',0)
                ->get();

        $user_details = $this->user_details;
        return view($this->_config['view'], compact('user_details', 'orders', 'products'));
	}

    public function unusedform()
    {
        $data=array();
        $orders = $this->orders->findWhere([
            'customer_id' => $this->user_details->id,
        ])->whereIn('status', ['completed','delivered'])->sortByDesc('id');

        
        $products = \DB::table('product_flat')
                ->leftjoin('products','products.id','product_flat.product_id')
                ->leftjoin('product_inventories','product_inventories.product_id','product_flat.product_id')
                ->where('products.is_deleted',0)
                ->get();

        $user_details = $this->user_details;

        return view($this->_config['view'], compact('user_details', 'orders', 'products'));
    }

    public function show($id){

        $data['failed_implant'] = DB::table('failed_implants')->where('id', $id)->where('is_deleted', 0)->first();

        if($data['failed_implant']->order_type == 'on'){
            $data['order_items'] = DB::table('order_items')->where('id', $data['failed_implant']->order_item_id)->first();
            $data['invoice_item_id'] = DB::table('invoice_items')->where('order_item_id', $data['order_items']->id)->first();
            $data['order'] = DB::table('orders')->where('id', $data['order_items']->order_id)->first();
            $data['user'] = DB::table('users')->where('id', $data['order']->customer_id)->first();
        }else{
            $data['user'] = DB::table('users')->where('id', $data['failed_implant']->user_id)->first();
        }

        $data['product'] = \DB::table('product_flat')
                            ->where('product_id', $data['failed_implant']->replacement_product_id)
                            ->first();
        return view($this->_config['view'], $data);
    }

    public function download(Request $data){
        if($data->type == 'before'){
            $url="failed_implants/xray_implant/".$data->file;
        }else if($data->type == 'after'){
            $url="failed_implants/before_implant_removed/".$data->file;
        }else{
            return "Invalid request";
        }

        if(Storage::exists($url)){
            return Storage::download($url);
        }else{
            Session::flash('error','File not found on the server');
            return Redirect::back();
        }
    }

    public function edit($id){
        $data['failed_implant'] = DB::table('failed_implants')->where('id', $id)->where('is_deleted', 0)->first();
         $data['orders'] = $this->orders->findWhere([
            'customer_id' => $this->user_details->id,
        ])->whereIn('status', ['completed', 'delivered'])->sortByDesc('id');
        $user_details = $this->user_details;
        $data['user_details'] = $this->user_details;

        $data['products'] = \DB::table('product_flat')
                ->leftjoin('products','products.id','product_flat.product_id')
                ->leftjoin('product_inventories','product_inventories.product_id','product_flat.product_id')
                ->where('products.is_deleted',0)
                ->get();

        return view($this->_config['view'], $data);
    }

    public function destroy($id){
        DB::table('failed_implants')->where('id', $id)->update(['is_deleted'=>1]);
        return redirect()->route('failure.implants.index');
    }

    public function print($id){
        $data['failed_implant'] = DB::table('failed_implants')->where('id', $id)->where('is_deleted', 0)->first();

        if($data['failed_implant']->order_type == 'on'){
            $data['order_items'] = DB::table('order_items')->where('id', $data['failed_implant']->order_item_id)->first();
            $data['invoice_item_id'] = DB::table('invoice_items')->where('order_item_id', $data['order_items']->id)->first();
            $data['order'] = DB::table('orders')->where('id', $data['order_items']->order_id)->first();
            $data['user'] = DB::table('users')->where('id', $data['order']->customer_id)->first();
        }else{
            $data['user'] = DB::table('users')->where('id', $data['failed_implant']->user_id)->first();
        }

        $pdf = PDF::loadView('shop::customers.account.failure_implants.pdf', $data)->setPaper('a4');
        return $pdf->download('return_implants-' . date('d-m-Y', strtotime($data['failed_implant']->created_at)).'-'.rand(1, 9999). '.pdf');
    }

    public function usedupdate(Request $request, $id){
        $input=$request->all();

        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        if($input['formtype']==1){
            $input['reason']=implode(', ', $input['reason']);

            $rand = date('Y-m-d-His').rand(1, 9999);
            $xray_implant=$request->file('xray_implant');

            $xray = false;
            $before_xray = false;

            if(isset($xray_implant) && $xray_implant->getClientOriginalName()) {
                $xray_implant_name = pathinfo($xray_implant->getClientOriginalName(), PATHINFO_FILENAME).'-'.$rand.'.'.$xray_implant->getClientOriginalExtension();
                $xray_implant->storeAs('failed_implants\xray_implant',$xray_implant_name);
                $xray = true;
            }

            $before_implant_removed=$request->file('before_implant_removed');
            if(isset($before_implant_removed) && $before_implant_removed->getClientOriginalName()) {
                $before_implant_removed_name = pathinfo($before_implant_removed->getClientOriginalName(), PATHINFO_FILENAME).'-'.$rand.'.'.$before_implant_removed->getClientOriginalExtension();
                $before_implant_removed->storeAs('failed_implants\before_implant_removed',$before_implant_removed_name);
                $before_xray = true;
            }

            $implant_table = [
                'user_id' => $this->user_details->id,
                'implant_code' => $input['implant_code'],
                'lot' => $input['lot'],
                'order_type' => $input['order_type'],  // new
                'order_number' => $input['order_number'],  // new
                'qty' => ($input['order_type']=='on')?$input['order_qty']:$input['order_qty_offline'],
                'order_item' => ($input['order_type']=='off')?$input['product_name']:'',
                'replace_qty' => $input['replace_qty'] ?? 0,
                'replace_offline_qty' => $input['replace_offline_qty'] ?? 0,
                'bone_type' => $input['bone_type'],
                'order_item_id' => intval($input['order_item']),
                'flapless' => (($input['flapless']=='Yes')?true:false),
                'immediate_implant' => (($input['immediate_implant']=='Yes')?true:false),
                'immediate_load' => (($input['immediate_load']=='Yes')?true:false),
                'implant_date' => $input['implant_date'],
                'implant_removal_date' => $input['implant_removal_date'],
                'remove_location' => $input['remove_location'],
                'reason' => $input['reason'],
                'other_reason_failure' => $input['other_reason_failure'],
                'patient_age' => $input['age'],
                'is_lessper'=> $input['formtype'],
                'gender' => $input['gender'],
                'item_status' => $input['item_status'],
                'replacement_reason' => $input['replacement_reason'],
                'replacement_product_id'=> ($input['order_type']=='on')?$input['replacement_item_online']:$input['replacement_item_offline'],
                'normal_history' => (($input['normal_history']=='Yes')?true:false),
                'smoker_history' => (($input['smoker_history']=='Yes')?true:false),
                'hypertension_history' => (($input['hypertension_history']=='Yes')?true:false),
                'cardiac_problems_history' => (($input['cardiac_problems_history']=='Yes')?true:false),
                'diabetes_history' => (($input['diabetes_history']=='Yes')?true:false),
                'alcoholism_history' => (($input['alcoholism_history']=='Yes')?true:false),
                'trauma_history' => (($input['trauma_history']=='Yes')?true:false),
                'cancer_history' => (($input['cancer_history']=='Yes')?true:false),
                'others' => $input['others'],
                'updated_at' => date('Y-m-d h:i:s')];

                if($xray){
                    $implant_table['xray_implant'] =  $xray_implant_name;
                }

                if($before_xray){
                    $implant_table['before_implant_removed'] =  $before_implant_removed_name;
                }

                DB::table('failed_implants')->where('id', $id)->update($implant_table);

                $admins = DB::table('users')->whereIn('role_id',[1, 4])->where('is_deleted',0)->get();

                $orders = $this->orders->findWhere([
                    'customer_id' => $this->user_details->id,
                ])->where('status', 'completed')->sortByDesc('id');

                foreach ($admins as $key => $admin) {
                    Mail::send('shop::emails.customer.failed-implants', compact('input', 'orders'), function ($message) use ($input, $xray_implant, $before_implant_removed, $admin, $xray, $before_xray){
                        $message->from('info@genxtimplants.com', 'GenXT');
                        $message->to($admin->email)->subject('Return Implants Updated');
                        if($xray){
                            $message->attach($xray_implant->getRealPath(), array('as' => $xray_implant->getClientOriginalName(),'mime' => $xray_implant->getMimeType()));
                        }

                        if($before_xray){
                            $message->attach($before_implant_removed->getRealPath(), array('as' => $before_implant_removed->getClientOriginalName(),'mime' => $before_implant_removed->getMimeType()));
                        }
                    });
                }

                Mail::send('shop::emails.customer.failed-implants', compact('input', 'orders'), function ($message) use($input, $xray_implant,$before_implant_removed,$user, $xray, $before_xray){
                        $message->from('info@genxtimplants.com', 'GenXT');
                        $message->to($user->email)->subject('Return Implants Updated');
                        if($xray){
                            $message->attach($xray_implant->getRealPath(), array('as' => $xray_implant->getClientOriginalName(),'mime' => $xray_implant->getMimeType()));
                        }

                        if($before_xray){
                            $message->attach($before_implant_removed->getRealPath(), array('as' => $before_implant_removed->getClientOriginalName(),'mime' => $before_implant_removed->getMimeType()));
                        }
                });
            }else{
                $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
                if(empty($user))
                    $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
                    
                DB::table('failed_implants')->where('id', $id)->update([
                        'user_id' => $user->id,
                        'implant_code' => $input['implant_code'],
                        'lot' => $input['lot'],
                        'order_type' => $input['order_type'],  // new
                        'order_number' => $input['order_number'],  // new
                        'order_item' => ($input['order_type']=='off')?$input['product_name']:'',
                        'qty' => ($input['order_type']=='on')?$input['order_qty']:$input['order_qty_offline'],
                        'replace_qty' => $input['replace_qty'] ?? 0,
                        'replace_offline_qty' => $input['replace_offline_qty'] ?? 0,
                        'bone_type' => '',
                        'order_item_id' => intval($input['order_item']),
                        'flapless' => false,
                        'immediate_implant' => false,
                        'immediate_load' => false,
                        'implant_date' => '',
                        'implant_removal_date' => '',
                        'remove_location' => '',
                        'reason' => '',
                        'other_reason_failure' => '',
                        'patient_age' => 0,
                        'is_lessper'=> $input['formtype'],
                        'gender' => '',
                        'item_status' => $input['item_status'],
                        'replacement_product_id'=> ($input['order_type']=='on')?$input['replacement_item_online']:$input['replacement_item_offline'],
                        'replacement_reason' => $input['replacement_reason'],
                        'normal_history' => false,
                        'smoker_history' => false,
                        'hypertension_history' => false,
                        'cardiac_problems_history' => false,
                        'diabetes_history' => false,
                        'alcoholism_history' => false,
                        'trauma_history' => false,
                        'cancer_history' => false,
                        'others' => '',
                        'xray_implant' => '',
                        'before_implant_removed' => '',
                        'created_at' => date('Y-m-d h:i:s'),
                        'updated_at' => date('Y-m-d h:i:s')]);
        
                $admins = DB::table('users')->whereIn('role_id', [1, 4])->where('is_deleted', 0)->get();
        
                $orders = $this->orders->findWhere([
                    'customer_id' => $this->user_details->id,
                ])->whereIn('status', ['completed', 'delivered'])->sortByDesc('id');
        
                foreach ($admins as $key => $admin) {
                     Mail::send('shop::emails.customer.failed-implants', compact('input', 'orders'), function ($message) use ($input, $admin){
                         $message->from('info@genxtimplants.com', 'GenXT');
                         $message->to($admin->email)->subject('Return Implants');
                     });
                }
        
                Mail::send('shop::emails.customer.failed-implants-customer', compact('input', 'orders', 'user'), function ($message) use ($input , $user){
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($user->email)->subject('Return Implants');
                });

            }
            

        Session::flash('success', 'Failure details updated successfully');

        return Redirect::route('failure.implants.index');
    }

    public function unusedstore(Request $request){
        $input=$request->all();
		
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        DB::table('failed_implants')->insert([
                'user_id' => $user->id,
                'implant_code' => $input['implant_code'],
                'lot' => $input['lot'],
                'order_type' => $input['order_type'],  // new
                'order_number' => $input['order_number'],  // new
                'order_item' => ($input['order_type']=='off')?$input['product_name']:'',
                'qty' => ($input['order_type']=='on')?$input['order_qty']:$input['order_qty_offline'],
                'replace_qty' => $input['replace_qty'] ?? 0,
                'replace_offline_qty' => $input['replace_offline_qty'] ?? 0,
                'bone_type' => '',
                'order_item_id' => intval($input['order_item']),
                'flapless' => false,
                'immediate_implant' => false,
                'immediate_load' => false,
                'implant_date' => '',
                'implant_removal_date' => '',
                'remove_location' => '',
                'reason' => '',
                'other_reason_failure' => '',
                'patient_age' => 0,
                'is_lessper'=> 2,
                'gender' => '',
                'item_status' => $input['item_status'],
                'replacement_product_id'=> ($input['order_type']=='on')?$input['replacement_item_online']:$input['replacement_item_offline'],
                'replacement_reason' => $input['replacement_reason'],
                'normal_history' => false,
                'smoker_history' => false,
                'hypertension_history' => false,
                'cardiac_problems_history' => false,
                'diabetes_history' => false,
                'alcoholism_history' => false,
                'trauma_history' => false,
                'cancer_history' => false,
                'others' => '',
                'xray_implant' => '',
                'before_implant_removed' => '',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')]);

        $admins = DB::table('users')->whereIn('role_id', [1, 4])->where('is_deleted', 0)->get();

        $orders = $this->orders->findWhere([
            'customer_id' => $this->user_details->id,
        ])->whereIn('status', ['completed', 'delivered'])->sortByDesc('id');

        foreach ($admins as $key => $admin) {
             Mail::send('shop::emails.customer.failed-implants', compact('input', 'orders'), function ($message) use ($input, $admin){
                 $message->from('info@genxtimplants.com', 'GenXT');
                 $message->to($admin->email)->subject('Return Implants');
             });
        }

        Mail::send('shop::emails.customer.failed-implants-customer', compact('input', 'orders', 'user'), function ($message) use ($input , $user){
                 $message->from('info@genxtimplants.com', 'GenXT');
                 $message->to($user->email)->subject('Return Implants');
        });

        Session::flash('success','Failure details posted successfully');

        return Redirect::route('failure.implants.index');
    }


    public function unusedupdate(Request $request, $id){
        $input=$request->all();
		
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
            
        DB::table('failed_implants')->where('id', $id)->update([
                'user_id' => $user->id,
                'implant_code' => $input['implant_code'],
                'lot' => $input['lot'],
                'order_type' => $input['order_type'],  // new
                'order_number' => $input['order_number'],  // new
                'order_item' => ($input['order_type']=='off')?$input['product_name']:'',
                'qty' => ($input['order_type']=='on')?$input['order_qty']:$input['order_qty_offline'],
                'replace_qty' => $input['replace_qty'] ?? 0,
                'replace_offline_qty' => $input['replace_offline_qty'] ?? 0,
                'bone_type' => '',
                'order_item_id' => intval($input['order_item']),
                'flapless' => false,
                'immediate_implant' => false,
                'immediate_load' => false,
                'implant_date' => '',
                'implant_removal_date' => '',
                'remove_location' => '',
                'reason' => '',
                'other_reason_failure' => '',
                'patient_age' => 0,
                'is_lessper'=> 2,
                'gender' => '',
                'item_status' => $input['item_status'],
                'replacement_product_id'=> ($input['order_type']=='on')?$input['replacement_item_online']:$input['replacement_item_offline'],
                'replacement_reason' => $input['replacement_reason'],
                'normal_history' => false,
                'smoker_history' => false,
                'hypertension_history' => false,
                'cardiac_problems_history' => false,
                'diabetes_history' => false,
                'alcoholism_history' => false,
                'trauma_history' => false,
                'cancer_history' => false,
                'others' => '',
                'xray_implant' => '',
                'before_implant_removed' => '',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')]);

        $admins = DB::table('users')->whereIn('role_id', [1, 4])->where('is_deleted', 0)->get();

        $orders = $this->orders->findWhere([
            'customer_id' => $this->user_details->id,
        ])->whereIn('status', ['completed', 'delivered'])->sortByDesc('id');

        foreach ($admins as $key => $admin) {
             Mail::send('shop::emails.customer.failed-implants', compact('input', 'orders'), function ($message) use ($input, $admin){
                 $message->from('info@genxtimplants.com', 'GenXT');
                 $message->to($admin->email)->subject('Return Implants');
             });
        }

        Mail::send('shop::emails.customer.failed-implants-customer', compact('input', 'orders', 'user'), function ($message) use ($input , $user){
                 $message->from('info@genxtimplants.com', 'GenXT');
                 $message->to($user->email)->subject('Return Implants');
        });

        Session::flash('success','Return details posted successfully');

        return Redirect::route('failure.implants.index');
    }


	public function usedstore(Request $request)
	{
        //dd($request->all());
        $input=$request->all();
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        
        if(empty($user)){
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
        }
        
        if($input['formtype']==1){
            $input['reason']=implode(', ', $input['reason']);            

            $xray_implant=$request->file('xray_implant');
            $before_implant_removed=$request->file('before_implant_removed');

            if((isset($xray_implant) && $xray_implant->getClientOriginalName()) &&
                (isset($before_implant_removed) && $before_implant_removed->getClientOriginalName())){
                //random name
                $rand = date('Y-m-d-His').rand(1, 9999);
                //Generate File name
                $xray_implant_name = pathinfo($xray_implant->getClientOriginalName(), PATHINFO_FILENAME).'-'.$rand.'.'.$xray_implant->getClientOriginalExtension();
                $before_implant_removed_name = pathinfo($before_implant_removed->getClientOriginalName(), PATHINFO_FILENAME).'-'.$rand.'.'.$before_implant_removed->getClientOriginalExtension();
                //Store file
                $xray_implant->storeAs('failed_implants\xray_implant',$xray_implant_name);
                $before_implant_removed->storeAs('failed_implants\before_implant_removed',$before_implant_removed_name);
            }else{
                $xray_implant_name = '';
                $before_implant_removed_name = '';
            }

            DB::table('failed_implants')->insert([
                    'user_id' => $user->id,
                    'implant_code' => $input['implant_code'],
                    'lot' => $input['lot'],
                    'order_type' => $input['order_type'],  // new
                    'order_number' => $input['order_number'],  // new
                    'order_item' => ($input['order_type']=='off')?$input['product_name']:'',
                    'qty' => ($input['order_type']=='on')?$input['order_qty']:$input['order_qty_offline'],
                    'replace_qty' => $input['replace_qty'] ?? 0,
                    'replace_offline_qty' => $input['replace_offline_qty'] ?? 0,
                    'bone_type' => $input['bone_type'],
                    'order_item_id' => intval($input['order_item']),
                    'flapless' => (($input['flapless']=='Yes')?true:false),
                    'immediate_implant' => (($input['immediate_implant']=='Yes')?true:false),
                    'immediate_load' => (($input['immediate_load']=='Yes')?true:false),
                    'implant_date' => $input['implant_date'],
                    'implant_removal_date' => $input['implant_removal_date'],
                    'remove_location' => $input['remove_location'],
                    'reason' => $input['reason'],
                    'other_reason_failure' => $input['other_reason_failure'],
                    'patient_age' => $input['age'],
                    'is_lessper'=> $input['formtype'],
                    'gender' => $input['gender'],
                    'item_status' => $input['item_status'],
                    'replacement_reason' => $input['replacement_reason'],
                    'replacement_product_id'=> ($input['order_type']=='on')?$input['replacement_item_online']:$input['replacement_item_offline'],
                    'normal_history' => (($input['normal_history']=='Yes')?true:false),
                    'smoker_history' => (($input['smoker_history']=='Yes')?true:false),
                    'hypertension_history' => (($input['hypertension_history']=='Yes')?true:false),
                    'cardiac_problems_history' => (($input['cardiac_problems_history']=='Yes')?true:false),
                    'diabetes_history' => (($input['diabetes_history']=='Yes')?true:false),
                    'alcoholism_history' => (($input['alcoholism_history']=='Yes')?true:false),
                    'trauma_history' => (($input['trauma_history']=='Yes')?true:false),
                    'cancer_history' => (($input['cancer_history']=='Yes')?true:false),
                    'others' => $input['others'],
                    'xray_implant' => $xray_implant_name,
                    'before_implant_removed' => $before_implant_removed_name,
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')]);

            $admins = DB::table('users')->whereIn('role_id', [1, 4])->where('is_deleted', 0)->get();

            $orders = $this->orders->findWhere([
                'customer_id' => $this->user_details->id,
            ])->whereIn('status', ['completed', 'delivered'])->sortByDesc('id');

            foreach ($admins as $key => $admin) {
                Mail::send('shop::emails.customer.failed-implants', compact('input', 'orders'), function ($message) use ($input, $xray_implant, $before_implant_removed, $admin){
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($admin->email)->subject('Failure Implants');
                    $message->attach($xray_implant->getRealPath(), array('as' => $xray_implant->getClientOriginalName(),'mime' => $xray_implant->getMimeType()));
                    $message->attach($before_implant_removed->getRealPath(), array('as' => $before_implant_removed->getClientOriginalName(),'mime' => $before_implant_removed->getMimeType()));
                });
            }


            Mail::send('shop::emails.customer.failed-implants-customer', compact('input', 'orders', 'user'), function ($message) use ($input, $xray_implant,$before_implant_removed,$user){
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($user->email)->subject('Failure Implants');
                    $message->attach($xray_implant->getRealPath(), array('as' => $xray_implant->getClientOriginalName(),'mime' => $xray_implant->getMimeType()));
                    $message->attach($before_implant_removed->getRealPath(), array('as' => $before_implant_removed->getClientOriginalName(),'mime' => $before_implant_removed->getMimeType()));

            });

        }else{
            DB::table('failed_implants')->insert([
                'user_id' => $user->id,
                'implant_code' => $input['implant_code'],
                'lot' => $input['lot'],
                'order_type' => $input['order_type'],  // new
                'order_number' => $input['order_number'],  // new
                'order_item' => ($input['order_type']=='off')?$input['product_name']:'',
                'qty' => ($input['order_type']=='on')?$input['order_qty']:$input['order_qty_offline'],
                'replace_qty' => $input['replace_qty'] ?? 0,
                'replace_offline_qty' => $input['replace_offline_qty'] ?? 0,
                'bone_type' => '',
                'order_item_id' => intval($input['order_item']),
                'flapless' => false,
                'immediate_implant' => false,
                'immediate_load' => false,
                'implant_date' => '',
                'implant_removal_date' => '',
                'remove_location' => '',
                'reason' => '',
                'other_reason_failure' => '',
                'patient_age' => 0,
                'is_lessper'=> $input['formtype'],
                'gender' => '',
                'item_status' => $input['item_status'],
                'replacement_product_id'=> ($input['order_type']=='on')?$input['replacement_item_online']:$input['replacement_item_offline'],
                'replacement_reason' => $input['replacement_reason'],
                'normal_history' => false,
                'smoker_history' => false,
                'hypertension_history' => false,
                'cardiac_problems_history' => false,
                'diabetes_history' => false,
                'alcoholism_history' => false,
                'trauma_history' => false,
                'cancer_history' => false,
                'others' => '',
                'xray_implant' => '',
                'before_implant_removed' => '',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')]);

            $admins = DB::table('users')->whereIn('role_id', [1, 4])->where('is_deleted', 0)->get();

            $orders = $this->orders->findWhere([
                'customer_id' => $this->user_details->id,
            ])->whereIn('status', ['completed', 'delivered'])->sortByDesc('id');

            foreach ($admins as $key => $admin) {
                Mail::send('shop::emails.customer.failed-implants', compact('input', 'orders'), function ($message) use ($input, $admin){
                    $message->from('info@genxtimplants.com', 'GenXT');
                    $message->to($admin->email)->subject('Failure Implants');
                });
            }

            Mail::send('shop::emails.customer.failed-implants-customer', compact('input', 'orders', 'user'), function ($message) use ($input , $user){
                $message->from('info@genxtimplants.com', 'GenXT');
                $message->to($user->email)->subject('Failure Implants');
            });        
        }	
		

        Session::flash('success','Failure details posted successfully');

        return Redirect::route('failure.implants.index');
	}
}

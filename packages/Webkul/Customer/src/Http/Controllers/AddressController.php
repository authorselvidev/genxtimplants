<?php

namespace Webkul\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Customer\Repositories\CustomerAddressRepository;
use Auth;
use DB;

/**
 * Customer controlller for the customer basically for the tasks of customers which will
 * be done after customer authenticastion.
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    protected $customer;

    protected $address;

    public function __construct(
        CustomerRepository $customer,
        CustomerAddressRepository $address
    )
    {
        $this->middleware('customer');
        $this->middleware('address');

        $this->_config = request('_config');

        $this->customer = auth()->guard('customer')->user();

        $this->address = $address;
    }

    /**
     * Address Route index page
     *
     * @return view
     */
    public function index()
    {
        return view($this->_config['view'])->with('addresses', $this->customer->addresses);
    }

    /**
     * Show the address create form
     *
     * @return view
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Create a new address for customer.
     *
     * @return view
     */
    public function store()
    {
        $data = collect(request()->input())->except('_token')->toArray();
        //dd(request()->all());
        $this->validate(request(), [
            'my_title' => 'required',
            'name' => 'required',
            'address' => 'string|required',
            'state' => 'string|required',
            'city' => 'string|required',
            'postcode' => 'required',
            'phone' => 'required'
        ]);

        $cust_id['customer_id'] = $this->customer->id;
        $data['name'] = $data['my_title'].'-'.$data['name'].'-'.$data['last_name'];
        $data = array_merge($cust_id, $data);

        if ($this->customer->addresses->count() == 0) {
            $data['default_address'] = 1;
        }
        $data['country'] = 101;
        if(request()->get('type') != null)
        {
             
            if(request()->get('type') == 'delivery') 
                $field = 'delivery_selected';
            else
                $field = 'billing_selected';
            $remove_selected = DB::table('user_addresses')
                            ->where('customer_id',auth()->guard('customer')->user()->id)
                            ->update([$field => 0]);
            $data[$field] = 1;

        }
        //dd($data['billing_selected']);
        if($this->address->create($data)) {
            session()->flash('success', trans('shop::app.customer.account.address.create.success'));
            if(request()->get('checkout') != null)
                return redirect()->route('shop.checkout.onepage.index'); 
            else
                return redirect()->route($this->_config['redirect']);
        } else {
            session()->flash('error', trans('shop::app.customer.account.address.create.error'));

            return redirect()->back();
        }
    }

    /**
     * For editing the existing addresses of current logged in customer
     *
     * @return view
     */
    public function edit($id)
    {
        $id = base64_decode($id);
        $address = $this->address->find($id);

        return view($this->_config['view'], compact('address'));
    }

    /**
     * Edit's the premade resource of customer called
     * Address.
     *
     * @return redirect
     */
    public function update($id)
    {
        //dd(request()->all());
        $id = base64_decode($id);
        $this->validate(request(), [
            'my_title' => 'required',
            'name' => 'required',
            'address' => 'string|required',
            'state' => 'string|required',
            'city' => 'string|required',
            'postcode' => 'required',
            'phone' => 'required'
        ]);

        $data = collect(request()->input())->except('_token')->toArray();
        $data['name'] = $data['my_title'].'-'.$data['name'].'-'.$data['last_name'];
        $this->address->update($data, $id);

        session()->flash('success', trans('shop::app.customer.account.address.edit.success'));
        if(request()->get('checkout') != null)
           return redirect()->route('shop.checkout.onepage.index'); 
        else
        return redirect()->route('customer.address.index');
    }

    /**
     * To change the default address or make the default address, by default when first address is created will be the default address
     *
     * @return Response
     */

    public function change(Request $request)
    {
        $addresses = $this->customer->addresses;
        return view($this->_config['view'], compact('addresses'));
    }

    public function changedAddress(Request $request)
    {
        //dd($request->all());
         $this->validate(request(), [
            'addr_type' => 'required',
            'change_address' => 'required',
            ]);
         $field = 'billing_selected';
        if($request->get('addr_type') == 'delivery') $field = 'delivery_selected';
        $remove_selected = DB::table('user_addresses')
                            ->where('customer_id',$request->get('id'))
                            ->update([$field => 0]);


        $update_address = DB::table('user_addresses')
                            ->where('customer_id',$request->get('id'))
                            ->where('id', $request->get('change_address'))
                            ->update([$field => 1]);
        //dd($field);
        return redirect()->route($this->_config['redirect']);
        
    }
    public function makeDefault($id)
    {
        $id = base64_decode($id);
        if ($default = $this->customer->default_address) {
            $this->address->find($default->id)->update(['default_address' => 0]);
        }

        if ($address = $this->address->find($id)) {
            $address->update(['default_address' => 1]);
        } else {
            session()->flash('success', trans('shop::app.customer.account.address.index.default-delete'));
        }

        return redirect()->back();
    }

    /**
     * Delete address of the current customer
     *
     * @param integer $id
     *
     * @return response mixed
     */
    public function destroy($id)
    {
        $id = base64_decode($id);
        $this->address->delete($id);

        session()->flash('success', trans('shop::app.customer.account.address.delete.success'));

        return redirect()->back();
    }
}

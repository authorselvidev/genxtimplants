<?php

namespace Webkul\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Webkul\Customer\Mail\VerificationEmail;
use Webkul\Customer\Mail\WaitingForApproval;
use Webkul\Customer\Mail\UserSignup;
use Webkul\Customer\Models\Countries;
use Webkul\Customer\Models\States;
use Webkul\Customer\Models\Cities;
use Illuminate\Routing\Controller;
use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Customer\Repositories\CustomerAddressRepository;
use Cookie;
use DB;
use Hash;
use Carbon\Carbon;

/**
 * Registration controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $_config;
    protected $customer;
    protected $address;

    /**
     * @param CustomerRepository object $customer
     */
    public function __construct(CustomerRepository $customer,  CustomerAddressRepository $address)
    {
        $this->_config = request('_config');
        $this->customer = $customer;
        $this->address = $address;
    }

    /**
     * Opens up the user's sign up form.
     *
     * @return view
     */
    public function show(Request $request)
    {
        $data = [];
        if(!empty($request->dealer_invite)){
            if($this->checkInvite($request->dealer_invite)){
                $data['dealer_invite_code'] = $request->dealer_invite;
            }else{
                session()->flash('error', 'Invalid Invitation Code');

            }
        }

        if(isset($request->id)) {
            $check_temp_user = DB::table('temp_users')->where('id',$request->id)->first();

            $expire_time = Carbon::now()->subDays(3)->format("Y-m-d H:i:s");
            if(isset($check_temp_user)){
                $check_user = DB::table('users')->where('email',$check_temp_user->email)->where('is_verified',0)->where('created_at','>=',$expire_time)->first();
                //dd($expire_time,$check_user);
                if(isset($check_user))
                return redirect()->route('check.otp',$check_user->id);  
            }
            return view($this->_config['view'], $data);
        }
        // dd($this->_config['view']);
        return view($this->_config['view'], $data);
    }

    public function dealerInvite($code){
        $invite_url = '';
        if($this->checkInvite($code)){
            $invite_url = "?dealer_invite=$code";
            session()->flash('success', 'Invited by dealer');
        }else{
            session()->flash('error', 'Invalid Invitation Code');
        }
        return redirect()->to(route('customer.register.index').$invite_url);
    }

    private function checkInvite($code){
        $dealer = DB::table('users')->where('invite_code', $code)->get();
        if($dealer->count()==1){
            return true;
        }
        return false;
    }

    /**
     * Method to store user's sign up form data to DB.
     *
     * @return Mixed
     */
    public function create(Request $request)
    {
        $data = request()->input();

        if(!empty($request->user_id)){
            $temp_user = DB::table('temp_users')->where('id',$request->user_id)->first();
            $data['customer_group_id'] = $temp_user->customer_group_id ?? 4;
        }else{
            $data['customer_group_id'] = 4;
        }
        
        $already_user = DB::table('users')->where('email',$request->email)->orWhere('phone_number',$request->phone_number)->first();

        session()->forget(['already_exists_user', 'is_phone_exists', 'is_email_exists', 'is_phone_email_exists']);

        if(isset($already_user)){
            DB::table('temp_users')->where('email', $already_user->email)->delete();
            if($already_user->is_verified == 0){
                $user = DB::table('users')->where('id', $already_user->id);
                session()->put('already_exists_user', true);
                
                if($already_user->email != $request->email){
                    $user->update(['email' => $request->email]);
                    session()->put('is_phone_exists', true);

                }elseif($already_user->phone_number != $request->phone_number){
                    $user->update(['phone_number' => $request->phone_number]);
                    session()->put('is_email_exists', true);

                }else{
                    session()->put('is_phone_email_exists', true);
                }

                session()->flash('success', 'Account already exists'); 
                return redirect()->route('check.otp',$already_user->id);
            }
        }

        $rules = ['my_title' => 'required',
            'first_name' => 'string|required',
            'email' => 'email|required|unique:users,email',
            'phone_number' => 'required|digits:10|numeric|unique:users,phone_number',
            'dental_license_no' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'clinic_name' => 'required',
            'clinic_address' => 'required',
            'pin_code' => 'required'
        ];

        $data['role_id'] = 3;
        $request->validate($rules);

        $data['channel_id'] = core()->getCurrentChannel()->id;
        $data['country_id'] = 101;
        //$data['is_verified'] = 1;

        //bronze group = 4
        //$data['customer_group_id'] = 4;

        //$temp_user = DB::table('temp_users')->where('id',$request->user_id)->first();
        if(!empty($request->invite_code)){
            if($this->checkInvite($request->invite_code)){
                $dealer = DB::table('users')->where('invite_code', $request->invite_code)->first();
                $data['dealer_id'] = $dealer->id;
            }else{
                session()->flash('error','Invalid Invite code');
                return redirect()->back();
            }

        }

        if(isset($temp_user) && isset($temp_user->dealer_id)){
            $data['dealer_id'] = $temp_user->dealer_id;
            $data['created_by'] = $temp_user->created_by;
            $discount_id = $temp_user->discount_id;
        }

        $verificationData['email'] = $data['email'];
        $verificationData['token'] = md5(uniqid(rand(), true));
        $data['token'] = $verificationData['token'];

        $otp_number = rand(10,1000000);
        $data['verify_otp'] = $otp_number;
        $otp_data = array();
        $otp_message = 'Your OTP for genxtimplants.com is: '.$otp_number;
        $otp_data['sms_numbers'] = '91'.$data['phone_number'];

        $otp_data['sms_message'] = $otp_message;

        $sent_sms=view('shop::send-sms',$otp_data)->render();
        //dd($sent_sms);
        Event::fire('customer.registration.before');
        
        $customer = $this->customer->create($data);
        //dd($request->all());

        $data['role_id'] = 'Doctor';

        $data['country_name'] = Countries::GetCountryName($data['country_id']);
        $data['state_name'] = States::GetStateName($data['state_id']);
        $data['city_name'] = Cities::GetCityName($data['city_id']);
        $full_name = $data['first_name'].' '.$data['last_name'];
        Event::fire('customer.registration.after', $customer);

        if ($customer) {

            $address_data=array();
            $address_data['customer_id'] = $customer->id;
            $address_data['name'] = $customer->my_title.'-'.$data['first_name'].'-'.$data['last_name'];
            $address_data['address'] = $data['clinic_address'];
            $address_data['phone'] = $data['phone_number'];
            $address_data['state'] = $data['state_id'];
            $address_data['city'] = $data['city_id'];
            $address_data['postcode'] = $data['pin_code'];
            $address_data['default_address'] = 1;
            
            $address = $this->address->create($address_data);


            if(isset($discount_id)){
                 DB::table('discount_users')->where('discount_id',$discount_id)
                                            ->update(['user_id' => $customer->id]);
            }
            
                //session()->flash('success', trans('shop::app.customer.signup-form.success'));

                //Mail::send(new VerificationEmail($verificationData));
               /* $admins = DB::table('users')->where('role_id',1)->where('status',1)->get();
                foreach($admins as $key =>$admin)
                {
                    $mail_sent = Mail::to($admin->email)->send(new UserSignup($data));   
                }*/
                
           
            session()->flash('success', 'Please enter the OTP'); 
            return redirect()->route('check.otp',$customer->id);
        } else {
            session()->flash('error', trans('shop::app.customer.signup-form.failed'));

            return redirect()->back();
        }
    }
    
    public function checkOtp($customer_id)
    {
        $data = array();
        $customer_details = DB::table('users')->where('id',$customer_id)->first();

        if(!$customer_details){
            return redirect()->route('customer.register.index');
        }        

        return view('shop::customers.signup.verify-otp',[
            'customer_details' => $customer_details,
        ]);    
    }

    public function verifyOtp(Request $request)
    {

        $input = request()->input();

        $check = DB::table('users')->where('phone_number',$input['phone_number'])->first();
        //dd($input['verify_otp'],$check->verify_otp);
        $data['my_title']          = $check->my_title;
        $data['first_name']        = $check->first_name;
        $data['last_name']         = $check->last_name ;
        $data['email']             = $check->email;
        $data['phone_number']      = $check->phone_number; 
        $data['pin_code']          = $check->pin_code; 
        $data['clinic_name']       = $check->clinic_name;
        $data['clinic_address']    = $check->clinic_address;
        $data['clinic_number']     = $check->clinic_number;
        $data['dental_license_no'] = $check->dental_license_no;

        $data['country_name'] = Countries::GetCountryName($check->country_id);
        $data['state_name'] = States::GetStateName($check->state_id);
        $data['city_name'] = Cities::GetCityName($check->city_id);
        
        if($input['verify_otp']==$check->verify_otp) {
            $update = DB::table('users')
                                ->where('id',$check->id)
                                ->update(['is_verified' => 1, 'token' => NULL, 'verify_otp' => NULL, 'prepayment_available' => 1]);
            $account_info=array();
            $message = 'Hello Dr '.$check->first_name.', your details are successfully submitted to GenXT WebX. We will let you know once your account is active.';
            $account_info['sms_numbers'] = '91'.$check->phone_number;
            $account_info['sms_message'] = $message;
            $sent_sms=view('shop::send-sms',$account_info)->render();
            DB::table('temp_users')->where('email',$check->email)->delete();

            Mail::to($check->email)->send(new WaitingForApproval($check->first_name));


            $admins = DB::table('users')->where('role_id',1)->where('status',1)->get();
            foreach($admins as $key =>$admin)
            {
                $mail_sent = Mail::to($admin->email)->send(new UserSignup($data));   
            }
            session()->flash('success', 'Thank you for signing up! GenXT will approve you soon! You will be notified.');
            return redirect()->route('customer.register.success');
        }
        else{
            session()->flash('error', 'Your OTP is invalid');
            return redirect()->back();            
        }
        
    }

    public function changeOtp(Request $request, $id)
    {   

        $customer = DB::table('users')->where('id',$id)->first();
        $otp_number = rand(10,1000000);        
        $otp_message = 'Your OTP for genxtimplants.com is: '.$otp_number;
        $otp_data = array();
        $otp_data['sms_numbers'] = $customer->phone_number;
        $otp_data['sms_message'] = $otp_message;     
        $sent_sms=view('shop::send-sms',$otp_data)->render();
        if($sent_sms)
        {
            $update = DB::table('users')
                                ->where('id',$id)
                                ->update(['verify_otp' => $otp_number ]);
            session()->flash('success', 'OTP Resend Successfully');

            session()->put('otp_resend', true);
        }else{
            session()->flash('error', 'Something went wrong. Please try again later');
        }

        return redirect()->route('check.otp',$id);
    }

    /**
     * Method to verify account
     *
     * @param string $token
     */
    public function verifyAccount($id)
    {
        $customer = $this->customer->findOneByField('id', $id);

        if ($customer) {
            $customer->update(['is_verified' => 1, 'token' => NULL]);

            session()->flash('success', 'You have been verfied. Just wait for a admin to approve you!');
        } else {
            session()->flash('warning', trans('shop::app.customer.signup-form.verify-failed'));
        }

        return redirect()->route('customer.session.index');
    }

    public function resendVerificationEmail($email)
    {

        $customer = $this->customer->findOneByField('email', $email);
        $token = md5(uniqid(rand(), true));

        $this->customer->update(['token' => $token], $customer->id);

        $verificationData = [
            'id' => $customer->id,
            'email' => $customer->email,
            'created_by' => $customer->created_by,
            'first_name' => $customer->first_name,
            'token' => $token
        ];

        Mail::send(new VerificationEmail($verificationData));
        try {            

            if (Cookie::has('enable-resend')) {
                \Cookie::queue(\Cookie::forget('enable-resend'));
            }

            if (Cookie::has('email-for-resend')) {
                \Cookie::queue(\Cookie::forget('email-for-resend'));
            }
        } catch(\Exception $e) {
            session()->flash('success', trans('shop::app.customer.signup-form.verification-not-sent'));

            return redirect()->back();
        }
        session()->flash('success', trans('shop::app.customer.signup-form.verification-sent'));

        return redirect()->back();
    }

    public function signupSuccess()
    {
         return view($this->_config['view']); 
    }

    public function GetCityList(Request $request)
    {
       $cities = DB::table("country_state_cities")
            ->where("state_id",$request->state_id)
            ->pluck("name","id");

            return response()->json($cities);
    }

    public function SetUserPassword(Request $request, $token)
    {
        $get_user = DB::table('users')->where('token',$token)->first();
        if($get_user)
            return view($this->_config['view'],compact('get_user'));
        else
            return 'Your not a valid user';

    }

    public function StoreUserPassword(Request $request, $token){
       $rules = [
            'user_id' => 'required',
            'email' => 'required|email|exists:users',
            'password' => 'required|confirmed'
        ];

        $request->validate($rules);
        $user_info = DB::table('users')->where('email', $request->get('email'))->first();
        $update_pwd = DB::table('users')->where('email', $request->get('email'))->update([
                                                        'token' => null,
                                                        'password' => Hash::make($request->get('password'))                                            ]);
        if($update_pwd)
        {
            auth()->guard('customer')->logout();
            auth()->guard('admin')->logout();
            return redirect()->route($this->_config['redirect']);
        }
    }
}

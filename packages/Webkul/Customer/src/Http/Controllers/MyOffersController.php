<?php

namespace Webkul\Customer\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Product\Repositories\ProductReviewRepository as ProductReview;
use Webkul\Customer\Models\CustomerGroup;

use Redirect;
use Session;
use DB;
/**
 * Home page controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
 class MyOffersController extends controller{
	protected $_config;
    public function __construct(
        CustomerRepository $customer,
        ProductReview $productReview
    ){
        $this->_config = request('_config');
        $this->customer = $customer;
    }

    public function index()
	{
		$expired_promo_ids = array();
		$data['used_promo'] = array();
		$promo_used = \DB::table('orders')
						->where('customer_id', auth()->guard('customer')->user()->id)
						->where('promocode_discount_id', '!=', '0')
						->select(DB::raw('count(*) as count, promocode_discount_id'))->groupBy('promocode_discount_id')->get();
		
		$user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
		if(empty($user))
			$user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

		foreach($promo_used as $promo){
			$promo_dis = DB::table('discounts')->where('id', $promo->promocode_discount_id)->first();

			if($promo_dis->discount_usage_limit<=$promo->count){
				array_push($expired_promo_ids, $promo_dis->id);
			}else{
				$data['used_promo'][$promo_dis->id] = $promo->count;
			}

		}		
		
		$data['promo_codes'] = DB::table('discounts as ds')	
									->join('promo_codes as pc', 'ds.id','=','pc.discount_id')
									->where('ds.discount_type',3)
									->where('ds.active',1)
									->whereNotIn('ds.id', $expired_promo_ids)
									->where('ds.product_id', null)
									->where('ds.is_deleted',0)
									->where('pc.for_whom',$user->role_id)									
									->addSelect('ds.id','ds.discount_title','ds.discount_description','ds.minimum_order_amount','ds.coupon_code', 'ds.discount_usage_limit')
									->orderBy('ds.minimum_order_amount')->paginate(4);

		$member_ship = DB::table('discounts')
									->where('discount_type',2)
									->where('product_id',null)
									->where('is_deleted',0)
									->addSelect('id', 'discount_title', 'discount_description', 'customer_group_id', 'minimum_order_amount', 'coupon_code')->get();

		$newarray = array();
		foreach ($member_ship as $key => $value) {
			$arr = explode('-', CustomerGroup::CustomerGroupAmountRange($value->customer_group_id));
			$newarray[$key] =  array("id" => $value->id,
									 "discount_title"=> $value->discount_title,
									 "discount_description" => $value->discount_description,
									 "customer_group_id" => $value->customer_group_id,
									 "minimum_order_amount" => $value->minimum_order_amount,
									 "coupon_code" => $value->coupon_code,
									 "minval" =>  intval(preg_replace('/[^0-9]/', '', $arr[0])),
									 "maxval" => intval(preg_replace('/[^0-9]/', '', $arr[1])));
		}

		usort($newarray, function($a,$b){ return $a['minval']-$b['minval'];});
        $data['member_ship'] = $newarray;
        $data['customer'] = $this->customer->find(auth()->guard('customer')->user()->id);
		
        return view($this->_config['view'], $data);
	}

}
<?php

namespace Webkul\Customer\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Sales\Repositories\OrderRepository;
use Illuminate\Support\Facades\Storage;
use Mail;
use Redirect;
use Session;
use PDF;
use DB;


 class FailureImplantsController extends controller{
	protected $_config;

    public function __construct(OrderRepository $orders){
        $this->_config = request('_config');
        $this->orders = $orders;
        $this->user_details = auth()->guard('customer')->user();
    }

    public function index(){
        $data["failure_implants"] = DB::table('failed_implants')
                            ->where('is_deleted', 0)
                            ->where('user_id', $this->user_details->id)                            
                            ->latest()->get();       
                
        return view($this->_config['view'], $data);
    }

    public function show($id){
        $data['failed_implant'] = DB::table('failed_implants')->where('id', $id)->where('is_deleted', 0)->first();
        $data['return_implants_return'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->where('type',1)->get();        
        $data['return_implants_replace'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->where('type',2)->get();        
        $data['user'] = DB::table('users')->where('id', $data['failed_implant']->user_id)->first();

        $data['product'] = \DB::table('product_flat')
                            ->where('product_id', $data['failed_implant']->replacement_product_id)
                            ->first();
        return view($this->_config['view'], $data);
    }

    public function create(){
        $data=array();
        $orders = $this->orders->findWhere([
            'customer_id' => $this->user_details->id,
        ])->whereIn('status', ['completed','delivered'])->sortByDesc('id');

        $products = \DB::table('product_flat')
                ->leftjoin('products','products.id','product_flat.product_id')
                ->leftjoin('product_inventories','product_inventories.product_id','product_flat.product_id')
                ->where('products.is_deleted',0)
                ->get();

        $user_details = $this->user_details;
        return view($this->_config['view'], compact('user_details', 'orders', 'products'));
    }



    public function edit($id){
        $data['failed_implant'] = DB::table('failed_implants')->where('id', $id)->where('is_deleted', 0)->first();
        $data['return_implants_items'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->get();

         $data['orders'] = $this->orders->findWhere([
            'customer_id' => $this->user_details->id,
        ])->whereIn('status', ['completed', 'delivered'])->sortByDesc('id');

        $user_details = $this->user_details;
        $data['user_details'] = $this->user_details;

        $data['products'] = \DB::table('product_flat')
                ->leftjoin('products','products.id','product_flat.product_id')
                ->leftjoin('product_inventories','product_inventories.product_id','product_flat.product_id')
                ->where('products.is_deleted',0)
                ->get();

        return view($this->_config['view'], $data);
    }

    public function store(Request $request){
        //dd($request->all());
        $input=$request->all();
        $data = array();

        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        $implant_form = DB::table('failed_implants')->insertGetId([
                'user_id' => $user->id,
                'implant_code' => $input['implant_code']  ?? '',                
                'order_type' => $input['order_type'],  // new
                'order_number' => '',  // new
                'order_item' => '',
                // 'qty' => ($input['order_type']=='on')?$input['order_qty']:$input['order_qty_offline'],
                // 'replace_qty' => $input['replace_qty'] ?? 0,
                // 'replace_offline_qty' => $input['replace_offline_qty'] ?? 0,
                'qty' => 0,
                'replace_qty' => 0,
                'replace_offline_qty' => 0,                
                'bone_type' => '',
                'order_item_id' => 0,
                'flapless' => false,
                'immediate_implant' => false,
                'immediate_load' => false,
                'implant_date' => '',
                'implant_removal_date' => '',
                'remove_location' => '',
                'reason' => '',
                'other_reason_failure' => '',
                'patient_age' => 0,
                'is_lessper'=> 0,
                'gender' => '',
                'item_status' => $input['item_status'],
                'replacement_product_id'=> 0,
                'replacement_reason' => $input['replacement_reason'],
                'normal_history' => false,
                'smoker_history' => false,
                'hypertension_history' => false,
                'cardiac_problems_history' => false,
                'diabetes_history' => false,
                'alcoholism_history' => false,
                'trauma_history' => false,
                'cancer_history' => false,
                'others' => '',
                'xray_implant' => '',
                'before_implant_removed' => '',
                'lot' => $input['lot'] ?? '',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')]);        

        if($request->order_type == "on"){
            foreach($request->return_item as $item_data){                
                $data[] = [ 
                    'failed_implants_id' => $implant_form,
                    'item' => $item_data["item"],
                    'order' => $item_data['order'],
                    'quantity' => $item_data["qty"],                
                    'type' => 1
                ];
            }
    
            foreach($request->replace_item as $item_data){
                $data[] = [
                    'failed_implants_id' => $implant_form,
                    'item' => $item_data["item"],                    
                    'order' => '',
                    'quantity' => $item_data["qty"],                
                    'type' => 2
                ];            
            }
        }else if($request->order_type == "off"){
            foreach($request->replace_item_offline as $item_data){
                $data[] = [ 
                    'failed_implants_id' => $implant_form,
                    'item' => $item_data["item"]  ?? '',
                    'order' => $item_data['order']  ?? '',
                    'quantity' => $item_data["qty"],                
                    'type' => 1
                ];
            }
    
            foreach($request->return_item_offline as $item_data){
                $data[] = [
                    'failed_implants_id' => $implant_form,
                    'item' => $item_data["item"]  ?? '',                    
                    'order' => '',
                    'quantity' => $item_data["qty"],                
                    'type' => 2
                ];            
            }
        }
        
        DB::table('return_implants_items')->insert($data);
        $admins = DB::table('users')->whereIn('role_id', [1, 4])->where('is_deleted', 0)->get();        

        foreach ($admins as $key => $admin) {
            Mail::send('shop::emails.customer.failed-implants', compact('input', 'user'), function ($message) use ($input, $admin){
                $message->from('info@genxtimplants.com', 'GenXT');
                $message->to($admin->email)->subject('Return Implants');
            });
        }
        
        
        $datapdf['failed_implant'] =DB::table('failed_implants')
                            ->where('id', $implant_form)->where('is_deleted', 0)
                            ->first();
        $datapdf['return_implants_return'] = DB::table('return_implants_items')->where('failed_implants_id', $implant_form)->where('type',1)->get();        
        $datapdf['return_implants_replace'] = DB::table('return_implants_items')->where('failed_implants_id', $implant_form)->where('type',2)->get();
        $datapdf['user'] = $user; 


        $pdf = PDF::loadView('shop::customers.account.failure_implants.pdf', $datapdf)->setPaper('a4');

        $input['is_customer'] = true;
        
        Mail::send('shop::emails.customer.failed-implants', compact('input', 'user'), function ($message) use ($input, $user, $pdf){
            $message->from('info@genxtimplants.com', 'GenXT');
            $message->to($user->email)->subject('Return Implants');
            $message->attachData($pdf->output(), 'return-implant.pdf');
        });

        session()->flash('returncreated', 'check');

        Session::flash('success','Return details posted successfully');

        return Redirect::route('failure.implants.index');
    }

    public function update(Request $request, $id){        
        $input=$request->all();
        $data = array();
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";
        DB::table('failed_implants')->where('id', $id)->update([
                'user_id' => $user->id,
                'implant_code' => $input['implant_code'] ?? '',                
                'order_type' => $input['order_type'],  // new
                'order_number' => '',  // new
                'order_item' => '',
                // 'qty' => ($input['order_type']=='on')?$input['order_qty']:$input['order_qty_offline'],
                // 'replace_qty' => $input['replace_qty'] ?? 0,
                // 'replace_offline_qty' => $input['replace_offline_qty'] ?? 0,
                'qty' => 0,
                'replace_qty' => 0,
                'replace_offline_qty' => 0,                
                'bone_type' => '',
                'order_item_id' => 0,
                'flapless' => false,
                'immediate_implant' => false,
                'immediate_load' => false,
                'implant_date' => '',
                'implant_removal_date' => '',
                'remove_location' => '',
                'reason' => '',
                'other_reason_failure' => '',
                'patient_age' => 0,
                'is_lessper'=> 0,
                'gender' => '',
                'item_status' => $input['item_status'],
                'replacement_product_id'=> 0,
                'replacement_reason' => $input['replacement_reason'],
                'normal_history' => false,
                'smoker_history' => false,
                'hypertension_history' => false,
                'cardiac_problems_history' => false,
                'diabetes_history' => false,
                'alcoholism_history' => false,
                'trauma_history' => false,
                'cancer_history' => false,
                'others' => '',
                'xray_implant' => '',
                'before_implant_removed' => '',
                'lot' => $input['lot'] ?? '',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')]);        

        if($request->order_type == "on"){
            foreach($request->return_item as $item_data){                
                $data[] = [ 
                    'failed_implants_id' => $id,
                    'item' => $item_data["item"],
                    'order' => $item_data['order'],
                    'quantity' => $item_data["qty"],                
                    'type' => 1
                ];
            }
    
            foreach($request->replace_item as $item_data){
                $data[] = [
                    'failed_implants_id' => $id,
                    'item' => $item_data["item"],                    
                    'order' => '',
                    'quantity' => $item_data["qty"],                
                    'type' => 2
                ];            
            }
        }else if($request->order_type == "off"){
            foreach($request->replace_item_offline as $item_data){
                $data[] = [ 
                    'failed_implants_id' => $id,
                    'item' => $item_data["item"]  ?? '',
                    'order' => $item_data['order'] ?? '',
                    'quantity' => $item_data["qty"],                
                    'type' => 1
                ];
            }
    
            foreach($request->return_item_offline as $item_data){
                $data[] = [
                    'failed_implants_id' => $id,
                    'item' => $item_data["item"]  ?? '',                    
                    'order' => '',
                    'quantity' => $item_data["qty"],                
                    'type' => 2
                ];            
            }
        }

        $input['is_customer'] = true;

        DB::table('return_implants_items')->where('failed_implants_id', $id)->delete();
        DB::table('return_implants_items')->insert($data);
        $admins = DB::table('users')->whereIn('role_id', [1, 4])->where('is_deleted', 0)->get();

        foreach ($admins as $key => $admin) {
            Mail::send('shop::emails.customer.failed-implants', compact('input', 'user'), function ($message) use ($input, $admin){
                $message->from('info@genxtimplants.com', 'GenXT');
                $message->to($admin->email)->subject('Return Implants');
            });
        }


        $datapdf['failed_implant'] =DB::table('failed_implants')
                            ->where('id', $id)->where('is_deleted', 0)
                            ->first();
        $datapdf['return_implants_return'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->where('type',1)->get();        
        $datapdf['return_implants_replace'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->where('type',2)->get();
        $datapdf['user'] = $user; 


        $pdf = PDF::loadView('shop::customers.account.failure_implants.pdf', $datapdf)->setPaper('a4');
        
        Mail::send('shop::emails.customer.failed-implants', compact('input', 'user'), function ($message) use ($input, $user, $pdf){
            $message->from('info@genxtimplants.com', 'GenXT');
            $message->to($user->email)->subject('Return Implants');
            $message->attachData($pdf->output(), 'failed-implant.pdf');
        });

        Session::flash('success','Return details updated successfully');

        return Redirect::route('failure.implants.index');
    }

    public function destroy($id){
        DB::table('return_implants_items')->where('failed_implants_id', $id)->delete();
        DB::table('failed_implants')->where('id', $id)->update(['is_deleted'=>1]);        
        return redirect()->route('failure.implants.index');
    }

    public function print($id){
        $data['failed_implant'] = DB::table('failed_implants')->where('id', $id)->where('is_deleted', 0)->first();
        $data['return_implants_return'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->where('type',1)->get();        
        $data['return_implants_replace'] = DB::table('return_implants_items')->where('failed_implants_id', $id)->where('type',2)->get();        
        $data['user'] = DB::table('users')->where('id', $data['failed_implant']->user_id)->first();
        
        $pdf = PDF::loadView('shop::customers.account.failure_implants.pdf', $data)->setPaper('a4');
        return $pdf->download('return_implants-' . date('d-m-Y', strtotime($data['failed_implant']->created_at)).'-'.rand(1, 9999). '.pdf');
    }


    // public function download(Request $data){
    //     if($data->type == 'before'){
    //         $url="failed_implants/xray_implant/".$data->file;
    //     }else if($data->type == 'after'){
    //         $url="failed_implants/before_implant_removed/".$data->file;
    //     }else{
    //         return "Invalid request";
    //     }

    //     if(Storage::exists($url)){
    //         return Storage::download($url);
    //     }else{
    //         Session::flash('error','File not found on the server');
    //         return Redirect::back();
    //     }
    // }    
}

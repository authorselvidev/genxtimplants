<?php

namespace Webkul\Customer\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Redirect;
use Session;
use DB;

/**
 * Home page controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
 class WebinarController extends controller{

    public function __construct(){
        $this->_config = request('_config');
    }

    private function generateZoomToken(){
        $key = env('ZOOM_API_KEY', '');
        $secret = env('ZOOM_API_SECRET', '');
        $payload = [
            'iss' => $key,
            'exp' => strtotime('+5 minute'),
        ];
        return \Firebase\JWT\JWT::encode($payload, $secret, 'HS256');
    }

    private function zoomGetUsers()
    {
        $url =  env('ZOOM_API_URL', '');
        $jwt = $this->generateZoomToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url."users/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".$jwt,
            "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          dd("cURL Error #:" . $err);
        }

        return json_decode($response);
    }

    private function zoomGet($zoom_user_id, $type='m')
    {
        $url =  env('ZOOM_API_URL', '');
        if($type=='m'){
            $api_url = $url."users/".$zoom_user_id."/meetings";
        }else if($type=='w'){
            $api_url = $url."users/".$zoom_user_id."/webinars";
        }else{
            return false;
        }

        $jwt = $this->generateZoomToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".$jwt,
            "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          dd("cURL Error #:" . $err);
        }

        return json_decode($response);
    }

    private function zoomGetDetails($zoom_cat_id, $type='m')
    {
        $url =  env('ZOOM_API_URL', '');
        $jwt = $this->generateZoomToken();
        $curl = curl_init();

        $url =  env('ZOOM_API_URL', '');
        if($type=='m'){
            $api_url = $url."meetings/".$zoom_cat_id;
        }else if($type=='w'){
            $api_url = $url."webinars/".$zoom_cat_id;
        }else{
            return false;
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".$jwt,
            "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          dd("cURL Error #:" . $err);
        }

        return json_decode($response);
    }

    private function toUnixTimeStamp(string $dateTime, string $timezone)
    {
        try {
            $date = new \DateTime($dateTime, new \DateTimeZone($timezone));
            return $date->getTimestamp();
        } catch (\Exception $e) {
            Log::error('ZoomJWT->toUnixTimeStamp : ' . $e->getMessage());
            return '';
        }
    }

    private function generate_signature($meeting_number, $role){
        $api_key = env('ZOOM_API_KEY', '');
        $api_secret = env('ZOOM_API_SECRET', '');
        date_default_timezone_set("UTC");
        $time = time() * 1000 - 30000;//time in milliseconds (or close enough)
        $data = base64_encode($api_key . $meeting_number . $time . $role);
        $hash = hash_hmac('sha256', $data, $api_secret, true);
        $_sig = $api_key . "." . $meeting_number . "." . $time . "." . $role . "." . base64_encode($hash);
        //return signature, url safe base64 encoded
        return rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
    }

    public function meetings(){
        $data = array();
        $users = $this->zoomGetUsers()->users;
        foreach ($users as $user) {
            if($user->verified==1){
                $meetings = $this->zoomGet($user->id, 'm')->meetings ?? null;
                if(isset($meetings)) {
                    foreach ($meetings as $meet) {
                        $meeting = $this->zoomGetDetails($meet->id,'m');
                        $data['meetings'][$meeting->id]['topic'] = $meeting->topic;
                        if(isset($meeting->start_time))
                            $data['meetings'][$meeting->id]['start_time'] = $this->toUnixTimeStamp($meeting->start_time, $meeting->timezone);

                        $data['meetings'][$meeting->id]['description'] = $meeting->agenda ?? '';
                        $data['meetings'][$meeting->id]['status'] = $meeting->status;

                        if(isset($meeting->duration))
                            $data['meetings'][$meeting->id]['duration'] = $meeting->duration;
                        
                        $data['meetings'][$meeting->id]['created_at'] = $this->toUnixTimeStamp($meeting->created_at, $meeting->timezone);
                    }
                }
            }
        }
        return view($this->_config['view'], $data);
    }

    public function webinar(){
        $data = array();
        $users = $this->zoomGetUsers()->users;
        foreach ($users as $user) {
            if($user->verified==1){
                $webinars = $this->zoomGet($user->id, 'w')->webinars ?? null;
                if(isset($webinars)){
                    $webinars = array_reverse($webinars);
                    foreach($webinars as $wb){
                        $webinar = $this->zoomGetDetails($wb->id, 'w');
                        $start_time = null;
                        $end_time = null;

                        

                        if(isset($webinar->start_time)){
                            $start_time = $this->toUnixTimeStamp($webinar->start_time, $webinar->timezone);
                        }else if(isset($wb->start_time)){
                            $start_time = $this->toUnixTimeStamp($wb->start_time, $wb->timezone);
                        }
                        $end_time = $start_time+($webinar->duration*60);
                        if($end_time < time()) continue;

                        $data['webinars'][$webinar->id]['start_time'] = $start_time;
                        $data['webinars'][$webinar->id]['end_time'] = $end_time;

                        $data['webinars'][$webinar->id]['topic'] = $webinar->topic;
                        
                        $data['webinars'][$webinar->id]['description'] = $webinar->agenda ?? '';

                        if(isset($webinar->duration))
                            $data['webinars'][$webinar->id]['duration'] = $webinar->duration;

                        if(isset($webinar->end_times))
                            $data['webinars'][$webinar->id]['end_times'] = $webinar->end_times;

                        if(isset($webinar->password))
                            $data['webinars'][$webinar->id]['password'] = $webinar->password;

                        $type = [
                            '5' => 'webinar',
                            '6' => 'Recurring webinar with no fixed time',
                            '9' => 'Recurring webinar with fixed time',
                        ];

                        $data['webinars'][$webinar->id]['type'] = $webinar->type;
                        $data['webinars'][$webinar->id]['type_full'] = $type[$webinar->type];  

                        if($webinar->type == 9){
                            $recurrence = (array) $webinar->recurrence;
                            
                            if(isset($recurrence['end_date_time'])) {
                                $recurrence['end_date_time'] = $this->toUnixTimeStamp($recurrence['end_date_time'], $webinar->timezone);
                            }

                            if($recurrence['type']==2){
                                $ary = array_map(function($val){
                                    return ["","Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][$val];
                                }, explode(',', $recurrence['weekly_days']));
                                $recurrence['weekly_days'] = implode(', ',$ary);
                            }
                            $data['webinars'][$webinar->id]['recurrence'] = $recurrence;
                        }

                        $data['webinars'][$webinar->id]['created_at'] = $this->toUnixTimeStamp($webinar->created_at, $webinar->timezone);
                        //dd($data);
                    }
                }
            }
        }
        return view($this->_config['view'], $data);
    }

    public function liveview($webinar){
        $signature = $this->generate_signature($webinar, 0);
        $webinar = $this->zoomGetDetails($webinar,'w');

        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        $webinar_config = array(
            'name' => base64_encode($user->my_title.' '.$user->first_name.' '.$user->last_name),
            'mn' => $webinar->id,
            'email' => base64_encode($user->email),
            'pwd' => $webinar->password ?? '',
            'role' => 0,
            'lang' => "en-US",
            'signature' => $signature,
            'china' => 0,
            'apiKey' => env('ZOOM_API_KEY', '')
        );

        $data['config'] = $webinar_config;
        return view($this->_config['view'], $data);
    }

    public function ondemand(Request $Request){
    	$data['category'] = DB::table('webinars')->where('is_live', 0)->where('is_active', 1)->groupBy('category')->addSelect('category')->get();

        return view($this->_config['view'], $data);
    }
}

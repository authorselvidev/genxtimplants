<?php

namespace Webkul\Customer\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CustomerGroupTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('user_groups')->delete();

        DB::table('user_groups')->insert([
            'id' => 1,
            'name' => 'General',
            'is_user_defined' => 0,
        ], [
            'id' => 2,
            'name' => 'Wholesale',
            'is_user_defined' => 0,
        ]);
    }
}
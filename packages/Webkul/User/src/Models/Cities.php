<?php

namespace Webkul\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Cities extends Model
{
    static function GetCityName($id='')
    {
    	$city_name=DB::table('country_state_cities')->where('id',$id)->value('name');
    	return $city_name;
    }
}

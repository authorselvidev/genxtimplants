<?php

namespace Webkul\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Countries extends Model
{
   static function GetCountryName($id='')
    {
    	$country_name=DB::table('countries')->where('id',$id)->value('name');
    	return $country_name;
    }
}

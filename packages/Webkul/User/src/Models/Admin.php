<?php

namespace Webkul\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Webkul\User\Models\Role;
use Webkul\User\Notifications\AdminResetPassword;


class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = ['my_title','first_name','last_name', 'channel_id', 'phone_number', 'email', 'password','clinic_name','user_type','clinic_address','clinic_number','state_id','city_id','country_id','pin_code','dental_license_no','dealer_id','dealer_commission','is_approved','role_id','verify_otp','super_admin','customer_group_id', 'is_verified', 'token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the role that owns the admin.
     */
    
}
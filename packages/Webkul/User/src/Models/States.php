<?php

namespace Webkul\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class States extends Model
{
    static function GetStateName($id='')
    {
    	$state_name=DB::table('country_states')->where('id',$id)->value('name');
    	return $state_name;
    }
}

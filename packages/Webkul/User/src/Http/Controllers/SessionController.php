<?php

namespace Webkul\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;

/**
 * Admin user session controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class SessionController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->except(['create','store']);

        $this->_config = request('_config');

        $this->middleware('guest', ['except' => 'destroy']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->guard('admin')->check()) {
            if (auth()->guard('admin')->user()->role_id == 3){
                auth()->guard('admin')->logout();
                session()->flash('error', 'You are not a admin!');
                return view($this->_config['view']);
            }
            if(auth()->guard('admin')->user()->role_id == 4){
                return redirect()->route('admin.sales.orders.index');
            }

            $routes = [
                'default' => 'admin.dashboard.index',
                'cart' => 'shop.checkout.onepage.index'
            ];

            return redirect()->route($routes[request()->redirect ?? 'default'] ?? $routes['default']);
        } else {
            session()->put('url.intended', url()->previous());
            
            return view($this->_config['view']);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $remember = request('remember');

        if (! auth()->guard('admin')->attempt(request(['email', 'password']), $remember)) {
            session()->flash('error', 'Please check your credentials and try again.');

            return back();
        }

        if (auth()->guard('admin')->user()->status == 0) {
            session()->flash('warning', 'Your account is yet to be activated, please contact administrator.');

            auth()->guard('admin')->logout();

            return redirect()->route('admin.session.create');
        }

        if(auth()->guard('admin')->user()->role_id == 2 && auth()->guard('admin')->user()->invite_code == "0"){
            DB::table('users')->where('id', auth()->guard('admin')->user()->id)->update([
                'invite_code' => $this->genInviteCode(),
            ]);
        }

        if (auth()->guard('admin')->user()->role_id == 3) {
            session()->flash('info', 'You are not a admin!');
            return redirect()->back();
        }

        if(auth()->guard('admin')->user()->role_id == 4){
            return redirect()->route('admin.sales.orders.index');
        }

        $routes = [
            'default' => $this->_config['redirect'],
            'cart' => 'shop.checkout.onepage.index'
        ];

        return redirect()->route($routes[$request->redirect ?? 'default'] ?? $routes['default']);
    }

    private function genInviteCode(){
        $iCode = 0;
        do{
            $iCode = rand(100000,999999);
        }while(\DB::table('users')->where('invite_code', $iCode)->count()>0);        
        return $iCode;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        auth()->guard('admin')->logout();

        return redirect()->route($this->_config['redirect']);
    }
}

<?php

namespace Webkul\User\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mail;
use Redirect;
use Session;
use DB;
use Razorpay\Api\Api;

/**
 * Home page controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
 class CreditPaymentsController extends controller
{

	protected $_config;

    public function __construct()
    {
        $this->_config = request('_config');
    }

	public function pay()
	{
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        $data['credit_payment'] = DB::table('credit_payments')->where('user_id', $user->id)
                                            ->orderBy('created_at','desc')
                                            ->paginate(10);

        $data['credit_pending'] = DB::table('credit_payments')
                                    ->where('user_id', $user->id)
                                    ->where('status', 0)
                                    ->sum('amount');

        return view($this->_config['view'], $data);
	}

	public function PayViaCheque(Request $request)
	{
        $this->validate(request(), [
            'pay_credit_amount' => 'required|min:1',
        ]);
        //dd($request->all());

        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
            $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        
        
        $image_name="";
        if($request->payment_type ==2 && $request->hasfile('cheque_image')){
            $cheque_image = $request->file('cheque_image');
            $image_name=$cheque_image->getClientOriginalName();
            $image_name = str_replace(' ','_',$image_name);
            $cheque_image->move(public_path().'/themes/default/assets/images/credit_images/', $image_name);
        }else if($request->payment_type == 3 && $request->hasfile('receipt_image')){
            $transation_image = $request->file('receipt_image');
            $image_name = $transation_image->getClientOriginalName();
            $image_name = str_replace(' ','_',$image_name);
            $transation_image->move(public_path().'/themes/default/assets/images/credit_images/', $image_name);
        }



        $data = [
            'user_id' => $user->id,
            'amount' => $request->pay_credit_amount,
            'payment_type' => $request->payment_type,
            'cheque_number' => ($request->payment_type ==2 )? $request->cheque_number : $request->transation_number,
            'cheque_image' => $image_name ?? '',
            'paid_by' => $user->id,
            'status' => 0,
            'notes' => $request->notes ?? '',
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $getId = \DB::table('credit_payments')->insertGetId($data);
        $credit_payment = \DB::table('credit_payments')->where('id',$getId)->first();
        
        $admin = \DB::table('users')->where('id',2)->first();

        Mail::send('shop::emails.credit_payments.get_approvals', compact('credit_payment','admin','user'), function ($message) use($admin, $credit_payment) {
            $message->from('info@genxtimplants.com', 'GenXT');
            $subject = 'GenXT - Requested '.(($credit_payment->payment_type==2)?'Cheque':(($credit_payment->payment_type==3)?'Bank Transfer':'')).' payment for approval!';
            $message->to($admin->email)->subject($subject);
        });

        session()->flash('success', 'Please wait for the admin to approve your payment!');
        return redirect()->route('dealer.credit_payment.pay');
    }

    // RazorPay Suspended for Credit Payments Deposits
    public function PayViaRazorpay(Request $request)
    {
        
        $user = (auth()->guard('admin')->user() != null) ? auth()->guard('admin')->user() : "";
        if(empty($user))
        $user = (auth()->guard('customer')->user() != null) ? auth()->guard('customer')->user() : "";

        $input = $request->all();

        $api = new Api('rzp_live_qxiDQtOJ1uUMPT', 'SgfQvrR7F0LoD3TwhbsborAW');
        $payment = $api->payment->fetch($input['payment_id']);
        if(count($input)  && !empty($input['payment_id'])) {
            try {
                $response=$api->payment->fetch($input['payment_id'])->capture(array('amount'=>$payment['amount'])); 
            }
            catch (\Exception $e) {
                return  $e->getMessage();
                return redirect()->back();
            }
        }

        $data = [
                    'user_id' => $user->id,
                    'payment_id' => $request->payment_id,
                    'amount' => $request->amount,
                    'payment_type' => 'razorpay',
                    'paid_by' => $user->id,
                    'status' => 1,
                    'notes' => $request->notes ?? '',
                    'created_at' => date('Y-m-d H:i:s'),
                ];

        $already_paid =$user->credit_paid;
        $user->credit_paid =  $already_paid + $request->amount;
        $user->save();

        $getId = \DB::table('credit_payments')->insertGetId($data);  
        $arr = array('payment_id' => $request->payment_id, 'msg' => 'Payment successfully credited', 'status' => true);
        session()->flash('success', 'Your Payment has been made successfully!');
        return Response()->json($arr);   
    }



}

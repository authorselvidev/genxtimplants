<?php

namespace Webkul\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Webkul\User\Repositories\AdminRepository as Admin;
use Webkul\Customer\Repositories\CustomerRepository as Customer;
use Webkul\User\Repositories\RoleRepository as Role;
use Webkul\User\Http\Requests\UserForm;
use Hash;
use Carbon\Carbon;
use Webkul\Admin\Mail\AdminAccountCreation;
use Mail;
/**
 * Admin user controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class UserController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * AdminRepository object
     *
     * @var array
     */
    protected $admin;
    protected $customer;

    /**
     * RoleRepository object
     *
     * @var array
     */
    protected $role;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\User\Repositories\AdminRepository $admin
     * @param  Webkul\User\Repositories\RoleRepository $role
     * @return void
     */
    public function __construct(Admin $admin, Customer $customer, Role $role)
    {
        $this->admin = $admin;
        $this->customer = $customer;

        $this->role = $role;

        $this->_config = request('_config');

        $this->middleware('guest', ['except' => 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->all();

        return view($this->_config['view'], compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Webkul\User\Http\Requests\UserForm  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->all();
        //dd($data);
         $this->validate(request(), [
            'first_name' => 'string|required',
            'email' => 'email|required|unique:users,email',
            'phone_number' => 'required|numeric',
            'password' => 'required|confirmed',
            'state_id' => 'required',
            'city_id' => 'required'
                    ]);

        if($request->get('super_admin') != null && $request->get('super_admin') == 1)
            $data['super_admin'] = 1;
        else
            $data['super_admin'] = 0;
         
        $password = $request->get('password');
        $data['password'] = Hash::make($password);    //$data['user_type'] = 1;
        $data['country_id'] = 101;
        $data['is_approved'] = 1;
        $data['role_id'] =$request->get('role_id');; //admin role
        $data['channel_id'] = core()->getCurrentChannel()->id;
        $data['is_verified'] = 1;
        $full_name = $data['first_name'].' '.$data['last_name'];
        $verificationData['token'] = md5(uniqid(rand(), true));
        $data['token'] = $verificationData['token'];
        $type="create";

        $admin_created = $this->customer->create($data);

        Mail::to($data['email'])->send(new AdminAccountCreation($full_name, $data['email'],$password,$type));







       /* if (isset($data['password']) && $data['password'])
            $data['password'] = bcrypt($data['password']);

        Event::fire('user.admin.create.before');

        $admin = $this->admin->create($data);

        Event::fire('user.admin.delete.after', $admin);*/

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Admin']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->customer->find($id);

        $roles = $this->role->all();

        return view($this->_config['view'], compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Webkul\User\Http\Requests\UserForm  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->all();
        //dd($data);
         $this->validate(request(), [
            'first_name' => 'string|required',
            'email' => 'email|required',
            'phone_number' => 'required|numeric',
            'state_id' => 'required',
            'city_id' => 'required'
                    ]);

         $password = $request->get('password');
         //dd($password);
            //$data['user_type'] = 1;
        if($request->get('super_admin') != null && $request->get('super_admin') == 1)
            $data['super_admin'] = 1;
        else
            $data['super_admin'] = 0;

        $data['country_id'] = 101;
        $data['is_approved'] = 1;
        $data['role_id'] = $request->get('role_id'); //admin role
        $data['channel_id'] = core()->getCurrentChannel()->id;
        $data['is_verified'] = 1;
        $full_name = $data['first_name'].' '.$data['last_name'];
        $type="update";
        if($password != "")
        {
            $data['password'] = Hash::make($password); 
            Mail::to($data['email'])->send(new AdminAccountCreation($full_name, $data['email'],$password,$type));
        }
        else {
            unset($data['password']);
        }

        $admin_updated = $this->customer->update($data,$id);


        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Admin']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin_count = \DB::table('users')->where('role_id',1)->sum('role_id');
        if ($admin_count == 1) {
            session()->flash('error', 'At least one admin is required.');
        }
        else if(auth()->guard('admin')->user()->id == $id){
            session()->flash('error', 'You cannot delete your own account.');
        }
        else 
        {
            $delete =  \DB::table('users')->where('id',$id)->update(['is_deleted' => 1]);
            session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Admin']));
        }
        return redirect()->back();
    }

    /**
     * destroy current after confirming
     *
     * @return mixed
     */
    public function destroySelf()
    {
        $password = request()->input('password');

        if (Hash::check($password, auth()->guard('admin')->user()->password)) {
            if ($this->admin->count() == 1) {
                session()->flash('error', trans('admin::app.users.users.delete-last'));
            } else {
                $id = auth()->guard('admin')->user()->id;

                Event::fire('user.admin.delete.before', $id);

                $this->admin->delete($id);

                Event::fire('user.admin.delete.after', $id);

                session()->flash('success', trans('admin::app.users.users.delete-success'));

                return redirect()->route('admin.session.create');
            }
        } else {
            session()->flash('warning', trans('admin::app.users.users.incorrect-password'));

            return redirect()->route($this->_config['redirect']);
        }
    }

    public function profile_index()
    {
        $user = $this->customer->find(auth()->guard('admin')->user()->id);
        
        \DB::table('reward_points')->where('user_id',$user->id)
                                   ->where('points_credited',1)
                                   ->where('updated_date','<=', Carbon::now()->subDays(365))
                                   ->update(['points_expired' => 1]);

        return view($this->_config['view'], compact('user'));
    }

      public function editIndex()
    {
        $user = $this->customer->find(auth()->guard('admin')->user()->id);

        return view($this->_config['view'], compact('user'));
    }

    public function updateProfile()
    {
        
        $id = auth()->guard('admin')->user()->id;

        $this->validate(request(), [
            'my_title' => 'required',
            'first_name' => 'string',
            'phone_number' => 'required',
            'email' => 'email|unique:customers,email,'.$id,
            'state_id' => 'required',
            'city_id' => 'required',
        ]);

        $data = collect(request()->input())->except('_token')->toArray();
        
        $data['country_id'] = 101;
        if ($data['oldpassword'] != "") {
            if(Hash::check($data['oldpassword'], auth()->guard('customer')->user()->password)) {
                $data['password'] = Hash::make($data['password']);
            } else {
                session()->flash('warning', trans('shop::app.customer.account.profile.unmatch'));

                return redirect()->back();
            }
        }
        else{
            unset($data['oldpassword']);
            unset($data['password']);
        }

        if ($this->customer->update($data, $id)) {
            Session()->flash('success', trans('shop::app.customer.account.profile.edit-success'));

            return redirect()->route('admin.profile.index');
        } else {
            Session()->flash('success', trans('shop::app.customer.account.profile.edit-fail'));

            return redirect()->back();
        }
    }


}

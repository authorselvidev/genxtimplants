<?php

namespace Webkul\User\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Product\Repositories\ProductReviewRepository as ProductReview;
use Webkul\Customer\Models\CustomerGroup;

use Redirect;
use Session;
use DB;
/**
 * Home page controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
 class MyOffersController extends controller{
	protected $_config;
    public function __construct(
        CustomerRepository $customer,
        ProductReview $productReview
    ){
        $this->_config = request('_config');
        $this->customer = $customer;
    }

    public function index()
	{
		$data['promo_codes'] = DB::table('discounts as d')	
								->join('promo_codes as pc','pc.discount_id','=','d.id')
									->where('d.discount_type',3)
									->where('d.product_id', null)
									->whereIn('pc.for_whom',[auth()->guard('admin')->user()->role_id, '0'])
									->where('d.is_deleted',0)
									->addSelect('d.id','d.discount_title','d.discount_description','d.minimum_order_amount','d.coupon_code', 'd.discount_usage_limit')
									->orderBy('d.minimum_order_amount')->paginate(4); 

		$member_ship = DB::table('discounts')
									->where('discount_type',2)
									->where('product_id',null)
									->where('is_deleted',0)
									->addSelect('id', 'discount_title', 'discount_description', 'customer_group_id', 'minimum_order_amount', 'coupon_code')->get();

		$newarray = array();
		foreach ($member_ship as $key => $value) {
			$arr = explode('-', CustomerGroup::CustomerGroupAmountRange($value->customer_group_id));
			$newarray[$key] =  array("id" => $value->id,
									 "discount_title"=> $value->discount_title,
									 "discount_description" => $value->discount_description,
									 "customer_group_id" => $value->customer_group_id,
									 "minimum_order_amount" => $value->minimum_order_amount,
									 "coupon_code" => $value->coupon_code,
									 "minval" =>  intval(preg_replace('/[^0-9]/', '', $arr[0])),
									 "maxval" => intval(preg_replace('/[^0-9]/', '', $arr[1])));
		}

		usort($newarray, function($a,$b){ return $a['minval']-$b['minval'];});
        $data['member_ship'] = $newarray;
        $data['customer'] = $this->customer->find(auth()->guard('admin')->user()->id);
		
        return view($this->_config['view'], $data);
	}

}
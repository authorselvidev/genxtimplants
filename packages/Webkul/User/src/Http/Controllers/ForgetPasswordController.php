<?php

namespace Webkul\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Mail;
use DB;
use Webkul\Admin\Mail\ForgotPassword;

/**
 * Admin forget password controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ForgetPasswordController extends Controller
{

    use SendsPasswordResetEmails;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_config = request('_config');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'email' => 'required|email'
        ]);

        $response = $this->broker()->sendResetLink(
            request(['email'])
        );

        if ($response == Password::RESET_LINK_SENT) {
            session()->flash('success', trans($response));

            return back();
        }
     /*   $data=array();
        $data['email'] = request('email');
        $token = str_random(60);
        $user = DB::table('users')->where('email',$data['email'])->update(['token' => $token]);
        $data['html'] ='You are receiving this email because we received a password reset request for your account.<br/> Reset Password: '.url('admin/reset-password', $token).'<br/> If you did not request a password reset, no further action is required.';
       $mail_send = Mail::send([], [], function ($message) use ($data) {
                $message->from('info@genxtimplants.com', 'GenXT');
                $message->to($data['email']);
                $message->subject('GenXT - Reset Password');
                $message->setBody( $data['html'], 'text/html' );
            });
       //dd($mail_send);
         session()->flash('success', 'Your reset link has been sent to you email!');
*/
        return back();
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('customers');
    }
}

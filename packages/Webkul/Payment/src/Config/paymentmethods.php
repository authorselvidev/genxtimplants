<?php
return [
    'cashondelivery' => [
        'code' => 'cashondelivery',
        'title' => 'Cash On Delivery',
        'description' => 'Cash On Delivery',
        'class' => 'Webkul\Payment\Payment\CashOnDelivery',
        'active' => true
    ],

    'razorpay' => [
        'code' => 'razorpay',
        'title' => 'RazorPay',
        'description' => 'RazorPay',
        'class' => 'Webkul\Payment\Payment\RazorPay',
        'active' => true
    ],
    'paylaterwithcredit' => [
        'code' => 'paylaterwithcredit',
        'title' => 'Pay Later With Credit',
        'description' => 'Pay Later With Credit',
        'class' => 'Webkul\Payment\Payment\PayLaterWithCredit',
        'active' => true
    ],
    'prepayment' => [
        'code' => 'prepayment',
        'title' => 'PrePayment',
        'description' => 'PrePayment',
        'class' => 'Webkul\Payment\Payment\PrePayment',
        'active' => true
    ],
    'paypal_standard' => [
        'code' => 'paypal_standard',
        'title' => 'Paypal Standard',
        'description' => 'Paypal Standard',
        'class' => 'Webkul\Paypal\Payment\Standard',
        'sandbox' => true,
        'active' => true,
        'business_account' => 'test@webkul.com'
    ]
];
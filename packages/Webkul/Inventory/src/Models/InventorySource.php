<?php

namespace Webkul\Inventory\Models;

use Illuminate\Database\Eloquent\Model;

class InventorySource extends Model
{
    protected $guarded = ['_token'];

    protected $table = 'inventory_sources';

    protected $fillable = ['code','name', 'description', 'dealer_id', 'address', 'postcode','priority','latitude','longitude','status','created_at','updated_at'];
}
<?php

namespace Webkul\Paypal\Payment;

/**
 * Paypal Standard payment method class
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class Standard extends Paypal
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code  = 'paypal_standard';

    /**
     * Line items fields mapping
     *
     * @var array
     */
    protected $itemFieldsFormat = [
        'id'       => 'item_number_%d',
        'name'     => 'item_name_%d',
        'quantity' => 'quantity_%d',
        'price'    => 'amount_%d',
    ];

    /**
     * Return paypal redirect url
     *
     * @var string
     */
    public function getRedirectUrl()
    {
        return route('paypal.standard.redirect');
    }

    /**
     * Return form field array
     *
     * @return array
     */
    public function getFormFields()
    {
        $cart = $this->getCart();

        foreach ($cart->items as $item)
        {
            $product_weights[] = $item->total_weight;
        }

        $earn_points=$apply_membership=$disable_mem="";
        $promocode_price=$reward_coins=$membership_price=0;
        /*$membership_details = session('membership_details');
        $promocode_details = session('promocode_details');
        $reward_details = session('reward_details');
        
        if(isset($promocode_details))
            $promocode_price = $promocode_details['promo_code_price'];
        
        if(isset($membership_details))
            $membership_price = $membership_details['mem_discount_price'];
        
        if(isset($reward_details))
            $reward_coins = $reward_details['coin_value'];   */     
        $cart_details = session('cart_details');  
        if(isset($cart_details)){
            $promocode_price = $cart_details['promo_code_price'];
            $membership_price = $cart_details['mem_discount_price'];
            $reward_coins = $cart_details['reward_coin'];
        }
               

        $discount_price = abs($promocode_price - $reward_coins - $membership_price);

        $fields = [
            'business'        => $this->getConfigData('business_account'),
            'invoice'         => $cart->id,
            'currency_code'   => $cart->cart_currency_code,
            'paymentaction'   => 'sale',
            'return'          => route('paypal.standard.success'),
            'cancel_return'   => route('paypal.standard.cancel'),
            'notify_url'      => route('paypal.standard.ipn'),
            'charset'         => 'utf-8',
            'item_name'       => core()->getCurrentChannel()->name,
            'amount'          => $cart->sub_total,
            'tax'             => $cart->tax_total,
            //'shipping'        => $shipping_amount,
            'discount_amount' => $discount_price
        ];

        if ($this->getIsLineItemsEnabled()) {
            $fields = array_merge($fields, array(
                'cmd'    => '_cart',
                'upload' => 1,
            ));

            $this->addLineItemsFields($fields);

            $this->addShippingAsLineItems($fields, $cart->items()->count() + 1);

            if (isset($fields['tax'])) {
                $fields['tax_cart'] = $fields['tax'];
            }

            if (isset($fields['discount_amount'])) {
                $fields['discount_amount_cart'] = $fields['discount_amount'];
            }
        } else {
            $fields = array_merge($fields, array(
                'cmd'           => '_ext-enter',
                'redirect_cmd'  => '_xclick',
            ));
        }

        $this->addAddressFields($fields);

        return $fields;
    }

    /**
     * Add shipping as item
     *
     * @param array $fields
     * @param int $i
     * @return void
     */
    protected function addShippingAsLineItems(&$fields, $i)
    {
        $cart = $this->getCart();
        foreach ($cart->items as $item)
        {
            $product_weights[] = $item->total_weight;
        }
        $shipping_amount = 0;  
        $total_product_weights = array_sum($product_weights);

        $get_weight_ranges = \DB::table('shipping')->where('status',1)->get();
        foreach($get_weight_ranges as $label => $get_weight_range)
        {
            if($total_product_weights >= $get_weight_range->min_weight && $total_product_weights <= $get_weight_range->max_weight)
            {
                $shipping_amount = $get_weight_range->shipping_amount;
                break;
            }
        }
        $fields[sprintf('item_number_%d', $i)] = "";
        $fields[sprintf('item_name_%d', $i)] = 'Shipping';
        $fields[sprintf('quantity_%d', $i)] = 1;
        $fields[sprintf('amount_%d', $i)] = $shipping_amount;
    }
}
<html>
	<head>
		<title>Test Gpay</title>
	</head>
	<body>
		
		<script type="text/javascript">
			
			function onBuyClicked(){
				if (!window.PaymentRequest) {
					alert('Web payments are not supported in this browser.');
					return;
				}
				
				const supportedInstruments = [
					{
						supportedMethods: ['https://tez.google.com/pay'],
						data: {
							pa: 'mswipe.1400270120003428@kotak',
							pn: 'REGENICS',
							tr: 'GXT253',
							url: 'https://genxtimplants.com/test.php',
							mc: 'K1090780',
							tn: 'Deepak Test Transaction'
						},
					}
				];
				
				const details = {
					total: {
						label: 'Total',
						amount: {
							currency: 'INR',
							value: '1',
							},
						},
					
					displayItems: [{
						label: 'Original Amount',
						amount: {
							currency: 'INR',
							value: '1',
							},
						}],
				};
				
				let request = null;
				try {
					request = new PaymentRequest(supportedInstruments, details);
					return request;
				} catch (e) {
					alert('Payment Request Error: ' + e.message);
					return null;
				}
				
				if (!request) {
					alert('Web payments are not supported in this browser.');
					return null;
				}
			}		
			
			
			const canMakePaymentCache = 'canMakePaymentCache';

			
			function checkCanMakePayment(request) {
				// if (sessionStorage.hasOwnProperty(canMakePaymentCache)) {
				//	return Promise.resolve(JSON.parse(sessionStorage[canMakePaymentCache]));
				//}
				
				var canMakePaymentPromise = Promise.resolve(true);
				
				if (request.canMakePayment) {
					canMakePaymentPromise = request.canMakePayment();
				}
				
				return canMakePaymentPromise
							.then((result) => {
								//sessionStorage[canMakePaymentCache] = result;
								showPaymentUI(request, result);
								return result;
							}).catch((err) => {
								alert('Error calling canMakePayment: ' + err);
							});
			}
			
			function handleNotReadyToPay(){
				alert('Google Pay is not ready to pay.');
			}
						
			function processResponse(instrument) {
				var instrumentString = instrumentToJsonString(instrument);
				//alert(instrumentString);
				
				fetch('https://genxtimplants.com/response_dumper', {
					method: 'POST',
					headers: new Headers({'Content-Type': 'application/json'}),
					body: instrumentString,
					})
					.then(function(buyResult) {
						if (buyResult.ok) {
							return buyResult.json();
							}
							alert('Error sending instrument to server.');
							})
						.then(function(buyResultJson) {
							completePayment(instrument, buyResultJson.status, buyResultJson.message);
					})
				.catch(function(err) {
				   alert('Unable to process payment. ' + err);
				});
			}
			
			function completePayment(instrument, result, msg) {
				instrument.complete(result)
					.then(function() {
						alert('Payment succeeds.');
						alert(msg);
					})
					.catch(function(err) {
						alert(err);
					});
			}

            function instrumentToJsonString(instrument) {
                if (instrument.toJSON) {
                    return JSON.stringify(instrument, undefined, 2);
                } else {
                    return JSON.stringify({
                        methodName: instrument.methodName,
                        details: instrument.details,
                    }, undefined, 2);
                }
            }

			
			function showPaymentUI(request, canMakePayment) {
				if (!canMakePayment) {
					handleNotReadyToPay();
					return;
				}
				
				let paymentTimeout = window.setTimeout(function() {
					window.clearTimeout(paymentTimeout);
					request.abort()
						.then(function() {
							alert('Payment timed out after 20 minutes.');
							})
						.catch(function() {
							alert('Unable to abort, user is in the process of paying.');
						});
				}, 20 * 60 * 1000); /* 20 minutes */
				
				request.show()
					.then(function(instrument) {
						window.clearTimeout(paymentTimeout);
						processResponse(instrument); // Handle response from browser.
					})
					.catch(function(err) {
						alert(err);
					});
			}
			
			const request = onBuyClicked();
			checkCanMakePayment(request);
			
			
		</script>
	</body>
</html>

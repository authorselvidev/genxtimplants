'use strict';
 
const cacheName = 'genxt-implants';
const startPage = 'https://genxtimplants.com';
const offlinePage = 'https://genxtimplants.com';
const filesToCache = [startPage, offlinePage];
const neverCacheUrls = [];

// Install
self.addEventListener('install', function(e) {
	e.waitUntil(
		caches.open(cacheName).then(function(cache) {
			filesToCache.map(function(url) {
				return cache.add(url).catch(function (reason) {
					return console.log('Error: ' + String(reason) + ' ' + url);
				});
			});
		})
	);
});

// Activate
self.addEventListener('activate', function(e) {
	e.waitUntil(
		caches.keys().then(function(keyList) {
			return Promise.all(keyList.map(function(key) {
				if ( key !== cacheName ) {
					console.log('Old cache removed', key);
					return caches.delete(key);
				}
			}));
		})
	);
	return self.clients.claim();
});

// Fetch
self.addEventListener('fetch', function(e) {
	
	if ( ! neverCacheUrls.every(checkNeverCacheList, e.request.url) ) {
	  return;
	}

	if ( ! e.request.url.match(/^(http|https):\/\//i) )
		return;

	if ( new URL(e.request.url).origin !== location.origin )
		return;

	if ( e.request.method !== 'GET' ) {
		e.respondWith(
			fetch(e.request).catch( function() {
				return caches.match(offlinePage);
			})
		);
		return;
	}

	if ( e.request.mode === 'navigate' && navigator.onLine ) {
		e.respondWith(
			fetch(e.request).then(function(response) {
				return caches.open(cacheName).then(function(cache) {
					cache.put(e.request, response.clone());
					return response;
				});  
			})
		);
		return;
	}

	e.respondWith(
		caches.match(e.request).then(function(response) {
			return response || fetch(e.request).then(function(response) {
				return caches.open(cacheName).then(function(cache) {
					cache.put(e.request, response.clone());
					return response;
				});  
			});
		}).catch(function() {
			return caches.match(offlinePage);
		})
	);
});

function checkNeverCacheList(url) {
	if ( this.match(url) ) {
		return false;
	}
	return true;
}
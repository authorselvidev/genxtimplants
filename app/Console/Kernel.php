<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use Log;
use Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
            $schedule->call(function(){
                Log::info(date('Y-m-d H:i:s a'));
            })->everyMinute();
    		
            
            $schedule->call(function(){
                //$to = array('preethi@authorselvi.com', 'rajesh.settu@gmail.com', 'baki@authorselvi.com', 'deepak@authorselvi.com');
                $to = array('deepak@authorselvi.com');

                Mail::send('shop::emails.admin.cron-check', $to ,function ($message) use ($to){
                        $message->from('info@genxtimplants.com', 'GenXT');
                        $message->to($to)->subject('GenXT CronJob - Status');
                });         
                
            })->hourly('11:00');
            

            $schedule->call(function(){
                DB::table('users')->where('is_verified', 0)->where('created_at', '<', DB::raw('DATE_SUB(NOW(), INTERVAL 2 DAY)'))->delete();
            })->hourly();
        */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
